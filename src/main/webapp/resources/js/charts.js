
$(document).ready(function() {
	
//setDynamicChart(rec,exp);
	
	$.ajax
	({
		type : "Get",
		url : myContextPath+"/fetchTopChart",
		success : function(data) 
		{
			
			$('#totalSalesId').html(data.totalSales.toFixed(2)+" &#8377;");
			$('#totalAmountInvestInMarketId').html(parseFloat(data.totalAmountInvestInMarket).toFixed(2)+" &#8377;");
			$('#totalValueOfCurrentInventoryId').html(parseFloat(data.totalValueOfCurrentInventory).toFixed(2)+" &#8377;");
			$('#productUnderThresholdCountId').html(data.productUnderThresholdCount);
		}
	});
	$.ajax
	({
		type : "Get",
		url : myContextPath+"/fetchTotalAmountChart",
		success : function(data) 
		{
			setTotalAmountChart(data.recieved,data.expected);
		}
	});
	$.ajax
	({
		type : "Get",
		url : myContextPath+"/fetchTopFiveProductChart",
		success : function(data) 
		{	
			var productDetailsArray=[];
			productDetailsArray[0]=data.productName1;
			productDetailsArray[1]=data.productPercentage1;
			productDetailsArray[2]=data.productName2;
			productDetailsArray[3]=data.productPercentage2;
			productDetailsArray[4]=data.productName3;
			productDetailsArray[5]=data.productPercentage3;
			productDetailsArray[6]=data.productName4;
			productDetailsArray[7]=data.productPercentage4;
			productDetailsArray[8]=data.productName5;
			productDetailsArray[9]=data.productPercentage5;
			
			setTopFiveProductChart(productDetailsArray);
		}
	});
	$.ajax
	({
		type : "Get",
		url : myContextPath+"/fetchTopFiveSallersChart",
		success : function(data) 
		{
			var sallerDetailsArray=[];
			sallerDetailsArray[0]=data.salesManName1;
			sallerDetailsArray[1]=data.salesManPercentage1;
			sallerDetailsArray[2]=data.salesManName2;
			sallerDetailsArray[3]=data.salesManPercentage2;
			sallerDetailsArray[4]=data.salesManName3;
			sallerDetailsArray[5]=data.salesManPercentage3;
			sallerDetailsArray[6]=data.salesManName4;
			sallerDetailsArray[7]=data.salesManPercentage4;
			sallerDetailsArray[8]=data.salesManName5;
			sallerDetailsArray[9]=data.salesManPercentage5;
			setTopFiveSallersChart(sallerDetailsArray);
		}
	});
	$.ajax
	({
		type : "Get",
		url : myContextPath+"/fetchTopFiveReturnReportChart",
		success : function(data) 
		{
			var returnDetailsArray=[];
			returnDetailsArray[0]=data.returnProductName1;
			returnDetailsArray[1]=data.returnProductPercentage1;
			returnDetailsArray[2]=data.returnProductName2;
			returnDetailsArray[3]=data.returnProductPercentage2;
			returnDetailsArray[4]=data.returnProductName3;
			returnDetailsArray[5]=data.returnProductPercentage3;
			returnDetailsArray[6]=data.returnProductName4;
			returnDetailsArray[7]=data.returnProductPercentage4;
			returnDetailsArray[8]=data.returnProductName5;
			returnDetailsArray[9]=data.returnProductPercentage5;
			setTopFiveReturnReportChart(returnDetailsArray);			
		}
	});
	
});
function setTotalAmountChart(rc,ep){
	
     Highcharts.chart('container1', {
    	 
         colors: ['#f45b5b', '#b0bec5 '],
         chart: {
             backgroundColor: '#eceff1',
             style: {
                 fontFamily: 'sans-serif'
             },
             spacingBottom: 5,
             spacingTop: 5,
             spacingLeft: 5,
             spacingRight: 5,
         },
         credits: ({
             enabled: false
         }),
         legend: {
             itemStyle: {
                 fontWeight: 'bold',
                 fontSize: '13px',
             }
         },
         title: {
             text: 'Total Amount'
         },
         tooltip: {
             pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
         },
         plotOptions: {
             pie: {
                 allowPointSelect: true,
                 cursor: 'pointer',

                 dataLabels: {
                     enabled: true,
                     format: '<b>{point.name}%</b>: {point.percentage:.1f} %',
                     style: {
                         color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                     }

                 }
             }
         },
         series: [{
             type: 'pie',
             name: 'Total',
             data: [

                 ['Expected', ep], {
	                 name: 'Recieved', y: rc,
	                 sliced: false,
	                 selected: true
                 }
             ],
             center: [260, 80],
             size: 150,
             height: 100,
             showInLegend: true,
             dataLabels: {
                 enabled: true
             }
         }],

     });
     
     //setTimeout(setDynamicChart, 2000);
     
	}
function setTopFiveProductChart(productDetailsArray){
     Highcharts.chart('container2', {


         colors: ['#ff8a80 ', '#aa00ff ', '#00e676', '#c51162', '#ff9100'],
         title: {
             text: 'Top 5 Sold Product',

         },

         chart: {
             backgroundColor: '#eceff1 ',
             style: {
                 fontFamily: 'sans-serif'
             },
             spacingBottom: 5,
             spacingTop: 5,
             spacingLeft: 5,
             spacingRight: 5,

         },
         credits: ({
             enabled: false
         }),
         tooltip: {
             pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
         },
         plotOptions: {
             pie: {
                 allowPointSelect: true,
                 cursor: 'pointer',

                 dataLabels: {
                     enabled: true,
                     format: '<b>{point.name}%</b>: {point.percentage:.1f} %',
                     style: {
                         color: (Highcharts.theme && Highcharts.theme.contrastTextColor) ||
                             'black'
                     }
                 }
             }
         },
         series: [{
             type: 'pie',
             name: 'Sold',
             data: 
             [
                 [productDetailsArray[1], productDetailsArray[0]],
                 [productDetailsArray[3], productDetailsArray[2]],
                 [productDetailsArray[5], productDetailsArray[4]],
                 [productDetailsArray[7], productDetailsArray[6]], 
                 {                	 
                     name: productDetailsArray[9],
                     y: productDetailsArray[8],
                     sliced: true,
                     selected: true
                 }
             ],
             center: [260, 80],
             size: 150,
             showInLegend: true,
             dataLabels: {
                 enabled: true
             }
         }],




     });
}

function setTopFiveSallersChart(sallerDetailsArray){
     Highcharts.chart('container3', {

         colors: ['#f48fb1 ', '#90caf9 ', '#26a69a ', '#ffca28', '#b0bec5'

         ],
         title: {
             text: 'Top 5 Sellers'
         },
         chart: {
             backgroundColor: '#eceff1 ',
             style: {
                 fontFamily: 'sans-serif'
             },
             spacingBottom: 5,
             spacingTop: 5,
             spacingLeft: 5,
             spacingRight: 5,

         },
         credits: ({
             enabled: false
         }),
         tooltip: {
             pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
         },
         plotOptions: {
             pie: {
                 allowPointSelect: true,
                 cursor: 'pointer',

                 dataLabels: {
                     enabled: true,
                     format: '<b>{point.name}%</b>: {point.percentage:.1f} %',
                     style: {
                         color: (Highcharts.theme && Highcharts.theme.contrastTextColor) ||
                             'black'
                     }
                 }
             }
         },
         series: [{
             type: 'pie',
             name: 'Employees',
             data: 
             [
                 [sallerDetailsArray[1], sallerDetailsArray[0]],
                 [sallerDetailsArray[3], sallerDetailsArray[2]],
                 [sallerDetailsArray[5], sallerDetailsArray[4]],
                 [sallerDetailsArray[7], sallerDetailsArray[6]], 
                 {                	 
                     name: sallerDetailsArray[9],
                     y: sallerDetailsArray[8],
                     sliced: true,
                     selected: true
                 }
             ],
             center: [260, 80],
             size: 150,
             showInLegend: true,

             dataLabels: {
                 enabled: true
             }
         }],


     });
}
function setTopFiveReturnReportChart(returnDetailsArray){
     Highcharts.chart('container4', {



         colors: ['#8d4654', '#7798BF', '#aaeeee', '#ff0066', '#eeaaee'],
         title: {
             text: 'Top 5 Return Item',

         },
         chart: {
             backgroundColor: '#eceff1 ',
             style: {
                 fontFamily: 'sans-serif'
             },
             spacingBottom: 5,
             spacingTop: 5,
             spacingLeft: 5,
             spacingRight: 5,

         },
         credits: ({
             enabled: false
         }),
         tooltip: {
             pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
         },
         plotOptions: {
             pie: {
                 allowPointSelect: true,
                 cursor: 'pointer',

                 dataLabels: {
                     enabled: true,
                     format: '<b>{point.name}%</b>: {point.percentage:.1f} %',
                     style: {
                         color: (Highcharts.theme && Highcharts.theme.contrastTextColor) ||
                             'black'
                     }
                 }
             }
         },
         series: [{
             type: 'pie',
             name: 'Blue Square',
             data: 
             [
                 [returnDetailsArray[1], returnDetailsArray[0]],
                 [returnDetailsArray[3], returnDetailsArray[2]],
                 [returnDetailsArray[5], returnDetailsArray[4]],
                 [returnDetailsArray[7], returnDetailsArray[6]], 
                 {                	 
                     name: returnDetailsArray[9],
                     y: returnDetailsArray[8],
                     sliced: true,
                     selected: true
                 }
             ],
             center: [260, 80],
             size: 150,

             showInLegend: true,
             dataLabels: {
                 enabled: true
             }
         }],
     });
}
   
