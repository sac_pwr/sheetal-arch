  var productidlist = new Array();
 var count = 1;
 var userIdValid=true; 
 var areaASMCheck=false;
 var routeSMCheck=false;   
$(document).ready(function() {
						  $('.preloader-background').hide();
						$('#Cnfrmpassword').keyup(function() {

				               var pass = $('#password').val();
				               var cpass = $('#Cnfrmpassword').val();

				               if (pass !== cpass) {
				                   // alert(cpass);
				                   $('#CnfrmpasswordLabel').text("Password not matched").attr('class', 'red-text');
				                   // $('#CnfrmpasswordLabel').show();
				               } else {
				                   $('#CnfrmpasswordLabel').text("Password  matched").attr('class', 'green-text');
				                   // $('#CnfrmpasswordLabel').show();
				               }
				           });
						
						$('#mobileNo').on("keyup change blur",function() {
							var mobileNo = $('#mobileNo').val();
							//alert(mobileNo);
							if (mobileNo.length !== 10) {
				                   // alert(cpass);
				                   $('#mobileNumberlabel').text("Mobile Number Must be 10 Digit").attr('class', 'red-text');
				                   // $('#CnfrmpasswordLabel').show();
				               } else {
				                   $('#mobileNumberlabel').text("Right").attr('class', 'green-text');
				                   // $('#CnfrmpasswordLabel').show();
				               }
						});
						
						$('#userId').blur(function(){
							//alert('1');
							var userId=$('#userId').val();
							var employeeId=$('#employeeId').val();
							//alert(userId);
							$.ajax({
								url : myContextPath+"/checkUserIdForUpdate?userId="+userId+"&employeeId="+employeeId,
								dataType : "json",
								success : function(data) {
									
									/*input:not([type]).valid+label:after, 
									input:not([type]):focus.valid+label:after, 
									input[type=text].valid+label:after, 
									input[type=text]:focus.valid+label:after,*/
									
									if(data==true)
									{
										$("#useridvalid").attr("data-success", "UserId Available");
										$('#userId').css({"border-bottom":"1px solid green","box-shadow":"0 1px 0 0 green"});
										userIdValid=true;
									}
									else
									{
										$("#useridvalid").attr("data-success", "UserId Not Available");
										$("#useridvalid.active::after").attr("class","red-text");
										$('#userId').css({"border-bottom":"1px solid red","box-shadow":"0 1px 0 0 red"});
										userIdValid=false;
										
									}
								}
							});
							
						});
						
						
						$('#mobileNo').keypress(function( event ){
						    var key = event.which;
						    
						    if( ! ( key >= 48 && key <= 57 || key === 13) )
						        event.preventDefault();
						});
						
						
						
						$('#saveEmployeeSubmit').click(function(){
							
							var mobileNo = $('#mobileNo').val();							
							if (mobileNo.length !== 10) {				                   
								/*$('#addeditmsg').modal('open');
			           	     	$('#msgHead').text("Mobile Number Warning");
			           	     	$('#msg').text("Check Your Mobile Number");*/
			           	     	$('#mobileNo').focus();
			           	        $('#mobileNumberlabel').text("Mobile Number Must be 10 Digit").attr('class', 'red-text');
			           	     	return false;   
				              }
							
									
							$('#departmentidLabel').text("");
							$('.clusterLabel').text('');
							$('#areaLabel').text('');
							$('#routeLabel').text('');
							
							var department=$("#departmentid").val();
						
							if(department==='0')
							 {
							     $('#departmentidLabel').text("Atleast one Department need to select");
							     return false;
							 }
							 else if(department!='0')
							 {
								//var Mulcluster=$("#cluster").val();
								var SinCluster=$("#SelectSingleCluster").val();
								var departmentTxt=$("#departmentid option:selected").text();
								var area=$("#areaId").val();
								var route=$("#routeId").val();
								if(departmentTxt ==="Gatekeeper")
									{
									if(SinCluster ==='0'){
										 $('.clusterLabel').text("Atleast one Cluster need to select");
										 return false;
										}
									}
							
								
								 else if(departmentTxt ==="AreaSalesMan"){
									 if(SinCluster ==='0'){
										 $('.clusterLabel').text("Atleast one Cluster need to select");
										 return false;
										}
									 else if(area==='0'){
							    	 $('#areaLabel').text("Atleast one area need to select");	
							    	 return false;
									 }
									 else if(areaASMCheck==true)
									 {
										 $('#areaLabel').text("This area already assiged To other ASM");	
										 return false;
									 }
							     }
								 else if(departmentTxt ==="SalesMan"){
									 if(SinCluster ==='0'){
										 $('.clusterLabel').text("Atleast one Cluster need to select");
										 return false;
										}
									 else if(area==='0'){
								    	 $('#areaLabel').text("Atleast one area need to select");	
								    	 return false;
										 }
									 else if(route==='0'){
								    	 $('#routeLabel').text("Atleast one route need to select");	
								    	 return false;
										 }
									 else if(routeSMCheck==true)
									 {
										 $('#routeLabel').text("This Route already assiged To other SalesMan");
										 return false;
									 }
							     }
								
							 }
							
							if(userIdValid==false)
							{
								$("#useridvalid").attr("data-success", "UserId Not Available");
								$("#useridvalid.active::after").attr("class","red-text");
								$('#userId').css({"border-bottom":"1px solid red","box-shadow":"0 1px 0 0 red"}); 
							     return false;
							}
							var pass=$('#password').val();
							var confpass=$('#Cnfrmpassword').val();
							if(pass!==confpass)
							{
								$('#addeditmsg').modal('open');
			           	     	$('#msgHead').text("Password Warning");
			           	     	$('#msg').text("Confirm password didn't Match With Password");
			           	        $("#useridvalid.active::after").attr("class","red-text");
								$('#userId').css({"border-bottom":"1px solid red","box-shadow":"0 1px 0 0 red"});
								$('#userId').focus();
			           	     	return false;
							}		
						});
						
						
						
						$("#SelectSingleCluster").on("change", function() {
							
							if($("#departmentid option:selected").text()==="GateKeeper"){
								
								var clusterId=$("#SelectSingleCluster").val();
								$.ajax({
									url : myContextPath+"/addClusterInCart?clusterId="+clusterId,
									dataType : "json",
									beforeSend: function() {
										$('.preloader-background').show();
										$('.preloader-wrapper').show();
							           },
									success : function(data) {
										
										clusterList=data;
										$('#tblDataCluster').empty();
										var srno=1;
										for(i=0; i<clusterList.length; i++)
										{
											$('#tblDataCluster').append("<tr>"+
													    "<td>"+srno+"</td>"+
							                            "<td>"+clusterList[i].name+"</td>"+
							                            "<td><button  type='button'  onclick='removecluster("+clusterList[i].clusterId+")' class='btn btn-flat'><i class='material-icons'>clear</i></button></td>"+
							                            "</tr>");
											srno++;
										}
										$('#tblDataCluster').change();
										$('.preloader-wrapper').hide();
										$('.preloader-background').hide();
									},
									error: function(xhr, status, error) {
										$('.preloader-wrapper').hide();
										$('.preloader-background').hide();
										  //alert(error +"---"+ xhr+"---"+status);
										$('#addeditmsg').modal('open');
					           	     	$('#msgHead').text("Message : ");
					           	     	$('#msg').text("Something Went Wrong cluster"); 
					           	     		setTimeout(function() 
											  {
					  	     					$('#addeditmsg').modal('close');
											  }, 1000);
										}
									});
								
								
								return false;
							}
							
							var clusterId=$('#SelectSingleCluster').val();
							
							// Get the raw DOM object for the select box
							var select = document.getElementById('areaId');

							// Clear the old options
							select.options.length = 0;
							//$('#countryListForState').html('');

							//Load the new options
							select.options.add(new Option("Choose Area", 0));
							
							$.ajax({
								url : myContextPath+"/fetchAreaByClusterId?clusterId="+clusterId,
								dataType : "json",
								beforeSend: function() {
									$('.preloader-background').show();
									$('.preloader-wrapper').show();
						           },
								success : function(data) {
																	
								var options, index, option;
								select = document.getElementById('areaId');

								for (var i = 0, len = data.length; i < len; ++i) {
									var area = data[i];
									select.options.add(new Option(area.name, area.areaId));
								}	
									
								$('.preloader-wrapper').hide();
								$('.preloader-background').hide();
							},
							error: function(xhr, status, error) {
								$('.preloader-wrapper').hide();
								$('.preloader-background').hide();
								  //alert(error +"---"+ xhr+"---"+status);
								/*$('#addeditmsg').modal('open');
			           	     	$('#msgHead').text("Message : ");
			           	     	$('#msg').text("Something Went Wrong cluster other"); 
			           	     		setTimeout(function() 
									  {
			  	     					$('#addeditmsg').modal('close');
									  }, 1000);*/
								}
							});
							
						});
						
						$("#areaId").on("change", function() {
							var areaId=$('#areaId').val();
							var departmentTxt=$("#departmentid option:selected").text();
							var departmentId=$("#departmentid").val();
						 if(departmentId!=="0")
						 {
							if(departmentTxt !=="AreaSalesMan")
							{
							
								// Get the raw DOM object for the select box
									var select = document.getElementById('routeId');
		
									// Clear the old options
									select.options.length = 0;
									//$('#countryListForState').html('');
		
									//Load the new options
									select.options.add(new Option("Choose Route", 0));
									
									$.ajax({
										url : myContextPath+"/fetchRouteByAreaId?areaId="+areaId,
										dataType : "json",
										beforeSend: function() {
											$('.preloader-background').show();
											$('.preloader-wrapper').show();
								           },
										success : function(data) {
																			
										var options, index, option;
										select = document.getElementById('routeId');
		
										for (var i = 0, len = data.length; i < len; ++i) {
											var route = data[i];
											select.options.add(new Option(route.routeNo, route.routeId));
										}	
											
										$('.preloader-wrapper').hide();
										$('.preloader-background').hide();
									},
									error: function(xhr, status, error) {
										$('.preloader-wrapper').hide();
										$('.preloader-background').hide();
										  //alert(error +"---"+ xhr+"---"+status);
										/*$('#addeditmsg').modal('open');
					           	     	$('#msgHead').text("Message : ");
					           	     	$('#msg').text("Something Went Wrong"); 
					           	     		setTimeout(function() 
											  {
					  	     					$('#addeditmsg').modal('close');
											  }, 1000);*/
										}
									});							
							}
						 
							if(departmentTxt ==="AreaSalesMan")
							{
								var employeeDetailsId=$('#employeeDetailsId').val();
								$.ajax({
				    				url : myContextPath+"/checkAreaAssignedToASMForUpdate?areaId=" + areaId+"&employeeDetailsId="+employeeDetailsId,
				    				//dataType : "json",
				    				beforeSend: function() {
										$('.preloader-background').show();
										$('.preloader-wrapper').show();
							           },
				    				success : function(data) {
				    						
				    					if(data==="Failed")
				    					{
				    						$('#areaLabel').text("This area already assiged To other ASM");
				               	     		areaASMCheck=true;
				    					}
				    					else if(data==="Success")
				    					{
				    						$('#areaLabel').text("");
				    						areaASMCheck=false;
				    					}
				    					
				    					$('.preloader-wrapper').hide();
										$('.preloader-background').hide();
				    				},
									error: function(xhr, status, error) {
										$('.preloader-wrapper').hide();
										$('.preloader-background').hide();
										  //alert(error +"---"+ xhr+"---"+status);
										/*$('#addeditmsg').modal('open');
				               	     	$('#msgHead').text("Message : ");
				               	     	$('#msg').text("Something Went Wrong check Area"); 
				               	     		setTimeout(function() 
											  {
				      	     					$('#addeditmsg').modal('close');
											  }, 1000);*/
										}
				    			});
							}
						 }
						});
						
						
						$("#departmentid").change(function(){
							routeSMCheck=false;
							areaASMCheck=false;
							$('#routeLabel').text("");
							$('#areaLabel').text("");
						});
						
						$("#routeId").on("change", function() {
							var routeId=$('#routeId').val();
							var departmentTxt=$("#departmentid option:selected").text();
							var departmentId=$("#departmentid").val();
						 if(departmentId!=="0")
						 {
							if(departmentTxt ==="SalesMan")
							{
								var employeeDetailsId=$('#employeeDetailsId').val();
								$.ajax({
				    				url : myContextPath+"/checkRouteAssignedToSMForUpdate?routeId=" + routeId+"&employeeDetailsId="+employeeDetailsId,
				    				//dataType : "json",
				    				beforeSend: function() {
										$('.preloader-background').show();
										$('.preloader-wrapper').show();
							           },
				    				success : function(data) {
				    						
				    					if(data==="Failed")
				    					{
				    						$('#routeLabel').text("This Route already assiged To other SalesMan");
				    						routeSMCheck=true;
				    					}
				    					else if(data==="Success")
				    					{
				    						$('#routeLabel').text("");
				    						routeSMCheck=false;
				    					}
				    					
				    					$('.preloader-wrapper').hide();
										$('.preloader-background').hide();
				    				},
									error: function(xhr, status, error) {
										$('.preloader-wrapper').hide();
										$('.preloader-background').hide();
										  //alert(error +"---"+ xhr+"---"+status);
										/*$('#addeditmsg').modal('open');
				               	     	$('#msgHead').text("Message : ");
				               	     	$('#msg').text("Something Went Wrong check route"); 
				               	     		setTimeout(function() 
											  {
				      	     					$('#addeditmsg').modal('close');
											  }, 1000);*/
										}
				    			});
							}
						 }
						});
						
					});


function removecluster(id)
{

	$.ajax({
		url : myContextPath+"/removeClusterInCart?clusterId="+id,
		dataType : "json",
		beforeSend: function() {
			$('.preloader-background').show();
			$('.preloader-wrapper').show();
           },
		success : function(data) {
			
			clusterList=data;
			$('#tblDataCluster').empty();
			var srno=1;
			for(i=0; i<clusterList.length; i++)
			{
				$('#tblDataCluster').append("<tr>"+
						    "<td>"+srno+"</td>"+
                            "<td>"+clusterList[i].name+"</td>"+
                            "<td><button type='button' onclick='removecluster("+clusterList[i].clusterId+")' class='btn btn-flat'><i class='material-icons'>clear</i></button></td>"+
                            "</tr>");
				srno++;
			}
			
			$('.preloader-wrapper').hide();
			$('.preloader-background').hide();
		},
		error: function(xhr, status, error) {
			$('.preloader-wrapper').hide();
			$('.preloader-background').hide();
			  //alert(error +"---"+ xhr+"---"+status);
			$('#addeditmsg').modal('open');
   	     	$('#msgHead').text("Message : ");
   	     	$('#msg').text("Something Went Wrong"); 
   	     		setTimeout(function() 
				  {
   					$('#addeditmsg').modal('close');
				  }, 1000);
			}
		});
	
}