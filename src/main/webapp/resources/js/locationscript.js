var options, index, option;	
$(document)
			.ready(
					function() {
						
			      
			            $('.preloader-background').hide();
						$('#areawork').click();
						//  for no of entries and global search
						$('#tblDataArea').DataTable({
					         "oLanguage": {
					             "sLengthMenu": "Display Records _MENU_",
					             "sSearch": " _INPUT_" //search
					         },
					         lengthMenu: [
					                      [10, 25., 50, -1],
					                      ['10 ', '25 ', '50 ', 'All']
					                  ],
					       
					         dom: 'lBfrtip',
					         buttons: {
					             buttons: [
					                 //      {
					                 //      extend: 'pageLength',
					                 //      className: 'pageLengthButton waves-effect waves-light   white-text blue-grey lighten-1'
					                 //  }, 
					                 {
					                     extend: 'pdf',
					                     className: 'pdfButton waves-effect waves-light  grey lighten-3 light-blue-text text-darken-4 z-depth-2',
					                     text: '<i class="fa fa-file-pdf-o"></i> &nbsp;<span style="font-size:15px;">PDF<span>',
					                     //title of the page
					                     title: 'Area Details',
					                     //file name 
					                     filename: 'areadetails',
					                    
					                     //  exports only dataColumn
					                     exportOptions: {
					                         columns: ':visible.print-col'
					                     },
					                 },
					                 {
					                     extend: 'excel',
					                     className: 'excelButton waves-effect waves-light grey lighten-3 light-blue-text text-darken-4 z-depth-2',
					                     text: '<i class="fa fa-file-excel-o  fa-fw"></i> &nbsp;<span style="font-size:15px;">EXCEL<span>',
					                     //title of the page
					                     title: 'Area Details',
					                     //file name 
					                     filename: 'areadetails',
					                     //  exports only dataColumn
					                     exportOptions: {
					                         columns: ':visible.print-col'
					                     },
					                 },
					                 {
					                     extend: 'print',
					                     className: 'printButton waves-effect waves-light grey lighten-3 light-blue-text text-darken-4 z-depth-2',
					                     text: '<i class="fa fa-print fa-fw"></i> &nbsp;<span style="font-size:15px;">PRINT<span>',
					                     //title of the page
					                     title:'Area Details',
					                     //file name 
					                     filename: 'areadetails',
					                     //  exports only dataColumn
					                     exportOptions: {
					                         columns: ':visible.print-col'
					                     },
					                 },
					                 {
					                     extend: 'colvis',
					                     className: 'colvisButton waves-effect waves-light grey lighten-3 light-blue-text text-darken-4 z-depth-2',
					                     text: '<span style="font-size:15px;">COLUMN VISISBILITY<span>&nbsp;&nbsp;<i class="fa fa-caret-down fa-fw"></i> ',
					                     collectionLayout: 'fixed two-column',
					                     align: 'left'
					                 },
					             ]
					         }
								});
						$('#tblDataRoute').DataTable({
					         "oLanguage": {
					             "sLengthMenu": "Display Records _MENU_",
					             "sSearch": " _INPUT_" //search
					         },
					         lengthMenu: [
					                      [10, 25., 50, -1],
					                      ['10 ', '25 ', '50 ', 'All']
					                  ],
					        
					         dom: 'lBfrtip',
					         buttons: {
					             buttons: [
					                 //      {
					                 //      extend: 'pageLength',
					                 //      className: 'pageLengthButton waves-effect waves-light   white-text blue-grey lighten-1'
					                 //  }, 
					                 {
					                     extend: 'pdf',
					                     className: 'pdfButton waves-effect waves-light grey lighten-3 light-blue-text text-darken-4 z-depth-2',
					                     text: '<i class="fa fa-file-pdf-o"></i> &nbsp;<span style="font-size:15px;">PDF<span>',
					                     //title of the page
					                     title: 'Region Deatails',
					                     //file name 
					                     filename: 'regiondetails',
					                     //  exports only dataColumn
					                     exportOptions: {
					                         columns: ':visible.print-col'
					                     },
					                 },
					                 {
					                     extend: 'excel',
					                     className: 'excelButton waves-effect waves-light  grey lighten-3 light-blue-text text-darken-4 z-depth-2',
					                     text: '<i class="fa fa-file-excel-o  fa-fw"></i> &nbsp;<span style="font-size:15px;">EXCEL<span>',
					                     //title of the page
					                     title:'Region Deatails',
					                     //file name 
					                     filename:  'regiondetails',
					                     //  exports only dataColumn
					                     exportOptions: {
					                         columns: ':visible.print-col'
					                     },
					                 },
					                 {
					                     extend: 'print',
					                     className: 'printButton waves-effect waves-light  grey lighten-3 light-blue-text text-darken-4 z-depth-2',
					                     text: '<i class="fa fa-print fa-fw"></i> &nbsp;<span style="font-size:15px;">PRINT<span>',
					                     //title of the page
					                     title: 'Region Deatails',
					                     //file name 
					                     filename:  'regiondetails',
					                     //  exports only dataColumn
					                     exportOptions: {
					                         columns: ':visible.print-col'
					                     },
					                 },
					                 {
					                     extend: 'colvis',
					                     className: 'colvisButton waves-effect waves-light grey lighten-3 light-blue-text text-darken-4 z-depth-2',
					                     text: '<span style="font-size:15px;">COLUMN VISISBILITY<span>&nbsp;&nbsp;<i class="fa fa-caret-down fa-fw"></i> ',
					                     collectionLayout: 'fixed two-column',
					                     align: 'left'
					                 },
					             ]
					         }
								});

						$('#tblDataCluster').DataTable({
					         "oLanguage": {
					             "sLengthMenu": "Display Records _MENU_",
					             "sSearch": " _INPUT_" //search
					         },
					         lengthMenu: [
					                      [10, 25., 50, -1],
					                      ['10 ', '25 ', '50 ', 'All']
					                  ],
					         
					         dom: 'lBfrtip',
					         buttons: {
					             buttons: [
					                 //      {
					                 //      extend: 'pageLength',
					                 //      className: 'pageLengthButton waves-effect waves-light   white-text blue-grey lighten-1'
					                 //  }, 
					                 {
					                     extend: 'pdf',
					                     className: 'pdfButton waves-effect waves-light grey lighten-3 light-blue-text text-darken-4 z-depth-2',
					                     text: '<i class="fa fa-file-pdf-o"></i> &nbsp;<span style="font-size:15px;">PDF<span>',
					                     //title of the page
					                     title: 'City Details',
					                     //file name 
					                     filename:'citydetails',
					                     //  exports only dataColumn
					                     exportOptions: {
					                         columns: ':visible.print-col'
					                     },
					                 },
					                 {
					                     extend: 'excel',
					                     className: 'excelButton waves-effect waves-light  grey lighten-3 light-blue-text text-darken-4 z-depth-2',
					                     text: '<i class="fa fa-file-excel-o  fa-fw"></i> &nbsp;<span style="font-size:15px;">EXCEL<span>',
					                     //title of the page
					                     title:  'City Details',
					                     //file name 
					                     filename: 'citydetails',
					                     //  exports only dataColumn
					                     exportOptions: {
					                         columns: ':visible.print-col'
					                     },
					                 },
					                 {
					                     extend: 'print',
					                     className: 'printButton waves-effect waves-light grey lighten-3 light-blue-text text-darken-4 z-depth-2',
					                     text: '<i class="fa fa-print fa-fw"></i> &nbsp;<span style="font-size:15px;">PRINT<span>',
					                     //title of the page
					                     title:  'City Details',
					                     //file name 
					                     filename:'citydetails',
					                     //  exports only dataColumn
					                     exportOptions: {
					                         columns: ':visible.print-col'
					                     },
					                 },
					                 {
					                     extend: 'colvis',
					                     className: 'colvisButton waves-effect waves-light grey lighten-3 light-blue-text text-darken-4 z-depth-2',
					                     text: '<span style="font-size:15px;">COLUMN VISISBILITY<span>&nbsp;&nbsp;<i class="fa fa-caret-down fa-fw"></i> ',
					                     collectionLayout: 'fixed two-column',
					                     align: 'left'
					                 },
					             ]
					         }
								});
						
						 $("select")
		                .change(function() {
		                    var t = this;
		                    var content = $(this).siblings('ul').detach();
		                    setTimeout(function() {
		                        $(t).parent().append(content);
		                        $("select").material_select();
		                    }, 200);
		                });
					      $('select').material_select();
					      $('.dataTables_filter input').attr("placeholder", "       search");
					      //stop form submitting
					      $("form").submit(function(e){
					          e.preventDefault();
					      });
					      
					      
					      
						$('#clusterTab').click(function() {
								
									var t = $('#tblDataCluster').DataTable();
									t.clear().draw();
									
									$.ajax({
												type : "GET",
												url : myContextPath+"/fetchClusterList",
												/* data: "id=" + id + "&name=" + name, */
												beforeSend: function() {
													$('.preloader-background').show();
													$('.preloader-wrapper').show();
										           },

												success : function(data) {
													var srno = 1;
													//$("#tblDataCountry tbody").remove();

													//alert("data clean");

													var count = $("#tblDataCountry").find("tr:first th").length;
													//alert(count);

													if (data == null) {
														    t.row.add('<tr>'
																	+ '<td colspan="'+count+'"><center>No Data Found</center></td>'
																	+ '</tr>');
													}
													for (var i = 0, len = data.length; i < len; ++i) {
														var cluster = data[i];
														t.row.add([
																		srno,
																		cluster.name,
																		"<button onclick='openClusterEdit("+cluster.clusterId+")' class='btn-flat'><i class='material-icons tooltipped blue-text' data-position='right' data-delay='50' data-tooltip='Edit'>edit</i></button>"

																  ]).draw(false); 

														srno = srno + 1;
													}
													$('.preloader-wrapper').hide();
													$('.preloader-background').hide();
							    				},
												error: function(xhr, status, error) {
													$('.preloader-wrapper').hide();
													$('.preloader-background').hide();
													  //alert(error +"---"+ xhr+"---"+status);
													$('#addeditmsg').modal('open');
							               	     	$('#msgHead').text("Message : ");
							               	     	$('#msg').text("Something Went Wrong"); 
							               	     		setTimeout(function() 
														  {
							      	     					$('#addeditmsg').modal('close');
														  }, 1000);
													}
											});

								});
						
						$("#saveClusterSubmit").click(function() {
									
							$('#errorForCluster').html('');
									if($('#txtAddCluster').val().trim()==="")
									{
										$('#errorForCluster').html("<font color='red'>Enter Cluster Name</font>");
										return false;
									}
									
									var form = $('#saveClusterForm');

									$.ajax({

												type : form.attr('method'),
												url : form.attr('action'),
												beforeSend: function() 
												{
													$('.preloader-background').show();
													$('.preloader-wrapper').show();
										        },
												data : form.serialize(),
												success : function(data) {
													
														if(data=="Success")
														{
															$('#addCluster').modal('close');
															
															$('#addeditmsg').modal('open');
									               	     	$('#msgHead').text("Message : ");
									               	     	$('#msg').html("<font color='green'>Cluster Saved SuccessFully</font>"); 
									               	     	
															$('#clusterTab').click();
														}
														else if(data==="UpdateSuccess")
														{
															$('#addCluster').modal('close');
															
															$('#addeditmsg').modal('open');
									               	     	$('#msgHead').text("Message : ");
									               	     	$('#msg').html("<font color='green'>Cluster Updated SuccessFully</font>"); 
									               	     	
															$('#clusterTab').click();
														}
														else
														{
															$('#errorForCluster').html("<font color='red'>"+data+"</font>");
														}
													
														$('.preloader-wrapper').hide();
														$('.preloader-background').hide();
								    				},
													error: function(xhr, status, error) {
														$('.preloader-wrapper').hide();
														$('.preloader-background').hide();
														  //alert(error +"---"+ xhr+"---"+status);
														$('#addeditmsg').modal('open');
								               	     	$('#msgHead').text("Message : ");
								               	     	$('#msg').text("Something Went Wrong"); 
								               	     		setTimeout(function() 
															  {
								      	     					$('#addeditmsg').modal('close');
															  }, 1000);
														}
											});
									return false;
								});
						
						$('#areaTab').click(function() {
							
							var t = $('#tblDataArea').DataTable();
							t.clear().draw();
							
							$.ajax({
										type : "GET",
										url : myContextPath+"/fetchAreaList",
										/* data: "id=" + id + "&name=" + name, */
										beforeSend: function() {
											$('.preloader-background').show();
											$('.preloader-wrapper').show();
								           },

										success : function(data) {
											var srno = 1;
											//$("#tblDataCountry tbody").remove();

											//alert("data clean");

											var count = $("#tblDataArea").find("tr:first th").length;
											//alert(count);

											if (data == null) {
												    t.row.add('<tr>'
															+ '<td colspan="'+count+'"><center>No Data Found</center></td>'
															+ '</tr>');
											}
											for (var i = 0, len = data.length; i < len; ++i) 
											{
												var area = data[i];
												t.row.add([
																srno,
																area.name,
																area.cluster.name,
																"<button onclick='openAreaEdit("+area.areaId+")' class='btn-flat'><i class='material-icons tooltipped blue-text' data-position='right' data-delay='50' data-tooltip='Edit'>edit</i></button>"

														  ]).draw(false); 

												srno = srno + 1;
											}
											$('.preloader-wrapper').hide();
											$('.preloader-background').hide();
					    				},
										error: function(xhr, status, error) {
											$('.preloader-wrapper').hide();
											$('.preloader-background').hide();
											  //alert(error +"---"+ xhr+"---"+status);
											$('#addeditmsg').modal('open');
					               	     	$('#msgHead').text("Message : ");
					               	     	$('#msg').text("Something Went Wrong"); 
					               	     		setTimeout(function() 
												  {
					      	     					$('#addeditmsg').modal('close');
												  }, 1000);
											}
								});

						});
						
						$("#saveAreaSubmit").click(function() {
							
					
							var clusterId=$('#clusterSelectForArea').val();
							$('#errorForArea').html('');
							if(clusterId==="0")
							{
								$('#errorForArea').html("<font color='red'>Select Cluster</font>");
								return false;
							}
							else if($('#txtAddArea').val().trim()==="")
							{
								$('#errorForArea').html("<font color='red'>Enter Area Name</font>");
								return false;
							}
							
							var form = $('#saveAreaForm');

							$.ajax({

										type : form.attr('method'),
										url : form.attr('action'),
										beforeSend: function() 
										{
											$('.preloader-background').show();
											$('.preloader-wrapper').show();
								        },
										data : form.serialize(),
										success : function(data) {
											
												if(data=="Success")
												{
													$('#addArea').modal('close');
													
													$('#addeditmsg').modal('open');
							               	     	$('#msgHead').text("Message : ");
							               	     	$('#msg').html("<font color='green'>Area Saved SuccessFully</font>"); 
							               	     	
													$('#areaTab').click();
												}
												else if(data==="UpdateSuccess")
												{
													$('#addArea').modal('close');
													
													$('#addeditmsg').modal('open');
							               	     	$('#msgHead').text("Message : ");
							               	     	$('#msg').html("<font color='green'>Area Updated SuccessFully</font>"); 
							               	     	
													$('#areaTab').click();
												}
												else
												{
													$('#errorForArea').html("<font color='red'>"+data+"</font>");
												}
											
												$('.preloader-wrapper').hide();
												$('.preloader-background').hide();
						    				},
											error: function(xhr, status, error) {
												$('.preloader-wrapper').hide();
												$('.preloader-background').hide();
												  //alert(error +"---"+ xhr+"---"+status);
												$('#addeditmsg').modal('open');
						               	     	$('#msgHead').text("Message : ");
						               	     	$('#msg').text("Something Went Wrong"); 
						               	     		setTimeout(function() 
													  {
						      	     					$('#addeditmsg').modal('close');
													  }, 1000);
												}
									});
							return false;
						});
						
						
						$('#routeTab').click(function() {
							
							var t = $('#tblDataRoute').DataTable();
							t.clear().draw();
							
							$.ajax({
										type : "GET",
										url : myContextPath+"/fetchRouteList",
										/* data: "id=" + id + "&name=" + name, */
										beforeSend: function() {
											$('.preloader-background').show();
											$('.preloader-wrapper').show();
								           },

										success : function(data) {
											var srno = 1;
											//$("#tblDataCountry tbody").remove();

											//alert("data clean");

											var count = $("#tblDataRoute").find("tr:first th").length;
											//alert(count);

											if (data == null) {
												    t.row.add('<tr>'
															+ '<td colspan="'+count+'"><center>No Data Found</center></td>'
															+ '</tr>');
											}
											for (var i = 0, len = data.length; i < len; ++i) 
											{
												var route = data[i];
												t.row.add([
																srno,
																route.routeNo,
																route.area.name,
																route.area.cluster.name,
																"<button onclick='openRouteEdit("+route.routeId+")' class='btn-flat'><i class='material-icons tooltipped blue-text' data-position='right' data-delay='50' data-tooltip='Edit'>edit</i></button>"

														  ]).draw(false); 

												srno = srno + 1;
											}
											$('.preloader-wrapper').hide();
											$('.preloader-background').hide();
					    				},
										error: function(xhr, status, error) {
											$('.preloader-wrapper').hide();
											$('.preloader-background').hide();
											  //alert(error +"---"+ xhr+"---"+status);
											$('#addeditmsg').modal('open');
					               	     	$('#msgHead').text("Message : ");
					               	     	$('#msg').text("Something Went Wrong"); 
					               	     		setTimeout(function() 
												  {
					      	     					$('#addeditmsg').modal('close');
												  }, 1000);
											}
								});

						});
						
						$("#saveRouteSubmit").click(function() {
							
							var areaId=$('#areaSelectForRoute').val();
							var clusterId=$('#clusterSelectForRoute').val();
							$('#errorMsgForRoute').html('');
							if(clusterId==="0")
							{
								$('#errorMsgForRoute').html("<font color='red'>Select Cluster</font>");
								return false;
							}
							else if(areaId==="0")
							{
								$('#errorMsgForRoute').html("<font color='red'>Select Area</font>");
								return false;
							}
							else if($('#txtAddRoute').val().trim()==="")
							{
								$('#errorMsgForRoute').html("<font color='red'>Enter Route Number</font>");
								return false;
							}
							
							var form = $('#saveRouteForm');

							$.ajax({

										type : form.attr('method'),
										url : form.attr('action'),
										beforeSend: function() 
										{
											$('.preloader-background').show();
											$('.preloader-wrapper').show();
								        },
										data : form.serialize(),
										success : function(data) {
											
												if(data=="Success")
												{
													$('#addRoute').modal('close');
													
													$('#addeditmsg').modal('open');
							               	     	$('#msgHead').text("Message : ");
							               	     	$('#msg').html("<font color='green'>Route Saved SuccessFully</font>"); 
							               	     	
													$('#routeTab').click();
												}
												else if(data==="UpdateSuccess")
												{
													$('#addRoute').modal('close');
													
													$('#addeditmsg').modal('open');
							               	     	$('#msgHead').text("Message : ");
							               	     	$('#msg').html("<font color='green'>Route Updated SuccessFully</font>"); 
							               	     	
													$('#routeTab').click();
												}
												else
												{
													$('#errorMsgForRoute').html("<font color='red'>"+data+"</font>");
												}
											
												$('.preloader-wrapper').hide();
												$('.preloader-background').hide();
						    				},
											error: function(xhr, status, error) {
												$('.preloader-wrapper').hide();
												$('.preloader-background').hide();
												  //alert(error +"---"+ xhr+"---"+status);
												$('#addeditmsg').modal('open');
						               	     	$('#msgHead').text("Message : ");
						               	     	$('#msg').text("Something Went Wrong"); 
						               	     		setTimeout(function() 
													  {
						      	     					$('#addeditmsg').modal('close');
													  }, 1000);
												}
									});
							return false;
						});
						
						
						$('#clusterSelectForRoute').change(function(){
							
							var clusterId=$('#clusterSelectForRoute').val();
							// Get the raw DOM object for the select box
							var select = document.getElementById('areaSelectForRoute');

							// Clear the old options
							select.options.length = 0;
							//$('#countryListForState').html('');

							//Load the new options
							select.options.add(new Option("Choose Area", 0));
							
							$.ajax({
								type : "GET",
								url : myContextPath+"/fetchAreaByClusterId?clusterId="+clusterId,
								beforeSend: function() 
								{
									$('.preloader-background').show();
									$('.preloader-wrapper').show();
						        },
						        async:false,
								success : function(data) {		
															
									var options, index, option;
									select = document.getElementById('areaSelectForRoute');

									
									for (var i = 0, len = data.length; i < len; ++i) 
									{
										var area = data[i];
										select.options.add(new Option(area.name, area.areaId));
									}			
									$('.preloader-wrapper').hide();
									$('.preloader-background').hide();
								},
								error: function(xhr, status, error) {
									$('.preloader-wrapper').hide();
									$('.preloader-background').hide();
									  //alert(error +"---"+ xhr+"---"+status);
									$('#addeditmsg').modal('open');
								     	$('#msgHead').text("Message : ");
								     	$('#msg').text("Something Went Wrong"); 
								     		setTimeout(function() 
										  {
												$('#addeditmsg').modal('close');
										  }, 1000);
									
									}
								});
							
							
						});
						
								
						$('#routeTab').click();
					});

	
	function openClusterEdit(id) 
	{
		if(id==0)
		{
			$('#clusterIdForCluster').val('0');
			$('#txtAddCluster').val('');
			$('#txtAddCluster').change();
			$('#saveClusterSubmit').text('Add');
			$('#errorForCluster').html('');
			$('#addCluster').modal('open');
			return false;
		}
		
		$.ajax({
			type : "GET",
			url : myContextPath+"/fetchClusterByClusterId?clusterId="+id,
			beforeSend: function() 
			{
				$('.preloader-background').show();
				$('.preloader-wrapper').show();
	        },
			success : function(data) {				
				
				$('#clusterIdForCluster').val(data.clusterId);
				$('#txtAddCluster').val(data.name);
				$('#txtAddCluster').change();
				$('#saveClusterSubmit').text('Update');
				$('#errorForCluster').html('');
				$('#addCluster').modal('open');
				
				$('.preloader-wrapper').hide();
				$('.preloader-background').hide();
			
			},
			error: function(xhr, status, error) {
				$('.preloader-wrapper').hide();
				$('.preloader-background').hide();
				  //alert(error +"---"+ xhr+"---"+status);
				$('#addeditmsg').modal('open');
			     	$('#msgHead').text("Message : ");
			     	$('#msg').text("Something Went Wrong"); 
			     		setTimeout(function() 
					  {
							$('#addeditmsg').modal('close');
					  }, 1000);
				
				}
			});
			
	}
	
	
	function openAreaEdit(id) 
	{
		
		// Get the raw DOM object for the select box
		var select = document.getElementById('clusterSelectForArea');

		// Clear the old options
		select.options.length = 0;
		//$('#countryListForState').html('');

		//Load the new options
		select.options.add(new Option("Choose Cluster", 0));
		
		$.ajax({
			type : "GET",
			url : myContextPath+"/fetchClusterList",
			beforeSend: function() 
			{
				$('.preloader-background').show();
				$('.preloader-wrapper').show();
	        },
	        async:false,
			success : function(data) {		
										
				var options, index, option;
				select = document.getElementById('clusterSelectForArea');

				for (var i = 0, len = data.length; i < len; ++i) 
				{
					var cluster = data[i];
					select.options.add(new Option(cluster.name, cluster.clusterId));
				}
				
				$('.preloader-wrapper').hide();
				$('.preloader-background').hide();					
				
			},
			error: function(xhr, status, error) {
				$('.preloader-wrapper').hide();
				$('.preloader-background').hide();
				  //alert(error +"---"+ xhr+"---"+status);
				$('#addeditmsg').modal('open');
			     	$('#msgHead').text("Message : ");
			     	$('#msg').text("Something Went Wrong"); 
			     		setTimeout(function() 
					  {
							$('#addeditmsg').modal('close');
					  }, 1000);
				
				}
			});
		
		
		if(id==0)
		{
			$('#addAreaId').val('0');
			$('#txtAddArea').val('');
			$('#txtAddArea').change();
			$('#saveAreaSubmit').text('Add');
			$('#errorForArea').html('');
			var cluster = $("#clusterSelectForArea");
			cluster.val(0);
			cluster.change();
			$('#addArea').modal('open');
			return false;
		}
		
		$.ajax({
			type : "GET",
			url : myContextPath+"/fetchAreaByAreaId?areaId="+id,
			beforeSend: function() 
			{
				$('.preloader-background').show();
				$('.preloader-wrapper').show();
	        },
			success : function(data) {				
				
				$('#addAreaId').val(data.areaId);
				$('#txtAddArea').val(data.name);
				
				var cluster = $("#clusterSelectForArea");
				cluster.val(data.cluster.clusterId);
				cluster.change();				
				
				$('#txtAddArea').change();
				$('#saveAreaSubmit').text('Update');
				$('#errorForArea').html('');
				$('#addArea').modal('open');
				
				$('.preloader-wrapper').hide();
				$('.preloader-background').hide();
			
			},
			error: function(xhr, status, error) {
				$('.preloader-wrapper').hide();
				$('.preloader-background').hide();
				  //alert(error +"---"+ xhr+"---"+status);
				$('#addeditmsg').modal('open');
			     	$('#msgHead').text("Message : ");
			     	$('#msg').text("Something Went Wrong"); 
			     		setTimeout(function() 
					  {
							$('#addeditmsg').modal('close');
					  }, 1000);
				
				}
			});
			
	}
	
	function openRouteEdit(id) 
	{
		
		// Get the raw DOM object for the select box
		var select = document.getElementById('clusterSelectForRoute');

		// Clear the old options
		select.options.length = 0;
		//$('#countryListForState').html('');

		//Load the new options
		select.options.add(new Option("Choose Cluster", 0));
		
		$.ajax({
			type : "GET",
			url : myContextPath+"/fetchClusterList",
			beforeSend: function() 
			{
				$('.preloader-background').show();
				$('.preloader-wrapper').show();
	        },
	        async:false,
			success : function(data) {		
										
				var options, index, option;
				select = document.getElementById('clusterSelectForRoute');

				
				for (var i = 0, len = data.length; i < len; ++i) 
				{
					var cluster = data[i];
					select.options.add(new Option(cluster.name, cluster.clusterId));
				}			
				$('.preloader-wrapper').hide();
				$('.preloader-background').hide();
			},
			error: function(xhr, status, error) {
				$('.preloader-wrapper').hide();
				$('.preloader-background').hide();
				  //alert(error +"---"+ xhr+"---"+status);
				$('#addeditmsg').modal('open');
			     	$('#msgHead').text("Message : ");
			     	$('#msg').text("Something Went Wrong"); 
			     		setTimeout(function() 
					  {
							$('#addeditmsg').modal('close');
					  }, 1000);
				
				}
			});
		
		
		if(id==0)
		{
			$('#addAreaId').val('0');
			$('#txtAddArea').val('');
			$('#txtAddArea').change();
			$('#saveAreaSubmit').text('Add');
			$('#errorForArea').html('');
			var cluster = $("#clusterSelectForArea");
			cluster.val(0);
			cluster.change();			
			$('#addRoute').modal('open');	
			return false;
		}
		
		$.ajax({
			type : "GET",
			url : myContextPath+"/fetchRouteByRouteId?routeId="+id,
			beforeSend: function() 
			{
				$('.preloader-background').show();
				$('.preloader-wrapper').show();
	        },
			success : function(data) {				
				
				$('#addRouteId').val(data.routeId);
				$('#txtAddRoute').val(data.routeNo);
				
				var cluster = $("#clusterSelectForRoute");
				cluster.val(data.area.cluster.clusterId);
				cluster.change();
				
				setTimeout(function()
				{
					var cluster = $("#areaSelectForRoute");
					cluster.val(data.area.areaId);
					cluster.change();
					
				},1000);
				
				$('#txtAddRoute').change();
				$('#saveRouteSubmit').text('Update');
				$('#errorForRoute').html('');
				$('#addRoute').modal('open');
				
				$('.preloader-wrapper').hide();
				$('.preloader-background').hide();
			
			},
			error: function(xhr, status, error) {
				$('.preloader-wrapper').hide();
				$('.preloader-background').hide();
				  //alert(error +"---"+ xhr+"---"+status);
				$('#addeditmsg').modal('open');
			     	$('#msgHead').text("Message : ");
			     	$('#msg').text("Something Went Wrong"); 
			     		setTimeout(function() 
					  {
							$('#addeditmsg').modal('close');
					  }, 1000);
				
				}
			});
			
	}