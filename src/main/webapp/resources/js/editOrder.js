    
var orderSupplierList = new Hashtable();
var count=1;
$(document).ready(function() 
{    
    $('#addOrderProduct').click(function(){
		
		var buttonStatus=$('#addOrderProduct').text();
    	if(buttonStatus==="Update")
    	{
    		updaterow($('#currentUpdateRowId').val());
    		return false;
    	}
		
		var productIdForOrder=$('#productIdForOrder').val();
		var supplierIdForOrder=$('#supplierIdForOrder').val();
		var orderQuantity=$('#orderQuantity').val();
		var supplierMobileNumber=$('#supplierMobileNumber').val();
		//alert("'"+supplierIdForOrder+"' - '"+productIdForOrder+"'");
		for (let [key, value] of orderSupplierList.entries()) {
  			//alert("supplierId : "+value[0]+" - ProductId : "+value[1]+" - quantity : "+value[2]+" - mobile Number : "+value[3]);
  			if(value[0]===supplierIdForOrder && value[1] === productIdForOrder)
  			{
  				//alert("dfd");
  				$('#addeditmsg').modal('open');
       	     	$('#msgHead').text("Product Select Warning");
       	     	$('#msg').text("This Product and Supplier is already added to Cart"); 
       	     	return false;
  			}
  		}
		//alert(orderSupplierList.entries());
		//orderProductAndQuantityList.put(productIdForOrder,orderQuantity);
		var prodctlst = [supplierIdForOrder,productIdForOrder, orderQuantity,supplierMobileNumber];
		orderSupplierList.put(count,prodctlst);
		$('#orderentriesId').val(orderSupplierList.entries());
		//alert(orderSupplierList.entries());
		var supplierName=$('#supplierIdForOrder option:selected').text();
		var productName=$('#productIdForOrder option:selected').text();
		var supplierRate=$('#supplierRateForOrder').val();
		
		$("#orderCartTb").append(	"<tr id='rowdel_" + count + "' >"+
		                            "<td id='rowcount_" + count + "'>"+count+"</td>"+
		                            "<td id='rowsuppliername_"+count+"'><input type='hidden' id='rowsupplierid_"+count+"' value="+supplierIdForOrder+"><input type='hidden' id='rowsuppliermob_"+count+"' value="+supplierMobileNumber+"><span id='rowsuppliernametb_"+count+"'>"+supplierName+"</span></td>"+
		                            "<td id='rowproductname_"+count+"'><input type='hidden' id='rowproductid_"+count+"' value="+productIdForOrder+"><span id='rowproductnametb_"+count+"'>"+productName+"</span></td>"+
		                            "<td id='rowsupplierrate_"+count+"'>"+supplierRate+"</td>"+
		                            "<td id='roworderquantity_"+count+"'>"+orderQuantity+"</td>"+
		                            "<td id='roweditbutton_" + count + "'><button type='button'  onclick='editrow(" + count + ")' class='btn-flat'><i class='material-icons'>edit</i></button></td>"+
		                            "<td id='rowdelbutton_" + count + "'><button type='button' onclick='deleterow(" + count + ")' class='btn-flat'><i class='material-icons'>cancel</i></button></td>"+
		                        	"</tr>");
		
		count++;
		//alert(orderSupplierList.entries());
	}); 
    
    $('#orderProductsButton').click(function(){
		//alert(orderSupplierList.entries());
		var productIdList="";
    	for (let [key, value] of orderSupplierList.entries()) {
    		//alert(value[0]+"-"+value[1]+"-"+value[2]+"-"+value[3]);
    		productIdList=productIdList+value[0]+"-"+value[1]+"-"+value[2]+"-"+value[3]+",";
  		}
    	productIdList=productIdList.slice(0,-1)
    	//alert(productIdList);
    	$('#productWithSupplikerlist').val(productIdList);
    	
    	var form = $('#editOrderBookForm');

		$.ajax({
					type : form.attr('method'),
					url : form.attr('action'),
					data : $("#editOrderBookForm").serialize(),
					success : function(data) {
						if(data=="Success")
						{
								$('#add').modal('close');
								$('#addeditmsg').modal('open');
								$('#msgHead').text("Success : ");
								$('#msg').html("<font color='green'>Order Book SuccessFully</font>");
								fetchProductList();
						}
						else
						{
								$('#addeditmsg').modal('open');
								$('#msgHead').text("Failed : ");
								$('#msg').html("<font color='red'>Order Book Failed</font>");
						}
					}
		});
		
	});
    
    $('#supplierIdForOrder').change(function(){
		var supplierId=$('#supplierIdForOrder').val();
		$('#productIdForOrder').empty();
		$("#productIdForOrder").append('<option value="0">Choose Product</option>');
		
		$.ajax({
			url : myContextPath+"/fetchProductBySupplierId?supplierId="+supplierId,
			dataType : "json",
			success : function(data) {
				
				for(var i=0; i<data.length; i++)
				{								
					//alert(data[i].productId +"-"+ data[i].productName);
					$("#productIdForOrder").append('<option value='+data[i].productId+'>'+data[i].productName+'</option>');										
				}	
			},
			error: function(xhr, status, error) {
				  //var err = eval("(" + xhr.responseText + ")");
				  //alert("Error");
				}
		});
		
		
		var supplierId=$('#supplierIdForOrder').val();
		$('#supplierMobileNumber').val('');
		$('#supplierMobileNumber').change();
		$.ajax({
			url : myContextPath+"/fetchSupplierBySupplierId?supplierId="+supplierId,
			dataType : "json",
			success : function(data) {
				
				$('#supplierMobileNumber').val(data.contact.mobileNumber);
				$('#supplierMobileNumber').change();
				
			},
			error: function(xhr, status, error) {
				  //var err = eval("(" + xhr.responseText + ")");
				  //alert("Error");
				}
		});
	});
    
    $('#categoryIdForOrder').change(function(){
		
		var categoryId=$('#categoryIdForOrder').val();
		var brandId=$('#brandIdForOrder').val();
		var supplierId=$('#supplierIdForOrder').val();
		
		$('#productIdForOrder').empty();
		$("#productIdForOrder").append('<option value="0">Choose Product</option>');
		$.ajax({ 
			url : myContextPath+"/fetchProductBySupplierIdAndCategoryIdAndBrandId?categoryId="+categoryId+"&brandId="+brandId+"&supplierId="+supplierId,
			dataType : "json",
			async :false,
			success : function(data) {
				
					for(var i=0; i<data.length; i++)
					{								
						//alert(data[i].productId +"-"+ data[i].productName);
						$("#productIdForOrder").append('<option value='+data[i].productId+'>'+data[i].productName+'</option>');										
					}	
					//alert("done");
					$("#productIdForOrder").change();
				
				},
				error: function(xhr, status, error) {
					  //var err = eval("(" + xhr.responseText + ")");
					//alert("Error ");
				}
			});
		
	});
    
    $('#brandIdForOrder').change(function(){
		
		var categoryId=$('#categoryIdForOrder').val();
		var brandId=$('#brandIdForOrder').val();
		var supplierId=$('#supplierIdForOrder').val();
		
		$('#productIdForOrder').empty();
		$("#productIdForOrder").append('<option value="0">Choose Product</option>');
		$.ajax({ 
			url : myContextPath+"/fetchProductBySupplierIdAndCategoryIdAndBrandId?categoryId="+categoryId+"&brandId="+brandId+"&supplierId="+supplierId,
			dataType : "json",
			async :false,
			success : function(data) {
				
				for(var i=0; i<data.length; i++)
				{								
					//alert(data[i].productId +"-"+ data[i].productName);
					$("#productIdForOrder").append('<option value='+data[i].productId+'>'+data[i].productName+'</option>');										
				}	
				//alert("done");
				$("#productIdForOrder").change();
				
				},
				error: function(xhr, status, error) {
					  //var err = eval("(" + xhr.responseText + ")");
					//alert("Error ");
				}
			});
		
	});
    
    $('#productIdForOrder').change(function(){
		var productId=$('#productIdForOrder').val();
		var supplierId=$('#supplierIdForOrder').val();
		$.ajax({
			url : myContextPath+"/fetchSupplierById?supplierId="+supplierId+"&productId="+productId,
			dataType : "json",
			success : function(data) {
				
				$('#supplierRateForOrder').val(data);
				$('#supplierRateForOrder').change();
			},
			error: function(xhr, status, error) {
				  //var err = eval("(" + xhr.responseText + ")");
				  //alert("Error ");
				}
		});
		
	});
    
    $("#OrderMobileNumberchecker").change(function() {
	    var ischecked= $(this).is(':checked');
	    if(ischecked)
	    {
	    	$('#supplierMobileNumber').attr('readonly', true);
	    }
	    else
	    {
	    	$('#supplierMobileNumber').attr('readonly', false);
	    }
	});
	$('#OrderMobileNumberchecker').attr('checked', true);
	$('#supplierMobileNumber').attr('readonly', true);
});
    function editOrder(supplierOrderIddd)
    {
    	$('#supplierOrderIdId').val(supplierOrderIddd);
    	$('#supplierMobileNumber').val('');
    	$('#supplierRateForOrder').val('');
    	$('#orderQuantity').val('');
    	$('#OrderMobileNumberchecker').attr('checked', true);
    	$('#supplierMobileNumber').attr('readonly', true); 
    	
    	$.ajax({
    		url : myContextPath+"/fetchBrandAndCategoryList",
    		dataType : "json",
    		async:false,
    		success : function(data) {
    			categoryList=data.categoryList;
    			$('#categoryIdForOrder').empty();
    			$("#categoryIdForOrder").append('<option value="0">Choose Category</option>');
    			for(var i=0; i<categoryList.length; i++)
    			{
    				$("#categoryIdForOrder").append('<option value='+categoryList[i].categoryId+'>'+categoryList[i].categoryName+'</option>');
    			}	
    			$("#categoryIdForOrder").change();
    			/*if(selectProduct==true){
    				var source=$('#categoryIdForOrder');
    				source.val(v);
    				source.change();
    			}*/
    			
    			brandList=data.brandList;
    			$('#brandIdForOrder').empty();
    			$("#brandIdForOrder").append('<option value="0">Choose Brand</option>');
    			for(var j=0; j<brandList.length; j++)
    			{
    				$("#brandIdForOrder").append('<option value='+brandList[j].brandId+'>'+brandList[j].name+'</option>');
    			}	
    			$("#brandIdForOrder").change();
    			
    			
    			supplierList=data.supplierList;
    			$('#supplierIdForOrder').empty();
    			$("#supplierIdForOrder").append('<option value="0">Choose Supplier</option>');
    			for(var k=0; k<supplierList.length; k++)
    			{
    				$("#supplierIdForOrder").append('<option value='+supplierList[k].supplierId+'>'+supplierList[k].name+'</option>');
    			}	
    			$("#supplierIdForOrder").change();
    			
    	
    		}
    	});
    	
    	
    	if(supplierOrderIddd!=null && supplierOrderIddd!=undefined )
		{
			$.ajax({
				url : myContextPath+"/editSupplierOrderAjax?supplierOrderId="+supplierOrderIddd,
				dataType : "json",
				async :false,
				success : function(data) {
					
					count=1;
					$("#orderCartTb").html('');
					for(var i=0; i<data.length; i++)
					{
						var supplierOrderDetaillist=data[i];
						var supplierIdForOrder=supplierOrderDetaillist.supplier.supplierId;
						var supplierMobileNumber=supplierOrderDetaillist.supplier.contact.mobileNumber;
						var supplierName=supplierOrderDetaillist.supplier.name;
						var productIdForOrder=supplierOrderDetaillist.product.productId;
						var productName=supplierOrderDetaillist.product.productName;
						var supplierRate=supplierOrderDetaillist.supplierRate;
						var orderQuantity=supplierOrderDetaillist.quantity;
						
						var prodctlst = [supplierIdForOrder,productIdForOrder.toString(), orderQuantity.toString(),supplierMobileNumber];
						orderSupplierList.put(count,prodctlst);
						
						$("#orderCartTb").append("<tr id='rowdel_" + count + "' >"+
					                             "<td id='rowcount_" + count + "'>"+count+"</td>"+
					                             "<td id='rowsuppliername_"+count+"'><input type='hidden' id='rowsupplierid_"+count+"' value="+supplierIdForOrder+"><input type='hidden' id='rowsuppliermob_"+count+"' value="+supplierMobileNumber+"><span id='rowsuppliernametb_"+count+"'>"+supplierName+"</span></td>"+
					                             "<td id='rowproductname_"+count+"'><input type='hidden' id='rowproductid_"+count+"' value="+productIdForOrder+"><span id='rowproductnametb_"+count+"'>"+productName+"</span></td>"+
					                             "<td id='rowsupplierrate_"+count+"'>"+supplierRate+"</td>"+
					                             "<td id='roworderquantity_"+count+"'>"+orderQuantity+"</td>"+
					                             "<td id='roweditbutton_" + count + "'><button type='button'  onclick='editrow(" + count + ")' class='btn-flat'><i class='material-icons'>edit</i></button></td>"+
					                             "<td id='rowdelbutton_" + count + "'><button type='button' onclick='deleterow(" + count + ")' class='btn-flat'><i class='material-icons'>cancel</i></button></td>"+
					                        	 "</tr>");
						count++;
					}
					
				},
				error: function(xhr, status, error) {
					 // alert("Error");
				}
			});
		}
		//alert(orderSupplierList.entries());
		//alert(count);
		$('.modal').modal();
		$('#order').modal('open');
    }
    
    function showOrderDetails(id)
	   {
    	$('#orderentriesId').val(orderSupplierList.entries());
		   $.ajax({
				url : myContextPath+"/fetchProductBySupplierOrderId?supplierOrderId="+id,
				dataType : "json",
				success : function(data) {
				//alert(data);
				var totalAll=0,totalAmtWithTaxAll=0;
				    $("#productOrderedDATA").empty();
					var srno=1;
					for (var i = 0, len = data.length; i < len; ++i) {
						var supplierOrderDetail = data[i];
						
						/* 
						<tr>
                            <td>1</td>
                            <td>Mouse</td>
                            <td>I.T</td>
                            <td>BlueSquare</td>
                            <td>5</td>
                            <td>350</td>
                            <th>Total Amount</th>
                            <th>Total Amount With Tax</th>
                            </tr>
						*/
						var igst=supplierOrderDetail.product.categories.igst;
						var cgst=supplierOrderDetail.product.categories.cgst;
						var sgst=supplierOrderDetail.product.categories.sgst;
						
						var total=parseFloat(supplierOrderDetail.quantity)*parseFloat(supplierOrderDetail.supplierRate);
						var totalAmtWithTax=parseFloat(total)+
												( (parseFloat(total)*parseFloat(igst))/100 )+
												( (parseFloat(total)*parseFloat(cgst))/100 )+
												( (parseFloat(total)*parseFloat(sgst))/100 );
						$("#productOrderedDATA").append("<tr>"+
	                           "<td>"+srno+"</td>"+
	                           "<td>"+supplierOrderDetail.product.productName+"</td>"+
	                           "<td>"+supplierOrderDetail.product.categories.categoryName+"</td>"+
	                           "<td>"+supplierOrderDetail.product.brand.name+"</td>"+
	                           "<td>"+supplierOrderDetail.quantity+"</td>"+
	                           "<td>"+supplierOrderDetail.supplierRate.toFixed(2)+"</td>"+
	                           "<td>"+total.toFixed(2)+"</td>"+
	                           "<td>"+totalAmtWithTax.toFixed(2)+"</td>"+
	                       "</tr>"); 
						srno++;
						totalAmtWithTaxAll=parseFloat(totalAmtWithTaxAll)+totalAmtWithTax;
						totalAll=parseFloat(totalAll)+total;
					}
					
					$("#productOrderedDATA").append("<tr>"+	     
													   "<td colspan='6'><b>All Total</b></td>"+
							                           "<td  class='red-text'><b>"+totalAll.toFixed(2)+"</b></td>"+
							                           "<td  class='red-text'><b>"+totalAmtWithTaxAll.toFixed(2)+"</b></td>"+
							                       "</tr>");
					
					$('.modal').modal();
					$('#product').modal('open');
					
				},
				error: function(xhr, status, error) {
					  	//alert("error");
					}
			});
	   }
    function editrow(id) {
    	$('#orderentriesId').val(orderSupplierList.entries());
    	
    	$('#currentUpdateRowId').val(id);
    	var supplierId=$('#rowsupplierid_'+id).val();
    	var productId=$('#rowproductid_'+id).val();
    	//alert($('#rowproductrate_'+id).val());
    			
    	var source1 = $("#supplierIdForOrder");
    	var v1=supplierId;
    	source1.val(v1);
    	source1.change();
    	
    	setTimeout(
    			  function() 
    			  {
    				  var source = $("#productIdForOrder");
    					var v=productId;
    					source.val(v);
    					source.change();
    			  }, 1000);
    	
    	
    	$('#supplierMobileNumber').val($('#rowsuppliermob_'+id).text());
    	$('#orderQuantity').val($('#roworderquantity_'+id).text());
    	$('#supplierRateForOrder').val($('#rowsupplierrate_'+id).text());
    	
    	$("#addOrderProduct").text("Update");	
    }

    function updaterow(id) {
    	$('#orderentriesId').val(orderSupplierList.entries());
    	$("#addOrderProduct").text("Add");	
    	
    	var productIdForOrder=$('#productIdForOrder').val();
    	var supplierIdForOrder=$('#supplierIdForOrder').val();
    	var orderQuantity=$('#orderQuantity').val();
    	var supplierMobileNumber=$('#supplierMobileNumber').val();
    	
    	/*for (let [key, value] of orderSupplierList.entries()) {
    			//alert("supplierId : "+key+" - ProductId : "+value[0]+" - quantity : "+value[1]+" - mobile Number : "+value[2]);
    			if(key===supplierIdForOrder && value[0] === productIdForOrder)
    			{
    				$('#addeditmsg').modal('open');
       	     	$('#msgHead').text("Product Select Warning");
       	     	$('#msg').text("This Product and Supplier is already added to Cart"); 
       	     	return false;
    			}
    		}*/
    	
    	//orderProductAndQuantityList.put(productIdForOrder,orderQuantity);
    	orderSupplierList.remove(id);
    	var prodctlst = [supplierIdForOrder,productIdForOrder, orderQuantity,supplierMobileNumber];
    	orderSupplierList.put(id,prodctlst);
    	/*alert("length "+orderSupplierList.size());*/
    	var supplierName=$('#supplierIdForOrder option:selected').text();
    	var productName=$('#productIdForOrder option:selected').text();
    	var supplierRate=$('#supplierRateForOrder').val();
    	
    	var rowCount = $('#orderCartTb tr').length;
    	var trData="";
    	count=1;
    	for(var i=1; i<=rowCount; i++)
    	{
    		//alert($('#rowcount_'+i).html() +"---"+ $('#rowprocustname_'+i).html() +"---"+ $('#rowdelbutton_'+i).html());
    		
    		if(id!=i)
    		{
    			//alert("predata");
    			//alert(i+"-(----)-"+$('#tbproductname_' + i).text());
        		 /* trData=trData+"<tr id='rowdel_" + count + "' >"+
           				"<td id='rowcount_" + count + "'>" + count + "</td>"+
           				"<td id='rowproductname_" + count + "'><input type='hidden' id='rowproductkey_" + count + "' value='"+$('#rowproductkey_' + i).val()+"'><center><span id='tbproductname_" + count + "'>"+$('#tbproductname_' + i).text()+"</span></center></td>"+
           				"<td id='rowdelbutton_" + count + "'><button class='btn-flat' type='button' onclick='deleterow(" + count + ")'><i class='material-icons '>clear</i></button></td>"+
           				"</tr>"; */
    			
           				trData=trData+"<tr id='rowdel_" + count + "' >"+
                                "<td id='rowcount_" + count + "'>"+count+"</td>"+
                                "<td id='rowsuppliername_"+count+"'><input type='hidden' id='rowsupplierid_"+count+"' value="+$('#rowsupplierid_' + i).val()+"><input type='hidden' id='rowsuppliermob_"+count+"' value="+$('#rowsuppliermob_' + i).val()+"><span id='rowsuppliernametb_"+count+"'>"+$('#rowsuppliernametb_' + i).text()+"</span></td>"+
                                "<td id='rowproductname_"+count+"'><input type='hidden' id='rowproductid_"+count+"' value="+$('#rowproductid_' + i).val()+"><span id='rowproductnametb_"+count+"'>"+$('#rowproductnametb_' + i).text()+"</span></td>"+
                                "<td id='rowsupplierrate_"+count+"'>"+$('#rowsupplierrate_' + i).text()+"</td>"+
                                "<td id='roworderquantity_"+count+"'>"+$('#roworderquantity_' + i).text()+"</td>"+
                                "<td id='roweditbutton_" + count + "'><button type='button'  onclick='editrow(" + count + ")' class='btn-flat'><i class='material-icons'>edit</i></button></td>"+
                                "<td id='rowdelbutton_" + count + "'><button type='button' onclick='deleterow(" + count + ")' class='btn-flat'><i class='material-icons'>cancel</i></button></td>"+
                            	"</tr>";
        		 count++;
    		}
    		else
    		{
    			trData=trData+"<tr id='rowdel_" + count + "' >"+
    				            "<td id='rowcount_" + count + "'>"+count+"</td>"+
    				            "<td id='rowsuppliername_"+count+"'><input type='hidden' id='rowsupplierid_"+count+"' value="+supplierIdForOrder+"><input type='hidden' id='rowsuppliermob_"+count+"' value="+supplierMobileNumber+"><span id='rowsuppliernametb_"+count+"'>"+supplierName+"</span></td>"+
    				            "<td id='rowproductname_"+count+"'><input type='hidden' id='rowproductid_"+count+"' value="+productIdForOrder+"><span id='rowproductnametb_"+count+"'>"+productName+"</span></td>"+
    				            "<td id='rowsupplierrate_"+count+"'>"+supplierRate+"</td>"+
    				            "<td id='roworderquantity_"+count+"'>"+orderQuantity+"</td>"+
    				            "<td id='roweditbutton_" + count + "'><button type='button'  onclick='editrow(" + count + ")' class='btn-flat'><i class='material-icons'>edit</i></button></td>"+
    				            "<td id='rowdelbutton_" + count + "'><button type='button' onclick='deleterow(" + count + ")' class='btn-flat'><i class='material-icons'>cancel</i></button></td>"+
    				        	"</tr>";
        		 count++;
    		}
    		//alert(trData);
    	} 
    	$("#orderCartTb").html('');
    	$("#orderCartTb").html(trData);
    	
    	//alert(productList.entries());
        //$('#rowdel_' + id).remove();
       // alert('productidlist '+productidlist);

    }

    function deleterow(id) {
    	
    	var supplierIdForOrder=$('#supplierIdForOrder').val();
        orderSupplierList.remove(id);
        alert(orderSupplierList.entries());
        var rowCount = $('#orderCartTb tr').length;
    	var trData="";
    	count=1;
    	$('#orderentriesId').val(orderSupplierList.entries());
    	for(var i=1; i<=rowCount; i++)
    	{
    		//alert($('#rowcount_'+i).html() +"---"+ $('#rowprocustname_'+i).html() +"---"+ $('#rowdelbutton_'+i).html());
    		
    		if(id!==i)
    		{
    			
           				trData=trData+"<tr id='rowdel_" + count + "' >"+
    				                    "<td id='rowcount_" + count + "'>"+count+"</td>"+
    				                    "<td id='rowsuppliername_"+count+"'><input type='hidden' id='rowsupplierid_"+count+"' value="+$('#rowsupplierid_' + i).val()+"><input type='hidden' id='rowsuppliermob_"+count+"' value="+$('#rowsuppliermob_' + i).val()+"><span id='rowsuppliernametb_"+count+"'>"+$('#rowsuppliernametb_' + i).text()+"</span></td>"+
    				                    "<td id='rowproductname_"+count+"'><input type='hidden' id='rowproductid_"+count+"' value="+$('#rowproductid_' + i).val()+"><span id='rowproductnametb_"+count+"'>"+$('#rowproductnametb_' + i).text()+"</span></td>"+
    				                    "<td id='rowsupplierrate_"+count+"'>"+$('#rowsupplierrate_' + i).text()+"</td>"+
    				                    "<td id='roworderquantity_"+count+"'>"+$('#roworderquantity_' + i).text()+"</td>"+
    				                    "<td id='roweditbutton_" + count + "'><button type='button'  onclick='editrow(" + count + ")' class='btn-flat'><i class='material-icons'>edit</i></button></td>"+
    				                    "<td id='rowdelbutton_" + count + "'><button type='button' onclick='deleterow(" + count + ")' class='btn-flat'><i class='material-icons'>cancel</i></button></td>"+
    				                	"</tr>";
        		 count++;
    		}
    		//alert(trData);
    	} 
    	$("#orderCartTb").html('');
    	$("#orderCartTb").html(trData);
    	//alert(productList.entries()); 
        //$('#rowdel_' + id).remove();
       // alert('productidlist '+productidlist);

    }