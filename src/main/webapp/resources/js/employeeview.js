var holidays;
var no=0;
var holidaycheckornot=false;
var checkedId=[];
var dateAdded;
$(document).ready(function() {
						var oneDate,fromDate,toDate;
						/*$('#oneday').click(function(){
							//alert("oneday");
							oneday=true;
							oneDate=$('#oneDate').val();
							fromDate=$('#fromDate').val();
							toDate=$('#toDate').val();
							alert(oneDate+" <> "+fromDate+" <> "+toDate);
						});
						$('#manydays').click(function(){
							//alert("manydays");
							oneday=false;
							
						});*/
						$('#oneDateDiv').hide();
						$('#fromDateDiv').hide();
						$('#toDateDiv').hide();
						
						$("#holiodaydetailsnoofholidays").on("keyup change",function(){
							$('#oneDateDiv').hide();
							$('#fromDateDiv').hide();
							$('#toDateDiv').hide();
							holidays=0;
							no=$("#holiodaydetailsnoofholidays").val();
							if(no>1){
								$('#fromDateDiv').show();
								$('#toDateDiv').show();
								holidays=no;
							}
							else if(no==1){
								$('#oneDateDiv').show();
								holidays=1;
							}
							else{
								$('#oneDateDiv').hide();
								$('#fromDateDiv').hide();
								$('#toDateDiv').hide();
								holidays=0;
							}
						})
							
						$('#holiodaydetailsnoofholidays').keypress(function( event ){
						    var key = event.which;
						    
						    if( ! ( key >= 48 && key <= 57 || key === 13) )
						        event.preventDefault();
						});
						
						$('#submitHoliday').click(function(){
							
							holidaycheckornot=false;
							var toDate=new Date($('#toDate').val()).setHours(0,0,0,0);
							var fromDate=new Date($('#fromDate').val()).setHours(0,0,0,0);
							var today=new Date().setHours(0,0,0,0);
							var oneDate=new Date($('#oneDate').val()).setHours(0,0,0,0);
							var dateAddedInDate=new Date(parseInt(dateAdded)).setHours(0,0,0,0);
							var reason=$('#reason').val();
							
							
							if(no<=0){
								$('#msgholidaysave').html("Select Holiday Dates");
							}
							
							var start=$('#fromDate').val();
							var end=$('#toDate').val();
							var one=$('#oneDate').val();
							var empId=$('#employeeIdForHolidaySave').val();
							var noOfDays=$('#holiodaydetailsnoofholidays').val();
							
							var employeeHolidaysId=$('#employeeHolidaysId').val();
							
							if(noOfDays==="")
							{
								$('#msgholidaysave').html("Enter No Of Days To Select Holiday Dates");
								
								return false;
							}
							else if(parseInt(noOfDays)==1)
							{
								start=one;
								end=one;
								
								//oneDate check
								if(one.trim()==="")
								{
									$('#msgholidaysave').html("Select FromDate Reason");
									return false;
								}								
								else if(oneDate < dateAddedInDate)
								{
									$('#msgholidaysave').html("Select One Date must be after Employee Added Date");
									return false;
								}
								else if(oneDate == today)
								{
									$('#msgholidaysave').html("");									
								}
								else
								{
									$('#msgholidaysave').html("");
									
								}
								
							}
							else if(parseInt(noOfDays)>1)
							{
								start=$('#fromDate').val();
								end=$('#toDate').val();
								
								//fromDate check
								if(start.trim()==="")
								{
									$('#msgholidaysave').html("Select FromDate Reason");
									return false;
								}								
								else if(fromDate < dateAddedInDate)
								{
									$('#msgholidaysave').html("Select From Date must be after Employee Added Date");
									return false;
								}
								else if(fromDate == today)
								{
									$('#msgholidaysave').html("");
									
								}
								else if(toDate <= fromDate)
								{
									$('#msgholidaysave').html("Select From Date in Before To Date");
									return false;
								}
								else
								{
									$('#msgholidaysave').html("");
									
								}
								
								//toDate check
								if(end.trim()==="")
								{
									$('#msgholidaysave').html("Select ToDate Reason");
									return false;
								}								
								else if(toDate < dateAddedInDate)
								{
									$('#msgholidaysave').html("Select To Date must be after Employee Added Date");
									return false;
								}
								else if(toDate <= fromDate)
								{
									$('#msgholidaysave').html("Select To Date in After From Date");
									return false;
								}
								else if(toDate == today)
								{
									$('#msgholidaysave').html("");
									
								}
								else
								{
									$('#msgholidaysave').html("");
									
								}
							}	
							else
							{
								$('#msgholidaysave').html("Enter above Zero('0') No Of Days To Select Holiday Dates");
								return false;
							}
							
							if(reason.trim()==="")
							{
								$('#msgholidaysave').html("Fill Reason");
								return false;
							}
							
							if(employeeHolidaysId!=="0")
							{
								$.ajax({
									type : "GET",
									url : myContextPath+"/checkUpdatingHoliday?employeeDetailsId="+empId+"&startDate="+start+"&endDate="+end+"&employeeHolidayId="+employeeHolidaysId,
									dataType : "json",
									data : $("#searchForm").serialize(),
									async: false,
									success : function(data) {
										//alert(data.status);
										$('#msgholidaysave').html(data.status);
										if(data.status!=="")
										{
											//alert("not submit");
											holidaycheckornot=false;
											return false;
										}
										holidaycheckornot=true;
										//alert(holidaycheckornot);
									},
									error: function(xhr, status, error) {
										  //var err = eval("(" + xhr.responseText + ")");
										  alert("Error");
										}
								});
								
								var paidStatus=$('#paidStatusId').val();
								if(paidStatus==="0")
								{
									$('#msgholidaysave').html("Select Paid Status");
									return false;
								}

								
								if(holidaycheckornot===true && $('#msgholidaysave').html()==="")
								{
									var form = $('#saveHoliday');
									//alert($('#saveHoliday').attr('action'));
									//alert("going submit");
									//alert(form.serialize()+"&paidStatus="+$('#paidStatusId').val());
									$.ajax({
											type : form.attr('method'),
											url : myContextPath+"/updateHoildays",
											//dataType : "json",
											data : form.serialize()+"&paidStatus="+$('#paidStatusId').val(),
											//async: false,
											success : function(data) {
												$('.modal').modal();
												if(data==="Success")
												{
													$('#addeditmsg').modal('open');
													$('#head').html("Holiday Booking Message ");
													$('#msg').html("<font color='green'>Holiday Update SuccessFully</font>");
															 setTimeout(
															  function() 
															  {
																  location.reload();
															  }, 2000);
													
												}
												else
												{
													$('#addeditmsg').modal('open');
													$('#head').html("Holiday Booking Message ");
													$('#msg').html("<font color='green'>Holiday Updating Failed</font>");
												}
										},
										error: function(xhr, status, error) {
											  //var err = eval("(" + xhr.responseText + ")");
											  //alert(error +"---"+ xhr.responseText+"---"+status);
											 alert("Error");
											}
									});
								}
								else
								{
									//$('#msgholidaysave').html("Select Proper Holiday Dates");
								}
							
								return false;
							}
							
							$.ajax({
								type : "GET",
								url : myContextPath+"/checkHoliday?employeeDetailsId="+empId+"&startDate="+start+"&endDate="+end,
								dataType : "json",
								data : $("#searchForm").serialize(),
								async: false,
								success : function(data) {
									//alert(data.status);
									$('#msgholidaysave').html(data.status);
									if(data.status!=="")
									{
										//alert("not submit");
										holidaycheckornot=false;
										return false;
									}
									holidaycheckornot=true;
									//alert(holidaycheckornot);
								},
								error: function(xhr, status, error) {
									  //var err = eval("(" + xhr.responseText + ")");
									  alert("Error");
									}
							});
							
							var paidStatus=$('#paidStatusId').val();
							if(paidStatus==="0")
							{
								$('#msgholidaysave').html("Select Paid Status");
								return false;
							}

							var reason=$('#reason').val();
							/*if(reason==="")
							{
								$('#msgholidaysave').html("Enter Holiday Reason");
								return false;
							}*/
							
							/*alert("success : "+holidaycheckornot);
							return false;*/
							
							if(holidaycheckornot===true && $('#msgholidaysave').html()==="")
							{
								var form = $('#saveHoliday');
								//alert($('#saveHoliday').attr('action'));
								//alert("going submit");
								//alert(form.serialize()+"&paidStatus="+$('#paidStatusId').val());
								$.ajax({
										type : form.attr('method'),
										url : form.attr('action'),
										//dataType : "json",
										data : form.serialize()+"&paidStatus="+$('#paidStatusId').val(),
										//async: false,
										success : function(data) {
											$('.modal').modal();
											if(data==="Success")
											{
												$('#addeditmsg').modal('open');
												$('#head').html("Holiday Booking Message ");
												$('#msg').html("<font color='green'>Holiday Booked</font>");
														 setTimeout(
														  function() 
														  {
															  location.reload();
														  }, 2000);
												
											}
											else
											{
												$('#addeditmsg').modal('open');
												$('#head').html("Holiday Booking Message ");
												$('#msg').html("<font color='green'>Holiday Booking Failed</font>");
											}
									},
									error: function(xhr, status, error) {
										  //var err = eval("(" + xhr.responseText + ")");
										  //alert(error +"---"+ xhr.responseText+"---"+status);
										 alert("Error");
										}
								});
							}
							else
							{
								//$('#msgholidaysave').html("Select Proper Holiday Dates");
							}
						});
						
						$('#fromDate').change(function(){

							var toDate=new Date($('#toDate').val()).setHours(0,0,0,0);
							var fromDate=new Date($('#fromDate').val()).setHours(0,0,0,0);
							var today=new Date().setHours(0,0,0,0);
							//$('#toDate').val($('#fromDate').val());
							//$('#toDate').pickadate();
							//$('#toDate').focus();
							$('#fromDate').focus();
							
							/*if(fromDate < today)
							{
								$('#msgholidaysave').html("Select From Date in Current Date or After that date");
								
							}
							else*/ if(fromDate == today)
							{
								$('#msgholidaysave').html("");
								
							}
							else if(toDate <= fromDate)
							{
								$('#msgholidaysave').html("Select From Date in Before ToDate");
								
							}
							else
							{
								$('#msgholidaysave').html("");
								
							}
							
						});
						
						
						
						$('#toDate').change(function(){
							//alert(new Date());		
							var toDate=new Date($('#toDate').val()).setHours(0,0,0,0);
							var fromDate=new Date($('#fromDate').val()).setHours(0,0,0,0);
							var today=new Date().setHours(0,0,0,0);
							
							/*if(toDate < today)
							{
								$('#msgholidaysave').html("Select To Date in Current Date or After that date");
								
							}
							else*/ if(toDate <= fromDate)
							{
								$('#msgholidaysave').html("Select To Date in After From Date");
								
							}
							else if(toDate == today)
							{
								$('#msgholidaysave').html("");
								
							}
							else
							{
								$('#msgholidaysave').html("");
								
							}
							
						}); 
						
						$('#oneDate').change(function(){
							var today=new Date().setHours(0,0,0,0);
							var oneDate=new Date($('#oneDate').val()).setHours(0,0,0,0);
							
							/*if(oneDate < today)
							{
								$('#msgholidaysave').html("Select One Date in Current Date or After that date");
								
							}
							else*/ if(toDate == today)
							{
								$('#msgholidaysave').html("");
								
							}
							else
							{
								$('#msgholidaysave').html("");
								
							}
						});
						
						$('#saveIncentivesSubmit').click(function(){
							
							var incentiveAmount=$('#incentiveAmount').val();
							var reason=$('#reasonIncentive').val();
							if(incentiveAmount==="")
							{
								$('#msgincentivesave').html("Enter Incentive Amount");
								return false;
							}
							else if(reason.trim()==="")
							{
								$('#msgincentivesave').html("Enter Incentive Reason");
								return false;
							}
							var form = $('#saveIncentive');
							/*var incentiveId=$('#incentiveId').val();
							if(incentiveId==="0")
							{*/
							
								$('#msgincentivesave').html("");
								
								//alert(form.serialize());
								$.ajax({
										type : form.attr('method'),
										url : form.attr('action'),
										data : form.serialize(),
										//async: false,
										success : function(data) {
											
											if(data==="Success")
											{
												$('#msgincentivesave').html("<font color='green'>Incentive Assigned </green>");
												//searchEmployeeIncentive(1);
												/*$('.modal').modal();
												$('#incentive').modal('close');*/
												 setTimeout(
														  function() 
														  {
															  location.reload();
														  }, 2000);
											}
											else
											{
												$('#msgincentivesave').html("<font color='red'>Incentive Assigning Failed</green>");
											}
									},
									error: function(xhr, status, error) {
										  //var err = eval("(" + xhr.responseText + ")");
										  //alert(error +"---"+ xhr.responseText+"---"+status);
										 alert("Error");
										}
								});
							
							/*}
							else
							{
								$.ajax({
									type : form.attr('method'),
									url : myContextPath+"/editIncentivesAjax",
									data : form.serialize(),
									success : function(data) {
									
										if(data==="Success")
										{
											$('#msgincentivesave').html("<font color='green'>Incentive SuccessFully Updated</green>");
											$('#incentiveId').val("0");
											$('#incentiveAmount').val("");
											$('#saveIncentivesSubmit').text("Save");
											searchEmployeeIncentive(1);
										}
										else
										{
											$('#msgincentivesave').html("<font color='red'>Incentive Updating Failed</green>");
										}
									}
								});
							}*/
							
						});
						
						$('#resetIncentive').click(function (){
							$('#incentiveId').val("0");
							$('#incentiveAmount').val("");
							$('#saveIncentivesSubmit').text("Save");
						});
						
						/*$('#paymentamount').keydown(function(e){            	
							-1!==$.inArray(e.keyCode,[46,8,9,27,13,110,190])||/65|67|86|88/.test(e.keyCode)&&(!0===e.ctrlKey||!0===e.metaKey)||35<=e.keyCode&&40>=e.keyCode||(e.shiftKey||48>e.keyCode||57<e.keyCode)&&(96>e.keyCode||105<e.keyCode)&&e.preventDefault()
						 });
			            
						document.getElementById('paymentamount').onkeypress=function(e){
			            	
			            	if (e.keyCode === 46 && this.value.split('.').length === 2) {
			              		 return false;
			          		 }

			            }*/
						
						
						$('#incentiveAmount').keydown(function(e){            	
							-1!==$.inArray(e.keyCode,[46,8,9,27,13,110,190])||/65|67|86|88/.test(e.keyCode)&&(!0===e.ctrlKey||!0===e.metaKey)||35<=e.keyCode&&40>=e.keyCode||(e.shiftKey||48>e.keyCode||57<e.keyCode)&&(96>e.keyCode||105<e.keyCode)&&e.preventDefault()
						 });
			            
						document.getElementById('incentiveAmount').onkeypress=function(e){
			            	
			            	if (e.keyCode === 46 && this.value.split('.').length === 2) {
			              		 return false;
			          		 }

			            }
						
						$('#savePaymentId').click(function(){
							
							var payCash=$('#paymentCash').is(':checked');
							var payCheque=$('#paymentCheque').is(':checked');
							
							var amount=$('#paymentamount').val();
							var bankName=$('#bankName').val();
							var cheqNo=$('#cheqNo').val();
							var cheqDate=$('#cheqDate').val();
							var paymentcomment=$('#paymentcomment').val();
							
							if(amount==="")
							{
								$('#msgpaymentsave').html("Enter Amount");
								return false;
							}
							
							if(paymentcomment==="")
							{
								$('#msgpaymentsave').html("Enter Comment");
								return false;
							}
							
							if(payCheque==true)
							{
								if(bankName==="")
								{
									$('#msgpaymentsave').html("Enter Bank Name");
									return false;
								}
								
								if(cheqNo==="")
								{
									$('#msgpaymentsave').html("Enter Cheque Number");
									return false;
								}
								
								if(cheqDate==="")
								{
									$('#msgpaymentsave').html("Enter Cheque Date");
									return false;
								}
								
								var today=new Date().setHours(0,0,0,0);
								var chDate=new Date(cheqDate).setHours(0,0,0,0);
								
								if(chDate < today)
								{
									$('#msgpaymentsave').html("Select Cheque Date in Current Date or After that date");
									return false;
								}
								
							}
							$('#msgpaymentsave').html('');
							
							var form = $('#savePayment');
							
							$.ajax({
									type : form.attr('method'),
									url : form.attr('action'),
									data : form.serialize(),
									//async: false,
									success : function(data) {
										
										if(data==="Success")
										{
											$('#msgpaymentsave').html("<font color='green'>Payment Done </green>");
											id=$('#employeeDetailsIdId').val();
											$('#employeeDetailsIdId').val('');
											$('#paymentamount').val('');
											$('#bankName').val('');
											$('#cheqNo').val('');
											$('#cheqDate').val('');
											$('#paymentcomment').val('');
											$('#employeeDetailsIdId').val(id);
											
											setTimeout(
													  function() 
													  {
														  location.reload();
													  }, 2000);
											
											$.ajax({
												url : myContextPath+"/openPaymentModel?employeeDetailsId="+id,
												dataType : "json",
												async: false,
												success : function(data) {
													
													employeePaymentModel=data;
													$('#employeeDetailsIdId').val(id);
													$('#paymentName').html("<b>Name : </b>"+employeePaymentModel.name);
													$('#paymenttotalAmt').html("<b>Total Amount : </b>"+employeePaymentModel.totalsalary.toFixed(2));
													$('#paymentamtPaid').html("<b>Amount Paid : </b>"+employeePaymentModel.amountPaid.toFixed(2));
													$('#paymentamtRemaining').html("<b>Amount Pending : </b>"+employeePaymentModel.amountPending.toFixed(2));
												}
											});
										}
										else
										{
											$('#msgpaymentsave').html("<font color='red'>Payment Failed</green>");
										}
								},
								error: function(xhr, status, error) {
									  //var err = eval("(" + xhr.responseText + ")");
									  //alert(error +"---"+ xhr.responseText+"---"+status);
									 alert("Error");
									}
							});
							
						});
						
						$("#checkAll").click(function () {
			            	var colcount=$('#tblData').DataTable().columns().header().length;
			                var cells = $('#tblData').DataTable().column(colcount-1).nodes(), // Cells from 1st column
			                    state = this.checked;
								//alert(state);
			                for (var i = 0; i < cells.length; i += 1) {
			                    cells[i].querySelector("input[type='checkbox']").checked = state;
			                }		                
			                
			            });
			            
			             $("input:checkbox").change(function(a){
			            	 
			            	 var colcount=$('#tblData').DataTable().columns().header().length;
			                 var cells = $('#tblData').DataTable().column(colcount-1).nodes(), // Cells from 1st column
			                     
			                  state = false; 				
			                 var ch=false;
			                 
			                 for (var i = 0; i < cells.length; i += 1) {
			                	 //alert(cells[i].querySelector("input[type='checkbox']").checked);
			                	
			                     if(cells[i].querySelector("input[type='checkbox']").checked == state)
			                     { 
			                    	 $("#checkAll").prop('checked', false);
			                    	 ch=true;
			                     }                      
			                 }
			                 if(ch==false)
			                 {
			                	 $("#checkAll").prop('checked', true);
			                 }
			                	
			                //alert($('#tblData').DataTable().rows().count());
			            });
						
						$('#sendsmsid').click(function(){
							var count=parseInt('${count}')
							checkedId=[];
							//chb
							 var idarray = $("#employeeTblData")
				             .find("input[type=checkbox]") 
				             .map(function() { return this.id; }) 
				             .get();
							 var j=0;
							 for(var i=0; i<idarray.length; i++)
							 {								  
								 idarray[i]=idarray[i].replace('chb','');
								 if($('#chb'+idarray[i]).is(':checked'))
								 {
									 checkedId[j]=idarray[i];
									 j++;
								 }
							 }
							 $('#smsText').val('');
							 $('#smsSendMesssage').html('');
							 
							 $('#smsText').val('');
							 $('#smsSendMesssage').html('');
							 
							 if(checkedId.length==0)
							 {
								 $('#addeditmsg').modal('open');
							     $('#msgHead').text("Message : ");
							     $('#msg').text("Select Employee For Send SMS");
							 }
							 else
							 {
								 $('#smsText').val('');
								 $('#smsSendMesssage').html('');
								 $('#sendMsg').modal('open');
							 }
						});
						
						$('#sendMessageId').click(function(){
							
							
							 $('#smsSendMesssage').html('');
							
							if(checkedId.length==0)
							{
								$('#smsSendMesssage').html("<font color='red'>Select Employee For Send SMS</font>");
								return false;
							}
							
							var form = $('#sendSMSForm');
							//alert(form.serialize()+"&employeeDetailsId="+checkedId);
							
							$.ajax({
									type : form.attr('method'),
									url : form.attr('action'),
									data : form.serialize()+"&employeeDetailsId="+checkedId,
									//async: false,
									success : function(data) {
										if(data==="Success")
										{
											$('#smsSendMesssage').html("<font color='green'>SMS send SuccessFully</font>");
											$('#smsText').val('');
										}
										else
										{
											$('#smsSendMesssage').html("<font color='red'>SMS sending Failed</font>");
										}
									}
							});
						});
						
						
					});
function showSalaryDetail(id)
{
	
	
	$.ajax({
		url : myContextPath+"/fetchEmployeeDetail?employeeDetailsId="+id,
		dataType : "json",
		success : function(data) {
			employeeSalaryStatus=data[0];
			$('#empdetailname').html("<b>Name : </b>"+employeeSalaryStatus.name);
			$('#empdetaildept').html("<b>Department : </b>"+employeeSalaryStatus.departmentname);
			$('#empdetailmobile').html("<b>Mobile Number : </b>"+employeeSalaryStatus.mobileNumber);
			$('#empdetailaddress').html("<b>Address : </b>"+employeeSalaryStatus.address);
			$('#empdetailareas').html("<b>Areas : </b>"+employeeSalaryStatus.areaList);
			$('#empdetailnoofholidays').html("<b>Holidays : </b>"+employeeSalaryStatus.noOfHolidays+" days");
			$('#empdetailbasicsalary').html("<b>Basic Salary : </b>"+employeeSalaryStatus.basicMonthlySalary+" &#8377;");
			$('#empdetaildeduction').html("<b>Deduction : </b>"+employeeSalaryStatus.deduction+" &#8377;");
			$('#empdetailincentive').html("<b>Incentives : </b>"+employeeSalaryStatus.getIncentive+" &#8377;");
			$('#empdetailtotalsalary').html("<b>Total Amount : </b>"+employeeSalaryStatus.totalAmount+" &#8377;");
			$('#empdetailamtpaid').html("<b>Paid Amount : </b>"+employeeSalaryStatus.amountPaidCurrentMonth+" &#8377;");
			$('#empdetailamtpending').html("<b>Pending Amount : </b>"+employeeSalaryStatus.amountPendingCurrentMonth+" &#8377;");
			//alert(employeeSalaryStatus.employeeDetailsId);
			$('#employeeDetailsId').val(employeeSalaryStatus.employeeDetailsId);	
			
			employeeSalarytable=employeeSalaryStatus.employeeSalaryList;
			$("#empdetailtable").empty();
			if(employeeSalarytable!==null)
			{
				for (var i = 0, len = employeeSalarytable.length; i < len; ++i)
				{
					employeeSalary=employeeSalarytable[i];
					var currentTime=new Date(parseInt(employeeSalary.date));
					var month = currentTime.getMonth() + 1;
					var day = currentTime.getDate();
					var year = currentTime.getFullYear();
					var date = day + "/" + month + "/" + year;
					
						$("#empdetailtable").append("<tr>"+
											        "<td>"+employeeSalary.srno+"</td>"+
											        "<td>"+employeeSalary.amount+" &#8377;"+"</td>"+
											        "<td>"+employeeSalary.incentives+" &#8377;"+"</td>"+
											        "<td>"+date+"</td>"+
											        "<td>"+employeeSalary.modeOfPayment+"</td>"+
											        "<td>"+employeeSalary.comment+"</td>"+
											    	"</tr>"); 
						
				}
			}
			$('.modal').modal();
			$('#EmpDetails').modal('open');
		}
	});
}

function searchEmployeeSalary(choice)
{
	
	var filter="";
	var url="";
	var employeeDetailsId=$('#employeeDetailsId').val();
	var startDate=$('#startDate1').val();
	var endDate=$('#endDate1').val();
	//alert(startDate +"-"+ endDate);
	//return false;
	if(choice==1)
	{
		filter="CurrentMonth";
		url=myContextPath+"/fetchEmployeeSalaryByFilter?filter="+filter+"&employeeDetailsId="+employeeDetailsId+"&startDate=''&endDate=''";
	}
	else if(choice==2)
	{
		filter="Last6Months";
		url=myContextPath+"/fetchEmployeeSalaryByFilter?filter="+filter+"&employeeDetailsId="+employeeDetailsId+"&startDate=''&endDate=''";
		
	}
	else if(choice==3)
	{
		filter="Last1Year";
		url=myContextPath+"/fetchEmployeeSalaryByFilter?filter="+filter+"&employeeDetailsId="+employeeDetailsId+"&startDate=''&endDate=''";
	}
	else if(choice==4)
	{
		filter="Range";
		
		url=myContextPath+"/fetchEmployeeSalaryByFilter?filter="+filter+"&employeeDetailsId="+employeeDetailsId+"&startDate="+startDate+"&endDate="+endDate;
	}
	else if(choice==5)
	{
		filter="ViewAll";
		url=myContextPath+"/fetchEmployeeSalaryByFilter?filter="+filter+"&employeeDetailsId="+employeeDetailsId+"&startDate=''&endDate=''";
	}
		
	
	$.ajax({
		type : "GET",
		url : url,
		/* data: "id=" + id + "&name=" + name, */
		success : function(data) {
			
			employeeSalaryStatus=data[0];
			$('#empdetailname').html("<b>Name : </b>"+employeeSalaryStatus.name);
			$('#empdetaildept').html("<b>Department : </b>"+employeeSalaryStatus.departmentname);
			$('#empdetailmobile').html("<b>Mobile Number : </b>"+employeeSalaryStatus.mobileNumber);
			$('#empdetailaddress').html("<b>Address : </b>"+employeeSalaryStatus.address);
			$('#empdetailareas').html("<b>Areas : </b>"+employeeSalaryStatus.areaList);
			$('#empdetailnoofholidays').html("<b>Holidays : </b>"+employeeSalaryStatus.noOfHolidays+" days");
			$('#empdetailbasicsalary').html("<b>Basic Salary : </b>"+employeeSalaryStatus.basicMonthlySalary+" &#8377;");
			$('#empdetaildeduction').html("<b>Deduction : </b>"+employeeSalaryStatus.deduction+" &#8377;");
			$('#empdetailincentive').html("<b>Incentives : </b>"+employeeSalaryStatus.getIncentive+" &#8377;");
			$('#empdetailtotalsalary').html("<b>Total Amount : </b>"+employeeSalaryStatus.totalAmount+" &#8377;");
			$('#empdetailamtpaid').html("<b>Paid Amount : </b>"+employeeSalaryStatus.amountPaidCurrentMonth+" &#8377;");
			$('#empdetailamtpending').html("<b>Pending Amount : </b>"+employeeSalaryStatus.amountPendingCurrentMonth+" &#8377;");
			//alert(employeeSalaryStatus.employeeDetailsId);
			$('#employeeDetailsId').val(employeeSalaryStatus.employeeDetailsId);	
			
			employeeSalarytable=employeeSalaryStatus.employeeSalaryList;
			$("#empdetailtable").empty();
			if(employeeSalarytable!==null)
			{
				for (var i = 0, len = employeeSalarytable.length; i < len; ++i)
				{
					employeeSalary=employeeSalarytable[i];
					var currentTime=new Date(parseInt(employeeSalary.date));
					var month = currentTime.getMonth() + 1;
					var day = currentTime.getDate();
					var year = currentTime.getFullYear();
					var date = day + "/" + month + "/" + year;
					
						$("#empdetailtable").append("<tr>"+
											        "<td>"+employeeSalary.srno+"</td>"+
											        "<td>"+employeeSalary.amount+" &#8377;"+"</td>"+
											        "<td>"+employeeSalary.incentives+" &#8377;"+"</td>"+
											        "<td>"+date+"</td>"+
											        "<td>"+employeeSalary.modeOfPayment+"</td>"+
											        "<td>"+employeeSalary.comment+"</td>"+
											    	"</tr>"); 
						
				}
			}
			/*$('.modal').modal();
			$('#EmpDetails').modal('open');*/
		}
	});
}
	function showHolidayDetail(id)
	{
		//alert("showHolidayDetail "+id);
		$.ajax({
			url : myContextPath+"/fetchEmployeeHolidayDetail?employeeDetailsId="+id,
			dataType : "json",
			success : function(data) {
				employeeHolidayStatus=data;
				//alert(employeeHolidayStatus);
				$('#employeeDetailsId1').val(employeeHolidayStatus.employeeDetailPkId);
				$('#empholidayname').html("<b>Name : </b>"+employeeHolidayStatus.name);
				$('#empholidaydepartment').html("<b>Department : </b>"+employeeHolidayStatus.departmentname);
				$('#empholidaymobileno').html("<b>Mobile Number : </b>"+employeeHolidayStatus.mobileNumber);
				$('#empholidaynoofholiday').html("<b>No Of Holidays : </b>"+employeeHolidayStatus.noOfHolidays);
										
				employeeHolidaytable=employeeHolidayStatus.employeeHolidayList;
				$("#holidaytable").empty();
				if(employeeHolidaytable!==null)
				{
					for (var i = 0, len = employeeHolidaytable.length; i < len; ++i)
					{
						employeeHoliday=employeeHolidaytable[i];
						var currentTime=new Date(parseInt(employeeHoliday.date));
						var month = currentTime.getMonth() + 1;
						var day = currentTime.getDate();
						var year = currentTime.getFullYear();
						var date = day + "/" + month + "/" + year;
						
						var currentTime=new Date(parseInt(employeeHoliday.dateFrom));
						 month = currentTime.getMonth() + 1;
						 day = currentTime.getDate();
						 year = currentTime.getFullYear();
						var dateFrom = day + "/" + month + "/" + year;
						
						var currentTime=new Date(parseInt(employeeHoliday.dateTo));
						 month = currentTime.getMonth() + 1;
						 day = currentTime.getDate();
						 year = currentTime.getFullYear();
						var dateTo = day + "/" + month + "/" + year;
						//alert(employeeHoliday.employeeHolidayId);
						
						//alert(employeeHolidayStatus.status);
						if(employeeHolidayStatus.status==true)
						{
							editButton="<td><button type='button' class='btn btn-flat blue-text' disabled='disabled' onclick='editHoliday("+employeeHoliday.employeeHolidayId+")'><i class='material-icons'>edit</i></button></td>";
						}	
						else
						{
							editButton="<td><button type='button' class='btn btn-flat blue-text' onclick='editHoliday("+employeeHoliday.employeeHolidayId+")'><i class='material-icons'>edit</i></button></td>";
						}
						
							$("#holidaytable").append("<tr>"+
												        "<td>"+employeeHoliday.srno+"</td>"+
												        "<td>"+employeeHoliday.noOfHolidays+"</td>"+	
												        "<td>"+dateFrom+"</td>"+
												        "<td>"+dateTo+"</td>"+
												        "<td>"+employeeHoliday.typeOfLeave+"</td>"+
												        "<td>"+(employeeHoliday.amountDeduct).toFixed(2)+" &#8377;"+"</td>"+
												        "<td>"+employeeHoliday.reason+"</td>"+
												        "<td>"+date+"</td>"+
												         editButton+
												    	"</tr>"); 
							
					}
				}
				$('.modal').modal();
				$('#viewHoliday').modal('open');
			}
		});
	}
	function searchEmployeeHolidays(choice)
	{  $(".showDates").hide();
		var filter="";
		var url="";
		var employeeDetailsId=$('#employeeDetailsId1').val();
		var startDate=$('#startDate2').val();
		var endDate=$('#endDate2').val();
		//alert(startDate +"-"+ endDate);
		//return false;
		if(choice==1)
		{
			filter="CurrentMonth";
			url=myContextPath+"/fetchEmployeeHolidayDetailByFilter?filter="+filter+"&employeeDetailsId="+employeeDetailsId+"&startDate=''&endDate=''";
		}
		else if(choice==2)
		{
			filter="Last6Months";
			url=myContextPath+"/fetchEmployeeHolidayDetailByFilter?filter="+filter+"&employeeDetailsId="+employeeDetailsId+"&startDate=''&endDate=''";
			
		}
		else if(choice==3)
		{
			filter="Last1Year";
			url=myContextPath+"/fetchEmployeeHolidayDetailByFilter?filter="+filter+"&employeeDetailsId="+employeeDetailsId+"&startDate=''&endDate=''";
		}
		else if(choice==4)
		{
			filter="Range";
			
			url=myContextPath+"/fetchEmployeeHolidayDetailByFilter?filter="+filter+"&employeeDetailsId="+employeeDetailsId+"&startDate="+startDate+"&endDate="+endDate;
		}
		else if(choice==5)
		{
			filter="ViewAll";
			url=myContextPath+"/fetchEmployeeHolidayDetailByFilter?filter="+filter+"&employeeDetailsId="+employeeDetailsId+"&startDate=''&endDate=''";
		}
			
		
		$.ajax({
			type : "GET",
			url : url,
			/* data: "id=" + id + "&name=" + name, */
			success : function(data) {
				
				employeeHolidayStatus=data;
				
				$('#empholidayname').html("<b>Name : </b>"+employeeHolidayStatus.name);
				$('#empholidaydepartment').html("<b>Department : </b>"+employeeHolidayStatus.departmentname);
				$('#empholidaymobileno').html("<b>Mobile Number : </b>"+employeeHolidayStatus.mobileNumber);
				$('#empholidaynoofholiday').html("<b>No Of Holidays : </b>"+employeeHolidayStatus.noOfHolidays);
						
				employeeHolidaytable=employeeHolidayStatus.employeeHolidayList;
				$("#holidaytable").empty();
				if(employeeHolidaytable!==null)
				{
					for (var i = 0, len = employeeHolidaytable.length; i < len; ++i)
					{
						employeeHoliday=employeeHolidaytable[i];
						var holydayTakenDate=new Date(parseInt(employeeHoliday.date));
						var month = holydayTakenDate.getMonth() + 1;
						var day = holydayTakenDate.getDate();
						var year = holydayTakenDate.getFullYear();
						var holydayTakenDateString = day + "/" + month + "/" + year;
						
						var dateFrom=new Date(parseInt(employeeHoliday.dateFrom));
						month = dateFrom.getMonth() + 1;
						day = dateFrom.getDate();
						year = dateFrom.getFullYear();
						var dateFromString = day + "/" + month + "/" + year;
						
						var dateTo=new Date(parseInt(employeeHoliday.dateTo));
						month = dateTo.getMonth() + 1;
						day = dateTo.getDate();
						year = dateTo.getFullYear();
						var dateToString = day + "/" + month + "/" + year;
						
						/*<th>Sr.No</th>
                        <th>No.of holiday</th>
                        <th>From Date</th>
                        <th>To Date</th>
                        <th>Type Of Leave</th>
                        <th>Amount Deduct</th>
                        <th>Reason</th>
                        <th>Holiday Given Date</th>
                        
                        *private long srno;
						private long noOfHolidays;
						private String typeOfLeave;
						private double amountDeduct;
						private Date date;
						private Date dateFrom;
						private Date dateTo;	
						private String reason;
                        *
                        *disabled="disabled"
                        */
						//alert(employeeHolidayStatus.status);
						if(employeeHolidayStatus.status==true)
						{
							editButton="<td><button type='button' class='btn btn-flat blue-text' disabled='disabled' onclick='editHoliday("+employeeHoliday.employeeHolidayId+")'><i class='material-icons blue-text'>edit</i></button></td>";
						}	
						else
						{
							editButton="<td><button type='button' class='btn btn-flat blue-text' onclick='editHoliday("+employeeHoliday.employeeHolidayId+")'><i class='material-icons blue-text'>edit</i></button></td>";
						}
						$("#holidaytable").append("<tr>"+
											        "<td>"+employeeHoliday.srno+"</td>"+
											        "<td>"+employeeHoliday.noOfHolidays+"</td>"+
											        "<td>"+dateFromString+"</td>"+
											        "<td>"+dateToString+"</td>"+
											        "<td>"+employeeHoliday.typeOfLeave+"</td>"+												        
											        "<td>"+employeeHoliday.amountDeduct.toFixed(2)+" &#8377;"+"</td>"+
											        "<td>"+employeeHoliday.reason+"</td>"+
											        "<td>"+holydayTakenDateString+"</td>"+
											        editButton+
											    	"</tr>"); 
							
					}
				}
				/*$('.modal').modal();
				$('#EmpDetails').modal('open');*/
			}
		});

	}
	
	function editHoliday(id)
	{
		$.ajax({
			url : myContextPath+"/fetchEmployeeHolidaysByEmployeeHolidaysId?employeeHolidaysId="+id,
			dataType : "json",
			success : function(data) {
				$('#holiodaydetailsname').html("<b>Name : </b>"+data.employeeDetails.name);
				$('#holiodaydetailsmobilenumber').html("<b>Mobile Number : </b>"+data.employeeDetails.contact.mobileNumber);
				dateAdded=data.employeeDetails.employeeDetailsAddedDatetime;
				$('#employeeIdForHolidaySave').val(data.employeeDetails.employeeDetailsId);
				$('#employeeHolidaysId').val(data.employeeHolidaysId);
				
				var dateFrom=new Date(parseInt(data.fromDate));
				month = dateFrom.getMonth() + 1;
				day = dateFrom.getDate();
				year = dateFrom.getFullYear();
				var dateFromString = year + "-" + month + "-" + day;
				
				var dateTo=new Date(parseInt(data.toDate));
				month = dateTo.getMonth() + 1;
				day = dateTo.getDate();
				year = dateTo.getFullYear();
				var dateToString = year + "-" + month + "-" + day;

				//alert(dateToString);
				
				var fromDate=new Date(dateFromString).setHours(0,0,0,0);
				var toDate=new Date(dateToString).setHours(0,0,0,0);
				
				if(fromDate==toDate)
				{				
					$('#holiodaydetailsnoofholidays').val("1");
					
					$('#oneDate').val(dateFromString);
					$('#oneDate').change();
					$('#holiodaydetailsnoofholidays').change();
				}
				else
				{
					duration = moment(dateTo).diff(moment(dateFrom), 'days')+1; 
					
					//var diffDays = daydiff(parseDate(fromDate), parseDate(toDate));
					$('#holiodaydetailsnoofholidays').val(duration);
					
					$('#fromDate').val(dateFromString);
					$('#fromDate').change();
					
					$('#toDate').val(dateToString);
					$('#toDate').change();
					$('#holiodaydetailsnoofholidays').change();
				}
				
				
				var source3 = $("#paidStatusId");
				if(data.paidHoliday==1)
				{
					source3.val("paid");
				}
				else
				{
					source3.val("unpaid");
				}
				source3.change();
				
				$('#reason').val(data.reason);
				$('#reason').change();
				$('#submitHoliday').text("Update");
								
				//holiday
				$('.modal').modal();
				$('#viewHoliday').modal('close');
				$('#holiday').modal('open');
			}
		});
	}
	
	function showAreaDetail(id)
	{
		$.ajax({
			url : myContextPath+"/fetchEmployeeRoutes?employeeDetailsId="+id,
			dataType : "json",
			success : function(data) {
				
				employeeAreaDetails=data;
				$('#employeeareaname').html("<b>Name : </b>"+employeeAreaDetails.name);
				$('#employeeareadeptname').html("<b>Department: </b>"+employeeAreaDetails.departmentName);
				//alert(employeeAreaDetails.MobileNumber);
				$('#employeeareamobilenumber').html("<b>Mobile No. : </b>"+employeeAreaDetails.mobileNumber);
				
				employeeAreaList=employeeAreaDetails.routeList;
				var srno=1;
				$("#areaList").empty();
				for(var i=0; i<employeeAreaList.length; i++)
				{					
					routelist=employeeAreaList[i];
					//alert(arealist);
					$('#areaList').append("<tr>"+
					        "<td>"+srno+"</td>"+
					        "<td>"+routelist.routeNo+"</td>"+
					        "<td>"+routelist.area.name+"</td>"+
					        "<td>"+routelist.area.cluster.name+"</td>"+
					        "</tr>"); 
					srno++;
				}
				$('.modal').modal();
				$('#area').modal('open');
			}
		});
	}
	
function openHolidayBookModel(id)
{
	$('#submitHoliday').text("Submit");
	$('#employeeIdForHolidaySave').val(id);
	$('#msgholidaysave').html('');
	$('#oneday').click();
	//$('#onedays').prop('checked', true);
	$('#holiodaydetailsnoofholidays').val('');
	$('#oneDate').val('');
	$('#fromDate').val('');
	$('#toDate').val('');
	$('#fromDate').change();
	$('#toDate').change();
	$('#oneDate').change();
	$('#reason').val('');
	$('#oneDateDiv').hide();
	$('#fromDateDiv').hide();
	$('#toDateDiv').hide();
	 var source=$('#paidStatusId');
		source.val('0');
		source.change();
		holidays=0;
	
	
	$.ajax({
		url : myContextPath+"/fetchEmployeeDetailsAjax?employeeDetailsId="+id,
		dataType : "json",
		success : function(data) {
			
			employeeDetails=data;
			$('#holiodaydetailsname').html("<b>Name : </b>"+employeeDetails.name);
			$('#holiodaydetailsmobilenumber').html("<b>Mobile Number : </b>"+employeeDetails.contact.mobileNumber);
			dateAdded=employeeDetails.employeeDetailsAddedDatetime;
			$('.modal').modal();
			$('#holiday').modal('open');
		}
	});
	
	
	
}

function editIncentives(id,amt){
	$('#incentiveId').val(id);
	$('#incentiveAmount').val(amt);
	$('#saveIncentivesSubmit').text("Update");
}



function openIncentivesModel(id)
{

	$('#incentiveName').val('');
	$('#incentiveAmount').val('');
	$('#incentiveEmployeeDetailsId').val(id);
	$('#msgincentivesave').html('');
	
	$.ajax({
		url : myContextPath+"/fetchEmployeeDetailsAjax?employeeDetailsId="+id,
		dataType : "json",
		success : function(data) {
			
			employeeDetails=data;
			$('#incentiveName').val(employeeDetails.name);
			
			$('.modal').modal();
			$('#incentive').modal('open');
		}
	});
	
	
	
}

/*function searchEmployeeIncentive(choice)
{
	var filter="";
	var url="";
	var employeeDetailsId=$('#incentiveEmployeeDetailsId').val();
	var startDate=$('#startDateIncentive').val();
	var endDate=$('#endDateIncentive').val();
	//alert(startDate +"-"+ endDate);
	//return false;
	if(choice==1)
	{
		filter="CurrentMonth";
		url=myContextPath+"/fetchEmployeeIncentiveDetailByFilter?filter="+filter+"&employeeDetailsId="+employeeDetailsId+"&startDate=''&endDate=''";
	}
	else if(choice==2)
	{
		filter="Last6Months";
		url=myContextPath+"/fetchEmployeeIncentiveDetailByFilter?filter="+filter+"&employeeDetailsId="+employeeDetailsId+"&startDate=''&endDate=''";		
	}
	else if(choice==3)
	{
		filter="Last1Year";
		url=myContextPath+"/fetchEmployeeIncentiveDetailByFilter?filter="+filter+"&employeeDetailsId="+employeeDetailsId+"&startDate=''&endDate=''";
	}
	else if(choice==4)
	{
		filter="Range";
		
		url=myContextPath+"/fetchEmployeeIncentiveDetailByFilter?filter="+filter+"&employeeDetailsId="+employeeDetailsId+"&startDate="+startDate+"&endDate="+endDate;
	}
	else if(choice==5)
	{
		filter="ViewAll";
		url=myContextPath+"/fetchEmployeeIncentiveDetailByFilter?filter="+filter+"&employeeDetailsId="+employeeDetailsId+"&startDate=''&endDate=''";
	}
		
	
	$.ajax({
		type : "GET",
		url : url,
		 data: "id=" + id + "&name=" + name, 
		success : function(data) {
			
			employeeDetails=data.employeeDetails; 
			employeeIncentiveList=data.employeeIncentiveList;
			
			$('#incentiveName').val(employeeDetails.name);
			
			$("#incentiveTable").empty();
			if(employeeIncentiveList!==null)
			{
				for (var i = 0, len = employeeIncentiveList.length; i < len; ++i)
				{
					employeeIncentive=employeeIncentiveList[i];
					var incentiveGivenDate=new Date(parseInt(employeeIncentive.incentiveGivenDate));
					var month = incentiveGivenDate.getMonth() + 1;
					var day = incentiveGivenDate.getDate();
					var year = incentiveGivenDate.getFullYear();
					var incentiveGivenDateString = day + "/" + month + "/" + year;
					
					
					$("#incentiveTable").append("<tr>"+
										        "<td>"+(i+1)+"</td>"+
										        "<td>"+employeeIncentive.incentiveAmount+"</td>"+
										        "<td>"+incentiveGivenDateString+"</td>"+
										        "<td><button type='button' onclick='editIncentives("+employeeIncentive.employeeIncentiveId+","+employeeIncentive.incentiveAmount+")'>Edit</button></td>"+
										    	"</tr>"); 
						
				}
			}
		}
	});

}*/


	function openPaymentModel(id)
	{

		$('#employeeDetailsIdId').val('');
		$('#paymentamount').val('');
		$('#bankName').val('');
		$('#cheqNo').val('');
		$('#cheqDate').val('');
		$('#paymentcomment').val('');
		$('#employeeDetailsIdId').val(id);
		$('#msgpaymentsave').html('');
		$.ajax({
			url : myContextPath+"/openPaymentModel?employeeDetailsId="+id,
			dataType : "json",
			async: false,
			success : function(data) {
				
				employeePaymentModel=data;
				$('#employeeDetailsIdId').val(id);
				$('#paymentName').html("<b>Name : </b>"+employeePaymentModel.name);
				$('#paymenttotalAmt').html("<b>Total Amount : </b>"+employeePaymentModel.totalsalary.toFixed(2));
				$('#paymentamtPaid').html("<b>Amount Paid : </b>"+employeePaymentModel.amountPaid.toFixed(2));
				$('#paymentamtRemaining').html("<b>Amount Pending : </b>"+employeePaymentModel.amountPending.toFixed(2));
				
				$('.modal').modal();
				$('#pay').modal('open');
			}
		});	
}
