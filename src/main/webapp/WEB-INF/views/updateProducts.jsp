<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<html>

<head>
     <%@include file="components/header_imports.jsp" %>

<script type="text/javascript">

var categoryIgst;
var categorySgst;
var categoryCgst;

function gstAmountCalc(){
	var rate=parseFloat($("#productRate").val());
	if(rate>0){
	var unitprice;
	var divisor;
	var multiplier;
	var igstAmt;
	var cgstAmt;
	var sgstAmt;
	
		/* for calculating divisor means*/
		divisor=(categoryIgst/100)+1;
		//console.log(divisor);
		unitprice=(rate/divisor);
		//console.log(unitprice);
		cgstAmt=(unitprice*categoryCgst)/100;
		//console.log(igstAmt);
		//for calculating cgst percent from igst percent
		//var cgstPer=(categoryIgst/2);					
		igstAmt=parseFloat(cgstAmt.toFixed(2))+parseFloat(cgstAmt.toFixed(2));
		//console.log(cgstAmt);
		
		$("#unitRate").val(unitprice.toFixed(2));
		$("#unitRate").focus();
		$("#cgst").val(cgstAmt.toFixed(2));
		$("#cgst").focus();
		$("#sgst").val(cgstAmt.toFixed(2));
		$("#sgst").focus();
		$("#igst").val(igstAmt);
		$("#igst").focus();
		
	
	}
};

$(document).ready(function() {
			
	 /* for taking igst value of category selected */
	
	$("#categoryid").change(function(){
		var categoryid=$("#categoryid").val();
			$.ajax({
				type : "GET",
				url :"${pageContext.request.contextPath}/fetchCategories?categoriesId="+categoryid,
				async:false,
				beforeSend: function() {
					$('.preloader-background').show();
					$('.preloader-wrapper').show();
		           },
		           success : function(data) {
		        	    categoryIgst = data.igst;
		        	    categorySgst = data.sgst;
		        	    categoryCgst = data.cgst;
		        	   // gstAmountCalc();
		        	   //console.log(CategoryIgst);
		        	   $('.preloader-wrapper').hide();
						$('.preloader-background').hide();
		           },
		           error: function(xhr, status, error) {
						$('.preloader-wrapper').hide();
						$('.preloader-background').hide();
						$('#addeditmsg').modal('open');
	           	     	$('#head').text("Warning: ");
	           	     	$('#msg').text("Something Went Wrong"); 
	           	     		setTimeout(function() 
							  {
	  	     					$('#addeditmsg').modal('close');
							  }, 1000);
						}						
		           
			});			
	});
			
	/* for calculating unit price of product */
	$("#productRate").blur(function(){
		gstAmountCalc();					
	});
			
			var msg="${saveMsg}";
			 //alert(msg);
			 if(msg!='' && msg!=undefined)
			 {
			     $('#addeditmsg').modal('open');
			     $('#msgHead').text("Product Update Message");
			     $('#msg').text(msg);
			 }
			 
			 $("#updateProductSubmit").click(function() {

				 var brandid = $("#brandid").val();
				//alert(brandid);
				if (brandid == 0) {
					$('#addeditmsg').modal('open');
				     $('#msgHead').text("Product Update Message");
				     $('#msg').text("Select Brand");
					return false;
				}
			 
				var categoryid = $("#categoryid").val();
				//alert(categoryid);
				if (categoryid == 0) {
					$('#addeditmsg').modal('open');
				     $('#msgHead').text("Product Update Message");
				     $('#msg').text("Select Category");
					return false;
				}
				
				var units = $("#unitId").val();
				//alert(categoryid);
				if (units === "0") {
					$('#addeditmsg').modal('open');
				     $('#msgHead').text("Product Adding Message");
				     $('#msg').text("Select Unit Type");
					return false;
				}
				
			});
			 $('#upload').change(function (evt) {
				    var tgt = evt.target || window.event.srcElement,
				        files = tgt.files;
				
				    // FileReader support
				    if (FileReader && files && files.length) {
				        var fr = new FileReader();
				        fr.onload = function () {
				            document.getElementById('addImage').src = fr.result;
				        }
				        fr.readAsDataURL(files[0]);
				    }
				
				    // Not supported
				    else {
				        // fallback -- perhaps submit the input to an iframe and temporarily store
				        // them on the server until the user's session ends.
				    }
				}); 
			
 			$("#categoryid").change();
			 
			 var uprice="${product.rate}";
			 var divisor=(categoryIgst/100)+1;
			 var mrp=(uprice*divisor);
			 $("#productRate").val(mrp.toFixed(2));			 
			
			 gstAmountCalc();
			 
		});
</script>
</head>

<body>
   <!--navbar start-->
   	<%@include file="components/navbar.jsp" %>
    <!--navbar end-->
    <!--content start-->
    <main class="paddingBody">

        <br>
        <div class="container">
            <form action="${pageContext.servletContext.contextPath}/updateProduct" method="post" id="updateProductForm"  enctype="multipart/form-data">
			
			
                <div class="row  z-depth-3 ">
                    <br>
                    <div class="input-field col s12 m6 l4 left-align">
                    
                    	<c:choose>
                    	 <c:when test="${isProductImg==false}">
						         <img style="border:1px solid;height:200px;width:200px; margin-left:10%;margin-top:2%;" id="addImage" src="resources/img/defaultphoto.png">
                        		<input type="file" id="upload" style="display: none;" name="file"/>
				         </c:when>
				         <c:otherwise>
				         	 <img style="border:1px solid;height:200px;width:200px; margin-left:10%;margin-top:2%;" id="addImage" src="${pageContext.request.contextPath}/downloadProductImage/${product.productId}">
                        	 <input type="file" id="upload" style="display: none;" name="file"/>
						</c:otherwise>
						</c:choose>
                    </div>
                   <%--  <div class="input-field col s12 m6 l4 left-align">
                        <img id="addImage" style="border:1px solid;height:200px;width:200px; margin-left:10%;" src="${pageContext.request.contextPath}/downloadProductImage/${product.productId}">
                    </div> --%>

                   <div class="input-field col s12 m4 l4">

                        <i class="material-icons prefix">star</i>
                         <label for="brandid" class="active"><span class="red-text">*</span></label>
                        <select id="brandid" name="brandId" required>
                                 <option value="0">Choose Brand</option>
                                <c:if test="${not empty brandlist}">
							<c:forEach var="listValue" items="${brandlist}">							
								<option value="<c:out value="${listValue.brandId}" />" <c:if test="${listValue.brandId == product.brand.brandId}">selected</c:if>><c:out
										value="${listValue.name}" /></option>
							</c:forEach>
						</c:if>
                        </select>
                    </div>
                    <div class="input-field col s12 m4 l4">
                        <i class="material-icons prefix">filter_list</i>
                         <label for="categoryid" class="active"><span class="red-text">*</span></label>
                        <select id="categoryid" name="categoryId" required>
                                 <option value="0">Choose Category</option>
                                <c:if test="${not empty categorylist}">
							<c:forEach var="listValue" items="${categorylist}">
								<option value="<c:out value="${listValue.categoryId}" />"  <c:if test="${listValue.categoryId == product.categories.categoryId}">selected</c:if>><c:out
										value="${listValue.categoryName}" /></option>
							</c:forEach>
						</c:if>
                        </select>
                    </div>
                    	<div class="input-field col s12 m4 l4">
                        <i class="material-icons prefix">play_for_work</i>
                          <label for="unitId" class="active"><span class="red-text">*</span></label>
                        <select id="unitId" name="unitType" required>
                                 <option value="0" selected>Choose UnitType</option>
                                 <option value="kg" >kg</option>
                                 <option value="ml" >ml</option>
                                 <option value="gms" >gms</option>
                        </select>
                    </div>
					<input id="productId" type="hidden" class="validate" name="productId" value="<c:out value="${product.productId}" />">
                    <div class="input-field col s12 m4 l4">
                        <i class="material-icons prefix">shopping_cart</i>                        
                        <input id="product" type="text" class="validate" name="productname" value="<c:out value="${product.productName}" />" required>
                        <label for="product" class="active"><span class="red-text">*</span>Enter Product Name</label>
                    </div>
                   <!--  <div class="input-field file-field  col s12 m4 l4">

                        <div class="file-path-wrapper">
                            <i class="material-icons prefix" style="margin-left:-3%;">image</i>
                            <input id="upload" type="file" name="file">
                            <input class="file-path validate" type="text" placeholder="Upload one or more files">
                        </div>

                    </div> -->
                    <div class="input-field col s12 m4 l4">
                        <i class="material-icons prefix"> rate_review</i>
                        <input id="code" type="text" class="validate" name="productcode" value="<c:out value="${product.productCode}" />">
                        <label for="code" class="active">Enter Product Code</label>
                    </div>
                    <div class="input-field col s12 m4 l4">
                        <i class="material-icons prefix">view_comfy</i>
                        <input id="threshold" type="text" class="validate" name="thresholvalue" value="<c:out value="${product.threshold}" />" required>
                        <label for="threshold" class="active"><span class="red-text">*</span>Enter Threshold Value</label>
                    </div>
                    <div class="input-field col s12 m4 l4 offset-l4 offset-m4" id="productrate">
                        <i class="fa fa-inr prefix"></i>
                        <input id="productRate" type="text" class="validate"  value="<c:out value="${product.rate}" />"  required>
                        <label for="productRate" class="active"><span class="red-text">*</span>Enter Product Rate</label>
                    </div>
                     <div class="input-field col s12 m4 l4" id="unitprice">
						 <i class="fa fa-inr prefix"></i>
                        <input id="unitRate" type="text" class="grey lighten-2"  name="productRate" readonly>
                        <label for="productRate" class="active">Product Unit Rate</label>                       
                    </div> 

					<div class="input-field col s12 m3 l3" id="cgstDiv">
                        <i class="fa fa-inr prefix"></i>
                        <input id="cgst" type="text" class="grey lighten-2" readonly>
                        <label for="cgst" class="active">CGST Amount</label>
                    </div>
                     <div class="input-field col s12 m4 l4 offset-m1 offset-l1" id="sgstDiv">
                      <i class="fa fa-inr prefix"></i>
                        <input id="sgst" type="text" class="grey lighten-2" readonly>
                        <label for="sgst" class="active">SGST Amount</label>
                    </div>
                     <div class="input-field col s12 m4 l4" id="igstDiv">
                     <i class="fa fa-inr prefix"></i>
                        <input id="igst" type="text" class="grey lighten-2"  readonly>
                        <label for="igst" class="active">IGST Amount</label>
                    </div>  
                    
                    <div class="input-field file-field  col s12 m4 l4 offset-l4 offset-m4">

                        <i class="material-icons prefix">date_range</i>
                        <fmt:formatDate pattern="yyyy-MM-dd" var="dt" value="${product.productAddedDatetime}" />                        
                        <input id="threshold" type="text" name="brand" value="<c:out value="${dt}" />" readonly>
                        <label for="threshold" class="active">Product Added Date</label>

                    </div>

                    <div class="input-field file-field  col s12 m4 l4">
                        <i class="material-icons prefix">event</i>
                         <fmt:formatDate pattern="yyyy-MM-dd" var="dt2" value="${product.productQuantityUpdatedDatetime}" />
                        
                        <input id="threshold" type="text" name="brand" value="<c:out value="${dt2}" />" readonly>
                        <label for="threshold" class="active">Last Updated Date</label>

                    </div>
                  
                  <%--   <div class="input-field col s12 m5 l5 push-l5 push-m5  " id="productrate">
                        <input id="productTax" type="text" name="productTax" value="<c:out value="${product.tax}" />"  readonly>
                        <label for="productTax" class="active">Product Tax</label>
                    </div>
                    <br>
                    <div class="input-field col s12 m5 l5 push-l5 push-m5  " id="productrate">
                        <input id="productRateWithTax" type="text" name="productRateWithTax"  value="<c:out value="${product.rateWithTax}" />" readonly>
                        <label for="productRateWithTax" class="active">Product Rate With Tax</label>
                    </div> --%>

                    

                    
                    <div class="input-field col s12 m4 l4 offset-l4  offset-m4 center-align">
                        <button class="btn waves-effect waves-light blue-gradient" id="updateProductSubmit" type="submit">Update<i class="material-icons right">send</i> </button>
                        <br><br>
                    </div>

                </div>


            </form>
        </div>
        
       	<div class="row">
			<div class="col s8 m2 l2">
				<div id="addeditmsg" class="modal" style="width:40%;">
					<div class="modal-content">
						<h5 id="msgHead">Error</h5>
						<hr>	
						<h6 id="msg"></h6>
					</div>
					<div class="col s8 m2 l2 offset-l5 modal-footer">
						<a href="#!"
							class="modal-action modal-close waves-effect btn blue-gradient">OK</a>
					</div>
				</div>
			</div>
		</div>
    </main>
    <!--content end-->
</body>

</html>