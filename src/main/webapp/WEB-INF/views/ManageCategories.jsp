<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%> 
<html>

<head>
    <%@include file="components/header_imports.jsp" %>

<script type="text/javascript">

 $(document).ready(function(){
	 
	 var msg="${saveMsg}";
	 //alert(msg);
	 if(msg!='' && msg!=undefined)
	 {
	     $('#addeditmsg').modal('open');
	     $('#msgHead').text("Category Message");
	     $('#msg').text(msg);
	 }
	 
	
	
	$('#per0').click(function(){
		
		$('#cgst').val(0);
		$('#igst').val(0);
		$('#sgst').val(0);
		
		$('#cgst').change();
		$('#igst').change();
		$('#sgst').change();
	});
	$('#per5').click(function(){
			
			$('#cgst').val(2.5);
			$('#igst').val(5);
			$('#sgst').val(2.5);
			
			$('#cgst').change();
			$('#igst').change();
			$('#sgst').change();
			
		});
	$('#per12').click(function(){
		
		$('#cgst').val(6);
		$('#igst').val(12);
		$('#sgst').val(6);
		
		$('#cgst').change();
		$('#igst').change();
		$('#sgst').change();
		
	});
	$('#per18').click(function(){
		
		$('#cgst').val(9);
		$('#igst').val(18);
		$('#sgst').val(9);
		
		$('#cgst').change();
		$('#igst').change();
		$('#sgst').change();
		
	});
	$('#per28').click(function(){
		
		$('#cgst').val(14);
		$('#igst').val(28);
		$('#sgst').val(14);
		
		$('#cgst').change();
		$('#igst').change();
		$('#sgst').change();
		
	});
	
	$('#addCategoryButton').click(function(){
		$('#categoryName').val('');	
		$('#categoryId').val('0');
		$('#hsnCode').val('');
		$('#categoryName').change();	
		$('#categoryId').change();	
		$('#hsnCode').change();	
		$('#cgst').val(0);
		$('#igst').val(0);
		$('#sgst').val(0);
		$('#per0').click();
		$("#saveCategorySubmit").html('<i class="material-icons left">add</i> Add');
	});
	
	$('#per0').click();
});
 
function editcategory(id) {
	//alert("${pageContext.request.contextPath}/fetchCategories?categoriesId="+id);
	$.ajax({
				type : "GET",
				url : "${pageContext.request.contextPath}/fetchCategories?categoriesId="+id,
				/* data: "id=" + id + "&name=" + name, */
				beforeSend: function() {
						$('.preloader-background').show();
						$('.preloader-wrapper').show();
			           },
				success : function(data) {
					var category = data;
					//alert(brand.name +" "+ brand.brandId);
					$('#categoryName').val(category.categoryName);	
					$('#categoryId').val(category.categoryId);
					$('#categoryName').change();
					$('#categoryId').change();
					
					$('#igst').val(category.igst);
					$('#igst').focus();
					$('#sgst').val(category.sgst);
					$('#sgst').focus();
					$('#cgst').val(category.cgst);
					$('#cgst').focus();
					
					if(parseFloat(category.igst)==0)
					{
						$('#per0').click();
					}
					else if(parseFloat(category.igst)==5)
					{
						$('#per5').click();
					}
					else if(parseFloat(category.igst)==12)
					{
						$('#per12').click();
					}
					else if(parseFloat(category.igst)==18)
					{
						$('#per18').click();
					}
					else if(parseFloat(category.igst)==28)
					{
						$('#per28').click(); 
					}
					$('#hsnCode').val(category.hsnCode);
					$('#hsnCode').focus();
					$('#hsnCode').change();
					$('#categoryName').focus();
					$('#addCategory').modal('open');
					
						$("#saveCategorySubmit").html('<i class="material-icons left">send</i>Update');
						
						$('.preloader-wrapper').hide();
						$('.preloader-background').hide();
					},
					error: function(xhr, status, error) {
						$('.preloader-wrapper').hide();
						$('.preloader-background').hide();
						  //alert(error +"---"+ xhr+"---"+status);
						$('#addeditmsg').modal('open');
	           	     	$('#msgHead').text("Message : ");
	           	     	$('#msg').text("Something Went Wrong"); 
	           	     		setTimeout(function() 
							  {
	  	     					$('#addeditmsg').modal('close');
							  }, 1000);
						}						
			
			});
	
	
}


</script>

</head>

<body>
<body>
   <!--navbar start-->
   	<%@include file="components/navbar.jsp" %>
    <!--navbar end-->
    <!--content start-->
    <main>
       
     <div class="row">
       	<div class="col s12 m3 l3 right">
       		 <a class="btn waves-effect waves-light blue-gradient modal-trigger right" href="#addCategory" id="addCategoryButton" type="button"  name="action"><i class="left material-icons">add</i>Add Category</a>
       	</div>
       	
       	<div class="col s12 m12 l12">
       	<div id="addCategory" class="modal" style="width:40%;">
       		<form  action="${pageContext.servletContext.contextPath}/saveCategories" method="post" id="saveCategoryForm">
					<div class="modal-content">
						<h5 class="center-align">Add Category</h5>
						<hr>
	
          			  <input id="categoryId" type="hidden" name="categoriesId" value="0">
           			 <!--Add New Area-->
           
		                <div class="input-field col s10 l6 m6">
		                 <label for="categoryName" class="active"><span class="red-text">*</span>Enter Category Name</label>
		                    <input id="categoryName" type="text" name="categoryName" required style="margin-bottom:0;">
		                 
		                </div>
		                <div class="input-field col s12 m4 l6">
		                <label for="hsnCode" class="active"><span class="red-text">*</span>Enter HSN Code</label>
		                    <input id="hsnCode" type="text" name="hsnCode" required style="margin-bottom:0;">		                   
		                </div>
                
		                <div class="col s12 m4 l10 offset-l2">
		                	<br><br>
		              	  <input name="group1" type="radio" id="per0" required/>
		     				 <label for="per0">0%</label>
		     				 <input name="group1" type="radio" id="per5" required/>
		     				 <label for="per5">5%</label>
		     				 <input name="group1" type="radio" id="per12" required/>
		     				 <label for="per12">12%</label>
		     				 <input name="group1" type="radio" id="per18" required/>
		     				 <label for="per18">18%</label>
		     				 <input name="group1" type="radio" id="per28" required/>
		     				 <label for="per28">28%</label>
		     				 
		     				 <br> <br>
		                </div>
	                  
	                   		 <input id="cgst" type="hidden" name="cgst" class="grey lighten-2" readonly style="height:2rem;">
	                   
	               	
		              
		                    <input id="sgst" type="hidden" name="sgst"  class="grey lighten-2" readonly style="height:2rem;">
		                   
		                
			              
			                    <input id="igst" type="hidden" name="igst" class="grey lighten-2" readonly style="height:2rem;">
			                   
			              
                
                
           
           		</div>
         
             <div class=" modal-footer">
					<div class="col s8 m2 l4 offset-l3">
					 <button class="modal-action btn waves-effect waves-light blue-gradient" type="submit" id="saveCategorySubmit"name="action" style="margin-top:15%"><i class="material-icons left" >add</i>Add</button>
					
				</div>
					</div>
						</form>  
				</div>
       	
       	</div>
   <%--      <form  action="${pageContext.servletContext.contextPath}/saveCategories" method="post" id="saveCategoryForm">
            <input id="categoryId" type="hidden" name="categoriesId" value="0">
            <!--Add New Area-->
            <div class="row" style="margin-left:1%;margin-bottom:5px;">
                <div class="input-field col s10 l2 m6"  >
                 <label for="categoryName" class="active">Enter Category Name</label>
                    <input id="categoryName" type="text" name="categoryName" required style="margin-bottom:0;">
                 
                </div>
                <div class="input-field col s12 m4 l2"  >
                <label for="hsnCode" class="active">Enter HSN Code</label>
                    <input id="hsnCode" type="text" name="hsnCode" required style="margin-bottom:0;">
                   
                </div>
                
                <div class="col s12 m4 l4" style="margin-top:3%;">
              	  <input name="group1" type="radio" id="per0" required/>
     				 <label for="per0">0%</label>
     				 <input name="group1" type="radio" id="per5" required/>
     				 <label for="per5">5%</label>
     				 <input name="group1" type="radio" id="per12" required/>
     				 <label for="per12">12%</label>
     				 <input name="group1" type="radio" id="per18" required/>
     				 <label for="per18">18%</label>
     				 <input name="group1" type="radio" id="per28" required/>
     				 <label for="per28">28%</label>
                </div>
                <div class="col s10 l2 m6" style="width:14%;">
                    <button class="btn waves-effect waves-light blue-gradient" type="submit" id="saveCategorySubmit"name="action" style="margin-top:15%"><i class="material-icons left" >add</i>Add</button>
                </div>
                <div class="col s10 l2 m6" style="margin-top:-2px;">
                    <button class="btn waves-effect waves-light blue-gradient" type="button" id="resetCategorySubmit" name="action" style="margin-top:15%"><i class="left material-icons">refresh</i>Reset</button>
                    
                </div>
           </div>
           <div class="row" style="margin-left:1%;margin-bottom:5px;"> 
                <div class="input-field col s12 m4 l1">
          	 <label for="cgst" class="active">% CGST </label>
                    <input id="cgst" type="text" name="cgst" class="grey lighten-2" readonly style="height:2rem;">
                   
                </div>
                <div class="input-field col s12 m4 l1">
                 <label for="sgst" class="active">% SGST </label>
                    <input id="sgst" type="text" name="sgst"  class="grey lighten-2" readonly style="height:2rem;">
                   
                </div>
                <div class="input-field col s12 m4 l1 ">
                 <label for="igst" class="active"> % IGST</label>
                    <input id="igst" type="text" name="igst" class="grey lighten-2" readonly style="height:2rem;">
                   
                </div>

                </div>
            	

    </form>   --%>  
    
	<div class="col s12 m12 l12">

            <table class="striped highlight centered" id="tblData" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th class="print-col">Sr.No.</th>
                        <th class="print-col">Category Name</th>
                        <th class="print-col">HSN</th>
                        <th class="print-col">% CGST</th>
                        <th class="print-col">% SGST</th>
                        <th class="print-col" >% IGST</th>
                        <th class="print-col">Date</th>
                        <th>Edit</th>
                        <!-- <th>Delete</th> -->

                    </tr>
                </thead>

                <tbody>
                     <% int rowincrement=0; %>
                     <c:if test="${not empty categorieslist}">
							<c:forEach var="listValue" items="${categorieslist}">
						
						<c:set var="rowincrement" value="${rowincrement + 1}" scope="page"/>
						<tr>
                        <td><c:out value="${rowincrement}" /></td>
                        <td><c:out value="${listValue.categoryName}" /></td>
                       <td><c:out value="${listValue.hsnCode}" /></td>
                       <td><c:out value="${listValue.cgst}" /></td>
                       <td><c:out value="${listValue.sgst}" /></td>
                       <td><c:out value="${listValue.igst}" /></td>
                       <td>
                       			<fmt:formatDate pattern="dd-MM-yyyy" var="dt" value="${listValue.categoryDate}" />
		                        <c:out value="${dt}" /> & 
		                        <fmt:formatDate pattern="HH:mm:ss" var="time" value="${listValue.categoryDate}" />
		                         <c:out value="${time}" />
                       </td>
                        <td><button class=" btn-flat " type="button" class="brandedit" onclick='editcategory(${listValue.categoryId})'><i class="material-icons tooltipped blue-text " data-position="right" data-delay="50" data-tooltip="Edit" >edit</i></button></td>
                        <!-- Modal Trigger -->
                        <!-- <td><a href="#delete" class="modal-trigger"><i class="material-icons tooltipped" data-position="right" data-delay="50" data-tooltip="Delete">delete</i></a></td> -->
                    </tr>
                    </c:forEach>
                    </c:if>
                    
                    <!-- Modal Structure for delete -->

                   <!--  <div id="delete" class="modal">
                        <div class="modal-content">
                            <h4>Confirmation</h4>
                            <p>Are you sure you want to delete?</p>
                        </div>
                        <div class="modal-footer">
                            <a href="#!" class="modal-action modal-close waves-effect  btn-flat ">Delete</a>
                            <a href="#!" class="modal-action modal-close waves-effect btn-flat ">Close</a>
                        </div>
                    </div> -->
                </tbody>
            </table>
</div>
</div>
       	 <div class="row">
			<div class="col s12 m12 l12">
				<div id="addeditmsg" class="modal" style="width:40%;">
					<div class="modal-content">
						<h5 id="msgHead"></h5>
						<hr>
						<h6 id="msg"></h6>
					</div>
					<div class="col s8 m2 l2 offset-l5 modal-footer">
						<a href="#!"
							class="modal-action modal-close waves-effect waves-green btn blue-gradient">OK</a>
					</div>
				</div>
			</div>
		</div>
    </main>
    <!--content end-->
</body>

</html>