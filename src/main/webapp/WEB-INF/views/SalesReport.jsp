<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<html>

<head>
      <%@include file="components/header_imports.jsp" %>
    
    <script type="text/javascript">
    
    $(document).ready(function(){
    	 $(".showDates").hide();
         $(".rangeSelect").click(function() {
        	 $("#oneDateDiv").hide();
             $(".showDates").show();
         });
    	$("#oneDateDiv").hide();
    	$(".pickdate").click(function(){
    		 $(".showDates").hide();
    		$("#oneDateDiv").show();
    	});
    	
    	
    });
    
    
    </script>
     <style>
      .card-image{
		width:40% !important;
		background-color:#01579b !important;
	}
    </style>
</head>

<body>
    <!--navbar start-->
   	<%@include file="components/navbar.jsp" %>
    <!--navbar end-->
    <!--content start-->
    <main>
    
        <div class="row">
           
            <div class="col s12 l3 m3">
      		  <div class="card horizontal">
      		  	<div class="card-image">
       				  <h6 class="white-text" style="padding:8px">Total Amount:</h6>
      			</div>
      			<div class="card-stacked grey lighten-3">
      			  <div class="card-content" style="padding:8px">
       			<h6 class="">&#8377;<span id="totalAmountId"><c:out value="${totalAmount}" /></span></h6>
       			  </div>
        	    </div>
                
          	  </div>
           </div>  
             <div class="col s12 l3 m3">
             	 <div class="card horizontal">
      		  	<div class="card-image">
       				  <h6 class="white-text" style="padding:8px">No of Order:</h6>
      			</div>
      			<div class="card-stacked grey lighten-3">
      			  <div class="card-content" style="padding:8px">
       			<h6 class=""><span id="totalAmountId"><c:out value="${noOfOrders}" /></span></h6>
       			  </div>
        	    </div>
                
          	  </div>
                
            </div>
            <div class="col s12 m4 l4 right-align right" style="margin-top:1%;">
                <div class="col s6 m4 l4 right">
                    <!-- Dropdown Trigger -->
                    <a class='dropdown-button btn waves-effect waves-light light-blue darken-4' href='#' data-activates='filter'>Filter<i
                class="material-icons right">arrow_drop_down</i></a>
                    <!-- Dropdown Structure -->
                    <ul id='filter' class='dropdown-content'>
                    	<li><a href="${pageContext.request.contextPath}/salesReport?range=yesterday">Yesterday</a></li>
                        <li><a href="${pageContext.request.contextPath}/salesReport?range=last7days">Last 7 Days</a></li>
                        <li><a href="${pageContext.request.contextPath}/salesReport?range=last1month">Last 1 Month</a></li>
                        <li><a href="${pageContext.request.contextPath}/salesReport?range=last3months">Last 3 Month</a></li>
                        <li><a class="rangeSelect">Range</a></li>
                        <li><a class="pickdate">Pick date</a></li>                        
                        <li><a href="${pageContext.request.contextPath}/salesReport?range=viewAll">View All</a></li>
                    </ul>
                </div>
                	<div class="col s12 l8	m8 right"> 
                 <form action="${pageContext.request.contextPath}/salesReport" method="post">
                    <input type="hidden" name="range" value="range">
                     <span class="showDates">
                     			 <div class="input-field col s6 m2 l2 right">
                            <button type="submit">View</button>
                            </div>
                             
                              <div class="input-field col s6 m5 l5 right">
                                    <input type="date" class="datepicker" placeholder="Choose Date" name="endDate" id="endDate">
                                     <label for="endDate">To</label>
                               </div>
                                <div class="input-field col s6 m5 l5 right">
                                <input type="date" class="datepicker" placeholder="Choose Date" name="startDate" id="startDate" required> 
                                <label for="startDate">From</label>
                                 </div>
                          </span>
                </form>
                 </div>
				<div class="col s12 l6	m6 right" id="oneDateDiv">                            	
                    <form action="${pageContext.request.contextPath}/salesReport" method="post">
                    <input type="hidden" name="range" value="pickdate">
	                    <div class="input-field col s12 m10 l10">
		                    <input type="text" id="oneDate" class="datepicker" placeholder="Choose date" name="startDate">
		                    <label for="oneDate" class="black-text">Pick Date</label>
	                    </div>
	                    <div class="input-field col s12 m2 l2">
	                     <button type="submit">View</button>
                       
                        </div>
                    </form>
               </div>
               
               
                
            </div>
        </div>

        <div class="row">
            <div class="col s12 l12 m12 ">
                <table class="striped highlight bordered centered " id="tblData" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th class="print-col">Sr.No</th>
                            <th class="print-col">Order Id</th>
                            <th class="print-col">Booth No</th>
                            <th class="print-col">Owner Name</th>
                            <th class="print-col">Route No</th>
                            <th class="print-col">Salesman Name</th>
                            <th class="print-col">Amount Of Business</th>
                            <th class="print-col">Date of Order</th>
                            <th class="print-col">Order Delivered date</th>
                        </tr>
                    </thead>

                    <tbody>
	                    <c:if test="${not empty salesManReportModelList}">
						<c:forEach var="listValue" items="${salesManReportModelList}">
	                        <tr>
	                            <td><c:out value="${listValue.srno}"/></td>
	                            <td><a href="#"><c:out value="${listValue.orderId}"/></a></td>
	                            <td><c:out value="${listValue.boothNo}"/></td>
	                            <td><c:out value="${listValue.ownerName}"/></td>
	                            <td><a href="${pageContext.servletContext.contextPath}/getEmployeeView?employeeDetailsId=${listValue.employeeDetailsId}"><c:out value="${listValue.routeNo}"/></a></td>
	                            <td>
		                           <a title="View Salesman Details" href="${pageContext.servletContext.contextPath}/getEmployeeView?employeeDetailsId=${listValue.employeeDetailsId}"><c:out value="${listValue.smName}"/></a>
	                            </td>
	                            <td><c:out value="${listValue.orderAmount}"/></td>
	                            <td><c:out value="${listValue.dateOfOrder}"/></td>
	                            <td><c:out value="${listValue.dateOfDeliver}"/></td>
	                        </tr>
	                     </c:forEach>
	                     </c:if>
                    </tbody>
                </table>
            </div>
        </div>

		<!-- Modal Structure for Salesman Details -->
        <div id="salesmanDetails" class="modal">
            <div class="modal-content">
                <h4 class="center">Salesman Details</h4>
                <hr>
                <table border="2" class="centered tblborder">
                    <thead>
                        <tr>
                            <th>Sr.No</th>
                            <th>Route No</th>
                            <th>Area Name</th>
                            <th>Cluster</th>
                            <th>Salesman Name</th>
                            <th>Mobile No</th>
                            <th>Area Sales Manager</th>
                            <th>ASM-Mobile No.</th>

                        </tr>
                    </thead>
                    <tbody id="areaList">
                        
                    </tbody>
                </table>
            </div>
            <div class="modal-footer row">
				<div class="col s6 m6 l7">
                <a href="#!" class="modal-action modal-close waves-effect btn grey-text  text-lighten-3 light-blue darken-4">Ok</a>
                </div>
            </div>
        </div>

    </main>
    <!--content end-->
</body>

</html>