<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<html>

<head>
      <%@include file="components/header_imports.jsp" %>


<script type="text/javascript">
var checkedId=[];
$(document).ready(function() {
	$("#checkAll").click(function () {
		var colcount=$('#tblData').DataTable().columns().header().length;
	    var cells = $('#tblData').DataTable().column(colcount-1).nodes(), // Cells from 1st column
	        state = this.checked;
			//alert(state);
	    for (var i = 0; i < cells.length; i += 1) {
	        cells[i].querySelector("input[type='checkbox']").checked = state;
	    }		                
	    
	});
	
	 $("input:checkbox").change(function(a){
		 
		 var colcount=$('#tblData').DataTable().columns().header().length;
	     var cells = $('#tblData').DataTable().column(colcount-1).nodes(), // Cells from 1st column
	         
	      state = false; 				
	     var ch=false;
	     
	     for (var i = 0; i < cells.length; i += 1) {
	    	 //alert(cells[i].querySelector("input[type='checkbox']").checked);
	    	
	         if(cells[i].querySelector("input[type='checkbox']").checked == state)
	         { 
	        	 $("#checkAll").prop('checked', false);
	        	 ch=true;
	         }                      
	     }
	     if(ch==false)
	     {
	    	 $("#checkAll").prop('checked', true);
	     }
	    	
	    //alert($('#tblData').DataTable().rows().count());
	});
	 
	 $('#sendsmsid').click(function(){
			var count=parseInt('${count}')
			checkedId=[];
			//chb
			 var idarray = $("#employeeTblData")
          .find("input[type=checkbox]") 
          .map(function() { return this.id; }) 
          .get();
			 var j=0;
			 for(var i=0; i<idarray.length; i++)
			 {								  
				 idarray[i]=idarray[i].replace('chb','');
				 if($('#chb'+idarray[i]).is(':checked'))
				 {
					 checkedId[j]=idarray[i];
					 j++;
				 }
			 }
			 $('#smsText').val('');
			 $('#smsSendMesssage').html('');
			 
			 if(checkedId.length==0)
			 {
				 $('#addeditmsg').modal('open');
			     $('#msgHead').text("Message : ");
			     $('#msg').text("Select Business For Send SMS");
			 }
			 else
			 {
				 $('#smsText').val('');
				 $('#smsSendMesssage').html('');
				 $('#sendMsg').modal('open');
			 }
		});
		
		$('#sendMessageId').click(function(){
			
			
			 $('#smsSendMesssage').html('');
			var smsText=$("#smsText").val();
			if(checkedId.length==0)
			{
				$('#smsSendMesssage').html("<font color='red'>Select Employee For Send SMS</font>");
				return false;
			}
			if(smsText==="")
			{
				$('#smsSendMesssage').html("<font color='red'>Enter SMS Text</font>");
				return false;
			}
			var form = $('#sendSMSForm');
			//alert(form.serialize()+"&employeeDetailsId="+checkedId);
			
			$.ajax({
					type : form.attr('method'),
					url : form.attr('action'),
					data : form.serialize()+"&shopsIds="+checkedId,
					//async: false,
					success : function(data) {
						if(data==="Success")
						{
							$('#smsSendMesssage').html("<font color='green'>SMS send SuccessFully</font>");
							$('#smsText').val('');
						}
						else
						{
							$('#smsSendMesssage').html("<font color='red'>SMS sending Failed</font>");
						}
					}
			});
		});
	 
});
</script>
<style>
table.dataTable tbody td {
    padding: 0 2px !important;
}
	.dataTables_wrapper {
 
    margin-left: 2px !important;
}
</style>

</head>

<body>
   <!--navbar start-->
   	<%@include file="components/navbar.jsp" %>
    <!--navbar end-->
    <!--content start-->
    <main>
        <div class="row">
        
        <!--Add New Supplier-->
         
        <div class="col s10 l2 m6 right" >
            <a class="btn waves-effect waves-light blue-gradient right" href="${pageContext.servletContext.contextPath}/addBoothPage"><i class="material-icons left" >add</i>Add New Booth</a>
        </div>
     
        <div class="col s12 l12 m12">
         
            <table class="striped highlight centered mdl-data-table display " id="tblData" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th class="print-col">Sr.No.</th>
                        <th class="print-col">Booth No.</th>
                         <th class="print-col">Shop Name</th>
                        <th class="print-col">Owner Name</th>
                        <th class="print-col">Mobile No.</th>
                        <th class="print-col">Email-Id</th> 
                        <th class="print-col">GST No</th>
                        <th class="print-col">Address</th>
                     	<th class="print-col">Route No</th>
                        <th class="print-col">Area</th>
                        <th class="print-col">Cluster</th>
                        <!-- <th class="print-col">City</th> -->
                       <!--  <th class="print-col">State</th>
                        <th class="print-col">Country</th> -->
                        <!-- <th class="print-col">Credit Limit</th> -->
                        <th class="print-col">Balance Amount</th>
                        <th>Edit</th>
                        <th>Delete</th>
                        <th> <a href="#" class="modal-trigger" id="sendsmsid"> SMS  </a>
                            <input type="checkbox" class="filled-in checkAll" id="checkAll" />
                            <label for="checkAll"></label>
                        </th>
                    </tr>
                </thead>

                <tbody id="employeeTblData">
              <% int rowincrement=0; %>
                   <c:if test="${not empty boothList}">
					<c:forEach var="listValue" items="${boothList}">					
						<c:set var="rowincrement" value="${rowincrement + 1}" scope="page"/>
                    <tr ${listValue.booth.status == true ? 'class="red lighten-4"' : ''}>
                        <td><c:out value="${rowincrement}" /></td>
                        <td><c:out value="${listValue.booth.boothNo}" /></td>
                        <td><c:out value="${listValue.booth.shopName}" /></td>
                        <td><c:out value="${listValue.booth.ownerName}" /></td>                        
                        <td><c:out value="${listValue.booth.contact.mobileNumber}" /></td>
                        <td><c:out value="${listValue.booth.contact.emailId}" /></td>
                        
                        <c:choose>                          
						    <c:when test="${listValue.booth.gstNo == ''}">  
						        <td>---</td>
						    </c:when>  
						    <c:otherwise>  
						       <td><c:out value="${listValue.booth.gstNo}" /></td> 
						    </c:otherwise>  
						</c:choose>  
                        
                        <td><c:out value="${listValue.booth.address}" /></td>
                        <td><c:out value="${listValue.booth.route.routeNo}" /></td>
                        <td><c:out value="${listValue.booth.route.area.name}" /></td>
                        <td><c:out value="${listValue.booth.route.area.cluster.name}" /></td>
                        <td>
                        <fmt:formatNumber var="amount" value="${listValue.balanceAmount}" maxFractionDigits="2" minFractionDigits="2" />
                        <c:out value="${amount}" />
                        </td>
                        <%-- <td><c:out value="${businessNameList[i].businessName.area.region.city.state.name}" /></td>
                        <td><c:out value="${businessNameList[i].businessName.area.region.city.state.country.name}" /></td> --%>
                       
                        <td><a class=" btn-flat" ${listValue.booth.status == true ? 'disabled' : ''} href="${pageContext.servletContext.contextPath}/fetchBooth?boothId=${listValue.booth.boothId}"><i class="material-icons tooltipped blue-text " data-position="right" data-delay="50" data-tooltip="Edit" >edit</i></a></td>
                        <!-- Modal Trigger -->
                        <td><a href="#delete${listValue.booth.boothId}" class="modal-trigger"><i class="material-icons tooltipped" data-position="right" data-delay="50" data-tooltip="Delete">delete</i></a></td>
                        <td>
                            <input type="checkbox" class="filled-in" id="chb${listValue.booth.boothId}" />
                            <label for="chb${listValue.booth.boothId}"></label>
                        </td>
                    </tr>
                    	 <!-- Modal Structure for delete -->

                     <div id="delete${listValue.booth.boothId}" class="modal row" style="width:40%">
                        <div class="modal-content col s12 m12 l12">
                            <h4>Confirmation</h4>
                            <p>${listValue.booth.status == true ? 'Are you sure you want to Enable?' : 'Are you sure you want to Disable?'}</p>
                        </div>
                        <div class="modal-footer col s12 m12 l12">
                        <div class="divider"></div>
                        <div class="col s6 m3 l3 offset-l3">
                            <a href="${pageContext.request.contextPath}/disableBooth?boothId=${listValue.booth.boothId}" class="modal-action modal-close waves-effect  btn teal">${listValue.booth.status == true ? 'Enable' : 'Disable'}</a>
                            </div>
                            <div class="col s6 m3 l3">
                            <a href="#!" class="modal-action modal-close waves-effect btn red">Close</a>
                            </div>
                        </div>
                    </div> 
				</c:forEach>
				</c:if>


                                     
                    
                    
                    
                    <!-- Modal Structure for sendMsg -->

                    <div id="sendMsg" class="modal row modal-fixed-footer" style="width:40%;height:60%;">
                    <form id="sendSMSForm" action="${pageContext.request.contextPath}/sendSMSTOShops" method="post">
                        <div class="modal-content">
                            <h4 class="center">Message</h4>
                            <hr>	
                          
                                <div class="col s12 l7 m7 push-l3">
                                    <label for="smsText" class="black-text">Type Message</label>
                                    <textarea id="smsText" class="materialize-textarea" name="smsText"></textarea>
                                </div> 
                                <div class="input-field col s12 l7 push-l4 m5 push-m4 ">                                
                   			 		 <font color='red'><span id="smsSendMesssage"></span></font>
            					</div>                            
                           
                               				
                        </div>
                        <div class="modal-footer center-align">
                            <div class="col s6 m6 l6">
                            		 <button type="button" id="sendMessageId" class="modal-action waves-effect btn btn-waves">Send</button>
                             </div>
                          	<div class="col s6 m6 l4">	
                           			 <a href="#!" class="modal-action modal-close waves-effect  btn red">Cancel</a>
                             </div>
                            
                          
                        </div>
                        </form>
                    </div>
                </tbody>
            </table>
        </div>
        </div>
        <!-- Modal Structure for View Product Details -->

        <div id="viewDetails" class="modal">
            <div class="modal-content">
                <h4>Product Details</h4>
                <table border="2" class="tblborder centered">
                    <thead>
                        <tr>
                            <th>Sr.No</th>
                            <th>Product Name</th>
                            <th>Category</th>
                            <th>Brand</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>1</td>
                            <td>abc</td>
                            <td>abc</td>
                            <td>abc</td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div class="modal-footer">

                <a href="#!" class="modal-action modal-close waves-effect btn-flat ">Close</a>
            </div>
        </div>
<div class="row">
			<div class="col s8 m2 l2">
				<div id="addeditmsg" class="modal" style="width:40%;">
					<div class="modal-content">
						<h5 id="msgHead">Error</h5>
						<hr>	
						<h6 id="msg"></h6>
					</div>
					<div class="col s8 m2 l2 offset-l5 modal-footer">
						<a href="#!"
							class="modal-action modal-close waves-effect btn blue-gradient">OK</a>
					</div>
				</div>
			</div>
		</div>
    </main>
    <!--content end-->
</body>

</html>