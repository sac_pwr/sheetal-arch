<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<html>

<head>

    <%@include file="components/header_imports.jsp" %>
	<script>var myContextPath = "${pageContext.request.contextPath}"</script>
	<script type="text/javascript" src="resources/js/employeeview.js"></script>
	
	<!-- for find date different -->
	<script src="resources/js/moment.min.js"></script>
	<!-- 
	<script src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.3/moment.js"></script>
	<script>
	    if (!window.moment) { 
	        document.write('<script src="assets/plugins/moment/moment.min.js"><\/script>');
	    }
	</script>
	 -->
	
	<style>
	.btn-flat:focus,
.btn-flat:hover {
    background-color: transparent !important;
    box-shadow: none;
}
	table.dataTable tbody td {
    padding: 0 2px !important;
}
	/* .dataTables_wrapper {
 
    margin-left: 2px !important;
} */
#incentive .input-field{
	margin-top:0 !important;
}
#incentive{
			width:40%;
}
td.wrapok {
    white-space:nowrap !important;
}

	/* .datepicker table,
	.datepicker td,
	.datepicker th{
	border: none !important; 
     font-size: 13px; 
	}  */
	</style>

    <script>
        $(document).ready(function() {
            $("#BankDetails").css("display", "none");
            $(".cash").change(function() {
                $("#BankDetails").css("display", "none");
            });
            $(".cheque").change(function() {
                $("#BankDetails").css("display", "block");
            });
            $(".showDates1").hide();
            $(".rangeSelect1").click(function() {
                $(".showDates1").show();
            });
            
            
         /* // Use "DataTable" with upper "D"
            oTableStaticFlow = $('#tblData').DataTable({
                "aoColumnDefs": [{
                    'bSortable': true,
                    'aTargets': [0]
                }],
            });

            $("#checkAll").click(function () {
            	var colcount=oTableStaticFlow.columns().header().length;
                var cells = oTableStaticFlow.column(colcount-1).nodes(), // Cells from 1st column
                    state = this.checked;
				
                for (var i = 0; i < cells.length; i += 1) {
                    cells[i].querySelector("input[type='checkbox']").checked = state;
                }
            }); */

        });
    </script>



</head>

<body>
   <!--navbar start-->
   	<%@include file="components/navbar.jsp" %>
    <!--navbar end-->
    <!--content start-->
    <main>
     
        <div class="row">
       
            <!--Add Employee-->
            <div class="col s10 l3 m6 right">
                <a class="btn waves-effect waves-light blue-gradient right" href="${pageContext.request.contextPath}/addEmployee"><i class="material-icons left" >add</i>Add Employee</a>
            </div>
            <!--Add Business Type-->
            <!-- <div class="col s10 l4 m6 offset-s2 ">
                <a class="btn waves-effect waves-light blue darken-blue-gradient center" href="AddBusinessType.html"><i class="material-icons left" >add</i>Add Business Type</a>
            </div> -->
            <!--Add Department-->
        <!--  <div class="col s10 l4 m6  right">
                <a class="btn waves-effect waves-light blue-gradient right modal-trigger" href="#viewHoliday"><i class="material-icons left" >add</i>test</a>
            </div> -->
        


        <div class="col s12 l12 m12">       
            <table class="striped highlight centered" id="tblData" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th class="print-col">Sr. No.</th>
                        <th class="print-col">Employee Id</th>
                        <th class="print-col">Name</th>
                        <th class="print-col">Mobile No.</th>
                      <!--   <th class="print-col">EmailId</th> -->
                        <th class="print-col" >Total Amount</th>
                        <th class="print-col">Paid Amount</th>
                        <th class="print-col">Balance Amount</th>
                         <th class="print-col">No Of Holidays</th>
                       <!--  <th class="print-col">Incentives Amount</th> -->
                        <th class="print-col">Give Incentive</th> 
                       
                        <th class="print-col">Department</th>
                        <th class="print-col">Area</th>
                        <th class="print-col">Marks as Holiday</th>
                        <th class="print-col">Make Payment</th>
                        <th>Edit</th>
                        <th>Disable</th>
                        <th> <a href="#" class="modal-trigger" id="sendsmsid">SMS</a><br>
                            <input type="checkbox" class="filled-in checkAll" id="checkAll" />
                            <label for="checkAll"></label>
                        </th>
                    </tr>
                </thead>

                <tbody id="employeeTblData">
                 <c:if test="${not empty employeeViewModelList}">
							<c:forEach var="listValue" items="${employeeViewModelList}">													
                    <tr ${listValue.status == true ? 'class="red lighten-4"' : ''}>
                        <td><c:out value="${listValue.srno}"/></td>
                        <td><c:out value="${listValue.employeeId}"/></td>
                        <td class="wrapok"><c:out value="${listValue.name}"/></td>
                        <td><c:out value="${listValue.mobileNumber}"/></td>
                       <%--  <td><c:out value="${listValue.email}"/></td>   --%>
                        <td>
	                        <%-- <button type="button" class=" btn-flat blue-text" onclick="showSalaryDetail('${listValue.employeeId}')"  id="empDetails${listValue.srno}"> --%>
	                        <a href="${pageContext.request.contextPath}/viewEmployeeSalary?employeeDetailsId=${listValue.employeePkId}">
	                        <fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${listValue.totalAmount}" />
	                        </a>
	                        <!-- </button> -->
                        </td>
                        <td><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${listValue.paidAmount}" /></td>
                        <td><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${listValue.unPaidAmount}" /></td>
                        <%-- <td><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${listValue.incentives}" /></td> --%>
                      	 <td>
                            <button type="button" class=" btn-flat blue-text" onclick="showHolidayDetail('${listValue.employeePkId}')"  id="showholidayDetails${listValue.srno}"><c:out value="${listValue.noOfHolidays}"/></button>
                        </td>
                        <td>
                            <button type="button" class=" btn-flat "  ${listValue.status == true ? 'disabled="disabled"' : ''} onclick="openIncentivesModel('${listValue.employeePkId}')"  id="incentiveDetails${listValue.srno}"><i class="fa fa-inr tooltipped blue-text " data-position="right" data-delay="50" data-tooltip="Incentive"></i></button>
                        </td>
                          
                       
                        <td><c:out value="${listValue.departmentName}"/></td>
                        <td>
                            <button type="button" class="btn blue-gradient" onclick="showAreaDetail('${listValue.employeePkId}')"  style="font-size:10px;" id="areaDetails${listValue.srno}">
                            Show
                            </button>
                        </td>
                        <td>
                            <button type="button" class=" btn-flat " ${listValue.status == true ? 'disabled="disabled"' : ''} onclick="openHolidayBookModel('${listValue.employeePkId}')"  id="bookholidayDetails${listValue.srno}">
                            	<i class="fa fa-asterisk tooltipped blue-text" aria-hidden="true"  data-position="right" data-delay="50" data-tooltip="Holiday" ></i>
                            </button>
                        </td>

                        <td>
                    <%--     <button type="button" class=" btn-flat blue-text" onclick="openPaymentModel('${listValue.employeeId}')"  id="paymentDetails${listValue.srno}">
                        Pay
                        </button> --%>
                        <a type="button" class="btn blue-gradient" href="${pageContext.request.contextPath}/paymentEmployee?employeeDetailsId=${listValue.employeePkId}" >
                        Pay
                        </a>
                        </td>

					<c:choose>  
					    <c:when test="${listValue.status == true}">  
					       <td><a href="#" class=" btn-flat "><i class="material-icons tooltipped blue-text" area-hidden="true" data-position="right" data-delay="50" data-tooltip="Edit" >edit</i></a></td>
					    </c:when>  
					    <c:otherwise>  
					       <td><a href="${pageContext.request.contextPath}/fetchEmployee?employeeDetailsId=${listValue.employeePkId}" class=" btn-flat "><i class="material-icons tooltipped blue-text" area-hidden="true" data-position="right" data-delay="50" data-tooltip="Edit" >edit</i></a></td>
					    </c:otherwise>  
					</c:choose>  

                        
                        <!-- Modal Trigger -->
                        <td><a href="#delete${listValue.employeePkId}" class="modal-trigger"><i class="material-icons tooltipped" data-position="right" data-delay="50" data-tooltip="Delete">delete</i></a></td>
                        <td>
                            <input type="checkbox" class="filled-in chbclass" id="chb<c:out value="${listValue.employeePkId}"/>" />
                             <label for="chb<c:out value="${listValue.employeePkId}"/>"></label> 
                        </td>
                    </tr>
                    <!-- Modal Structure for delete -->

                    <div id="delete${listValue.employeePkId}" class="modal row" style="width:40%">
                        <div class="modal-content col s12 m12 l12">
                            <h4>Confirmation</h4>
                            <p>${listValue.status == true ? 'Are you sure you want to Enable?' : 'Are you sure you want to Disable?'}</p>
                        </div>
                        <div class="modal-footer col s12 m12 l12">
                        <div class="divider"></div>
                        <div class="col s6 m3 l3 offset-l3">
                            <a href="${pageContext.request.contextPath}/disableEmployee?employeeDetailsId=${listValue.employeePkId}" class="modal-action modal-close waves-effect  btn teal">${listValue.status == true ? 'Enable' : 'Disable'}</a>
                            </div>
                            <div class="col s6 m3 l3">
                            <a href="#!" class="modal-action modal-close waves-effect btn red ">Close</a>
                            </div>
                        </div>
                    </div>
                 </c:forEach>
                 </c:if>
                     <!-- Modal Structure for Holiday  booked-->
        <div id="holiday" class="modal modal-fixed-footer">
            <div class="modal-content">
                <div class="row">
                    <h4 class="center">Mark As Holiday</h4>
                      <hr>
                    <form action="${pageContext.request.contextPath}/bookHoildays" method="post" class="col s12 l12 m12" id="saveHoliday">
                     <input type="hidden" name="employeeDetailsId" id="employeeIdForHolidaySave" value="">
                     <input type="hidden" name="employeeHolidaysId" id="employeeHolidaysId" value="0">
                        <div class="row">
                            <div class="input-field col s12 l4 m4 ">
                                <!--<input id="image_url" type="text" class="validate">-->
                                <h6 id="holiodaydetailsname">Name:</h6>
                            </div>
                            <div class="input-field col s12 l4 m4 ">
                                <!--<input id="image_title" type="text" class="validate">-->
                                <h6 id="holiodaydetailsmobilenumber">Mobile No:</h6>
                            </div>
                            <div class="input-field col s12 l4  m4" style="margin-top:5px;">
                                <input id="holiodaydetailsnoofholidays" type="text" name="noOfDays">
                                <label for="holiodaydetailsnoofholidays" class="black-text"><span class="red-text">*</span>No of holidays</label>
                            </div>
                        </div>
                        <div class="row">                              
                            <div class="input-field col s12 l3 push-l5 m3 push-m1" id="oneDateDiv">                            	
                                <!--<input id="image_url" type="text" class="validate">-->
                                <input type="text" id="oneDate" class="datepicker " name="oneDate">
                                <label for="oneDate" class="black-text">One Date</label>
                            </div>
                           
                            <div class="input-field col s12 l3 push-l3 m3 push-m2" id="fromDateDiv">
                                <!--<input id="image_url" type="text" class="validate">-->
                                <input type="text" id="fromDate" class="datepicker " name="fromDate">
                                <label for="fromDate" class="black-text">From Date</label>
                            </div>
                            <div class="input-field col s12 l3 push-l3 m3 push-m2" id="toDateDiv">
                                <!--<input id="image_url" type="text" class="validate">-->
                                <input type="text" id="toDate" class="datepicker " name="toDate">
                                <label for="toDate" class="black-text">To Date</label>
                            </div>
                        </div>

								<div class="row">
									<div class="input-field col s12 l4"	style="margin-top: 6%;">
										<select id="paidStatusId" name="paidStatus" >
											<option value="0" selected>Select Type</option>
											<option value="paid">Paid</option>
											<option value="unpaid">Unpaid</option>
										</select> 
										<label><span class="red-text">*</span>Type Of Holiday</label>

									</div>
									<div class="input-field col s12 l8  m8"
										style="margin-top: 1%;">
										<textarea id="reason"
											class="materialize-textarea validate" name="reason"></textarea>
										<label for="reason" class="active black-text"><span class="red-text">*</span>Reason</label>
									</div>
								</div>
								<div class="input-field col s12 l5 push-l4 m5 push-m4 ">
                                
                                <font color='red'><span id="msgholidaysave"></span></font>
                        </div>
                    </form>
                </div>
            </div>
            <div class="modal-footer row">
            <div class="col s6 m6 l5">
                <button id="submitHoliday" type="button" class="modal-action waves-effect  btn  ">Submit</button>
                </div>
                <div class="col s6 m6 l4">
                <a href="#!" class="modal-action modal-close waves-effect btn red ">Close</a>
                </div>
            </div>

        </div>
        
        
                  
                    
                    
                    <!-- Modal Structure for sendMsg -->

                    <div id="sendMsg" class="modal row modal-fixed-footer">
                    <form id="sendSMSForm" action="${pageContext.request.contextPath}/sendSMS" method="post">
                        <div class="modal-content">
                            <h4 class="center">Message</h4>
                            <hr>	
                            
                                <div class="col s12 l7 m7 push-l3">
                                    <label for="smsText" class="black-text"><span class="red-text">*</span>Type Message</label>
                                    <textarea id="smsText" class="materialize-textarea" name="smsText"></textarea>
                                </div> 
                                <div class="input-field col s12 l7 push-l4 m5 push-m4 ">                                
                   			 		 <font color='red'><span id="smsSendMesssage"></span></font>
            					</div>                            
                           
                               				
                        </div>
                        <div class="modal-footer center-align">
                            <div class="col s6 m6 l6">
                            		 <button type="button" id="sendMessageId" class="modal-action waves-effect btn btn-waves  ">Send</button>
                             </div>
                          	<div class="col s6 m6 l4">	
                           			 <a href="#!" class="modal-action modal-close waves-effect  btn red ">Cancel</a>
                             </div>
                            
                          
                        </div>
                        </form>
                    </div>
                    
                </tbody>
            </table>
        </div>
        
       </div>
        
        <!--Modal Structure for payment-->
       <%--  <div id="pay" class="modal ">
            <div class="modal-content">
                <div class="row  ">
                    <form  action="${pageContext.request.contextPath}/givePayment" method="post" class="col s12 l12 m12" id="savePayment" class="col s12 l10 m8 push-l1">
                        <input name="employeeDetailsId" type="hidden" id="employeeDetailsIdId" class="cash" checked/>
                        <h4 class="center"> Payment Details </h4>
                        <hr>
                        <div class="row">
                            <div class="input-field col s12 l5 push-l2 m5 push-m1">
                                <!--<input id="image_url" type="text" class="validate">-->
                                <h6 id="paymentName">Name:</h6>
                            </div>
                            <div class="input-field col s12 l5 push-l2 m5 push-m1">
                                <!--<input id="image_title" type="text" class="validate">-->
                                <h6 id="paymenttotalAmt">Total Salary:</h6>
                            </div>
                        </div>

                        <div class="row">
                            <div class="input-field col s12 l5 push-l2 m5 push-m1">
                                <h6 id="paymentamtPaid">Amount Paid: </h6>
                            </div>
                            <div class="input-field col s12 l5 push-l2 m5 push-m1">
                                <h6 class="left" id="paymentamtRemaining">Amount Remaining: </h6>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col s12 l5 push-l2 m5 push-m1">
                                <div class="col s12 l6 push-l1">

                                    <input name="group1" type="radio" id="paymentCash" class="cash" checked/>
                                    <label class="left" for="paymentCash">Cash</label>
                                </div>
                                <div class="col s12 l6 push-l6">

                                    <input name="group1" type="radio" id="paymentCheque" class="cheque" />
                                    <label class="right" for="paymentCheque">Cheque</label>

                                </div>
                            </div>
                        </div>
                        <div class="input-field col s12 l5 push-l2 m5 push-m1">
                            <!--<i class="material-icons prefix">mode_edit</i>-->
                            <input id="paymentamount" type="text" name="amount">
                            <label for="paymentamount" class="active">Enter Amount:</label>
                        </div>
                        <div id="BankDetails">

                            <div class="input-field col s12 m5 l5 push-l2 push-m2">
                                <input id="bankName" type="text" name="bankName">
                                <label for="bankName" class="active">Enter Bank Name:</label>
                            </div>
                            <br>
                            <div class="col s12 m6 l12 ">
                                <div class="input-field col s12 m5 l5 push-l2 push-m2">
                                    <input id="cheqNo" type="text" name="cheqNo">
                                    <label for="cheqNo" class="active">Enter Cheque No.:</label>
                                </div>
                                <div class="input-field col s12 m5 l5 push-l2 push-m2">
                                    <input id="cheqDate" type="text" class="datepicker" name="cheqDate">
                                    <label for="cheqDate">Enter Cheque Date:</label>
                                </div>
                            </div>
                        </div>
                        <div class="input-field col s12 l8 push-l2 m5 push-m1">
                            <i class="material-icons prefix">mode_edit</i>
                            <textarea id="paymentcomment" class="materialize-textarea" name="comment"></textarea>
                            <label for="paymentcomment">Comment</label>
                        </div>
							<div class="input-field col s12 l5 push-l4 m5 push-m4 ">                                
                   			  <font color='red'><span id="msgpaymentsave"></span></font>
            				</div>
                    </form>

                </div>

            </div>
            <div class="modal-footer">
                <button type="button" id="savePaymentId" class="modal-action waves-effect  btn-flat ">Pay</button>
                <a class="modal-action modal-close waves-effect btn-flat ">Close</a>
            </div>
        </div> --%>
        
        
        
        <!-- Modal Structure for Employee Details -->
       <!--          <div id="EmpDetails" class="modal" style="width:80%; height:100%;">
            <div class="modal-content">
                <h4 class="center">Employee Details</h4>
                <hr>
                <div class="row">
                    <div class="col s12 l6 m6">
                        <div class="row z-depth-3 green lighten-4">
                            <div class="col s12 l12 m12">
                                <h6 id="empdetailname" class="black-text">Name : </h6>
                            </div>
                            <div class="col s12 l12 m12">
                                <h6 id="empdetaildept" class="black-text">Department : </h6>
                            </div>
                            <div class="col s12 l12 m12">
                                <h6 id="empdetailmobile" class="black-text">Mobile No : </h6>
                            </div>
                            <div class="col s12 l12 m12">
                                <h6 id="Add" class="black-text">Address : </h6><p class="black-text" id="empdetailaddress" ></p>
                            </div>
                            <div class="col s12 l12 m12">
                                <h6 id="empdetailareas" class="black-text">Area : </h6>
                            </div>
                        </div>
                    </div>
                    <div class="col s12 l6 m6">
                        <div class="row z-depth-3 red lighten-3">

                            <div class="col s12 l12 m12">
                                <h6 id="empdetailnoofholidays" class="black-text">No.of Holiday : </h6>
                            </div>
                            <div class="col s12 l12 m12">
                                <h6 id="empdetailbasicsalary" class="black-text">Basic Salary : </h6>
                            </div>
                            <div class="col s12 l12 m12">
                                <h6 id="empdetailincentive" class="black-text">Incentives : </h6>
                            </div>
                            <div class="col s12 l12 m12">
                                <h6 id="empdetaildeduction" class="black-text">Basic Salary : </h6>
                            </div>
                            <div class="col s12 l12 m12">
                                <h6 id="empdetailtotalsalary" class="black-text">Total Amount : </h6>
                            </div>
                            <div class="col s12 l12 m12">
                                <h6 id="empdetailamtpaid" class="black-text">Amount Paid : </h6>
                            </div>
                            <div class="col s12 l12 m12">
                                <h6 id="empdetailamtpending" class="black-text">Amount Pending : </h6>
                            </div>
                        </div>
                    </div>
                </div>

				<div class="row">
                   <div class="col s12 m12 l12">
                       <div class="col s6 m4 l4 offset-l1">
                           Dropdown Trigger
                           <a class='dropdown-button btn waves-effect waves-light blue darken-6' href='#' data-activates='filter1'>Filter<i
              				 class="material-icons right">arrow_drop_down</i></a>
                           Dropdown Structure
                           <ul id='filter1' class='dropdown-content'>
                               <li><button type="button" onclick="searchEmployeeSalary(1)">Current Month</button></li>
                               <li><button type="button" onclick="searchEmployeeSalary(2)">Last 6 Months</button></li>
                               <li><button type="button" onclick="searchEmployeeSalary(3)">Last 1 Year</button></li>
                               <li><button type="button" class="rangeSelect1">Range</button></li>
                               <li><button type="button" onclick="searchEmployeeSalary(5)">View All</button></li>
                           </ul>
                       </div>
                       <form action="ledger">
                           <input type="hidden" name="range" value="dateRange"> 
                           <input type="hidden" name="employeeDetailsId" id="employeeDetailsId">
                           <span class="showDates1">
                             <div class="input-field col s6 m6 l2">
                               <input type="date" class="datepicker" placeholder="Choose Date" name="startDate" id="startDate1" required>
                               <label for="startDate1">From</label>
                             </div>

                             <div class="input-field col s6 m6 l2">
                                   <input type="date" class="datepicker" placeholder="Choose Date" name="endDate" id="endDate1">
                                    <label for="endDate1">To</label>
                              </div>
                           <button type="button"  onclick="searchEmployeeSalary(4)">View</button>
                         </span>
                       </form>
                   </div>
               </div>

                <table border="2" class="centered tblborder">
                    <thead>
                        <tr>
                            <th>Sr.No</th>
                            <th>Amount Paid</th>
                            <th>Incentive</th>
                            <th>Date</th>
                            <th>Mode of Payment</th>
                            <th>Comment</th>
                        </tr>
                    </thead>
                    <tbody id="empdetailtable">
                        <tr>
                            <td>1</td>
                            <td>abc</td>
                            <td>abc</td>
                            <td>abc</td>
                            <td>abc</td>
                            <td>abc</td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <a href="#!" class="modal-action modal-close waves-effect btn-flat ">Ok</a>
            </div>
        </div> -->
        
        
        
        <!--modal structure for incentive-->
        <div id="incentive" class="modal row" style="width:40%;">
          
                  <div class="modal-content" style="padding:20px 24px 20px 24px;">
                    <h4 class="center">Incentive Details</h4>
                    <hr>
                    <br>
                    <form class="col s12 l12 m12" action="${pageContext.request.contextPath}/giveIncentives" method="post" id="saveIncentive">
                    <input id="incentiveEmployeeDetailsId" name="employeeDetailsId" type="hidden"/>
                       
                            <div class="input-field col s12 l6  m6">
                                Name: <input id="incentiveName" type="text" readonly>
                                <!--<h6 id="name">Name:</h6>-->
                            </div>
                    
                     
                            <div class="input-field col s12 l6  m6">
                            	<!-- <input id="incentiveId" type="hidden" name="incentiveId" value="0"> -->
                                <label><span class="red-text">*</span>Incentive:</label><input id="incentiveAmount" type="text" name="incentives">
                                <!--<h6 for="MobileNo"></h6>-->
                            </div>
                            <div class="input-field col s12 l12  m12">
                            	
                               <label><span class="red-text">*</span>Reason:</label><textarea id="reasonIncentive" class="materialize-textarea" name="reason"></textarea>
                                <!--<h6 for="MobileNo"></h6>-->
                            </div>
                            <div class="col s12 l7  m5 offset-l3 offset-m4">                                
                   			  <font color='red'><span id="msgincentivesave"></span></font>
                   			
            				</div>
                      			    <br> 
                         </form>
                        
                      </div>
                  
                       <div class="modal-footer">  
                                             		<div class="col s6 m6 l3 offset-l3">
			              			  <button type="button" id="saveIncentivesSubmit" class="modal-action waves-effect  btn ">Save</button>
			               	    </div>
			             <!--     <button type="button" id="resetIncentive" class="modal-action waves-effect  btn ">Rest</button>
			                 </div> -->
			                	 <div class="col s6 m6 l3">
			               				 <a href="#!" class="modal-action modal-close waves-effect btn red ">Close</a>
		                		</div>
		                		  <br>
		               </div>      
		        </div> 
            
            
            <!-- incentive modal filter and table start-->
                        <!-- <div class="row">
		                    <div class="col s12 m12 l12">
		                        <div class="col s6 m4 l6 ">
		                            Dropdown Trigger
		                            <a class='dropdown-button btn waves-effect waves-light blue darken-6' href='#' data-activates='filterIncentive'>Filter &nbsp &nbsp &nbsp &nbsp   <i
		                class="material-icons right">arrow_drop_down</i></a>
		                            Dropdown Structure
		                            <ul id='filterIncentive' class='dropdown-content'>
		                                <li><button type="button" class="btn btn-flat" style="font-size:13px;" onclick="searchEmployeeIncentive(1)">This Month</button></li>
		                               <li><button type="button" class="btn btn-flat" style="font-size:13px;"  onclick="searchEmployeeIncentive(2)">Last 6 Months</button></li>
		                               <li><button type="button" class="btn btn-flat" style="font-size:13px;"  onclick="searchEmployeeIncentive(3)">Last 1 Year</button></li>
		                               <li><button type="button" class="btn rangeSelect btn-flat" style="font-size:13px;" >Range</button></li>
		                               <li><button type="button" class="btn btn-flat" style="font-size:13px;"  onclick="searchEmployeeIncentive(5)">View All</button></li>
		                            </ul>
		                        </div>
		                        <form action="ledger">
		                            <input type="hidden" name="range" value="dateRange"> 
		                            <span class="showDates">
		                            <input type="hidden" name="employeeDetailsId" id="employeeDetailsIdIncentive">
		                              <div class="input-field col s6 m6 l2">
		                                <input type="date" class="datepicker" placeholder="Choose Date" name="startDate" id="startDateIncentive" required> 
		                                <label for="startDateIncentive">From</label>
		                              </div>
		                              <div class="input-field col s6 m6 l2">
		                                    <input type="date" class="datepicker" placeholder="Choose Date" name="endDate" id="endDateIncentive">
		                                     <label for="endDateIncentive">To</label>
		                               </div>
		                            <button type="button" onclick="searchEmployeeIncentive(4)">View</button>
		                          </span>
		                        </form>
		                    </div>
		                </div> -->
                <!-- <table border="2" class="centered">
                    <thead>
                        <tr>
                            <th>Sr.No</th>
                            <th>Amount</th>
                            <th>Date</th>
                            <th>Edit</th>
                        </tr>
                    </thead>
                    <tbody id="incentiveTable">
                    
                    </tbody>
                </table> -->
               
             <!-- incentive modal filter and table end-->
           
            
       
        <!-- Modal Structure for Area Details -->
        <div id="area" class="modal modal-fixed-footer">
            <div class="modal-content">
                <h4 class="center">Area Details</h4>
                <hr>
                <div class="row">
                    <div class="col s12 l4 m4">
                        <h6 id="employeeareaname" class="black-text">Name:</h6>
                    </div>
                     <div class="col s12 l4 m4">
                        <h6 id="employeeareamobilenumber" class="black-text">Mobile No:</h6>
                    </div>
                    <div class="col s12 l4 m4">
                        <h6 id="employeeareadeptname" class="black-text">Department:</h6>
                    </div>
                   
                </div>
                <table border="2" class="centered tblborder">
                    <thead>
                        <tr>
                            <th>Sr.No</th>
                            <th>Route No</th>
                            <th>Area</th>
                            <th>Cluster</th>

                        </tr>
                    </thead>
                    <tbody id="areaList">
                        
                    </tbody>
                </table>
            </div>
            <div class="modal-footer row">
				<div class="col s6 m6 l7">
                <a href="#!" class="modal-action modal-close waves-effect btn ">Ok</a>
                </div>
            </div>
        </div>


        <!-- Modal Structure for View Holiday details -->
        <div id="viewHoliday" class="modal modal-fixed-footer">
            <div class="modal-content">
                <h4 class="center">Holiday Details</h4>
                <hr>

                <div class="row">
                    <div class="col s12 l6 m6">
                        <h6 id="empholidayname" class="black-text">Name:</h6>
                    </div>
                    <div class="col s12 l6 m6">
                        <h6 id="empholidaydepartment" class="black-text">Department:</h6>
                    </div>
                
                
                    <div class="col s12 l6 m6">
                        <h6 id="empholidaymobileno" class="black-text">Mobile No:</h6>
                    </div>
                    <div class="col s12 l6 m6">
                        <h6 id="empholidaynoofholiday" class="black-text">Total No Of Holiday:</h6>
                    </div>
                </div>
                <div class="row">
                    <div class="col s12 m12 l12">
                        <div class="col s6 m4 l6 left">
                            <!-- Dropdown Trigger -->
                            <a class='dropdown-button btn waves-effect waves-light blue darken-6' href='#' data-activates='filter'>Filter &nbsp &nbsp &nbsp &nbsp   <i
                class="material-icons right">arrow_drop_down</i></a>
                            <!-- Dropdown Structure -->
                            <ul id='filter' class='dropdown-content'>
                                <li><button type="button" class="btn btn-flat" style="font-size:13px;" onclick="searchEmployeeHolidays(1)">This Month</button></li>
                               <li><button type="button" class="btn btn-flat" style="font-size:13px;"  onclick="searchEmployeeHolidays(2)">Last 6 Months</button></li>
                               <li><button type="button" class="btn btn-flat" style="font-size:13px;"  onclick="searchEmployeeHolidays(3)">Last 1 Year</button></li>
                               <li><button type="button" class="btn rangeSelect btn-flat" style="font-size:13px;" >Range</button></li>
                               <li><button type="button" class="btn btn-flat" style="font-size:13px;"  onclick="searchEmployeeHolidays(5)">View All</button></li>
                            </ul>
                        </div>
                        <form action="ledger">
                            <input type="hidden" name="range" value="dateRange"> 
                            <span class="showDates">
                            <input type="hidden" name="employeeDetailsId" id="employeeDetailsId1">
                              <div class="input-field col s6 m6 l2">
                                <input type="date" class="datepicker" placeholder="Choose Date" name="startDate" id="startDate2" required> 
                                <label for="startDate">From</label>
                              </div>
                              <div class="input-field col s6 m6 l2">
                                    <input type="date" class="datepicker" placeholder="Choose Date" name="endDate" id="endDate2">
                                     <label for="endDate">To</label>
                               </div>
                            <button type="button" onclick="searchEmployeeHolidays(4)">View</button>
                          </span>
                        </form>
                    </div>
                </div>
                <table border="2" class="centered tblborder">
                    <thead>
                        <tr>
                            <th>Sr.No</th>
                            <th>No.of holiday</th>
                            <th>From Date</th>
                            <th>To Date</th>
                            <th>Type Of Leave</th>
                            <th>Amount Deduct</th>
                            <th>Reason</th>
                            <th>Holiday Given Date</th>
                            <th>Edit</th>
                        </tr>
                    </thead>
                    <tbody id="holidaytable">
                       
                    </tbody>
                </table>
            </div>
            <div class="modal-footer row">
             <div class="col s6 m6 l3 offset-l4">
                <a href="#!" class="modal-action modal-close waves-effect btn">Ok</a>
                 </div>	
            </div>
        </div>




         <!-- Modal Structure for View Product Details -->
      <!--  <div id="viewDetails" class="modal modal-fixed-footer">
            <div class="modal-content">
                <h4>Product Details</h4>
                <table border="2">
                    <thead>
                        <tr>
                            <th>Sr.No</th>
                            <th>Category</th>
                            <th>Brand</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>1</td>
                            <td>abc</td>
                            <td>abc</td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <a href="#!" class="modal-action modal-close waves-effect  btn-flat ">Delete</a>
                <a href="#!" class="modal-action modal-close waves-effect btn-flat ">Close</a>
            </div>
        </div> -->
        
        
		<div class="row">
			<div class="col s12 m12 l12">
				<div id="addeditmsg" class="modal" style="width:40%;">
					<div class="modal-content">
						<h5 id="msgHead"></h5>
						<hr>
						<h6 id="msg"></h6>
					</div>
					<div class="col s8 m2 l2 offset-l5 modal-footer">
						<a href="#!"
							class="modal-action modal-close waves-effect waves-green btn blue-gradient">OK</a>
					</div>
				</div>
			</div>
		</div>
    </main>
    <!--content end-->
</body>

</html>