<%@page import="com.sheetalarch.entity.AdminLogin"%>
<%
  if(session != null)
  {
	  AdminLogin adminLogin=(AdminLogin)session.getAttribute("adminLogin");
	  if (adminLogin == null)
	  {
	    String address = "/";
	    RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(address);
	    dispatcher.forward(request,response);
	  }
  }
  else
  {
	    String address ="/";
	    RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(address);
	    dispatcher.forward(request,response);
  }
%>
<style>
        /*.highcharts-container {
            display: block;
            height: 300px !important;
        }
        
        .highcharts-legend {
            transform: translate(33, 3000);
        }
        
        .card-content {
            height: 100px !important;
        }
        .greycolor {
            background-color: #eceff1;
        }
        */
        
      
    </style>



<!--top to scroll button-->
    <div onclick="topFunction()" id="myBtn"> <img src="resources/img/arrow-up.png" alt="scroll up" /> </div>
  
  
  
    <!--navbar start-->
    
     <!-- Dropdown Structure of Product-->
    <ul id="dropdown1" class="dropdown-content">
        <li><a href="${pageContext.servletContext.contextPath}/fetchCategoriesList">Category</a></li>
        <li class="divider"></li>
        <li><a href="${pageContext.servletContext.contextPath}/fetchBrandList">Brand</a></li>
        <li class="divider"></li>
        <li><a href="${pageContext.servletContext.contextPath}/fetchProductList">Product</a></li>
    </ul>
    
   	<!-- side nav of reports -->
     <ul id="slide-out" class="side-nav">
        <li><a href="${pageContext.servletContext.contextPath}/fetchInventoryReportView?range=today">Inventory Report</a></li>
        <li class="divider"></li>
        <li><a href="${pageContext.servletContext.contextPath}/orderReport?currentPending=current">Order Report</a></li>
        <li class="divider"></li>
        <li><a href="${pageContext.servletContext.contextPath}/salesReport?range=today">Sales Report</a></li>
        <li class="divider"></li>
        <li><a href="${pageContext.servletContext.contextPath}/customerReportList?range=today">Customer Report</a></li>
        <li class="divider"></li>
        <li><a href="${pageContext.servletContext.contextPath}/fetchProductListForReport?range=today">Product Report</a></li>
        <li class="divider"></li>
        <li><a href="${pageContext.servletContext.contextPath}/SMReportToday?type=AreaSalesMan">ASM Report</a></li>
        <li class="divider"></li>
        <li><a href="${pageContext.servletContext.contextPath}/SMReportToday?type=SalesMan">Salesman Report</a></li>
        <li class="divider"></li>
        <li><a href="${pageContext.servletContext.contextPath}/getCollectionReportDetails?range=CurrentDate">Collection Report</a></li>
    </ul>
    
    <div class="navbar-fixed z-depth-3">
    
        <!--class=" blue darken-8 nav"-->
        <nav class="light-blue darken-4">
			 <div class="nav-wrapper">
                <a href="${pageContext.servletContext.contextPath}/" class="brand-logo White-text" style="font-family:">SHEETAL ARCH</a>
                <a href="#" data-activates="mobile-demo" class="button-collapse"><i class="material-icons">menu</i></a>
                <a href="#" data-activates="slide-out" class="button-collapse show-on-large right hide-on-med-and-down"><i class="material-icons">menu</i></a>
                <ul class="right hide-on-med-and-down">
                    <li><a href="${pageContext.servletContext.contextPath}/fetchBoothList">BoothList</a></li>
                    <!-- Dropdown Trigger -->
                    <li><a class="dropdown-button" href="#!" data-activates="dropdown1" data-beloworigin="true">Product<i class="material-icons right">arrow_drop_down</i></a></li>
                    <li><a href="${pageContext.servletContext.contextPath}/fetchSupplierList">Manage Supplier</a></li>
                    <li><a href="${pageContext.servletContext.contextPath}/fetchEmployeeList">HRM</a></li>
                    <li><a href="${pageContext.servletContext.contextPath}/location">Location</a></li>
                    <li><a href="${pageContext.servletContext.contextPath}/fetchProductListForInventory">Manage Inventory</a></li>
                     <li><a href="#" class="waves-effect waves-light btn-flat white-text" style="padding: 0px 5px 4px 5px; height: 64px; margin-right: 0px;"><i
					class="fa fa-user white-text">&nbsp;</i>${adminLogin.name}</a></li>
                    <li><a href="${pageContext.servletContext.contextPath}/logoutAdmin" class="waves-effect waves-light btn-flat white-text" style="padding: 0px 5px 4px 5px; height: 64px;"><i
					class="fa fa-sign-out white-text"></i> Logout</a></li>
                </ul>

               
            </div>
</nav>
  </div>
	<div class="row" style="margin-bottom:10px;">
		<div class="col s12 m12 l12">
					<h5 class="center heading">${pageName}</h5>
		</div>
	</div>		
          

  
    <!--navbar end-->
     <!-- code for loader -->
    <div class="preloader-background">
    <div class="preloader-wrapper big active">
      <div class="spinner-layer spinner-blue">
        <div class="circle-clipper left">
          <div class="circle"></div>
        </div><div class="gap-patch">
          <div class="circle"></div>
        </div><div class="circle-clipper right">
          <div class="circle"></div>
         
        </div>
      </div>
      
    </div>
     <div class="red-text"><br><h6>Wait.....</h6></div>
     </div>