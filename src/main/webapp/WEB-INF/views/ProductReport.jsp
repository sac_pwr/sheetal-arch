<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<html>

<head>
  <%@include file="components/header_imports.jsp" %>
    <script type="text/javascript">
	    
	    $(document).ready(function() {
            $(".showQuantity").hide();
            $(".showDates").hide();
            $("#oneDateDiv").hide();
            $(".topProduct").click(function() {
                $(".showQuantity").show();
                $(".showDates").hide();
                $("#oneDateDiv").hide();
            });
           
            $(".rangeSelect").click(function() {
            	$("#oneDateDiv").hide();	
                $(".showDates").show();
                $(".showQuantity").hide();
            });
           
        	$(".pickdate").click(function(){
        		 $(".showQuantity").hide();
        		 $(".showDates").hide();
        		$("#oneDateDiv").show();
        	});
            $('#topProductId').change(function(){
            
            	window.location.href = '${pageContext.servletContext.contextPath}/fetchProductListForReport?range=TopProducts&topProductNo='+$('#topProductId').val();
            	
            });
            
            
            var table = $('#tblData').DataTable();	
        	$.fn.dataTable.ext.search.push(function( settings, data, dataIndex ) {
        		       	        
        		        var salesManId=data[3].trim();
        				var salesManId2=$('#brandid').val();
        				if(salesManId2==="0")
        				{
        					totalAmount=parseFloat(totalAmount)+parseFloat(data[5].trim());
        					return true;
        				}
        				else if ( salesManId2 === salesManId )
        		        {
        					totalAmount=parseFloat(totalAmount)+parseFloat(data[5].trim());
        		            return true;
        		        }
        		        return false;
        		        
        	});
        	
    	    $('#brandid').change(function(){
    	    	totalAmount=0;
        		table.draw();
        		$('#totalAmountId').text(totalAmount);
        	});
            
        });

    </script>
    <style>
    .card-image{
        	width:40% !important;
        	background-color:#01579b !important;
        }
    </style>
</head>

<body>
    <!--navbar start-->
   	<%@include file="components/navbar.jsp" %>
    <!--navbar end-->
    <!--content start-->
    <main>
     
        <div class="row">
        	 <div class="col s12 l3 m3">
      		  <div class="card horizontal">
      		  	<div class="card-image">
       				  <h6 class="white-text" style="padding:8px">Total Amount:</h6>
      			</div>
      			<div class="card-stacked grey lighten-3">
      			  <div class="card-content" style="padding:8px">
       			<h6 class="">&#8377;<span id="totalAmountId"><c:out value="${totalAmountWithTax}" /></span></h6>
       			  </div>
        	    </div>
                
          	  </div>
           </div>  
        	     <div class="col s12 m4 l4 right" style="margin-top:1%;">
                <div class="col s6 m4 l4 right">
                    <!-- Dropdown Trigger -->
                    <a class='dropdown-button btn waves-effect waves-light grey-text  text-lighten-3 light-blue darken-4' href='#' data-activates='filter'>Filter<i
                class="material-icons right">arrow_drop_down</i></a>
                    <!-- Dropdown Structure -->
                    <ul id='filter' class='dropdown-content'>
                    	 <li><button class="btn-flat topProduct teal-text">Top Sold Product</button></li>
                    	<li><a href="${pageContext.request.contextPath}/fetchProductListForReport?range=yesterday">Yesterday</a></li>
                        <li><a href="${pageContext.request.contextPath}/fetchProductListForReport?range=last7days">Last 7 Days</a></li>
                        <li><a href="${pageContext.request.contextPath}/fetchProductListForReport?range=last1month">Last 1 Month</a></li>
                        <li><a href="${pageContext.request.contextPath}/fetchProductListForReport?range=last3months">Last 3 Month</a></li>
                        <li><a class="rangeSelect">Range</a></li>
                        <li><a class="pickdate">Pick date</a></li>                        
                        <li><a href="${pageContext.request.contextPath}/fetchProductListForReport?range=viewAll">View All</a></li>
                    </ul>
                </div>
                	<div class="col s12 l8	m8 right"> 
                 <form action="${pageContext.request.contextPath}/fetchProductListForReport" method="post">
                    <input type="hidden" name="range" value="range">
                     <span class="showDates">
                     			 <div class="input-field col s6 m2 l2 right">
                            <button type="submit">View</button>
                            </div>
                             
                              <div class="input-field col s6 m5 l5 right">
                                   <input type="date" class="datepicker" placeholder="Choose Date" name="endDate" id="endDate" required> 
                                <label for="endDate">To</label>
                               </div>
                                <div class="input-field col s6 m5 l5 right">
                                	<input type="date" class="datepicker" placeholder="Choose Date" name="startDate" id="startDate">
                                     <label for="startDate">From</label>
                                 </div>
                                 
                               
                          </span>
                </form>
                 </div>
				<div class="col s12 l6	m6 right" id="oneDateDiv">                            	
                    <form action="${pageContext.request.contextPath}/fetchProductListForReport" method="post">
                    <input type="hidden" name="range" value="pickdate">
	                    <div class="input-field col s12 m10 l10">
		                    <input type="text" id="oneDate" class="datepicker" placeholder="Choose date" name="startDate">
		                    <label for="oneDate" class="black-text">Pick Date</label>
	                    </div>
	                    <div class="input-field col s12 m2 l2">
	                     <button type="submit">View</button>
                       
                        </div>
                    </form>
               </div>
               
               
                
            </div>
            	 <div class="input-field col s6 m2 l2 offset-l1 offset-m1">
                   
                         <select id="brandid" name="brandId" required>
                                 <option value="0" selected>Choose Brand</option>
                                <c:if test="${not empty brandlist}">
							<c:forEach var="listValue" items="${brandlist}">
								<option value="<c:out value="${listValue.name}" />"><c:out value="${listValue.name}" /></option>
							</c:forEach>
						</c:if>
                        </select>
                   
                    </div>  
            <div class="input-field col s6 m2 l2">
                    <span class="showQuantity">
                         <select id="topProductId">
                         <option value=""  selected>Choose Option</option>
                            <option value="5">5</option>
                            <option value="10">10</option>
                           <option value="15">15</option>
                        </select>
                        </span>
                    </div>  
            </div>
        	

        <div class="row">
            <div class="col s12 l12 m12">
                <table class="striped highlight bordered centered" id="tblData" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th class="print-col">Sr.No</th>
                            <th class="print-col">Product</th>
                            <th class="print-col">Category</th>
                            <th class="print-col">Brand</th>
                            <th class="print-col">Quantity Sold</th>
                            <th class="print-col">Amount</th>
                        </tr>
                    </thead>

                    <tbody>
                    
                    <c:if test="${not empty productReportViews}">
					<c:forEach var="listValue" items="${productReportViews}">
                        <tr>
                            <td><c:out value="${listValue.srno}" /></td>
                            <td><c:out value="${listValue.product.productName}" /></td>
                            <td><c:out value="${listValue.product.categories.categoryName}" /></td>
                            <td><c:out value="${listValue.product.brand.name}" /></td>
                            <td><c:out value="${listValue.totalIssuedQuantity}" /></td>
                            <td><c:out value="${listValue.totalAmountWithTax}" /> </td>
                        </tr>
                     </c:forEach>
                     </c:if>
                     
                    </tbody>
                </table>
            </div>
        </div>

 <!-- Modal Structure for Tax Details -->
        <div id="tax" class="modal">
            <div class="modal-content">
                <h4 class="center">Tax Details</h4>
                <hr>
                <br>
                <table class="centered tblborder" border="2">
					<thead>
                    <tr>
                        <th colspan="2">%CGST</th>                        
                        <th colspan="2">%SGST</th>
                        <th colspan="2">%IGST</th>
                        <th rowspan="2">Total tax Amount</th>
                    </tr>
                    <tr>
                        <th>%</th>
                        <th>Amount</th>
                        <th>%</th>
                        <th>Amount</th>
                        <th>%</th>
                        <th>Amount</th>

                    </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td id="cgstPerId"></td>
                            <td id="cgstAmtId"></td>
                            <td id="sgstPerId"></td>
                            <td id="sgstAmtId"></td>
                            <td id="igstPerId"></td>
                            <td id="igstAmtId"></td>
                            <td id="taxAmountId"></td>
                        </tr>
                    </tbody>
                </table>
            </div>
             <div class="modal-footer row">

                <div class="col s12 m6 l6 offset-l1">
                    <a href="#!" class="modal-action modal-close waves-effect btn red">Close</a>
                </div>

            </div>
        </div>

    </main>
    <!--content end-->
</body>

</html>