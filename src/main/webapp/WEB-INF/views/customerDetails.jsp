<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<html>

<head>
  <%@include file="components/header_imports.jsp" %>
  <style>
  	.btn {
    font-size: 0.8rem;
    border: none;
    border-radius: 2px;
    display: inline-block;
    height: 32px;
    line-height: 32px;
    padding: 0 8px !important;
    text-transform: uppercase;
    vertical-align: middle;
    -webkit-tap-highlight-color: transparent;
}
	.card-image{
		width:40% !important;
		background-color:#01579b !important;
	}
	h6 {
    font-size: 0.8rem !important;
    line-height: 110%;
    margin: .5rem 0 .4rem 0;
}
  </style>
    <script type="text/javascript">
	    
	    $(document).ready(function() {
            $(".showQuantity").hide();
            $(".showDates").hide();
            $("#oneDateDiv").hide();
            $(".topProduct").click(function() {
                $(".showQuantity").show();
                $(".showDates").hide();
                $("#oneDateDiv").hide();
            });
           
            $(".rangeSelect").click(function() {
            	$("#oneDateDiv").hide();	
                $(".showDates").show();
                $(".showQuantity").hide();
            });
           
        	$(".pickdate").click(function(){
        		 $(".showQuantity").hide();
        		 $(".showDates").hide();
        		$("#oneDateDiv").show();
        	});
        });

    </script>
</head>

<body>
    <!--navbar start-->
   	<%@include file="components/navbar.jsp" %>
    <!--navbar end-->
    <!--content start-->
    <main>
     
        <div class="row">
        	 <div class="col s12 l3 m3">
      		 	<div class="card horizontal">
      		  	<div class="card-image">
       				  <h6 class="white-text" style="padding:8px">OWNER NAME:</h6>
      			</div>
      			<div class="card-stacked grey lighten-3">
      			  <div class="card-content" style="padding:8px">
       			    <h6 class=""><c:out value="${singleCustomerOrderReport.booth.ownerName}" /></h6>
       			  </div>
        	    </div>
                
          	  </div>
          	  <div class="card horizontal">
      		  	<div class="card-image">
       				  <h6 class="white-text" style="padding:8px">MOBILE NO:</h6>
      			</div>
      			<div class="card-stacked grey lighten-3">
      			  <div class="card-content" style="padding:8px">
       			<h6 class=""><c:out value="${singleCustomerOrderReport.booth.contact.mobileNumber}" /></h6>
       			  </div>
        	    </div>
                
          	  </div>
          	  <div class="card horizontal">
      		  	<div class="card-image">
       				  <h6 class="white-text" style="padding:8px">BOOTH NO:</h6>
      			</div>
      			<div class="card-stacked grey lighten-3">
      			  <div class="card-content" style="padding:8px">
       			<h6 class=""><c:out value="${singleCustomerOrderReport.booth.boothNo}" /></h6>
       			  </div>
        	    </div>
                
          	  </div>
           </div>  
            <div class="col s12 l3 m3">
      		  <div class="card horizontal">
      		  	<div class="card-image">
       				  <h6 class="white-text" style="padding:8px">ROUTE NO:</h6>
      			</div>
      			<div class="card-stacked grey lighten-3">
      			  <div class="card-content" style="padding:8px">
       			<h6 class=""><c:out value="${singleCustomerOrderReport.booth.route.routeNo}" /></h6>
       			  </div>
        	    </div>
                
          	  </div>
          	  <div class="card horizontal">
      		  	<div class="card-image">
       				  <h6 class="white-text" style="padding:8px">AREA:</h6>
      			</div>
      			<div class="card-stacked grey lighten-3">
      			  <div class="card-content" style="padding:8px">
       			<h6 class=""><c:out value="${singleCustomerOrderReport.booth.route.area.name}" /></h6>
       			  </div>
        	    </div>
                
          	  </div><div class="card horizontal">
      		  	<div class="card-image">
       				  <h6 class="white-text" style="padding:8px">CLUSTER:</h6>
      			</div>
      			<div class="card-stacked grey lighten-3">
      			  <div class="card-content" style="padding:8px">
       			<h6 class=""><c:out value="${singleCustomerOrderReport.booth.route.area.cluster.name}" /></h6>
       			  </div>
        	    </div>
                
          	  </div>
           </div> 
            <div class="col s12 l3 m3">
      		  <div class="card horizontal">
      		  	<div class="card-image">
       				  <h6 class="white-text" style="padding:8px">TOTAL PURCHASE:</h6>
      			</div>
      			<div class="card-stacked grey lighten-3">
      			  <div class="card-content" style="padding:8px">
       			<h6 class="">&#8377;<span id="totalAmountId"><c:out value="${singleCustomerOrderReport.totalAmount}" /></span></h6>
       			  </div>
        	    </div>
                
          	  </div>
          	  <div class="card horizontal">
      		  	<div class="card-image">
       				  <h6 class="white-text" style="padding:8px">AMOUNT PAID:</h6>
      			</div>
      			<div class="card-stacked grey lighten-3">
      			  <div class="card-content" style="padding:8px">
       			<h6 class="">&#8377;<span id="totalAmountId"><c:out value="${singleCustomerOrderReport.totalAmountPaid}" /></span></h6>
       			  </div>
        	    </div>
                
          	  </div><div class="card horizontal">
      		  	<div class="card-image">
       				  <h6 class="white-text" style="padding:8px">BALANCE AMOUNT:</h6>
      			</div>
      			<div class="card-stacked grey lighten-3">
      			  <div class="card-content" style="padding:8px">
       			<h6 class="">&#8377;<span id="totalAmountId"><c:out value="${singleCustomerOrderReport.totalAmountUnPaid}" /></span></h6>
       			  </div>
        	    </div>
                
          	  </div>
           </div> 
           
        	     <div class="col s12 m3 l3 right" style="margin-top:1%;">
                <div class="col s6 m12 l12 right-align">
                    <!-- Dropdown Trigger -->
                    <a class='dropdown-button btn waves-effect waves-light grey-text  text-lighten-3 light-blue darken-4' href='#' data-activates='filter'>Filter<i
               			 class="material-icons right">arrow_drop_down</i></a>
                    <!-- Dropdown Structure -->
                    <ul id='filter' class='dropdown-content'>
                    	<li><a href="${pageContext.request.contextPath}/customerReportByBoothId?range=yesterday&boothId=${singleCustomerOrderReport.booth.boothId}">Yesterday</a></li>
                        <li><a href="${pageContext.request.contextPath}/customerReportByBoothId?range=last7days&boothId=${singleCustomerOrderReport.booth.boothId}">Last 7 Days</a></li>
                        <li><a href="${pageContext.request.contextPath}/customerReportByBoothId?range=last1month&boothId=${singleCustomerOrderReport.booth.boothId}">Last 1 Month</a></li>
                        <li><a href="${pageContext.request.contextPath}/customerReportByBoothId?range=last3months&boothId=${singleCustomerOrderReport.booth.boothId}">Last 3 Month</a></li>
                        <li><a class="rangeSelect">Range</a></li>
                        <li><a class="pickdate">Pick date</a></li>                        
                        <li><a href="${pageContext.request.contextPath}/customerReportByBoothId?range=viewAll&boothId=${singleCustomerOrderReport.booth.boothId}">View All</a></li>
                    </ul>
                </div>
               <div class="col s12 l12	m12"> 
                 <form action="${pageContext.request.contextPath}/customerReportByBoothId" method="post">
                    <input type="hidden" name="range" value="range">
                    <input type="hidden" name="boothId" value="${singleCustomerOrderReport.booth.boothId}">
                     <span class="showDates">
                     			 <div class="input-field col s6 m2 l2 right">
                            <button type="submit">View</button>
                            </div>
                             
                              <div class="input-field col s6 m5 l5 right">
                                    <input type="date" class="datepicker" placeholder="Choose Date" name="endDate" id="endDate">
                                     <label for="endDate">To</label>
                               </div>
                                <div class="input-field col s6 m5 l5 right">
                                <input type="date" class="datepicker" placeholder="Choose Date" name="startDate" id="startDate" required> 
                                <label for="startDate">From</label>
                                 </div>
                          </span>
                </form>
                 </div>
				<div class="col s12 l12	m12" id="oneDateDiv">                            	
                    <form action="${pageContext.request.contextPath}/customerReportByBoothId" method="post">
	                    <input type="hidden" value=pickdate name="range">
	                    <input type="hidden" name="boothId" value="${singleCustomerOrderReport.booth.boothId}">
	                    <div class="input-field col s12 m5 l5">
		                    <input type="text" id="oneDate" class="datepicker" placeholder="Choose date" name="startDate">
		                    <label for="oneDate" class="black-text">Pick Date</label>
	                    </div>
	                    <div class="input-field col s12 m2 l2">
	                     <button type="submit">View</button>
                        </div>
                    </form>
               </div>
               
               
                
            </div>
            	  
           
                             
                 </div>
        	
 
        <div class="row">
            <div class="col s12 l12 m12">
                <table class="striped highlight bordered centered" id="tblData" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th class="print-col">Sr.No</th>
                            <th class="print-col">Order Id</th>
                            <th class="print-col">Order Status</th>
                            <th class="print-col">Date of Cancel</th>
                            <th class="print-col">Date of order</th>
                            <th class="print-col">Order delivered date</th>
                            <th class="print-col">Total Amount</th>
                            <th class="print-col">Amount Paid</th>
                            <th class="print-col">Balance Amount</th>
                            <th class="print-col">Mode</th>
                            <th class="print-col">Date of Payment</th>
                        </tr>
                    </thead>

                    <tbody>
                    
                    <c:if test="${not empty singleCustomerOrderReport.singleCustomerOrderReportList}">
					<c:forEach var="listValue" items="${singleCustomerOrderReport.singleCustomerOrderReportList}">
                        <tr>
                            <td><c:out value="${listValue.srno}" /></td>
                            <td><a href="#"><c:out value="${listValue.orderId}" /></a></td>
                            <td><c:out value="${listValue.orderStatus}" /></td>
                            <td><c:out value="${listValue.cancelDate}" /></td>
                            <td><c:out value="${listValue.orderDate}" /></td>
                            <td><c:out value="${listValue.deliveryDate}" /></td>
                            <td><c:out value="${listValue.orderAmount}" /></td>
                            <td><c:out value="${listValue.orderAmountPaid}" /></td>
                            <td><c:out value="${listValue.orderAmountUnPaid}" /></td>
                           	<td><c:out value="${listValue.payMode}" /></td>
                            <td><c:out value="${listValue.lastPaymentDate}" /></td>                                                      
                        </tr>
                     </c:forEach>
                     </c:if>
                     
                    </tbody>
                </table>
            </div>
        </div>

 		<div class="row">
			<div class="col s8 m2 l2">
				<div id="addeditmsg" class="modal" style="width:40%;">
					<div class="modal-content">
						<h5 id="msgHead">Error</h5>
						<hr>	
						<h6 id="msg"></h6>
					</div>
					<div class="col s8 m2 l2 offset-l5 modal-footer">
						<a href="#!"
							class="modal-action modal-close waves-effect btn grey-text  text-lighten-3 light-blue darken-4">OK</a>
					</div>
				</div>
			</div>
		</div>
    </main>
    <!--content end-->
</body>

</html>