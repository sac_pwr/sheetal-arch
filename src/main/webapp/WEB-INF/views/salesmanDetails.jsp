<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<html>

<head>
  <%@include file="components/header_imports.jsp" %>
  <style>
  	.btn {
    font-size: 0.8rem;
    border: none;
    border-radius: 2px;
    display: inline-block;
    height: 32px;
    line-height: 32px;
    padding: 0 8px !important;
    text-transform: uppercase;
    vertical-align: middle;
    -webkit-tap-highlight-color: transparent;
}
	.card-image{
		width:40% !important;
		background-color:#01579b !important;
	}
	h6 {
    font-size: 0.8rem !important;
    line-height: 110%;
    margin: .5rem 0 .4rem 0;
}
	tr:last-child th{
		border-bottom:1px solid black;
	}
  </style>
    <script type="text/javascript">
	 
	    $(document).ready(function() {
            $(".showQuantity").hide();
            $(".showDates").hide();
            $("#oneDateDiv").hide();
            $(".topProduct").click(function() {
                $(".showQuantity").show();
                $(".showDates").hide();
                $("#oneDateDiv").hide();
            });
           
            $(".rangeSelect").click(function() {
            	$("#oneDateDiv").hide();	
                $(".showDates").show();
                $(".showQuantity").hide();
            });
           
        	$(".pickdate").click(function(){
        		 $(".showQuantity").hide();
        		 $(".showDates").hide();
        		$("#oneDateDiv").show();
        	});


        	
        	
        });

    </script>
</head>

<body>
    <!--navbar start-->
   	<%@include file="components/navbar.jsp" %>
    <!--navbar end-->
    <!--content start-->
    <main>
     
        <div class="row">
        	 <div class="col s12 l4 m4">
      		 	<div class="card horizontal">
      		  	<div class="card-image">
       				  <h6 class="white-text" style="padding:8px">NAME :</h6>
      			</div>
      			<div class="card-stacked grey lighten-3">
      			  <div class="card-content" style="padding:8px">
       			    <h6 class=""><c:out value="${sMReportOrderDetails.name}" /></h6>
       			  </div>
        	    </div>
                
          	  </div>
          	  <div class="card horizontal">
      		  	<div class="card-image">
       				  <h6 class="white-text" style="padding:8px">MOBILE NO :</h6>
      			</div>
      			<div class="card-stacked grey lighten-3">
      			  <div class="card-content" style="padding:8px">
       			<h6 class=""><c:out value="${sMReportOrderDetails.mobileNumber}" /></h6>
       			  </div>
        	    </div>
                
          	  </div>
          	  <div class="card horizontal">
      		  	<div class="card-image">
       				  <h6 class="white-text" style="padding:8px">ROUTE NO:</h6>
      			</div>
      			<div class="card-stacked grey lighten-3">
      			  <div class="card-content" style="padding:8px">
       			<h6 class=""><c:out value="${sMReportOrderDetails.routeName}" /></h6>
       			  </div>
        	    </div>
                
          	  </div>
           </div>  
            <div class="col s12 l4 m4">
      		  
          	  <div class="card horizontal">
      		  	<div class="card-image">
       				  <h6 class="white-text" style="padding:8px">AREA:</h6>
      			</div>
      			<div class="card-stacked grey lighten-3">
      			  <div class="card-content" style="padding:8px">
       			<h6 class=""><c:out value="${sMReportOrderDetails.areaName}" /></h6>
       			  </div>
        	    </div>
                
          	  </div><div class="card horizontal">
      		  	<div class="card-image">
       				  <h6 class="white-text" style="padding:8px">CLUSTER:</h6>
      			</div>
      			<div class="card-stacked grey lighten-3">
      			  <div class="card-content" style="padding:8px">
       			<h6 class=""><c:out value="${sMReportOrderDetails.clusterName}" /></h6>
       			  </div>
        	    </div>
                
          	  </div>
          	  <c:choose>
          	  <c:when test="${sMReportOrderDetails.type=='SalesMan'}">
	          	  <div class="card horizontal">
	      		  	<div class="card-image">
	       				  <h6 class="white-text" style="padding:8px">ASM Name :</h6>
	      			</div>
	      			<div class="card-stacked grey lighten-3">
	      			  <div class="card-content" style="padding:8px">
	       			<h6 class=""><c:out value="${sMReportOrderDetails.ASMName}" /></h6>
	       			  </div>
	        	    </div>
	                
	          	  </div>
          	  </c:when>
          	  <c:otherwise>
	          	  <div class="card horizontal">
	      		  	<div class="card-image">
	       				  <h6 class="white-text" style="padding:8px">TOTAL AMOUNT:</h6>
	      			</div>
	      			<div class="card-stacked grey lighten-3">
	      			  <div class="card-content" style="padding:8px">
	       			<h6 class="">&#8377;<span id="totalAmountId"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${sMReportOrderDetails.totalAmount}" /></span></h6>
	       			  </div>
	        	    </div>
	                
	          	  </div>
          	  </c:otherwise>
          	  </c:choose>
           </div> 
            
           
        	     <div class="col s12 m4 l4 right-align" style="margin-top:1%;">
                <div class="col s6 m4 l4 right">
                    <!-- Dropdown Trigger -->
                    <a class='dropdown-button btn waves-effect waves-light grey-text  text-lighten-3 light-blue darken-4' href='#' data-activates='filter'>Filter<i
               			 class="material-icons right">arrow_drop_down</i></a>
                    <!-- Dropdown Structure -->
                    <ul id='filter' class='dropdown-content'>
                    	<li><a href="${pageContext.request.contextPath}/SMReportOrderDetails?range=yesterday&employeeDetailsId=${sMReportOrderDetails.employeeDetailsId}">Yesterday</a></li>
                        <li><a href="${pageContext.request.contextPath}/SMReportOrderDetails?range=last7days&employeeDetailsId=${sMReportOrderDetails.employeeDetailsId}">Last 7 Days</a></li>
                        <li><a href="${pageContext.request.contextPath}/SMReportOrderDetails?range=last1month&employeeDetailsId=${sMReportOrderDetails.employeeDetailsId}">Last 1 Month</a></li>
                        <li><a href="${pageContext.request.contextPath}/SMReportOrderDetails?range=last3months&employeeDetailsId=${sMReportOrderDetails.employeeDetailsId}">Last 3 Month</a></li>
                        <li><a class="rangeSelect">Range</a></li>
                        <li><a class="pickdate">Pick date</a></li>                        
                        <li><a href="${pageContext.request.contextPath}/SMReportOrderDetails?range=viewAll&employeeDetailsId=${sMReportOrderDetails.employeeDetailsId}">View All</a></li>
                    </ul>
                </div>
                	<div class="col s12 l8	m8" id="oneDateDiv">                            	
                    <form action="${pageContext.request.contextPath}/SMReportOrderDetails" method="post">
                    <input type="hidden" name="range" value="pickdate">
                    <input type="hidden" name="employeeDetailsId" value="${sMReportOrderDetails.employeeDetailsId}">
	                    <div class="input-field col s12 m5 l5">
		                    <input type="text" id="oneDate" class="datepicker" placeholder="Choose date" name="startDate">
		                    <label for="oneDate" class="black-text">Pick Date</label>
	                    </div>
	                    <div class="input-field col s12 m2 l2">
	                     <button type="submit">View</button>
	                    <input type="hidden" value="0" name="areaId">
                        <input type="hidden" value=pickDate name="range">
                       
                        </div>
                    </form>
               </div>
               <div class="col s12 l8 m8"> 
                 <form action="${pageContext.request.contextPath}/SMReportOrderDetails" method="post">
                    <input type="hidden" name="range" value="range">
                    <input type="hidden" name="employeeDetailsId" value="${sMReportOrderDetails.employeeDetailsId}">
                     <span class="showDates">
                     			 <div class="input-field col s6 m2 l2 right">
                            <button type="submit">View</button>
                            </div>
                             
                              <div class="input-field col s6 m5 l5 right">
                                <input type="date" class="datepicker" placeholder="Choose Date" name="endDate" id="endDate" required> 
                                <label for="endDate">To</label>
                                </div>
                                <div class="input-field col s6 m5 l5 right">
                                    <input type="date" class="datepicker" placeholder="Choose Date" name="startDate" id="startDate">
                                     <label for="startDate">From</label>
                                </div>
                                 
                               
                          </span>
                </form>
                 </div>
				
               
               
                
            </div>
            	  
           
                             
                 </div>
        	
 
        <div class="row">
            <div class="col s12 l12 m12 ">
                <table class="striped highlight bordered centered tblborder" id="tblData" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th class="print-col">Sr.No</th>
                            <th class="print-col">Order Id</th>
                            <th class="print-col">Booth No</th>
                            <th class="print-col">Owner No</th>                            
                            <th class="print-col">Total Amount</th>
                            <th class="print-col">Date of order</th>
                            <th class="print-col">Order delivered date</th>
                        </tr>
                    </thead>

                    <tbody>
                    
                    <c:if test="${not empty sMReportOrderDetails.SMReportOrdersList}">
					<c:forEach var="listValue" items="${sMReportOrderDetails.SMReportOrdersList}">
                        <tr>
                            <td><c:out value="${listValue.srno}" /></td>
                            <td><a href="#"><c:out value="${listValue.orderId}" /></a></td>
                            <td><c:out value="${listValue.boothNo}" /></td>
                            <td><c:out value="${listValue.ownerName}" /></td>
                            <td><c:out value="${listValue.orderAmount}" /></td>
                            <td><c:out   value = "${listValue.dateOfOrderBooked}"  /></td>
                            <td><c:out   value = "${listValue.dateOfOrderDelivered}"  /></td>
                                                      
                        </tr>
                     </c:forEach>
                     </c:if>
                     
                    </tbody>
                    <tfoot class="centered">
            		<tr>
             			  <th colspan="4">Total</th>
              			  <th><center>&#8377;  <span id="totalAmountId"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${sMReportOrderDetails.totalAmount}" /></span></center></th>
                		  <th></th>
                		  <th></th>
           			</tr>
        </tfoot>
                </table>
            </div>
        </div>

 
    </main>
    <!--content end-->
</body>

</html>