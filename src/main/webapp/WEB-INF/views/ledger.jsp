<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<html>

<head>
     <%@include file="components/header_imports.jsp" %>


<script type="text/javascript">

$(document).ready(function() 
{
	//filter in databale
	var table = $('#tblData').DataTable();	
	$.fn.dataTable.ext.search.push(function( settings, data, dataIndex ) {
		       	        
		        var id=data[1];
				var idName=id.substr(0, 3);
				var typeId=$('#typeId').val();
				
				if(typeId==="0")
				{
					return true;
				}
				
		        if ( idName === typeId )
		        {
		            return true;
		        }
		        return false;
		        
	});
	
	$('#typeId').change(function(){
		//when i select any type then its show changes in datable depend on which type select
		table.draw();
	});
});

</script>

</head>

<body>
    <!--navbar start-->
   	<%@include file="components/navbar.jsp" %>
    <!--navbar end-->
    <!--content start-->
    <main>
   
    <div class="row">
    
            <div class="col s12 m4 l2 right">
                <select id="typeId">
                    <option value="0" selected>Choose Type</option>
                         <option value="SUP">Supplier</option>
                         <option value="DB1">Delivery Boy</option>
                     <option value="SM1">Sales Man</option>
                     <option value="GK1">Gate Keeper</option>
                </select>
            </div>
            <div class="col s12 m8 l8 right" style="margin-top:1%">
                  <div class="col s6 m2 l2 right">
                    <!-- Dropdown Trigger -->
                    <a class='dropdown-button btn waves-effect waves-light grey-text  text-lighten-3 light-blue darken-4' href='#' data-activates='filter'>Filter<i
                class="material-icons right">arrow_drop_down</i></a>
                    <!-- Dropdown Structure -->
                    <ul id='filter' class='dropdown-content'>
                        <li><a href="${pageContext.servletContext.contextPath}/fetchLedgerPaymentView?range=Last6Months">Last 6 Months</a></li>
                        <li><a href="${pageContext.servletContext.contextPath}/fetchLedgerPaymentView?range=Last1Year">Last 1 Year</a></li>
                        <li><a href="${pageContext.servletContext.contextPath}/fetchLedgerPaymentView?range=CurrentMonth">Current Month</a></li>
                        <li><a class="rangeSelect">Range</a></li>
                        <li><a href="${pageContext.servletContext.contextPath}/fetchLedgerPaymentView?range=ViewAll">View All</a></li>
                    </ul>
                </div>
                <form action="${pageContext.servletContext.contextPath}/fetchLedgerPaymentView" method="post">
                    <input type="hidden" name="range" value="Range"> <span class="showDates">
                    		<div class="input-field col s6 m1 l1 right">
                    			 <button type="submit">View</button>
                    		</div>
                    		<div class="input-field col s6 m2 l2 right">
                                    <input type="date" class="datepicker" placeholder="Choose Date" name="endDate" id="endDate">
                                     <label for="endDate">To</label>
                               </div>
                    		
                              <div class="input-field col s6 m2 l2 right">
                                <input type="date" class="datepicker" placeholder="Choose Date" name="startDate" id="startDate" required> 
                                <label for="startDate">From</label>
                              </div>

                              
                           
                          </span>
                </form>
              
            </div>


        </div>
      

       <input type="hidden" value="<c:out value="${ledgerPaymentViews.size()}" />" id="srNoId">
          
                <div class="col s12 l12 m12">
                    <table class="striped highlight bordered centered " id="tblData" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th class="print-col">Sr.No</th>
                                <th class="print-col">Id</th>
                                <th class="print-col">Name</th>
                                <th class="print-col">Amount</th>
                                <th class="print-col">Payment Mode</th>
                                <th class="print-col">Department</th>
                                <th class="print-col">Paid Date</th>
                            </tr>
                        </thead>

                        <tbody>
                        
                        <c:if test="${not empty ledgerPaymentViews}">
						<c:forEach var="listValue" items="${ledgerPaymentViews}">
						
							<c:choose>  
						    <c:when test="${listValue.departmentName == 'Supplier'}">  
	                            <tr id="row${listValue.srno}" class="orange lighten-5">
	                            	
	                                <td>
	                                <input type="hidden" value="<c:out value="${listValue.genId}" />" id="idd${listValue.srno}">
	                                <c:out value="${listValue.srno}" /></td>
	                                <td><a title="View Supplier Details" href="${pageContext.servletContext.contextPath}/fetchSupplierListBySupplierId?supplierId=${listValue.genId}"><c:out value="${listValue.genId}" /></a></td>
	                                <td><c:out value="${listValue.name}" /></td>
	                                <td><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${listValue.amountPaid}" /></td>
	                                <td><c:out value="${listValue.payMode}" /></td>
	                                <td><c:out value="${listValue.departmentName}" /></td>
	                                <td>
	                                	<fmt:formatDate pattern = "yyyy-MM-dd" var="date"   value = "${listValue.paymentPaidDate}"  />
	                                	<c:out value="${date}" />
	                                </td>
	                            </tr>
	                         </c:when>
	                         <c:when test="${listValue.departmentName == 'DeliveryBoy'}">  
	                            <tr id="row${listValue.srno}"  class=" pink lighten-5">
	                                <td>
	                                <input type="hidden" value="<c:out value="${listValue.genId}" />" id="idd${listValue.srno}">
	                                <c:out value="${listValue.srno}" /></td>
	                                <td><a title="View Deliveryboy Details" href="${pageContext.servletContext.contextPath}/getEmployeeView?employeeDetailsId=${listValue.id}"><c:out value="${listValue.genId}" /></a></td>
	                                <td><c:out value="${listValue.name}" /></td>
	                                <td><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${listValue.amountPaid}" /></td>
	                                <td><c:out value="${listValue.payMode}" /></td>
	                                <td><c:out value="${listValue.departmentName}" /></td>
	                                <td>
	                                	<fmt:formatDate pattern = "yyyy-MM-dd" var="date"   value = "${listValue.paymentPaidDate}"  />
	                                	<c:out value="${date}" />
	                                </td>
	                            </tr>
	                         </c:when>
	                            <c:when test="${listValue.departmentName == 'SalesMan'}">  
	                            <tr  id="row${listValue.srno}" class="teal lighten-5">
	                                <td>
	                                <input type="hidden" value="<c:out value="${listValue.genId}" />" id="idd${listValue.srno}">
	                                <c:out value="${listValue.srno}" /></td>
	                                <td><a title="View Salesman Details" href="${pageContext.servletContext.contextPath}/getEmployeeView?employeeDetailsId=${listValue.id}"><c:out value="${listValue.genId}" /></a></td>
	                                <td><c:out value="${listValue.name}" /></td>
	                                <td><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${listValue.amountPaid}" /></td>
	                                <td><c:out value="${listValue.payMode}" /></td>
	                                <td><c:out value="${listValue.departmentName}" /></td>
	                                <td>
	                                	<fmt:formatDate pattern = "yyyy-MM-dd" var="date"   value = "${listValue.paymentPaidDate}"  />
	                                	<c:out value="${date}" />
	                                </td>
	                            </tr>
	                         </c:when>
	                         <c:when test="${listValue.departmentName == 'GateKeeper'}">  
	                            <tr  id="row${listValue.srno}" class="lime lighten-5">
	                                <td>
	                                <input type="hidden" value="<c:out value="${listValue.genId}" />" id="idd${listValue.srno}">
	                                <c:out value="${listValue.srno}" /></td>
	                                <td><a title="View Gatekeeper Details" href="${pageContext.servletContext.contextPath}/getEmployeeView?employeeDetailsId=${listValue.id}"><c:out value="${listValue.genId}" /></a></td>
	                                <td><c:out value="${listValue.name}" /></td>
	                                <td><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${listValue.amountPaid}" /></td>
	                                <td><c:out value="${listValue.payMode}" /></td>
	                                <td><c:out value="${listValue.departmentName}" /></td>
	                                <td>
	                                	<fmt:formatDate pattern = "yyyy-MM-dd" var="date"   value = "${listValue.paymentPaidDate}"  />
	                                	<c:out value="${date}" />
	                                </td>
	                            </tr>
	                         </c:when>
	                        </c:choose>
                          </c:forEach>
                          </c:if>
                        </tbody>
                    </table>
                </div>



    </main>
    <!--content end-->
</body>

</html>