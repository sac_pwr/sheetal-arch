<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<html>

<head>
    <%@include file="components/header_imports.jsp" %>

<script type="text/javascript">
var myContextPath = "${pageContext.request.contextPath}";
var boothCheck=false;
var gstinCheck=false;

$(document).ready(function() {
	$('#mobileNo').keypress(function( event ){
		
	    var key = event.which;
	    
	    if( ! ( key >= 48 && key <= 57 || key === 13 ) )
	        event.preventDefault();
	}); 
	 $('#telephoneNo').keypress(function( event ){
		    var key = event.which;
		    
		    if( ! ( key >= 48 && key <= 57 || key === 13 ) )
		        event.preventDefault();
		}); 
	$("#boothno").on("blur keyup",function(){
		var boothNo=$("#boothno").val();
		if(boothNo== null || boothNo== ''){
			$("#boothno").removeClass("invalid");
			$("#boothno").removeClass("valid");
			boothCheck=false;
		}
		else{
		$.ajax({
			/* please kindly check the url */				
			url : myContextPath+"/checkBoothNoAvailabilityForUpdate?boothNo="+boothNo,
			beforeSend: function() {
				$('.preloader-background').show();
				$('.preloader-wrapper').show();
	           },
			success : function(data) {
				if(data==="Failed")
				{
					$("#boothno").addClass("invalid");
					boothCheck=true;
				}
				else
				{
					$("#boothno").addClass("valid");
					boothCheck=false;
				}
				$('.preloader-wrapper').hide();
				$('.preloader-background').hide();
			},
			error: function(xhr, status, error) {
				$('.preloader-wrapper').hide();
				$('.preloader-background').hide();
				  //alert(error +"---"+ xhr+"---"+status);
				$('#addeditmsg').modal('open');
       	     	$('#msgHead').text("Message : ");
       	     	$('#msg').text("Something Went Wrong"); 
       	     		setTimeout(function() 
					  {
	     					$('#addeditmsg').modal('close');
					  }, 1000);
				}
			});
		}
	});
	 $("#gstno").on("blur keyup",function(){
			var gstno = $("#gstno").val();
			if(gstno== null || gstno== ''){
				$("#gstno").removeClass("invalid");
				$("#gstno").removeClass("valid");
			}
			else{
			$.ajax({
				/* please kindly check the url */				
				url : myContextPath+"/checkGSTInAvailabilityForUpdate?gstNo="+gstno,
				//dataType : "json",
				success : function(data) {
					if(data==="Failed")
					{
			        		$("#gstno").addClass("invalid");
			        		gstinCheck=true;
					}
					else
					{
						$("#gstno").addClass("valid");
		        		gstinCheck=false;
					}
					$('.preloader-wrapper').hide();
					$('.preloader-background').hide();
				},
				error: function(xhr, status, error) {
					$('.preloader-wrapper').hide();
					$('.preloader-background').hide();
					  //alert(error +"---"+ xhr+"---"+status);
					$('#addeditmsg').modal('open');
	       	     	$('#msgHead').text("Message : ");
	       	     	$('#msg').text("Something Went Wrong"); 
	       	     		setTimeout(function() 
						  {
		     					$('#addeditmsg').modal('close');
						  }, 1000);
					}
				});
			}
		});
	
	$("#routeId").change(function(){
		if($(this).val()!=='0')
		{
			 $('#errorMsg').text("");
		}
	});
		
	$('#updateBusinessSubmit').click(function(){
	    var route=$('#routeId').val();
	
		if(route === '0' )
		{		
		     $('#errorMsg').text("Please Select Route to Save Business");
			return false;
		}
		
		if(boothCheck==true)
		{
			$("#boothno").addClass("invalid");
			return false;
		}
		if(gstinCheck==true)
		{
			$("#gstno").addClass("invalid");
			return false;
		}
	});
	
	
	
	/* 
	var msg="${saveMsg}";
	 //alert(msg);
	 if(msg!='' && msg!=undefined)
	 {
	     $('#addeditmsg').modal('open');
	     $('#msgHead').text("Business Message");
	     $('#msg').text(msg);
	 } */
	
	
});

</script>
<style>
	.container {
    width: 80% !important;
}
</style>
</head>

<body >
   <!--navbar start-->
   	<%@include file="components/navbar.jsp" %>
    <!--navbar end-->
    <!--content start-->
    <main>
        <div class="container">
            <form action="${pageContext.servletContext.contextPath}/updateBooth" method="post" id="updateBusinessForm">
            <input type="hidden" name="boothId" id="boothId" value="${booth.boothId}">
                <div class="row  z-depth-3">
                    <div class="col l12 m12 s12">
                        <h5 class="center"> Business Details </h5>
                    </div>
                      <div class="input-field col s12 m3 l3 offset-l2 offset-m2">
                    		<i class="material-icons prefix">person</i>
                        <input id="boothno" type="text" class="validate" name="boothNo" value="${booth.boothNo}" title="Enter Booth Number" required>
                        <label for="boothno" class="active" data-error="Booth number already Exist" data-success="Booth number available"><span class="red-text">*</span>Booth No</label>
                    		
                    	</div>
                      <div class="input-field col s12 m3 l3">
                        <i class="material-icons prefix">person</i>
                        <input id="ownername" type="text" class="validate" name="ownerName" value="${booth.ownerName}"  title="Enter Owner Name" required>
                        <label for="ownername" class="active"><span class="red-text">*</span>Owner Name</label>

                    </div>
                    	
                    	 <div class="input-field col s12 m3 l3">
                        <i class="material-icons prefix">store</i>
                        <input id="shopname" type="text" class="validate" name="shopName"  value="${booth.shopName}" required>
                        <label for="shopname" class="active"><span class="red-text">*</span>Shop Name</label>

                    </div> 
                    	<div class="input-field col s12 m3 l3 offset-l2 offset-m2">
                    		<i class="material-icons prefix">person</i>
                        <input id="gstno" type="text" class="validate" name="gstNo"   value="${booth.gstNo}"   title="Enter GST Number" minlength="15" maxlength="15">
                        <label for="gstno"  class="active" data-error="Gst number already Exist" data-success="Gst number available">GST No.</label>
                    		
                    	</div>
                      <div class="input-field col s12 m3 l3">
                        <i class="material-icons prefix">gps_fixed</i>
                        <label for="routeId" class="active" title="this field is mandatory"><span class="red-text">*</span></label>
                        <select name="routeId" id="routeId" required>
                                 <option value="0" selected>Choose Route</option>
                                <c:if test="${not empty routeList}">
							<c:forEach var="listValue" items="${routeList}">
								<option value="<c:out value="${listValue.routeId}" />" ${listValue.routeId == booth.route.routeId ? 'selected' : ''}><c:out
										value="${listValue.routeNo}" /></option>
							</c:forEach>
						</c:if>
                        </select>
                        <span class="red-text" id="errorMsg"></span>
                    </div>
                </div>
                <div class="row  z-depth-3">
                    <div class="col l12 m12 s12">
                        <h5 class="center"> Contact Details </h5>
                    </div>
                     <div class="input-field col s12 m3 l3 offset-l2 offset-m2">
                        <i class="fa fa-volume-control-phone prefix" aria-hidden="true"></i>
                        <input id="telephoneNo" type="tel" name="telephoneNumber"   value="${booth.contact.telephoneNumber}"   minlength="10" maxlength="11">
                        <label for="telephoneNo" class="active">Telephone No.</label>
                    </div>

                      <div class="input-field col s12 m3 l3">
                        <i class="material-icons prefix">stay_current_portrait</i>
                        <input id="mobileNo" type="tel" name="mobileNumber"  value="${booth.contact.mobileNumber}"   required minlength="10" maxlength="10">
                        <label for="mobileNo" class="active">Mobile No.</label>
                    </div>

                  <div class="input-field col s12 m3 l3">
                        <i class="material-icons prefix">mail</i>
                        <input id="emailId" type="email" name="emailId" value="${booth.contact.emailId}">
                        <label for="emailId" class="active">Email Id</label>
                    </div>
                     <div class="input-field col s12 m4 l4 offset-l2 offset-m2">
                        <i class="material-icons prefix">store</i>
                        <textarea id="address" class="materialize-textarea" name="address" required><c:out value="${booth.address}" /></textarea>                    
                        <label for="address" class="active"><span class="red-text">*</span>Address</label>

                    </div>
                    
                </div>
               

               <!--  <div class="row  z-depth-3">
                    <div class="col l12 m12 s12">
                        <h4 class="center">Credentials </h4>
                    </div>
                   <div class="input-field col s12 m5 l5 push-l1 push-m1">
                        <i class="material-icons prefix">rate_review</i>
                        <input id="gstNo" type="text" name="gstinNumber" minlength="15" maxlength="15" required>
                        <label for="gstNo" class="active">Enter GST No.</label>

                    </div>

                    <div class="input-field col s12 m5 l5 push-l1 push-m1">
                        <i class="material-icons prefix">credit_card</i>

                        <input id="creditLimit" type="text" name="creditLimit" required>
                        <label for="creditLimit" class="active">Enter Credit Limit</label>

                    </div>
                </div> -->
                <div class="input-field col s12 m6 l4 offset-l5 center-align">
                    <button class="btn waves-effect waves-light blue-gradient" type="submit" id="updateBusinessSubmit">Update Business<i class="material-icons right">send</i> </button>
                </div>
                <br>
            </form>

        </div>

		
			 <div class="row">
			<div class="col s12 m12 l12">
				<div id="addeditmsg" class="modal" style="width:40%;">
					<div class="modal-content">
						<h5 id="msgHead"></h5>
						<hr>
						<h6 id="msg"></h6>
					</div>
					<div class="col s8 m2 l2 offset-l5 modal-footer">
						<a href="#!"
							class="modal-action modal-close waves-effect waves-green btn blue-gradient">OK</a>
					</div>
				</div>
			</div>
		</div>



		

    </main>
    <!--content end-->
</body>

</html>