<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<html>

<head>

    <%@include file="components/header_imports.jsp" %>
    <!-- <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
   <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script> -->
    <script>var myContextPath = "${pageContext.request.contextPath}"</script>
	<script type="text/javascript" src="resources/js/updateEmployee.js"></script>

        <script>
        $(document).ready(function() {
            $('select').material_select();
            $(".eye-slash").hide();
            $(".eye").click(function() {
                $(".eye").hide();
                $(".eye-slash").show();
                $("#password").attr("type", "text");
            });
            $(".eye-slash").click(function() {
                $(".eye-slash").hide();
                $(".eye").show();
                $("#password").attr("type", "password");
            });
            $(".eye-slash1").hide();
            $(".eye1").click(function() {
                $(".eye1").hide();
                $(".eye-slash1").show();
                $("#Cnfrmpassword").attr("type", "text");
            });
            $(".eye-slash1").click(function() {
                $(".eye-slash1").hide();
                $(".eye1").show();
                $("#Cnfrmpassword").attr("type", "password");
            });
            
            $('#basicSalary').keydown(function(e){            	
				-1!==$.inArray(e.keyCode,[46,8,9,27,13,110,190])||/65|67|86|88/.test(e.keyCode)&&(!0===e.ctrlKey||!0===e.metaKey)||35<=e.keyCode&&40>=e.keyCode||(e.shiftKey||48>e.keyCode||57<e.keyCode)&&(96>e.keyCode||105<e.keyCode)&&e.preventDefault()
			 });
            
			document.getElementById('basicSalary').onkeypress=function(e){
            	
            	if (e.keyCode === 46 && this.value.split('.').length === 2) {
              		 return false;
          		 }

            }
			
			/* $('#cluster').material_select('destroy');
	           $('#cluster').select2({
	               placeholder: "Choose Cluster",
	              allowClear: true, 
	               width: "100%"	               
	           }); */
	           $("#tableCluster").hide();
	           $("#areaDiv").hide();
	           $("#routeDiv").hide();
	          /*  $('#clusterMultiple').hide(); */
	           
			$("#departmentid").change(function(){
				
				$('#departmentidLabel').text("");
				
				var deptName=$("#departmentid option:selected").text();
				
			/* 	$('#cluster').val(null).trigger('change'); */
				//$('#SelectSingleCluster').val('0');
				
				if(deptName==="GateKeeper"){
			/* 	$('#clusterSingle').hide(); */
				/* $('#clusterMultiple').show(); */
				   $("#areaDiv").hide(); 
		           $("#routeDiv").hide();
		           $("#tableCluster").show();
				}
				else if(deptName==="AreaSalesMan"){
					/* $('#clusterMultiple').hide();
					$('#clusterSingle').show(); */
					$("#routeDiv").hide();  
					$("#areaDiv").show();
					$("#tableCluster").hide(); 
					
				
				} 
				else if(deptName==="SalesMan"){
					/* $('#clusterMultiple').hide();
					$('#clusterSingle').show(); */
					
					$("#routeDiv").show();
					$("#areaDiv").show();
					$("#tableCluster").hide();
				} 
			});
			 $("#SelectSingleCluster").change(function(){
	        	 $('.clusterLabel').text('');
	         });
	         $("#cluster").change(function(){
	        	 $('.clusterLabel').text('');
	         });
	         $("#areaId").change(function(){
	        		$('#areaLabel').text('');
	         });
	         $("#routeId").change(function(){
	        	 $('#routeLabel').text('');
	         }); 
	         
	        $("#departmentid").change();
	         
        	 var departmentName="${employeeDetailsForUpdate.employee.department.name}";
        	 if(departmentName==="AreaSalesMan")
        	 {
        		 var clusterId = $("#SelectSingleCluster");
        		 clusterId.val("${employeeRouteList.get(0).area.cluster.clusterId}");
        		 clusterId.change(); 
        		 setTimeout( function() 
   	     		 { 
	        	 	var areaId = $("#areaId");
	        	 	areaId.val("${employeeRouteList.get(0).area.areaId}");
	        	 	areaId.change();	
   	     		 },1000);
        	 }
        	 else if(departmentName==="SalesMan")
        	 {
        		 var clusterId = $("#SelectSingleCluster");
        		 clusterId.val("${employeeRouteList.get(0).route.area.cluster.clusterId}");
        		 clusterId.change();
        		 
        		setTimeout( function() 
     			{
        		 var areaId = $("#areaId");
        	 	 areaId.val("${employeeRouteList.get(0).route.area.areaId}");
        	 	 areaId.change();	
     			}, 1000);
        	 	setTimeout( function() 
				{
        	 		var routeId = $("#routeId");
					routeId.val("${employeeRouteList.get(0).route.routeId}");
        	 		routeId.change();
				}, 2000);
        	 }
	         
	         
        });
        
       
    </script>
    <style>
        .eye .eye-slash .eye1 .eye-slash1 {
            transform: translateY(-16%);
        }
        #CnfrmpasswordLabel {
           margin-left: 14%;
           font-size: 13px;
       }
       #mobileNumberlabel{
           margin-left: 14%;
           font-size: 13px;
       }
      /*  .select2-container {
       display:content !important;
       } */
       .select2-container--default .select2-selection--multiple {
    background-color: white;
    border:none;
    border-bottom: 1px solid #aaa !important;
    border-radius: 0px;
    cursor: text;
}
	.select2-container--default.select2-container--focus .select2-selection--multiple {
	border:none;
    border-bottom: solid black 1px;
    outline: 0;
}
    </style>
</head>

<body>
   <!--navbar start-->
   	<%@include file="components/navbar.jsp" %>
    <!--navbar end-->
    <!--content start-->
    <main>
        <br>
        <div class="container">
            <form action="${pageContext.servletContext.contextPath}/updateEmployee" method="post" id="saveEmployeeForm">
            <input id="employeeId" type="hidden" class="validate" value="${employeeDetailsForUpdate.employee.employeeId}" name="employeeId">
            <input id="employeeDetailsId" type="hidden" class="validate" value="${employeeDetailsForUpdate.employeeDetailsId}" name="employeeDetailsId">
                <div class="row  z-depth-3">
                    <div class="col l12 m12 s12">
                        <h4 class="center">Personal Details </h4>
                    </div>
                   <div class="input-field col s12 m5 l5 push-l1 push-m1 ">
                        <i class="material-icons prefix">person</i>
                        <input id="name" type="text" class="validate" name="employeeName" value="${employeeDetailsForUpdate.name}" required>
                        <label for="name" class="active"><span class="red-text">*</span>Name</label>
                    </div>

                   <div class="input-field col s12 m5 l5 push-l1 push-m1 ">
                        <i class="material-icons prefix">stay_current_portrait</i>
                        <input id="mobileNo" type="tel" class="validate" minlength="10" maxlength="10" name="mobileNumber"  value="${employeeDetailsForUpdate.contact.mobileNumber}" required style="transform:translate(0,10px)">
                        <label for="mobileNo" class="active"><span class="red-text">*</span>Mobile No.</label>
                        <span id="mobileNumberlabel" class="red-text"></span>
                    </div>

                   <div class="input-field col s12 m5 l5 push-l1 push-m1 ">
                        <i class="material-icons prefix">mail</i>
                        <input id="emailId" type="email" class="validate" name="emailId"  value="${employeeDetailsForUpdate.contact.emailId}">
                        <label for="emailId" data-error="wrong" data-success="right"  class="active">Email Id</label>
                    </div>
                    <div class="input-field col s12 m5 l5 push-l1 push-m1 ">
                        <i class="material-icons prefix">location_on</i>
                        <textarea id="textarea1" class="materialize-textarea" name="address" required> <c:out value="${employeeDetailsForUpdate.address}"/></textarea>
                        <label for="textarea1"><span class="red-text">*</span>Address</label>
                    </div>
                </div>
               
                <div class="row  z-depth-3">
                    <div class="col l12 m12 s12">
                        <h4 class="center"> Work Details </h4>
                    </div>
                 <div class="input-field col s12 m3 l3 offset-l1">
                        <i class="fa fa-money prefix" aria-hidden="true"></i>
                        <input id="basicSalary" type="text" class="validate" name="basicSalary" required value="${employeeDetailsForUpdate.basicSalary}" />
                        <label for="basicSalary" class="active"><span class="red-text">*</span>Salary</label>
                    </div>
                    <div class="input-field col s12 m3 l3">
                      <i class="material-icons prefix">view_stream</i>
                       <label for="departmentid" class="active"><span class="red-text">*</span></label>
                        <select id="departmentid" name="departmentId">
                                 <option value="0" selected>Choose Department</option>
                                 <c:if test="${not empty departmentList}">
									<c:forEach var="listValue" items="${departmentList}">
		                                   <option value="${listValue.departmentId}"  ${listValue.departmentId == employeeDetailsForUpdate.employee.department.departmentId ? 'selected' : ''}><c:out value="${listValue.name}" /></option>
		                             </c:forEach>
		                        </c:if>
                        </select>
                          <span id="departmentidLabel" class="red-text"></span>
                    </div>
           <%--          <div class="input-field col s12 m4 l3" id="clusterMultiple" style="margin-top:3%;">
                        <i class="material-icons prefix">location_on</i>
                     
                        <select name="clusterIds" id="cluster" class="selectCluster" multiple>
                                 <option value="0" >Choose Cluster</option> 
                                 <c:if test="${not empty clusterList}">
									<c:forEach var="listValue" items="${clusterList}">
										<option value="<c:out value="${listValue.clusterId}" />"><c:out value="${listValue.name}" /></option>
									</c:forEach>
								</c:if> 
                        </select>
                         <span id="clusterLabel" class="red-text clusterLabel"></span>
                        </div> --%>
                           <div class="input-field col s12 m4 l3" id="clusterDiv">
                            <i class="material-icons prefix">location_on</i>
                            <label for="SelectSingleCluster" class="active"><span class="red-text">*</span></label>
                       		   <select name="cluster" id="SelectSingleCluster" class="selectCluster">
                                 <option value="0" selected>Choose Cluster</option> 
                                 <c:if test="${not empty clusterList}">
									<c:forEach var="listValue" items="${clusterList}">									
										<option value="<c:out value="${listValue.clusterId}" />"><c:out value="${listValue.name}" /></option>
									</c:forEach>
								</c:if>  
                        </select>
                      		  <span id="clusterLabel" class="red-text clusterLabel"></span>
                         </div>
                    <div class="col s12 m12 l12">
                    
                     
                    <div class="input-field col s12 m4 l3 offset-l1" id="areaDiv">
                        <i class="material-icons prefix">location_on</i>
                     	<label for="areaId" class="active"><span class="red-text">*</span></label>
                        <select name="areaId" id="areaId">
                                 <option value="0" selected>Choose Area</option>                                
                        </select>
                         <span id="areaLabel" class="red-text"></span>
                    </div>
                     <div class="input-field col s12 m4 l3" id="routeDiv">
                        <i class="material-icons prefix">location_on</i>
                     	<label for="routeId" class="active"><span class="red-text">*</span></label>
                        <select name="routeId" id="routeId">
                                 <option value="0" selected>Choose Route</option>
                        </select>
                        <span id="routeLabel" class="red-text"></span>
                    </div>
                   
                     <!-- <div class="input-field col s12 m2 l2 offset-l1">
                      <button class="btn waves-effect waves-light blue darken-8 center" type="button">Ok</button>
                    </div> -->
                  </div>
                   <div class="input-field col s12 m6 l6 offset-l3 offset-m3 center-align" id="tableCluster">
                        <table class="centered tblborder" id="areatable">
                            <thead>
                                <tr>
                                    <th>Sr.No</th>
                                    <th>Cluster</th>
                                    <th>Cancel</th>
                                </tr>
                            </thead>
                            <tbody id="tblDataCluster">
                            <% int rowincrement=0; %>
                             <c:if test="${not empty clusterIdForGK}">
									<c:forEach var="listValue" items="${clusterIdForGK}">
									<c:set var="rowincrement" value="${rowincrement + 1}" scope="page"/>
			                            <tr>
			                            <td><c:out value="${rowincrement}" /></td>
			                            <td><c:out value="${listValue.name}" /></td>
			                            <td><button  type='button'  onclick='removecluster(${listValue.clusterId})' class='btn btn-flat'><i class='material-icons'>clear</i></button></td>
			                            </tr> 
			                         </c:forEach>
			                  </c:if>
                            </tbody>
                        </table>
                        <br><br>
                    </div>

                </div>

                <div class="row  z-depth-3">
                    <div class="col l12 m12 s12">
                        <h4 class="center"> Login Credentials </h4>
                    </div>
                    <div class="input-field col s12 m6 l6 push-l2 push-m2 offset-m1 offset-l1 ">
                        <i class="material-icons prefix">person</i>
                        <input id="userId" type="text" class="validate" name="userId" required   value="${employeeDetailsForUpdate.employee.userId}">
                        <label for="userId" id="useridvalid" data-error="UserId Required" data-success="" class="active"><span class="red-text">*</span>User Id</label>
					 </div>	
					               
					             

                    <div class="input-field col col s12 m6 l6 push-l2 push-m2 offset-m1 offset-l1 ">

                        <i class="material-icons prefix">vpn_key</i>

                        <input id="password"  type="password" name="password" required value="${employeeDetailsForUpdate.employee.password}">
                        <label for="password" class="active"><span class="red-text">*</span>Password</label>

                    </div>
                   <div class="input-field col s12 m1 l1 push-l1 push-m1 " style="transform:translate(0,10px)">
                        <i class="fa fa-eye  right eye" aria-hidden="true"></i>
                        <i class="fa  fa-eye-slash right eye-slash" aria-hidden="true"></i>
                    </div>

                   <div class="input-field col s12 m6 l6 push-l2 push-m2 offset-m1 offset-l1">
                        <i class="material-icons prefix">vpn_key</i>

                        <input id="Cnfrmpassword" type="password" name="confirmpass" required style="margin-bottom:5px;"  value="${employeeDetailsForUpdate.employee.password}">
                        <label for="Cnfrmpassword" class="active"><span class="red-text">*</span>Confirm Password</label>
                        <span id="CnfrmpasswordLabel" class="red-text"></span>
                         
                    </div>
                     <div class="input-field col s12 m1 l1 push-l1 push-m1" style="transform:translate(0,10px)">
                        <i class="fa fa-eye   eye1 right" aria-hidden="true"></i>
                        <i class="fa  fa-eye-slash  eye-slash1 right" aria-hidden="true"></i>
                   
                    </div>
                  
                </div>
                <div class="row z-depth-3">
                   <div class="col s12 l12 m12">
                   
                   <br>
                       <div class="col s12 l5 m5 push-l1 push-m1">
                           <h6>Added Date : <c:out value="${employeeDetailsForUpdate.employeeDetailsAddedDatetime}" /></h6>
                       </div>
                       <div class="col s12 l5 m5 push-l1 push-m1">
                           <h6>Last Updated Date : <c:out value="${employeeDetailsForUpdate.employeeDetailsUpdatedDatetime}" /></h6>
                       </div>
                       <br> <br>
                   </div>
                    
               </div>
                <div class="input-field col s12 m6 l4 offset-l5 center-align">
                    <button class="btn waves-effect waves-light blue darken-8" type="submit" id="saveEmployeeSubmit">Update Employee<i class="material-icons right">send</i> </button>
                </div>
                <br>
            </form>
 
        </div>



			<div class="row">
			<div class="col s12 m12 l12">
				<div id="addeditmsg" class="modal" style="width:40%;">
					<div class="modal-content">
						<h5 id="head"></h5>
						<hr>
						<h6 id="msg"></h6>
					</div>
					<div class="col s8 m2 l2 offset-l5 modal-footer">
						<a href="#!"
							class="modal-action modal-close waves-effect waves-green btn blue-gradient">OK</a>
					</div>
				</div>
			</div>
		</div>

    </main>
    <!--content end-->
</body>

</html>