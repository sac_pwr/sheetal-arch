<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<html>

<head>
      <%@include file="components/header_imports.jsp" %>
</head>

<body>
    <!--navbar start-->
   	<%@include file="components/navbar.jsp" %>
    <!--navbar end-->
    <!--content start-->
    <main>
       
        <form action="">

            <table class="striped highlight centered col s12 l8 m12" id="tblData" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th class="print-col">Sr.No.</th>
                        <th class="print-col">Id</th>
                        <th class="print-col"> Name</th>
                        <th class="print-col">Total Amount</th>
                        <th class="print-col">Amount Paid</th>
                        <th class="print-col">Amount Remaining</th>
                        <th>View </th>
                    </tr>
                </thead>

                <tbody>
                <c:if test="${not empty paymentPendingLists}">
				<c:forEach var="listValue" items="${paymentPendingLists}">
			   <!-- <script type="text/javascript">
						alert("${listValue.name}");					
					</script> -->
					<c:choose>  
					    <c:when test="${listValue.type == 'Supplier'}">  
						       <tr class="orange lighten-5">
		                        <td><c:out value="${listValue.srno}" /></td>
		                        <td><a title="View Supplier Details" href="${pageContext.servletContext.contextPath}/fetchSupplierListBySupplierId?supplierId=${listValue.id}"><c:out value="${listValue.id}" /></a></td>
		                        <td><c:out value="${listValue.name}" /></td>
		                        <td>&#8377;<fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${listValue.totalAmount}" /></td>
		                        <td>&#8377;<fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${listValue.amountPaid}" /></td>
		                        <td>&#8377;<fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${listValue.amountRemaining}" /></td>
		                        <td><a title="View Order Details" href="${pageContext.servletContext.contextPath}/fetchInventoryReportView?supplierId=${listValue.id}">Order Details</a></td>
		                       </tr> 
					    </c:when>  
					    <c:when test="${listValue.type == 'GateKeeper'}">  
					           <tr class="lime lighten-5">
		                        <td><c:out value="${listValue.srno}" /></td>
		                        <td><a title="View Gatekeeper Details" href="${pageContext.servletContext.contextPath}/getEmployeeView?employeeDetailsId=${listValue.id}"><c:out value="${listValue.genId}" /></a></td>
		                        <td><c:out value="${listValue.name}" /></td>
		                        <td>&#8377;<fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${listValue.totalAmount}" /></td>
		                        <td>&#8377;<fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${listValue.amountPaid}" /></td>
		                        <td>&#8377;<fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${listValue.amountRemaining}" /></td>
		                        <td><a title="View Payment Details" href="${pageContext.servletContext.contextPath}/viewEmployeeSalary?employeeDetailsId=${listValue.id}">Payment Info</a></td>
		                       </tr>   
					    </c:when>  
					    <c:when test="${listValue.type == 'SalesMan'}">  
					           <tr class="teal lighten-5">
		                        <td><c:out value="${listValue.srno}" /></td>
		                        <td><a title="View Salesman Details" href="${pageContext.servletContext.contextPath}/getEmployeeView?employeeDetailsId=${listValue.id}"><c:out value="${listValue.genId}" /></a></td>
		                        <td><c:out value="${listValue.name}" /></td>
		                        <td>&#8377;<fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${listValue.totalAmount}" /></td>
		                        <td>&#8377;<fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${listValue.amountPaid}" /></td>
		                        <td>&#8377;<fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${listValue.amountRemaining}" /></td>
		                        <td><a title="View Payment Details" href="${pageContext.servletContext.contextPath}/viewEmployeeSalary?employeeDetailsId=${listValue.id}">Payment Info</a></td>
		                       </tr>   
					    </c:when>  
					    <c:when test="${listValue.type == 'DeliveryBoy'}">  
					           <tr class="pink lighten-5">
		                        <td><c:out value="${listValue.srno}" /></td>
		                        <td><a title="View Deliveryboy Details" href="${pageContext.servletContext.contextPath}/getEmployeeView?employeeDetailsId=${listValue.id}"><c:out value="${listValue.genId}" /></a></td>
		                        <td><c:out value="${listValue.name}" /></td>
		                        <td>&#8377;<fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${listValue.totalAmount}" /></td>
		                        <td>&#8377;<fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${listValue.amountPaid}" /></td>
		                        <td>&#8377;<fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${listValue.amountRemaining}" /></td>
		                        <td><a title="View Payment Details" href="${pageContext.servletContext.contextPath}/viewEmployeeSalary?employeeDetailsId=${listValue.id}">Payment Info</a></td>
		                       </tr>   
					    </c:when>   
					</c:choose>  

				</c:forEach>
				</c:if>
                </tbody>
            </table>

        </form>
    </main>
    <!--content end-->
</body>

</html>