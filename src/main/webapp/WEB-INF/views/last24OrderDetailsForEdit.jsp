<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<html>

<head>
        <%@include file="components/header_imports.jsp" %>
        <script>var myContextPath = "${pageContext.request.contextPath}"</script>
      <script type="text/javascript" src="resources/js/hashtable.js"></script>
      <script type="text/javascript" src="resources/js/manageInventory.js"></script>
 

    
    <style>
      
	#order
	{
	width:90% !important;
	max-height:80% !important;
	}
	.dropdown-content{
	max-height:250px !important;
	}
    </style>
</head>

<body>
    <!--navbar start-->
   	<%@include file="components/navbar.jsp" %>
    <!--navbar end-->
    <!--content start-->
    <main>
       
        
        
        <div class="row">
            
            <div class="col s12 l12 m12 ">
                <table class="striped highlight bordered centered " id="tblData" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th class="print-col">Sr.No</th>
                            <th class="print-col">Order Id</th>
                            <th>Order Details</th>
                            <th class="print-col">Total Amount</th>
                            <th class="print-col">Total Amount With Tax</th>
                            <th class="print-col">Order Date</th>
                            <th>Edit Order</th>
                        </tr>
                    </thead>

                    <tbody>
                    <% int rowincrement=0; %>
                   <c:if test="${not empty last24Order}">
					<c:forEach var="listValue" items="${last24Order}">
					<c:set var="rowincrement" value="${rowincrement + 1}" scope="page"/>
                        <tr>
                            <td><c:out value="${rowincrement}" /></td>
                            <td><c:out value="${listValue.supplierOrderId}" /></td>
                            <td><button onclick="showOrderDetails('${listValue.supplierOrderId}')" class="btn blue-gradient">View</button></td>
                            <td><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${listValue.totalAmount}" /></td>
                            <td><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${listValue.totalAmountWithTax}" /></td>
                            <td>
                            	<fmt:formatDate pattern = "yyyy-MM-dd" var="date"   value = "${listValue.supplierOrderDatetime}"  />
		                        <c:out value="${date}" />
                            </td>
                            <td><a href="${pageContext.servletContext.contextPath}/SupplierOrderProducts?orderStatus=edit&supplierOrderIddd=${listValue.supplierOrderId}" class="btn blue-gradient">Edit</a></td>
                        </tr>
					</c:forEach>
					</c:if>
                    </tbody>
                </table>
            </div>
        </div>

        <!-- Modal Structure for View Product Details -->
        <div id="product" class="modal">
            <div class="modal-content">
                <h4 class="center">Product Details</h4>
                <hr>
                <br>
                <table border="2" class="centered tblborder" >
                    <thead>
                        <tr>
                            <th>Sr.No</th>
                            <th>Product Name</th>
                            <th>Category</th>
                            <th>Brand</th>
                            <th>Quantity</th>
                            <th>Rate</th>
                            <th>Total Amount</th>
                            <th>Total Amount With Tax</th>
                        </tr>
                    </thead>
                    <tbody id="productOrderedDATA">
                        <!-- <tr>
                            <td>1</td>
                            <td>Mouse</td>
                            <td>I.T</td>
                            <td>BlueSquare</td>
                            <td>5</td>
                            <td>350</td>
                        </tr> -->
                    </tbody>
                </table>
            </div>
            <div class="modal-footer row">

                <div class="col s12 m6 l6 offset-l1">
                    <a href="#!" class="modal-action modal-close waves-effect btn blue-gradient">Close</a>
                </div>

            </div>
        </div>


        <!-- Modal Structure for order -->
        <div id="order" class="modal">
            <div class="modal-content" style="padding:0;">
                <h4 class="center-align">Order Information</h4>
                
                <hr>	
                <p class="center-align">
                    <div class="row">
                        <form action="${pageContext.servletContext.contextPath}/editOrderBook" method="post" id="editOrderBookForm" class="col s12 l12 m12  ">
							<input id="productWithSupplikerlist" type="hidden" class="validate" name="productIdList">
							<input id="supplierOrderIdId" type="hidden" class="validate" name="supplierOrderId">
                        	<input type='hidden' id='currentUpdateRowId'>
                            <div class="col s12 l6 m6">
                            	<div class="input-field col s12 m6 l5 offset-l2">
                                    <select id="supplierIdForOrder" name="supplierId">
                                            <option value="0"  selected>Choose Supplier Name</option>
                                     </select>
                                    <!--<label>Supplier Name</label>-->
                                </div>
                                 <div class="input-field col s12 m6 l5">
                                    <a href="AddSupplier.html" class="modal-action waves-effect waves-green btn" style="margin-top:3%">Add New Supplier</a>
                                </div>
                                <div class="input-field col s12 m6 l5 offset-l2">
                                     <select id="brandIdForOrder" name="brandId">
                                            <option value="0"  selected>Choose Brand</option>
                                           
                                     </select>
                                </div>
                                <div class="input-field col s12 m6 l5">
                                     <select id="categoryIdForOrder" name="categoryId">
                                            <option value="0"  selected>Choose Category</option>
                                            
                                     </select>
                                </div>
                                <div class="col s12 m6 l5 offset-l2">
                                Product
                                     <select id="productIdForOrder" name="productId">
                                            <option value="0"  selected>Choose Product Name</option>
                                            
                                     </select>
                                </div>                                
                               
                                <!-- </div>
                                  <div class="row"> -->
                                <div class="col s12 m6 l3">
                                    Mobile Number <input id="supplierMobileNumber" type="text">
                                    <!-- <label for="mobile"></label> -->
                                </div>
                                <div class="col s12 m6 l2 " style="margin-top:4%">
                                    <input type="checkbox" id="OrderMobileNumberchecker" required/>
                                    <label for="OrderMobileNumberchecker" class="black-text">Edit</label>
                                    <!-- <a href="#!" class="modal-action waves-effect waves-green btn">Edit</a> -->
                                </div>
                                <div class="input-field col s12 m6 l5 offset-l2">
                                    <input id="orderQuantity" type="text" class="validate" required>
                                    <label for="orderQuantity" class="black-text">Quantity</label>
                                </div>
                                 <div class="input-field col s12 m6 l5">
                                    <input id="supplierRateForOrder" type="text" class="validate" required readonly>
                                    <label for="supplierRateForOrder" class="black-text">Rate</label>
                                </div>
                                
                                <div class="input-field col s12 m6 l4 offset-l5">
                                    <button id="addOrderProduct" type="button" name="addOrderCart" class="modal-action waves-effect waves-green btn" style="margin-top:3%">Add Cart</button>
                                </div>
                                
                            </div>


                            <div class="col s12 l6 m6">
                                <table class="tblborder centered">
                                    <thead>
                                        <tr>
                                            <th>Sr No.</th>
                                            <th>Supplier Name</th>
                                            <th>Product</th>
                                            <th>Rate</th>
                                            <th>Quantity</th>
                                            <th>Edit</th>
                                            <th>Cancel</th>
                                        </tr>
                                    </thead>
                                    <tbody id="orderCartTb">
                                        <!-- <tr>
                                            <td>1</td>
                                            <td>Ankit</td>
                                            <td>John</td>
                                            <td>300</td>
                                            <td>5</td>
                                            <td><button type="button" class="btn-flat"><i class="material-icons ">edit</i></button></td>
                                            <td><button type="button" class="btn-flat"><i class="material-icons ">cancel</i></button></td>
                                        </tr> -->
                                    </tbody>
                                </table>
                            </div>

							<br/>
							<br/>
							
							<span id="orderentriesId"></span>
							
                        </form>
                    </div>
                </p>
            </div>
            <div class="divider"></div>
            <div class="modal-footer">
                <div class="row" style="margin-bottom:0;">
                    <div class="col s6 m3 l3 offset-l3 offset-m3  ">
                        <button type="button" id="orderProductsButton" class="modal-action waves-effect  btn">Order Products</button>
                    </div>
                    <div class="col s6 m3 l3 pull-l1 pull-m1">
                        <a href="#!" class="modal-action waves-effect modal-close btn red">close</a>
                    </div>

                </div>
            </div>
        </div>


	<!-- msg pop up -->
		<div class="row">
			<div class="col s8 m2 l2">
				<div id="addeditmsg" class="modal" style="width:40%;">
					<div class="modal-content">
						<h5 id="msgHead">Error</h5>
						<hr>	
						<h6 id="msg"></h6>
					</div>
					<div class="col s8 m2 l2 offset-l5 modal-footer">
						<a href="#!"
							class="modal-action modal-close waves-effect btn blue-gradient">OK</a>
					</div>
				</div>
			</div>
		</div>
    </main>
    <!--content end-->
</body>

</html>