<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<html>

<head>
 <%@include file="components/header_imports.jsp" %>
    

<script type="text/javascript">
    
    $(document).ready(function() {
    	 $(".showQuantity").hide();
         $(".showDates").hide();
         $("#oneDateDiv").hide();
         $(".topProduct").click(function() {
             $(".showQuantity").show();
             $(".showDates").hide();
             $("#oneDateDiv").hide();
         });
        
         $(".rangeSelect").click(function() {
         	$("#oneDateDiv").hide();	
             $(".showDates").show();
             $(".showQuantity").hide();
         });
        
     	$(".pickdate").click(function(){
     		 $(".showQuantity").hide();
     		 $(".showDates").hide();
     		$("#oneDateDiv").show();
     	});
    	
    	var table = $('#tblData').DataTable();	
    	$.fn.dataTable.ext.search.push(function( settings, data, dataIndex ) {
    		       	        
    		        var salesManId=data[4].trim();
    				var salesManId2=$('#clusterIds').val();
    				if(salesManId2==="0")
    				{
    					totalAmount=parseFloat(totalAmount)+parseFloat(data[6].trim());
    					return true;
    				}
    				else if ( salesManId2 === salesManId )
    		        {
    					totalAmount=parseFloat(totalAmount)+parseFloat(data[6].trim());
    		            return true;
    		        }
    		        return false;
    		        
    	});
    	
	    $('#clusterIds').change(function(){
	    	totalAmount=0;
    		table.draw();
    		$('#totalAmountId').text(totalAmount);
    	});
    	
    });
    
    
    function showAreaDetail(id)
	{ 
		$.ajax({
			url : "${pageContext.request.contextPath}/fetchEmployeeRoutes?employeeDetailsId="+id,
			dataType : "json",
			beforeSend: function() {
				$('.preloader-background').show();
				$('.preloader-wrapper').show();
	           },
			success : function(data) {
				
				employeeAreaDetails=data;
				
				employeeAreaList=employeeAreaDetails.routeList;
				var srno=1;
				$("#areaList").empty();
				for(var i=0; i<employeeAreaList.length; i++)
				{					
					routelist=employeeAreaList[i];
					//alert(arealist);
					$('#areaList').append("<tr>"+
					        "<td>"+srno+"</td>"+
					        "<td>"+routelist.routeNo+"</td>"+
					        "<td>"+routelist.area.name+"</td>"+
					        "<td>"+routelist.area.cluster.name+"</td>"+
					        "</tr>"); 
					srno++;
				}
				$('.modal').modal();
				$('#areaDetails').modal('open');
				$('.preloader-wrapper').hide();
				$('.preloader-background').hide();
			},
			error: function(xhr, status, error) {
				$('.preloader-wrapper').hide();
				$('.preloader-background').hide();
				  //alert(error +"---"+ xhr+"---"+status);
				$('#addeditmsg').modal('open');
       	     	$('#msgHead').text("Message : ");
       	     	$('#msg').text("Something went wrong"); 
       	     		setTimeout(function() 
					  {
	     					$('#addeditmsg').modal('close');
					  }, 1000);
				}
		});
	}
    
    </script>
     <style>
      .card-image{
		width:42% !important;
		background-color:#01579b !important;
	}
    </style>
</head>

<body>
   <!--navbar start-->
   	<%@include file="components/navbar.jsp" %>
    <!--navbar end-->
    <!--content start-->
    <main>
       
      <div class="row">
      	<div class="col s12 l3 m3">
      		  <div class="card horizontal">
      		  	<div class="card-image">
       				  <h6 class="white-text" style="padding:8px">Total Amount:</h6>
      			</div>
      			<div class="card-stacked grey lighten-3">
      			  <div class="card-content" style="padding:8px">
       			<h6 class="">&#8377 <span id="totalAmountId"><c:out value="${smReportModel.totalAmount}" /></span></h6>
       			  </div>
        	    </div>
                
          	  </div>
           </div>  
             <div class="col s12 l3 m3">
             	 <div class="card horizontal">
      		  	<div class="card-image">
       				  <h6 class="white-text" style="padding:8px">No of Salesman:</h6>
      			</div>
      			<div class="card-stacked grey lighten-3">
      			  <div class="card-content" style="padding:8px">
       			<h6 class=""><c:out value="${smReportModel.noOfSMandASM}" /></h6>
       			  </div>
        	    </div>
                
          	  </div>
                
            </div>
            <div class="col s12 l2 m2" style="margin-top:10px;">
                <select id="clusterIds">
			      <option value="0" selected>Choose Cluster</option>
                      <c:if test="${not empty smReportModel.clusterList}">
							<c:forEach var="listValue" items="${smReportModel.clusterList}">
								<option value="<c:out value="${listValue.name}" />"><c:out value="${listValue.name}" /></option>
							</c:forEach>
					  </c:if>
			    </select>
            </div>
            <%-- </c:if> --%>
            <c:if test="${orderDetailsList.pageName=='Current'}">
            <div class="col s12 l2 m2 right-align">
                <a href="${pageContext.servletContext.contextPath}/GKOrderDetailsPendingList?areaId=0" class="btn waves-effect waves-light  light-blue darken-4" style="margin-top:7%">Pending Order</a>
            </div>
            </c:if>
			
 			<div class="col s12 l2 m2 right-align" style="margin-top:1%;">
                <div class="col s6 m12 l12 right">
                    <!-- Dropdown Trigger -->
                    <a class='dropdown-button btn waves-effect waves-light  light-blue darken-4' href='#' data-activates='filter'>Filter<i
                class="material-icons right">arrow_drop_down</i></a>
                    <!-- Dropdown Structure -->
                    <ul id='filter' class='dropdown-content'>
                    	<li><a href="${pageContext.request.contextPath}/SMReport?range=yesterday&type=${type}">Yesterday</a></li>
                        <li><a href="${pageContext.request.contextPath}/SMReport?range=last7days&type=${type}">Last 7 Days</a></li>
                        <li><a href="${pageContext.request.contextPath}/SMReport?range=last1month&type=${type}">Last 1 Month</a></li>
                        <li><a href="${pageContext.request.contextPath}/SMReport?range=last3months&type=${type}">Last 3 Month</a></li>
                        <li><a class="rangeSelect">Range</a></li>
                        <li><a class="pickdate">Pick date</a></li>                        
                        <li><a href="${pageContext.request.contextPath}/SMReport?range=viewAll&type=${type}">View All</a></li>
                    </ul>
                </div>
                </div>
                <div class="col s12 l3	m3 right"> 
                 <form action="${pageContext.request.contextPath}/SMReport" method="post">
                    <input type="hidden" name="range" value="range">
                    <input type="hidden" name="type" value="${type}">
                     <span class="showDates">
                     		<div class="input-field col s6 m1 l1 right">
                            <button type="submit">View</button>
                            </div>
                              <div class="input-field col s6 m5 l5 right">
                                 <input type="date" class="datepicker" placeholder="Choose Date" name="endDate" id="endDate" required> 
                                <label for="endDate">To</label>
                              </div>
                              <div class="input-field col s6 m5 l5 right">
                               <input type="date" class="datepicker" placeholder="Choose Date" name="startDate" id="startDate">
                                <label for="startDate">From</label>
                              </div>
                          </span>
                </form>
                 </div>
				<div class="col s12 l2	m2 right" id="oneDateDiv">                            	
                    <form action="${pageContext.request.contextPath}/SMReport" method="post">
                    <input type="hidden" name="range" value="pickdate">
                    <input type="hidden" name="type" value="${type}">
	                    <div class="input-field col s12 m8 l8">
		                    <input type="text" id="oneDate" class="datepicker" placeholder="Choose date" name="startDate">
		                    <label for="oneDate" class="black-text">Pick Date</label>
	                    </div>
	                    <div class="input-field col s12 m2 l2">
	                     <button type="submit">View</button>
                        </div>
                    </form>
               </div>
            
          	
      </div>
        <div class="row">
            <div class="col s12 l12 m12 ">
                <table class="striped highlight centered mdl-data-table display" id="tblData" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th class="print-col">Sr.No</th>
                            <th class="print-col">Salesman Name</th>
                            <th class="print-col">Route No</th>
                            <th class="print-col">Area</th>
                            <th class="print-col">Cluster</th>
                            <th class="print-col">Total No of Order</th>
                            <th class="print-col">Total Amount</th>    
                        </tr>
                        
                    </thead> 
                    <tbody>
                    <c:if test="${not empty smReportModel.smReportDetailsList}">
							<c:forEach var="listValue" items="${smReportModel.smReportDetailsList}">
	                        <tr>
	                             <td><c:out value="${listValue.srNo}"/></td>
	                            <td><a href="${pageContext.request.contextPath}/SMReportOrderDetailsToday?employeeDetailsId=${listValue.employeeDetailsId}"><c:out value="${listValue.salesManName}"/></a></td>
	                            <td><button class="btn blue-gradient" onclick="showAreaDetail(${listValue.employeeDetailsId})">View</button></td>
	                            <td><button class="btn blue-gradient" onclick="showAreaDetail(${listValue.employeeDetailsId})">View</button></td>
	                          	<td><c:out value="${listValue.clusterName}"/></td>
	                            <td><c:out value="${listValue.totalNoOfOrder}"/></td>
	                            <td>
	                            <c:out  value="${listValue.totalAmount}" />
	                            </td>
	                        </tr>
                        </c:forEach>
                        </c:if>
                    </tbody>
                </table>
            </div>
        </div>
			
						<!-- Modal Structure for View Product Details -->

        <div id="areaDetails" class="row modal" style="width:40%;">
            <div class="modal-content">
                <h4 class="center">Product Details</h4>
                <hr>			
               
                	
                   <table border="2" class="centered tblborder">
                    <thead>
                        <tr>
                            <th>Sr.No</th>
                            <th>Route No</th>
                            <th>Area</th>
                            <th>Cluster</th>
                          
                        </tr>
                    </thead>
                    <tbody id="areaList">
                    </tbody>
                </table>
            </div>
          
            <div class="modal-footer">
			
				<div class="col s12 m6 l5 offset-l2">
						 <a href="#!" class="modal-action modal-close waves-effect center btn">Close</a>
				</div>
				
				
			</div>			
               
            </div>
 
				<div class="row">
			<div class="col s8 m2 l2">
				<div id="addeditmsg" class="modal" style="width:40%;">
					<div class="modal-content">
						<h5 id="msgHead">Error</h5>
						<hr>	
						<h6 id="msg"></h6>
					</div>
					<div class="col s8 m2 l2 offset-l5 modal-footer">
						<a href="#!"
							class="modal-action modal-close waves-effect btn grey-text  text-lighten-3 light-blue darken-4">OK</a>
					</div>
				</div>
			</div>
		</div>
	

    </main>
    <!--content end-->
</body>

</html>