<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<html>

<head>
      <%@include file="components/header_imports.jsp" %>
    <script>
    var productList = [];
    var count = 1;
    var gstinNameValidation=false;
        $(document).ready(function() {

            
            $('#mobileNo').keypress(function( event ){
			    var key = event.which;
			    
			    if( ! ( key >= 48 && key <= 57 || key === 13) )
			        event.preventDefault();
			});

            $('#rate').keydown(function(e){            	
				-1!==$.inArray(e.keyCode,[46,8,9,27,13,110,190])||/65|67|86|88/.test(e.keyCode)&&(!0===e.ctrlKey||!0===e.metaKey)||35<=e.keyCode&&40>=e.keyCode||(e.shiftKey||48>e.keyCode||57<e.keyCode)&&(96>e.keyCode||105<e.keyCode)&&e.preventDefault()
			 });
            
			document.getElementById('rate').onkeypress=function(e){
            	
            	if (e.keyCode === 46 && this.value.split('.').length === 2) {
              		 return false;
          		 }

            }
            $("#productAddId").click( function() {
            	
            	var buttonStatus=$('#productAddButton').text();
            	if(buttonStatus==="Update")
            	{
            		updaterow($('#currentUpdateProductId').val());
            		return false;
            	}
            	$("#productAddId").html('<i class="material-icons left">add</i><span id="productAddButton">Add</span>');
                var val = $("#productid").val(); 
                var rate = $('#rate').val();
              	
              	//alert(buttonStatus);
              	if(val==0)
              	{
              		return false;
              	}
              	if(rate==0)
              	{
              		return false;
              	}
              	/* if(jQuery.inArray(val, productidlist) !== -1)
                {	
              		$('#addeditmsg').modal('open');
           	     	$('#msgHead').text("Product Select Warning");
           	     	$('#msg').text("This Product is already added");               	     	
                } */
              	else
           		{
	                
              		
              		//alert(productList.size());
              		/* for (let key of productList.keys()) {
              		    alert(key+"-"+productList[key]);
              		}*/
              		//alert(productList.entries());
              		/* for (let entry of productList.entries()) {
              			alert(entry[0]+"-"+ entry[1]);
              		} */
              		/* for (let [key, value] of productList.entries()) {
              			alert(key+"-"+value);
              			productList.remove(key);
              		} */
	             	//alert(productList.key +"-"+ productList.value);
	             	 
              		for (var i=0; i<productList.length; i++) {  
              			var value=productList[i];  
              			if(value[0]===val && buttonStatus==="Add")
              			{  
              				$('#addeditmsg').modal('open');
                   	     	$('#msgHead').text("Product Select Warning");
                   	     	$('#msg').text("This Product is already added"); 
                   	     	return false;
              			}
              		}
              		var prdData=[val,rate];
              		productList.push(prdData);
	             	/* $('#productratelistinput').val(productRateidlist);
	             	$('#productlistinput').val(productidlist); */
	                var text=$("#productid option:selected").text();
	                var vl=$("#productid option:selected").val();
					//alert(value+"-"+text);
					var rowCount = $('#productcart tr').length;
	                $("#t1").append("<tr id='rowdel_" + count + "' >"+
		               				"<td id='rowcount_" + count + "'>" + count + "</td>"+
		               				"<td id='rowproductname_" + count + "'><input type='hidden' id='rowproductkey_" + count + "' value='"+vl+"'><center><span id='tbproductname_" + count + "'>"+text+"</span></center></td>"+
		               				"<td id='rowproductrate_" + count + "'>" + rate + "</td>"+
		               				"<td id='rowcountproductedit_" + count + "'><button class='btn-flat' type='button' onclick='editrow(" + count + ")'><i class='material-icons '>edit</i></button></td>"+
		               				"<td id='rowdelbutton_" + count + "'><button class='btn-flat' type='button' onclick='deleterow(" + count + ")'><i class='material-icons '>clear</i></button></td>"+
		               				"</tr>");
	                count++;
           		}
              	//alert(productList.entries());
            });

            function updaterow(id) {
            	$("#productAddId").html('<i class="material-icons left">add</i><span id="productAddButton">Add</span>');
                var val = $("#productid").val(); 
                var rate = $('#rate').val();
              	var buttonStatus=$('#productAddButton').text();
              	var text=$("#productid option:selected").text();
                var vl=$("#productid option:selected").val();
              	
              	//alert(buttonStatus);
              	if(val==0)
              	{
              		return false;
              	}
              	if(rate==0)
              	{
              		return false;
              	}
            	
              	//alert($('#rowproductkey_' + id).val());
              	var removeItem=$('#rowproductkey_' + id).val();
              	
              	var productListTemp=[];
              	for(var i=0; i<productList.length; i++)
              	{
              		var value=productList[i];
              		if(removeItem!==value[0])
              		{
              			productListTemp.push(productList[i]);
              		}
              	}
              	productList=[];
              	for(var i=0; i<productListTemp.length; i++)
              	{
              		productList.push(productListTemp[i]);
              	}
              	var prdData=[val,rate];
              	productList.push(prdData);
              	
                //alert('#rowproductkey_'+id);
                
                //alert('removeItem '+$('#rowproductkey_' + id).val());
                //alert('productidlist '+productidlist);
                
                var rowCount = $('#t1 tr').length;
            	//alert(rowCount);
            	var trData="";
            	count=1;
            	for(var i=1; i<=rowCount; i++)
            	{
            		//alert($('#rowcount_'+i).html() +"---"+ $('#rowprocustname_'+i).html() +"---"+ $('#rowdelbutton_'+i).html());
            		
            		if(id!=i)
            		{
            			//alert("predata");
            			//alert(i+"-(----)-"+$('#tbproductname_' + i).text());
    	        		 /* trData=trData+"<tr id='rowdel_" + count + "' >"+
    	           				"<td id='rowcount_" + count + "'>" + count + "</td>"+
    	           				"<td id='rowproductname_" + count + "'><input type='hidden' id='rowproductkey_" + count + "' value='"+$('#rowproductkey_' + i).val()+"'><center><span id='tbproductname_" + count + "'>"+$('#tbproductname_' + i).text()+"</span></center></td>"+
    	           				"<td id='rowdelbutton_" + count + "'><button class='btn-flat' type='button' onclick='deleterow(" + count + ")'><i class='material-icons '>clear</i></button></td>"+
    	           				"</tr>"; */
    	           				trData=trData+"<tr id='rowdel_" + count + "' >"+
    		               				"<td id='rowcount_" + count + "'>" + count + "</td>"+
    		               				"<td id='rowproductname_" + count + "'><input type='hidden' id='rowproductkey_" + count + "' value='"+$('#rowproductkey_' + i).val()+"'><center><span id='tbproductname_" + count + "'>"+$('#tbproductname_' + i).text()+"</span></center></td>"+
    		               				"<td id='rowproductrate_" + count + "'>" + $('#rowproductrate_'+i).text() + "</td>"+
    		               				"<td id='rowcountproductedit_" + count + "'><button class='btn-flat' type='button' onclick='editrow(" + count + ")'><i class='material-icons '>edit</i></button></td>"+
    		               				"<td id='rowdelbutton_" + count + "'><button class='btn-flat' type='button' onclick='deleterow(" + count + ")'><i class='material-icons '>clear</i></button></td>"+
    		               				"</tr>";
    	        		 count++;
            		}
            		else
            		{
            			//alert("newdata");
            			trData=trData+"<tr id='rowdel_" + count + "' >"+
    		               				"<td id='rowcount_" + count + "'>" + count + "</td>"+
    		               				"<td id='rowproductname_" + count + "'><input type='hidden' id='rowproductkey_" + count + "' value='"+vl+"'><center><span id='tbproductname_" + count + "'>"+text+"</span></center></td>"+
    		               				"<td id='rowproductrate_" + count + "'>" + rate + "</td>"+
    		               				"<td id='rowcountproductedit_" + count + "'><button class='btn-flat' type='button' onclick='editrow(" + count + ")'><i class='material-icons '>edit</i></button></td>"+
    		               				"<td id='rowdelbutton_" + count + "'><button class='btn-flat' type='button' onclick='deleterow(" + count + ")'><i class='material-icons '>clear</i></button></td>"+
    		               				"</tr>";
    	        		 count++;
            		}
            		//alert(trData);
            	} 
            	$("#t1").html('');
            	$("#t1").html(trData);
            	//alert(productList.entries());
                //$('#rowdel_' + id).remove();
               // alert('productidlist '+productidlist);

            }
			
            $('#categoryid').change(function() {
    			var brandid = $('#brandid').val();
    			var categoryid = $('#categoryid').val();

    			if(brandid==0)
    			{
    				return false;
    			}
    			
    			if(categoryid==0)
    			{
    				return false;
    			}
    			// Get the raw DOM object for the select box
    			var select = document.getElementById('productid');

    			// Clear the old options
    			select.options.length = 0;

    			//Load the new options

    			select.options.add(new Option("Choose Product", 0));
    			$.ajax({
    				url : "${pageContext.request.contextPath}/fetchProductListByBrandIdAndCategoryId?brandId=" + brandid+"&categoryId="+categoryid,
    				dataType : "json",
    				beforeSend: function() {
						$('.preloader-background').show();
						$('.preloader-wrapper').show();
			           },
    				success : function(data) {

    					/* alert(data); */
    					var options, index, option;
    					select = document.getElementById('productid');

    					for (var i = 0, len = data.length; i < len; ++i) {
    						var product = data[i];
    						select.options.add(new Option(product.productName, product.productId));
    					}

    					/* for (index = 0; index < options.length; ++index) {
    					  option = options[index];
    					  select.options.add(new Option(option.name, option.cityId));
    					} */
    					$('.preloader-wrapper').hide();
						$('.preloader-background').hide();
    				},
					error: function(xhr, status, error) {
						$('.preloader-wrapper').hide();
						$('.preloader-background').hide();
						  //alert(error +"---"+ xhr+"---"+status);
						$('#addeditmsg').modal('open');
               	     	$('#msgHead').text("Message : ");
               	     	$('#msg').text("Product List Not Found"); 
               	     		setTimeout(function() 
							  {
      	     					$('#addeditmsg').modal('close');
							  }, 1000);
						}
    			});

    		});
            
            $('#brandid').change(function() {
    			var brandid = $('#brandid').val();
    			var categoryid = $('#categoryid').val();

    			if(brandid==0)
    			{
    				return false;
    			}
    			
    			if(categoryid==0)
    			{
    				return false;
    			}
    			// Get the raw DOM object for the select box
    			var select = document.getElementById('productid');

    			// Clear the old options
    			select.options.length = 0;

    			//Load the new options

    			select.options.add(new Option("Choose Product", 0));
    			$.ajax({
    				url : "${pageContext.request.contextPath}/fetchProductListByBrandIdAndCategoryId?brandId=" + brandid+"&categoryId="+categoryid,
    				dataType : "json",
    				beforeSend: function() {
						$('.preloader-background').show();
						$('.preloader-wrapper').show();
			           },
    				success : function(data) {

    					/* alert(data); */
    					var options, index, option;
    					select = document.getElementById('productid');

    					for (var i = 0, len = data.length; i < len; ++i) {
    						var product = data[i];
    						select.options.add(new Option(product.productName, product.productId));
    					}

    					/* for (index = 0; index < options.length; ++index) {
    					  option = options[index];
    					  select.options.add(new Option(option.name, option.cityId));
    					} */
    					$('.preloader-wrapper').hide();
						$('.preloader-background').hide();
    				},
					error: function(xhr, status, error) {
						$('.preloader-wrapper').hide();
						$('.preloader-background').hide();
						 // alert(error +"---"+ xhr+"---"+status);
						$('#addeditmsg').modal('open');
               	     	$('#msgHead').text("Message : ");
               	     	$('#msg').text("Product List Not Found"); 
               	     		 setTimeout(function() 
									  {
               	     					$('#addeditmsg').modal('close');
									  },1000);
						}
    			});

    		});
            

            
            $('#saveSupplierSubmit').click(function(){
       		 
        		var form = $('#saveSupplierForm');
        	/*  var txtname = document.querySelector("#name");
        	 var gstno= document.querySelector("#gstin");
        	var form = $('#saveSupplierForm');
        	if(txtname.value== ""){
        		txtname.setCustomValidity("field is required");
        		
        	}
            if(gstno.value== ""){
        		gstno.setCustomValidity("field is required");
        		
        	} */
        	
			$.ajax({
				type : form.attr('method'),				
				url : "${pageContext.request.contextPath}/checkSupplierNameForSave",
				data : form.serialize(),
				async:false,
				beforeSend: function() {
					$('.preloader-background').show();
					$('.preloader-wrapper').show();
		           },
		           success : function(data) {
						// alert(data);
						if(data==="nameFailed")
						{
							 $('#addeditmsg').modal('open');
			           	     $('#msgHead').text("Supplier Adding Message");
			           	     $('#msg').text("Name is already exist");
			           	     $("#name").focus();
		        			    gstinNameValidation=true;
		        			  $('.preloader-wrapper').hide();
							$('.preloader-background').hide();
			           	     return false;
			           	 }
						 if(data==="gstFailed")
						{
							 $('#addeditmsg').modal('open');
			           	     $('#msgHead').text("Supplier Adding Message");
			           	     $('#msg').text("GST number is already exist");
		        			$("#gstin").focus();
		        			gstinNameValidation=true;
		        			$('.preloader-wrapper').hide();
							$('.preloader-background').hide();
		        			return false;
						} 
						else 
						{
							
		        			gstinNameValidation=false;
						}
						$('.preloader-wrapper').hide();
						$('.preloader-background').hide();
    				},
					error: function(xhr, status, error) {
						$('.preloader-wrapper').hide();
						$('.preloader-background').hide();
						  //alert(error +"---"+ xhr+"---"+status);
						$('#addeditmsg').modal('open');
               	     	$('#msgHead').text("Message : ");
               	     	$('#msg').text("Something Went Wrong"); 
               	     		setTimeout(function() 
							  {
      	     					$('#addeditmsg').modal('close');
							  }, 1000);
						}
			});
        	
        	 if(gstinNameValidation==true)
			{
				return false;
			}
			
        	var productlistinput=productList.entries();
        	//alert(productlistinput);
        	 if(productList.length==0)
       	 	{
       	     $('#addeditmsg').modal('open');
       	     $('#msgHead').text("Supplier Adding Message");
       	     $('#msg').text("Select atleast 1 Product");
       	     return false;
       	 	}
        	//alert(productList.entries());
        	var productIdList="";
        	for (var i=0; i<productList.length; i++) {
        		var value=productList[i];
        		productIdList=productIdList+value[0]+"-"+value[1]+",";
      		}
        	productIdList=productIdList.slice(0,-1)
        	//alert(productIdList);
        	$('#productlistinput').val(productIdList);
        	
        });  
    });
        
        function editrow(id) {
        	$('#currentUpdateProductId').val(id);
        	var productId=$('#rowproductkey_'+id).val();
        	//alert($('#rowproductrate_'+id).val());
        	$('#rate').val($('#rowproductrate_'+id).text());
        	$('#rate').focus();
        	$('#rate').trigger('blur'); 
        	$("#productAddId").html('<i class="material-icons left">send</i><span id="productAddButton">Update</span>');
    		$.ajax({
    			type : "GET",
    			url : "${pageContext.request.contextPath}/fetchProductByProductId?productId="+productId,
    			/* data: "id=" + id + "&name=" + name, */
    			beforeSend: function() {
						$('.preloader-background').show();
						$('.preloader-wrapper').show();
			           },
    			success : function(data) {
    				product=data;
    		    	var source = $("#brandid");
    				var v1=product.brand.brandId;
    				source.val(v1);
    				source.change();		

    				var source2 = $("#categoryid");
    				var v2=product.categories.categoryId;
    				source2.val(v2);
    				source2.change();
    			
    				setTimeout(
    						  function() 
    						  {
    						    //do something special
    							  var source3 = $("#productid");
    								var v3=product.productId;
    								source3.val(v3);
    								source3.change();
    								//alert(v);
    								//alert($("#stateListForCity").val());
    						  }, 1000);
    				$('.preloader-wrapper').hide();
					$('.preloader-background').hide();
    				
    			},error: function(xhr, status, error) {
					$('.preloader-wrapper').hide();
					$('.preloader-background').hide();
					 // alert(error +"---"+ xhr+"---"+status);
					$('#addeditmsg').modal('open');
           	     	$('#msgHead').text("Message : ");
           	     	$('#msg').text("Something Went Wrong "); 
           	     		 setTimeout(function() 
								  {
           	     					$('#addeditmsg').modal('close');
								  },1000);
					}

    		});
        	
        }

        function deleterow(id) {
        	
            //alert('#rowproductkey_'+id);
            var removeItem=$('#rowproductkey_' + id).val();
            //alert('removeItem '+$('#rowproductkey_' + id).val());
            //alert('productidlist '+productidlist);
            var productListTemp=[];
           	for(var i=0; i<productList.length; i++)
           	{
           		var value=productList[i];
           		if(removeItem!==value[0])
           		{
           			productListTemp.push(productList[i]);
           		}
           	}
           	productList=[];
           	for(var i=0; i<productListTemp.length; i++)
           	{
           		productList.push(productListTemp[i]);
           	}
            var rowCount = $('#t1 tr').length;
        	//alert(rowCount);
        	var trData="";
        	count=1;
        	for(var i=1; i<=rowCount; i++)
        	{
        		//alert($('#rowcount_'+i).html() +"---"+ $('#rowprocustname_'+i).html() +"---"+ $('#rowdelbutton_'+i).html());
        		
        		if(id!==i)
        		{
        			//alert(i+"-(----)-"+$('#tbproductname_' + i).text());
	        		 /* trData=trData+"<tr id='rowdel_" + count + "' >"+
	           				"<td id='rowcount_" + count + "'>" + count + "</td>"+
	           				"<td id='rowproductname_" + count + "'><input type='hidden' id='rowproductkey_" + count + "' value='"+$('#rowproductkey_' + i).val()+"'><center><span id='tbproductname_" + count + "'>"+$('#tbproductname_' + i).text()+"</span></center></td>"+
	           				"<td id='rowdelbutton_" + count + "'><button class='btn-flat' type='button' onclick='deleterow(" + count + ")'><i class='material-icons '>clear</i></button></td>"+
	           				"</tr>"; */
	           				trData=trData+"<tr id='rowdel_" + count + "' >"+
		               				"<td id='rowcount_" + count + "'>" + count + "</td>"+
		               				"<td id='rowproductname_" + count + "'><input type='hidden' id='rowproductkey_" + count + "' value='"+$('#rowproductkey_' + i).val()+"'><center><span id='tbproductname_" + count + "'>"+$('#tbproductname_' + i).text()+"</span></center></td>"+
		               				"<td id='rowproductrate_" + count + "'>" + $('#rowproductrate_'+i).text() + "</td>"+
		               				"<td id='rowcountproductedit_" + count + "'><button class='btn-flat' type='button' onclick='editrow(" + count + ")'><i class='material-icons '>edit</i></button></td>"+
		               				"<td id='rowdelbutton_" + count + "'><button class='btn-flat' type='button' onclick='deleterow(" + count + ")'><i class='material-icons '>clear</i></button></td>"+
		               				"</tr>";
	        		 count++;
        		}
        		//alert(trData);
        	} 
        	$("#t1").html('');
        	$("#t1").html(trData);
        	//alert(productList.entries());
            //$('#rowdel_' + id).remove();
           // alert('productidlist '+productidlist);

        }
    </script>
<style>
	.container {
    width: 80% !important;
}

</style>
</head>

<body>
    <!--navbar start-->
	<%@include file="components/navbar.jsp" %>
    <!--navbar end-->
    <!--content start-->
   <main>   
        <div class="container">
		        
            <form action="${pageContext.servletContext.contextPath}/saveSupplier" method="post" id="saveSupplierForm">
            <input id="productlistinput" type="hidden" class="validate" name="productIdList">
                <div class="row  z-depth-3">
                
                
                    <div class="col l12 m12 s12">
                        <h5 class="center">Personal Details</h5>
                    </div>
                    <div class="input-field col s12 m3 l3 offset-l1 offset-m1">
                        <i class="material-icons prefix">person</i>
                        <input id="name" type="text" class="validate" name="name" required>
                        <label for="name" class="active" id="namelabel"><span class="red-text">*</span>Name</label>
							
                    </div>

                   <div class="input-field col s12 m3 l3">
                        <i class="material-icons prefix">stay_current_portrait</i>
                        <input id="mobileNo" type="tel" class="validate" name="mobileNumber" required minlength="10" maxlength="10">
                        <label for="mobileNo" data-error="wrong" data-success="right" class="active"><span class="red-text">*</span>Mobile No.</label>
                    </div>
                    
                   <div class="input-field col s12 m3 l3">
                        <i class="material-icons prefix">mail</i>
                        <input id="emailId" type="email" class="validate" name="emailId">
                        <label for="emailId" data-error="wrong" data-success="right" class="active">Email Id</label>
                    </div>

                    <div class="input-field col s12 m3 l3 offset-l1 offset-m1">
                        <i class="material-icons prefix">assistant</i>
                        <input id="gstin" type="text" class="validate" name="gstinNo" required minlength="15" maxlength="15">
                        <label for="gstin"  class="active">GST In</label>
                    
                    </div>

                   <div class="input-field col s12 m4 l4">
                        <i class="material-icons prefix">location_on</i>
                        <textarea id="textarea1" class="materialize-textarea" name="address" required></textarea>
                        <label for="textarea1"><span class="red-text">*</span>Address</label>
                    </div>

				

                </div>
                <div class="row z-depth-3">
                	<input type='hidden' id='currentUpdateProductId'>
                    <div class="col l12 m12 s12">
                        <h5 class="center"> Work Details </h5>
                    </div>
                    <div class="input-field col s12 m3 l3 offset-l1 offset-m1">
                         <i class="material-icons prefix">star</i>
                         <label for="brandid" class="active"><span class="red-text">*</span></label>
                        <select id="brandid" name="brandId">
                                 <option value="0">Choose Brand</option>
                                <c:if test="${not empty brandlist}">
							<c:forEach var="listValue" items="${brandlist}">							
								<option value="<c:out value="${listValue.brandId}" />"><c:out
										value="${listValue.name}" /></option>
							</c:forEach>
						</c:if>
                        </select>
                    </div>
                    <div class="input-field col s12 m3 l3">
                        <i class="material-icons prefix">filter_list</i>
                        <label for="categoryid" class="active"><span class="red-text">*</span></label>
                        <select id="categoryid" name="categoryId">
                                 <option value="0">Choose Category</option>
                                <c:if test="${not empty categorylist}">
							<c:forEach var="listValue" items="${categorylist}">
								<option value="<c:out value="${listValue.categoryId}" />"  ><c:out
										value="${listValue.categoryName}" /></option>
							</c:forEach>
						</c:if>
                        </select>
                    </div>
                    <div class="input-field col s12 m3 l3">
                        <i class="material-icons prefix">shopping_cart</i>
                        <label for="productid" class="active"><span class="red-text">*</span></label>
                         <select id="productid" name="productId" required>
                                 <option value="0">Choose Product</option>
                        <%--         <c:if test="${not empty categorylist}">
							<c:forEach var="listValue" items="${categorylist}">
								<option value="<c:out value="${listValue.categoryId}" />"  ><c:out
										value="${listValue.categoryName}" /></option>
							</c:forEach>
						</c:if> --%>
                        </select>
                    </div>
                    <div class="input-field col s12 m3 l3 offset-l1 offset-m1">
                        <i class="fa fa-inr prefix" aria-hidden="true"></i>
                        <input id="rate" type="text" name="rate">
                        <label for="rate" class="active"><span class="red-text">*</span>Rate</label>
                    </div>
                    <div class="input-field col s12 m1 l2 push-l1  push-m1">
                        <button class="btn waves-effect waves-light blue-gradient" type="button" id="productAddId" ><i class="material-icons left">add</i>Add</button>
                    </div>
                    <div class="input-field col s12 m10 l10 push-l1 push-m1">
                        <table class="centered tblborder" id="productcart">
                            <thead>
                                <tr>
                                    <th>Sr.No</th>
                                    <th>Product</th>
                                    <th>Rate</th>
                                    <th>Edit</th>
                                    <th>Cancel</th>
                                </tr>
                            </thead>
                            <tbody id="t1">
                            </tbody>
                        </table>
                        <br><br>
                    </div>

                </div>
                
                
                
                <%-- <div class="row z-depth-3">
                    <div class="col l12 m12 s12">
                        <h4 class="center"> Work Details </h4>
                    </div>
                   <div class="input-field col s12 m5 l5 push-l1 pull-m1 ">

                        <i class="material-icons prefix">star</i>
                        <select id="brandid" name="brandId">
                                 <option value="0">Choose Brand</option>
                                <c:if test="${not empty brandlist}">
							<c:forEach var="listValue" items="${brandlist}">							
								<option value="<c:out value="${listValue.brandId}" />"><c:out
										value="${listValue.name}" /></option>
							</c:forEach>
						</c:if>
                        </select>
                    </div>
                    <div class="input-field col s12 m5 l5 push-l1 pull-m1">
                        <i class="material-icons prefix">filter_list</i>
                        <select id="categoryid" name="categoryId">
                                 <option value="0">Choose Category</option>
                                <c:if test="${not empty categorylist}">
							<c:forEach var="listValue" items="${categorylist}">
								<option value="<c:out value="${listValue.categoryId}" />"  ><c:out
										value="${listValue.categoryName}" /></option>
							</c:forEach>
						</c:if>
                        </select>
                    </div>
                    <div class="input-field col s12 m5 l5 push-l1  push-m1">
                        <i class="material-icons prefix">shopping_cart</i>
                         <select id="productid" name="productId">
                                 <option value="0">Choose Product</option>
                                <c:if test="${not empty categorylist}">
							<c:forEach var="listValue" items="${categorylist}">
								<option value="<c:out value="${listValue.categoryId}" />"  ><c:out
										value="${listValue.categoryName}" /></option>
							</c:forEach>
						</c:if>
                        </select>
                    </div>
                    <!--<div class="input-field col s12 m5 l5 push-l2">
                        <button class="btn waves-effect waves-light blue darken-8" type="button">Ok</button>
                    </div>-->
                    <div class="input-field col s12 m5 l5 push-l1  push-m1">
                        <table class="centered" id="productcart">
                            <thead>
                                <tr>
                                    <th>Sr.No</th>
                                    <th>Product</th>
                                    <th>Cancel</th>
                                </tr>
                            </thead>
                            <tbody id="t1">
                                <!-- <tr>
                                    <td>1</td>
                                    <td>Abc</td>
                                    <td><i class="material-icons ">clear</i></td>
                                </tr> -->
                            </tbody>
                        </table>
                        
                        <br><br>
                    </div>

                </div> --%>
                <div class="input-field col s12 m6 l4 offset-l5 center-align">
                    <button class="btn waves-effect waves-light blue-gradient" id="saveSupplierSubmit">Add Supplier<i class="material-icons right">send</i> </button>

                </div>
                <br>
            </form>

        </div>


	 <div class="row">
			<div class="col s12 m12 l8">
				<div id="addeditmsg" class="modal" style="width:40%;">
					<div class="modal-content">
						<h5 id="msgHead"></h5>
						<hr>
						<h6 id="msg"></h6>
					</div>
					<div class="col s8 m2 l2 offset-l5 modal-footer">
						<a href="#!"
							class="modal-action modal-close waves-effect waves-green btn blue-gradient">OK</a>
					</div>
				</div>
			</div>
		</div>
	

  	

    </main>
    <!--content end-->
</body>

</html>