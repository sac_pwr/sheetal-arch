<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<html>

<head>
     <%@include file="components/header_imports.jsp" %>
 <script type="text/javascript" src="resources/js/hashtable.js"></script>
        <script>var myContextPath = "${pageContext.request.contextPath}"</script>
	<script type="text/javascript" src="resources/js/addMultipleInventory.js"></script>

    
   <script>
   /* $("#resetAddQuantitySubmit").click(function(){
	
		$("#productAddId").html('<i class="material-icons left">add</i> Add');

	}); */
   </script>
   <style>
	.container {
    width: 80% !important;
}
	#chooseDate{
		width:30% !important;
	}

</style>
</head>

<body >
		   <!--navbar start-->
		   	<%@include file="components/navbar.jsp" %>
		    <!--navbar end-->
		    
		    <!--content start-->
	<main>
     <div class="container">
            <form action="${pageContext.servletContext.contextPath}/saveInventory" method="post" id="saveAddQuantityForm">
            <input type='hidden' id='currentUpdateProductId'>
            <input id="productlistinput" type="hidden" class="validate" name="productIdList">
                <div class="row  z-depth-3">
                     <div class="col l12 m12 s12">
                        <h5 class="center">Inventory Details </h5>
                     
                    </div>
                    <div class="input-field col s12 m3 l3 offset-l1 offset-m1">
                        <i class="material-icons prefix">person</i>
                        <label for="supplierId" class="active"><span class="red-text">*</span></label>
                        <select name="supplierId" id="supplierId">
                                 <option value="0" selected>Select Supplier</option>
                                <c:if test="${not empty supplierList}">
							<c:forEach var="listValue" items="${supplierList}">
								<option value="<c:out value="${listValue.supplierId}" />"><c:out
										value="${listValue.name}" /></option>
							</c:forEach>
						</c:if>
                        </select>
                    </div>

                    <div class="input-field col s12 m3 l3">
                        <i class="material-icons prefix">star</i>
                         <label for="brandId" class="active"><span class="red-text">*</span></label>
                        <select name="brandId" id="brandId">
                                 <option value="0" selected>Brand Name</option>
                                <c:if test="${not empty brandlist}">
							<c:forEach var="listValue" items="${brandlist}">
								<option value="<c:out value="${listValue.brandId}" />"><c:out
										value="${listValue.name}" /></option>
							</c:forEach>
						</c:if>
                        </select>
                    </div>

                    <div class="input-field col s12 m3 l3">
                        <i class="material-icons prefix">filter_list</i>
                          <label for="categoryId" class="active"><span class="red-text">*</span></label>
                        <select name="categoryId" id="categoryId">
                                 <option value="0" selected>Category</option>
                                <c:if test="${not empty categorieslist}">
							<c:forEach var="listValue" items="${categorieslist}">
								<option value="<c:out value="${listValue.categoryId}" />"><c:out
										value="${listValue.categoryName}" /></option>
							</c:forEach>
						</c:if>
                        </select>
                    </div>
                    <div class="input-field col s12 m3 l3 offset-l1 offset-m1">
                        <i class="material-icons prefix">shopping_cart</i>
                        <label for="productId" class="active"><span class="red-text">*</span></label>
                        <select name="productId" id="productId">
                        	<option value="0" selected>Choose Product</option>
                        </select>
                    </div>
                    <div class="input-field col s12 m3 l3">
                        <i class="material-icons prefix">playlist_add</i>
                        <input id="quantity" type="text" name="">
                        <label for="quantity" class="active"><span class="red-text">*</span>Quantity</label>
                    </div>
                    
                    <!-- <div class="input-field col s12 m3 l3">
                     <i class="material-icons prefix">event</i>
                           <input id="paymentDate" type="text" name="paymentDate" title="Enter Payment Date" class="datepicker">
                           <label for="paymentDate" class="active black-text">Payment Date</label>
                    </div> -->
                    <div class="input-field col s12 m2 l2" style="margin-top:3%;">
                        <button type="button" class="btn waves-effect waves-light blue-gradient" type="button" id="productAddId"><span id="productAddButtonId">Add</span><i class="material-icons right">add</i></button>
                    </div>
	  				<div class="input-field col s12 m2 l2 left-align" style="margin-top:3%;">
	                    <button class="btn waves-effect waves-light blue-gradient left" type="button" id="resetAddQuantitySubmit">Reset<i class="material-icons right">refresh</i></button>
	                </div>
                    </div>
                    
                    
                    <div class="row  z-depth-3">
                     <div class="col l12 m12 s12">
                        <h5 class="center"> Other Details </h5>
                     
                    </div>
                    <div class="input-field col s12 m3 l3 offset-l1 offset-m1">
                      <i class="material-icons prefix">rate_review</i>
                        <input id="hsncode" type="text" name="" class="grey lighten-3" readonly>
                        <label for="hsncode" class="active">HSN Code</label>
                    </div>
                    <div class="input-field col s12 m3 l3">
                            <i class="fa fa-inr prefix" aria-hidden="true"></i>
                        <input id="AmtPerUnit" type="text" name="" class="grey lighten-3" readonly>
                        <label for="AmtPerUnit" class="active">Amount per unit(Supplier)</label>
                    </div>
                    <div class="input-field col s12 m3 l3">
                        <i class="material-icons prefix">playlist_add_check</i>
                        <input id="oldquantity" type="text" name="" class="grey lighten-3" readonly>
                        <label for="oldquantity" class="active">Old Quantity</label>
                    </div>
                     <div class="input-field col s12 m3 l3  offset-l1 offset-m1">
                        <i class="fa fa-inr prefix" aria-hidden="true"></i>
                        <input id="totalAmt" type="text" name="totalAmount" class="grey lighten-3" readonly>
                        <label for="totalAmt" class="active">Amount</label>
                    </div>
                     <div class="input-field col s12 m3 l3">
                        <i class="fa fa-percent prefix" aria-hidden="true" style="font-size:22px"></i>
                        <input id="igst" type="text" name="" class="grey lighten-3" readonly>
                        <label for="igst" class="active">Total Tax(<font color='blue'><span id="igstPer"></span>%</font>)</label>
                    </div>    
                    <div class="input-field col s12 m3 l3">
                        <i class="fa fa-inr prefix" aria-hidden="true"></i>
                        <input id="amountWithTax" type="text" name="totalAmountWithTax" class="grey lighten-3" readonly>
                        <label for="amountWithTax" class="active">Amount With Tax</label>
                    </div>
                  <!--   <div class="input-field col s12 m3 l3 offset-l1 offset-m1">
                      <i class="fa fa-percent prefix" aria-hidden="true" style="font-size:22px"></i>
                        <input id="cgst" type="text" name="" readonly>
                        <label for="cgst" class="active">CGST(<font color='blue'><span id="cgstPer"></span>%</font>)</label>
                    </div>
                      <div class="input-field col s12 m3 l3">
                        <i class="fa fa-percent prefix" aria-hidden="true" style="font-size:22px"></i>
                        <input id="sgst" type="text" name="" readonly>
                        <label for="sgst" class="active">SGST(<font color='blue'><span id="sgstPer"></span>%</font>)</label>
                    </div>  -->
                                
                    
                    <!-- <div class="input-field col s12 m3 l3  offset-l4 offset-m1" style="margin-top:3%;">
                        <button type="button" class="btn waves-effect waves-light blue-gradient" type="button" id="productAddId"><span id="productAddButtonId">Add</span><i class="material-icons right">add</i></button>
                    </div>
	  				<div class="input-field col s12 m2 l3" style="margin-top:3%;">
	                    <button class="btn waves-effect waves-light blue-gradient" type="button" id="resetAddQuantitySubmit">Reset<i class="material-icons right">refresh</i></button>
	                </div> -->
                    <div class="input-field col s12 m10 l10 push-l1 push-m1">
                        <table class="centered tblborder">
                            <thead>
                                <tr>
                                    <th>Sr.No</th>
                                    <th>Product</th>
                                    <th>Rate</th>
                                    <th>Quantity</th>
                                    <th>Edit</th>
                                    <th>Cancel</th>
                                </tr>
                            </thead>
                            <tbody id="t1">
                                <!-- <tr>
                                    <td>1</td>
                                    <td>Mouse</td>
                                    <td>5</td>
                                    <td>10</td>
                                    <td><button class="btn-flat" type="button"><i class="material-icons ">edit</i></button></td>
                                    <td><button class="btn-flat" type="button"><i class="material-icons ">clear</i></button></td>
                                </tr> -->
                            </tbody>
                        </table>
                        <br><br>
                    </div>
                    <div class=" col s12 m5 l5 push-l1 push-m1 ">

                       <h6 class="red-text"><b>Total Amount(Without Tax)</b> : <span id="finalTotalAmount">0</span></h5>
                           <br>

                           <br>
                   </div>
                   <div class=" col s12 m5 l5 push-l2 push-m1 ">
                       <h6 class="red-text"><b>Total Amount(With Tax)</b> : <span id="finalTotalAmountWithTax">0</span></h6>
                       <br>

                       <br>
                   </div>
                    
                </div>

						

              
              	
             <div class="row">
				<div class="col s8 m2 l2">
					<div id="chooseDate" class="modal" style="width:40%;">
						<div class="modal-content">
							  <div class="input-field col s12 m12 l12">
                   				  <i class="material-icons prefix">event</i>
                       		      <input id="paymentDate" type="text" name="paymentDate" title="Enter Payment Date" class="datepicker">
                           		  <label for="paymentDate" class="active black-text">Payment Date</label>
                           		  <span id="errorDate" class="red-text center-align"></span>
                    		 </div>
						</div>
						<div class="col s8 m2 l2 offset-l5 modal-footer">
							<button  type="submit" id="submitModal" class="modal-action waves-effect btn blue-gradient">OK</button>
						</div>
					</div>
				</div>
				</div>
              	
              	  <div class="input-field col s12 m6 l4 offset-l5 center-align">
                    <button class="btn waves-effect waves-light blue-gradient" type="button" id="saveAddQuantitySubmit">Add Inventory<i class="material-icons right">send</i> </button>
                </div>
                <br>
            </form>

        </div>



			<div class="row">
			<div class="col s8 m2 l2">
				<div id="addeditmsg" class="modal" style="width:40%;">
					<div class="modal-content">
						<h5 id="msgHead">Error</h5>
						<hr>	
						<h6 id="msg"></h6>
					</div>
					<div class="col s8 m2 l2 offset-l5 modal-footer">
						<a href="#!"
							class="modal-action modal-close waves-effect btn blue-gradient">OK</a>
					</div>
				</div>
			</div>
		</div>

    </main>
    <!--content end-->
</body>

</html>