<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<html>

<head>
    <%@include file="components/header_imports.jsp" %>
    <script type="text/javascript">
    var myContextPath = "${pageContext.request.contextPath}";
    var employeeDetailsId="${employeeDetailsId}";
    $(document).ready(function() {
   
   	 $('#empdetailtable').DataTable({
         "oLanguage": {
             "sLengthMenu": "Display Records _MENU_",
             "sSearch": " _INPUT_" //search
            	
         },
         columnDefs: [
                      { 'width': '1%', 'targets': 0 }
                      ],
         lengthMenu: [
             [10, 25., 50, -1],
             ['10 ', '25 ', '50 ', 'All']
         ],
//         scrollX: true,
         dom: 'lBfrtip',
         buttons: {
             buttons: [
                 //      {
                 //      extend: 'pageLength',
                 //      className: 'pageLengthButton waves-effect waves-light   white-text blue-grey lighten-1'
                 //  }, 
                 {
                     extend: 'pdf',
                     className: 'pdfButton waves-effect waves-light   white-text grey lighten-3 light-blue-text text-darken-4 z-depth-2',
                     text: '<i class="fa fa-file-pdf-o"></i> &nbsp;<span style="font-size:15px;">PDF<span>',
                     //title of the page
                     title: function() {
                         var name = $(".heading").text();
                         return name
                     },
                     //file name 
                     filename: function() {
                         var d = new Date();
                         var date = d.getDate();
                         var month = d.getMonth();
                         var year = d.getFullYear();
                         var name = $(".heading").text();
                         return name + date + '-' + month + '-' + year;
                     },
                     //  exports only dataColumn
                     exportOptions: {
                         columns: ':visible.print-col'
                     },
                 },
                 {
                     extend: 'excel',
                     className: 'excelButton waves-effect waves-light   white-text grey lighten-3 light-blue-text text-darken-4 z-depth-2',
                     text: '<i class="fa fa-file-excel-o  fa-fw"></i> &nbsp;<span style="font-size:15px;">EXCEL<span>',
                     //title of the page
                     title: function() {
                         var name = $(".heading").text();
                         return name
                     },
                     //file name 
                     filename: function() {
                         var d = new Date();
                         var date = d.getDate();
                         var month = d.getMonth();
                         var year = d.getFullYear();
                         var name = $(".heading").text();
                         return name + date + '-' + month + '-' + year;
                     },
                     //  exports only dataColumn
                     exportOptions: {
                         columns: ':visible.print-col'
                     },
                 },
                 {
                     extend: 'print',
                     className: 'printButton waves-effect waves-light   white-text grey lighten-3 light-blue-text text-darken-4 z-depth-2',
                     text: '<i class="fa fa-print fa-fw"></i> &nbsp;<span style="font-size:15px;">PRINT<span>',
                     //title of the page
                     title: function() {
                         var name = $(".heading h4").text();
                         return name
                     },
                     //file name 
                     filename: function() {
                         var d = new Date();
                         var date = d.getDate();
                         var month = d.getMonth();
                         var year = d.getFullYear();
                         var name = $(".heading").text();
                         return name + date + '-' + month + '-' + year;
                     },
                     //  exports only dataColumn
                     exportOptions: {
                         columns: ':visible.print-col'
                     },
                 },
                 {
                     extend: 'colvis',
                     className: 'colvisButton waves-effect waves-light  white-text grey lighten-3 light-blue-text text-darken-4 z-depth-2',
                     text: '<span style="font-size:15px;">COLUMN VISISBILITY<span>&nbsp;&nbsp;<i class="fa fa-caret-down fa-fw"></i> ',
                     collectionLayout: 'fixed two-column',
                     align: 'left'
                 },
             ]
         }

     });	
   	
   	 $("select").change(function() {
         var t = this;
         var content = $(this).siblings('ul').detach();
         setTimeout(function() {
             $(t).parent().append(content);
             $("select").material_select();
         }, 200);
     });
	      $('select').material_select();
    	//view All
    	filter="CurrentMonth";
		url=myContextPath+"/fetchEmployeeSalaryByFilter?filter="+filter+"&employeeDetailsId="+employeeDetailsId+"&startDate=''&endDate=''";
    	
    	$.ajax({
    		//url : "${pageContext.request.contextPath}/fetchEmployeeDetail?employeeDetailsId="+employeeDetailsId,//current month
    		url:url,
    		dataType : "json",
    		success : function(data) {
    			employeeSalaryStatus=data[0];
    			$('#empdetailname').html("<b>Name : </b>"+employeeSalaryStatus.name);
    			$('#empdetaildept').html("<b>Department : </b>"+employeeSalaryStatus.departmentname);
    			$('#empdetailmobile').html("<b>Mobile Number : </b>"+employeeSalaryStatus.mobileNumber);
    			$('#empdetailaddress').html("<b>Address : </b>"+employeeSalaryStatus.address);
    			$('#empdetailareas').html("<b>Areas : </b>"+employeeSalaryStatus.areaList);
    			$('#empdetailnoofholidays').html("<b>Holidays : </b>"+employeeSalaryStatus.noOfHolidays+" days");
    			$('#empdetailbasicsalary').html("<b>Basic Salary : </b>"+employeeSalaryStatus.basicMonthlySalary.toFixed(2)+" &#8377;");
    			$('#empdetaildeduction').html("<b>Deduction : </b>"+employeeSalaryStatus.deduction.toFixed(2)+" &#8377;");
    			$('#empdetailincentive').html("<b>Incentives : </b>"+employeeSalaryStatus.getIncentive.toFixed(2)+" &#8377;");
    			$('#empdetailtotalsalary').html("<b>Total Amount : </b>"+employeeSalaryStatus.totalAmount.toFixed(2)+" &#8377;");
    			$('#empdetailamtpaid').html("<b>Paid Amount : </b>"+employeeSalaryStatus.amountPaidCurrentMonth.toFixed(2)+" &#8377;");
    			$('#empdetailamtpending').html("<b>Pending Amount : </b>"+employeeSalaryStatus.amountPendingCurrentMonth.toFixed(2)+" &#8377;");
    			//alert(employeeSalaryStatus.employeeDetailsId);
    			//$('#employeeDetailsId').val(employeeSalaryStatus.employeeDetailsId);	
    			$("#payHrefId").attr("href", "${pageContext.request.contextPath}/paymentEmployee?employeeDetailsId="+employeeSalaryStatus.employeeDetailsPkId);
    			
    			employeeSalarytable=employeeSalaryStatus.employeeSalaryList;
    			
    			var t = $('#empdetailtable').DataTable();
						t.clear().draw();
						var count = $("#empdetailtable").find("tr:first th").length;
    			if(employeeSalarytable!==null)
    			{
    				for (var i = 0, len = employeeSalarytable.length; i < len; ++i)
    				{
    					employeeSalary=employeeSalarytable[i];
    					var currentTime=new Date(parseInt(employeeSalary.date));
    					var month = currentTime.getMonth() + 1;
    					var day = currentTime.getDate();
    					var year = currentTime.getFullYear();
    					var date = day + "/" + month + "/" + year;
    					
						t.row.add([
									employeeSalary.srno,
									employeeSalary.amount.toFixed(2),
									date,
									employeeSalary.modeOfPayment,
									employeeSalary.comment
								  ]).draw(false); 
					
    						
    				}
    			}
    			else
    			{
					t.row.add('<tr>'
							+ '<td colspan="'+count+'"><center>No Data Found</center></td>'
							+ '</tr>');					
    			}
    		}
    	});
    	
    	
    	$('#saveIncentivesSubmit').click(function(){
			
			var incentiveAmount=$('#incentiveAmount').val();
			var incentiveReasonId=$('#incentiveReasonId').val();
			if(incentiveAmount==="")
			{
				$('#msgincentivesave').html("Enter Incentive Amount");
				return false;
			}
			if(incentiveReasonId.trim()==="")
			{
				$('#msgincentivesave').html("Enter Incentive Reason");
				return false;
			}
			var form = $('#saveIncentive');
			
				$('#msgincentivesave').html("");
				
				$.ajax({
					type : form.attr('method'),
					url : myContextPath+"/editIncentivesAjax",
					data : form.serialize(),
					success : function(data) {
					
						if(data==="Success")
						{
							$('#msgincentivesave').html("<font color='green'>Incentive SuccessFully Updated</green>");
							$('#incentiveName').val('');
					    	$('#incentiveAmount').val('');
					    	$('#incentiveReasonId').val('');
							searchEmployeeIncentive(1);
						}
						else
						{
							$('#msgincentivesave').html("<font color='red'>Incentive Updating Failed</green>");
						}
					},
					error: function(xhr, status, error) {
						$('#addeditmsg').modal('open');
						$('#head').html("Incentive Update Error :  ");
						$('#msg').html("<font color='red'>Incentive Updating Problem</font>");
					}
				});
			
			
		});
				
	});
    function openIncentivesModel()
    {

    	$('#incentiveName').val('');
    	$('#incentiveAmount').val('');
    	$('#msgincentivesave').html('');
    	$('#incentiveReasonId').val('');
    	searchEmployeeIncentive(1);
    	
    	$('.modal').modal();
    	$('#incentivemodal').modal('open');
    	
    	$("#saveIncentive").hide();
    	
    }
    
    function searchEmployeeIncentive(choice)
    {
    	$(".showDates").hide();
    	$("#saveIncentive").hide();
    	
    	var filter="";
    	var url="";
    	var startDate=$('#startDateIncentive').val();
    	var endDate=$('#endDateIncentive').val();
    	
    	if(choice==1)
    	{
    		filter="CurrentMonth";
    		url=myContextPath+"/fetchEmployeeIncentiveDetailByFilter?filter="+filter+"&employeeDetailsId="+employeeDetailsId+"&startDate=''&endDate=''";
    	}
    	else if(choice==2)
    	{
    		filter="Last6Months";
    		url=myContextPath+"/fetchEmployeeIncentiveDetailByFilter?filter="+filter+"&employeeDetailsId="+employeeDetailsId+"&startDate=''&endDate=''";		
    	}
    	else if(choice==3)
    	{
    		filter="Last1Year";
    		url=myContextPath+"/fetchEmployeeIncentiveDetailByFilter?filter="+filter+"&employeeDetailsId="+employeeDetailsId+"&startDate=''&endDate=''";
    	}
    	else if(choice==4)
    	{
    		filter="Range";
    		
    		url=myContextPath+"/fetchEmployeeIncentiveDetailByFilter?filter="+filter+"&employeeDetailsId="+employeeDetailsId+"&startDate="+startDate+"&endDate="+endDate;
    	}
    	else if(choice==5)
    	{
    		filter="ViewAll";
    		url=myContextPath+"/fetchEmployeeIncentiveDetailByFilter?filter="+filter+"&employeeDetailsId="+employeeDetailsId+"&startDate=''&endDate=''";
    	}
    		
    	
    	$.ajax({
    		type : "GET",
    		url : url,
    		/* data: "id=" + id + "&name=" + name, */
    		success : function(data) {
    			
    			employeeDetails=data.employeeDetails; 
    			employeeIncentiveList=data.employeeIncentiveList;
    			
    			$('#incentiveName').val(employeeDetails.name);
    			
    			$("#incentiveTable").empty();
    			if(employeeIncentiveList!==null)
    			{
    				for (var i = 0, len = employeeIncentiveList.length; i < len; ++i)
    				{
    					employeeIncentive=employeeIncentiveList[i];
    					var incentiveGivenDate=new Date(parseInt(employeeIncentive.incentiveGivenDate));
    					var month = incentiveGivenDate.getMonth() + 1;
    					var day = incentiveGivenDate.getDate();
    					var year = incentiveGivenDate.getFullYear();
    					var incentiveGivenDateString = day + "/" + month + "/" + year;
    					
    					if(employeeDetails.status==true)
						{
    						editButton="<td><button type='button' class='btn-flat' disabled='disabled' onclick='editIncentives("+employeeIncentive.employeeIncentiveId+")'><i class='material-icons'>edit</i></button></td>";
						}	
						else
						{
							editButton="<td><button type='button' class='btn-flat' onclick='editIncentives("+employeeIncentive.employeeIncentiveId+")'><i class='material-icons'>edit</i></button></td>";
						}
    					
    					$("#incentiveTable").append("<tr>"+
    										        "<td>"+(i+1)+"</td>"+
    										        "<td>"+employeeIncentive.incentiveAmount.toFixed(2)+"</td>"+
    										        "<td>"+employeeIncentive.reason+"</td>"+
    										        "<td>"+incentiveGivenDateString+"</td>"+
    										        editButton+
    										    	"</tr>");     						
    				}
    			}
    		}
    	});

    }
    function editIncentives(id){
    	$('#incentiveId').val(id);
    	$('#msgincentivesave').html('');
    	$.ajax({
    		type : "GET",
    		url : myContextPath+"/fetchEmployeeIncentivesByEmployeeIncentivesId?employeeIncentivesId="+id,
    		success : function(data) {
    			$('#incentiveAmount').val(data.incentiveAmount.toFixed(2));
    			$('#incentiveAmount').change();
    	    	$('#incentiveReasonId').val(data.reason);
    	    	$('#msgincentivesave').html('');
    	    	$("#saveIncentive").show();
    		},
    		error: function(xhr, status, error) {
    			$('#msgincentivesave').html("<font color='red'>Something went wrong</green>");
			}
    	});
    }
    function searchEmployeeSalary(choice)
    {
    	 $(".showDates1").hide();
    	var filter="";
    	var url="";
    	var startDate=$('#startDate1').val();
    	var endDate=$('#endDate1').val();
    	//alert(employeeDetailsId);
    	//return false;
    	if(choice==1)
    	{
    		filter="CurrentMonth";
    		url=myContextPath+"/fetchEmployeeSalaryByFilter?filter="+filter+"&employeeDetailsId="+employeeDetailsId+"&startDate=''&endDate=''";
    	}
    	else if(choice==2)
    	{
    		filter="Last6Months";
    		url=myContextPath+"/fetchEmployeeSalaryByFilter?filter="+filter+"&employeeDetailsId="+employeeDetailsId+"&startDate=''&endDate=''";
    		
    	}
    	else if(choice==3)
    	{
    		filter="Last1Year";
    		url=myContextPath+"/fetchEmployeeSalaryByFilter?filter="+filter+"&employeeDetailsId="+employeeDetailsId+"&startDate=''&endDate=''";
    	}
    	else if(choice==4)
    	{
    		filter="Range";
    		
    		url=myContextPath+"/fetchEmployeeSalaryByFilter?filter="+filter+"&employeeDetailsId="+employeeDetailsId+"&startDate="+startDate+"&endDate="+endDate;
    	}
    	else if(choice==5)
    	{
    		filter="ViewAll";
    		url=myContextPath+"/fetchEmployeeSalaryByFilter?filter="+filter+"&employeeDetailsId="+employeeDetailsId+"&startDate=''&endDate=''";
    	}
    		
    	
    	$.ajax({
    		type : "GET",
    		url : url,
    		/* data: "id=" + id + "&name=" + name, */
    		success : function(data) {
    			
				
    			employeeSalaryStatus=data[0];
    			$('#empdetailname').html("<b>Name : </b>"+employeeSalaryStatus.name);
    			$('#empdetaildept').html("<b>Department : </b>"+employeeSalaryStatus.departmentname);
    			$('#empdetailmobile').html("<b>Mobile Number : </b>"+employeeSalaryStatus.mobileNumber);
    			$('#empdetailaddress').html("<b>Address : </b>"+employeeSalaryStatus.address);
    			$('#empdetailareas').html("<b>Areas : </b>"+employeeSalaryStatus.areaList);
    			$('#empdetailnoofholidays').html("<b>Holidays : </b>"+employeeSalaryStatus.noOfHolidays+" days");
    			$('#empdetailbasicsalary').html("<b>Basic Salary : </b>"+employeeSalaryStatus.basicMonthlySalary.toFixed(2)+" &#8377;");
    			$('#empdetaildeduction').html("<b>Deduction : </b>"+employeeSalaryStatus.deduction.toFixed(2)+" &#8377;");
    			$('#empdetailincentive').html("<b>Incentives : </b>"+employeeSalaryStatus.getIncentive.toFixed(2)+" &#8377;");
    			$('#empdetailtotalsalary').html("<b>Total Amount : </b>"+employeeSalaryStatus.totalAmount.toFixed(2)+" &#8377;");
    			$('#empdetailamtpaid').html("<b>Paid Amount : </b>"+employeeSalaryStatus.amountPaidCurrentMonth.toFixed(2)+" &#8377;");
    			$('#empdetailamtpending').html("<b>Pending Amount : </b>"+employeeSalaryStatus.amountPendingCurrentMonth.toFixed(2)+" &#8377;");
    			//alert(employeeSalaryStatus.employeeDetailsId);
    			//$('#employeeDetailsId').val(employeeSalaryStatus.employeeDetailsId);	
    			$("#payHrefId").attr("href", "${pageContext.request.contextPath}/paymentEmployee?employeeDetailsId="+employeeSalaryStatus.employeeDetailsPkId);
    			
    			employeeSalarytable=employeeSalaryStatus.employeeSalaryList;
    			
    			var t = $('#empdetailtable').DataTable();
						t.clear().draw();
						var count = $("#empdetailtable").find("tr:first th").length;
    			if(employeeSalarytable!==null)
    			{
    				for (var i = 0, len = employeeSalarytable.length; i < len; ++i)
    				{
    					employeeSalary=employeeSalarytable[i];
    					var currentTime=new Date(parseInt(employeeSalary.date));
    					var month = currentTime.getMonth() + 1;
    					var day = currentTime.getDate();
    					var year = currentTime.getFullYear();
    					var date = day + "/" + month + "/" + year;
    					
						t.row.add([
									employeeSalary.srno,
									employeeSalary.amount.toFixed(2),
									date,
									employeeSalary.modeOfPayment,
									employeeSalary.comment
								  ]).draw(false); 
					
    						
    				}
    			}
    			else
    			{
					t.row.add('<tr>'
							+ '<td colspan="'+count+'"><center>No Data Found</center></td>'
							+ '</tr>');					
    			}
    		}
    	});
    }
    
    </script>
    
    <style>
        /*main {
            flex: 1 0 auto;
            padding-left: 255px !important;
        }*/
        p{
        	font-size:14px !important;
        }
        .blue-text{
        color:#01579b!important;
        }
       /* .dataTables_filter{
       	margin-top:30px !important;
       } */
    </style>
</head>

<body>
    <!--navbar start-->
   	<%@include file="components/navbar.jsp" %>
    <!--navbar end-->
    <!--content start-->
    <main>      
        <div class="row">
        <div class="col s12 l5 m6 card-panel hoverable">
       
                <div class="col s12 l6 m6">
                    <p id="empdetailname" class="blue-text">Name:abc</p>
               		 <!--  <hr style="border:1px dashed teal;"> -->
                </div>
                <div class="col s12 l6 m6">
                    <p id="empdetaildept" class="blue-text">Department: abc</p>
               		  <!-- <hr style="border:1px dashed teal;"> -->
                </div>
                <div class="col s12 l6 m6 ">
                     <p id="empdetailmobile" class="blue-text">Mobile No: 7506018815</p>
               		  <!-- <hr style="border:1px dashed teal;"> -->
                </div>
                <div class="col s12 l6 m6 ">
                   <p id="empdetailareas" class="blue-text">Area: Poisar</p>
                	  <!-- <hr style="border:1px dashed teal;"> -->
                </div>
                <div class="col s12 l6 m6 ">
                    <p id="empdetailaddress" class="blue-text">Address: </p>
               		  <!-- <hr style="border:1px dashed teal;"> -->
                </div>
                <div class="col s12 l6 m6 ">
                  <p id="empdetailnoofholidays" class="blue-text">No.of Holiday:</p>
                	  <!-- <hr style="border:1px dashed teal;"> -->
                </div>
            
          </div>  
        	<div class="col s12 l5 m6 offset-l2 card-panel hoverable">
        	    <div class="col s12 l6 m6 ">
                    <p id="empdetailbasicsalary" class="blue-text">Basic Salary:</p>
               		 <!--  <hr style="border:1px dashed teal;"> -->
                </div>
                <div class="col s12 l6 m6 ">
                    <p id="empdetaildeduction" class="blue-text">Incentive:</p>
               		  <!-- <hr style="border:1px dashed teal;"> -->
                </div>
                <div class="col s12 l6 m6 ">
                       <p id="empdetailincentive" class="blue-text">Deduction:</p>
               		  <!-- <hr style="border:1px dashed teal;"> -->
                </div>
                <div class="col s12 l6 m6 ">
                     <p id="empdetailtotalsalary" class="blue-text">Total Salary:</p>
                	  <!-- <hr style="border:1px dashed teal;"> -->
                </div>
                <div class="col s12 l6 m6 ">
                     <p id="empdetailamtpaid" class="blue-text">Amount Paid:7506018815</p>
               		  <!-- <hr style="border:1px dashed teal;"> -->
                </div>
                <div class="col s12 l6 m6 ">
                     <p id="empdetailamtpending" class="blue-text">Amount Pending:10101010</p>
                	  <!-- <hr style="border:1px dashed teal;"> -->
                </div>
           
          </div>
        
        
        
          
        <!-- </div>


 		<div class="row"> -->
 				<div class="col s12 m2 l2 right">
	               <button id="viewIncentive" onclick="openIncentivesModel()" class="btn btn-waves blue-gradient right"><i class="material-icons right">send</i>Edit Incentive</button>
	           </div>
	              <div class="col s12 m2 l2 right">
	               <a id="payHrefId" href="#" class="btn btn-waves blue-gradient right"><i class="material-icons right ">send</i>Pay</a>
	           </div>
            <div class="col s12 m8 l8">
                <div class="col s6 m3 l3 left">
                    <!-- Dropdown Trigger -->
                    <a class='dropdown-button btn waves-effect waves-light blue-gradient' href='#' data-activates='filter1'>Filter<i
       				 class="material-icons right">arrow_drop_down</i></a>
                    <!-- Dropdown Structure -->
                    <ul id='filter1' class='dropdown-content'>
                        <li><button type="button" class="btn-flat" onclick="searchEmployeeSalary(1)">Current Month</button></li>
                        <li><button type="button" class="btn-flat" onclick="searchEmployeeSalary(2)">Last 6 Months</button></li>
                        <li><button type="button" class="btn-flat" onclick="searchEmployeeSalary(3)">Last 1 Year</button></li>
                        <li><button type="button" class="rangeSelect1 btn-flat">Range</button></li>
                        <li><button type="button" class="btn-flat" onclick="searchEmployeeSalary(5)">View All</button></li>
                    </ul>
                </div>
             
	            
                <form action="ledger">
                    <input type="hidden" name="range" value="dateRange"> 
                    <input type="hidden" name="employeeDetailsId" id="employeeDetailsId">
                    <span class="showDates1">
                      <div class="input-field col s6 m6 l2">
                        <input type="date" class="datepicker" placeholder="Choose Date" name="startDate" id="startDate1" required>
                        <label for="startDate1">From</label>
                      </div>

                      <div class="input-field col s6 m6 l2">
                            <input type="date" class="datepicker" placeholder="Choose Date" name="endDate" id="endDate1">
                             <label for="endDate1">To</label>
                       </div>
                    <button type="button"  onclick="searchEmployeeSalary(4)">View</button>
                  </span>
                </form>
            </div>
       
 <div class="col s12 l12 m12">
	<br>
        <table id="empdetailtable" border="2" class="striped highlight centered display" cellspacing="0" width="100%">
            <thead>
                <tr>
                     <th class="print-col">Sr.No</th>
                     <th class="print-col">Amount Paid</th>
                     <th class="print-col">Date</th>
                     <th class="print-col">Mode of Payment</th>
                     <th class="print-col">Comment</th>
                 </tr>
            </thead>
            <tbody>
            	
            </tbody>
        </table>
        <br>
  </div>  
   </div>    
  <!-- edit incentive modal start -->
        <div id="incentivemodal" class=" row modal modal-fixed-footer">
        
        		<div class="modal-content">
                    <h4 class="center">Incentive Details</h4>
                    <hr>
                    <br>
                    <form class="col s12 l12 m12" action="${pageContext.request.contextPath}/giveIncentives" method="post" class="col s12 l12 m12" id="saveIncentive">
                    <input id="incentiveEmployeeDetailsId" name="employeeDetailsId" type="hidden" value="${employeeDetailsId}"/>
                                      
                            <div class="input-field col s12 l3  m3">
                            	<input id="incentiveId" type="hidden" name="incentiveId" value="0">
                                <label for="incentiveAmount">Incentive</label>
                                <input id="incentiveAmount" type="text" name="incentives">
                                <!--<h6 for="MobileNo"></h6>-->
                            </div>
                             <div class="input-field col s12 l4  m4 ">
                            	
                               Reason:<textarea id="incentiveReasonId" class="materialize-textarea" name="reason" style="padding: 0 0 0 0;"></textarea>
                                <!--<h6 for="MobileNo"></h6>-->
                            </div>
                             
                      			    <br> 
                      			<div class="col s6 m6 l2">
			              			  <button type="button" id="saveIncentivesSubmit" class="modal-action waves-effect  btn ">Update</button>
			               	    </div>
			               	   <!--  <div class="col s6 m6 l2">
			              			  <button type="reset" id="resetIncentive" class="modal-action waves-effect  btn ">Reset</button>
			                	 </div>  -->                    
			            </form>
			            <div class="col s12 l7  m5 offset-l3 offset-m4">                                
                   			  <font color='red'><span id="msgincentivesave"></span></font>                   			
            				</div> 
			            <div class="row">
                           <div class="col s6 m4 l4" style="margin-top:2%;">
		                            <!-- Dropdown Trigger -->
		                            <a class='dropdown-button btn waves-effect waves-light blue darken-6' href='#' data-activates='filterIncentive'>Filter &nbsp &nbsp &nbsp &nbsp   <i
		                class="material-icons right">arrow_drop_down</i></a>
		                            <!-- Dropdown Structure -->
		                            <ul id='filterIncentive' class='dropdown-content'>
		                                <li><button type="button" class="btn btn-flat" style="font-size:13px;" onclick="searchEmployeeIncentive(1)">This Month</button></li>
		                               <li><button type="button" class="btn btn-flat" style="font-size:13px;"  onclick="searchEmployeeIncentive(2)">Last 6 Months</button></li>
		                               <li><button type="button" class="btn btn-flat" style="font-size:13px;"  onclick="searchEmployeeIncentive(3)">Last 1 Year</button></li>
		                               <li><button type="button" class="btn rangeSelect btn-flat" style="font-size:13px;" >Range</button></li>
		                               <li><button type="button" class="btn btn-flat" style="font-size:13px;"  onclick="searchEmployeeIncentive(5)">View All</button></li>
		                            </ul>
		                        </div>
		                        <form action="ledger">
		                            <input type="hidden" name="range" value="dateRange"> 
		                            <span class="showDates">
		                            <input type="hidden" name="employeeDetailsId" id="employeeDetailsIdIncentive">
		                              <div class="input-field col s6 m6 l2">
		                                <input type="date" class="datepicker" placeholder="Choose Date" name="startDate" id="startDateIncentive" required> 
		                                <label for="startDateIncentive">From</label>
		                              </div>
		                              <div class="input-field col s6 m6 l2">
		                                    <input type="date" class="datepicker" placeholder="Choose Date" name="endDate" id="endDateIncentive">
		                                     <label for="endDateIncentive">To</label>
		                               </div>
		                            <button type="button" onclick="searchEmployeeIncentive(4)">View</button>
		                          </span>
		                        </form>
                        
                        </div>
                  <table border="2" class="centered tblborder">
                    <thead>
                        <tr>
                            <th>Sr.No</th>
                            <th>Amount</th>
                            <th>Reason</th>
                            <th>Date</th>
                            <th>Edit</th>
                        </tr>
                    </thead>
                    <tbody id="incentiveTable">
                    	
                    </tbody>
                </table>
                      </div>
        
             			 <div class="modal-footer">                  
                            
                          	
			                	 <div class="col s6 m6 l3 offset-l4">
			               				 <a href="#!" class="modal-action modal-close waves-effect btn red ">Close</a>
		                		</div>
		                		  <br>
		               </div>  
        </div>
        <!-- edit incentive modal end-->
<!--Add Edit Confirm Modal Structure -->
		<div class="row">
			<div class="col s12 m12 l12">
				<div id="addeditmsg" class="modal">
					<div class="modal-content">
						<h3 id="head"></h3>
						<p id="msg"></p>
					</div>
					<div class="modal-footer">
						<a href="#!"
							class="modal-action modal-close waves-effect waves-green btn-flat">OK</a>
					</div>
				</div>
			</div>
		</div>
    </main>
    <!--content end-->
</body>

</html>