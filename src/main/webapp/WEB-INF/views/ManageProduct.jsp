<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%> 
<html>

<head>
     <%@include file="components/header_imports.jsp" %>
   <script type="text/javascript">
   $(document).ready(
			function() {
				var msg="${saveMsg}";
				// alert(msg);
				 if(msg!='' && msg!=undefined)
				 {
				     $('#addeditmsg').modal('open');
				     $('#msgHead').text("Product Message");
				     $('#msg').text(msg);
				 }
			});
   </script>
   
    <style>
      /*   table.dataTable tbody td {
        padding: 0 6px !important;
    } */
        table, td, th {
  
    font-size:13px;
}
	 td.wrapok {
    white-space:nowrap !important;
}
	
	 /* div.dataTables_wrapper {
        width: 100%;
        margin: 0 auto;
    } */
    </style>
</head>

<body >
   <!--navbar start-->
   	<%@include file="components/navbar.jsp" %>
    <!--navbar end-->
    <!--content start-->
    <main>
        <div class="row" style="margin-bottom:0;">      
        <div class="col s10 l3 m6">
         <a class="btn waves-effect waves-light blue-gradient" href="${pageContext.request.contextPath}/fetchCategoriesList"><i class="material-icons left" >settings</i>Manage Category</a>
            
        </div> 
		 <div class="col s10 l3 m6 offset-l1 right-align">
            <a class="btn waves-effect waves-light blue-gradient" href="${pageContext.request.contextPath}/fetchBrandList"><i class="material-icons left" >settings</i>Manage Brand</a>
        </div> 
        <div class="col s10 l3 m6 offset-l2 right-align">
           <a class="btn waves-effect waves-light blue-gradient" href="${pageContext.request.contextPath}/addProduct"><i class="material-icons left" >add</i>Add Product</a>
        </div>
        </div>
    

            <table class="striped highlight centered" id="tblData">
                <thead>
                    <tr>
                        <th rowspan="2" class="print-col">Sr No.</th>
                        <th rowspan="2">Image</th>
                        <th rowspan="2" class="print-col">Product Code</th>
                        <th rowspan="2" class="print-col">Product</th>
                        <th rowspan="2" class="print-col">Category</th>
                        <th rowspan="2" class="print-col">Brand</th>                        
                         <th rowspan="2" class="print-col">HSN Code</th>
                         <th rowspan="2" class="print-col">Threshold Value</th>
                        <th rowspan="2" class="print-col">Unit Price</th>
                        <th rowspan="2" class="print-col">MRP</th>
                        <th rowspan="2" class="print-col">Current Quantity</th>
                        <th rowspan="2" class="print-col">Taxable Total</th>
                        <th colspan="2" class="print-col" >%CGST</th>
                        <th colspan="2" class="print-col">%SGST</th>
                        <th colspan="2" class="print-col">%IGST</th>
                        <th rowspan="2" class="print-col">Total</th>
                        <th rowspan="2" class="print-col">Tax Slab</th>
                        <th rowspan="2" class="print-col">Added Date</th>
                        <th rowspan="2" class="print-col">Updated Date</th>
                        <th rowspan="2">Edit</th>
                       
                        <!-- <th rowspan="2">Delete</th> -->

                    </tr>
                    <tr>
                    	<th class="print-col">%</th>
                        <th class="print-col">Amt</th>
                        <th class="print-col">%</th>
                        <th class="print-col">Amt</th>
                        <th class="print-col">%</th>
                        <th class="print-col">Amt</th>
                    </tr>
                </thead>

                <tbody>
                     <c:if test="${not empty productlist}">
							<c:forEach var="listValue" items="${productlist}">
                    <tr>
                        <td><c:out value="${listValue.srno}" /></td>
                        <td><img src="${pageContext.request.contextPath}/downloadProductImage/${listValue.productId}"
                        		alt="" style="border:1px black solid;height:40px;width:40px;" /></td>
                        
                        <td><c:out value="${listValue.productCode}" /></td>
                        <td class="wrapok"><c:out value="${listValue.productName}" /></td>
                        <td><c:out value="${listValue.categoryName}" /></td>
                        <td><c:out value="${listValue.brandName}" /></td>
                        <td><c:out value="${listValue.hSNCode}" /></td>
                        <td><c:out value="${listValue.thresholdValue}" /></td>
                        <td><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${listValue.productRate}" /></td>
 						<td><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${listValue.productRateWithTax}" /></td>
                        <td><c:out value="${listValue.currentQuantity}" /></td>
                        <td><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${listValue.taxableTotal}" /></td>
                        <td><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${listValue.cgstPercentage}" />%</td>
                        <td><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${listValue.cgstamount}" /></td>
                        <td><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${listValue.sgstPercentage}" />%</td>
                        <td><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${listValue.sgstamount}" /></td>
                        <td><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${listValue.igstPercentage}" />%</td>
                        <td><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${listValue.igstamount}" /></td>
                        <td><fmt:formatNumber type="number"  minFractionDigits="2" maxFractionDigits="2" value="${listValue.total}" /></td>
                        <td><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${listValue.igstPercentage}" />%</td>
                        <td>
                        <fmt:formatDate pattern="yyyy-MM-dd" var="dt" value="${listValue.productAddedDatetime}" />
                        <c:out value="${dt}" /></td>
                        <td>
                        <fmt:formatDate pattern="yyyy-MM-dd" var="dt2" value="${listValue.productQuantityUpdatedDatetime}" />
                        <c:out value="${dt2}" /></td>
                        
                        <td width="2%"><a class=" btn-flat " href="${pageContext.request.contextPath}/fetchProduct?productId=${listValue.productId}"><i class="material-icons tooltipped blue-text " data-position="right" data-delay="50" data-tooltip="Edit" >edit</i></a></td>
                        <!-- Modal Trigger -->
                        <!-- <td><a href="#delete" class="modal-trigger"><i class="material-icons tooltipped" data-position="right" data-delay="50" data-tooltip="Delete">delete</i></a></td> -->
                    </tr>
                    </c:forEach>
                    </c:if>
                </tbody>
            </table>

    
        
       	<div class="row">
			<div class="col s8 m2 l2">
				<div id="addeditmsg" class="modal" style="width:40%;">
					<div class="modal-content">
						<h5 id="msgHead">Error</h5>
						<hr>	
						<h6 id="msg"></h6>
					</div>
					<div class="col s8 m2 l2 offset-l5 modal-footer">
						<a href="#!" class="modal-action modal-close waves-effect btn blue-gradient">OK</a>
					</div>
				</div>
			</div>
		</div>
    </main>
    <!--content end-->
</body>

</html>