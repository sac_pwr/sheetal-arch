<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<html>
<head>
     <%@include file="components/header_imports.jsp" %>
     
     <script>var myContextPath = "${pageContext.request.contextPath}"</script>
      <script type="text/javascript" src="resources/js/hashtable.js"></script>
   <script type="text/javascript" src="resources/js/manageInventory.js"></script>
    <style>
        tr [colspan="2"] {
            text-align: center;
        }
        
        tr,
        td,
        th {
            text-align: center;
        }
    

	.dropdown-content{
	max-height:250px !important;
	}
    </style>
   
	<script type="text/javascript">
	$(document).ready(function() {	
		
		var url="${url}";
		 if(url==="editOrderBook")
		 {
			 //alert("${supplierOrderIddd}");
			 editOrder("${supplierOrderIddd}");
		 }
		 else
		 {
			 showOrderProductQuantity(0);
		 }
		
	});
	</script>

</head>

<body>
   <!--navbar start-->
   	<%@include file="components/navbar.jsp" %>
    <!--navbar end-->
    <!--content start-->
    <main>
       <div class="container">
                        <form action="${pageContext.servletContext.contextPath}/<c:out value="${url}"/>" method="post" id="orderBookForm" class="col s12 l12 m12">
							<input id="productWithSupplikerlist" type="hidden" class="validate" name="productIdList">
                        	<input type='hidden' id='currentUpdateRowId'>
                       		<input id="supplierOrderIdId" type="hidden" class="validate" name="supplierOrderId">
                           <div class="row  z-depth-3">
                            	<div class="input-field col s12 m4 l4 offset-l2 offset-m2">
                            	<label for="supplierIdForOrder" class="active"><span class="red-text">*</span></label>
                                    <select id="supplierIdForOrder" name="supplierId">
                                            <option value="0"  selected>Choose Supplier Name</option>
                                     </select>
                                    <!--<label>Supplier Name</label>-->
                                </div>
                                 <div class="input-field col s12 m4 l4">
                                    <a href="${pageContext.request.contextPath}/addSupplier" class="modal-action waves-effect waves-green btn blue-gradient" style="letter-spacing:0.2px;margin-top:3%">Add New Supplier</a>
                                </div>
                            
                              <div class="col s12 m12 l12">
                               <div class="input-field col s12 m4 l4 offset-l2 offset-m2">
                               <label for="brandIdForOrder" class="active"><span class="red-text">*</span></label>
                                     <select id="brandIdForOrder" name="brandId">
                                            <option value="0"  selected>Choose Brand</option>
                                           
                                     </select>
                                </div>
                                <div class="input-field col s12 m4 l4">
                                <label for="categoryIdForOrder" class="active"><span class="red-text">*</span></label>
                                     <select id="categoryIdForOrder" name="categoryId">
                                            <option value="0"  selected>Choose Category</option>
                                            
                                     </select>
                                </div>
                             </div>
                              <div class="col s12 m12 l12">
                                <div class="input-field col s12 m4 l4 offset-l2 offset-m2">
                               <label for="productIdForOrder" class="active"><span class="red-text">*</span></label>
                                     <select id="productIdForOrder" name="productId">
                                            <option value="0"  selected>Choose Product</option>
                                            
                                     </select>
                                </div>  
                                <div class="input-field col s12 m4 l4">
                                    <input id="supplierRateForOrder" type="text" required readOnly>
                                    <label for="supplierRateForOrder" class="black-text"><span class="red-text">*</span>Rate</label>
                                </div>                              
                              </div>
                               
                             <div class="col s12 m12 l12">
                                <div class="col s12 m3 l3 offset-l2 offset-m2">
                                    Mobile Number <input id="supplierMobileNumber" type="text">
                                    <!-- <label for="mobile"></label> -->
                                </div>
                                <div class="col s12 m1 l1" style="margin-top:4%">
                                    <input type="checkbox" id="OrderMobileNumberchecker" required/>
                                    <label for="OrderMobileNumberchecker" class="black-text">Edit</label>
                                    <!-- <a href="#!" class="modal-action waves-effect waves-green btn">Edit</a> -->
                                </div>
                                <div class="input-field col s12 m4 l4">
                                    <input id="orderQuantity" type="text" class="validate" required>
                                    <label for="orderQuantity" class="black-text"><span class="red-text">*</span>Quantity</label>
                                </div>
                             </div>
                            
                                <div class="input-field col s12 m12 l12 center">
                                    <button id="addOrderProduct" type="button" name="addOrderCart" class="modal-action waves-effect  btn blue-gradient">Add Cart</button>
                                    	
                                </div>
                           
               
						
                            <div class="col s12 l8 m8 offset-l2 offset-m2">
                            	<br>
                                <table class="tblborder centered">
                                    <thead>
                                        <tr>
                                            <th>Sr No.</th>
                                            <th>Supplier Name</th>
                                            <th>Product</th>
                                            <th>Rate</th>
                                            <th>Quantity</th>
                                            <th>Edit</th>
                                            <th>Cancel</th>
                                        </tr>
                                    </thead>
                                    <tbody id="orderCartTb">
                                        <!-- <tr>
                                            <td>1</td>
                                            <td>Ankit</td>
                                            <td>John</td>
                                            <td>300</td>
                                            <td>5</td>
                                            <td><button type="button" class="btn-flat"><i class="material-icons ">edit</i></button></td>
                                            <td><button type="button" class="btn-flat"><i class="material-icons ">cancel</i></button></td>
                                        </tr> -->
                                    </tbody>
                                </table>
                                <br><br>
                            </div>


							 </div>
                        </form>
                        	  <div class="col s12 m12 l12 center">
                        <button type="button" id="orderProductsButton" class="waves-effect btn blue-gradient">Order Products</button>
                    </div>
                    </div>
              
         
               
                  
                    
 		<div class="row">
			<div class="col s8 m2 l2">
				<div id="addeditmsg" class="modal" style="width:40%;">
					<div class="modal-content">
						<h5 id="msgHead">Error</h5>
						<hr>	
						<h6 id="msg"></h6>
					</div>
					<div class="col s8 m2 l2 offset-l5 modal-footer">
						<a href="#!"
							class="modal-action modal-close waves-effect btn blue-gradient">OK</a>
					</div>
				</div>
			</div>
		</div>
             
        
    </main>
    </body>