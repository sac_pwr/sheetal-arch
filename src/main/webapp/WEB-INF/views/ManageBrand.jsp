<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<html>
 
<head>
     <%@include file="components/header_imports.jsp" %>

<script type="text/javascript">

 $(document).ready(function(){
	 
	 var msg="${saveMsg}";
	 //alert(msg);
	 if(msg!='' && msg!=undefined)
	 {
	     $('#addeditmsg').modal('open');
	     $('#msgHead').text("Brand Message");
	     $('#msg').text(msg);
	 }
	 
	$("#resetBrandSubmit").click(function(){
		
		$('#brandname').val('');	
		$('#brandId').val('0');
		$("#saveBrandSubmit").html('<i class="material-icons left">add</i> Add');

	});
	
	$('#addBrandButton').click(function(){
		
		$('#brandname').val('');	
		$('#brandId').val('0');
		$('#brandname').focus();
		$('#brandname').change();
		$("#saveBrandSubmit").html('<i class="material-icons left">send</i> Add');
		
	});
});
 
function editBrand(id) {
	
	$.ajax({
				type : "GET",
				url : "${pageContext.request.contextPath}/fetchBrand?brandId="+id,
				/* data: "id=" + id + "&name=" + name, */
				beforeSend: function() {
						$('.preloader-background').show();
						$('.preloader-wrapper').show();
			           },
				success : function(data) {
					var brand = data;
					//alert(brand.name +" "+ brand.brandId);
					$('#brandname').val(brand.name);	
					$('#brandId').val(brand.brandId);
					$('#brandname').focus();
					$('#brandname').change();
					$("#saveBrandSubmit").html('<i class="material-icons left">send</i> Update');
					$('#addBrand').modal('open');
					
					$('.preloader-wrapper').hide();
					$('.preloader-background').hide();
				},
				error: function(xhr, status, error) {
					$('.preloader-wrapper').hide();
					$('.preloader-background').hide();
					  //alert(error +"---"+ xhr+"---"+status);
					$('#addeditmsg').modal('open');
           	     	$('#msgHead').text("Message : ");
           	     	$('#msg').text("Something Went Wrong"); 
           	     		setTimeout(function() 
						  {
  	     					$('#addeditmsg').modal('close');
						  }, 1000);
					}					
			
			});
}


</script>
<style>

</style>

</head>

<body>
   <!--navbar start-->
   	<%@include file="components/navbar.jsp" %>
    <!--navbar end-->
    <!--content start-->
    <main>
    	<div class="row" style="margin-bottom:0;">
    		<div class="col s10 l3 m4 right">
                    <a  class="btn waves-effect waves-light blue-gradient right modal-trigger" id="addBrandButton" href="#addBrand"><i class="material-icons left" >add</i>Add Brand</a>
             </div>
    
      	 
      	 <div class="row" style="margin-bottom:0;">
			<div class="col s8 m12 l12">
				<div id="addBrand" class="modal" style="width:40%;">
					<form  action="${pageContext.servletContext.contextPath}/saveBrand" method="post" id="saveBrandForm">
					<div class="modal-content">
						<h5 class="center">Add Brand</h5>
						<hr>
       					 <input id="brandId" type="hidden" name="brandId" value="0" >
        				<div class="input-field col s12 m4 l7 left offset-l3" >                	
                    <input id="brandname" type="text" name="name" value="" required>
                    <label for="brandname" class="active"><span class="red-text">*</span>Enter Brand Name</label>
               			 </div> 
					</div>
					<div class="col s8 m2 l4 offset-l3 modal-footer">
						 <button id="saveBrandSubmit" class="btn modal-action  waves-effect waves-light blue-gradient" type="submit" name="action"><i class="material-icons left" >add</i>Add</button>
						
					</div>
					 </form>
				</div>
			</div>
		</div>
        
        
		<div class="col s12 m12 l12" style="padding:0;">
            <table class="striped highlight centered" id="tblData" cellspacing="0" width="100%">
                <thead>
                    <tr>

                        <th class="print-col">Sr.No.</th>
                        <th class="print-col">Name</th>
                        <th class="print-col">Date</th>
                        <th>Edit</th>
                        <!-- <th>Delete</th> -->

                    </tr>
                </thead>

                <tbody>
                <% int rowincrement=0; %>
                     <c:if test="${not empty brandlist}">
							<c:forEach var="listValue" items="${brandlist}">
						
						<c:set var="rowincrement" value="${rowincrement + 1}" scope="page"/>
						<tr>
	                        <td><c:out value="${rowincrement}" /></td>	
	                        <td><c:out value="${listValue.name}" /></td>
	                        <td>
								<fmt:formatDate pattern="dd-MM-yyyy" var="dt" value="${listValue.brandAddedDatetime}" />
		                        <c:out value="${dt}" /> & 
		                        <fmt:formatDate pattern="HH:mm:ss" var="time" value="${listValue.brandAddedDatetime}" />
		                         <c:out value="${time}" />
	                        </td>
	                        <td><button class="btn-flat" class="brandedit" onclick='editBrand(${listValue.brandId})'><i class="material-icons tooltipped blue-text " data-position="right" data-delay="50" data-tooltip="Edit" >edit</i></button></td>
	                   
	                   <!-- Modal Trigger -->
                       <!--  <td><a href="#delete" class="modal-trigger"><i class="material-icons tooltipped" data-position="right" data-delay="50" data-tooltip="Delete">delete</i></a></td> -->
                    	</tr>
                    	</c:forEach>
                    </c:if>
                 
                </tbody>
            </table>
        	</div>
        	</div>    
        	
        	
        	   <!-- Modal Structure for delete -->

                  <!--   <div id="delete" class="modal">
                        <div class="modal-content">
                            <h4>Confirmation</h4>
                            <p>Are you sure you want to delete?</p>
                        </div>
                        <div class="modal-footer">
                            <a href="#!" class="modal-action modal-close waves-effect  btn-flat ">Delete</a>
                            <a href="#!" class="modal-action modal-close waves-effect btn-flat ">Close</a>
                        </div>
                    </div> -->        
        <div class="row">
			<div class="col s8 m2 l2">
				<div id="addeditmsg" class="modal" style="width:40%;">
					<div class="modal-content">
						<h5 id="msgHead">Error</h5>
						<hr>	
						<h6 id="msg"></h6>
					</div>
					<div class="col s8 m2 l2 offset-l5 modal-footer">
						<a href="#!"
							class="modal-action modal-close waves-effect btn blue-gradient">OK</a>
					</div>
				</div>
			</div>
		</div>
    </main>
    <!--content end-->
</body>

</html>