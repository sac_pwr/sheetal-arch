<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%> 
<html lang="en">
<head>
<%@include file="components/header_imports.jsp" %>
<script>var myContextPath = "${pageContext.request.contextPath}"</script>
 <script type="text/javascript" src="resources/js/locationscript.js"></script> 
<script>
/* document.addEventListener("DOMContentLoaded", function(){
	
	alert();
	$('.preloader-background').delay(1700).fadeOut('slow');
	
	$('.preloader-wrapper')
		.delay(1700)
		.fadeOut();
}); */
</script>
<style>
/* .preloader-background {
	display: flex;
	align-items: center;
	justify-content: center;
	background-color: #eee;	
	position: fixed;
	z-index: 100;
	top: 0;
	left: 0;
	right: 0;
	bottom: 0;	
	opacity:0.8;
	} */
	input[type=search] {
    margin-top: 11% !important;
    margin-bottom: 0 !important;
    border-bottom: none !important;
    height: 2rem !important;
    text-indent: 10px !important;
}
div.dataTables_filter {
    margin: 0 1% !important;
}

</style>

</head>

<body>
   <!--navbar start-->
   	<%@include file="components/navbar.jsp" %>
    <!--navbar end-->
    <!--content start-->
    
 

    
    <main>
	<div class="row">
		<div class="col s12 l12 m12">
			<ul class="tabs tabs-fixed-width">
				<li class="tab col s4"><a class="btn z-depth-3 grey lighten-3 active" href="#route" id="routeTab">Route</a></li>
				<li class="tab col s4"><a class="btn z-depth-3 grey lighten-3" href="#area" id="areaTab">Area</a></li>
				<li class="tab col s4"><a class="btn z-depth-3 grey lighten-3" href="#cluster" id="clusterTab">Cluster</a></li>
			</ul>
		</div>
	</div>
	
	<!--ROUTE START-->
	<div id="route">
	<div class="row">
		<div class="col s12 m6 l6 right">
			 <!-- Modal Trigger -->
  			<button class="waves-effect waves-light btn right blue-gradient"  onclick="openRouteEdit(0)">Add New Route</button>
		</div>
		<div class="col s12 m12 l12">
		
   		<table class="striped highlight centered" id="tblDataRoute">
				<thead>
					<tr>
						<th class="print-col">Sr.No.</th>
						<th class="print-col">Route No</th>
						<th class="print-col">Area</th>
						<th class="print-col">Cluster</th>
						<th>Edit</th>
					</tr>
				</thead>

				<tbody>
					<!-- <tr>
						<td>1</td>
						<td>abc</td>
						<td>abc</td>
						<td>abc</td>
						<td><a href="#"><i class="material-icons tooltipped" data-position="right" data-delay="50" data-tooltip="Edit">edit</i></a></td>
						
					</tr> -->
					
				</tbody>
			</table>	
	</div>		
	</div>
		
   <div class="row modal" id="addRoute">
		<form   action="${pageContext.servletContext.contextPath}/saveRoute" method="post" id="saveRouteForm">
			<!--Add New Region-->
		<!-- style="display: none;" -->
			 <div class="modal-content">
			 <div class="row">
				<h4 class="center">Add Route</h4>
				<hr>
				<div class="input-field col s10 l4 m4" >
				<label for="clusterSelectForRoute" class="active"><span class="red-text">*</span></label>
					<select id="clusterSelectForRoute" name="clusterId">
						<option value="0" selected>Choose Cluster</option>
					</select>

				</div>
				
				<div class="input-field col s10 l4 m4">
				<label for="areaSelectForRoute" class="active"><span class="red-text">*</span></label>
					<select id="areaSelectForRoute" name="areaId">
						<option value="0" selected>Choose Area</option>
					</select>

				</div>
				<div class="input-field col s10 l4 m4">
					<input value="0" id="addRouteId" type="hidden" class="validate" name="routeId">
					<input value="" id="txtAddRoute" type="text" class="validate" name="routeNo">
					<label for="txtAddRoute"><span class="red-text">*</span>Enter Route No</label>
					
				</div>
				<div class="col s10 l7 m7 offset-l4">
					<span class="red-text" id="errorMsgForRoute"></span>
				</div>
				</div>
			</div>	
				
				
				
				<div class="modal-footer">
				
						<div class="col s10 l3 m3 offset-l3" style="margin-top:2%;">
					<button class="btn waves-effect waves-light blue-gradient"
						type="button" name="action" id="saveRouteSubmit">
						Add
					</button>
				   </div>
					<div class="col s10 l2 m2" style="margin-top:2%;">
					<a href="#!" class="btn modal-action modal-close waves-effect  blue-gradient">Cancel</a>
				   </div>
      					
    			</div>			
		</form>
   </div>
   
	</div>
	<!--ROUTE END-->
	
	
	<!--AREA START-->
	<div id="area">
		<div class="row">
		<div class="col s12 m6 l6 right">
			 <!-- Modal Trigger -->
  			<button class="waves-effect waves-light btn  right blue-gradient" onclick="openAreaEdit(0)">Add New Area</button>
		</div>	
		
		<div class="col s12 l12 m12 ">
		
			<table class="striped highlight centered" id="tblDataArea">
				<thead>
					<tr>
						<th class="print-col">Sr.No.</th>
						<th class="print-col">Area</th>
						<th class="print-col">Cluster</th>						
						<th>Edit</th>
						<!-- <th>Delete</th> -->

					</tr>
				</thead>

				<tbody>
				<!-- 	<tr>
						<td>1</td>
						<td>abc</td>
						<td>abc</td>
					
						<td><a href="#" ><i class="material-icons tooltipped"
								data-position="right" data-delay="50" data-tooltip="Edit">edit</i></a></td>
						Modal Trigger
						<td><a href="#delete" class="modal-trigger"><i
								class="material-icons tooltipped" data-position="right"
								data-delay="50" data-tooltip="Delete">delete</i></a></td>
					</tr> -->
					<!-- Modal Structure for delete -->


				</tbody>
			</table>

			<div id="delete" class="modal">
				<div class="modal-content">
					<h4>Confirmation</h4>
					<p>Are you sure you want to delete?</p>
				</div>
				<div class="modal-footer">
					<a href="#!"
						class="modal-action modal-close waves-effect  btn-flat ">Delete</a>
					<a href="#!"
						class="modal-action modal-close waves-effect btn-flat ">Close</a>
				</div>
			</div>
		</div>	
	</div>
	
	<div class="row modal" id="addArea">
		<form   action="${pageContext.servletContext.contextPath}/saveArea" method="post" id="saveAreaForm">
			<!--Add New Region-->
		<!-- style="display: none;" -->
			 <div class="modal-content">
			 <div class="row">
				<h4 class="center">Add Area</h4>
				<hr>
				<div class="input-field col s10 l4 m4 offset-l2">
				<label for="clusterSelectForArea" class="active"><span class="red-text">*</span></label>
					<select id="clusterSelectForArea" name="clusterId">
						<option value="0" selected>Choose Cluster</option>
					</select>

				</div>
				
				
				<div class="input-field col s10 l4 m4">
					<input value="0" id="addAreaId" type="hidden" class="validate" name="areaId">
					<input value="" id="txtAddArea" type="text" class="validate" name="name">
					<label for="txtAddArea"><span class="red-text">*</span>Enter Area Name</label>
					
				</div>
				<div class="col s10 l7 m7 offset-l4">
					<span class="red-text errorMsg" id="errorForArea"></span>
				</div>
				</div>
			</div>	
				
				
				
				<div class="modal-footer">
			
					<div class="col s10 l3 m3 offset-l3" style="margin-top:2%;">
					<button class="btn waves-effect waves-light blue-gradient"
						type="button" name="action" id="saveAreaSubmit">
						Add
					</button>
				   </div>
					<div class="col s10 l2 m2" style="margin-top:2%;">
					<a href="#!" class="btn modal-action modal-close waves-effect  blue-gradient">Cancel</a>
				   </div>
      					
    			</div>			
		</form>
   </div>
	

		
	</div>
	<!--AREA END-->
	
	  
	<!--CLUSTER START-->
	<div id="cluster">

		<div class="row">
		<div class="col s12 m6 l6 right">
			 <!-- Modal Trigger href="#addCluster"-->
  			<button class="waves-effect waves-light btn right blue-gradient" onclick="openClusterEdit(0)">Add New Cluster</button>
		</div>
		<div class="col s12 l12 m12">
	
			<table class="striped highlight centered " id="tblDataCluster">
				<thead>
					<tr>
						<th class="print-col">Sr.No.</th>
						<th class="print-col">Cluster</th>
						<th>Edit</th>
						<!-- <th>Delete</th> -->

					</tr>
				</thead>

				<tbody>
					<!-- <tr>
						<td>1</td>
						<td>abc</td>
						
						<td><button onclick="openClusterEdit(3)"><i class="material-icons tooltipped"
								data-position="right" data-delay="50" data-tooltip="Edit">edit</i></button></td>
						
					</tr> -->
				</tbody>
			</table>
		</div>		
	</div>
	
	<div class="row modal" id="addCluster" style="width:40%;">
		<form   action="${pageContext.servletContext.contextPath}/saveCluster" method="post" id="saveClusterForm">
			<!--Add New Region-->
		<!-- style="display: none;" -->
			 <div class="modal-content">
				<h4 class="center">Add Cluster</h4>
				<hr>
				
				<div class="input-field col s10 l7 m4 offset-l3">
					<input value="0" id="clusterIdForCluster" type="hidden" class="validate" name="clusterId">
					<input value="" id="txtAddCluster" type="text" class="validate" name="name">
					<label for="txtAddCluster"><span class="red-text">*</span>Enter Cluster</label>
					
				</div>
				<div class="col s10 l7 m7 offset-l4">
					<span class="red-text errorMsg" id="errorForCluster"></span>
				</div>
			</div>	
			
				<div class="modal-footer">
				
					<div class="col s10 l3 m3 offset-l3" style="margin-top:2%;">
					<button class="btn waves-effect waves-light blue-gradient"
						type="button" name="action" id="saveClusterSubmit">Add</button>
				   </div>
					<div class="col s10 l2 m2" style="margin-top:2%;">
					<a href="#!" class="btn modal-action modal-close waves-effect  blue-gradient">Cancel</a>
				   </div>
      					
    			</div>			
		</form>
   </div>


		
	</div>
	<!--CLUSTER END--> 
	 </main>
	<!--content end-->
	<div class="row">
			<div class="col s12 m12 l12">
				<div id="addeditmsg" class="modal" style="width:40%;">
					<div class="modal-content">
						<h5 id="msgHead"></h5>
						<hr>
						<h6 id="msg"></h6>
					</div>
					<div class="col s8 m2 l2 offset-l5 modal-footer">
						<a href="#!" class="modal-action modal-close waves-effect waves-green btn blue-gradient">OK</a>
					</div>
				</div>
			</div>
		</div>
	
</body>

</html>