<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<html>

<head>
      <%@include file="components/header_imports.jsp" %>

    <script>
        $(document).ready(function() {
            $('.chequeNo').hide();
            $('#cash').click(function() {
                $('.chequeNo').hide();
            });
            $('#cheque').click(function() {
                $('.chequeNo').show();
            });


            $('#amount').keydown(function(e){            	
				-1!==$.inArray(e.keyCode,[46,8,9,27,13,110,190])||/65|67|86|88/.test(e.keyCode)&&(!0===e.ctrlKey||!0===e.metaKey)||35<=e.keyCode&&40>=e.keyCode||(e.shiftKey||48>e.keyCode||57<e.keyCode)&&(96>e.keyCode||105<e.keyCode)&&e.preventDefault()
			 });
            
			document.getElementById('amount').onkeypress=function(e){
            	
            	if (e.keyCode === 46 && this.value.split('.').length === 2) {
              		 return false;
          		 }

            }	
            $('#paySubmit').click(function(){
            	var payCash=$('#cash').is(':checked');
    			var payCheque=$('#cheque').is(':checked');
    			
    			var amount=$('#amount').val();
				var bankName=$('#bankName').val();
				var cheqNo=$('#cheqNo').val();
				var cheqDate=$('#cheqDate').val();
				var paymentcomment=$('#comment').val();
				var type=$('#typeId').val();
				
				if(amount==="")
				{
					$('#addeditmsg').modal('open');
				     $('#msgHead').text("Payment Message");
				     $('#msg').text("Enter amount");
					return false;
				}
				
				if(paymentcomment==="" && type==="employee")
				{
					 $('#addeditmsg').modal('open');
				     $('#msgHead').text("Payment Message");
				     $('#msg').text("Enter Comment");
					return false;
				}
				
				if(payCheque==true)
				{
					if(bankName==="")
					{
						$('#addeditmsg').modal('open');
					     $('#msgHead').text("Payment Message");
					     $('#msg').text("Enter bank name");
						return false;
					}
					
					if(cheqNo==="")
					{
						$('#addeditmsg').modal('open');
					     $('#msgHead').text("Payment Message");
					     $('#msg').text("Enter Cheque number");
						return false;
					}
					
					if(cheqDate==="")
					{
						$('#addeditmsg').modal('open');
					     $('#msgHead').text("Payment Message");
					     $('#msg').text("Enter cheque date");
						return false;
					}
					
					var today=new Date().setHours(0,0,0,0);
					var chDate=new Date(cheqDate).setHours(0,0,0,0);
					
					if(chDate < today || chDate == today)
					{
						 $('#addeditmsg').modal('open');
					     $('#msgHead').text("Payment Message");
					     $('#msg').text("Select Cheque Date After Current Date");
						return false;
					}
					
				}
				else
				{
					$('#cheqDate').val('');
				}
            });
			

        });
    </script>
</head>

<body>
     <!--navbar start-->
   	<%@include file="components/navbar.jsp" %>
    <!--navbar end-->
    <!--content start-->
    <main>
      
        <div class="row">
            <div class="col l12 m12 s12">
                <!-- <h3 class="center"> Payment Details </h3> --><br>
            </div>
            <div class="col s12 m6 l4 offset-l1 z-depth-3 grey lighten-4" style="padding: 20px;margin-left:15%; height:320px;">
                
                
                <c:choose>  
				    <c:when test="${paymentDoInfo.type=='supplier'}">  
				       <h5 class="blue-text text-darken-8 center-align">Supplier Id : <c:out value="${paymentDoInfo.id}" /></h5>
				       <hr style="border:1px dashed teal;">
				       <h5 class="blue-text text-darken-8 center-align">Inventory Id : <c:out value="${paymentDoInfo.inventoryId}" /></h5>
				    </c:when>  
				    <c:otherwise>  
				       <h5 class="blue-text text-darken-8 center-align">Employee Id : <c:out value="${paymentDoInfo.id}" /></h5>
				    </c:otherwise>  
				</c:choose>  
                
                <hr style="border:1px dashed teal;">
                <h5 class="blue-text text-darken-8  center-align">Name : <c:out value="${paymentDoInfo.name}" /> </h5>
                <hr style="border:1px dashed teal;">
                <h5 class="center-align">Total Amount : <fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${paymentDoInfo.totalAmount}" /> </h5>
                 <hr style="border:1px dashed teal;">
                <h5 class="blue-text text-darken-8  center-align">Amount Paid : <fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${paymentDoInfo.amountPaid}" /></h5>
                <hr style="border:1px dashed teal;">
                <h5 class="blue-text text-darken-8  center-align"><b class="red-text">Balance Amount : <fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${paymentDoInfo.amountUnPaid}" /> </b></h5>
                
               
                
            </div>
            <div class="col s12 m6 l4 offset-l1 z-depth-3 grey lighten-4" style="padding: 20px;height:auto;">
                <form action="${pageContext.servletContext.contextPath}/${paymentDoInfo.url}" method="post">
                	<input value="${paymentDoInfo.type}" type="hidden" id="typeId">
                	<input value="${paymentDoInfo.pkId}" type="hidden" name="employeeDetailsId">
                	<input value="${paymentDoInfo.inventoryId}" type="hidden" name="inventoryId">
                	
                    <div class="col l11 m12 s12 offset-l1" style="margin-top:10px;">
                        <b> Payment Mode : <input type="radio" id="cash" name="group1" checked /> <label for="cash">Cash</label> <input type="radio" id="cheque" name="group1" /> <label for="cheque">Cheque</label></b>
                    </div>
                    <br>
                    <div class="input-field col l6 m6 s6 chequeNo">
                        <input type="text" name="cheqNo" class="validate num" id="cheqNo" title="Enter Cheque Number"> <label for="customerChequeNumber">Cheque Number</label>
                    </div>
                    <div class="input-field col l6 m6 s6 chequeNo">
                        <input type="text" name="bankName" class="validate" id="bankName" title="Enter Bank Name"> <label for="customerBankName">Bank Name</label>
                    </div>
                    <div class="input-field col l6 m6 s6 chequeNo">
                        <input type="date" class="datepicker" placeholder="Cheque Date" name="cheqDate" id="cheqDate">
                    </div>
                    <br>
                    <div class="input-field col s6 m6 l6">
                        <input type="text" name="amount" id="amount" title="Enter Amount" required>
                        <label for="amount">Amount</label>
                    </div>
                    <c:choose>  
					    <c:when test="${paymentDoInfo.type=='employee'}">
		                    <div class="input-field col s12 m12 l12">
		                        <textarea id="comment" type="text" name="comment" id="comment" class="materialize-textarea" title="Enter Comment" required></textarea>
		                        <label for="comment">Comment</label>
		                    </div>
	                    </c:when>
                    </c:choose>
                    <br><br>
                    <div class="input-field col s6 m6 l6 offset-l4 offset-m4 offset-s4">
                        <button id="paySubmit" class="btn waves-effect waves-light blue-gradient" type="submit">Pay<i class="material-icons right">send</i>
                                </button>
                    </div>
                    <br><br>
                </form>
            </div>

        </div>









        <br>
        <!--<div class="container">
            <form action="" method="post" name="aboutus">
                <div class="row  z-depth-3">
                    <div class="col l12 m12 s12">
                        <h4 class="center"> Payment Details </h4>
                    </div>
                    <div class="row">

                        <div class="input-field col s12 m6 l6 push-l2 push-m2 offset-m1 offset-l1">
                            <h5 class="left"> Name: </h5>
                            <h5 class="right" id="name">ABC </h5>
                        </div>

                        <div class="input-field col s12 m6 l6 push-l2 push-m2 offset-m1 offset-l1">
                            <h5 class="left">Total Amount: </h5>
                            <h5 class="right" id="totalAmt">123 </h5>
                        </div>

                        <div class="input-field col s12 m6 l6 push-l2 push-m2 offset-m1 offset-l1">
                            <h5 class="left">Amount Paid: </h5>
                            <h5 class="right" id="amtPaid">123 </h5>

                        </div>
                        <div class="input-field col s12 m6 l6 push-l2 push-m2 offset-m1 offset-l1">
                            <h5 class="left">Amount Remaining: </h5>
                            <h5 class="right" id="amtRemaining">123 </h5>
                        </div>
                        <br>
                        <div class="input-field col s12 m6 l12 push-l2 push-m2 offset-m1 offset-l1">
                            <div class="col s12 l6">
                                <p>

                                    <input name="group1" type="radio" id="test1" class="cash" checked/>
                                    <label class="left" for="test1">Cash</label>

                                    <input name="group1" type="radio" id="test2" class="cheque" />
                                    <label class="right" for="test2">Cheque</label>
                                </p>
                            </div>
                        </div>
                        <div class="input-field col s12 m12 l3 push-m2 offset-m1 push-l2 offset-l1">
                            <input id="amount" type="text">
                            <label for="amount" class="active">Enter Amount:</label>
                        </div>
                        <div id="BankDetails">

                            <div class="input-field col s12 m6 l3 push-l2 ">
                                <input id="bankName" type="text">
                                <label for="bankName" class="active">Enter Bank Name:</label>
                            </div>
                            <br>
                            <div class="col s12 m6 l12 push-l2">
                                <div class="input-field col s12 m6 l3 push-l1 ">
                                    <input id="cheqNo" type="text">
                                    <label for="cheqNo" class="active">Enter Cheque No.:</label>
                                </div>
                                <div class="input-field col s12 m6 l3 push-l1 ">
                                    <input id="cheqDate" type="text" class="datepicker">
                                    <label for="cheqDate">Enter Cheque Date:</label>
                                </div>
                            </div>
                        </div>
                        <div class="input-field col s12 m5 l5 pull-l1 pull-l1 offset-m1 offset-l5">
                            <!--<i class="material-icons prefix">mode_edit</i>-->
        <!--<textarea id="comment" class="materialize-textarea"></textarea>
        <label for="comment">Comment</label>
        </div>

        </div>

        </div>

        <div class="input-field col s12 m6 l4 offset-l5 center-align">
            <button class="btn waves-effect waves-light blue darken-8" type="submit">Pay<i class="material-icons right">send</i> </button>
        </div>
        <br>
        </form>-->
        </div>
        
       	<div class="row">
			<div class="col s8 m2 l2">
				<div id="addeditmsg" class="modal" style="width:40%;">
					<div class="modal-content">
						<h5 id="msgHead">Error</h5>
						<hr>	
						<h6 id="msg"></h6>
					</div>
					<div class="col s8 m2 l2 offset-l5 modal-footer">
						<a href="#!"
							class="modal-action modal-close waves-effect btn blue-gradient">OK</a>
					</div>
				</div>
			</div>
		</div>
    </main>





    <!--content end-->
</body>

</html>