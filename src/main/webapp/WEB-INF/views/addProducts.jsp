<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

<head>
      <%@include file="components/header_imports.jsp" %>

<script type="text/javascript">
var igst=0;
var cgst=0;
var sgst=0;
$(document).ready(function() {
			
			/* $("#categoryid").change(function(){
				$.ajax({
					url :"${pageContext.request.contextPath}/fetchCategories?categoriesId="+$("#categoryid").val(),
					dataType : "json",
					success : function(data) {
						igst=data.igst;
						cgst=data.cgst;
						sgst=data.sgst;		
						 $('#productRate').keyup();
						
					}
				});
			}); */
      
			var categoryIgst;
			var categorySgst;
			var categoryCgst;
			
			$("#categoryid").change(function(){
				var categoryid=$("#categoryid").val();
					$.ajax({
						type : "GET",
						url :"${pageContext.request.contextPath}/fetchCategories?categoriesId="+categoryid,
						beforeSend: function() {
							$('.preloader-background').show();
							$('.preloader-wrapper').show();
				           },
				           success : function(data) {
				        	   categoryIgst = data.igst;
				        	    categorySgst = data.sgst;
				        	    categoryCgst = data.cgst;
				        	   //console.log(CategoryIgst);
				        	    $('.preloader-wrapper').hide();
								$('.preloader-background').hide(); 
				           },
				           error: function(xhr, status, error) {
								$('.preloader-wrapper').hide();
								$('.preloader-background').hide();
								$('#addeditmsg').modal('open');
			           	     	$('#head').text("Warning: ");
			           	     	$('#msg').text("Something Went Wrong"); 
			           	     		setTimeout(function() 
									  {
			  	     					$('#addeditmsg').modal('close');
									  }, 1000);
								}						
				           
					});			
			});
			/* for calculating unit price of product */
			$("#productRate").blur(function(){
				
				var rate=parseFloat($("#productRate").val());
				if(rate>0){
				var unitprice;
				var divisor;
				var multiplier;
				var igstAmt;
				var cgstAmt;
				var sgstAmt;
				
				/* for calculating divisor means*/
				divisor=(categoryIgst/100)+1;
				//console.log(divisor);
				unitprice=(rate/divisor);
				//console.log(unitprice);
				cgstAmt=(unitprice*categoryCgst)/100;
				//console.log(igstAmt);
				//for calculating cgst percent from igst percent
				//var cgstPer=(categoryIgst/2);					
				igstAmt=parseFloat(cgstAmt.toFixed(2))+parseFloat(cgstAmt.toFixed(2));
				//console.log(cgstAmt);
				
				$("#unitRate").val(unitprice.toFixed(2));
				$("#unitRate").focus();
				$("#cgst").val(cgstAmt.toFixed(2));
				$("#cgst").focus();
				$("#sgst").val(cgstAmt.toFixed(2));
				$("#sgst").focus();
				$("#igst").val(igstAmt);
				$("#igst").focus();
					
				
				}
			});
			var msg="${saveMsg}";
			 //alert(msg);
			 if(msg!='' && msg!=undefined)
			 {
			     $('#addeditmsg').modal('open');
			     $('#msgHead').text("Product Adding Message");
			     $('#msg').text(msg);
			 }			 
		
				  $('#productrate').on('keydown', '#productRate', function(e){-1!==$.inArray(e.keyCode,[46,8,9,27,13,110,190])||/65|67|86|88/.test(e.keyCode)&&(!0===e.ctrlKey||!0===e.metaKey)||35<=e.keyCode&&40>=e.keyCode||(e.shiftKey||48>e.keyCode||57<e.keyCode)&&(96>e.keyCode||105<e.keyCode)&&e.preventDefault()});
				  $('#threshold').keypress(function( event ){
					    var key = event.which;
					    
					    if( ! ( key >= 48 && key <= 57 ) )
					        event.preventDefault();
					});
				  
				  /* $('#productRate').on('keyup',function(){
					    var productrate=$('#productRate').val();
					    //alert(productrate);
						if(productrate===undefined || productrate==="")
						{
							$('#productRateWithTax').val('0');
							$('#productTax').val('0');
						}
						else
						{
							var tax=( (parseFloat(productrate)*parseFloat(igst))/100 )+
									 ( (parseFloat(productrate)*parseFloat(cgst))/100 )+
									 ( (parseFloat(productrate)*parseFloat(sgst))/100 );
							var totalProduct=parseFloat(productrate)+tax;
							$('#productRateWithTax').val(totalProduct);
							$('#productTax').val(tax);
						}
						$('#productRateWithTax').change();
						$('#productTax').change();
				  }); */
			  $("#saveProductSubmit").click(function() {
					
					 var brandid = $("#brandid").val();
					//alert(brandid);
					if (brandid === "0") {
						$('#addeditmsg').modal('open');
					     $('#msgHead').text("Product Adding Message");
					     $('#msg').text("Select Brand");
						return false;
					}
				 
					var categoryid = $("#categoryid").val();
					//alert(categoryid);
					if (categoryid === "0") {
						$('#addeditmsg').modal('open');
					     $('#msgHead').text("Product Adding Message");
					     $('#msg').text("Select Category");
						return false;
					}
					 
					var units = $("#unitId").val();
					//alert(categoryid);
					if (units === "0") {
						$('#addeditmsg').modal('open');
					     $('#msgHead').text("Product Adding Message");
					     $('#msg').text("Select Unit Type");
						return false;
					}
				}); 
				  $("#addImage").click(function() {
					    $("#upload").click();
				});
			$('#upload').change(function (evt) {
			    var tgt = evt.target || window.event.srcElement,
			        files = tgt.files;
			
			    // FileReader support
			    if (FileReader && files && files.length) {
			        var fr = new FileReader();
			        fr.onload = function () {
			            document.getElementById('addImage').src = fr.result;
			        }
			        fr.readAsDataURL(files[0]);
			    }
			
			    // Not supported
			    else {
			        // fallback -- perhaps submit the input to an iframe and temporarily store
			        // them on the server until the user's session ends.
			    }
			}); 
		});
</script>

</head>

<body>
   <!--navbar start-->
   	<%@include file="components/navbar.jsp" %>
    <!--navbar end-->
    <!--content start-->
    <main>
        <br>
        <div class="container">
            <form action="${pageContext.servletContext.contextPath}/saveProduct" method="post" id="saveProductForm"  enctype="multipart/form-data">
				<!-- <input id="productId" type="text" class="validate" name="productId" value="0"> -->
                <div class="row  z-depth-3 ">
                    <br>
                    <div class="input-field col s12 m6 l4 left-align">
                        <img style="border:1px solid;height:200px;width:200px; margin-left:10%;margin-top:2%;" id="addImage" src="resources/img/defaultphoto.png">
                        <input type="file" id="upload" style="display: none;" name="file"/>
                    </div>

                    <div class="input-field col s12 m4 l4">

                        <i class="material-icons prefix">star</i>
                        <label for="brandid" class="active"><span class="red-text">*</span></label>
                        <select id="brandid" name="brandId" required>
                                 <option value="0" selected>Choose Brand</option>
                                <c:if test="${not empty brandlist}">
							<c:forEach var="listValue" items="${brandlist}">
								<option value="<c:out value="${listValue.brandId}" />"><c:out
										value="${listValue.name}" /></option>
							</c:forEach>
						</c:if>
                        </select>
                    </div>
                    <div class="input-field col s12 m4 l4">
                        <i class="material-icons prefix">filter_list</i>
                         <label for="brandid" class="active"><span class="red-text">*</span></label>
                        <select id="categoryid" name="categoryId" required>
                                 <option value="0" selected>Choose Category</option>
                                <c:if test="${not empty categorylist}">
							<c:forEach var="listValue" items="${categorylist}">
								<option value="<c:out value="${listValue.categoryId}" />"><c:out value="${listValue.categoryName}" /></option>
							</c:forEach>
						</c:if>
                        </select>
                    </div>
					<div class="input-field col s12 m4 l4">
                        <i class="material-icons prefix">play_for_work</i>
                         <label for="brandid" class="active"><span class="red-text">*</span></label>
                        <select id="unitId" name="unitType" required>
                                 <option value="0" selected>Choose UnitType</option>
                                 <option value="kg" >kg</option>
                                 <option value="ml" >ml</option>
                                 <option value="gms" >gms</option>
                        </select>
                    </div>                   
                    
                     <div class="input-field col s12 m4 l4">
                        <i class="material-icons prefix">shopping_cart</i>
                        <input id="product" type="text" class="validate" name="productname" required>
                        <label for="product" class="active"><span class="red-text">*</span>Enter Product Name</label>
                    </div>
                    
                    <!-- <div class="input-field file-field  col s12 m4 l4 left-align ">
                        <div class="file-path-wrapper">
                            <i class="material-icons prefix" style="margin-left:-3%;">add_a_photo</i>
                            <input id="upload" type="file" name="file">
                            <input class="file-path validate" type="text" placeholder="Upload one or more files" required>
                        </div>
                    </div> -->
                    <div class="input-field col s12 m4 l4" id="threshold">
                        <i class="material-icons prefix">view_comfy</i>
                        <input id="thresholdValue" type="text" class="validate" name="thresholvalue" required>
                        <label for="thresholdValue" class="active"><span class="red-text">*</span>Enter Threshold Value</label>
                    </div>
                    
                    <div class="input-field col s12 m4 l4">
                        <i class="material-icons prefix"> rate_review</i>
                        <input id="code" type="text" class="validate" name="productcode">
                        <label for="code" class="active">Enter Product Code</label>
                    </div>
                    <div class="input-field col s12 m4 l4 offset-l4 offset-m4"  id="productrate">
                        <i class="fa fa-inr prefix"></i>
                        <input id="productRate" type="text" class="validate"  required>
                        <label for="productRate" class="active"><span class="red-text">*</span>Enter Product Rate</label>
                    </div>
                     <div class="input-field col s12 m4 l4" id="unitprice">
						 <i class="fa fa-inr prefix"></i>
                        <input id="unitRate" type="text" class="grey lighten-2"  name="productRate" readonly>
                        <label for="productRate" class="active">Product Unit Rate</label>                       
                    </div> 
                    <div class="input-field col s12 m3 l3" id="cgstDiv">
                        <i class="fa fa-inr prefix"></i>
                        <input id="cgst" type="text" class="grey lighten-2" readonly>
                        <label for="cgst" class="active">CGST Amount</label>
                    </div>
                     <div class="input-field col s12 m4 l4 offset-l1 offset-m1" id="sgstDiv">
                      <i class="fa fa-inr prefix"></i>
                        <input id="sgst" type="text" class="grey lighten-2" readonly>
                        <label for="sgst" class="active">SGST Amount</label>
                    </div>
                    <div class="input-field col s12 m4 l4" id="igstDiv">
                      <i class="fa fa-inr prefix"></i> 
                        <input id="igst" type="text" class="grey lighten-2"  readonly>
                        <label for="igst" class="active">IGST Amount</label>
                    </div>  
                    <br>
                  <!--  <div class="input-field col s12 m5 l5 push-l5 push-m5  " id="productrate">
                        <input id="productTax" type="text" name="productTax" readonly>
                        <label for="productTax" class="active">Product Tax</label>
                    </div>
                    <br>
                    <div class="input-field col s12 m5 l5 push-l5 push-m5  " id="productrate">
                        <input id="productRateWithTax" type="text" name="productRateWithTax" readonly>
                        <label for="productRateWithTax" class="active">Product Rate With Tax</label>
                    </div> -->
				
                    <div class="input-field col s12 m4 l4 offset-l4  offset-m4 center-align">
                        <button class="btn waves-effect waves-light blue-gradient" type="submit" id="saveProductSubmit">Add Product<i class="material-icons right">send</i> </button>
                        <br><br>
                    </div>

                </div>


            </form>
        </div>
        
       <!--Add Edit Confirm Modal Structure -->
			 <div class="row">
			<div class="col s12 m12 l12">
				<div id="addeditmsg" class="modal" style="width:40%;">
					<div class="modal-content">
						<h5 id="msgHead"></h5>
						<hr>
						<h6 id="msg"></h6>
					</div>
					<div class="col s8 m2 l2 offset-l5 modal-footer">
						<a href="#!" class="modal-action modal-close waves-effect waves-green btn blue-gradient">OK</a>
					</div>
				</div>
			</div>
		</div>

        
    </main>
    <!--content end-->
</body>

</html>