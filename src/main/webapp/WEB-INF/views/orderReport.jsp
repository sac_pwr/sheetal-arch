<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<html>

<head>
 <%@include file="components/header_imports.jsp" %>
    

<script type="text/javascript">
    
    $(document).ready(function() {
    	
    	
		var totalAmount=0;
    	
    	var table = $('#tblData').DataTable();	
    	$.fn.dataTable.ext.search.push(function( settings, data, dataIndex ) {
    		       	        
    		        var salesManId=data[1].trim();
    				var salesManId2=$('#salesManId').val();
    				//alert(salesManId+"-"+salesManId2);
    				if(salesManId2==="SalesMan")
    				{
    					totalAmount=parseFloat(totalAmount)+parseFloat(data[3].trim()+"0.55");
    					//alert("Area : "+totalAmount);
    					return true;	    					
    				}
    				
    		        if ( salesManId2 === salesManId )
    		        {
    		        	totalAmount=parseFloat(totalAmount)+parseFloat(data[3].trim()+"0.55");
    		        	//alert("other : "+totalAmount);
    		            return true;
    		        }
    		        return false;
    		        
    	});
    	
	$('#salesManId').change(function(){
			totalAmount=0;
    		table.draw();
    		$('#totalAmountId').text(totalAmount);
    	});
    	
    	
    });
    
    </script>
    <style>
      .card-image{
		width:40% !important;
		background-color:#01579b !important;
	}
    </style>
</head>

<body>
   <!--navbar start-->
   	<%@include file="components/navbar.jsp" %>
    <!--navbar end-->
    <!--content start-->
    <main>
       
      <div class="row">
      	<div class="col s12 l3 m3">
      		  <div class="card horizontal">
      		  	<div class="card-image">
       				  <h6 class="white-text" style="padding:8px">Total Amount : </h6>
      			</div>
      			<div class="card-stacked grey lighten-3">
      			  <div class="card-content" style="padding:8px">
       			<h6 class="">&#8377;<span id="totalAmountId"><c:out value="${totalAmount}" /></span></h6>
       			  </div>
        	    </div>
                
          	  </div>
           </div>  
             <div class="col s12 l3 m3">
             	 <div class="card horizontal">
      		  	<div class="card-image">
       				  <h6 class="white-text" style="padding:8px">No of Order : </h6>
      			</div>
      			<div class="card-stacked grey lighten-3">
      			  <div class="card-content" style="padding:8px">
       						<h6 class=""><c:out value="${noOfOrders}" /></h6>
       			  </div>
        	    </div>
                
          	  </div>
                
            </div>
            
			
			<c:if test="${currentPending=='pending'}"> 
	            <div class="col s12 l2 m2 right right-align">
	                <a href="${pageContext.servletContext.contextPath}/orderReport?currentPending=current" class="btn waves-effect waves-light  light-blue darken-4" style="margin-top:7%">Current order</a>
	            </div>
	            <div class="col s12 l2 m2 right right-align">
	                <a href="${pageContext.servletContext.contextPath}/orderReport?currentPending=future" class="btn waves-effect waves-light  light-blue darken-4" style="margin-top:7%">Future Orders</a>
	            </div>
            </c:if>
            <c:if test="${currentPending=='current'}">
            	<div class="col s12 l2 m2 right right-align">
	                <a href="${pageContext.servletContext.contextPath}/orderReport?currentPending=future" class="btn waves-effect waves-light  light-blue darken-4" style="margin-top:7%">Future order</a>
	            </div>
	            <div class="col s12 l2 m2 right right-align">
	                <a href="${pageContext.servletContext.contextPath}/orderReport?currentPending=pending" class="btn waves-effect waves-light  light-blue darken-4" style="margin-top:7%">Pending Order</a>
	            </div>
            </c:if>
	        <c:if test="${currentPending=='future'}">
            	<div class="col s12 l2 m2 right right-align">
	                <a href="${pageContext.servletContext.contextPath}/orderReport?currentPending=current" class="btn waves-effect waves-light  light-blue darken-4" style="margin-top:7%">Current order</a>
	            </div>
	            <div class="col s12 l2 m2 right right-align">
	                <a href="${pageContext.servletContext.contextPath}/orderReport?currentPending=pending" class="btn waves-effect waves-light  light-blue darken-4" style="margin-top:7%">Pending Order</a>
	            </div>
            </c:if>
			<%-- <div class="col s12 l2 m2" style="margin-top:10px;">
                <select id="areaIds">
			      <option value="0" selected>Choose Cluster</option>
                      <c:if test="${not empty clusterList}">
							<c:forEach var="listValue" items="${clusterList}">
								<option value="<c:out value="${listValue.name}" />"><c:out value="${listValue.name}" /></option>
							</c:forEach>
					  </c:if>
			    </select>
            </div>
 		 --%>
          	
      </div>
        <div class="row">
            <div class="col s12 l12 m12 ">
                <table class="striped highlight centered mdl-data-table display" id="tblData" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th class="print-col" rowspan="2">Sr.No</th>
                            <th class="print-col" rowspan="2">Order Id</th>
                            <th class="print-col" rowspan="2">Booth No</th>
                            <th class="print-col" rowspan="2">Owner Name</th>
                            <th class="print-col" rowspan="2">Route No</th>
                            <th class="print-col" rowspan="2">Salesman Name</th>
                            <th class="print-col" rowspan="2">Amount Of Business</th>
                            <th class="print-col" rowspan="2">Date of Order</th>
                            <th class="print-col" rowspan="2">Order Delivered date</th>                           
                            <th class="print-col" colspan="3">Status</th>                           
                          
                        </tr>
                        <tr>
                        	<th>Sales Man</th>
                        	<th>GateKeeper</th>
                        	<th>Delivery Boy</th>
                        </tr>
                    </thead> 
                    <tbody>
                    <c:if test="${not empty orderReportModelList}">
							<c:forEach var="listValue" items="${orderReportModelList}">
	                        <tr>
	                             <td><c:out value="${listValue.srno}"/></td>
	                            <td><a href="invoicebill.jsp"><c:out value="${listValue.orderId}"/></a></td>
	                            <td><c:out value="${listValue.boothNo}"/></td>
	                            <td><c:out value="${listValue.ownerName}"/></td>
	                            <td><a href="${pageContext.servletContext.contextPath}/getEmployeeView?employeeDetailsId=${listValue.employeeDetailsId}"><c:out value="${listValue.routeNo}"/></a></td>
	                            <td><a title="View Salesman Details" href="${pageContext.servletContext.contextPath}/getEmployeeView?employeeDetailsId=${listValue.employeeDetailsId}"><c:out value="${listValue.smName}"/></a></td>
	                            <td><c:out value="${listValue.amount}"/></td>
	                            <td><c:out value="${listValue.dateOfOrder}"/></td>
	                            <td><c:out value="${listValue.dateOfDelivery}"/></td>	                            	                              
	                            <td><c:out value="${listValue.orderStatusSM}" /></td>
	                            <td><c:out value="${listValue.orderStatusGK}" /></td>
	                            <td><c:out value="${listValue.orderStatusDB}" /></td>
	                         </tr>
                        </c:forEach>
                        </c:if>
                    </tbody>
                </table>
            </div>
        </div>
			<!-- Modal Structure for Salesman Details -->
        <div id="salesmanDetails" class="modal">
            <div class="modal-content">
                <h4 class="center">Salesman Details</h4>
                <hr>
                <table border="2" class="centered tblborder">
                    <thead>
                        <tr>
                            <th>Sr.No</th>
                            <th>Route No</th>
                            <th>Area Name</th>
                            <th>Cluster</th>
                            <th>Salesman Name</th>
                            <th>Mobile No</th>
                            <th>Area Sales Manager</th>
                            <th>ASM-Mobile No.</th>
                       </tr>
                    </thead>
                    <tbody id="areaList">
                        
                    </tbody>
                </table>
            </div>
            <div class="modal-footer row">
				<div class="col s6 m6 l7">
                <a href="#!" class="modal-action modal-close waves-effect btn ">Ok</a>
                </div>
            </div>
        </div>
			
 
				<div class="row">
			<div class="col s8 m2 l2">
				<div id="addeditmsg" class="modal" style="width:40%;">
					<div class="modal-content">
						<h5 id="msgHead">Error</h5>
						<hr>	
						<h6 id="msg"></h6>
					</div>
					<div class="col s8 m2 l2 offset-l5 modal-footer">
						<a href="#!"
							class="modal-action modal-close waves-effect btn blue-gradient">OK</a>
					</div>
				</div>
			</div>
		</div>
	

    </main>
    <!--content end-->
</body>

</html>