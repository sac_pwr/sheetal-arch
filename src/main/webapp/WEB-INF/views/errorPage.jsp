<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
	<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<html>

<head>
    <title>Blue Square</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <link rel="shortcut icon" href="resources/img/bs_icon2-01.png">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Compiled and minified JavaScript -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.98.2/css/materialize.min.css">
    <script src="https://use.fontawesome.com/cf75b05ed3.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.98.2/js/materialize.min.js"></script>
    <link href="resources/css/style.css" rel="stylesheet">
    <script type="text/javascript" src="resources/js/script.js"></script>
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <script>
        
    </script>
    <style>
        button,
        body {
            /*background-color: red;*/
            background-image: linear-gradient( to right, #311b92, #3498ce);
        }
    </style>

    <body class="blue">

        <div class="container" style="transform: translate(20px, 140px);">
           
                <div class="row">
                    <div class="col s12 m12 l8 offset-l2 z-depth-5 card-panel" style="border-radius:20px;opacity:0.8;">
                        <div class="black-text">
                          <!--   <div class="col s12 m12 l12">
                            <h2 class="card-title center teal-text" style="font-family: Serif;">Error:404</h2>
                        </div>--> 
                            <div class="col l6 m6">
                                <div class="col s6 m7 l12">
                                    <img src="resources/img/oops3.jpg" class="responsive-img" style="margin-top:9%;">
                                   
                                </div>
                            </div>
                            <div class="col l6 m6">
                                <br>
                                 <!--<h2 class="center">404</h2>-->
                                 <div class="col s12 m12 l12">
                                    <h6>${url}</h6>
									<h2 class="card-title center teal-text" style="font-family: Serif;">Error : </h2>
									<span class="justify">${error}</span>
<br><br><!--${error}  --></div>
                                <a class="btn waves-effect waves-light blue darken-8 " href="${pageContext.request.contextPath}/">
								Go to home page&nbsp; <i class="material-icons right">send</i>
							</a>
<br><br>
                                </div>
                                
                            </div>
                            
                        </div>
                    </div>
                </div>
           
        </div>
    </body>

</html>