<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<html>

<head>
  <%@include file="components/header_imports.jsp" %>
    <script type="text/javascript">
	    
	    $(document).ready(function() {
            $(".showQuantity").hide();
            $(".showDates").hide();
            $("#oneDateDiv").hide();
            $(".topProduct").click(function() {
                $(".showQuantity").show();
                $(".showDates").hide();
                $("#oneDateDiv").hide();
            });
           
            $(".rangeSelect").click(function() {
            	$("#oneDateDiv").hide();	
                $(".showDates").show();
                $(".showQuantity").hide();
            });
           
        	$(".pickdate").click(function(){
        		 $(".showQuantity").hide();
        		 $(".showDates").hide();
        		$("#oneDateDiv").show();
        	});
        	
        	
        	var table = $('#tblData').DataTable();	
        	$.fn.dataTable.ext.search.push(function( settings, data, dataIndex ) {
        		       	        
        		        var salesManId=data[5].trim();
        				var salesManId2=$('#clusterIds').val();
        				if(salesManId2==="0")
        				{
        					totalAmount=parseFloat(totalAmount)+parseFloat(data[7].trim());
        					return true;
        				}
        				else if ( salesManId2 === salesManId )
        		        {
        					totalAmount=parseFloat(totalAmount)+parseFloat(data[7].trim());
        		            return true;
        		        }
        		        return false;
        		        
        	});
        	
    	    $('#clusterIds').change(function(){
    	    	totalAmount=0;
        		table.draw();
        		$('#totalAmountId').text(totalAmount);
        	});
        	
        });

    </script>
    <style>
    	.card-image{
		width:40% !important;
		background-color:#01579b !important;
	}
    
    
    </style>
</head>

<body>
    <!--navbar start-->
   	<%@include file="components/navbar.jsp" %>
    <!--navbar end-->
    <!--content start-->
    <main>
     
        <div class="row">
        	 <div class="col s12 l3 m3">
      		  <div class="card horizontal">
      		  	<div class="card-image">
       				  <h6 class="white-text" style="padding:8px">Total Amount:</h6>
      			</div>
      			<div class="card-stacked grey lighten-3">
      			  <div class="card-content" style="padding:8px">
       			<h6 class="">&#8377;<span id="totalAmountId"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${totalBoothAmt}" /></span></h6>
       			  </div>
        	    </div>
                
          	  </div>
           </div>  
        	     <div class="col s12 m4 l4 right" style="margin-top:1%;">
                <div class="col s6 m4 l4 right">
                    <!-- Dropdown Trigger -->
                    <a class='dropdown-button btn waves-effect waves-light grey-text  text-lighten-3 light-blue darken-4' href='#' data-activates='filter'>Filter<i
                class="material-icons right">arrow_drop_down</i></a>
                    <!-- Dropdown Structure -->
                    <ul id='filter' class='dropdown-content'>
                    	<li><a href="${pageContext.request.contextPath}/customerReportList?range=yesterday">Yesterday</a></li>
                        <li><a href="${pageContext.request.contextPath}/customerReportList?range=last7days">Last 7 Days</a></li>
                        <li><a href="${pageContext.request.contextPath}/customerReportList?range=last1month">Last 1 Month</a></li>
                        <li><a href="${pageContext.request.contextPath}/customerReportList?range=last3months">Last 3 Month</a></li>
                        <li><a class="rangeSelect">Range</a></li>
                        <li><a class="pickdate">Pick date</a></li>                        
                        <li><a href="${pageContext.request.contextPath}/customerReportList?range=viewAll">View All</a></li>
                    </ul>
                </div>
                	<div class="col s12 l8	m8 right"> 
                 <form action="${pageContext.request.contextPath}/customerReportList" method="post">
                    <input type="hidden" name="range" value="range">
                     <span class="showDates">
                     			 <div class="input-field col s6 m2 l2 right">
                            <button type="submit">View</button>
                            </div>
                             
                              <div class="input-field col s6 m5 l5 right">
                                    <input type="date" class="datepicker" placeholder="Choose Date" name="endDate" id="endDate">
                                     <label for="endDate">To</label>
                               </div>
                                <div class="input-field col s6 m5 l5 right">
                                <input type="date" class="datepicker" placeholder="Choose Date" name="startDate" id="startDate" required> 
                                <label for="startDate">From</label>
                                 </div>
                                 
                               
                          </span>
                </form>
                 </div>
				<div class="col s12 l6	m6 right" id="oneDateDiv">                            	
                    <form action="${pageContext.request.contextPath}/customerReportList" method="post">
                    <input type="hidden" name="range" value="pickdate">
	                    <div class="input-field col s12 m10 l10">
		                    <input type="text" id="oneDate" class="datepicker" placeholder="Choose date" name="startDate">
		                    <label for="oneDate" class="black-text">Pick Date</label>
	                    </div>
	                    <div class="input-field col s12 m2 l2">
	                     <button type="submit">View</button>
                       
                        </div>
                    </form>
               </div>
               
               
                
            </div>
            	 <div class="input-field col s6 m2 l2 offset-l1 offset-m1">
                   
                         <select id="clusterIds">
						      <option value="0" selected>Choose Cluster</option>
			                      <c:if test="${not empty clusterList}">
										<c:forEach var="listValue" items="${clusterList}">
											<option value="<c:out value="${listValue.name}" />"><c:out value="${listValue.name}" /></option>
										</c:forEach>
								  </c:if>
						    </select>
                   
                    </div>  
           
                             
                 </div>
        	
 
        <div class="row">
            <div class="col s12 l12 m12 ">
                <table class="striped highlight bordered centered " id="tblData" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th class="print-col">Sr.No</th>
                            <th class="print-col">Booth Number</th>
                            <th class="print-col">Owner Name</th>
                            <th class="print-col">Route No</th>
                            <th class="print-col">Area</th>
                            <th class="print-col">Cluster</th>
                            <th class="print-col">Total No of order</th>
                            <th class="print-col">Total purchase</th>
                            <th class="print-col">Amount Paid</th>
                            <th class="print-col">Balance Amount</th>
                        </tr>
                    </thead>

                    <tbody>
                    
                    <c:if test="${not empty customerRerportModelList}">
					<c:forEach var="listValue" items="${customerRerportModelList}">
                        <tr>
                            <td><c:out value="${listValue.srno}" /></td>
                            <td><a href="${pageContext.request.contextPath}/customerReportByBoothId?boothId=${listValue.booth.boothId}&range=today"><c:out value="${listValue.booth.boothNo}" /></a></td>
                            <td><a href="${pageContext.request.contextPath}/customerReportByBoothId?boothId=${listValue.booth.boothId}&range=today"><c:out value="${listValue.booth.ownerName}" /></a></td>
                            <td><c:out value="${listValue.booth.route.routeNo}" /></td>
                            <td><c:out value="${listValue.booth.route.area.name}" /></td>
                            <td><c:out value="${listValue.booth.route.area.cluster.name}" /></td>
                            <td><c:out value="${listValue.noOfOrders}" /></td>
                            <td><c:out value="${listValue.totalAmount}" /></td>
                           	<td><c:out value="${listValue.totalAmountPaid}" /></td>
                            <td><c:out value="${listValue.totalAmountUnPaid}" /></td>                           
                        </tr>
                     </c:forEach>
                     </c:if>
                     
                    </tbody>
                </table>
            </div>
        </div>

 		<div class="row">
			<div class="col s8 m2 l2">
				<div id="addeditmsg" class="modal" style="width:40%;">
					<div class="modal-content">
						<h5 id="msgHead">Error</h5>
						<hr>	
						<h6 id="msg"></h6>
					</div>
					<div class="col s8 m2 l2 offset-l5 modal-footer">
						<a href="#!"
							class="modal-action modal-close waves-effect btn grey-text  text-lighten-3 light-blue darken-4">OK</a>
					</div>
				</div>
			</div>
		</div>
    </main>
    <!--content end-->
</body>

</html>