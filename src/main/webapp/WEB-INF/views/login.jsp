<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<html>

<head>
    <title>Blue Square</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <link rel="shortcut icon" href="resources/img/bs_icon2-01.png">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Compiled and minified JavaScript -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.98.2/css/materialize.min.css">
    <script src="https://use.fontawesome.com/cf75b05ed3.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.98.2/js/materialize.min.js"></script>
    <link href="resources/css/style.css" rel="stylesheet">
    <script type="text/javascript" src="resources/js/script.js"></script>
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <script>
        $(document).ready(function() {
            $(".eye-slash").hide();
            $(".eye").click(function() {
                $(".eye").hide();
                $(".eye-slash").show();
                $("#password").attr("type", "text");
            });
            $(".eye-slash").click(function() {
                $(".eye-slash").hide();
                $(".eye").show();
                $("#password").attr("type", "password");
            });
            // $(".eye-slash1").hide();
            // $(".eye1").click(function() {
            //     $(".eye1").hide();
            //     $(".eye-slash1").show();
            //     $("#Cnfrmpassword").attr("type", "text");
            // });
            // $(".eye-slash1").click(function() {
            //     $(".eye-slash1").hide();
            //     $(".eye1").show();
            //     $("#Cnfrmpassword").attr("type", "password");
            // });
            
            var msg="${validateMsg}";
			 //alert(msg);
			 if(msg!='' && msg!=undefined)
			 {
				 $('#loginError').html('<font color="red" size="4">'+msg+'</font>');
			 }
            
            $('#loginValidateButton').click(function(){
            	
            	var user=$('#userId').val().trim();
            	var pass=$('#password').val().trim();
            	
            	if(user==="" && pass==="")
            	{
            		$('#loginError').html('<font color="red" size="4">Fill User Name and Password</font>');
            		return false;
            	}
            	else if(user==="")
            	{
            		$('#loginError').html('<font color="red" size="4">Fill User Name</font>');
            		return false;
            	}
            	else if(pass==="")
            	{
            		$('#loginError').html('<font color="red" size="4">Fill Password</font>');
            		return false;
            	}
            	$('#loginError').html('');
          	 	/*$('#loginError').html('<font color="red" size="4"><b>Wait..</b></font>');
        		$.ajax({
        			type:"GET",
					url : "${pageContext.request.contextPath}/loginAdmin?userId="+user+"&password="+pass,
					dataType : "json",
					success : function(data) {
						//alert(data.status);
						if(data.status==="Success")
						{
							$('#loginError').html('<font color="green" size="4"><b>Login SuccessFully</b></font>');
							window.location.href="${pageContext.request.contextPath}/";
							//location.reload();
						}
						else
						{
							$('#loginError').html('<font color="red" size="4">Login failed</font>');
						}
					}
				});	 */
            });
            
        });
    </script>
    <style>
        button,
        body {
            /*background-color: red;*/
            background-image: linear-gradient( to right, #311b92, #3498ce);
        }
    </style>

    <body class="blue">

        <div class="container" style="transform: translate(20px, 140px);">
            <form action="${pageContext.request.contextPath}/loginAdmin" method="post">
                <div class="row">
                    <div class="col s12 m12 l8 offset-l2 z-depth-5 card-panel" style="border-radius:20px;">
                        <div class="card-content black-text">
                            <!-- <div class="col s12 m12 l12">
                            <h2 class="card-title center teal-text" style="font-family: Serif;">Login</h2>
                        </div> -->
                            <div class="col l6 m6">
                                <div class="col s6 m7 l12">
                                    <!-- <img src="resources/img/bluelogo.jpg" class="responsive-img" style="margin-top:9%;border-right:2px solid teal;"> -->
                                    <h4 class="card-title center blue-text" style="font-family: Serif;">SHEETAL ARCH</h4>
                                </div>
                            </div> 
                            <div class="col l6 m6">
                                <br>
                                <!--   <h5 class="center">Login</h5>-->
                             
                                <div class="row">
                                    <div class="input-field col s12 l11 m12">
                                        <i class="material-icons prefix">account_circle</i> <input id="userId" name="userId" type="text" required class="validate" data-error="wrong" data-success="right">
                                        <label for="userId">User Name</label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="input-field col s10 l11 m10">
                                        <i class="material-icons prefix">lock</i> <input id="password" name="password" type="password" class="validate" required>
                                        <label for="password">Password</label>
                                    </div>
                                    <div class="col s2 m2 l1" style="transform: translate(-45px, 30px);">
                                        <i class="fa fa-eye fa-lg eye" aria-hidden="true"></i>
                                        <i class="fa fa-eye-slash fa-lg eye-slash" aria-hidden="true"></i>
                                    </div>
                                   	<div class="input-field col s10 l11 m10 center">                                   
                                   		<span id="loginError">
                                   		
                                   		</span>
                                   	</div>
                                   
                                </div>
                                <div class="input-field col s12 l12 m12 center">
                                    <button class="btn waves-effect waves-light blue darken-8 " type="submit" id="loginValidateButton" value="Login">
								Login&nbsp; <i class="material-icons right">send</i>
							</button>
                                </div>
                            </div>
                            <div class="row"></div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </body>

</html>