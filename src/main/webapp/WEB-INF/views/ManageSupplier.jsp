<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%> 
<html>

<head>
    <%@include file="components/header_imports.jsp" %>
<script type="text/javascript">
var checkedId=[];
   $(document).ready(
			function() {
				var msg="${saveMsg}";
				// alert(msg);
				 if(msg!='' && msg!=undefined)
				 {
				     $('#addeditmsg').modal('open');
				     $('#msgHead').text("Product Message");
				     $('#msg').text(msg);
				 }
				 
				 $("#checkAll").click(function () {
						var colcount=$('#tblData').DataTable().columns().header().length;
					    var cells = $('#tblData').DataTable().column(colcount-1).nodes(), // Cells from 1st column
					        state = this.checked;
							//alert(state);
					    for (var i = 0; i < cells.length; i += 1) {
					        cells[i].querySelector("input[type='checkbox']").checked = state;
					    }		                
					    
					});
					
					 $("input:checkbox").change(function(a){
						 
						 var colcount=$('#tblData').DataTable().columns().header().length;
					     var cells = $('#tblData').DataTable().column(colcount-1).nodes(), // Cells from 1st column
					         
					      state = false; 				
					     var ch=false;
					     
					     for (var i = 0; i < cells.length; i += 1) {
					    	 //alert(cells[i].querySelector("input[type='checkbox']").checked);
					    	
					         if(cells[i].querySelector("input[type='checkbox']").checked == state)
					         { 
					        	 $("#checkAll").prop('checked', false);
					        	 ch=true;
					         }                      
					     }
					     if(ch==false)
					     {
					    	 $("#checkAll").prop('checked', true);
					     }
					    	
					    //alert($('#tblData').DataTable().rows().count());
					});
				 
				 $('#sendsmsid').click(function(){
						var count=parseInt('${count}')
						checkedId=[];
						//chb
						 var idarray = $("#employeeTblData")
			          .find("input[type=checkbox]") 
			          .map(function() { return this.id; }) 
			          .get();
						 var j=0;
						 for(var i=0; i<idarray.length; i++)
						 {								  
							 idarray[i]=idarray[i].replace('chb','');
							 if($('#chb'+idarray[i]).is(':checked'))
							 {
								 checkedId[j]=idarray[i];
								 j++;
							 }
						 }
						
						 
						 if(checkedId.length==0)
						 {
							 $('#addeditmsg').modal('open');
						     $('#msgHead').text("Message : ");
						     $('#msg').text("Select Supplier For Send SMS");
						 }
						 else
						 {
							 $('#smsText').val('');
							 $('#smsSendMesssage').html('');
							 $('#sendMsg').modal('open');
						 }
					});
					
					$('#sendMessageId').click(function(){
						
						
						 $('#smsSendMesssage').html('');
						
						 var smsText=$("#smsText").val();
							if(checkedId.length==0)
							{
								$('#smsSendMesssage').html("<font color='red'>Select Employee For Send SMS</font>");
								return false;
							}
							if(smsText==="")
							{
								$('#smsSendMesssage').html("<font color='red'>Enter SMS Text</font>");
								return false;
							}
						
						var form = $('#sendSMSForm');
						//alert(form.serialize()+"&employeeDetailsId="+checkedId);
						
						$.ajax({
								type : form.attr('method'),
								url : form.attr('action'),
								data : form.serialize()+"&supplierIds="+checkedId,
								//async: false,
								success : function(data) {
									if(data==="Success")
									{
										$('#smsSendMesssage').html("<font color='green'>SMS send SuccessFully</font>");
										$('#smsText').val('');
									}
									else
									{
										$('#smsSendMesssage').html("<font color='red'>SMS sending Failed</font>");
									}
								}
						});
					});
			});
   
			function viewProducts(url)
			   {
				   $.ajax({
						url : url,
						dataType : "json",
						success : function(data) {
						//alert(data);
						    $("#tbproductlist").empty();
							var srno=1;
							for (var i = 0, len = data.length; i < len; ++i) {
								var supplierproduct = data[i];
								$("#tbproductlist").append("<tr>"+
			                           "<td>"+srno+"</td>"+
			                           "<td>"+supplierproduct.product.productName+"</td>"+
			                           "<td>"+supplierproduct.product.categories.categoryName+"</td>"+
			                           "<td>"+supplierproduct.product.brand.name+"</td>"+
			                           "<td>"+supplierproduct.supplierRate+"</td>"+
			                       "</tr>"); 
								srno++;
							}
								    	
							
							$('.modal').modal();
							$('#viewDetails').modal('open');
							//alert("data came");
							return false;
							/* for (index = 0; index < options.length; ++index) {
							  option = options[index];
							  select.options.add(new Option(option.name, option.cityId));
							} */
						},
						error: function(xhr, status, error) {
							  //var err = eval("(" + xhr.responseText + ")");
							  //alert(error +"---"+ xhr+"---"+status);
							  	 $('#addeditmsg').modal('open');
							     $('#msgHead').text("Manage Supplier Message");
							     $('#msg').text("Not Have Product List");
							}
					});
			   }
   </script>
	
</head>

<body>
    <!--navbar start-->
   	<%@include file="components/navbar.jsp" %>
    <!--navbar end-->
    <!--content start-->
    <main>
     
        <!--Add New Supplier-->
       <div class="col s10 l3 m6 right offset-s2">
            <a class="btn waves-effect waves-light blue-gradient" href="${pageContext.request.contextPath}/addSupplier"><i class="material-icons left" >add</i>Add Supplier</a>
        </div>


        <div class="col s12 l8 m12">
            <table class="striped highlight centered mdl-data-table display  select" id="tblData" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th class="print-col">Sr.No.</th>
                        <th class="print-col">Name</th>
                        <th class="print-col">Product Details</th>
                        <th class="print-col">Mobile No.</th>
                        <th class="print-col">Email</th>
                        <th class="print-col">Address</th>
                        <th class="print-col">GST IN</th>
                        <th class="print-col">Balance Payment</th>
                        <th class="print-col">Added Date</th>
                        <th class="print-col">Updated Date</th>
                        <th>Edit</th>
                        <!-- <th>Delete</th> -->
                        <th> <a href="#" class="modal-trigger" id="sendsmsid">SMS</a><br>
                            <input type="checkbox" class="filled-in checkAll" id="checkAll"/>
                            <label for="checkAll" style="padding-left: 20px;"></label>
                        </th>
                    </tr>
                </thead>

                <tbody id="employeeTblData">
                	<c:if test="${not empty fetchSupplierList}">
							<c:forEach var="listValue" items="${fetchSupplierList}">
                    	<tr>
                        <td><c:out value="${listValue.srNo}" /></td>
                        <td><c:out value="${listValue.name}" /></td>
                        <td>
                            <button id="getProductList" class=" btn blue-gradient" onclick="viewProducts('${pageContext.servletContext.contextPath}/fetchSupplierProductList?supplierId=${listValue.supplierId}')" class="modal-trigger">View</button>
                           <%--  <input id="supplierId_${listValue.srNo}" type="hidden" value="<c:out value="${listValue.supplierId}" />"> --%>
                        </td>

                        <td><c:out value="${listValue.mobileNumber}" /></td>
                        <td><c:out value="${listValue.emailId}" /></td>
                        <td><c:out value="${listValue.address}" /></td>
                        <td><c:out value="${listValue.gstinNo}" /></td>
                        
                        <td><c:out value="${listValue.balAmt}" /></td>
                        
                        <td>
                        		<fmt:formatDate pattern = "yyyy-MM-dd" var="date"   value = "${listValue.addedDate}"  />
		                        <c:out value="${date}" />
                        </td>
                        <td>
                      		<fmt:formatDate pattern = "yyyy-MM-dd" var="date"   value = "${listValue.updatedDate}"  />
                        	<c:out value="${date}" />
                        </td>
                        <td><a class=" btn-flat " href="${pageContext.servletContext.contextPath}/fechSupplier?supplierId=${listValue.supplierId}"><i class="material-icons tooltipped blue-text " data-position="right" data-delay="50" data-tooltip="Edit" >edit</i></a></td>
                        <!-- Modal Trigger -->
                        <!-- <td><a href="#delete" class="modal-trigger"><i class="material-icons tooltipped" data-position="right" data-delay="50" data-tooltip="Delete">delete</i></a></td> -->
                        <td>

                            <input type="checkbox" class="filled-in" id="chb${listValue.supplierId}" />
                            <label for="chb${listValue.supplierId}" style="padding-left: 20px;"></label>

                        </td>
                    </tr>
                   </c:forEach>
					</c:if>



                    <!-- Modal Structure for delete -->

                    <!-- <div id="delete" class="modal">
                        <div class="modal-content">
                            <h4>Confirmation</h4>
                            <p>Are you sure you want to delete?</p>
                        </div>
                        <div class="modal-footer">
                            <a href="#!" class="modal-action modal-close waves-effect  btn-flat ">Delete</a>
                            <a href="#!" class="modal-action modal-close waves-effect btn-flat ">Close</a>
                        </div>
                    </div> -->
                    <!-- Modal Structure for sendMsg -->

               <div id="sendMsg" class="modal modal-fixed-footer row" style="width:40%;">
                    <form id="sendSMSForm" action="${pageContext.request.contextPath}/sendSMSTOSupplier" method="post">
                        <div class="modal-content">
                            <h4 class="center">Message</h4>
                            <hr>	
                           
                                <div class="col s12 l7 m7 push-l3">
                                    <label for="smsText" class="black-text">Type Message</label>
                                    <textarea id="smsText" class="materialize-textarea" name="smsText"></textarea>
                                </div> 
                                <div class="input-field col s12 l7 push-l4 m5 push-m4 ">                                
                   			 		 <font color='red'><span id="smsSendMesssage"></span></font>
            					</div>                            
                         
                               				
                        </div>
                        <div class="modal-footer center-align">
                            <div class="col s6 m6 l6">
                            		 <button type="button" id="sendMessageId" class="modal-action waves-effect btn btn-waves  ">Send</button>
                             </div>
                          	<div class="col s6 m6 l4">	
                           			 <a href="#!" class="modal-action modal-close waves-effect  btn red ">Cancel</a>
                             </div>
                            
                          
                        </div>
                        </form>
                    </div>
                </tbody>
            </table>
        </div>
        <!-- Modal Structure for View Product Details -->

        <div id="viewDetails" class="row modal modal-fixed-footer" style="width:40%;">
            <div class="modal-content">
                <h4 class="center">Product Details</h4>
                <hr>			
               
                	
                   <table border="2" class="centered tblborder">
                    <thead>
                        <tr>
                            <th>Sr.No</th>
                            <th>Product Name</th>
                            <th>Category</th>
                            <th>Brand</th>
                            <th>Rate</th>
                        </tr>
                    </thead>
                    <tbody id="tbproductlist">
                       <!--  <tr>
                            <td>1</td>
                            <td>abc</td>
                            <td>abc</td>
                            <td>abc</td>
                        </tr> -->
                    </tbody>
                </table>
            </div>
          
            <div class="modal-footer">
			
				<div class="col s12 m6 l5 offset-l2">
						 <a href="#!" class="modal-action modal-close waves-effect center btn">Close</a>
				</div>
				
				
			</div>			
               
            </div>
       
        
	<div class="row">
			<div class="col s8 m2 l2">
				<div id="addeditmsg" class="modal" style="width:40%;">
					<div class="modal-content">
						<h5 id="msgHead">Error</h5>
						<hr>	
						<h6 id="msg"></h6>
					</div>
					<div class="col s8 m2 l2 offset-l5 modal-footer">
						<a href="#!"
							class="modal-action modal-close waves-effect btn blue-gradient">OK</a>
					</div>
				</div>
			</div>
		</div>
    </main>
    <!--content end-->
</body>

</html>