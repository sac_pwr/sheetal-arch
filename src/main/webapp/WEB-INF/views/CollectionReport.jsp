<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<html>

<head>
    <%@include file="components/header_imports.jsp" %>
    <style>
        .carousel .carousel-item {
            width: 100%;
        }
        
        label {
            display: block !important;
        }
        
        #tblData_length.dataTables_length,
        {
            width: 9% !important;
            margin-top: 30px !important;
            /*margin-left: 1% !important;*/
        }
        
        /* #tblFullPayment_length.dataTables_length select,
        #tblPartialPayment_length.dataTables_length select,
        #tblPendingPayment_length.dataTables_length select {
            position: relative;
            cursor: pointer;
            background-color: transparent;
            border: none;
            border-bottom: 1px solid #9e9e9e;
            outline: none;
            height: 1.5rem;
            line-height: 1rem;
            width: 100%;
            font-size: 1rem;
            margin: 0 0 0 0;
            padding: 0;
            display: block;
        } */
        .modal{
        	width:40% !important;
        }
        .card-image{
        	width:45% !important;
        	background-color:#01579b !important;
        }
        div.dt-button-collection.fixed {
          position: fixed;
          top: 69% !important;
          left: 63% !important;
          margin-left: -75px; 
          border-radius: 0;
       }
    </style>

    <script>
    var checkedId=[];
    var checkedIdPending=[];
        $(document).ready(function() {

            //  for no of entries and global search
            $('#tblFullPayment').DataTable({
                "oLanguage": {
                    "sLengthMenu": "Display Records _MENU_"
                },
                lengthMenu: [
                    [10, 25., 50, -1],
                    ['10 ', '25 ', '50 ', 'all']
                ],
                autoWidth: false,
                
                scrollX: true,
                // scrollX: true,
                dom: 'lBfrtip',
                buttons: {
                    buttons: [
                        //      {
                        //      extend: 'pageLength',
                        //      className: 'pageLengthButton waves-effect waves-light   white-text blue-grey lighten-1'
                        //  }, 
                        {
                            extend: 'pdf',
                            className: 'pdfButton waves-effect waves-light grey lighten-3 light-blue-text text-darken-4 z-depth-2',
                            text: '<i class="fa fa-file-pdf-o"></i> &nbsp;<span style="font-size:15px;">PDF<span>',
                            //title of the page
                            title: 'Full Payment Report',
                            //file name 
                            filename: function() {
                                var d = new Date();
                                var date = d.getDate();
                                var month = d.getMonth();
                                var year = d.getFullYear();
                                var name = "fullpaymentReport";
                                return name + date + '-' + month + '-' + year;
                            },
                            //  exports only dataColumn
                            exportOptions: {
                                columns: ':visible.print-col'
                            },
                        }, {
                            extend: 'excel',
                            className: 'excelButton waves-effect waves-light  grey lighten-3 light-blue-text text-darken-4 z-depth-2',
                            text: '<i class="fa fa-file-excel-o  fa-fw"></i> &nbsp;<span style="font-size:15px;">EXCEL<span>',
                            //title of the page
                            title: 'Full Payment Report',
                            //file name 
                            filename: function() {
                                var d = new Date();
                                var date = d.getDate();
                                var month = d.getMonth();
                                var year = d.getFullYear();
                                var name = "fullpaymentReport";
                                return name + date + '-' + month + '-' + year;
                            },
                            //  exports only dataColumn
                            exportOptions: {
                                columns: ':visible.print-col'
                            },
                        }, {
                            extend: 'print',
                            className: 'printButton waves-effect waves-light grey lighten-3 light-blue-text text-darken-4 z-depth-2',
                            text: '<i class="fa fa-print fa-fw"></i> &nbsp;<span style="font-size:15px;">PRINT<span>',
                            //title of the page
                            title: 'Full Payment Report',
                            //file name 
                            filename: function() {
                                var d = new Date();
                                var date = d.getDate();
                                var month = d.getMonth();
                                var year = d.getFullYear();
                                var name = "fullpaymentReport";
                                return name + date + '-' + month + '-' + year;
                            },
                            //  exports only dataColumn
                            exportOptions: {
                                columns: ':visible.print-col'
                            },
                        }, {
                            extend: 'colvis',
                            className: 'colvisButton waves-effect waves-light grey lighten-3 light-blue-text text-darken-4 z-depth-2',
                            text: '<span style="font-size:15px;">COLUMN VISISBILITY<span>&nbsp;&nbsp;<i class="fa fa-caret-down fa-fw"></i> ',
                            collectionLayout: 'fixed two-column',
                            align: 'left'
                        },
                    ]
                }
            });
            $('#tblPartialPayment').DataTable({
                "oLanguage": {
                    "sLengthMenu": "Display Records _MENU_"
                },
                lengthMenu: [
                    [10, 25., 50, -1],
                    ['10 ', '25 ', '50 ', 'all']
                ],
                autoWidth: false,
                
                scrollX: true,
                // scrollX: true,
                dom: 'lBfrtip',
                buttons: {
                    buttons: [
                        //      {
                        //      extend: 'pageLength',
                        //      className: 'pageLengthButton waves-effect waves-light   white-text blue-grey lighten-1'
                        //  }, 
                        {
                            extend: 'pdf',
                            className: 'pdfButton waves-effect waves-light   grey lighten-3 light-blue-text text-darken-4 z-depth-2',
                            text: '<i class="fa fa-file-pdf-o"></i> &nbsp;<span style="font-size:15px;">PDF<span>',
                            //title of the page
                            title:'Partial Payment Report',
                           
                            //file name 
                            filename: function() {
                                var d = new Date();
                                var date = d.getDate();
                                var month = d.getMonth();
                                var year = d.getFullYear();
                                var name = "partialpaymentReport";
                                return name + date + '-' + month + '-' + year;
                            },
                            //  exports only dataColumn
                            exportOptions: {
                                columns: ':visible.print-col'
                            },
                        }, {
                            extend: 'excel',
                            className: 'excelButton waves-effect waves-light   grey lighten-3 light-blue-text text-darken-4 z-depth-2',
                            text: '<i class="fa fa-file-excel-o  fa-fw"></i> &nbsp;<span style="font-size:15px;">EXCEL<span>',
                            //title of the page
                            title: 'Partial Payment Report',
                            //file name 
                            filename: function() {
                                var d = new Date();
                                var date = d.getDate();
                                var month = d.getMonth();
                                var year = d.getFullYear();
                                var name = "partialpaymentReport";
                                return name + date + '-' + month + '-' + year;
                            },
                            //  exports only dataColumn
                            exportOptions: {
                                columns: ':visible.print-col'
                            },
                        }, {
                            extend: 'print',
                            className: 'printButton waves-effect waves-light   grey lighten-3 light-blue-text text-darken-4 z-depth-2',
                            text: '<i class="fa fa-print fa-fw"></i> &nbsp;<span style="font-size:15px;">PRINT<span>',
                            //title of the page
                            title:'Partial Payment Report',
                            //file name 
                            filename: function() {
                                var d = new Date();
                                var date = d.getDate();
                                var month = d.getMonth();
                                var year = d.getFullYear();
                                var name = "partialpaymentReport";
                                return name + date + '-' + month + '-' + year;
                            },
                            //  exports only dataColumn
                            exportOptions: {
                                columns: ':visible.print-col'
                            },
                        }, {
                            extend: 'colvis',
                            className: 'colvisButton waves-effect waves-light  grey lighten-3 light-blue-text text-darken-4 z-depth-2',
                            text: '<span style="font-size:15px;">COLUMN VISISBILITY<span>&nbsp;&nbsp;<i class="fa fa-caret-down fa-fw"></i> ',
                            collectionLayout: 'fixed two-column',
                            align: 'left'
                        },
                    ]
                }

            });

            $('#tblPendingPayment').DataTable({
                "oLanguage": {
                    "sLengthMenu": "Display Records _MENU_"
                },
                lengthMenu: [
                    [10, 25., 50, -1],
                    ['10 ', '25 ', '50 ', 'all']
                ],
                autoWidth: false,
                
                scrollX: true,
                // scrollX: true,
                dom: 'lBfrtip',
                buttons: {
                    buttons: [
                        //      {
                        //      extend: 'pageLength',
                        //      className: 'pageLengthButton waves-effect waves-light   white-text blue-grey lighten-1'
                        //  }, 
                        {
                            extend: 'pdf',
                            className: 'pdfButton waves-effect waves-light  grey lighten-3 light-blue-text text-darken-4 z-depth-2',
                            text: '<i class="fa fa-file-pdf-o"></i> &nbsp;<span style="font-size:15px;">PDF<span>',
                            //title of the page
                            title: 'Pending Payment Report',
                            //file name 
                            filename: function() {
                                var d = new Date();
                                var date = d.getDate();
                                var month = d.getMonth();
                                var year = d.getFullYear();
                                var name = "pendingpaymentReport";
                                return name + date + '-' + month + '-' + year;
                            },
                            //  exports only dataColumn
                            exportOptions: {
                                columns: ':visible.print-col'
                            },
                        }, {
                            extend: 'excel',
                            className: 'excelButton waves-effect waves-light  grey lighten-3 light-blue-text text-darken-4 z-depth-2',
                            text: '<i class="fa fa-file-excel-o  fa-fw"></i> &nbsp;<span style="font-size:15px;">EXCEL<span>',
                            //title of the page
                            title: 'Pending Payment Report',
                            //file name 
                            filename: function() {
                                var d = new Date();
                                var date = d.getDate();
                                var month = d.getMonth();
                                var year = d.getFullYear();
                                var name = "pendingpaymentReport";
                                return name + date + '-' + month + '-' + year;
                            },
                            //  exports only dataColumn
                            exportOptions: {
                                columns: ':visible.print-col'
                            },
                        }, {
                            extend: 'print',
                            className: 'printButton waves-effect waves-light grey lighten-3 light-blue-text text-darken-4 z-depth-2',
                            text: '<i class="fa fa-print fa-fw"></i> &nbsp;<span style="font-size:15px;">PRINT<span>',
                            //title of the page
                            title: 'Pending Payment Report',
                            //file name 
                            filename: function() {
                                var d = new Date();
                                var date = d.getDate();
                                var month = d.getMonth();
                                var year = d.getFullYear();
                                var name = "pendingpaymentReport";
                                return name + date + '-' + month + '-' + year;
                            },
                            //  exports only dataColumn
                            exportOptions: {
                                columns: ':visible.print-col'
                            },
                        }, {
                            extend: 'colvis',
                            className: 'colvisButton waves-effect waves-light  grey lighten-3 light-blue-text text-darken-4 z-depth-2',
                            text: '<span style="font-size:15px;">COLUMN VISISBILITY<span>&nbsp;&nbsp;<i class="fa fa-caret-down fa-fw"></i> ',
                            collectionLayout: 'fixed two-column',
                            align: 'left'
                        },
                    ]
                }
            });
            $("select").change(function() {
                var t = this;
                var content = $(this).siblings('ul').detach();
                setTimeout(function() {
                    $(t).parent().append(content);
                    $("select").material_select();
                }, 200);
            });
		      $('select').material_select();
            $('ul.tabs').tabs();
            $('.modal').modal();
            
            /* partial Amount SMS */
            
            $("#partialCheckAll").click(function () {
				var colcount=$('#tblPartialPayment').DataTable().columns().header().length;
			    var cells = $('#tblPartialPayment').DataTable().column(colcount-1).nodes(), // Cells from 1st column
			        state = this.checked;
					//alert(state);
			    for (var i = 0; i < cells.length; i += 1) {
			        cells[i].querySelector("input[type='checkbox']").checked = state;
			    }		                
			    
			});
			
			 $("input:checkbox").change(function(a){
				 
				 var colcount=$('#tblPartialPayment').DataTable().columns().header().length;
			     var cells = $('#tblPartialPayment').DataTable().column(colcount-1).nodes(), // Cells from 1st column
			         
			      state = false; 				
			     var ch=false;
			     
			     for (var i = 0; i < cells.length; i += 1) {
			    	 //alert(cells[i].querySelector("input[type='checkbox']").checked);
			    	
			         if(cells[i].querySelector("input[type='checkbox']").checked == state)
			         { 
			        	 $("#partialCheckAll").prop('checked', false);
			        	 ch=true;
			         }                      
			     }
			     if(ch==false)
			     {
			    	 $("#partialCheckAll").prop('checked', true);
			     }
			    	
			    //alert($('#tblData').DataTable().rows().count());
			});
		 
		 $('#sendPartialSmsid').click(function(){
				var count=parseInt('${count}')
				checkedId=[];
				//chb
				 var idarray = $("#tblDataPartial")
	          .find("input[type=checkbox]") 
	          .map(function() { return this.id; }) 
	          .get();
				 var j=0;
				 for(var i=0; i<idarray.length; i++)
				 {								  
					 idarray[i]=idarray[i].replace('partialSms','');
					 if($('#partialSms'+idarray[i]).is(':checked'))
					 {
						 checkedId[j]=idarray[i].split("_")[0];
						 j++;
					 }
				 }
				//alert(checkedId);
				 //return false;
				 if(checkedId.length==0)
				 {
					 $('#addeditmsg').modal('open');
				     $('#msgHead').text("Message : ");
				     $('#msg').text("Select Orders For Send SMS");
				 }
				 else
				 {
					 $('#smsPartialText').val('');
					 $('#smsSendPartialMesssage').html('');
					 $('#sendMsgPartialAmt').modal('open');
				 }
			});
			
			$('#sendPartialMessageId').click(function(){
				
				
				 $('#smsSendPartialMesssage').html('');
				
				 var smsText=$("#smsText").val();
					if(checkedId.length==0)
					{
						$('#smsSendPartialMesssage').html("<font color='red'>Select Employee For Send SMS</font>");
						return false;
					}
					if(smsText==="")
					{
						$('#smsSendPartialMesssage').html("<font color='red'>Enter SMS Text</font>");
						return false;
					}
				
				var form = $('#sendPartialSMSForm');
				//alert(form.serialize()+"&employeeDetailsId="+checkedId);
				
				$.ajax({
						type : form.attr('method'),
						url : form.attr('action'),
						data : form.serialize()+"&shopsIds="+checkedId,
						beforeSend: function() 
						{
							$('.preloader-background').show();
							$('.preloader-wrapper').show();
				        },
						success : function(data) {
							if(data==="Success")
							{
								$('#smsSendPartialMesssage').html("<font color='green'>SMS sent SuccessFully</font>");
								$('#smsPartialText').val('');
							}
							else
							{
								$('#smsSendPartialMesssage').html("<font color='red'>SMS sending Failed</font>");
							}
							$('.preloader-wrapper').hide();
							$('.preloader-background').hide();
	    				},
						error: function(xhr, status, error) {
							$('.preloader-wrapper').hide();
							$('.preloader-background').hide();
							  //alert(error +"---"+ xhr+"---"+status);
							$('#addeditmsg').modal('open');
	               	     	$('#msgHead').text("Message : ");
	               	     	$('#msg').text("Something Went Wrong"); 
	               	     		setTimeout(function() 
								  {
	      	     					$('#addeditmsg').modal('close');
								  }, 1000);
							}
				});
			});
            
			/* pending Amount SMS */
            
            $("#pendingCheckAll").click(function () {
				var colcount=$('#tblPendingPayment').DataTable().columns().header().length;
			    var cells = $('#tblPendingPayment').DataTable().column(colcount-1).nodes(), // Cells from 1st column
			        state = this.checked;
					//alert(state);
			    for (var i = 0; i < cells.length; i += 1) {
			        cells[i].querySelector("input[type='checkbox']").checked = state;
			    }		                
			    
			});
			
			 $("input:checkbox").change(function(a){
				 
				 var colcount=$('#tblPendingPayment').DataTable().columns().header().length;
			     var cells = $('#tblPendingPayment').DataTable().column(colcount-1).nodes(), // Cells from 1st column
			         
			      state = false; 				
			     var ch=false;
			     
			     for (var i = 0; i < cells.length; i += 1) {
			    	 //alert(cells[i].querySelector("input[type='checkbox']").checked);
			    	
			         if(cells[i].querySelector("input[type='checkbox']").checked == state)
			         { 
			        	 $("#pendingCheckAll").prop('checked', false);
			        	 ch=true;
			         }                      
			     }
			     if(ch==false)
			     {
			    	 $("#pendingCheckAll").prop('checked', true);
			     }
			    	
			    //alert($('#tblData').DataTable().rows().count());
			});
		 
		 $('#sendPendingSmsid').click(function(){
				var count=parseInt('${count}')
				checkedIdPending=[];
				//chb
				 var idarray = $("#tblDataPending")
	          .find("input[type=checkbox]") 
	          .map(function() { return this.id; }) 
	          .get();
				 var j=0;
				 for(var i=0; i<idarray.length; i++)
				 {								  
					 idarray[i]=idarray[i].replace('pendingSms','');
					 if($('#pendingSms'+idarray[i]).is(':checked'))
					 {
						 checkedIdPending[j]=idarray[i].split("_")[0];
						 j++;
					 }
				 }
				//alert(checkedId);
				 //return false;
				 if(checkedIdPending.length==0)
				 {
					 $('#addeditmsg').modal('open');
				     $('#msgHead').text("Message : ");
				     $('#msg').text("Select Orders For Send SMS");
				 }
				 else
				 {
					 $('#smsPendingText').val('');
					 $('#smsSendPendingMesssage').html('');
					 $('#sendMsgPendingAmt').modal('open');
				 }
			});
			
			$('#sendPendingMessageId').click(function(){
				
				
				 $('#smsSendPendingMesssage').html('');
				
				 var smsText=$("#smsText").val();
					if(checkedIdPending.length==0)
					{
						$('#smsSendPendingMesssage').html("<font color='red'>Select Employee For Send SMS</font>");
						return false;
					}
					if(smsText==="")
					{
						$('#smsSendPendingMesssage').html("<font color='red'>Enter SMS Text</font>");
						return false;
					}
				
				var form = $('#sendPendingSMSForm');
				//alert(form.serialize()+"&employeeDetailsId="+checkedId);
				
				$.ajax({
						type : form.attr('method'),
						url : form.attr('action'),
						data : form.serialize()+"&shopsIds="+checkedIdPending,
						beforeSend: function() 
						{
							$('.preloader-background').show();
							$('.preloader-wrapper').show();
				        },
						success : function(data) {
							if(data==="Success")
							{
								$('#smsSendPendingMesssage').html("<font color='green'>SMS sent SuccessFully</font>");
								$('#smsPendingText').val('');
							}
							else
							{
								$('#smsSendPendingMesssage').html("<font color='red'>SMS sending Failed</font>");
							}
							$('.preloader-wrapper').hide();
							$('.preloader-background').hide();
	    				},
						error: function(xhr, status, error) {
							$('.preloader-wrapper').hide();
							$('.preloader-background').hide();
							  //alert(error +"---"+ xhr+"---"+status);
							$('#addeditmsg').modal('open');
	               	     	$('#msgHead').text("Message : ");
	               	     	$('#msg').text("Something Went Wrong"); 
	               	     		setTimeout(function() 
								  {
	      	     					$('#addeditmsg').modal('close');
								  }, 1000);
							}
				});
			});
        });
    </script>
</head>

<body>
     <!--navbar start-->
   	<%@include file="components/navbar.jsp" %>
    <!--navbar end-->
    <!--content start-->
    <main>
       
        <div class="row ">
        	<div class="col s12 l4 m4">
      		  <div class="card horizontal">
      		  	<div class="card-image">
       				  <h6 class="white-text" style="padding:8px">Amount to be collected :</h6>
      			</div>
      			<div class="card-stacked grey lighten-3">
      			  <div class="card-content" style="padding:8px">
       			<h6 class=""><span>&#8377</span><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${collectionReportMain.amountToBePaid}" /></h6>
       			  </div>
        	    </div>
                
          	  </div>
          	  <div class="card horizontal">
      		  	<div class="card-image">
       				  <h6 class="white-text" style="padding:8px">Pending Amount :</h6>
      			</div>
      			<div class="card-stacked grey lighten-3">
      			  <div class="card-content" style="padding:8px">
       			<h6 class=""><span>&#8377</span><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${collectionReportMain.pendingAmount}" /></h6>
       			  </div>
        	    </div>
                
          	  </div>
           </div>
           <div class="col s12 l4 m4">
      		  <div class="card horizontal">
      		  	<div class="card-image">
       				  <h6 class="white-text" style="padding:8px">Full Amount collected :</h6>
      			</div>
      			<div class="card-stacked grey lighten-3">
      			  <div class="card-content" style="padding:8px">
       			<h6 class=""><span>&#8377</span><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${collectionReportMain.fullAmountCollected}" /></h6>
       			  </div>
        	    </div>
                
          	  </div>
          	  <div class="card horizontal">
      		  	<div class="card-image">
       				  <h6 class="white-text" style="padding:8px">Partial Amount Collected :</h6>
      			</div>
      			<div class="card-stacked grey lighten-3">
      			  <div class="card-content" style="padding:8px">
       			<h6 class=""><span>&#8377</span><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${collectionReportMain.partialAmountCollected}" /></h6>
       			  </div>
        	    </div>
                
          	  </div>
           </div>
           <%--  <div class="col s12 l6 m6 card">
                <div class="col s12 l6 m6 ">
                    <h6 id="amtToBeCollected" class="light-blue-text text-darken-4">Amount to be collected : <fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${collectionReportMain.amountToBePaid}" /></h6>
               		 <!--  <hr style="border:1px dashed teal;"> -->
                </div>
                <div class="col s12 l6 m6 ">
                    <h6 id="amtPending" class="light-blue-text text-darken-4">Pending Amount : <fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${collectionReportMain.pendingAmount}" /></h6>
               		  <!-- <hr style="border:1px dashed teal;"> -->
                </div>
                <div class="col s12 l6 m6 ">
                    <h6 id="fullAmtCollected" class="light-blue-text text-darken-4">Full Amount collected : <fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${collectionReportMain.fullAmountCollected}" /></h6>
               		  <!-- <hr style="border:1px dashed teal;"> -->
                </div>
                <div class="col s12 l6 m6 ">
                    <h6 id="partialAmtCollected" class="light-blue-text text-darken-4">Partial Amount Collected : <fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${collectionReportMain.partialAmountCollected}" /></h6>
                	  <!-- <hr style="border:1px dashed teal;"> -->
                </div>
            </div> --%>
            <div class="col s6 m4 l4 right" style="margin-top:1%">
            	<div class="col s12 m4 l4 right">
                    <!-- Dropdown Trigger -->
                    <a class='dropdown-button btn waves-effect waves-light grey-text  text-lighten-3 light-blue darken-4' href='#' data-activates='filter'>Filter<i
                class="material-icons right">arrow_drop_down</i></a>
                    <!-- Dropdown Structure -->
                    <ul id='filter' class='dropdown-content'>
                        <li><a href="${pageContext.servletContext.contextPath}/getCollectionReportDetails?range=Last6Months">Last 6 Month</a></li>
                        <li><a href="${pageContext.servletContext.contextPath}/getCollectionReportDetails?range=Last7days">Last 7 Days</a></li>
                        <li><a href="${pageContext.servletContext.contextPath}/getCollectionReportDetails?range=Last1Year">Last 1 Month</a></li>
                        <li><a class="rangeSelect">Range</a></li>
                        <li><a href="${pageContext.servletContext.contextPath}/getCollectionReportDetails?range=ViewAll">View All</a></li>
                    </ul>
                </div>
               <div class="col s12 m8 l8">
                      <form action="${pageContext.servletContext.contextPath}/getCollectionReportDetails" method="post">
                    <input type="hidden" name="range" value="Range"> <span class="showDates">
                              <div class="input-field col s6 m5 l5">
                                <input type="date" class="datepicker" placeholder="Choose Date" name="startDate" id="startDate" required> 
                                <label for="startDate">From</label>
                              </div>

                              <div class="input-field col s6 m5 l5">
                                    <input type="date" class="datepicker" placeholder="Choose Date" name="endDate" id="endDate">
                                     <label for="endDate">To</label>
                               </div>
                            <button type="submit">View</button>
                          </span>
                     </form> 
             </div>

                    
         </div>
         
         	
         	
      
            <div class="col s12 l12 m12">
            <br>
                <ul id="tabs" class="tabs tabs-fixed-width">
                    <li class="tab col s3 l3 left"><a class="btn z-depth-3 grey lighten-3" href="#fullPayment">Full Payment</a></li>
                    <li class="tab col s3 l3"><a class="btn z-depth-3 grey lighten-3" href="#partialPayment">Partial Payment</a></li>
                    <li class="tab col s3 l3 right"><a class="btn z-depth-3 grey lighten-3" href="#pendingPayment">Pending Payment</a></li>

                </ul>
            </div>
        </div>




        <!--Full Payment START-->
        <div id="fullPayment">

            <div class="col s12 l8 m12 ">
                <table class="striped highlight centered mdl-data-table display  select" id="tblFullPayment" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                           <th class="print-col">Sr.No.</th>
                           <th class="print-col">Order Id</th>
                           <th class="print-col">Booth Number</th>
                           <th class="print-col">Owner Name</th>
                           <th class="print-col">Mobile No</th>
                           <th class="print-col">Route No</th>
                           <th class="print-col">Area</th>
                           <th class="print-col">Cluster</th>                            
                           <th class="print-col">Total Amount</th>
                           <th class="print-col">Amount Paid</th>
                           <th class="print-col">Balance Amount</th>
                           <th class="print-col">Mode</th>
                           <th class="print-col">Date of Payment</th>
                        </tr>
                    </thead>

                    <tbody>
                    <% int rowincrement=0; %>
                    <c:if test="${not empty collectionReportMain.collectionReportPaymentDetaillist}">
					<c:forEach var="listValue" items="${collectionReportMain.collectionReportPaymentDetaillist}">					
					<c:set var="rowincrement" value="${rowincrement + 1}" scope="page"/>
					<c:choose>  
						    <c:when test="${listValue.payStatus == 'Full'}"> 
		                        <tr>
		                            <td><c:out value="${rowincrement}" /></td>		                           
		                            <td><c:out value="${listValue.orderId}" /></td>
		                            <td><c:out value="${listValue.boothId}" /></td>
		                              <td><c:out value="${listValue.ownerName}" /></td>
		                            <td><c:out value="${listValue.mobileNumber}" /></td>
		                            <td><c:out value="${listValue.routeName}" /></td>
		                            <td><c:out value="${listValue.areaName}" /></td>
		                            <td><c:out value="${listValue.clusterName}" /></td>
		                            
		                            <td><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${listValue.totalAmount}" /></td>
		                            <td><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${listValue.amountPaid}" /></td>
		                            <td><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${listValue.balanceAmount}" /></td>
		                            <td><c:out value="${listValue.payMode}" /></td>
		                            <td>
			                            <fmt:formatDate pattern = "yyyy-MM-dd" var="date"   value = "${listValue.paidDate}"  />
				                        <c:out value="${date}" />
			                        </td>		
		                        </tr>
		                     </c:when>
		             </c:choose>
					</c:forEach>
					</c:if>
                    </tbody>
                </table>
            </div>
        </div>
        <!--Full Payment END-->


        <!--Partial Payment START-->
        <div id="partialPayment">

            <div class="col s12 l8 m12 ">
                <table class="striped highlight centered mdl-data-table display  select" id="tblPartialPayment" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th class="print-col">Sr.No.</th>
                           
                            <th class="print-col">Order Id</th>
                             <th class="print-col">Booth No.</th>
                              <th class="print-col">Owner Name</th>
                            <th class="print-col">Mobile No</th>
                             <th class="print-col">Route No</th>
                            <th class="print-col">Area</th>
                            <th class="print-col">Cluster</th>
                            
                            <th class="print-col">Total Amount</th>
                            <th class="print-col">Amount Paid</th>
                            <th class="print-col">Balance Amount</th>
                            <th class="print-col">Next Due Date</th>
                            <th class="print-col">Mode</th>
                            <th class="print-col">Date of Payment</th>
                            <th> <a href="#" id="sendPartialSmsid"> SMS  </a>
                                <input type="checkbox" class="filled-in" id="partialCheckAll" />
                                <label for="partialCheckAll"></label>
                            </th>
                        </tr>
                    </thead>

                    <tbody id="tblDataPartial">
                    <%rowincrement=0; int checkBoxId=0;%>
                    <c:if test="${not empty collectionReportMain.collectionReportPaymentDetaillist}">
					<c:forEach var="listValue" items="${collectionReportMain.collectionReportPaymentDetaillist}">					
					<c:set var="rowincrement" value="${rowincrement + 1}" scope="page"/>
					<c:set var="checkBoxId" value="${checkBoxId + 1}" scope="page"/>
					<c:choose>  
						    <c:when test="${listValue.payStatus == 'Partial'}"> 
		                        <tr>
		                            <td><c:out value="${rowincrement}" /></td>		                           
		                            <td><c:out value="${listValue.orderId}" /></td>
		                            <td><c:out value="${listValue.boothId}" /></td>
		                              <td><c:out value="${listValue.ownerName}" /></td>
		                            <td><c:out value="${listValue.mobileNumber}" /></td>
		                            <td><c:out value="${listValue.routeName}" /></td>
		                            <td><c:out value="${listValue.areaName}" /></td>
		                            <td><c:out value="${listValue.clusterName}" /></td>		                           
		                            <td><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${listValue.totalAmount}" /></td>
		                            <td><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${listValue.amountPaid}" /></td>
		                            <td><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${listValue.balanceAmount}" /></td>
		                            <td>
			                            <fmt:formatDate pattern = "yyyy-MM-dd" var="date"   value = "${listValue.nextDueDate}"  />
				                        <c:out value="${date}" />
			                        </td>
		                            <td><c:out value="${listValue.payMode}" /></td>
				                    <td>
			                            <fmt:formatDate pattern = "yyyy-MM-dd" var="date"   value = "${listValue.paidDate}"  />
				                        <c:out value="${date}" />
			                        </td>
		                            <td>
		
		                                <input type="checkbox" class="filled-in" id="partialSms${listValue.orderId}_${checkBoxId}" />
		                                <label for="partialSms${listValue.orderId}_${checkBoxId}"></label>
		
		                            </td>
		
		                        </tr>
                        </c:when>
		             </c:choose>
					</c:forEach>
					</c:if>
                        <!-- Modal Structure for sms -->
                        
                                            
                     <div id="sendMsgPartialAmt" class="modal"  style="width:40%;height:60%;">
                        <form id="sendPartialSMSForm" action="${pageContext.request.contextPath}/sendSMSTOShopsUsingOrderId" method="post">
                            <div class="modal-content">
                            
                                <h4 class="center">Message</h4>
                                <hr>
                                 <div class="row">
                                <div class="col s12 l7 m7 push-l3">
                                      <label for="smsPartialText">Type Message</label>
                                <textarea id="smsPartialText" class="materialize-textarea" name="smsText" required></textarea>
                                </div> 
                                 <div class="input-field col s12 l7 push-l4 m5 push-m4 ">                                
                   			 		 <font color='red'><span id="smsSendPartialMesssage"></span></font>
            					</div>                         
                           	 </div>
                                
                             

                            </div>
                          <div class="modal-footer center-align row">
                          <hr>
                            <div class="col s6 m6 l4 offset-l1">
                              <a href="#!" id="sendPartialMessageId" class="modal-action waves-effect btn">Send</a>
                            		
                             </div>
                          	<div class="col s6 m6 l4">	
                           			 <a href="#!" class="modal-action modal-close waves-effect  btn red ">Cancel</a>
                             </div>
                            
                          
                        </div>
                            
                        </form>
                        </div>

                    </tbody>
                </table>
            </div>
        </div>
        <!--Partial Payment END-->
        <!--Pending payment START-->
        <div id="pendingPayment">


            <div class="col s12 l8 m12 ">
                <table class="striped highlight centered mdl-data-table display  select" id="tblPendingPayment" cellspacing="0" width="100%">
                    <thead>
                   
                        <tr>
                            <th class="print-col">Sr.No.</th>                            
                            <th class="print-col">Order Id</th>
                            <th class="print-col">Booth No<th>
                            <th class="print-col">Owner Name<th>
                            <th class="print-col">Mobile No</th>
                            <th class="print-col">Route No<th>
                            <th class="print-col">Area</th>
                            <th class="print-col">Cluster</th>
                            <th class="print-col">Amount Due</th>
                            <th class="print-col">Payment Due Date</th>
                            <th> <a href="#" id="sendPendingSmsid"> SMS  </a>
                                <input type="checkbox" class="filled-in" id="pendingCheckAll" />
                                <label for="pendingCheckAll"></label>
                            </th>
                        </tr>
                    </thead>

                    <tbody  id="tblDataPending">
                    <%rowincrement=0; %>
                    <c:if test="${not empty collectionReportMain.collectionReportPaymentDetaillist}">
					<c:forEach var="listValue" items="${collectionReportMain.collectionReportPaymentDetaillist}">					
					<c:set var="rowincrement" value="${rowincrement + 1}" scope="page"/>
					<c:set var="checkBoxId" value="${checkBoxId + 1}" scope="page"/>
					<c:choose>  
						    <c:when test="${listValue.payStatus == 'UnPaid'}"> 
		                        <tr>
		                            <td><c:out value="${rowincrement}" /></td>		                           
		                            <td><c:out value="${listValue.orderId}" /></td>
		                            <td><c:out value="${listValue.boothId}" /></td>
		                              <td><c:out value="${listValue.ownerName}" /></td>
		                            <td><c:out value="${listValue.mobileNumber}" /></td>
		                            <td><c:out value="${listValue.routeName}" /></td>
		                            <td><c:out value="${listValue.areaName}" /></td>
		                            <td><c:out value="${listValue.clusterName}" /></td>
		                            <td><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${listValue.balanceAmount}" /></td>
		                            <td>
			                            <fmt:formatDate pattern = "yyyy-MM-dd" var="date"   value = "${listValue.nextDueDate}"  />
			                        	<c:out value="${date}" />
		                        	</td>
		                            <td>
		
		                                <input type="checkbox" class="filled-in" id="pendingSms${listValue.orderId}_${checkBoxId}" />
		                                <label for="pendingSms${listValue.orderId}_${checkBoxId}"></label>
		
		                            </td>
		
		                        </tr>
                        </c:when>
		             </c:choose>
					</c:forEach>
					</c:if>
                        <!-- Modal Structure for sms -->
                        <div id="sendMsgPendingAmt" class="modal">
                        <form id="sendPendingSMSForm" action="${pageContext.request.contextPath}/sendSMSTOShopsUsingOrderId" method="post">
                            
                            <div class="modal-content">
                               <h4 class="center">Message</h4>
                                <hr>
                                 <div class="row">
                                <div class="col s12 l7 m7 push-l3">
                                      <label for="smsPendingText">Type Message</label>
                                <textarea id="smsPendingText" class="materialize-textarea" name="smsText" required></textarea>
                                </div> 
                                 <div class="input-field col s12 l7 push-l4 m5 push-m4 ">                                
                   			 		 <font color='red'><span id="smsSendPendingMesssage"></span></font>
            					</div>                         
                           	 </div>
                            </div>
                            <div class="input-field col s12 l7 push-l4 m5 push-m4 ">                                
                   			 		 <font color='red'><span id="smsSendPendingMesssage"></span></font>
            					</div>  
            					
            		  <div class="modal-footer center-align row">
                          <hr>
                            <div class="col s6 m6 l4 offset-l1">
                              <a href="#!" id="sendPendingMessageId" class="modal-action waves-effect btn">Send</a>
                            		
                             </div>
                          	<div class="col s6 m6 l4">	
                           			 <a href="#!" class="modal-action modal-close waves-effect  btn red ">Cancel</a>
                             </div>
                            
                          
                        </div>
                          
                        </form>
                        </div>

                    </tbody>
                </table>
            </div>
        </div>
        <!--Pending payment END-->


<div class="row">
			<div class="col s8 m2 l2">
				<div id="addeditmsg" class="modal" style="width:40%;">
					<div class="modal-content">
						<h5 id="msgHead">Error</h5>
						<hr>	
						<h6 id="msg"></h6>
					</div>
					<div class="col s8 m2 l2 offset-l5 modal-footer">
						<a href="#!"
							class="modal-action modal-close waves-effect btn blue-gradient">OK</a>
					</div>
				</div>
			</div>
		</div>

    </main>
    <!--content end-->
</body>

</html>