<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<html>

<head>
  <%@include file="components/header_imports.jsp" %>
    <script type="text/javascript">
	    function showTaxCalculation(productId)
	    {
	
	    	$.ajax({
	    		url :"${pageContext.request.contextPath}/fetchProductTaxcalculation?productId="+productId,
	    		dataType : "json",
	    		success : function(data) {
	    			
	    			$('#cgstPerId').text(data.cgstPercentage);
	    			$('#cgstAmtId').text(data.cgstRate);
	    			$('#sgstPerId').text(data.sgstPercentage);
	    			$('#sgstAmtId').text(data.sgstRate);
	    			$('#igstPerId').text(data.igstPercentage);
	    			$('#igstAmtId').text(data.igstRate);
	    			$('#taxAmountId').text(data.totalTaxAmount.toFixed(2));
	    			$('.modal').modal();
	    			$('#tax').modal('open');
	    		},
	    		error: function(xhr, status, error) {
	    			  //var err = eval("(" + xhr.responseText + ")");
	    			  alert("Error");
	    			}
	    	});
	    	
	    }
	    $(document).ready(function() {
            $(".showQuantity").hide();
            $(".showDates").hide();
            $("#oneDateDiv").hide();
            $(".topProduct").click(function() {
                $(".showQuantity").show();
                $(".showDates").hide();
                $("#oneDateDiv").hide();
            });
           
            $(".rangeSelect").click(function() {
            	$("#oneDateDiv").hide();	
                $(".showDates").show();
                $(".showQuantity").hide();
            });
           
        	$(".pickdate").click(function(){
        		 $(".showQuantity").hide();
        		 $(".showDates").hide();
        		$("#oneDateDiv").show();
        	});
            $('#topProductId').change(function(){
            
            	window.location.href = '${pageContext.servletContext.contextPath}/fetchProductListForReport?range=TopProducts&topProductNo='+$('#topProductId').val();
            	
            });
        });

    </script>
    <style>
    .card-image{
		width:40% !important;
		background-color:#01579b !important;
	}
    </style>
</head>

<body>
    <!--navbar start-->
   	<%@include file="components/navbar.jsp" %>
    <!--navbar end-->
    <!--content start-->
    <main>
     
        <div class="row">
        	 <div class="col s12 l4 m4">
      		  <div class="card horizontal">
      		  	<div class="card-image">
       				  <h6 class="white-text" style="padding:8px">Total Amount:</h6>
      			</div>
      			<div class="card-stacked grey lighten-3">
      			  <div class="card-content" style="padding:8px">
       			<h6 class="">&#8377<span id="totalAmountId"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${salesManReport.salesManCollectionAmount}" /></span></h6>
       			  </div>
        	    </div>
                
          	  </div>
           </div>  
           <div class="col s12 l4 m4">
      		  <div class="card horizontal">
      		  	<div class="card-image">
       				  <h6 class="white-text" style="padding:8px">No of ASM:</h6>
      			</div>
      			<div class="card-stacked grey lighten-3">
      			  <div class="card-content" style="padding:8px">
       			<h6 class="">5</h6>
       			  </div>
        	    </div>
                
          	  </div>
           </div>  
        	    <div class="col s12 m4 l4 right" style="margin-top:1%;">
                <div class="col s6 m4 l4 right">
                    <!-- Dropdown Trigger -->
                    <a class='dropdown-button btn waves-effect waves-light grey-text  text-lighten-3 light-blue darken-4' href='#' data-activates='filter'>Filter<i
              			  class="material-icons right">arrow_drop_down</i></a>
                    <!-- Dropdown Structure -->
                    <ul id='filter' class='dropdown-content'>
                    	<li><a href="${pageContext.request.contextPath}/fetchFilteredOrderIssueReportForWeb?range=last7days&areaId=0">Yesterday</a></li>
                        <li><a href="${pageContext.request.contextPath}/fetchFilteredOrderIssueReportForWeb?range=last7days&areaId=0">Last 7 Days</a></li>
                        <li><a href="${pageContext.request.contextPath}/fetchFilteredOrderIssueReportForWeb?range=last1month&areaId=0">Last 1 Month</a></li>
                        <li><a href="${pageContext.request.contextPath}/fetchFilteredOrderIssueReportForWeb?range=last3months&areaId=0">Last 3 Month</a></li>
                        <li><a class="rangeSelect">Range</a></li>
                        <li><a class="pickdate">Pick date</a></li>                        
                        <li><a href="${pageContext.request.contextPath}/fetchFilteredOrderIssueReportForWeb?range=viewAll&areaId=0">View All</a></li>
                    </ul>
                </div>
                <div class="col s12 l8	m8 right"> 
                 <form action="${pageContext.request.contextPath}/fetchFilteredOrderIssueReportForWeb" method="post">
                    <input type="hidden" name="ranged" value="dateRange">
                     <span class="showDates">
                     			 <div class="input-field col s6 m2 l2 right">
                            <button type="submit">View</button>
                            </div>
                             
                              <div class="input-field col s6 m5 l5 right">
                                    <input type="date" class="datepicker" placeholder="Choose Date" name="toDate" id="endDate">
                                     <label for="endDate">To</label>
                               </div>
                                <div class="input-field col s6 m5 l5 right">
                                <input type="date" class="datepicker" placeholder="Choose Date" name="fromDate" id="startDate" required> 
                                <input type="hidden" value="0" name="areaId">
                                <input type="hidden" value="range" name="range">
                                <label for="startDate">From</label>
                                 </div>
                                 
                               
                          </span>
                </form>
                 </div>
				<div class="col s12 l6	m6 right" id="oneDateDiv">                            	
                    <form action="${pageContext.request.contextPath}/fetchFilteredOrderIssueReportForWeb" method="post">
	                    <div class="input-field col s12 m10 l10">
		                    <input type="text" id="oneDate" class="datepicker" placeholder="Choose date" name="fromDate">
		                    <label for="oneDate" class="black-text">Pick Date</label>
	                    </div>
	                    <div class="input-field col s12 m2 l2">
	                     <button type="submit">View</button>
	                    <input type="hidden" value="0" name="areaId">
                        <input type="hidden" value=pickDate name="range">
                       
                        </div>
                    </form>
               </div>
               
               
                
            </div>
            	
                             
 </div>
        	
 
        <div class="row">
            <div class="col s12 l12 m12 ">
                <table class="striped highlight bordered centered " id="tblData" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th class="print-col">Sr.No</th>
                            <th class="print-col">Area sales Manager Name</th>                            
                            <th class="print-col">Route No</th>
                            <th class="print-col">Area</th>
                            <th class="print-col">Cluster</th>
                            <th class="print-col">Total No of order</th>
                            <th class="print-col">Total Amount</th>
                            
                        </tr>
                    </thead>

                    <tbody>
                    
                    <c:if test="${not empty productReportViews}">
					<c:forEach var="listValue" items="${productReportViews}">
                        <tr>
                            <td><c:out value="${listValue.srno}" /></td>
                            <td><a href="#"><c:out value="${listValue.product.productName}"/>View</a></td>
                            <td><a href="#areaDetails" class="modal-trigger"><c:out value="${listValue.product.categories.categoryName}" />View</a></td>
                            <td><c:out value="${listValue.product.brand.name}" /></td>
                            <td><c:out value="${listValue.issuedQuantity}" /></td>
                            <td><c:out value="${listValue.issuedQuantity}" /></td>
                            
                           <%--  <td><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${listValue.rateWithTax}" /></td> --%>
                            <td>
                            
                            <fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${listValue.amountWithTax}" /> 
                            </td>
                           
                            <%-- <button class="btn-flat blue-text" onclick="showTaxCalculation(${listValue.product.productId})"> --%>
                            <!-- </button> -->
                           
                        </tr>
                     </c:forEach>
                     </c:if>
                     
                    </tbody>
                </table>
            </div>
        </div>
			<!-- Modal Structure for View Product Details -->

        <div id="areaDetails" class="row modal" style="width:40%;">
            <div class="modal-content">
                <h4 class="center">Product Details</h4>
                <hr>			
               
                	
                   <table border="2" class="centered tblborder">
                    <thead>
                        <tr>
                            <th>Sr.No</th>
                            <th>Route No</th>
                            <th>Area</th>
                            <th>Cluster</th>
                          
                        </tr>
                    </thead>
                    <tbody id="tbproductlist">
                       <!--  <tr>
                            <td>1</td>
                            <td>abc</td>
                            <td>abc</td>
                            <td>abc</td>
                        </tr> -->
                    </tbody>
                </table>
            </div>
          
            <div class="modal-footer">
			
				<div class="col s12 m6 l5 offset-l2">
						 <a href="#!" class="modal-action modal-close waves-effect center btn">Close</a>
				</div>
				
				
			</div>			
               
            </div>
            <div class="row">
			<div class="col s8 m2 l2">
				<div id="addeditmsg" class="modal" style="width:40%;">
					<div class="modal-content">
						<h5 id="msgHead">Error</h5>
						<hr>	
						<h6 id="msg"></h6>
					</div>
					<div class="col s8 m2 l2 offset-l5 modal-footer">
						<a href="#!"
							class="modal-action modal-close waves-effect btn grey-text  text-lighten-3 light-blue darken-4">OK</a>
					</div>
				</div>
			</div>
		</div>
 
    </main>
    <!--content end-->
</body>

</html>