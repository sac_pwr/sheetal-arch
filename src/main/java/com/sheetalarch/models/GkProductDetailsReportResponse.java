package com.sheetalarch.models;

public class GkProductDetailsReportResponse {
	
	private String productName;
	private long qty;
	private double amount;
	public String getProductName() {
		return productName;
	}
	public void setProductName(String productName) {
		this.productName = productName;
	}
	public long getQty() {
		return qty;
	}
	public void setQty(long qty) {
		this.qty = qty;
	}
	public double getAmount() {
		return amount;
	}
	public void setAmount(double amount) {
		this.amount = amount;
	}
	@Override
	public String toString() {
		return "GkProductDetailsReportResponse [productName=" + productName + ", qty=" + qty + ", amount=" + amount
				+ "]";
	}
	
	

}
