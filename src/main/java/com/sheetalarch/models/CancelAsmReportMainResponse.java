package com.sheetalarch.models;

import java.util.List;

public class CancelAsmReportMainResponse extends BaseDomain{
	
	private List<CancelAsmReportResponse> cancelAsmReportResponses;
	

	public List<CancelAsmReportResponse> getCancelAsmReportResponses() {
		return cancelAsmReportResponses;
	}

	public void setCancelAsmReportResponses(List<CancelAsmReportResponse> cancelAsmReportResponses) {
		this.cancelAsmReportResponses = cancelAsmReportResponses;
	}

	@Override
	public String toString() {
		return "CancelAsmReportMainResponse [cancelAsmReportResponses=" + cancelAsmReportResponses + "]";
	}
	
	

}
