package com.sheetalarch.models;

public class DispatchProductNameAndQtyListResponse extends BaseDomain{
	
	private String productName;
	private long qty;
	private long productId;
	private long employeeId;
	public String getProductName() {
		return productName;
	}
	public void setProductName(String productName) {
		this.productName = productName;
	}
	public long getQty() {
		return qty;
	}
	public void setQty(long qty) {
		this.qty = qty;
	}
	public long getProductId() {
		return productId;
	}
	public void setProductId(long productId) {
		this.productId = productId;
	}
	public long getEmployeeId() {
		return employeeId;
	}
	public void setEmployeeId(long employeeId) {
		this.employeeId = employeeId;
	}
	@Override
	public String toString() {
		return "DispatchProductNameAndQtyListResponse [productName=" + productName + ", qty=" + qty + ", productId="
				+ productId + ", employeeId=" + employeeId + "]";
	}


	

}
