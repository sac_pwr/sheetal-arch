package com.sheetalarch.models;

import java.util.List;

import com.sheetalarch.entity.Cluster;
import com.sheetalarch.entity.EmployeeDetails;

public class GKOrderListClusterAndASMListResponse extends BaseDomain {
	
	private List<Cluster> clusterList;
	private List<EmployeeDetails> asmList;
	public List<Cluster> getClusterList() {
		return clusterList;
	}
	public void setClusterList(List<Cluster> clusterList) {
		this.clusterList = clusterList;
	}
	public List<EmployeeDetails> getAsmList() {
		return asmList;
	}
	public void setAsmList(List<EmployeeDetails> asmList) {
		this.asmList = asmList;
	}
	@Override
	public String toString() {
		return "GKOrderListClusterAndASMListResponse [clusterList=" + clusterList + ", asmList=" + asmList + "]";
	}
	
	
	

}
