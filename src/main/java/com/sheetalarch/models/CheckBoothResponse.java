package com.sheetalarch.models;

public class CheckBoothResponse extends BaseDomain {

	private String boothAvailable;

	public String getBoothAvailable() {
		return boothAvailable;
	}

	public void setBoothAvailable(String boothAvailable) {
		this.boothAvailable = boothAvailable;
	}

	@Override
	public String toString() {
		return "CheckBoothResponse [boothAvailable=" + boothAvailable + "]";
	}
}
