package com.sheetalarch.models;

import java.util.List;

import com.sheetalarch.entity.OrderDetails;
import com.sheetalarch.entity.OrderProductDetails;

public class OrderListResponse extends BaseDomain{
	
	private OrderDetails orderDetailsList;
	private String employeeName;
	
	public OrderDetails getOrderDetailsList() {
		return orderDetailsList;
	}
	public void setOrderDetailsList(OrderDetails orderDetailsList) {
		this.orderDetailsList = orderDetailsList;
	}
	public String getEmployeeName() {
		return employeeName;
	}
	public void setEmployeeName(String employeeName) {
		this.employeeName = employeeName;
	}
	@Override
	public String toString() {
		return "OrderListResponse [orderDetailsList=" + orderDetailsList + ", employeeName=" + employeeName + "]";
	}
	
	
	

}
