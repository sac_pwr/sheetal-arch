package com.sheetalarch.models;

import java.util.List;

import com.sheetalarch.entity.Employee;
import com.sheetalarch.entity.EmployeeDetails;

public class SMListResponse extends BaseDomain{
	
	private List<Employee> employeeList;

	public List<Employee> getEmployeeList() {
		return employeeList;
	}

	public void setEmployeeList(List<Employee> employeeList) {
		this.employeeList = employeeList;
	}

	@Override
	public String toString() {
		return "SMListResponse [employeeList=" + employeeList + "]";
	}

	
	

}
