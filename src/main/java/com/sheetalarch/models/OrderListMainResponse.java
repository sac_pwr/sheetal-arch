package com.sheetalarch.models;

import java.util.List;

public class OrderListMainResponse extends BaseDomain{

	private List<OrderListResponse> orderListResponse;

	public List<OrderListResponse> getOrderListResponse() {
		return orderListResponse;
	}

	public void setOrderListResponse(List<OrderListResponse> orderListResponse) {
		this.orderListResponse = orderListResponse;
	}

	@Override
	public String toString() {
		return "OrderListMainResponse [orderListResponse=" + orderListResponse + "]";
	}
	
	
}
