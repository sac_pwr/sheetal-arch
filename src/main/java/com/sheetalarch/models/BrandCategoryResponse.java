package com.sheetalarch.models;

import java.util.List;

import com.sheetalarch.entity.Brand;
import com.sheetalarch.entity.Categories;

	public class BrandCategoryResponse extends BaseDomain{
		
		private List<Brand> brandList;
		private List<Categories> categoriesList;
		public List<Brand> getBrandList() {
			return brandList;
		}
		public void setBrandList(List<Brand> brandList) {
			this.brandList = brandList;
		}
		public List<Categories> getCategoriesList() {
			return categoriesList;
		}
		public void setCategoriesList(List<Categories> categoriesList) {
			this.categoriesList = categoriesList;
		}
		@Override
		public String toString() {
			return "BrandCategoryResponse [brandList=" + brandList + ", categoriesList=" + categoriesList + "]";
		}
}
