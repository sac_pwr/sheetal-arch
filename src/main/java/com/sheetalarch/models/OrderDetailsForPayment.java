package com.sheetalarch.models;

import java.util.Date;

public class OrderDetailsForPayment {

	private String boothNo;
	private String ownerName;
	private double amountDue;
	private Date dueDate;
	private String orderId;
	private String reOrderId;
	private String orderType;
	
	public OrderDetailsForPayment(String boothNo, String ownerName, double amountDue, Date dueDate, String orderId,
			String reOrderId, String orderType) {
		super();
		this.boothNo = boothNo;
		this.ownerName = ownerName;
		this.amountDue = amountDue;
		this.dueDate = dueDate;
		this.orderId = orderId;
		this.reOrderId = reOrderId;
		this.orderType = orderType;
	}
	public String getBoothNo() {
		return boothNo;
	}
	public void setBoothNo(String boothNo) {
		this.boothNo = boothNo;
	}
	public String getOwnerName() {
		return ownerName;
	}
	public void setOwnerName(String ownerName) {
		this.ownerName = ownerName;
	}
	public double getAmountDue() {
		return amountDue;
	}
	public void setAmountDue(double amountDue) {
		this.amountDue = amountDue;
	}
	public Date getDueDate() {
		return dueDate;
	}
	public void setDueDate(Date dueDate) {
		this.dueDate = dueDate;
	}
	public String getOrderId() {
		return orderId;
	}
	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}
	public String getReOrderId() {
		return reOrderId;
	}
	public void setReOrderId(String reOrderId) {
		this.reOrderId = reOrderId;
	}
	public String getOrderType() {
		return orderType;
	}
	public void setOrderType(String orderType) {
		this.orderType = orderType;
	}
	@Override
	public String toString() {
		return "OrderDetailsForPayment [boothNo=" + boothNo + ", ownerName=" + ownerName + ", amountDue=" + amountDue
				+ ", dueDate=" + dueDate + ", orderId=" + orderId + ", reOrderId=" + reOrderId + ", orderType="
				+ orderType + "]";
	}
	
	
}
