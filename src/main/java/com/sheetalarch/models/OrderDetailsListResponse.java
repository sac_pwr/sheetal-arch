package com.sheetalarch.models;

import java.util.List;

import com.sheetalarch.entity.OrderDetails;

public class OrderDetailsListResponse  extends BaseDomain{
	
	private List<OrderDetails> orderDetailsList;

	public List<OrderDetails> getOrderDetailsList() {
		return orderDetailsList;
	}

	public void setOrderDetailsList(List<OrderDetails> orderDetailsList) {
		this.orderDetailsList = orderDetailsList;
	}

	@Override
	public String toString() {
		return "OrderDetailsListResponse [orderDetailsList=" + orderDetailsList + "]";
	}
	
	

}
