package com.sheetalarch.models;

public class CancelAsmReportResponse {
	
	private String salesmanName;
	private long employeeId;
	private String mobileNo;
	private String routeNo;
	private long noOfCancelOrder;
	private long noOfReOrder;
	
	public String getSalesmanName() {
		return salesmanName;
	}
	public void setSalesmanName(String salesmanName) {
		this.salesmanName = salesmanName;
	}
	public long getEmployeeId() {
		return employeeId;
	}
	public void setEmployeeId(long employeeId) {
		this.employeeId = employeeId;
	}
	public String getMobileNo() {
		return mobileNo;
	}
	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}
	public String getRouteNo() {
		return routeNo;
	}
	public void setRouteNo(String routeNo) {
		this.routeNo = routeNo;
	}
	public long getNoOfCancelOrder() {
		return noOfCancelOrder;
	}
	public void setNoOfCancelOrder(long noOfCancelOrder) {
		this.noOfCancelOrder = noOfCancelOrder;
	}
	public long getNoOfReOrder() {
		return noOfReOrder;
	}
	public void setNoOfReOrder(long noOfReOrder) {
		this.noOfReOrder = noOfReOrder;
	}
	@Override
	public String toString() {
		return "CancelAsmReportResponse [salesmanName=" + salesmanName + ", employeeId=" + employeeId + ", mobileNo="
				+ mobileNo + ", routeNo=" + routeNo + ", noOfCancelOrder=" + noOfCancelOrder + ", noOfReOrder="
				+ noOfReOrder + "]";
	}
	
	
}
