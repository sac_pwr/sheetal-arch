package com.sheetalarch.models;

import java.util.List;

import com.sheetalarch.entity.Route;

public class GkProductDetailsReportMainResponse extends BaseDomain {
	
	private List<GkProductDetailsReportResponse> gkProductDetailsReportResponses;
	private String salesmanName;
	private List<Route> routeList;
	private String clusterName;
	private String mobileNo;
	public List<GkProductDetailsReportResponse> getGkProductDetailsReportResponses() {
		return gkProductDetailsReportResponses;
	}
	public void setGkProductDetailsReportResponses(List<GkProductDetailsReportResponse> gkProductDetailsReportResponses) {
		this.gkProductDetailsReportResponses = gkProductDetailsReportResponses;
	}
	public String getSalesmanName() {
		return salesmanName;
	}
	public void setSalesmanName(String salesmanName) {
		this.salesmanName = salesmanName;
	}
	public List<Route> getRouteList() {
		return routeList;
	}
	public void setRouteList(List<Route> routeList) {
		this.routeList = routeList;
	}
	public String getClusterName() {
		return clusterName;
	}
	public void setClusterName(String clusterName) {
		this.clusterName = clusterName;
	}
	public String getMobileNo() {
		return mobileNo;
	}
	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}
	@Override
	public String toString() {
		return "GkProductDetailsReportMainResponse [gkProductDetailsReportResponses=" + gkProductDetailsReportResponses
				+ ", salesmanName=" + salesmanName + ", routeList=" + routeList + ", clusterName=" + clusterName
				+ ", mobileNo=" + mobileNo + "]";
	}
	
	
	
}
