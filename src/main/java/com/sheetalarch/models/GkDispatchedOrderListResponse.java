package com.sheetalarch.models;

import java.util.List;

import com.sheetalarch.entity.DispatchOrderDetails;
import com.sheetalarch.entity.Route;

public class GkDispatchedOrderListResponse {
	
	private DispatchOrderDetails dispatchOrderDetails;
	private String employeeName;
	private List<Route>  routes;
	public DispatchOrderDetails getDispatchOrderDetails() {
		return dispatchOrderDetails;
	}
	public void setDispatchOrderDetails(DispatchOrderDetails dispatchOrderDetails) {
		this.dispatchOrderDetails = dispatchOrderDetails;
	}
	public String getEmployeeName() {
		return employeeName;
	}
	public void setEmployeeName(String employeeName) {
		this.employeeName = employeeName;
	}
	public List<Route> getRoutes() {
		return routes;
	}
	public void setRoutes(List<Route> routes) {
		this.routes = routes;
	}
	@Override
	public String toString() {
		return "GkDispatchedOrderListResponse [dispatchOrderDetails=" + dispatchOrderDetails + ", employeeName="
				+ employeeName + ", routes=" + routes + "]";
	}
	
	

}
