package com.sheetalarch.models;

import java.util.List;

import com.sheetalarch.entity.DispatchOrderDetails;
import com.sheetalarch.entity.DispatchOrderProductDetails;

public class DispatchedOrderAndProductListResponse extends BaseDomain {

	private DispatchOrderDetails dispatchOrderDetails;
	private List<DispatchOrderProductDetails> dispatchOrderProductDetailsList;
	private String gateKeeperName;
	private String mobileNo;
	
	public DispatchOrderDetails getDispatchOrderDetails() {
		return dispatchOrderDetails;
	}
	public void setDispatchOrderDetails(DispatchOrderDetails dispatchOrderDetails) {
		this.dispatchOrderDetails = dispatchOrderDetails;
	}
	public List<DispatchOrderProductDetails> getDispatchOrderProductDetailsList() {
		return dispatchOrderProductDetailsList;
	}
	public void setDispatchOrderProductDetailsList(List<DispatchOrderProductDetails> dispatchOrderProductDetailsList) {
		this.dispatchOrderProductDetailsList = dispatchOrderProductDetailsList;
	}
	public String getGateKeeperName() {
		return gateKeeperName;
	}
	public void setGateKeeperName(String gateKeeperName) {
		this.gateKeeperName = gateKeeperName;
	}
	public String getMobileNo() {
		return mobileNo;
	}
	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}
	@Override
	public String toString() {
		return "DispatchedOrderAndProductListResponse [dispatchOrderDetails=" + dispatchOrderDetails
				+ ", dispatchOrderProductDetailsList=" + dispatchOrderProductDetailsList + ", gateKeeperName="
				+ gateKeeperName + ", mobileNo=" + mobileNo + "]";
	}
	
	
	
	
}
