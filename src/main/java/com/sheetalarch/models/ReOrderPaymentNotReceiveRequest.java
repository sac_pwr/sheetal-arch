package com.sheetalarch.models;

import java.util.List;

import com.sheetalarch.entity.Booth;
import com.sheetalarch.entity.OrderDetails;
import com.sheetalarch.entity.OrderProductDetails;

public class ReOrderPaymentNotReceiveRequest {
	
	private String nextDueDate;
	private OrderDetails orderDetails;
	private List<OrderProductDetails> orderProductDetails;
	private Booth booth;
	public String getNextDueDate() {
		return nextDueDate;
	}
	public void setNextDueDate(String nextDueDate) {
		this.nextDueDate = nextDueDate;
	}
	public OrderDetails getOrderDetails() {
		return orderDetails;
	}
	public void setOrderDetails(OrderDetails orderDetails) {
		this.orderDetails = orderDetails;
	}
	public List<OrderProductDetails> getOrderProductDetails() {
		return orderProductDetails;
	}
	public void setOrderProductDetails(List<OrderProductDetails> orderProductDetails) {
		this.orderProductDetails = orderProductDetails;
	}
	public Booth getBooth() {
		return booth;
	}
	public void setBooth(Booth booth) {
		this.booth = booth;
	}
	@Override
	public String toString() {
		return "ReOrderPaymentNotReceiveRequest [nextDueDate=" + nextDueDate + ", orderDetails=" + orderDetails
				+ ", orderProductDetails=" + orderProductDetails + ", booth=" + booth + "]";
	}
	
	
	

}
