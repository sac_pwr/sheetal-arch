package com.sheetalarch.models;

public class BookOrderResponse extends BaseDomain{

	private String orderId;

	private String boothNo;
	private double totalAmount;
	private double totalAmountWithTax;
	public String getOrderId() {
		return orderId;
	}
	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}
	public String getBoothNo() {
		return boothNo;
	}
	public void setBoothNo(String boothNo) {
		this.boothNo = boothNo;
	}
	public double getTotalAmount() {
		return totalAmount;
	}
	public void setTotalAmount(double totalAmount) {
		this.totalAmount = totalAmount;
	}
	public double getTotalAmountWithTax() {
		return totalAmountWithTax;
	}
	public void setTotalAmountWithTax(double totalAmountWithTax) {
		this.totalAmountWithTax = totalAmountWithTax;
	}
	@Override
	public String toString() {
		return "BookOrderResponse [orderId=" + orderId + ", boothNo=" + boothNo + ", totalAmount=" + totalAmount
				+ ", totalAmountWithTax=" + totalAmountWithTax + "]";
	}
	
}
