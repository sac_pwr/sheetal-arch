package com.sheetalarch.models;

import java.util.List;

import com.sheetalarch.entity.ReOrderDetails;
import com.sheetalarch.entity.ReOrderProductDetails;

public class ReOrderDetailsAndProductDetailsRespose  extends BaseDomain{
	
	private ReOrderDetails reOrderDetails;
	private List<ReOrderProductDetails> reOrderProductDetailsList;
	public ReOrderDetails getReOrderDetails() {
		return reOrderDetails;
	}
	public void setReOrderDetails(ReOrderDetails reOrderDetails) {
		this.reOrderDetails = reOrderDetails;
	}
	public List<ReOrderProductDetails> getReOrderProductDetailsList() {
		return reOrderProductDetailsList;
	}
	public void setReOrderProductDetailsList(List<ReOrderProductDetails> reOrderProductDetailsList) {
		this.reOrderProductDetailsList = reOrderProductDetailsList;
	}
	@Override
	public String toString() {
		return "ReOrderDetailsAndProductDetailsRespose [reOrderDetails=" + reOrderDetails
				+ ", reOrderProductDetailsList=" + reOrderProductDetailsList + "]";
	}
	
	

}
