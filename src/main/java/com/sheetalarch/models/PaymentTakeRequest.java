package com.sheetalarch.models;

public class PaymentTakeRequest {

	private String orderId;
	private String cashCheckStatus;
	private String fullPartialPayment;
	private String dueDate;
	private String bankName;
	private String checkNo;
	private String checkDate;
	private double paidAmount;
	private String signature;
	private String instantPayment;
	
	
	public String getOrderId() {
		return orderId;
	}
	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}
	public String getCashCheckStatus() {
		return cashCheckStatus;
	}
	public void setCashCheckStatus(String cashCheckStatus) {
		this.cashCheckStatus = cashCheckStatus;
	}
	public String getFullPartialPayment() {
		return fullPartialPayment;
	}
	public void setFullPartialPayment(String fullPartialPayment) {
		this.fullPartialPayment = fullPartialPayment;
	}
	public String getDueDate() {
		return dueDate;
	}
	public void setDueDate(String dueDate) {
		this.dueDate = dueDate;
	}
	public String getBankName() {
		return bankName;
	}
	public void setBankName(String bankName) {
		this.bankName = bankName;
	}
	public String getCheckNo() {
		return checkNo;
	}
	public void setCheckNo(String checkNo) {
		this.checkNo = checkNo;
	}
	public String getCheckDate() {
		return checkDate;
	}
	public void setCheckDate(String checkDate) {
		this.checkDate = checkDate;
	}
	public double getPaidAmount() {
		return paidAmount;
	}
	public void setPaidAmount(double paidAmount) {
		this.paidAmount = paidAmount;
	}
	public String getSignature() {
		return signature;
	}
	public void setSignature(String signature) {
		this.signature = signature;
	}
	public String getInstantPayment() {
		return instantPayment;
	}
	public void setInstantPayment(String instantPayment) {
		this.instantPayment = instantPayment;
	}
	@Override
	public String toString() {
		return "PaymentTakeRequest [orderId=" + orderId + ", cashCheckStatus=" + cashCheckStatus
				+ ", fullPartialPayment=" + fullPartialPayment + ", dueDate=" + dueDate + ", bankName=" + bankName
				+ ", checkNo=" + checkNo + ", checkDate=" + checkDate + ", paidAmount=" + paidAmount + ", signature="
				+ signature + ", instantPayment=" + instantPayment + "]";
	}
	
	
	
}
