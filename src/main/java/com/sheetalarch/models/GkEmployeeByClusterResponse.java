package com.sheetalarch.models;

import com.sheetalarch.entity.Employee;

public class GkEmployeeByClusterResponse extends BaseDomain{
	
	private Employee gkEmployee;

	public Employee getGkEmployee() {
		return gkEmployee;
	}

	public void setGkEmployee(Employee gkEmployee) {
		this.gkEmployee = gkEmployee;
	}

	@Override
	public String toString() {
		return "GkEmployeeByClusterResponse [gkEmployee=" + gkEmployee + "]";
	}
	
	

}
