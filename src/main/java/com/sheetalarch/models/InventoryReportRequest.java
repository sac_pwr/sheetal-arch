package com.sheetalarch.models;

import java.util.Date;

public class InventoryReportRequest extends BaseDomain{
	
	
	private String fromDate;
	private String toDate;
	private String range;
	public String getFromDate() {
		return fromDate;
	}
	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}
	public String getToDate() {
		return toDate;
	}
	public void setToDate(String toDate) {
		this.toDate = toDate;
	}
	public String getRange() {
		return range;
	}
	public void setRange(String range) {
		this.range = range;
	}
	@Override
	public String toString() {
		return "InventoryReportRequest [fromDate=" + fromDate + ", toDate=" + toDate + ", range=" + range + "]";
	}
	

}