package com.sheetalarch.models;

import java.util.List;

import com.sheetalarch.entity.OrderDetails;
import com.sheetalarch.entity.OrderProductDetails;

public class OrderIssueRequest {
	
	private long dispatchedId;
	private long employeeIdGk;
	public long getDispatchedId() {
		return dispatchedId;
	}
	public void setDispatchedId(long dispatchedId) {
		this.dispatchedId = dispatchedId;
	}
	public long getEmployeeIdGk() {
		return employeeIdGk;
	}
	public void setEmployeeIdGk(long employeeIdGk) {
		this.employeeIdGk = employeeIdGk;
	}
	@Override
	public String toString() {
		return "OrderIssueRequest [dispatchedId=" + dispatchedId + ", employeeIdGk=" + employeeIdGk + "]";
	}
	
	
	

}
