package com.sheetalarch.models;

import java.util.List;

import com.sheetalarch.entity.Inventory;
import com.sheetalarch.entity.InventoryDetails;



public class InventorySaveRequestModel extends BaseDomain {

	private Inventory inventory;
	private List<InventoryDetails> inventoryDetails;
	private String paymentDate;
	
	public Inventory getInventory() {
		return inventory;
	}
	public void setInventory(Inventory inventory) {
		this.inventory = inventory;
	}
	public List<InventoryDetails> getInventoryDetails() {
		return inventoryDetails;
	}
	public void setInventoryDetails(List<InventoryDetails> inventoryDetails) {
		this.inventoryDetails = inventoryDetails;
	}
	public String getPaymentDate() {
		return paymentDate;
	}
	public void setPaymentDate(String paymentDate) {
		this.paymentDate = paymentDate;
	}
	@Override
	public String toString() {
		return "InventorySaveRequestModel [inventory=" + inventory + ", inventoryDetails=" + inventoryDetails
				+ ", paymentDate=" + paymentDate + "]";
	}
	
	
	
}
