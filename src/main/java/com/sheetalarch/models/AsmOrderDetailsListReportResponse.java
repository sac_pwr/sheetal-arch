package com.sheetalarch.models;

import java.util.List;

import com.sheetalarch.entity.OrderDetails;
import com.sheetalarch.entity.ReOrderDetails;

public class AsmOrderDetailsListReportResponse extends BaseDomain {
	
	private List<OrderDetails> orderDetailsList;
	private  String salesmanName;
	private String mobileNo;
	private String routeNo;
	private List<ReOrderDetails> reOrderDetailsList;
	
	
	public List<OrderDetails> getOrderDetailsList() {
		return orderDetailsList;
	}
	public void setOrderDetailsList(List<OrderDetails> orderDetailsList) {
		this.orderDetailsList = orderDetailsList;
	}
	public String getSalesmanName() {
		return salesmanName;
	}
	public void setSalesmanName(String salesmanName) {
		this.salesmanName = salesmanName;
	}
	public String getMobileNo() {
		return mobileNo;
	}
	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}
	public String getRouteNo() {
		return routeNo;
	}
	public void setRouteNo(String routeNo) {
		this.routeNo = routeNo;
	}
	public List<ReOrderDetails> getReOrderDetailsList() {
		return reOrderDetailsList;
	}
	public void setReOrderDetailsList(List<ReOrderDetails> reOrderDetailsList) {
		this.reOrderDetailsList = reOrderDetailsList;
	}
	@Override
	public String toString() {
		return "AsmOrderDetailsListReportResponse [orderDetailsList=" + orderDetailsList + ", salesmanName="
				+ salesmanName + ", mobileNo=" + mobileNo + ", routeNo=" + routeNo + ", reOrderDetailsList="
				+ reOrderDetailsList + "]";
	}
	
	

}
