package com.sheetalarch.models;

import com.sheetalarch.entity.Employee;

public class LoginResponse extends BaseDomain{
	public Employee employee;

	public Employee getEmployee() {
		return employee;
	}

	public void setEmployee(Employee employee) {
		this.employee = employee;
	}

	@Override
	public String toString() {
		return "LoginResponse [employee=" + employee + "]";
	}
}
