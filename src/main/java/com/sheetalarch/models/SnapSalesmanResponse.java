package com.sheetalarch.models;

public class SnapSalesmanResponse extends BaseDomain {

	private long totalNoOfOrder;
	private long totalNoOfOrderDelivered;
	private long totalCancelOrder;
	private long totalNoOfReOrder;
	private double totalCollectionAmount;
	private long totalNoOfQtyReceivedFromGk;
	private long totalNoOfQtyDelivered;
	private long remainingQty;
	
	
	public long getTotalNoOfOrder() {
		return totalNoOfOrder;
	}
	public void setTotalNoOfOrder(long totalNoOfOrder) {
		this.totalNoOfOrder = totalNoOfOrder;
	}
	public long getTotalNoOfOrderDelivered() {
		return totalNoOfOrderDelivered;
	}
	public void setTotalNoOfOrderDelivered(long totalNoOfOrderDelivered) {
		this.totalNoOfOrderDelivered = totalNoOfOrderDelivered;
	}
	public long getTotalCancelOrder() {
		return totalCancelOrder;
	}
	public void setTotalCancelOrder(long totalCancelOrder) {
		this.totalCancelOrder = totalCancelOrder;
	}
	public long getTotalNoOfReOrder() {
		return totalNoOfReOrder;
	}
	public void setTotalNoOfReOrder(long totalNoOfReOrder) {
		this.totalNoOfReOrder = totalNoOfReOrder;
	}
	public double getTotalCollectionAmount() {
		return totalCollectionAmount;
	}
	public void setTotalCollectionAmount(double totalCollectionAmount) {
		this.totalCollectionAmount = totalCollectionAmount;
	}
	public long getTotalNoOfQtyReceivedFromGk() {
		return totalNoOfQtyReceivedFromGk;
	}
	public void setTotalNoOfQtyReceivedFromGk(long totalNoOfQtyReceivedFromGk) {
		this.totalNoOfQtyReceivedFromGk = totalNoOfQtyReceivedFromGk;
	}
	public long getTotalNoOfQtyDelivered() {
		return totalNoOfQtyDelivered;
	}
	public void setTotalNoOfQtyDelivered(long totalNoOfQtyDelivered) {
		this.totalNoOfQtyDelivered = totalNoOfQtyDelivered;
	}
	public long getRemainingQty() {
		return remainingQty;
	}
	public void setRemainingQty(long remainingQty) {
		this.remainingQty = remainingQty;
	}
	@Override
	public String toString() {
		return "SnapSalesmanResponse [totalNoOfOrder=" + totalNoOfOrder + ", totalNoOfOrderDelivered="
				+ totalNoOfOrderDelivered + ", totalCancelOrder=" + totalCancelOrder + ", totalNoOfReOrder="
				+ totalNoOfReOrder + ", totalCollectionAmount=" + totalCollectionAmount
				+ ", totalNoOfQtyReceivedFromGk=" + totalNoOfQtyReceivedFromGk + ", totalNoOfQtyDelivered="
				+ totalNoOfQtyDelivered + ", remainingQty=" + remainingQty + "]";
	}
	
	
	
}
