package com.sheetalarch.models;

import java.util.List;

import com.sheetalarch.entity.Booth;
import com.sheetalarch.entity.OrderDetails;
import com.sheetalarch.entity.OrderProductDetails;

public class ReOrderPaymentTakeRequest extends BaseDomain{
	
	
	private String reOrderId;
	private String cashCheckStatus;
	private String fullPartialPayment;
	private String dueDate;
	private String bankName;
	private String checkNo;
	private String checkDate;
	private double paidAmount;
	private String signature;
	private String instantPayment;
	
	private OrderDetails orderDetails;
	private List<OrderProductDetails> orderProductDetails;
	private Booth booth;
	public String getReOrderId() {
		return reOrderId;
	}
	public void setReOrderId(String reOrderId) {
		this.reOrderId = reOrderId;
	}
	public String getCashCheckStatus() {
		return cashCheckStatus;
	}
	public void setCashCheckStatus(String cashCheckStatus) {
		this.cashCheckStatus = cashCheckStatus;
	}
	public String getFullPartialPayment() {
		return fullPartialPayment;
	}
	public void setFullPartialPayment(String fullPartialPayment) {
		this.fullPartialPayment = fullPartialPayment;
	}
	public String getDueDate() {
		return dueDate;
	}
	public void setDueDate(String dueDate) {
		this.dueDate = dueDate;
	}
	public String getBankName() {
		return bankName;
	}
	public void setBankName(String bankName) {
		this.bankName = bankName;
	}
	public String getCheckNo() {
		return checkNo;
	}
	public void setCheckNo(String checkNo) {
		this.checkNo = checkNo;
	}
	public String getCheckDate() {
		return checkDate;
	}
	public void setCheckDate(String checkDate) {
		this.checkDate = checkDate;
	}
	public double getPaidAmount() {
		return paidAmount;
	}
	public void setPaidAmount(double paidAmount) {
		this.paidAmount = paidAmount;
	}
	public String getSignature() {
		return signature;
	}
	public void setSignature(String signature) {
		this.signature = signature;
	}
	public String getInstantPayment() {
		return instantPayment;
	}
	public void setInstantPayment(String instantPayment) {
		this.instantPayment = instantPayment;
	}
	public OrderDetails getOrderDetails() {
		return orderDetails;
	}
	public void setOrderDetails(OrderDetails orderDetails) {
		this.orderDetails = orderDetails;
	}
	public List<OrderProductDetails> getOrderProductDetails() {
		return orderProductDetails;
	}
	public void setOrderProductDetails(List<OrderProductDetails> orderProductDetails) {
		this.orderProductDetails = orderProductDetails;
	}
	public Booth getBooth() {
		return booth;
	}
	public void setBooth(Booth booth) {
		this.booth = booth;
	}
	@Override
	public String toString() {
		return "ReOrderPaymentTakeRequest [reOrderId=" + reOrderId + ", cashCheckStatus=" + cashCheckStatus
				+ ", fullPartialPayment=" + fullPartialPayment + ", dueDate=" + dueDate + ", bankName=" + bankName
				+ ", checkNo=" + checkNo + ", checkDate=" + checkDate + ", paidAmount=" + paidAmount + ", signature="
				+ signature + ", instantPayment=" + instantPayment + ", orderDetails=" + orderDetails
				+ ", orderProductDetails=" + orderProductDetails + ", booth=" + booth + "]";
	}
	
	

}
