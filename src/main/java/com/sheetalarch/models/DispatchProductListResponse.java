package com.sheetalarch.models;

import java.util.List;

import com.sheetalarch.entity.OrderProductDetails;

public class DispatchProductListResponse extends BaseDomain{

	private List<DispatchProductNameAndQtyListResponse> dispatchProductNameAndQtyListResponses;
	private String GateKeeperName;
	private String MobileNo;
	public List<DispatchProductNameAndQtyListResponse> getDispatchProductNameAndQtyListResponses() {
		return dispatchProductNameAndQtyListResponses;
	}
	public void setDispatchProductNameAndQtyListResponses(
			List<DispatchProductNameAndQtyListResponse> dispatchProductNameAndQtyListResponses) {
		this.dispatchProductNameAndQtyListResponses = dispatchProductNameAndQtyListResponses;
	}
	public String getGateKeeperName() {
		return GateKeeperName;
	}
	public void setGateKeeperName(String gateKeeperName) {
		GateKeeperName = gateKeeperName;
	}
	public String getMobileNo() {
		return MobileNo;
	}
	public void setMobileNo(String mobileNo) {
		MobileNo = mobileNo;
	}
	@Override
	public String toString() {
		return "DispatchProductListResponse [dispatchProductNameAndQtyListResponses="
				+ dispatchProductNameAndQtyListResponses + ", GateKeeperName=" + GateKeeperName + ", MobileNo="
				+ MobileNo + "]";
	}
	
	
	
	
}
