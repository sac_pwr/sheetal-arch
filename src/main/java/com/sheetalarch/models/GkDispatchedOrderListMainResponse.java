package com.sheetalarch.models;

import java.util.List;

public class GkDispatchedOrderListMainResponse extends BaseDomain {
	
	private List<GkDispatchedOrderListResponse> gkDispatchedOrderListResponses;

	public List<GkDispatchedOrderListResponse> getGkDispatchedOrderListResponses() {
		return gkDispatchedOrderListResponses;
	}

	public void setGkDispatchedOrderListResponses(List<GkDispatchedOrderListResponse> gkDispatchedOrderListResponses) {
		this.gkDispatchedOrderListResponses = gkDispatchedOrderListResponses;
	}

	@Override
	public String toString() {
		return "GkDispatchedOrderListMainResponse [gkDispatchedOrderListResponses=" + gkDispatchedOrderListResponses
				+ "]";
	}
	
	

}
