package com.sheetalarch.models;

public class OrderListGKRequest {
	
	private long employeeId;
	private String orderScheduling;
	private long clusterId;
	private long asmEmployeeId;
	
	
	public long getEmployeeId() {
		return employeeId;
	}
	public void setEmployeeId(long employeeId) {
		this.employeeId = employeeId;
	}
	public String getOrderScheduling() {
		return orderScheduling;
	}
	public void setOrderScheduling(String orderScheduling) {
		this.orderScheduling = orderScheduling;
	}
	public long getClusterId() {
		return clusterId;
	}
	public void setClusterId(long clusterId) {
		this.clusterId = clusterId;
	}
	public long getAsmEmployeeId() {
		return asmEmployeeId;
	}
	public void setAsmEmployeeId(long asmEmployeeId) {
		this.asmEmployeeId = asmEmployeeId;
	}
	@Override
	public String toString() {
		return "OrderListGKRequest [employeeId=" + employeeId + ", orderScheduling=" + orderScheduling + ", clusterId="
				+ clusterId + ", asmEmployeeId=" + asmEmployeeId + "]";
	}
	
	

}
