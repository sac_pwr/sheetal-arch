package com.sheetalarch.models;

import java.util.List;

import com.sheetalarch.entity.OrderDetails;
import com.sheetalarch.entity.OrderProductDetails;



public class OrderRequest {
	
	private OrderDetails orderDetails;
	
	private List<OrderProductDetails> orderProductDetailList;
	
	private String returnProductOrderId;

	public OrderDetails getOrderDetails() {
		return orderDetails;
	}

	public void setOrderDetails(OrderDetails orderDetails) {
		this.orderDetails = orderDetails;
	}

	public List<OrderProductDetails> getOrderProductDetailList() {
		return orderProductDetailList;
	}

	public void setOrderProductDetailList(List<OrderProductDetails> orderProductDetailList) {
		this.orderProductDetailList = orderProductDetailList;
	}

	public String getReturnProductOrderId() {
		return returnProductOrderId;
	}

	public void setReturnProductOrderId(String returnProductOrderId) {
		this.returnProductOrderId = returnProductOrderId;
	}

	@Override
	public String toString() {
		return "OrderRequest [orderDetails=" + orderDetails + ", orderProductDetailList=" + orderProductDetailList
				+ ", returnProductOrderId=" + returnProductOrderId + "]";
	}

	

}
