package com.sheetalarch.models;

import java.util.List;

public class GkDispatchProductReportMainResponse extends BaseDomain{
	
	private List<GkDispatchProductReportResponse> gkDispatchProductReportResponses;

	public List<GkDispatchProductReportResponse> getGkDispatchProductReportResponses() {
		return gkDispatchProductReportResponses;
	}

	public void setGkDispatchProductReportResponses(
			List<GkDispatchProductReportResponse> gkDispatchProductReportResponses) {
		this.gkDispatchProductReportResponses = gkDispatchProductReportResponses;
	}

	@Override
	public String toString() {
		return "GkDispatchProductReportMainResponse [gkDispatchProductReportResponses="
				+ gkDispatchProductReportResponses + "]";
	}
	
	
	

}
