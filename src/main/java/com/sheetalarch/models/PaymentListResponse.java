package com.sheetalarch.models;

import java.util.List;

import com.sheetalarch.entity.Payment;

public class PaymentListResponse extends  BaseDomain{
	
	private List<Payment> paymentList;

	public List<Payment> getPaymentList() {
		return paymentList;
	}

	public void setPaymentList(List<Payment> paymentList) {
		this.paymentList = paymentList;
	}

	@Override
	public String toString() {
		return "PaymentListResponse [paymentList=" + paymentList + "]";
	}
	
	

}
