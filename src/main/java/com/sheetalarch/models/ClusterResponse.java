package com.sheetalarch.models;

import java.util.List;

import com.sheetalarch.entity.Cluster;

public class ClusterResponse extends BaseDomain {
	
	private Cluster cluster;
	private List<Cluster> clusterList;
	public Cluster getCluster() {
		return cluster;
	}
	public void setCluster(Cluster cluster) {
		this.cluster = cluster;
	}
	public List<Cluster> getClusterList() {
		return clusterList;
	}
	public void setClusterList(List<Cluster> clusterList) {
		this.clusterList = clusterList;
	}
	@Override
	public String toString() {
		return "ClusterResponse [cluster=" + cluster + ", clusterList=" + clusterList + "]";
	}

	

}
