package com.sheetalarch.models;

import com.sheetalarch.entity.Booth;

public class BoothResponse extends BaseDomain {

	private Booth booth;

	public Booth getBooth() {
		return booth;
	}

	public void setBooth(Booth booth) {
		this.booth = booth;
	}

	@Override
	public String toString() {
		return "BoothResponse [booth=" + booth + "]";
	}
	
	
	
}
