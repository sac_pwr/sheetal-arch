package com.sheetalarch.models;

import java.util.List;

import com.sheetalarch.entity.DispatchOrderProductDetails;
import com.sheetalarch.entity.Route;

public class GkDispatchedProductListResponse extends BaseDomain{
	
	private List<DispatchOrderProductDetails> dispatchOrderProductDetails;
	private String empName;
	private String cluster;
	private String mobNo;
	private List<Route>  routes;
	private double totalAmount;
	private long totalQty;
	
	
	public List<DispatchOrderProductDetails> getDispatchOrderProductDetails() {
		return dispatchOrderProductDetails;
	}
	public void setDispatchOrderProductDetails(List<DispatchOrderProductDetails> dispatchOrderProductDetails) {
		this.dispatchOrderProductDetails = dispatchOrderProductDetails;
	}
	public String getEmpName() {
		return empName;
	}
	public void setEmpName(String empName) {
		this.empName = empName;
	}
	public String getCluster() {
		return cluster;
	}
	public void setCluster(String cluster) {
		this.cluster = cluster;
	}
	public String getMobNo() {
		return mobNo;
	}
	public void setMobNo(String mobNo) {
		this.mobNo = mobNo;
	}
	public List<Route> getRoutes() {
		return routes;
	}
	public void setRoutes(List<Route> routes) {
		this.routes = routes;
	}
	public double getTotalAmount() {
		return totalAmount;
	}
	public void setTotalAmount(double totalAmount) {
		this.totalAmount = totalAmount;
	}
	public long getTotalQty() {
		return totalQty;
	}
	public void setTotalQty(long totalQty) {
		this.totalQty = totalQty;
	}
	@Override
	public String toString() {
		return "GkDispatchedProductListResponse [dispatchOrderProductDetails=" + dispatchOrderProductDetails
				+ ", empName=" + empName + ", cluster=" + cluster + ", mobNo=" + mobNo + ", routes=" + routes
				+ ", totalAmount=" + totalAmount + ", totalQty=" + totalQty + "]";
	}
	
	

}
