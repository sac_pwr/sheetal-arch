package com.sheetalarch.models;

public class PaymentNotReceiveRequest {
	
	private String orderId;
	private String nextDueDate;
	
	public String getOrderId() {
		return orderId;
	}
	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}
	public String getNextDueDate() {
		return nextDueDate;
	}
	public void setNextDueDate(String nextDueDate) {
		this.nextDueDate = nextDueDate;
	}
	@Override
	public String toString() {
		return "PaymentNotReceiveRequest [orderId=" + orderId + ", nextDueDate=" + nextDueDate + "]";
	}
	
	

}
