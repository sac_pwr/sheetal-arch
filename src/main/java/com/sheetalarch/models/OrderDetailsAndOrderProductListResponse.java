package com.sheetalarch.models;

import java.util.List;

import com.sheetalarch.entity.Booth;
import com.sheetalarch.entity.OrderDetails;
import com.sheetalarch.entity.OrderProductDetails;

public class OrderDetailsAndOrderProductListResponse extends BaseDomain {
	
	
	private OrderDetails orderDetails;
	private String mobileNumber;
	
	private List<OrderProductDetails> orderProductDetailsList;
	private Booth booth;

	public OrderDetails getOrderDetails() {
		return orderDetails;
	}
	public void setOrderDetails(OrderDetails orderDetails) {
		this.orderDetails = orderDetails;
	}
	public String getMobileNumber() {
		return mobileNumber;
	}
	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}
	public List<OrderProductDetails> getOrderProductDetailsList() {
		return orderProductDetailsList;
	}
	public void setOrderProductDetailsList(List<OrderProductDetails> orderProductDetailsList) {
		this.orderProductDetailsList = orderProductDetailsList;
	}
	public Booth getBooth() {
		return booth;
	}
	public void setBooth(Booth booth) {
		this.booth = booth;
	}
	@Override
	public String toString() {
		return "OrderDetailsAndOrderProductListResponse [orderDetails=" + orderDetails + ", mobileNumber="
				+ mobileNumber + ", orderProductDetailsList=" + orderProductDetailsList + ", booth=" + booth + "]";
	}

}
