package com.sheetalarch.models;

public class OrderPaymentPeriodDaysRequest extends BaseDomain {

	private String orderId;
	
	private String deliveryDate;
	public String getOrderId() {
		return orderId;
	}
	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}
	public String getDeliveryDate() {
		return deliveryDate;
	}
	public void setDeliveryDate(String deliveryDate) {
		this.deliveryDate = deliveryDate;
	}
	@Override
	public String toString() {
		return "OrderPaymentPeriodDaysRequest [orderId=" + orderId + ", deliveryDate=" + deliveryDate + "]";
	}
	

	
}
