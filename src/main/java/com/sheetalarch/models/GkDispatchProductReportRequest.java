package com.sheetalarch.models;

public class GkDispatchProductReportRequest {

	private String fromDate;
	private String toDate;
	private String range;
	private  long clusterId;
	private long gkEmployeeId;
	
	public String getFromDate() {
		return fromDate;
	}
	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}
	public String getToDate() {
		return toDate;
	}
	public void setToDate(String toDate) {
		this.toDate = toDate;
	}
	public String getRange() {
		return range;
	}
	public void setRange(String range) {
		this.range = range;
	}
	public long getClusterId() {
		return clusterId;
	}
	public void setClusterId(long clusterId) {
		this.clusterId = clusterId;
	}
	public long getGkEmployeeId() {
		return gkEmployeeId;
	}
	public void setGkEmployeeId(long gkEmployeeId) {
		this.gkEmployeeId = gkEmployeeId;
	}
	@Override
	public String toString() {
		return "GkDispatchProductReportRequest [fromDate=" + fromDate + ", toDate=" + toDate + ", range=" + range
				+ ", clusterId=" + clusterId + ", gkEmployeeId=" + gkEmployeeId + "]";
	}
	
	
	
}
