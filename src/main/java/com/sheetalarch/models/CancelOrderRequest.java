package com.sheetalarch.models;

public class CancelOrderRequest {
	
	 private String orderId;
	 private long employeeId;
	 private String cancelReason;
	 
	 
	public String getOrderId() {
		return orderId;
	}
	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}
	public long getEmployeeId() {
		return employeeId;
	}
	public void setEmployeeId(long employeeId) {
		this.employeeId = employeeId;
	}
	public String getCancelReason() {
		return cancelReason;
	}
	public void setCancelReason(String cancelReason) {
		this.cancelReason = cancelReason;
	}
	@Override
	public String toString() {
		return "CancelOrderRequest [orderId=" + orderId + ", employeeId=" + employeeId + ", cancelReason="
				+ cancelReason + "]";
	}
	 
	 

}
