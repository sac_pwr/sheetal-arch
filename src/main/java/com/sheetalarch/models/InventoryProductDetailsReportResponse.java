package com.sheetalarch.models;

import java.util.List;

import com.sheetalarch.entity.Inventory;
import com.sheetalarch.entity.InventoryDetails;

public class InventoryProductDetailsReportResponse extends BaseDomain {
	
	private Inventory inventory;
	
	private List<InventoryDetails> inventoryDetails;

	public Inventory getInventory() {
		return inventory;
	}

	public void setInventory(Inventory inventory) {
		this.inventory = inventory;
	}

	public List<InventoryDetails> getInventoryDetails() {
		return inventoryDetails;
	}

	public void setInventoryDetails(List<InventoryDetails> inventoryDetails) {
		this.inventoryDetails = inventoryDetails;
	}

	@Override
	public String toString() {
		return "InventoryProductDetailsReportResponse [inventory=" + inventory + ", inventoryDetails="
				+ inventoryDetails + "]";
	}
	
	
	
	

}
