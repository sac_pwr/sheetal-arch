package com.sheetalarch.models;

public class FetchSnapShotForSalesman extends BaseDomain {

	private SnapSalesmanResponse snapSalesmanResponse;

	public SnapSalesmanResponse getSnapSalesmanResponse() {
		return snapSalesmanResponse;
	}

	public void setSnapSalesmanResponse(SnapSalesmanResponse snapSalesmanResponse) {
		this.snapSalesmanResponse = snapSalesmanResponse;
	}

	@Override
	public String toString() {
		return "FetchSnapShotForSalesman [snapSalesmanResponse=" + snapSalesmanResponse + "]";
	}
	
}
