package com.sheetalarch.models;

import java.util.List;

import com.sheetalarch.entity.Booth;

public class BoothListResponse extends BaseDomain{
	
	private List<Booth> boothList;

	public List<Booth> getBoothList() {
		return boothList;
	}

	public void setBoothList(List<Booth> boothList) {
		this.boothList = boothList;
	}

	@Override
	public String toString() {
		return "BoothListResponse [boothList=" + boothList + "]";
	}
	
	
	

}
