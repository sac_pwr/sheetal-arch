package com.sheetalarch.models;

import java.util.List;

import com.sheetalarch.entity.DispatchOrderDetails;
import com.sheetalarch.entity.Route;

public class GkDispatchProductReportResponse extends BaseDomain{
	
	private String salesmanName;
	private long salesmanEmployeeId;
	private List<Route> routeList;
	private long totalQty;
	private double totalAmt;
	private List<DispatchOrderDetails> dispatchOrderDetails;
	
	
	public String getSalesmanName() {
		return salesmanName;
	}
	public void setSalesmanName(String salesmanName) {
		this.salesmanName = salesmanName;
	}
	public long getSalesmanEmployeeId() {
		return salesmanEmployeeId;
	}
	public void setSalesmanEmployeeId(long salesmanEmployeeId) {
		this.salesmanEmployeeId = salesmanEmployeeId;
	}
	public List<Route> getRouteList() {
		return routeList;
	}
	public void setRouteList(List<Route> routeList) {
		this.routeList = routeList;
	}
	public long getTotalQty() {
		return totalQty;
	}
	public void setTotalQty(long totalQty) {
		this.totalQty = totalQty;
	}
	public double getTotalAmt() {
		return totalAmt;
	}
	public void setTotalAmt(double totalAmt) {
		this.totalAmt = totalAmt;
	}
	public List<DispatchOrderDetails> getDispatchOrderDetails() {
		return dispatchOrderDetails;
	}
	public void setDispatchOrderDetails(List<DispatchOrderDetails> dispatchOrderDetails) {
		this.dispatchOrderDetails = dispatchOrderDetails;
	}
	@Override
	public String toString() {
		return "GkDispatchProductReportResponse [salesmanName=" + salesmanName + ", salesmanEmployeeId="
				+ salesmanEmployeeId + ", routeList=" + routeList + ", totalQty=" + totalQty + ", totalAmt=" + totalAmt
				+ ", dispatchOrderDetails=" + dispatchOrderDetails + "]";
	}
	
	
}
