package com.sheetalarch.models;

import java.util.List;

import com.sheetalarch.entity.DispatchOrderProductDetails;

public class GkSnapResponse extends  BaseDomain {

	private List<DispatchOrderProductDetails> dispatchOrderProductDetails;

	public List<DispatchOrderProductDetails> getDispatchOrderProductDetails() {
		return dispatchOrderProductDetails;
	}

	public void setDispatchOrderProductDetails(List<DispatchOrderProductDetails> dispatchOrderProductDetails) {
		this.dispatchOrderProductDetails = dispatchOrderProductDetails;
	}

	@Override
	public String toString() {
		return "GkSnapResponse [dispatchOrderProductDetails=" + dispatchOrderProductDetails + "]";
	}
	
	
}
