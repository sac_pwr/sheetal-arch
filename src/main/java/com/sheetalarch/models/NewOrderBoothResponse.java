package com.sheetalarch.models;

import java.util.List;

import com.sheetalarch.entity.Booth;
import com.sheetalarch.entity.Brand;
import com.sheetalarch.entity.Categories;

	public class NewOrderBoothResponse extends BaseDomain{
		
		private Booth booth;
		private List<Brand> brandList;
		private List<Categories> categoriesList;
		
		
		public Booth getBooth() {
			return booth;
		}
		public void setBooth(Booth booth) {
			this.booth = booth;
		}
		public List<Brand> getBrandList() {
			return brandList;
		}
		public void setBrandList(List<Brand> brandList) {
			this.brandList = brandList;
		}
		public List<Categories> getCategoriesList() {
			return categoriesList;
		}
		public void setCategoriesList(List<Categories> categoriesList) {
			this.categoriesList = categoriesList;
		}
		@Override
		public String toString() {
			return "NewOrderBoothResponse [booth=" + booth + ", brandList=" + brandList + ", categoriesList=" + categoriesList
					+ "]";
		}




}
