package com.sheetalarch.models;

import java.util.List;

import com.sheetalarch.entity.Product;
import com.sheetalarch.entity.SupplierProductList;

public class ProductListResponse extends BaseDomain{

	private List<Product> productsList;
	private List<SupplierProductList> supplierProductList;
	public List<Product> getProductsList() {
		return productsList;
	}
	public void setProductsList(List<Product> productsList) {
		this.productsList = productsList;
	}
	public List<SupplierProductList> getSupplierProductList() {
		return supplierProductList;
	}
	public void setSupplierProductList(List<SupplierProductList> supplierProductList) {
		this.supplierProductList = supplierProductList;
	}
	@Override
	public String toString() {
		return "ProductListResponse [productsList=" + productsList + ", supplierProductList=" + supplierProductList
				+ "]";
	}

	
	
}
