package com.sheetalarch.models;

import java.util.List;

import com.sheetalarch.entity.Booth;
import com.sheetalarch.entity.OrderDetails;
import com.sheetalarch.entity.OrderProductDetails;

public class ReOrderDeliveryRequest {

	private OrderDetails orderDetails;
	private List<OrderProductDetails> orderProductDetailsList;
	private Booth booth;
	public OrderDetails getOrderDetails() {
		return orderDetails;
	}
	public void setOrderDetails(OrderDetails orderDetails) {
		this.orderDetails = orderDetails;
	}
	public List<OrderProductDetails> getOrderProductDetailsList() {
		return orderProductDetailsList;
	}
	public void setOrderProductDetailsList(List<OrderProductDetails> orderProductDetailsList) {
		this.orderProductDetailsList = orderProductDetailsList;
	}
	public Booth getBooth() {
		return booth;
	}
	public void setBooth(Booth booth) {
		this.booth = booth;
	}
	@Override
	public String toString() {
		return "ReOrderDeliveryRequest [orderDetails=" + orderDetails + ", orderProductDetailsList="
				+ orderProductDetailsList + ", booth=" + booth + "]";
	}
	
	

}
