package com.sheetalarch.models;

import java.util.List;

import com.sheetalarch.entity.Route;

public class RouteListResponse extends BaseDomain{
	
	private List<Route> routeList;

	public List<Route> getRouteList() {
		return routeList;
	}

	public void setRouteList(List<Route> routeList) {
		this.routeList = routeList;
	}

	@Override
	public String toString() {
		return "RouteListResponse [routeList=" + routeList + "]";
	}
	
	

}
