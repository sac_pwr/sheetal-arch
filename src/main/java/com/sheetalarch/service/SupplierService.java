package com.sheetalarch.service;

import java.util.List;

import com.sheetalarch.admin.models.FetchSupplierList;
import com.sheetalarch.entity.Supplier;
import com.sheetalarch.entity.SupplierProductList;

public interface SupplierService {
	
	
	public List<Supplier> fetchSupplierList();
	public List<SupplierProductList> fetchProductBySupplierId(String supplierId);
	

	//webApp
	public List<Supplier> fetchSupplierForWebAppList();
	public Supplier fetchSupplier(String supplierId);
	public Supplier saveSupplier(Supplier supplier);
	public Supplier updateSupplier(Supplier supplier);
	public Supplier saveSupplierProductList(String productIdList,String supplierId);
	public List<FetchSupplierList> fetchSupplierForWebApp();
	public List<SupplierProductList> fetchSupplierProductListBySupplierIdForWebApp(String supplierId);
	public List<SupplierProductList> makeSupplierProductListNullForWebApp(List<SupplierProductList> supplierProductList);
	public String getProductIdList(List<SupplierProductList> supplierProductList);
	public Supplier updateSupplierProductList(String productIdList,String supplierId);
	public double fetchSupplierProductRate(long productId,String supplierId);
	public SupplierProductList fetchSupplierByProductIdAndSupplierId(long productId,String supplierId);
	public String sendSMSTOSupplier(String supplierIds,String smsText);
}
