package com.sheetalarch.service;

import com.sheetalarch.entity.Employee;

public interface EmployeeService {

	public Employee login(String userId,String password);
	public boolean checkUserIdForWebApp(String userId);
	public boolean checkUserIdForUpdateForWebApp(String userId,String employeeId);
	public void saveForWebApp(Employee employee);
	public void updateForWebApp(Employee employee);
}
