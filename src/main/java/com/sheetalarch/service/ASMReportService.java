package com.sheetalarch.service;

import java.util.List;

import com.sheetalarch.entity.OrderDetails;
import com.sheetalarch.entity.Payment;
import com.sheetalarch.entity.ReOrderDetails;
import com.sheetalarch.entity.ReOrderProductDetails;
import com.sheetalarch.models.CancelAsmReportResponse;

public interface ASMReportService {

	// Cancel Report Of Asm
	public List<CancelAsmReportResponse> fetchSmAndCancelOrderByAsmId(long employeeId);
	public List<OrderDetails> todaysCancelOrderListByEmployeeId(long employeeId);
	
	// ReOrder Report Of Asm
	public List<ReOrderDetails> todaysReOrderListByEmployeeId(long employeeId);
	public List<CancelAsmReportResponse> fetchSmAndReOrderByAsmId(long employeeId);
	
	// here we are fetching ReorderObject and ReOrderProduct List by reOrderId
	public ReOrderDetails fetchReOrderDetailsByReOrderId(String reOrderId);
	public List<ReOrderProductDetails> fetchReOrderProductDetailsByReOrderId(String reOrderId);
	
	public List<OrderDetails> fetchOrderDetailsListByEmpIdAndDateRangeforASMMemberReport(long employeeId,
			String fromDate, String toDate, String range);
	public List<Payment> fetchPaymentListByEmployeeIdAndDateRange(long employeeId,
			String fromDate, String toDate, String range);


}
