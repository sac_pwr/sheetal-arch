package com.sheetalarch.service;

import java.util.List;

import com.sheetalarch.admin.models.CustomerRerportModel;
import com.sheetalarch.admin.models.ManageBoothListModel;
import com.sheetalarch.admin.models.SingleCustomerReport;
import com.sheetalarch.entity.Area;
import com.sheetalarch.entity.Booth;

public interface BoothService {

	public List<Booth> fetchBoothListByEmployeeId(long employeeId);
	public void saveBooth(Booth booth);
	public List<Booth> fetchBoothList();
	public Booth fetchBoothByBoothId(long boothId);
	public Booth fetchBoothByBoothNo(String boothNo);
	public void updateBooth(Booth booth);
	public List<ManageBoothListModel> fetchManageBoothList();
	public String sendSMSTOShops(String shopsId,String smsText);
	public SingleCustomerReport fetchSingleCustomerReport(String startDate,String endDate,String range,long boothId);
	public List<CustomerRerportModel> fetchCustomerReport(String startDate,String endDate,String range);
}
