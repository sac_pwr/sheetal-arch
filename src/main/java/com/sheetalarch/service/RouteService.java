/**
 * 
 */
package com.sheetalarch.service;

import java.util.List;

import com.sheetalarch.entity.Route;

/**
 * @author aNKIT
 *
 */
public interface RouteService {
	
	
	//webApp
	public List<Route> fetchRouteByAreaId(long areaId);
	public List<Route> fetchRouteList();
	public void saveRoute(Route route);
	public void updateRoute(Route route);
	public Route fetchRouteByRouteId(long routeId);
}
