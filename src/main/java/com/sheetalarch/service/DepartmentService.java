/**
 * 
 */
package com.sheetalarch.service;

import java.util.List;

import com.sheetalarch.entity.Department;

/**
 * @author aNKIT
 *
 */
public interface DepartmentService {
	public String saveDepartment(Department department);
	public String updateDepartment(Department department);
	public List<Department> fetchDepartmentList();
	public Department fetchDepartment(long departmentId);
}
