package com.sheetalarch.service;

import java.util.List;

import com.sheetalarch.entity.Categories;


public interface CategoriesService {
	//webapp

			public void saveCategoriesForWebApp(Categories categories);
			public void updateCategoriesForWebApp(Categories categories);
			public Categories fetchCategoriesForWebApp(long categoriesId);
			public List<Categories> fetchCategoriesListForWebApp();
}
