/**
 * 
 */
package com.sheetalarch.service;

import com.sheetalarch.entity.AdminLogin;

/**
 * @author aNKIT
 *
 */
public interface AdminLoginService {
	public AdminLogin loginValidate(String userName,String password);
}
