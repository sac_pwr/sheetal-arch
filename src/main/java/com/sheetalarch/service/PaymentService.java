package com.sheetalarch.service;

import java.util.List;

import com.sheetalarch.admin.models.CollectionReportMain;
import com.sheetalarch.entity.Payment;
import com.sheetalarch.models.OrderDetailsForPayment;
import com.sheetalarch.models.PaymentNotReceiveRequest;
import com.sheetalarch.models.PaymentTakeRequest;
import com.sheetalarch.models.ReOrderPaymentNotReceiveRequest;
import com.sheetalarch.models.ReOrderPaymentTakeRequest;

public interface PaymentService {
	
	public void updatePaymentStatusAndOrderStatus(PaymentNotReceiveRequest paymentNotReceiveRequest);
	public void savePaymentStatus(PaymentTakeRequest paymentTakeRequest);
	public void updatePaymentNotReceiveStatusOfReOrder(ReOrderPaymentNotReceiveRequest reOrderPaymentNotReceiveRequest);
	public void savePaymentStatusOfReOrder(ReOrderPaymentTakeRequest reOrderPaymentTakeRequest);
	
	//public List<Payment> fetchOrderDetaisForPaymentByEmployeeId(long employeeId,String paymentStatus);
	public List<OrderDetailsForPayment> fetchOrderListForPayment(long employeeId,String paymentStatus);
	public CollectionReportMain getCollectionReportDetails(String startDate, String endDate,String range);
}
