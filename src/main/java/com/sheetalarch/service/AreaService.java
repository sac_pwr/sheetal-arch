package com.sheetalarch.service;

import java.util.List;

import com.sheetalarch.entity.Area;
import com.sheetalarch.entity.Cluster;
import com.sheetalarch.entity.Employee;

public interface AreaService {


	public Cluster fetchClusterBySalesmanId(long employeeId);
	public Employee fetchGKEmployeeByClusterId(long clusterId);
	public Area fetchAreaBysalesmanId(long employeeId);
	public Cluster fetchClusterBtRouteId(long routeId);

	//webApp
	public List<Area> fetchAreaListByClusterId(long clusterId);
	public List<Area> fetchAreaList();
	public void saveArea(Area area);
	public void updateArea(Area area);
	public Area fetchAreaByAreaId(long areaId);
}
