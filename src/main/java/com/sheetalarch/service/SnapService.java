package com.sheetalarch.service;

import java.util.List;

import com.sheetalarch.entity.DispatchOrderProductDetails;
import com.sheetalarch.models.GkSnapProductResponse;
import com.sheetalarch.models.SnapSalesmanResponse;

public interface SnapService {

	public SnapSalesmanResponse fetchSnapShotDetailsForSalemanByEmployeeId(long employeeId);
	public List<DispatchOrderProductDetails> fetchDispatchedOrderProductDetailsByGkId(long employeeId);
	public List<GkSnapProductResponse> fetchSnapProductDetailsForGk(long employeeId) ;
}
