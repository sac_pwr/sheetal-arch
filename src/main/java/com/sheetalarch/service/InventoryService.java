package com.sheetalarch.service;

import java.util.List;

import com.sheetalarch.admin.models.InventoryProduct;
import com.sheetalarch.admin.models.InventoryReportView;
import com.sheetalarch.admin.models.PaymentDoInfo;
import com.sheetalarch.entity.Inventory;
import com.sheetalarch.entity.InventoryDetails;
import com.sheetalarch.entity.PaymentPaySupplier;
import com.sheetalarch.entity.SupplierProductList;
import com.sheetalarch.models.InventorySaveRequestModel;
import com.sheetalarch.models.ProductAddInventory;



public interface InventoryService {
	public List<ProductAddInventory> fetchProductListByBrandIdAndCategoryIdForApp(String supplierId,long categoryId, long brandId);
	public List<InventoryProduct> inventoryProductList();
	public List<SupplierProductList> fetchSupplierListByProductId(long productId);
	public void addInventory(Inventory inventory,String productIdList);
	public PaymentDoInfo fetchPaymentStatus(String inventoryTransactionId);
	public Inventory fetchInventory(String inventoryId);
	public void givePayment(PaymentPaySupplier paymentPaySupplier);
	public List<InventoryReportView> fetchInventoryReportView(String supplierId,String startDate,String endDate, String range);
	public List<InventoryDetails> fetchTrasactionDetailsByInventoryId(String inventoryTransactionId);
	public List<InventoryProduct> makeInventoryProductViewProductNull(List<InventoryProduct> inventoryProductsList);
	public void saveInventoryForApp(InventorySaveRequestModel inventorySaveRequestModel);
	public List<Inventory> fetchAddedInventoryforGateKeeperReportByDateRange(String fromDate,
			String toDate, String range);
	public Inventory fetchInventoryByInventoryTransactionIdforGateKeeperReport(String inventoryTransactionId);
	public List<InventoryDetails> fetchInventoryDetailsByInventoryTransactionIdforGateKeeperReport(
			String inventoryTransactionId);
	public List<InventoryDetails> makeProductImageNullOfInventoryDetailsList(List<InventoryDetails> inventoryDetailsList);
	public double fetchTotalValueOfCurrrentInventory();
	public long fetchProductUnderThresholdCount();
	public double fetchBalanceAmtBySupplierId(String supplierId);
}
