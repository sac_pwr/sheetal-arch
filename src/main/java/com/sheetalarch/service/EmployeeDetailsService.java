package com.sheetalarch.service;

import java.util.List;

import com.sheetalarch.admin.models.EmployeeAreaDetails;
import com.sheetalarch.admin.models.EmployeeHolidayModel;
import com.sheetalarch.admin.models.EmployeePaymentModel;
import com.sheetalarch.admin.models.EmployeeSalaryStatus;
import com.sheetalarch.admin.models.EmployeeViewModel;
import com.sheetalarch.admin.models.SMListAssignToASMModel;
import com.sheetalarch.admin.models.SMReportModel;
import com.sheetalarch.admin.models.SMReportOrderDetails;
import com.sheetalarch.entity.Area;
import com.sheetalarch.entity.Cluster;
import com.sheetalarch.entity.Employee;
import com.sheetalarch.entity.EmployeeBasicSalaryStatus;
import com.sheetalarch.entity.EmployeeDetails;
import com.sheetalarch.entity.EmployeeHolidays;
import com.sheetalarch.entity.EmployeeIncentives;
import com.sheetalarch.entity.EmployeeRouteList;
import com.sheetalarch.entity.EmployeeSalary;
import com.sheetalarch.entity.OrderDetails;
import com.sheetalarch.entity.Route;

public interface EmployeeDetailsService {

	public EmployeeDetails getEmployeeDetailsByemployeeId(long employeeId);
	public void updateEmployeeDetails(EmployeeDetails employeeDetails);
	public EmployeeDetails getGateKeeperEmployeeDetailsByrouteId(long routeId);
	public List<Route> fetchEmployeeRouteListByEmployeeId(long employeeId);	
	public List<Cluster> fetchClusterList();
	public List<EmployeeDetails> fetchASM();	
	public List<Employee> fetchSalesmanByASMId(long employeeId);	
	public String fetchEmployeeNameByOrderDetails(OrderDetails orderDetails);
	public String fetchMobileNoByOrderId(OrderDetails orderDetails);
	public List<Area> fetchAreaListByEmployeeId(long employeeId);
	public List<Route> fetchRouteListByAreaId(long areaId);	
	public List<Cluster> fetchClusterListByGkEmployeeId(long employeeId);	
	//webApp
	public void saveForWebApp(EmployeeDetails employeeDetails);
	public EmployeeDetails fetchEmployeeDetailsForWebApp(long employeeDetailsId);
	public void updateForWebApp(EmployeeDetails employeeDetails);
	public void saveEmployeeOldBasicSalary(EmployeeBasicSalaryStatus employeeOldBasicSalary);
	public void updateEmployeeOldBasicSalary(EmployeeBasicSalaryStatus employeeOldBasicSalary);
	public SMListAssignToASMModel saveEmployeeAreasForWebApp(Long cluster[],long areaId,long rootId,String deptType, long employeeDetailsId);
	public SMListAssignToASMModel updateEmployeeRoutesForWebApp(Long cluster[],long areaId,long rootId,String deptType, long employeeDetailsId);
	public void updateSM_List_To_ASM_ForWebApp(SMListAssignToASMModel sMListAssignToASMModel);
	public EmployeeBasicSalaryStatus fetchLastEmployeeOldBasicSalaryByEmployeeOldBasicSalaryId(long employeeDetailsId);
	public List<EmployeeRouteList> fetchEmployeeRouteListByEmployeeDetailsId(long employeeDetailsId);
	public List<EmployeeViewModel> fetchEmployeeDetailsForView();
	public List<EmployeeSalaryStatus> fetchEmployeeSalaryStatusForWebApp(long employeeDetailsId);
	public List<EmployeeSalaryStatus> tofilterRangeEmployeeSalaryStatusForWebApp(String startDate, String endDate,String range, long employeeDetailsId);
	public EmployeeHolidayModel fetchEmployeeHolidayModelForWebApp(String employeeDetailsId,String filter,String startDate,String endDate);
	public List<EmployeeIncentives> fetchEmployeeIncentiveListByFilter(long employeeDetailsId,String filter,String startDate,String endDate);
	public EmployeeIncentives fetchEmployeeIncentivesByEmployeeIncentivesId(long employeeIncentivesId);
	public String updateIncentive(EmployeeIncentives employeeIncentives);
	public EmployeeAreaDetails fetchEmployeeAreaDetails(String employeeDetailsId);
	public void bookHolidayForWebApp(EmployeeHolidays employeeHolidays);
	public EmployeeHolidays fetchEmployeeHolidayByEmployeeHolidayId(long employeeHolidayId);
	public String updateEmployeeHoliday(EmployeeHolidays employeeHolidays);
	public String checkHolidayGivenOrNot(String startDate, String endDate,String employeeDetailsId);
	public String checkUpdatingHolidayGivenOrNot(String startDate, String endDate,String employeeDetailsId,long employeeHolidayId);
	public void giveIncentives(EmployeeIncentives employeeIncentives);
	public void givePayment(EmployeeSalary employeeSalary);
	public EmployeePaymentModel openPaymentModel(String employeeDetailsId);
	public String sendSMSTOEmployee(String employeeDetailsIdList,String smsText);
	public List<EmployeeViewModel> fetchEmployeeDetail(String employeeDetailId);
	public List<EmployeeDetails> fetchEmployeeDetailsByDepartmentId(long deparmentId);
	public List<Route> fetchRouteByEmployeeId(long employeeId);
	public String checkAreaAssignedToASM(long areaId);
	public String checkAreaAssignedToASMUpdate(long areaId,long employeeDetailsId);
	public String checkRouteAssignedToSM(long routeId);
	public String checkRouteAssignedToSMUpdate(long areaId,long employeeDetailsId);
	public SMReportModel fetchSMReportModel(String startDate,String endDate,String range,String type);
	public SMReportOrderDetails fetchSMReportOrderDetails(String startDate,String endDate,String range,long employeeDetailsId);
}
