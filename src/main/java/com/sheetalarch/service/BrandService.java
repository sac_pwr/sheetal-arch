package com.sheetalarch.service;

import java.util.List;

import com.sheetalarch.entity.Brand;

public interface BrandService {
	public void saveBrandForWebApp(Brand brand);
	public void updateBrandForWebApp(Brand brand);
	public Brand fetchBrandForWebApp(long brandId);
	public List<Brand> fetchBrandListForWebApp();	
}
