package com.sheetalarch.service;

import java.util.List;

import com.sheetalarch.admin.models.CustomerRerportModel;
import com.sheetalarch.admin.models.OrderReportModel;
import com.sheetalarch.admin.models.SalesManReportModel;
import com.sheetalarch.admin.models.SingleCustomerReport;
import com.sheetalarch.entity.Area;
import com.sheetalarch.entity.DispatchOrderDetails;
import com.sheetalarch.entity.DispatchOrderProductDetails;
import com.sheetalarch.entity.Employee;
import com.sheetalarch.entity.OrderDetails;
import com.sheetalarch.entity.OrderProductDetails;
import com.sheetalarch.entity.OrderStatus;
import com.sheetalarch.entity.ReOrderDetails;
import com.sheetalarch.entity.ReOrderProductDetails;
import com.sheetalarch.models.CancelOrderRequest;
import com.sheetalarch.models.DispatchProductNameAndQtyListResponse;
import com.sheetalarch.models.GkDispatchProductReportRequest;
import com.sheetalarch.models.GkDispatchProductReportResponse;
import com.sheetalarch.models.GkDispatchedOrderListResponse;
import com.sheetalarch.models.GkProductDetailsReportResponse;
import com.sheetalarch.models.OrderIssueRequest;
import com.sheetalarch.models.OrderListGKRequest;
import com.sheetalarch.models.OrderListResponse;
import com.sheetalarch.models.OrderRequest;
import com.sheetalarch.models.ReOrderDeliveryRequest;

public interface OrderDetailsService {
	
	public String bookOrder(OrderRequest orderRequest);
	public OrderDetails fetchOrderDetailsByOrderId(String orderDetailsId);
	public void updateOrderDetailsPaymentDays(OrderDetails orderDetails);
	public void updateDispatchProduct(DispatchOrderDetails dispatchOrderDetailsList,OrderDetails orderDetails, Employee gKemployee , Area salemanArea);
	public void updateDispatchProductDetails(List<OrderProductDetails> orderProductDetailsList,List<DispatchOrderProductDetails> dispatchOrderProductDetailsList,DispatchOrderDetails dispatchOrderDetails);
	public List<DispatchOrderProductDetails> fetchDispatchOrderProductListByDispatchId(long dispatchedId);
	//public List<OrderDetails> fetchOrderDetailsListForGK(OrderListGKRequest orderListGKRequest);
	public List<OrderProductDetails> fetchOrderProductDetailsByOrderId(String orderId);
	
	public DispatchOrderDetails fetchDispatchOrderDetailsByDispatchedId(long dispatchedId);
	public String packedOrderForDelivery(OrderIssueRequest orderIssueRequest);
	//public String packedOrderToDeliverBoy(OrderIssueRequest orderIssueRequest);
	public List<OrderListResponse> fetchOrderDetailsListForGK1(OrderListGKRequest orderListGKRequest);
	public List<GkDispatchedOrderListResponse> fetchDispatchedOrderDetailsListForGK(OrderListGKRequest orderListGKRequest);
	
	public List<DispatchProductNameAndQtyListResponse> fetchOrderProductDetailsForDeliveryByEmpId(long employeeId);
	
	public void OrderConfirmForDelivery(long employeeId);
	
	 public List<OrderDetails> remainingOrderListAndDeliveredOrderedListByEmpId(long employeeId, String status);
	// public List<OrderDetails> deliveredOrderedList(long employeeId);
	
	 public void cancelOrder(CancelOrderRequest cancelOrderRequest);
	 public List<OrderDetails> fetchOrderListForReorder(long employeeId);
	 
	 public DispatchOrderDetails fetchDispatchOrderDetailsByemployeeId(long employeeId);
	 public DispatchOrderDetails fetchDispatchOrderDetailsByemployeeIdAndDeliveryDate1(long employeeId , String deliveryDate);
	 
	 public DispatchOrderDetails fetchDispatchOrderForDeliveryByempId(long employeeId);
	 public void confirmOrderForDelivery(long employeeId,long dispatchedId);
	 
	 public void deliveryOfReOrderProduct(ReOrderDeliveryRequest reOrderDeliveryRequest);
		public List<ReOrderProductDetails> fecthReOrderProductDetailsByOrderId(String orderId);
	 public List<OrderProductDetails> fetchOrderProductDetailsForReOrderByOrderId(List<OrderProductDetails> orderProductDetails,List<ReOrderProductDetails> reOrderProductDetails);
	 
	 //DispatchOrderDetails Reports Gk
	 public List<DispatchOrderDetails> fetchDispatchOrderDetailsForGkReportByClusterIdAndDateRange(
				GkDispatchProductReportRequest gkDispatchProductReportRequest);
	 public List<GkDispatchProductReportResponse> fetchDispatchOrderDetailsListForGkReportByClusterIdAndDateRange(GkDispatchProductReportRequest gkDispatchProductReportRequest);
	 
	 public List<GkProductDetailsReportResponse> fetchDispatchProductDetailsListForGkReportByDateRange(long employeeId,String fromDate,String toDate,String range);
	 public void updateDispatchProductNotReceivingReason(long dispatchedId, String reason);
	 public List<ReOrderProductDetails> fetchReOrderProductDetailsByReOrderId(String reOrderDetailsId);
	 public ReOrderDetails fetchReOrderDetailsByReOrderId(String reOrderDetailsId);
	 
	 //webApp
	 public String sendSMSTOShopsUsingOrderId(String orderIds,String smsText);
		public ReOrderDetails fetchReOrderDetailsByOrderId(String reOrderDetailsId);
		public List<SalesManReportModel> fetchSalesManReportModel(String startDate, String endDate,String range);
		public List<OrderReportModel> fetchOrderReport(String currrentPending);
}
