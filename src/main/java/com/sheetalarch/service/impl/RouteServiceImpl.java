/**
 * 
 */
package com.sheetalarch.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.sheetalarch.dao.RouteDAO;
import com.sheetalarch.entity.Route;
import com.sheetalarch.service.RouteService;

/**
 * @author aNKIT
 *
 */
@Component
@Transactional
@Service("routeService")
@Qualifier("routeService")
public class RouteServiceImpl implements RouteService {

	@Autowired
	RouteDAO routeDAO;
	
	/* (non-Javadoc)
	 * @see com.sheetalarch.service.RouteService#fetchRouteByAreaId(long)
	 * Nov 29, 20176:15:15 PM
	 */
	@Override
	public List<Route> fetchRouteByAreaId(long areaId) {
		// TODO Auto-generated method stub
		return routeDAO.fetchRouteByAreaId(areaId);
	}

	/* (non-Javadoc)
	 * @see com.sheetalarch.service.RouteService#fetchRouteList()
	 * Nov 30, 20171:18:28 PM
	 */
	@Override
	public List<Route> fetchRouteList() {
		// TODO Auto-generated method stub
		return routeDAO.fetchRouteList();
	}

	/* (non-Javadoc)
	 * @see com.sheetalarch.service.RouteService#saveRoute(com.sheetalarch.entity.Route)
	 * Nov 30, 20171:18:28 PM
	 */
	@Override
	public void saveRoute(Route route) {
		// TODO Auto-generated method stub
		routeDAO.saveRoute(route);
	}

	/* (non-Javadoc)
	 * @see com.sheetalarch.service.RouteService#updateRoute(com.sheetalarch.entity.Route)
	 * Nov 30, 20171:18:28 PM
	 */
	@Override
	public void updateRoute(Route route) {
		// TODO Auto-generated method stub
		routeDAO.updateRoute(route);
	}

	/* (non-Javadoc)
	 * @see com.sheetalarch.service.RouteService#fetchRouteByRouteId(long)
	 * Dec 1, 20173:49:50 PM
	 */
	@Override
	public Route fetchRouteByRouteId(long routeId) {
		// TODO Auto-generated method stub
		return routeDAO.fetchRouteByRouteId(routeId);
	}

}
