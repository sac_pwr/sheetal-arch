package com.sheetalarch.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.sheetalarch.dao.ASMReportDAO;
import com.sheetalarch.entity.OrderDetails;
import com.sheetalarch.entity.Payment;
import com.sheetalarch.entity.ReOrderDetails;
import com.sheetalarch.entity.ReOrderProductDetails;
import com.sheetalarch.models.CancelAsmReportResponse;
import com.sheetalarch.service.ASMReportService;

@Component
@Transactional
@Service("cancelReportService")
@Qualifier("cancelReportService")
public class ASMReportServiceImpl implements ASMReportService {

	@Autowired
	ASMReportDAO asmReportDAO;
	
	@Override
	public List<CancelAsmReportResponse> fetchSmAndCancelOrderByAsmId(long employeeId) {
		// TODO Auto-generated method stub
		return asmReportDAO.fetchSmAndCancelOrderByAsmId(employeeId);
	}

	@Override
	public List<OrderDetails> todaysCancelOrderListByEmployeeId(long employeeId) {
		// TODO Auto-generated method stub
		return asmReportDAO.todaysCancelOrderListByEmployeeId(employeeId);
	}

	@Override
	public List<ReOrderDetails> todaysReOrderListByEmployeeId(long employeeId) {
		// TODO Auto-generated method stub
		return asmReportDAO.todaysReOrderListByEmployeeId(employeeId);
	}

	@Override
	public List<CancelAsmReportResponse> fetchSmAndReOrderByAsmId(long employeeId) {
		// TODO Auto-generated method stub
		return asmReportDAO.fetchSmAndReOrderByAsmId(employeeId);
	}

	@Override
	public ReOrderDetails fetchReOrderDetailsByReOrderId(String reOrderId) {
		// TODO Auto-generated method stub
		return asmReportDAO.fetchReOrderDetailsByReOrderId(reOrderId);
	}

	@Override
	public List<ReOrderProductDetails> fetchReOrderProductDetailsByReOrderId(String reOrderId) {
		// TODO Auto-generated method stub
		return asmReportDAO.fetchReOrderProductDetailsByReOrderId(reOrderId);
	}

	@Override
	public List<OrderDetails> fetchOrderDetailsListByEmpIdAndDateRangeforASMMemberReport(long employeeId,
			String fromDate, String toDate, String range) {
		// TODO Auto-generated method stub
		return asmReportDAO.fetchOrderDetailsListByEmpIdAndDateRangeforASMMemberReport(employeeId, fromDate, toDate, range);
	}

	@Override
	public List<Payment> fetchPaymentListByEmployeeIdAndDateRange(long employeeId, String fromDate, String toDate,
			String range) {
		// TODO Auto-generated method stub
		return asmReportDAO.fetchPaymentListByEmployeeIdAndDateRange(employeeId, fromDate, toDate, range);
	}

	
}
