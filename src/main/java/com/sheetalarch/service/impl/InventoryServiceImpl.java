package com.sheetalarch.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.sheetalarch.admin.models.InventoryProduct;
import com.sheetalarch.admin.models.InventoryReportView;
import com.sheetalarch.admin.models.PaymentDoInfo;
import com.sheetalarch.dao.InventoryDAO;
import com.sheetalarch.entity.Inventory;
import com.sheetalarch.entity.InventoryDetails;
import com.sheetalarch.entity.PaymentPaySupplier;
import com.sheetalarch.entity.SupplierProductList;
import com.sheetalarch.models.InventorySaveRequestModel;
import com.sheetalarch.models.ProductAddInventory;
import com.sheetalarch.service.InventoryService;


@Component
@Transactional
@Service("inventoryService")
@Qualifier("inventoryService")
public class InventoryServiceImpl implements InventoryService {

	@Autowired
	InventoryDAO inventoryDAO;
	
	
	@Override
	public List<InventoryProduct> inventoryProductList() {
		// TODO Auto-generated method stub
		return inventoryDAO.inventoryProductList();
	}


	


	@Override
	public List<SupplierProductList> fetchSupplierListByProductId(long productId) {
		// TODO Auto-generated method stub
		return inventoryDAO.fetchSupplierListByProductId(productId);
	}

	@Override
	public void addInventory(Inventory inventory,String productIdList) {
		// TODO Auto-generated method stub
		inventoryDAO.addInventory(inventory,productIdList);
	}


	@Override
	public PaymentDoInfo fetchPaymentStatus(String inventoryTransactionId) {
		// TODO Auto-generated method stub
		return inventoryDAO.fetchPaymentStatus(inventoryTransactionId);
	}


	@Override
	public Inventory fetchInventory(String inventoryId) {
		// TODO Auto-generated method stub
		return inventoryDAO.fetchInventory(inventoryId);
	}


	@Override
	public void givePayment(PaymentPaySupplier paymentPaySupplier) {
		// TODO Auto-generated method stub
		inventoryDAO.givePayment(paymentPaySupplier);
	}


	@Override
	public List<InventoryReportView> fetchInventoryReportView(String supplierId,String startDate,String endDate, String range) {
		// TODO Auto-generated method stub
		return inventoryDAO.fetchInventoryReportView(supplierId,startDate,endDate, range);
	}


	@Override
	public List<InventoryDetails> fetchTrasactionDetailsByInventoryId(String inventoryTransactionId) {
		// TODO Auto-generated method stub
		return inventoryDAO.fetchTrasactionDetailsByInventoryId(inventoryTransactionId);
	}


	@Override
	public List<InventoryProduct> makeInventoryProductViewProductNull(List<InventoryProduct> inventoryProductsList) {
		// TODO Auto-generated method stub
		return inventoryDAO.makeInventoryProductViewProductNull(inventoryProductsList);
	}


	@Override
	public void saveInventoryForApp(InventorySaveRequestModel inventorySaveRequestModel) {
		// TODO Auto-generated method stub
		inventoryDAO.saveInventoryForApp(inventorySaveRequestModel);
	}


	/* (non-Javadoc)
	 * @see com.bluesquare.service.InventoryService#fetchAddedInventoryforGateKeeperReportByDateRange(java.lang.String, java.lang.String, java.lang.String)
	 * Oct 14, 20176:10:49 PM
	 */
	@Override
	public List<Inventory> fetchAddedInventoryforGateKeeperReportByDateRange(String fromDate, String toDate,
			String range) {
		// TODO Auto-generated method stub
		return inventoryDAO.fetchAddedInventoryforGateKeeperReportByDateRange(fromDate, toDate, range);
	}


	/* (non-Javadoc)
	 * @see com.bluesquare.service.InventoryService#fetchInventoryByInventoryTransactionIdforGateKeeperReport(java.lang.String)
	 * Oct 14, 20176:12:20 PM
	 */
	@Override
	public Inventory fetchInventoryByInventoryTransactionIdforGateKeeperReport(String inventoryTransactionId) {
		// TODO Auto-generated method stub
		return inventoryDAO.fetchInventoryByInventoryTransactionIdforGateKeeperReport(inventoryTransactionId);
	}


	/* (non-Javadoc)
	 * @see com.bluesquare.service.InventoryService#fetchInventoryDetailsByInventoryTransactionIdforGateKeeperReport(java.lang.String)
	 * Oct 14, 20176:13:58 PM
	 */
	@Override
	public List<InventoryDetails> fetchInventoryDetailsByInventoryTransactionIdforGateKeeperReport(
			String inventoryTransactionId) {
		// TODO Auto-generated method stub
		return inventoryDAO.fetchInventoryDetailsByInventoryTransactionIdforGateKeeperReport(inventoryTransactionId);
	}


	/* (non-Javadoc)
	 * @see com.bluesquare.service.InventoryService#fetchTotalValueOfCurrrentInventory()
	 * Oct 31, 20177:02:01 PM
	 */
	@Override
	public double fetchTotalValueOfCurrrentInventory() {
		// TODO Auto-generated method stub
		return inventoryDAO.fetchTotalValueOfCurrrentInventory();
	}


	/* (non-Javadoc)
	 * @see com.bluesquare.service.InventoryService#fetchProductUnderThresholdCount()
	 * Oct 31, 20177:22:56 PM
	 */
	@Override
	public long fetchProductUnderThresholdCount() {
		// TODO Auto-generated method stub
		return inventoryDAO.fetchProductUnderThresholdCount();
	}


	/* (non-Javadoc)
	 * @see com.bluesquare.service.InventoryService#makeProductImageNullOfInventoryDetailsList(java.util.List)
	 * Nov 18, 20177:30:48 PM
	 */
	@Override
	public List<InventoryDetails> makeProductImageNullOfInventoryDetailsList(
			List<InventoryDetails> inventoryDetailsList) {
		// TODO Auto-generated method stub
		return inventoryDAO.makeProductImageNullOfInventoryDetailsList(inventoryDetailsList);
	}





	/* (non-Javadoc)
	 * @see com.sheetalarch.service.InventoryService#fetchProductListByBrandIdAndCategoryIdForApp(java.lang.String, long, long)
	 * Dec 13, 20173:43:01 PM
	 */
	@Override
	public List<ProductAddInventory> fetchProductListByBrandIdAndCategoryIdForApp(String supplierId, long categoryId,
			long brandId) {
		// TODO Auto-generated method stub
		return inventoryDAO.fetchProductListByBrandIdAndCategoryIdForApp(supplierId, categoryId, brandId);
	}





	/* (non-Javadoc)
	 * @see com.sheetalarch.service.InventoryService#fetchBalanceAmtBySupplierId(java.lang.String)
	 * Dec 14, 201711:06:44 AM
	 */
	@Override
	public double fetchBalanceAmtBySupplierId(String supplierId) {
		// TODO Auto-generated method stub
		return inventoryDAO.fetchBalanceAmtBySupplierId(supplierId);
	}

}
