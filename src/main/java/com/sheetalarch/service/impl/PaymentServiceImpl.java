package com.sheetalarch.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.sheetalarch.admin.models.CollectionReportMain;
import com.sheetalarch.dao.PaymentDAO;
import com.sheetalarch.entity.Payment;
import com.sheetalarch.models.OrderDetailsForPayment;
import com.sheetalarch.models.PaymentNotReceiveRequest;
import com.sheetalarch.models.PaymentTakeRequest;
import com.sheetalarch.models.ReOrderPaymentNotReceiveRequest;
import com.sheetalarch.models.ReOrderPaymentTakeRequest;
import com.sheetalarch.service.PaymentService;


@Component
@Transactional
@Service("paymentService")
@Qualifier("paymentService")
public class PaymentServiceImpl implements PaymentService {

	@Autowired
	PaymentDAO paymentDAO;
	
	
	@Override
	public void updatePaymentStatusAndOrderStatus(PaymentNotReceiveRequest  paymentNotReceiveRequest ) {
		// TODO Auto-generated method stub
		paymentDAO.updatePaymentStatusAndOrderStatus(paymentNotReceiveRequest);	
	}


	@Override
	public void savePaymentStatus(PaymentTakeRequest paymentTakeRequest) {
		// TODO Auto-generated method stub
		paymentDAO.savePaymentStatus(paymentTakeRequest);
	}


	@Override
	public List<OrderDetailsForPayment> fetchOrderListForPayment(long employeeId, String paymentStatus) {
		// TODO Auto-generated method stub
		return paymentDAO.fetchOrderListForPayment(employeeId, paymentStatus);
	}


	/* (non-Javadoc)
	 * @see com.sheetalarch.service.PaymentService#getCollectionReportDetails(java.lang.String, java.lang.String, java.lang.String)
	 * Dec 8, 20175:26:24 PM
	 */
	@Override
	public CollectionReportMain getCollectionReportDetails(String startDate, String endDate, String range) {
		// TODO Auto-generated method stub
		return paymentDAO.getCollectionReportDetails(startDate, endDate, range);
	}


	/* (non-Javadoc)
	 * @see com.sheetalarch.service.PaymentService#updatePaymentNotReceiveStatusOfReOrder(com.sheetalarch.models.ReOrderPaymentNotReceiveRequest)
	 * Dec 13, 20173:51:27 PM
	 */
	@Override
	public void updatePaymentNotReceiveStatusOfReOrder(
			ReOrderPaymentNotReceiveRequest reOrderPaymentNotReceiveRequest) {
		// TODO Auto-generated method stub
		paymentDAO.updatePaymentNotReceiveStatusOfReOrder(reOrderPaymentNotReceiveRequest);
	}


	/* (non-Javadoc)
	 * @see com.sheetalarch.service.PaymentService#savePaymentStatusOfReOrder(com.sheetalarch.models.ReOrderPaymentTakeRequest)
	 * Dec 13, 20173:51:27 PM
	 */
	@Override
	public void savePaymentStatusOfReOrder(ReOrderPaymentTakeRequest reOrderPaymentTakeRequest) {
		// TODO Auto-generated method stub
		paymentDAO.savePaymentStatusOfReOrder(reOrderPaymentTakeRequest);
	}


	/*@Override
	public List<Payment> fetchOrderDetaisForPaymentByEmployeeId(long employeeId, String paymentStatus) {
		// TODO Auto-generated method stub
		return paymentDAO.fetchOrderDetaisForPaymentByEmployeeId(employeeId, paymentStatus);
	}*/

}
