package com.sheetalarch.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.sheetalarch.dao.EmployeeDAO;
import com.sheetalarch.entity.Employee;
import com.sheetalarch.service.EmployeeService;

@Component
@Transactional
@Service("employeeService")
@Qualifier("employeeService")
public class EmployeeServiceImpl implements EmployeeService {

	@Autowired
	EmployeeDAO employeeDAO;
	
	@Override
	public Employee login(String userId, String password) {
		// TODO Auto-generated method stub
		return employeeDAO.login(userId, password);
	}

	/* (non-Javadoc)
	 * @see com.sheetalarch.service.EmployeeService#checkUserIdForWebApp(java.lang.String)
	 * Nov 28, 20173:26:14 PM
	 */
	@Override
	public boolean checkUserIdForWebApp(String userId) {
		// TODO Auto-generated method stub
		return employeeDAO.checkUserIdForWebApp(userId);
	}

	/* (non-Javadoc)
	 * @see com.sheetalarch.service.EmployeeService#saveForWebApp(com.sheetalarch.entity.Employee)
	 * Nov 28, 20177:20:23 PM
	 */
	@Override
	public void saveForWebApp(Employee employee) {
		// TODO Auto-generated method stub
		employeeDAO.saveForWebApp(employee);
	}

	/* (non-Javadoc)
	 * @see com.sheetalarch.service.EmployeeService#updateForWebApp(com.sheetalarch.entity.Employee)
	 * Nov 29, 20171:44:29 PM
	 */
	@Override
	public void updateForWebApp(Employee employee) {
		// TODO Auto-generated method stub
		employeeDAO.updateForWebApp(employee);
	}

	/* (non-Javadoc)
	 * @see com.sheetalarch.service.EmployeeService#checkUserIdForUpdateForWebApp(java.lang.String, java.lang.String)
	 * Nov 30, 20176:49:04 PM
	 */
	@Override
	public boolean checkUserIdForUpdateForWebApp(String userId, String employeeId) {
		// TODO Auto-generated method stub
		return employeeDAO.checkUserIdForUpdateForWebApp(userId, employeeId);
	}

}
