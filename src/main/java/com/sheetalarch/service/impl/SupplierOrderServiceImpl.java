package com.sheetalarch.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.sheetalarch.dao.SupplierOrderDAO;
import com.sheetalarch.entity.SupplierOrder;
import com.sheetalarch.entity.SupplierOrderDetails;
import com.sheetalarch.service.SupplierOrderService;


@Component
@Transactional
@Service("supplierOrderService")
@Qualifier("supplierOrderService")
public class SupplierOrderServiceImpl implements SupplierOrderService {

	@Autowired
	SupplierOrderDAO supplierOrderDAO;
	
	@Override
	public void saveSupplierOrder(String productWithSupplier) {
		// TODO Auto-generated method stub
		supplierOrderDAO.saveSupplierOrder(productWithSupplier);
	}

	@Override
	public SupplierOrder fetchSupplierOrder(String supplierOrderId) {
		// TODO Auto-generated method stub
		return supplierOrderDAO.fetchSupplierOrder(supplierOrderId);
	}

	@Override
	public List<SupplierOrderDetails> fetchSupplierOrderDetailsList(String supplierOrderId) {
		// TODO Auto-generated method stub
		return supplierOrderDAO.fetchSupplierOrderDetailsList(supplierOrderId);
	}

	@Override
	public void editSupplierOrder(String productWithSupplier, SupplierOrder supplierOrder) {
		// TODO Auto-generated method stub
		supplierOrderDAO.editSupplierOrder(productWithSupplier, supplierOrder);
	}

	/* (non-Javadoc)
	 * @see com.bluesquare.service.SupplierOrderService#fetchSupplierOrderList()
	 * Nov 3, 20174:08:24 PM
	 */
	@Override
	public List<SupplierOrder> fetchSupplierOrderList() {
		// TODO Auto-generated method stub
		return supplierOrderDAO.fetchSupplierOrderList();
	}

	/* (non-Javadoc)
	 * @see com.sheetalarch.service.SupplierOrderService#fetchSupplierOrders24hour(java.lang.String, java.lang.String, java.lang.String)
	 * Dec 7, 20178:13:19 PM
	 */
	@Override
	public List<SupplierOrder> fetchSupplierOrders24hour(String range, String startDate, String endDate) {
		// TODO Auto-generated method stub
		return supplierOrderDAO.fetchSupplierOrders24hour(range, startDate, endDate);
	}

	/* (non-Javadoc)
	 * @see com.sheetalarch.service.SupplierOrderService#fetchSupplierOrderDetailsListBySupplierOrderId(java.lang.String)
	 * Dec 7, 20178:18:49 PM
	 */
	@Override
	public List<SupplierOrderDetails> fetchSupplierOrderDetailsListBySupplierOrderId(String supplierOrderId) {
		// TODO Auto-generated method stub
		return supplierOrderDAO.fetchSupplierOrderDetailsListBySupplierOrderId(supplierOrderId);
	}

	

}
