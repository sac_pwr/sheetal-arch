/**
 * 
 */
package com.sheetalarch.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.sheetalarch.dao.ClusterDAO;
import com.sheetalarch.entity.Cluster;
import com.sheetalarch.entity.Employee;
import com.sheetalarch.service.ClusterService;

/**
 * @author aNKIT
 *
 */
@Component
@Transactional
@Service("clusterService")
@Qualifier("clusterService")
public class ClusterServiceImpl implements ClusterService {

	@Autowired
	ClusterDAO clusterDAO;
	
	@Override
	public Cluster fetchClusterBySalesmanId(long employeeId) {
		// TODO Auto-generated method stub
		return clusterDAO.fetchClusterBySalesmanId(employeeId);
	}

	@Override
	public Employee fetchGKEmployeeByClusterId(long clusterId) {
		// TODO Auto-generated method stub
		return clusterDAO.fetchGKEmployeeByClusterId(clusterId);
	}

	/* (non-Javadoc)
	 * @see com.sheetalarch.service.ClusterService#fetchClusterList()
	 * Nov 28, 20173:22:47 PM
	 */
	@Override
	public List<Cluster> fetchClusterList() {
		// TODO Auto-generated method stub
		return clusterDAO.fetchClusterList();
	}

	/* (non-Javadoc)
	 * @see com.sheetalarch.service.ClusterService#saveCluster(com.sheetalarch.entity.Cluster)
	 * Nov 30, 20171:07:44 PM
	 */
	@Override
	public void saveCluster(Cluster cluster) {
		// TODO Auto-generated method stub
		clusterDAO.saveCluster(cluster);
	}

	/* (non-Javadoc)
	 * @see com.sheetalarch.service.ClusterService#updateCluster(com.sheetalarch.entity.Cluster)
	 * Nov 30, 20171:07:44 PM
	 */
	@Override
	public void updateCluster(Cluster cluster) {
		// TODO Auto-generated method stub
		clusterDAO.updateCluster(cluster);
	}

	/* (non-Javadoc)
	 * @see com.sheetalarch.service.ClusterService#fetchClusterByClusterId(long)
	 * Nov 30, 20177:27:09 PM
	 */
	@Override
	public Cluster fetchClusterByClusterId(long clusterId) {
		// TODO Auto-generated method stub
		return clusterDAO.fetchClusterByClusterId(clusterId);
	}

}
