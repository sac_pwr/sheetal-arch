/**
 * 
 */
package com.sheetalarch.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.sheetalarch.dao.AdminLoginDAO;
import com.sheetalarch.entity.AdminLogin;
import com.sheetalarch.service.AdminLoginService;

/**
 * @author aNKIT
 *
 */
@Component
@Transactional
@Service("adminLoginService")
@Qualifier("adminLoginService")
public class AdminLoginServiceImpl implements AdminLoginService {

	@Autowired
	AdminLoginDAO adminLoginDAO;
	
	/* (non-Javadoc)
	 * @see com.sheetalarch.service.AdminLoginService#loginValidate(java.lang.String, java.lang.String)
	 * Dec 14, 20171:01:27 PM
	 */
	@Override
	public AdminLogin loginValidate(String userName, String password) {
		// TODO Auto-generated method stub
		return adminLoginDAO.loginValidate(userName, password);
	}

}
