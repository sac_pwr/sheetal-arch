package com.sheetalarch.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.sheetalarch.admin.models.FetchSupplierList;
import com.sheetalarch.dao.SupplierDAO;
import com.sheetalarch.entity.Supplier;
import com.sheetalarch.entity.SupplierProductList;
import com.sheetalarch.service.SupplierService;

@Component
@Transactional
@Service("supplierService")
@Qualifier("supplierService")
public class SupplierServiceImpl implements SupplierService {

	
	@Autowired
	SupplierDAO supplierDAO;
	@Override
	public List<Supplier> fetchSupplierList() {
		// TODO Auto-generated method stub
		return supplierDAO.fetchSupplierList();
	}
	@Override
	public List<SupplierProductList> fetchProductBySupplierId(String supplierId) {
		// TODO Auto-generated method stub
		return supplierDAO.fetchProductBySupplierId(supplierId);
	}

	@Override
	public List<Supplier> fetchSupplierForWebAppList() {
		// TODO Auto-generated method stub
		return supplierDAO.fetchSupplierForWebAppList();
	}

	@Override
	public Supplier fetchSupplier(String supplierId) {
		// TODO Auto-generated method stub
		return supplierDAO.fetchSupplier(supplierId);
	}

	@Override
	public Supplier saveSupplier(Supplier supplier) {
		// TODO Auto-generated method stub
		return supplierDAO.saveSupplier(supplier);
	}

	@Override
	public Supplier updateSupplier(Supplier supplier) {
		// TODO Auto-generated method stub
		return supplierDAO.updateSupplier(supplier);
	}

	@Override
	public Supplier saveSupplierProductList(String productIdList, String supplierId) {
		// TODO Auto-generated method stub
		return supplierDAO.saveSupplierProductList(productIdList, supplierId);
	}

	@Override
	public List<FetchSupplierList> fetchSupplierForWebApp() {
		// TODO Auto-generated method stub
		return supplierDAO.fetchSupplierForWebApp();
	}

	@Override
	public List<SupplierProductList> fetchSupplierProductListBySupplierIdForWebApp(String supplierId) {
		// TODO Auto-generated method stub
		return supplierDAO.fetchSupplierProductListBySupplierIdForWebApp(supplierId);
	}

	@Override
	public List<SupplierProductList> makeSupplierProductListNullForWebApp(
			List<SupplierProductList> supplierProductList) {
		// TODO Auto-generated method stub
		return supplierDAO.makeSupplierProductListNullForWebApp(supplierProductList);
	}

	@Override
	public String getProductIdList(List<SupplierProductList> supplierProductList) {
		// TODO Auto-generated method stub
		return supplierDAO.getProductIdList(supplierProductList);
	}

	@Override
	public Supplier updateSupplierProductList(String productIdList, String supplierId) {
		// TODO Auto-generated method stub
		return supplierDAO.updateSupplierProductList(productIdList, supplierId);
	}

	@Override
	public double fetchSupplierProductRate(long productId, String supplierId) {
		// TODO Auto-generated method stub
		return supplierDAO.fetchSupplierProductRate(productId, supplierId);
	}

	

	@Override
	public String sendSMSTOSupplier(String supplierIds, String smsText) {
		// TODO Auto-generated method stub
		return supplierDAO.sendSMSTOSupplier(supplierIds, smsText);
	}
	/* (non-Javadoc)
	 * @see com.sheetalarch.service.SupplierService#fetchSupplierByProductIdAndSupplierId(long, java.lang.String)
	 * Dec 7, 20171:47:14 PM
	 */
	@Override
	public SupplierProductList fetchSupplierByProductIdAndSupplierId(long productId, String supplierId) {
		// TODO Auto-generated method stub
		return supplierDAO.fetchSupplierByProductIdAndSupplierId(productId, supplierId);
	}
}
