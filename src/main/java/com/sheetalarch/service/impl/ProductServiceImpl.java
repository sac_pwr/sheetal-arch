package com.sheetalarch.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import com.sheetalarch.admin.models.ProductReportView;
import com.sheetalarch.admin.models.ProductViewList;
import com.sheetalarch.dao.ProductDAO;
import com.sheetalarch.entity.Brand;
import com.sheetalarch.entity.Categories;
import com.sheetalarch.entity.Product;
import com.sheetalarch.entity.SupplierProductList;
import com.sheetalarch.models.BrandAndCategoryRequest;
import com.sheetalarch.service.ProductService;


@Component
@Transactional
@Service("productService")
@Qualifier("productService")
public class ProductServiceImpl implements ProductService {

	@Autowired
	ProductDAO productDAO;
	@Override
	public List<Brand> fetchBrandList() {
		// TODO Auto-generated method stub
		return productDAO.fetchBrandList();
	}

	@Override
	public List<Categories> fetchCategories() {
		// TODO Auto-generated method stub
		return productDAO.fetchCategories();
	}

	@Override
	public List<Product> fetchProductByBrandAndCategory(BrandAndCategoryRequest brandAndCategoryRequest) {
		// TODO Auto-generated method stub
		return productDAO.fetchProductByBrandAndCategory(brandAndCategoryRequest);
	}

	@Override
	public List<Product> makeProductImageMNullorderProductDetailsList(List<Product> productList) {
		// TODO Auto-generated method stub
		return productDAO.makeProductImageMNullorderProductDetailsList(productList);
	}

	@Override
	public List<SupplierProductList> fetchProductListBySupplierIDAndBrandIdAndCategoryIdForApp(String supplierId, long categoryId,
			long brandId) {
		// TODO Auto-generated method stub
		return productDAO.fetchProductListBySupplierIDAndBrandIdAndCategoryIdForApp(supplierId, categoryId, brandId);
	}

	@Override
	public List<SupplierProductList> makeSupplierProductListImageMNullorderProductDetailsList(
			List<SupplierProductList> supplierProductList) {
		// TODO Auto-generated method stub
		return productDAO.makeSupplierProductListImageMNullorderProductDetailsList(supplierProductList);
	}

	@Override
	public void saveProductForWebApp(MultipartFile file, Product product) {
		// TODO Auto-generated method stub
		productDAO.saveProductForWebApp(file, product);
	}

	@Override
	public List<Product> fetchProductList() {
		// TODO Auto-generated method stub
		return productDAO.fetchProductList();
	}

	/* (non-Javadoc)
	 * @see com.sheetalarch.service.ProductService#fetchProductViewListForWebApp()
	 * Dec 7, 201711:11:44 AM
	 */
	@Override
	public List<ProductViewList> fetchProductViewListForWebApp() {
		// TODO Auto-generated method stub
		return productDAO.fetchProductViewListForWebApp();
	}

	/* (non-Javadoc)
	 * @see com.sheetalarch.service.ProductService#fetchProductForWebApp(long)
	 * Dec 7, 201711:11:44 AM
	 */
	@Override
	public Product fetchProductForWebApp(long productId) {
		// TODO Auto-generated method stub
		return productDAO.fetchProductForWebApp(productId);
	}

	/* (non-Javadoc)
	 * @see com.sheetalarch.service.ProductService#fetchProductListForWebApp()
	 * Dec 7, 201711:11:44 AM
	 */
	@Override
	public List<Product> fetchProductListForWebApp() {
		// TODO Auto-generated method stub
		return productDAO.fetchProductListForWebApp();
	}

	/* (non-Javadoc)
	 * @see com.sheetalarch.service.ProductService#updateProductForWebApp(org.springframework.web.multipart.MultipartFile, com.sheetalarch.entity.Product)
	 * Dec 7, 201711:11:44 AM
	 */
	@Override
	public void updateProductForWebApp(MultipartFile file, Product product) {
		// TODO Auto-generated method stub
		productDAO.updateProductForWebApp(file, product);
	}

	/* (non-Javadoc)
	 * @see com.sheetalarch.service.ProductService#fetchProductListForReport(java.lang.String, java.lang.String, java.lang.String, java.lang.String)
	 * Dec 7, 201711:11:44 AM
	 */
	@Override
	public List<ProductReportView> fetchProductListForReport(String range, String startDate, String endDate,
			String topProductNo) {
		// TODO Auto-generated method stub
		return productDAO.fetchProductListForReport(range, startDate, endDate, topProductNo);
	}

	/* (non-Javadoc)
	 * @see com.sheetalarch.service.ProductService#fetchProductListByBrandIdAndCategoryIdForWebApp(long, long)
	 * Dec 7, 20171:52:19 PM
	 */
	@Override
	public List<Product> fetchProductListByBrandIdAndCategoryIdForWebApp(long categoryId, long brandId) {
		// TODO Auto-generated method stub
		return productDAO.fetchProductListByBrandIdAndCategoryIdForWebApp(categoryId, brandId);
	}

	/* (non-Javadoc)
	 * @see com.sheetalarch.service.ProductService#makeProductImageNull(java.util.List)
	 * Dec 7, 20171:53:16 PM
	 */
	@Override
	public List<Product> makeProductImageNull(List<Product> productList) {
		// TODO Auto-generated method stub
		return productDAO.makeProductImageNull(productList);
	}

	/* (non-Javadoc)
	 * @see com.sheetalarch.service.ProductService#makeSupplierProductImageNull(java.util.List)
	 * Dec 7, 20172:10:03 PM
	 */
	@Override
	public List<SupplierProductList> makeSupplierProductImageNull(List<SupplierProductList> supplierProductLists) {
		// TODO Auto-generated method stub
		return productDAO.makeSupplierProductImageNull(supplierProductLists);
	}

	/* (non-Javadoc)
	 * @see com.sheetalarch.service.ProductService#fetchProductListBySupplierId(java.lang.String)
	 * Dec 7, 20172:12:38 PM
	 */
	@Override
	public List<Product> fetchProductListBySupplierId(String supplierId) {
		// TODO Auto-generated method stub
		return productDAO.fetchProductListBySupplierId(supplierId);
	}

	/* (non-Javadoc)
	 * @see com.sheetalarch.service.ProductService#fetchProductListByBrandIdAndCategoryIdForWebApp(java.lang.String, long, long)
	 * Dec 7, 20172:13:59 PM
	 */
	@Override
	public List<Product> fetchProductListByBrandIdAndCategoryIdForWebApp(String supplierId, long categoryId,
			long brandId) {
		// TODO Auto-generated method stub
		return productDAO.fetchProductListByBrandIdAndCategoryIdForWebApp(supplierId, categoryId, brandId);
	}

	/* (non-Javadoc)
	 * @see com.sheetalarch.service.ProductService#saveMultipleProductForWebApp(java.util.List)
	 * Dec 13, 20171:26:35 PM
	 */
	@Override
	public void saveMultipleProductForWebApp(List<Product> productList) {
		// TODO Auto-generated method stub
		productDAO.saveMultipleProductForWebApp(productList);
	}

	/* (non-Javadoc)
	 * @see com.sheetalarch.service.ProductService#fetchProductByCategoryIdForWebApp(long)
	 * Dec 20, 20178:08:43 PM
	 */
	@Override
	public List<Product> fetchProductByCategoryIdForWebApp(long categoryId) {
		// TODO Auto-generated method stub
		return productDAO.fetchProductByCategoryIdForWebApp(categoryId);
	}

	/* (non-Javadoc)
	 * @see com.sheetalarch.service.ProductService#updateProductForWebApp(com.sheetalarch.entity.Product)
	 * Dec 20, 20178:10:44 PM
	 */
	@Override
	public void updateProductForWebApp(Product product) {
		// TODO Auto-generated method stub
		productDAO.updateProductForWebApp(product);
	}

}
