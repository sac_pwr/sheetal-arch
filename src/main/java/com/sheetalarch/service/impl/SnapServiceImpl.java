package com.sheetalarch.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.sheetalarch.dao.SnapDAO;
import com.sheetalarch.entity.DispatchOrderProductDetails;
import com.sheetalarch.models.GkSnapProductResponse;
import com.sheetalarch.models.SnapSalesmanResponse;
import com.sheetalarch.service.SnapService;

@Component
@Transactional
@Service("snapService")
@Qualifier("snapService")

public class SnapServiceImpl implements SnapService {

	@Autowired
	SnapDAO snapDAO;
	
	@Override
	public SnapSalesmanResponse fetchSnapShotDetailsForSalemanByEmployeeId(long employeeId) {
		// TODO Auto-generated method stub		
		return snapDAO.fetchSnapShotDetailsForSalemanByEmployeeId(employeeId);
	}

	@Override
	public List<DispatchOrderProductDetails> fetchDispatchedOrderProductDetailsByGkId(long employeeId) {
		// TODO Auto-generated method stub
		return snapDAO.fetchDispatchedOrderProductDetailsByGkId(employeeId);
	}

	@Override
	public List<GkSnapProductResponse> fetchSnapProductDetailsForGk(long employeeId) {
		// TODO Auto-generated method stub
		return snapDAO.fetchSnapProductDetailsForGk(employeeId);
	}

}
