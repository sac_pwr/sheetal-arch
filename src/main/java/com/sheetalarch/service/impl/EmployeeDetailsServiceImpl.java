package com.sheetalarch.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.sheetalarch.admin.models.EmployeeAreaDetails;
import com.sheetalarch.admin.models.EmployeeHolidayModel;
import com.sheetalarch.admin.models.EmployeePaymentModel;
import com.sheetalarch.admin.models.EmployeeSalaryStatus;
import com.sheetalarch.admin.models.EmployeeViewModel;
import com.sheetalarch.admin.models.SMListAssignToASMModel;
import com.sheetalarch.admin.models.SMReportModel;
import com.sheetalarch.admin.models.SMReportOrderDetails;
import com.sheetalarch.dao.EmployeeDetailsDAO;
import com.sheetalarch.entity.Area;
import com.sheetalarch.entity.Cluster;
import com.sheetalarch.entity.Employee;
import com.sheetalarch.entity.EmployeeBasicSalaryStatus;
import com.sheetalarch.entity.EmployeeDetails;
import com.sheetalarch.entity.EmployeeHolidays;
import com.sheetalarch.entity.EmployeeIncentives;
import com.sheetalarch.entity.EmployeeRouteList;
import com.sheetalarch.entity.EmployeeSalary;
import com.sheetalarch.entity.OrderDetails;
import com.sheetalarch.entity.Route;
import com.sheetalarch.service.EmployeeDetailsService;

@Component
@Transactional
@Service("employeeDetailsService")
@Qualifier("employeeDetailsService")
public class EmployeeDetailsServiceImpl implements EmployeeDetailsService {

	@Autowired
	EmployeeDetailsDAO employeeDetailsDAO;

	/* (non-Javadoc)
	 * @see com.sheetalarch.service.EmployeeDetailsService#getEmployeeDetailsByemployeeId(long)
	 * Dec 1, 201712:19:34 PM
	 */
	@Override
	public EmployeeDetails getEmployeeDetailsByemployeeId(long employeeId) {
		// TODO Auto-generated method stub
		return employeeDetailsDAO.getEmployeeDetailsByemployeeId(employeeId);
	}

	/* (non-Javadoc)
	 * @see com.sheetalarch.service.EmployeeDetailsService#updateEmployeeDetails(com.sheetalarch.entity.EmployeeDetails)
	 * Dec 1, 201712:19:34 PM
	 */
	@Override
	public void updateEmployeeDetails(EmployeeDetails employeeDetails) {
		// TODO Auto-generated method stub
		employeeDetailsDAO.updateEmployeeDetails(employeeDetails);
	}

	/* (non-Javadoc)
	 * @see com.sheetalarch.service.EmployeeDetailsService#getGateKeeperEmployeeDetailsByrouteId(long)
	 * Dec 1, 201712:19:34 PM
	 */
	@Override
	public EmployeeDetails getGateKeeperEmployeeDetailsByrouteId(long routeId) {
		// TODO Auto-generated method stub
		return employeeDetailsDAO.getGateKeeperEmployeeDetailsByrouteId(routeId);
	}

	/* (non-Javadoc)
	 * @see com.sheetalarch.service.EmployeeDetailsService#fetchEmployeeRouteListByEmployeeId(long)
	 * Dec 1, 201712:19:34 PM
	 */
	@Override
	public List<Route> fetchEmployeeRouteListByEmployeeId(long employeeId) {
		// TODO Auto-generated method stub
		return employeeDetailsDAO.fetchEmployeeRouteListByEmployeeId(employeeId);
	}

	/* (non-Javadoc)
	 * @see com.sheetalarch.service.EmployeeDetailsService#fetchClusterList()
	 * Dec 1, 201712:19:34 PM
	 */
	@Override
	public List<Cluster> fetchClusterList() {
		// TODO Auto-generated method stub
		return employeeDetailsDAO.fetchClusterList();
	}

	/* (non-Javadoc)
	 * @see com.sheetalarch.service.EmployeeDetailsService#fetchASM()
	 * Dec 1, 201712:19:34 PM
	 */
	@Override
	public List<EmployeeDetails> fetchASM() {
		// TODO Auto-generated method stub
		return employeeDetailsDAO.fetchASM();
	}

	/* (non-Javadoc)
	 * @see com.sheetalarch.service.EmployeeDetailsService#fetchSalesmanByASMId(long)
	 * Dec 1, 201712:19:34 PM
	 */
	@Override
	public List<Employee> fetchSalesmanByASMId(long employeeId) {
		// TODO Auto-generated method stub
		return employeeDetailsDAO.fetchSalesmanByASMId(employeeId);
	}

	/* (non-Javadoc)
	 * @see com.sheetalarch.service.EmployeeDetailsService#fetchEmployeeNameByOrderDetails(com.sheetalarch.entity.OrderDetails)
	 * Dec 1, 201712:19:34 PM
	 */
	@Override
	public String fetchEmployeeNameByOrderDetails(OrderDetails orderDetails) {
		// TODO Auto-generated method stub
		return employeeDetailsDAO.fetchEmployeeNameByOrderDetails(orderDetails);
	}

	/* (non-Javadoc)
	 * @see com.sheetalarch.service.EmployeeDetailsService#fetchMobileNoByOrderId(com.sheetalarch.entity.OrderDetails)
	 * Dec 1, 201712:19:34 PM
	 */
	@Override
	public String fetchMobileNoByOrderId(OrderDetails orderDetails) {
		// TODO Auto-generated method stub
		return employeeDetailsDAO.fetchMobileNoByOrderId(orderDetails);
	}

	/* (non-Javadoc)
	 * @see com.sheetalarch.service.EmployeeDetailsService#saveForWebApp(com.sheetalarch.entity.EmployeeDetails)
	 * Dec 1, 201712:19:34 PM
	 */
	@Override
	public void saveForWebApp(EmployeeDetails employeeDetails) {
		// TODO Auto-generated method stub
		employeeDetailsDAO.saveForWebApp(employeeDetails);
	}

	/* (non-Javadoc)
	 * @see com.sheetalarch.service.EmployeeDetailsService#fetchEmployeeDetailsForWebApp(long)
	 * Dec 1, 201712:19:34 PM
	 */
	@Override
	public EmployeeDetails fetchEmployeeDetailsForWebApp(long employeeDetailsId) {
		// TODO Auto-generated method stub
		return employeeDetailsDAO.fetchEmployeeDetailsForWebApp(employeeDetailsId);
	}

	/* (non-Javadoc)
	 * @see com.sheetalarch.service.EmployeeDetailsService#updateForWebApp(com.sheetalarch.entity.EmployeeDetails)
	 * Dec 1, 201712:19:34 PM
	 */
	@Override
	public void updateForWebApp(EmployeeDetails employeeDetails) {
		// TODO Auto-generated method stub
		employeeDetailsDAO.updateForWebApp(employeeDetails);
	}

	/* (non-Javadoc)
	 * @see com.sheetalarch.service.EmployeeDetailsService#saveEmployeeOldBasicSalary(com.sheetalarch.entity.EmployeeBasicSalaryStatus)
	 * Dec 1, 201712:19:34 PM
	 */
	@Override
	public void saveEmployeeOldBasicSalary(EmployeeBasicSalaryStatus employeeOldBasicSalary) {
		// TODO Auto-generated method stub
		employeeDetailsDAO.saveEmployeeOldBasicSalary(employeeOldBasicSalary);
	}

	/* (non-Javadoc)
	 * @see com.sheetalarch.service.EmployeeDetailsService#updateEmployeeOldBasicSalary(com.sheetalarch.entity.EmployeeBasicSalaryStatus)
	 * Dec 1, 201712:19:34 PM
	 */
	@Override
	public void updateEmployeeOldBasicSalary(EmployeeBasicSalaryStatus employeeOldBasicSalary) {
		// TODO Auto-generated method stub
		employeeDetailsDAO.updateEmployeeOldBasicSalary(employeeOldBasicSalary);
	}

	/* (non-Javadoc)
	 * @see com.sheetalarch.service.EmployeeDetailsService#saveEmployeeAreasForWebApp(java.lang.Long[], long, long, java.lang.String, long)
	 * Dec 1, 201712:19:34 PM
	 */
	@Override
	public SMListAssignToASMModel saveEmployeeAreasForWebApp(Long[] cluster, long areaId, long rootId, String deptType,
			long employeeDetailsId) {
		// TODO Auto-generated method stub
		return employeeDetailsDAO.saveEmployeeAreasForWebApp(cluster, areaId, rootId, deptType, employeeDetailsId);
	}

	/* (non-Javadoc)
	 * @see com.sheetalarch.service.EmployeeDetailsService#updateEmployeeRoutesForWebApp(java.lang.Long[], long, long, java.lang.String, long)
	 * Dec 1, 201712:19:34 PM
	 */
	@Override
	public SMListAssignToASMModel updateEmployeeRoutesForWebApp(Long[] cluster, long areaId, long rootId, String deptType,
			long employeeDetailsId) {
		// TODO Auto-generated method stub
		return employeeDetailsDAO.updateEmployeeRoutesForWebApp(cluster, areaId, rootId, deptType, employeeDetailsId);
	}

	/* (non-Javadoc)
	 * @see com.sheetalarch.service.EmployeeDetailsService#fetchLastEmployeeOldBasicSalaryByEmployeeOldBasicSalaryId(long)
	 * Dec 1, 201712:19:34 PM
	 */
	@Override
	public EmployeeBasicSalaryStatus fetchLastEmployeeOldBasicSalaryByEmployeeOldBasicSalaryId(long employeeDetailsId) {
		// TODO Auto-generated method stub
		return employeeDetailsDAO.fetchLastEmployeeOldBasicSalaryByEmployeeOldBasicSalaryId(employeeDetailsId);
	}

	/* (non-Javadoc)
	 * @see com.sheetalarch.service.EmployeeDetailsService#fetchEmployeeRouteListByEmployeeDetailsId(long)
	 * Dec 1, 201712:19:34 PM
	 */
	@Override
	public List<EmployeeRouteList> fetchEmployeeRouteListByEmployeeDetailsId(long employeeDetailsId) {
		// TODO Auto-generated method stub
		return employeeDetailsDAO.fetchEmployeeRouteListByEmployeeDetailsId(employeeDetailsId);
	}

	/* (non-Javadoc)
	 * @see com.sheetalarch.service.EmployeeDetailsService#fetchEmployeeDetailsForView()
	 * Dec 1, 201712:19:34 PM
	 */
	@Override
	public List<EmployeeViewModel> fetchEmployeeDetailsForView() {
		// TODO Auto-generated method stub
		return employeeDetailsDAO.fetchEmployeeDetailsForView();
	}

	/* (non-Javadoc)
	 * @see com.sheetalarch.service.EmployeeDetailsService#fetchEmployeeSalaryStatusForWebApp(java.lang.String)
	 * Dec 1, 201712:19:34 PM
	 */
	@Override
	public List<EmployeeSalaryStatus> fetchEmployeeSalaryStatusForWebApp(long employeeDetailsId) {
		// TODO Auto-generated method stub
		return employeeDetailsDAO.fetchEmployeeSalaryStatusForWebApp(employeeDetailsId);
	}

	/* (non-Javadoc)
	 * @see com.sheetalarch.service.EmployeeDetailsService#tofilterRangeEmployeeSalaryStatusForWebApp(java.lang.String, java.lang.String, java.lang.String, long)
	 * Dec 1, 201712:19:34 PM
	 */
	@Override
	public List<EmployeeSalaryStatus> tofilterRangeEmployeeSalaryStatusForWebApp(String startDate, String endDate,
			String range, long employeeDetailsId) {
		// TODO Auto-generated method stub
		return employeeDetailsDAO.tofilterRangeEmployeeSalaryStatusForWebApp(startDate, endDate, range, employeeDetailsId);
	}

	/* (non-Javadoc)
	 * @see com.sheetalarch.service.EmployeeDetailsService#fetchEmployeeHolidayModelForWebApp(java.lang.String, java.lang.String, java.lang.String, java.lang.String)
	 * Dec 1, 201712:19:34 PM
	 */
	@Override
	public EmployeeHolidayModel fetchEmployeeHolidayModelForWebApp(String employeeDetailsId, String filter,
			String startDate, String endDate) {
		// TODO Auto-generated method stub
		return employeeDetailsDAO.fetchEmployeeHolidayModelForWebApp(employeeDetailsId, filter, startDate, endDate);
	}

	/* (non-Javadoc)
	 * @see com.sheetalarch.service.EmployeeDetailsService#fetchEmployeeIncentiveListByFilter(long, java.lang.String, java.lang.String, java.lang.String)
	 * Dec 1, 201712:19:34 PM
	 */
	@Override
	public List<EmployeeIncentives> fetchEmployeeIncentiveListByFilter(long employeeDetailsId, String filter,
			String startDate, String endDate) {
		// TODO Auto-generated method stub
		return employeeDetailsDAO.fetchEmployeeIncentiveListByFilter(employeeDetailsId, filter, startDate, endDate);
	}

	/* (non-Javadoc)
	 * @see com.sheetalarch.service.EmployeeDetailsService#fetchEmployeeIncentivesByEmployeeIncentivesId(long)
	 * Dec 1, 201712:19:34 PM
	 */
	@Override
	public EmployeeIncentives fetchEmployeeIncentivesByEmployeeIncentivesId(long employeeIncentivesId) {
		// TODO Auto-generated method stub
		return employeeDetailsDAO.fetchEmployeeIncentivesByEmployeeIncentivesId(employeeIncentivesId);
	}

	/* (non-Javadoc)
	 * @see com.sheetalarch.service.EmployeeDetailsService#updateIncentive(com.sheetalarch.entity.EmployeeIncentives)
	 * Dec 1, 201712:19:34 PM
	 */
	@Override
	public String updateIncentive(EmployeeIncentives employeeIncentives) {
		// TODO Auto-generated method stub
		return employeeDetailsDAO.updateIncentive(employeeIncentives);
	}

	/* (non-Javadoc)
	 * @see com.sheetalarch.service.EmployeeDetailsService#fetchEmployeeAreaDetails(java.lang.String)
	 * Dec 1, 201712:19:34 PM
	 */
	@Override
	public EmployeeAreaDetails fetchEmployeeAreaDetails(String employeeDetailsId) {
		// TODO Auto-generated method stub
		return employeeDetailsDAO.fetchEmployeeAreaDetails(employeeDetailsId);
	}

	/* (non-Javadoc)
	 * @see com.sheetalarch.service.EmployeeDetailsService#bookHolidayForWebApp(com.sheetalarch.entity.EmployeeHolidays)
	 * Dec 1, 201712:19:34 PM
	 */
	@Override
	public void bookHolidayForWebApp(EmployeeHolidays employeeHolidays) {
		// TODO Auto-generated method stub
		employeeDetailsDAO.bookHolidayForWebApp(employeeHolidays);
	}

	/* (non-Javadoc)
	 * @see com.sheetalarch.service.EmployeeDetailsService#fetchEmployeeHolidayByEmployeeHolidayId(long)
	 * Dec 1, 201712:19:34 PM
	 */
	@Override
	public EmployeeHolidays fetchEmployeeHolidayByEmployeeHolidayId(long employeeHolidayId) {
		// TODO Auto-generated method stub
		return employeeDetailsDAO.fetchEmployeeHolidayByEmployeeHolidayId(employeeHolidayId);
	}

	/* (non-Javadoc)
	 * @see com.sheetalarch.service.EmployeeDetailsService#updateEmployeeHoliday(com.sheetalarch.entity.EmployeeHolidays)
	 * Dec 1, 201712:19:34 PM
	 */
	@Override
	public String updateEmployeeHoliday(EmployeeHolidays employeeHolidays) {
		// TODO Auto-generated method stub
		return employeeDetailsDAO.updateEmployeeHoliday(employeeHolidays);
	}

	/* (non-Javadoc)
	 * @see com.sheetalarch.service.EmployeeDetailsService#checkHolidayGivenOrNot(java.lang.String, java.lang.String, java.lang.String)
	 * Dec 1, 201712:19:34 PM
	 */
	@Override
	public String checkHolidayGivenOrNot(String startDate, String endDate, String employeeDetailsId) {
		// TODO Auto-generated method stub
		return employeeDetailsDAO.checkHolidayGivenOrNot(startDate, endDate, employeeDetailsId);
	}

	/* (non-Javadoc)
	 * @see com.sheetalarch.service.EmployeeDetailsService#checkUpdatingHolidayGivenOrNot(java.lang.String, java.lang.String, java.lang.String, long)
	 * Dec 1, 201712:19:34 PM
	 */
	@Override
	public String checkUpdatingHolidayGivenOrNot(String startDate, String endDate, String employeeDetailsId,
			long employeeHolidayId) {
		// TODO Auto-generated method stub
		return employeeDetailsDAO.checkUpdatingHolidayGivenOrNot(startDate, endDate, employeeDetailsId, employeeHolidayId);
	}

	/* (non-Javadoc)
	 * @see com.sheetalarch.service.EmployeeDetailsService#giveIncentives(com.sheetalarch.entity.EmployeeIncentives)
	 * Dec 1, 201712:19:34 PM
	 */
	@Override
	public void giveIncentives(EmployeeIncentives employeeIncentives) {
		// TODO Auto-generated method stub
		employeeDetailsDAO.giveIncentives(employeeIncentives);
	}

	/* (non-Javadoc)
	 * @see com.sheetalarch.service.EmployeeDetailsService#givePayment(com.sheetalarch.entity.EmployeeSalary)
	 * Dec 1, 201712:19:34 PM
	 */
	@Override
	public void givePayment(EmployeeSalary employeeSalary) {
		// TODO Auto-generated method stub
		employeeDetailsDAO.givePayment(employeeSalary);
	}

	/* (non-Javadoc)
	 * @see com.sheetalarch.service.EmployeeDetailsService#openPaymentModel(java.lang.String)
	 * Dec 1, 201712:19:34 PM
	 */
	@Override
	public EmployeePaymentModel openPaymentModel(String employeeDetailsId) {
		// TODO Auto-generated method stub
		return employeeDetailsDAO.openPaymentModel(employeeDetailsId);
	}

	/* (non-Javadoc)
	 * @see com.sheetalarch.service.EmployeeDetailsService#sendSMSTOEmployee(java.lang.String, java.lang.String)
	 * Dec 1, 201712:19:34 PM
	 */
	@Override
	public String sendSMSTOEmployee(String employeeDetailsIdList, String smsText) {
		// TODO Auto-generated method stub
		return employeeDetailsDAO.sendSMSTOEmployee(employeeDetailsIdList, smsText);
	}

	/* (non-Javadoc)
	 * @see com.sheetalarch.service.EmployeeDetailsService#fetchEmployeeDetail(java.lang.String)
	 * Dec 1, 201712:19:34 PM
	 */
	@Override
	public List<EmployeeViewModel> fetchEmployeeDetail(String employeeDetailId) {
		// TODO Auto-generated method stub
		return employeeDetailsDAO.fetchEmployeeDetail(employeeDetailId);
	}

	/* (non-Javadoc)
	 * @see com.sheetalarch.service.EmployeeDetailsService#fetchEmployeeDetailsByDepartmentId(long)
	 * Dec 1, 201712:19:34 PM
	 */
	@Override
	public List<EmployeeDetails> fetchEmployeeDetailsByDepartmentId(long deparmentId) {
		// TODO Auto-generated method stub
		return employeeDetailsDAO.fetchEmployeeDetailsByDepartmentId(deparmentId);
	}

	/* (non-Javadoc)
	 * @see com.sheetalarch.service.EmployeeDetailsService#fetchRouteByEmployeeId(long)
	 * Dec 1, 201712:19:34 PM
	 */
	@Override
	public List<Route> fetchRouteByEmployeeId(long employeeId) {
		// TODO Auto-generated method stub
		return employeeDetailsDAO.fetchRouteByEmployeeId(employeeId);
	}

	/* (non-Javadoc)
	 * @see com.sheetalarch.service.EmployeeDetailsService#updateSM_List_To_ASM_ForWebApp(com.sheetalarch.admin.models.SMListAssignToASMModel)
	 * Dec 5, 201711:35:40 AM
	 */
	@Override
	public void updateSM_List_To_ASM_ForWebApp(SMListAssignToASMModel sMListAssignToASMModel) {
		// TODO Auto-generated method stub
		employeeDetailsDAO.updateSM_List_To_ASM_ForWebApp(sMListAssignToASMModel);
	}

	/* (non-Javadoc)
	 * @see com.sheetalarch.service.EmployeeDetailsService#checkAreaAssignedToASM(long)
	 * Dec 5, 201712:33:54 PM
	 */
	@Override
	public String checkAreaAssignedToASM(long areaId) {
		// TODO Auto-generated method stub
		return employeeDetailsDAO.checkAreaAssignedToASM(areaId);
	}

	/* (non-Javadoc)
	 * @see com.sheetalarch.service.EmployeeDetailsService#checkAreaAssignedToASMUpdate(long, long)
	 * Dec 5, 201712:38:00 PM
	 */
	@Override
	public String checkAreaAssignedToASMUpdate(long areaId, long employeeDetailsId) {
		// TODO Auto-generated method stub
		return employeeDetailsDAO.checkAreaAssignedToASMUpdate(areaId, employeeDetailsId);
	}

	/* (non-Javadoc)
	 * @see com.sheetalarch.service.EmployeeDetailsService#checkRouteAssignedToSM(long)
	 * Dec 5, 20171:48:43 PM
	 */
	@Override
	public String checkRouteAssignedToSM(long routeId) {
		// TODO Auto-generated method stub
		return employeeDetailsDAO.checkRouteAssignedToSM(routeId);
	}

	/* (non-Javadoc)
	 * @see com.sheetalarch.service.EmployeeDetailsService#checkRouteAssignedToSMUpdate(long, long)
	 * Dec 5, 20171:48:43 PM
	 */
	@Override
	public String checkRouteAssignedToSMUpdate(long areaId, long employeeDetailsId) {
		// TODO Auto-generated method stub
		return employeeDetailsDAO.checkRouteAssignedToSMUpdate(areaId, employeeDetailsId);
	}

	/* (non-Javadoc)
	 * @see com.sheetalarch.service.EmployeeDetailsService#fetchSMReportModel(java.lang.String)
	 * Dec 11, 201711:00:43 AM
	 */
	@Override
	public SMReportModel fetchSMReportModel(String startDate,String endDate,String range,String type) {
		// TODO Auto-generated method stub
		return employeeDetailsDAO.fetchSMReportModel(startDate,endDate,range,type);
	}

	/* (non-Javadoc)
	 * @see com.sheetalarch.service.EmployeeDetailsService#fetchSMReportOrderDetails(java.lang.String, java.lang.String, java.lang.String, long)
	 * Dec 11, 201711:00:43 AM
	 */
	@Override
	public SMReportOrderDetails fetchSMReportOrderDetails(String startDate, String endDate, String range,
			long employeeDetailsId) {
		// TODO Auto-generated method stub
		return employeeDetailsDAO.fetchSMReportOrderDetails(startDate, endDate, range, employeeDetailsId);
	}

	/* (non-Javadoc)
	 * @see com.sheetalarch.service.EmployeeDetailsService#fetchAreaListByEmployeeId(long)
	 * Dec 13, 20173:36:53 PM
	 */
	@Override
	public List<Area> fetchAreaListByEmployeeId(long employeeId) {
		// TODO Auto-generated method stub
		return employeeDetailsDAO.fetchAreaListByEmployeeId(employeeId);
	}

	/* (non-Javadoc)
	 * @see com.sheetalarch.service.EmployeeDetailsService#fetchRouteListByAreaId(long)
	 * Dec 13, 20173:37:26 PM
	 */
	@Override
	public List<Route> fetchRouteListByAreaId(long areaId) {
		// TODO Auto-generated method stub
		return employeeDetailsDAO.fetchRouteListByAreaId(areaId);
	}

	/* (non-Javadoc)
	 * @see com.sheetalarch.service.EmployeeDetailsService#fetchClusterListByGkEmployeeId(long)
	 * Dec 13, 20173:50:01 PM
	 */
	@Override
	public List<Cluster> fetchClusterListByGkEmployeeId(long employeeId) {
		// TODO Auto-generated method stub
		return employeeDetailsDAO.fetchClusterListByGkEmployeeId(employeeId);
	}
	
	
	
	
	
}
