package com.sheetalarch.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.sheetalarch.admin.models.OrderReportModel;
import com.sheetalarch.admin.models.SalesManReportModel;
import com.sheetalarch.dao.OrderDetailsDAO;
import com.sheetalarch.entity.Area;
import com.sheetalarch.entity.DispatchOrderDetails;
import com.sheetalarch.entity.DispatchOrderProductDetails;
import com.sheetalarch.entity.Employee;
import com.sheetalarch.entity.OrderDetails;
import com.sheetalarch.entity.OrderProductDetails;
import com.sheetalarch.entity.ReOrderDetails;
import com.sheetalarch.entity.ReOrderProductDetails;
import com.sheetalarch.models.CancelOrderRequest;
import com.sheetalarch.models.DispatchProductNameAndQtyListResponse;
import com.sheetalarch.models.GkDispatchProductReportRequest;
import com.sheetalarch.models.GkDispatchProductReportResponse;
import com.sheetalarch.models.GkDispatchedOrderListResponse;
import com.sheetalarch.models.GkProductDetailsReportResponse;
import com.sheetalarch.models.OrderIssueRequest;
import com.sheetalarch.models.OrderListGKRequest;
import com.sheetalarch.models.OrderListResponse;
import com.sheetalarch.models.OrderRequest;
import com.sheetalarch.models.ReOrderDeliveryRequest;
import com.sheetalarch.service.OrderDetailsService;

@Component
@Transactional
@Service("orderDetailsService")
@Qualifier("orderDetailsService")
public class OrderDetailsServiceImpl implements OrderDetailsService {
	
	@Autowired
	OrderDetailsDAO orderDetailsDAO;

	@Override
	public String bookOrder(OrderRequest orderRequest) {
		// TODO Auto-generated method stub
		return orderDetailsDAO.bookOrder(orderRequest);
	}

	@Override
	public OrderDetails fetchOrderDetailsByOrderId(String orderDetailsId) {
		// TODO Auto-generated method stub
		return orderDetailsDAO.fetchOrderDetailsByOrderId(orderDetailsId);
	}

	@Override
	public void updateOrderDetailsPaymentDays(OrderDetails orderDetails) {
		// TODO Auto-generated method stub
		orderDetailsDAO.updateOrderDetailsPaymentDays(orderDetails);
	}

	/*
	 * @Override public List<OrderDetails>
	 * fetchOrderDetailsListForGK(OrderListGKRequest orderListGKRequest){ //
	 * TODO Auto-generated method stub return
	 * orderDetailsDAO.fetchOrderDetailsListForGK(orderListGKRequest); }
	 */

	@Override
	public List<OrderProductDetails> fetchOrderProductDetailsByOrderId(String orderId) {
		// TODO Auto-generated method stub
		return orderDetailsDAO.fetchOrderProductDetailsByOrderId(orderId);
	}

	@Override
	public List<OrderListResponse> fetchOrderDetailsListForGK1(OrderListGKRequest orderListGKRequest) {
		// TODO Auto-generated method stub
		return orderDetailsDAO.fetchOrderDetailsListForGK1(orderListGKRequest);
	}

	@Override
	public List<DispatchProductNameAndQtyListResponse> fetchOrderProductDetailsForDeliveryByEmpId(long employeeId) {
		// TODO Auto-generated method stub
		return orderDetailsDAO.fetchOrderProductDetailsForDeliveryByEmpId(employeeId);
	}

	@Override
	public void OrderConfirmForDelivery(long employeeId) {
		// TODO Auto-generated method stub
		orderDetailsDAO.OrderConfirmForDelivery(employeeId);
	}

	@Override
	public List<OrderDetails> remainingOrderListAndDeliveredOrderedListByEmpId(long employeeId, String status) {
		// TODO Auto-generated method stub
		return orderDetailsDAO.remainingOrderListAndDeliveredOrderedListByEmpId(employeeId, status);
	}

	@Override
	public void cancelOrder(CancelOrderRequest CancelOrderRequest) {
		// TODO Auto-generated method stub
		orderDetailsDAO.cancelOrder(CancelOrderRequest);

	}

	@Override
	public List<OrderDetails> fetchOrderListForReorder(long employeeId) {
		// TODO Auto-generated method stub
		return orderDetailsDAO.fetchOrderListForReorder(employeeId);
	}

	@Override
	public DispatchOrderDetails fetchDispatchOrderDetailsByemployeeId(long employeeId) {
		// TODO Auto-generated method stub
		return orderDetailsDAO.fetchDispatchOrderDetailsByemployeeId(employeeId);
	}

	@Override
	public void updateDispatchProduct(DispatchOrderDetails dispatchOrderDetailsList, OrderDetails orderDetails,
			Employee gKemployee, Area salemanArea) {
		// TODO Auto-generated method stub
		orderDetailsDAO.updateDispatchProduct(dispatchOrderDetailsList, orderDetails, gKemployee, salemanArea);
	}

	@Override
	public void updateDispatchProductDetails(List<OrderProductDetails> orderProductDetailsList,
			List<DispatchOrderProductDetails> dispatchOrderProductDetailsList,
			DispatchOrderDetails dispatchOrderDetails) {
		// TODO Auto-generated method stub
		orderDetailsDAO.updateDispatchProductDetails(orderProductDetailsList, dispatchOrderProductDetailsList,
				dispatchOrderDetails);
	}

	@Override
	public List<DispatchOrderProductDetails> fetchDispatchOrderProductListByDispatchId(long dispatchedId) {
		// TODO Auto-generated method stub
		return orderDetailsDAO.fetchDispatchOrderProductListByDispatchId(dispatchedId);
	}

	@Override
	public DispatchOrderDetails fetchDispatchOrderDetailsByemployeeIdAndDeliveryDate1(long employeeId,
			String deliveryDate) {
		// TODO Auto-generated method stub
		return orderDetailsDAO.fetchDispatchOrderDetailsByemployeeIdAndDeliveryDate1(employeeId, deliveryDate);
	}

	@Override
	public List<GkDispatchedOrderListResponse> fetchDispatchedOrderDetailsListForGK(
			OrderListGKRequest orderListGKRequest) {
		// TODO Auto-generated method stub
		return orderDetailsDAO.fetchDispatchedOrderDetailsListForGK(orderListGKRequest);
	}

	@Override
	public String packedOrderForDelivery(OrderIssueRequest orderIssueRequest) {
		// TODO Auto-generated method stub
		return orderDetailsDAO.packedOrderForDelivery(orderIssueRequest);
	}

	@Override
	public DispatchOrderDetails fetchDispatchOrderDetailsByDispatchedId(long dispatchedId) {
		// TODO Auto-generated method stub
		return orderDetailsDAO.fetchDispatchOrderDetailsByDispatchedId(dispatchedId);
	}

	@Override
	public DispatchOrderDetails fetchDispatchOrderForDeliveryByempId(long employeeId) {
		// TODO Auto-generated method stub
		return orderDetailsDAO.fetchDispatchOrderForDeliveryByempId(employeeId);
	}

	@Override
	public void confirmOrderForDelivery(long employeeId, long dispatchedId) {
		// TODO Auto-generated method stub
		orderDetailsDAO.confirmOrderForDelivery(employeeId, dispatchedId);
	}

	@Override
	public void deliveryOfReOrderProduct(ReOrderDeliveryRequest reOrderDeliveryRequest) {
		// TODO Auto-generated method stub
		orderDetailsDAO.deliveryOfReOrderProduct(reOrderDeliveryRequest);	
	}

	@Override
	public List<OrderProductDetails> fetchOrderProductDetailsForReOrderByOrderId(List<OrderProductDetails> orderProductDetails,List<ReOrderProductDetails> reOrderProductDetails) {
		// TODO Auto-generated method stub
		return orderDetailsDAO.fetchOrderProductDetailsForReOrderByOrderId(orderProductDetails, reOrderProductDetails);
	}

	@Override
	public List<ReOrderProductDetails> fecthReOrderProductDetailsByOrderId(String orderId) {
		// TODO Auto-generated method stub
		return orderDetailsDAO.fecthReOrderProductDetailsByOrderId(orderId);
	}

	@Override
	public List<DispatchOrderDetails> fetchDispatchOrderDetailsForGkReportByClusterIdAndDateRange(
			GkDispatchProductReportRequest gkDispatchProductReportRequest) {
		// TODO Auto-generated method stub
		return orderDetailsDAO.fetchDispatchOrderDetailsForGkReportByClusterIdAndDateRange(gkDispatchProductReportRequest);
	}

	@Override
	public List<GkDispatchProductReportResponse> fetchDispatchOrderDetailsListForGkReportByClusterIdAndDateRange(
			GkDispatchProductReportRequest gkDispatchProductReportRequest) {
		// TODO Auto-generated method stub
		return orderDetailsDAO.fetchDispatchOrderDetailsListForGkReportByClusterIdAndDateRange(gkDispatchProductReportRequest);
	}

	@Override
	public List<GkProductDetailsReportResponse> fetchDispatchProductDetailsListForGkReportByDateRange(long employeeId,
			String fromDate, String toDate, String range) {
		// TODO Auto-generated method stub
		return orderDetailsDAO.fetchDispatchProductDetailsListForGkReportByDateRange(employeeId, fromDate, toDate, range);
	}

	@Override
	public void updateDispatchProductNotReceivingReason(long dispatchedId, String reason) {
		// TODO Auto-generated method stub
		orderDetailsDAO.updateDispatchProductNotReceivingReason(dispatchedId, reason);
	}

	@Override
	public List<ReOrderProductDetails> fetchReOrderProductDetailsByReOrderId(String reOrderDetailsId) {
		// TODO Auto-generated method stub
		return orderDetailsDAO.fetchReOrderProductDetailsByReOrderId(reOrderDetailsId);
	}

	@Override
	public ReOrderDetails fetchReOrderDetailsByReOrderId(String reOrderDetailsId) {
		// TODO Auto-generated method stub
		return orderDetailsDAO.fetchReOrderDetailsByReOrderId(reOrderDetailsId);
	}

	/* (non-Javadoc)
	 * @see com.sheetalarch.service.OrderDetailsService#sendSMSTOShopsUsingOrderId(java.lang.String, java.lang.String)
	 * Dec 8, 20176:48:07 PM
	 */
	@Override
	public String sendSMSTOShopsUsingOrderId(String orderIds, String smsText) {
		// TODO Auto-generated method stub
		return orderDetailsDAO.sendSMSTOShopsUsingOrderId(orderIds, smsText);
	}

	/* (non-Javadoc)
	 * @see com.sheetalarch.service.OrderDetailsService#fetchReOrderDetailsByOrderId(java.lang.String)
	 * Dec 8, 20176:48:07 PM
	 */
	@Override
	public ReOrderDetails fetchReOrderDetailsByOrderId(String reOrderDetailsId) {
		// TODO Auto-generated method stub
		return orderDetailsDAO.fetchReOrderDetailsByOrderId(reOrderDetailsId);
	}

	/* (non-Javadoc)
	 * @see com.sheetalarch.service.OrderDetailsService#fetchSalesManReportModel(java.lang.String, java.lang.String, java.lang.String)
	 * Dec 12, 20176:51:27 PM
	 */
	@Override
	public List<SalesManReportModel> fetchSalesManReportModel(String startDate, String endDate, String range) {
		// TODO Auto-generated method stub
		return orderDetailsDAO.fetchSalesManReportModel(startDate, endDate, range);
	}

	/* (non-Javadoc)
	 * @see com.sheetalarch.service.OrderDetailsService#fetchOrderReport(java.lang.String)
	 * Dec 12, 20178:20:53 PM
	 */
	@Override
	public List<OrderReportModel> fetchOrderReport(String currrentPending) {
		// TODO Auto-generated method stub
		return orderDetailsDAO.fetchOrderReport(currrentPending);
	}

}
