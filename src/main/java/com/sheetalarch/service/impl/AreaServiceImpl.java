package com.sheetalarch.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.sheetalarch.dao.AreaDAO;
import com.sheetalarch.entity.Area;
import com.sheetalarch.entity.Cluster;
import com.sheetalarch.entity.Employee;
import com.sheetalarch.service.AreaService;

@Component
@Transactional
@Service("areaService")
@Qualifier("areaService")
public class AreaServiceImpl implements AreaService {

	@Autowired
	AreaDAO  areaDAO;
	
	@Override
	public Cluster fetchClusterBySalesmanId(long employeeId) {
		// TODO Auto-generated method stub
		return areaDAO.fetchClusterBySalesmanId(employeeId);
	}

	@Override
	public Employee fetchGKEmployeeByClusterId(long clusterId) {
		// TODO Auto-generated method stub
		return areaDAO.fetchGKEmployeeByClusterId(clusterId);
	}

	@Override
	public Area fetchAreaBysalesmanId(long employeeId) {
		// TODO Auto-generated method stub
		return areaDAO.fetchAreaBysalesmanId(employeeId);
	}

	/* (non-Javadoc)
	 * @see com.sheetalarch.service.AreaService#fetchAreaListByClusterId(long)
	 * Nov 29, 20176:06:04 PM
	 */
	@Override
	public List<Area> fetchAreaListByClusterId(long clusterId) {
		// TODO Auto-generated method stub
		return areaDAO.fetchAreaListByClusterId(clusterId);
	}

	/* (non-Javadoc)
	 * @see com.sheetalarch.service.AreaService#fetchAreaList()
	 * Nov 30, 20171:17:49 PM
	 */
	@Override
	public List<Area> fetchAreaList() {
		// TODO Auto-generated method stub
		return areaDAO.fetchAreaList();
	}

	/* (non-Javadoc)
	 * @see com.sheetalarch.service.AreaService#saveArea(com.sheetalarch.entity.Area)
	 * Nov 30, 20171:17:49 PM
	 */
	@Override
	public void saveArea(Area area) {
		// TODO Auto-generated method stub
		areaDAO.saveArea(area);
	}

	/* (non-Javadoc)
	 * @see com.sheetalarch.service.AreaService#updateArea(com.sheetalarch.entity.Area)
	 * Nov 30, 20171:17:49 PM
	 */
	@Override
	public void updateArea(Area area) {
		// TODO Auto-generated method stub
		areaDAO.updateArea(area);
	}

	/* (non-Javadoc)
	 * @see com.sheetalarch.service.AreaService#fetchAreaByAreaId(long)
	 * Dec 1, 20173:49:36 PM
	 */
	@Override
	public Area fetchAreaByAreaId(long areaId) {
		// TODO Auto-generated method stub
		return areaDAO.fetchAreaByAreaId(areaId);
	}

	/* (non-Javadoc)
	 * @see com.sheetalarch.service.AreaService#fetchClusterBtRouteId(long)
	 * Dec 13, 20173:49:15 PM
	 */
	@Override
	public Cluster fetchClusterBtRouteId(long routeId) {
		// TODO Auto-generated method stub
		return areaDAO.fetchClusterBtRouteId(routeId);
	}
	

}
