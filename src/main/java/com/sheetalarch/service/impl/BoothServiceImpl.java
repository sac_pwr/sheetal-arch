package com.sheetalarch.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.sheetalarch.admin.models.CustomerRerportModel;
import com.sheetalarch.admin.models.ManageBoothListModel;
import com.sheetalarch.admin.models.SingleCustomerReport;
import com.sheetalarch.dao.BoothDAO;
import com.sheetalarch.entity.Booth;
import com.sheetalarch.service.BoothService;

@Component
@Transactional
@Service("boothService")
@Qualifier("boothService")
public class BoothServiceImpl implements BoothService {

	@Autowired
	BoothDAO boothDAO;
	
	
	@Override
	public List<Booth> fetchBoothList() {
		// TODO Auto-generated method stub
		return boothDAO.fetchBoothList();
	}


	@Override
	public void saveBooth(Booth booth) {
		// TODO Auto-generated method stub
		boothDAO.saveBooth(booth);
		
	}


	@Override
	public Booth fetchBoothByBoothId(long boothNo) {
		// TODO Auto-generated method stub
		return boothDAO.fetchBoothByBoothId(boothNo);
	}


	@Override
	public void updateBooth(Booth booth) {
		// TODO Auto-generated method stub
		boothDAO.updateBooth(booth);
	}


	/* (non-Javadoc)
	 * @see com.sheetalarch.service.BoothService#fetchManageBoothList()
	 * Dec 5, 20176:09:19 PM
	 */
	@Override
	public List<ManageBoothListModel> fetchManageBoothList() {
		// TODO Auto-generated method stub
		return boothDAO.fetchManageBoothList();
	}


	/* (non-Javadoc)
	 * @see com.sheetalarch.service.BoothService#sendSMSTOShops(java.lang.String, java.lang.String)
	 * Dec 5, 20177:54:20 PM
	 */
	@Override
	public String sendSMSTOShops(String shopsId, String smsText) {
		// TODO Auto-generated method stub
		return boothDAO.sendSMSTOShops(shopsId, smsText);
	}


	/* (non-Javadoc)
	 * @see com.sheetalarch.service.BoothService#fetchSingleCustomerReport(java.lang.String, java.lang.String, java.lang.String, long)
	 * Dec 12, 20174:00:24 PM
	 */
	@Override
	public SingleCustomerReport fetchSingleCustomerReport(String startDate, String endDate, String range,
			long boothId) {
		// TODO Auto-generated method stub
		return boothDAO.fetchSingleCustomerReport(startDate, endDate, range, boothId);
	}


	/* (non-Javadoc)
	 * @see com.sheetalarch.service.BoothService#fetchCustomerReport(java.lang.String, java.lang.String, java.lang.String)
	 * Dec 12, 20174:00:24 PM
	 */
	@Override
	public List<CustomerRerportModel> fetchCustomerReport(String startDate, String endDate, String range) {
		// TODO Auto-generated method stub
		return boothDAO.fetchCustomerReport(startDate, endDate, range);
	}


	/* (non-Javadoc)
	 * @see com.sheetalarch.service.BoothService#fetchBoothByBoothNo(java.lang.String)
	 * Dec 13, 20173:34:57 PM
	 */
	@Override
	public Booth fetchBoothByBoothNo(String boothNo) {
		// TODO Auto-generated method stub
		return boothDAO.fetchBoothByBoothNo(boothNo);
	}


	/* (non-Javadoc)
	 * @see com.sheetalarch.service.BoothService#fetchBoothListByEmployeeId(long)
	 * Dec 13, 20173:41:48 PM
	 */
	@Override
	public List<Booth> fetchBoothListByEmployeeId(long employeeId) {
		// TODO Auto-generated method stub
		return boothDAO.fetchBoothListByEmployeeId(employeeId);
	}

}
