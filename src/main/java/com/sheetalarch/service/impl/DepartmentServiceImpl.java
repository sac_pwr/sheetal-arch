/**
 * 
 */
package com.sheetalarch.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.sheetalarch.dao.DepartmentDAO;
import com.sheetalarch.entity.Department;
import com.sheetalarch.service.DepartmentService;

/**
 * @author aNKIT
 *
 */
@Component
@Transactional
@Service("departmentService")
@Qualifier("departmentService")
public class DepartmentServiceImpl implements DepartmentService{

	@Autowired
	DepartmentDAO departmentDAO;
	
	/* (non-Javadoc)
	 * @see com.sheetalarch.service.DepartmentService#saveDepartment(com.sheetalarch.entity.Department)
	 * Nov 28, 20172:38:19 PM
	 */
	@Override
	public String saveDepartment(Department department) {
		// TODO Auto-generated method stub
		return departmentDAO.saveDepartment(department);
	}

	/* (non-Javadoc)
	 * @see com.sheetalarch.service.DepartmentService#updateDepartment(com.sheetalarch.entity.Department)
	 * Nov 28, 20172:38:19 PM
	 */
	@Override
	public String updateDepartment(Department department) {
		// TODO Auto-generated method stub
		return departmentDAO.updateDepartment(department);
	}

	/* (non-Javadoc)
	 * @see com.sheetalarch.service.DepartmentService#fetchDepartmentList()
	 * Nov 28, 20172:38:19 PM
	 */
	@Override
	public List<Department> fetchDepartmentList() {
		// TODO Auto-generated method stub
		return departmentDAO.fetchDepartmentList();
	}

	/* (non-Javadoc)
	 * @see com.sheetalarch.service.DepartmentService#fetchDepartment(long)
	 * Nov 28, 20172:47:42 PM
	 */
	@Override
	public Department fetchDepartment(long departmentId) {
		// TODO Auto-generated method stub
		return departmentDAO.fetchDepartment(departmentId);
	}

}
