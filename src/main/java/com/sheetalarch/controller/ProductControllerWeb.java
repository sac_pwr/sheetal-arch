package com.sheetalarch.controller;

import java.io.IOException;
import java.io.OutputStream;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.io.IOUtils;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import com.sheetalarch.admin.models.ProductReportView;
import com.sheetalarch.admin.models.ProductViewList;
import com.sheetalarch.entity.Brand;
import com.sheetalarch.entity.Categories;
import com.sheetalarch.entity.Product;
import com.sheetalarch.service.BrandService;
import com.sheetalarch.service.CategoriesService;
import com.sheetalarch.service.ProductService;
import com.sheetalarch.utils.Constants;

@Controller
public class ProductControllerWeb {

	@Autowired
	CategoriesService categoriesService;

	@Autowired
	BrandService brandService;
	
	@Autowired
	ProductService productService;
	
	@Autowired
	Product product;
	
	@Autowired
	Brand brand;
	
	@Autowired
	Categories categories;
	
	@RequestMapping("/fetchProductList")
	public ModelAndView fetchProductList(Model model,HttpSession session) {

		System.out.println("in fechProductList controller");
		model.addAttribute("pageName", "Manage Product");
		
		
		List<ProductViewList> productList=productService.fetchProductViewListForWebApp();
		
		model.addAttribute("productlist", productList);
		model.addAttribute("saveMsg", session.getAttribute("saveMsg"));		
		session.setAttribute("saveMsg", null);
		return new ModelAndView("ManageProduct");
	}
	
	@RequestMapping("/fetchProduct")
	public ModelAndView fechProduct(Model model,HttpServletRequest request,HttpSession session) {

		System.out.println("in fechProductList controller");
		
		model.addAttribute("pageName", "Update Product");
		
		
		
		long productId=Long.parseLong(request.getParameter("productId"));
		product=productService.fetchProductForWebApp(productId);
		
		model.addAttribute("product", product);	
		model.addAttribute("isProductImg", (product.getProductImage()==null)?false:true);
		List<Brand> brandList=brandService.fetchBrandListForWebApp();
		List<Categories> categoryList=categoriesService.fetchCategoriesListForWebApp();
		
		model.addAttribute("brandlist", brandList);
		model.addAttribute("categorylist", categoryList);
		
		return new ModelAndView("updateProducts");
	}
	
	@RequestMapping("/addProduct")
	public ModelAndView addProduct(Model model,HttpSession session) {

		System.out.println("in addProduct controller");
		
		model.addAttribute("pageName", "Add Product");
		
		
		
		List<Brand> brandList=brandService.fetchBrandListForWebApp();
		List<Categories> categoryList=categoriesService.fetchCategoriesListForWebApp();
		
		model.addAttribute("brandlist", brandList);
		model.addAttribute("categorylist", categoryList);
		
		
		return new ModelAndView("addProducts");
	}
	
	@RequestMapping(value = "/uploadProductExcel", method = RequestMethod.POST)
    public String processExcel(Model model, @RequestParam("file") MultipartFile file) {
        try {
            List<Product> productList = new ArrayList<>();
            int i = 1;
            // Creates a workbook object from the uploaded excelfile
            XSSFWorkbook workbook = new XSSFWorkbook(file.getInputStream());
            // Creates a worksheet object representing the first sheet
            XSSFSheet worksheet = workbook.getSheetAt(0);
            // Reads the data in excel file until last row is encountered
            while (i <= worksheet.getLastRowNum()) 
            {
                // Creates an object for the UserInfo Model
                Product product = new Product();
                // Creates an object representing a single row in excel
                XSSFRow row = worksheet.getRow(i++);
                
                product.setProductName(row.getCell(0).getStringCellValue());
        		
        		product.setProductCode(row.getCell(1).getStringCellValue());
        		//this.product.setProductImage(productImage);
        		brand.setBrandId((int)row.getCell(3).getNumericCellValue());
        		product.setBrand(brand);		
        		categories=categoriesService.fetchCategoriesForWebApp((int)row.getCell(2).getNumericCellValue());
        		product.setCategories(categories);
        		product.setRate((float)row.getCell(4).getNumericCellValue());

        		product.setInterRate((product.getRate()*categories.getIgst())/100);
        		product.setIntraRate((product.getRate()*categories.getIgst())/100);
        		
        		product.setThreshold((int)row.getCell(5).getNumericCellValue());
        		product.setProductAddedDatetime(new Date());
        		product.setCurrentQuantity(0);
        		product.setProductQuantityUpdatedDatetime(new Date());

                // persist data into database in here
                productList.add(product);
            }
            workbook.close();
            
            productService.saveMultipleProductForWebApp(productList);
            
        } catch (Exception e) {
            e.printStackTrace();
        }

        return "upload_file";
    }
	
	
	@RequestMapping(value = "/saveProduct", method = RequestMethod.POST)
	public ModelAndView saveCountryForWeb(@ModelAttribute("file") MultipartFile file,HttpSession session ,HttpServletRequest request,Model model)  {
		System.out.println("in Save Product");		
		
		this.product.setProductName(request.getParameter("productname"));
		
		this.product.setProductCode(request.getParameter("productcode"));
		//this.product.setProductImage(productImage);
		this.brand.setBrandId(Long.parseLong(request.getParameter("brandId")));
		this.product.setBrand(brand);		
		this.categories=categoriesService.fetchCategoriesForWebApp(Long.parseLong(request.getParameter("categoryId")));
		this.product.setCategories(categories);
		this.product.setRate(Float.parseFloat(request.getParameter("productRate")));

		this.product.setInterRate(this.product.getRate()*this.categories.getIgst());
		this.product.setIntraRate(this.product.getRate()*this.categories.getIgst());
		
		this.product.setThreshold(Long.parseLong(request.getParameter("thresholvalue")));
		this.product.setProductAddedDatetime(new Date());
		this.product.setCurrentQuantity(0);
		this.product.setProductQuantityUpdatedDatetime(new Date());
		this.product.setProductUnit(request.getParameter("unitType"));
		
		
		this.product.setProductName((Character.toString(this.product.getProductName().charAt(0)).toUpperCase() + this.product.getProductName().substring(1)));
		boolean flag = false;
		List<Product> productList = productService.fetchProductListForWebApp();
		if(productList!=null){
			for (Product productFromDb : productList) {
				if (productFromDb.getProductName().trim().toUpperCase().equals(product.getProductName().trim().toUpperCase())) {
					flag = true;
					break;
				}
			}
		}
			
		if(flag==true)
		{ 
			session.setAttribute("saveMsg", "Product Name "+Constants.ALREADY_EXIST);
			return new ModelAndView("redirect:/fetchProductList");		
		}
		
		productService.saveProductForWebApp(file, this.product);
		session.setAttribute("saveMsg", Constants.SAVE_SUCCESS);
		return new ModelAndView("redirect:/fetchProductList");
		
	}
	
	@RequestMapping(value = "/updateProduct", method = RequestMethod.POST)
	public ModelAndView updateCountryForWeb(@ModelAttribute("file") MultipartFile file,HttpSession session,HttpServletRequest request,Model model) {

			System.out.println("in Update Country");

			this.product.setProductId(Long.parseLong(request.getParameter("productId")));
			
			if (product.getProductId() == 0) {
				System.out.println("con not update because 0 city not available");
				
				session.setAttribute("saveMsg", "Re-Click to Edit Button");
				return new ModelAndView("redirect:/fetchProductList");
			}
			
			Product prdct=productService.fetchProductForWebApp(Long.parseLong(request.getParameter("productId")));
		
			this.product.setProductName(request.getParameter("productname"));
			this.product.setProductCode(request.getParameter("productcode"));
			//this.product.setProductImage(productImage);
			this.brand.setBrandId(Long.parseLong(request.getParameter("brandId")));
			this.product.setBrand(brand);
			this.categories=categoriesService.fetchCategoriesForWebApp(Long.parseLong(request.getParameter("categoryId")));
			this.product.setCategories(categories);
			this.product.setRate(Float.parseFloat(request.getParameter("productRate")));

			this.product.setInterRate(this.product.getRate()*this.categories.getIgst());
			this.product.setIntraRate(this.product.getRate()*this.categories.getIgst());
			
			this.product.setThreshold(Long.parseLong(request.getParameter("thresholvalue")));
			this.product.setProductAddedDatetime(prdct.getProductAddedDatetime());
			this.product.setCurrentQuantity(prdct.getCurrentQuantity());
			this.product.setProductQuantityUpdatedDatetime(new Date());
			this.product.setProductUnit(request.getParameter("unitType"));
			
			this.product.setProductName((Character.toString(this.product.getProductName().charAt(0)).toUpperCase() + this.product.getProductName().substring(1)));
			boolean flag = false;
			List<Product> productList = productService.fetchProductListForWebApp();
			if(productList!=null){
				for (Product productFromDb : productList) {
					if (productFromDb.getProductName().trim().toUpperCase().equals(product.getProductName().trim().toUpperCase()) && prdct.getProductId()!=this.product.getProductId()) {
						flag = true;
						break;
					}
				}
			}
				
			if(flag==true)
			{ 
				session.setAttribute("saveMsg", "Product Name "+Constants.ALREADY_EXIST);
				return new ModelAndView("redirect:/fetchProductList");		
			}			
			
			product.setProductName((Character.toString(product.getProductName().charAt(0)).toUpperCase() + product.getProductName().substring(1)));
			
			if(file.isEmpty())
			{
				this.product.setProductImage(prdct.getProductImage());
			}
			
			productService.updateProductForWebApp(file, this.product);

			session.setAttribute("saveMsg", Constants.UPDATE_SUCCESS);
			return new ModelAndView("redirect:/fetchProductList");

	}
	
	@RequestMapping("/downloadProductImage/{productId}")
	public ModelAndView downloadAdvertImage(@PathVariable("productId") long productId, HttpServletResponse response,HttpSession session) {

		
		  
		try {
			this.product = productService.fetchProductForWebApp(productId);
		
			response.setHeader("Content-Disposition", "inline;filename=\"" + this.product.getProductName() + "\"");
			OutputStream out = response.getOutputStream();
			response.setContentType("image/png");
			IOUtils.copy(this.product.getProductImage().getBinaryStream(), out);
			out.flush();
			out.close();
			System.out.println("get image" + this.product.getProductName());
		} catch (IOException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return null;
	}

	@RequestMapping("/fetchProductByProductId")
	public @ResponseBody Product fetchProductByProductId(Model model,HttpServletRequest request) {

		System.out.println("in fechProductList controller");
		
		long productId=Long.parseLong(request.getParameter("productId"));
		product=productService.fetchProductForWebApp(productId);
		product.setProductImage(null);	
		return product;
	}
	
	@RequestMapping("/fetchProductListForReport")
	public ModelAndView fetchProductListForReport(Model model,HttpServletRequest request,HttpSession session) {
		model.addAttribute("pageName", "Product Report");
		
		
		List<ProductReportView> productReportViews=productService.fetchProductListForReport(request.getParameter("range"), request.getParameter("startDate"), request.getParameter("endDate"),request.getParameter("topProductNo"));
		model.addAttribute("productReportViews", productReportViews);
		double totalAmountWithTax=0;
		for(ProductReportView productReportView:productReportViews)
		{
			totalAmountWithTax+=productReportView.getTotalAmountWithTax();
		}
		model.addAttribute("totalAmountWithTax", totalAmountWithTax);
		
		List<Brand> brandList=brandService.fetchBrandListForWebApp();
		model.addAttribute("brandlist", brandList);
		return new ModelAndView("ProductReport");
	}
	
}
