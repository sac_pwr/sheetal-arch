/**
 * 
 */
package com.sheetalarch.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.sheetalarch.admin.models.CollectionReportMain;
import com.sheetalarch.service.OrderDetailsService;
import com.sheetalarch.service.PaymentService;



/**
 * @author aNKIT
 *
 */

@Controller
public class PaymentControllerWeb {

	@Autowired
	PaymentService paymentService;
	
	@Autowired
	OrderDetailsService orderDetailsService;
	
	@RequestMapping("/getCollectionReportDetails")
	public ModelAndView getCollectionReportDetails(Model model,HttpServletRequest request,HttpSession session){
		
		model.addAttribute("pageName", "Collection Report");
		  
		String startDate=request.getParameter("startDate");
		String endDate=request.getParameter("endDate");
		String range=request.getParameter("range");
		
		CollectionReportMain collectionReportMain=paymentService.getCollectionReportDetails(startDate, endDate, range);
		model.addAttribute("collectionReportMain", collectionReportMain);
		
		return new ModelAndView("CollectionReport");
	}
	
	@RequestMapping("/sendSMSTOShopsUsingOrderId")
	public @ResponseBody String sendSMSTOShopsUsingOrderId(HttpServletRequest request) {
		
		try {
			String shopsIds=request.getParameter("shopsIds");		
			String smsText=request.getParameter("smsText");	
			orderDetailsService.sendSMSTOShopsUsingOrderId(shopsIds, smsText);
			
			return "Success";
		} catch (Exception e) {
			System.out.println(e.toString());
			return "Failed";
		}
	}	
	
}
