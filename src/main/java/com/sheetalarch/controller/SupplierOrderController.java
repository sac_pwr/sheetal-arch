/**
 * 
 */
package com.sheetalarch.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;


import com.sheetalarch.entity.SupplierOrder;
import com.sheetalarch.entity.SupplierOrderDetails;
import com.sheetalarch.service.SupplierOrderService;
import com.sheetalarch.service.SupplierService;



/**
 * @author aNKIT
 *
 */
@Controller
public class SupplierOrderController {

	@Autowired
	SupplierOrderService supplierOrderService; 
		
	@Autowired
	SupplierOrder supplierOrder;
	
	@Autowired
	SupplierOrderDetails supplierOrderDetails;
	
	@Autowired
	SupplierService supplierService;
	
	@RequestMapping("/SupplierOrderProducts")
	public ModelAndView SupplierOrderProducts(HttpServletRequest request,Model model,HttpSession session) {
		
		model.addAttribute("pageName", "Supplier Order");
		
		String orderStatus=request.getParameter("orderStatus");
		if(orderStatus.equals("edit"))
		{
			model.addAttribute("url", "editOrderBook");
			model.addAttribute("supplierOrderIddd", request.getParameter("supplierOrderIddd"));			
		}
		else
		{
			model.addAttribute("url", "orderBook");
			model.addAttribute("supplierOrderIddd", "");
		}
		
		
		
		return new ModelAndView("SupplierOrderProducts");
	}
	
	@RequestMapping("/orderBook")
	public @ResponseBody String orderBook(HttpServletRequest request,HttpSession session) {
		System.out.println("inside order book");
		try {
				supplierOrderService.saveSupplierOrder(request.getParameter("productIdList"));
		} catch (Exception e) {
			return "Failed";
		}
		
		return "Success"; 
	}
	@RequestMapping("/editOrderBook")
	public @ResponseBody String editOrderBook(HttpServletRequest request) {
		System.out.println("inside editOrderBook");
		try {
			supplierOrder=supplierOrderService.fetchSupplierOrder(request.getParameter("supplierOrderId"));
			supplierOrderService.editSupplierOrder(request.getParameter("productIdList"),supplierOrder);
		} catch (Exception e) {
			return "Failed";
		}
		
		return "EditSuccess"; 
	}

	@RequestMapping("/fetchLast24HoursOrdersForEdit")
	public ModelAndView fetchLast24HoursOrdersForEdit(Model model,HttpServletRequest request,HttpSession session) {
		
		System.out.println("in fetchLast24HoursOrdersForEdit controller");
		
		model.addAttribute("pageName", "Editable Supplier Order List");
				
		List<SupplierOrder> supplierOrdersList=supplierOrderService.fetchSupplierOrders24hour("CurrentDay","","");
		model.addAttribute("last24Order", supplierOrdersList);
		
		return new ModelAndView("last24OrderDetailsForEdit");
	}
	
	@RequestMapping("/showSupplierOrderReport")
	public ModelAndView showSupplierOrderReport(Model model,HttpSession session) {
		
		model.addAttribute("pageName", "Supplier Oreder Report");
		
		
		List<SupplierOrder> supplierOrderLists= supplierOrderService.fetchSupplierOrderList();
		model.addAttribute("supplierOrderLists", supplierOrderLists);
		
		return new ModelAndView("SupplierOrderView");
	}
	
	@RequestMapping("/fetchSupplierOrderDetails")
	public @ResponseBody List<SupplierOrderDetails> showSupplierOrderDetails(HttpServletRequest request,Model model) {
		
		List<SupplierOrderDetails> supplierOrderDetailsLists= supplierOrderService.fetchSupplierOrderDetailsList(request.getParameter("supplierOrderId"));
				
		model.addAttribute("supplierOrderDetailsLists", supplierOrderDetailsLists);
		
		return supplierOrderDetailsLists;
	}
	
	@RequestMapping("/fetchProductBySupplierOrderId")
	public @ResponseBody List<SupplierOrderDetails> fetchSupplierOrderDetailsList(Model model,HttpServletRequest request) {

		System.out.println("in fetchProductBySupplierOrderId controller");
		
		List<SupplierOrderDetails> supplierOrderDetailList=supplierOrderService.fetchSupplierOrderDetailsListBySupplierOrderId(request.getParameter("supplierOrderId"));
			
		return supplierOrderDetailList;
	}
	@RequestMapping("/editSupplierOrderAjax")
	public @ResponseBody List<SupplierOrderDetails> editSupplierOrderAjax(Model model,HttpServletRequest request) {

		System.out.println("in editSupplierOrder controller");
		
		String supplierOrderId=request.getParameter("supplierOrderId");
		
		List<SupplierOrderDetails> supplierOrderDetaillist=supplierOrderService.fetchSupplierOrderDetailsList(supplierOrderId);
		
		return supplierOrderDetaillist;
	}
}
