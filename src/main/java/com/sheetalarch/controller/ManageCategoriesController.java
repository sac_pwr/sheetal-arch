package com.sheetalarch.controller;

import java.text.DecimalFormat;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.sheetalarch.entity.Categories;
import com.sheetalarch.entity.Product;
import com.sheetalarch.service.CategoriesService;
import com.sheetalarch.service.ProductService;
import com.sheetalarch.utils.Constants;


@Controller
public class ManageCategoriesController {

	@Autowired
	CategoriesService categoriesService;
	
	@Autowired
	ProductService productService;
	
	@Autowired
	Categories categories;
	
	@RequestMapping("/fetchCategoriesList")
	public ModelAndView fetchCategoriesList(Model model,HttpSession session) {
		System.out.println("in fetchCategoriesList controller");
		
		model.addAttribute("pageName", "Manage Category");
		
		List<Categories> categoriesList=categoriesService.fetchCategoriesListForWebApp();
		model.addAttribute("categorieslist", categoriesList);
		model.addAttribute("saveMsg", session.getAttribute("saveMsg"));
		session.setAttribute("saveMsg", "");
		return new ModelAndView("ManageCategories");
	}
	
	@RequestMapping("/fetchCategories")
	public @ResponseBody Categories fetchCategories(Model model,HttpServletRequest request,HttpSession session) {
		System.out.println("in fetchCategories controller");
		
		long categoriesId=Long.parseLong(request.getParameter("categoriesId"));
		Categories Categories=categoriesService.fetchCategoriesForWebApp(categoriesId);
		
		return Categories;
	}
	
		//saveCountry
		@RequestMapping("/saveCategories")
		public ModelAndView saveCategoriesForWeb(HttpServletRequest request,Model model,HttpSession session)  {
			System.out.println("in Save Categories");		
			
			
			
			this.categories.setCategoryId(Long.parseLong(request.getParameter("categoriesId")));
			this.categories.setCategoryName(request.getParameter("categoryName"));
			this.categories.setHsnCode(request.getParameter("hsnCode"));
			this.categories.setIgst(Float.parseFloat(request.getParameter("igst")));
			this.categories.setCgst(Float.parseFloat(request.getParameter("cgst")));
			this.categories.setSgst(Float.parseFloat(request.getParameter("sgst")));
			session.setAttribute("saveMsg", "");
						
			categories.setCategoryName((Character.toString(categories.getCategoryName().charAt(0)).toUpperCase() + categories.getCategoryName().substring(1)));
			
			
			
			boolean flag = false;
			List<Categories> CategoriesList = categoriesService.fetchCategoriesListForWebApp();
			if(CategoriesList!=null){
				for (Categories categoriesFromDb : CategoriesList) {
					if (categoriesFromDb.getCategoryName().trim().toUpperCase().equals(categories.getCategoryName().trim().toUpperCase())) {
						flag = true;
						break;
					}
				}
			}
			
			if (categories.getCategoryId() == 0 ) 
			{
				if(flag == false)
				{
					categories.setCategoryDate(new Date());
					categoriesService.saveCategoriesForWebApp(categories);
					session.setAttribute("saveMsg", Constants.SAVE_SUCCESS);
					return new ModelAndView("redirect:/fetchCategoriesList");
				}
			}
			
			if (categories.getCategoryId() != 0 ) {
				
				flag = false;
				if(CategoriesList!=null){
					for (Categories categoriesFromDb : CategoriesList) {
						if ( categoriesFromDb.getCategoryName().trim().toUpperCase().equals(categories.getCategoryName().trim().toUpperCase()) 	
							&& categoriesFromDb.getCategoryId()!=categories.getCategoryId()) {
							flag = true;
							break;
						}
					}
				}
				
				if(flag == false)
				{
					System.out.println("Moving request for Update");
					session.setAttribute("saveMsg", Constants.UPDATE_SUCCESS);
					return updateCategoriesForWeb(categories,model,session);
				}
			}
			
			/*if (!flag) 
			{
				
				if (Categories.getCategoriesId() != 0) 
				{
					System.out.println("Moving request for Update");
					model.addAttribute("saveMsg", Constants.UPDATE_SUCCESS);
					return updateCategoriesForWeb(Categories);
				}
			}	*/	
			
			session.setAttribute("saveMsg", Constants.ALREADY_EXIST);
			return new ModelAndView("redirect:/fetchCategoriesList");

		}
		
		@RequestMapping("/updateCategories")
		public ModelAndView updateCategoriesForWeb(@ModelAttribute Categories categories,Model model,HttpSession session) {

			System.out.println("in Update Country");

			
			if (categories.getCategoryId() == 0 || categories.getCategoryName() == null || categories.getCategoryName().equals("")) {
				System.out.println("con not update because 0 city not available");
				return new ModelAndView("redirect:/fetchCategoriesList");
			}
			
			this.categories=categoriesService.fetchCategoriesForWebApp(categories.getCategoryId());
			float igst=this.categories.getIgst();
			categories.setCategoryDate(this.categories.getCategoryDate());
			categoriesService.updateCategoriesForWebApp(categories);

			List<Product> productList=productService.fetchProductByCategoryIdForWebApp(categories.getCategoryId());
			DecimalFormat decimalFormat=new DecimalFormat("#0.00");
			if(productList!=null)
			{
				for(Product product: productList)
				{
					double mrp=product.getRate()+((product.getRate()*igst)/100);
					mrp=Double.parseDouble(decimalFormat.format(mrp));
					//double unipPrice=(mrp)
					double divisor=(categories.getIgst()/100)+1;
					double unitprice=(mrp/divisor);
					product.setRate((float)Double.parseDouble(decimalFormat.format(unitprice)));
					productService.updateProductForWebApp(product);
				}
			}
			
			return new ModelAndView("redirect:/fetchCategoriesList");

		}

}
