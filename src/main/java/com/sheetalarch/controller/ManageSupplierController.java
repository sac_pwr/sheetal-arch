package com.sheetalarch.controller;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.sheetalarch.admin.models.FetchSupplierList;
import com.sheetalarch.admin.models.PaymentDoInfo;
import com.sheetalarch.entity.Brand;
import com.sheetalarch.entity.Categories;
import com.sheetalarch.entity.Contact;
import com.sheetalarch.entity.EmployeeDetails;
import com.sheetalarch.entity.Inventory;
import com.sheetalarch.entity.PaymentPaySupplier;
import com.sheetalarch.entity.Product;
import com.sheetalarch.entity.Supplier;
import com.sheetalarch.entity.SupplierProductList;
import com.sheetalarch.service.BrandService;
import com.sheetalarch.service.CategoriesService;
import com.sheetalarch.service.InventoryService;
import com.sheetalarch.service.ProductService;
import com.sheetalarch.service.SupplierService;
import com.sheetalarch.utils.Constants;



@Controller
public class ManageSupplierController {

	@Autowired
	SupplierService supplierService;
	
	@Autowired
	CategoriesService categoriesService;

	@Autowired
	BrandService brandService;
	
	@Autowired
	InventoryService inventoryService;
	
	@Autowired
	ProductService productService;
	
	@Autowired
	Product product;
	
	@Autowired
	Brand brand;
	
	@Autowired
	Categories categories;
	
	@Autowired
	Contact contact;
	
	@Autowired
	PaymentPaySupplier paymentPaySupplier;
	
	@Autowired
	Inventory inventory;
	
	@Autowired
	Supplier supplier;
	
	@RequestMapping("/addSupplier")
	public ModelAndView manageSupplier(Model model,HttpSession session) {
		System.out.println("in addSupplier controller");
		
		model.addAttribute("pageName", "Add Supplier");
		
		
		
		
		List<Brand> brandList=brandService.fetchBrandListForWebApp();
		List<Categories> categoryList=categoriesService.fetchCategoriesListForWebApp();
		List<Product> productList=productService.fetchProductListForWebApp();
		
		model.addAttribute("brandlist", brandList);
		model.addAttribute("categorylist", categoryList);
		model.addAttribute("productlist", productList);
		
		return new ModelAndView("addSupplier");
	}
	
	@RequestMapping("/checkSupplierNameForSave")
	public @ResponseBody String checkSupplierNameForSave(Model model,HttpServletRequest request,HttpSession session)
	{
		String supplierName=request.getParameter("name");
		String supplierGSTIN=request.getParameter("gstinNo");
		
		List<Supplier> supplierList = supplierService.fetchSupplierForWebAppList();
		
		boolean flag=false;
		if(supplierList!=null){
			for (Supplier supplier : supplierList) {
				if (supplier.getName().toUpperCase().equals(supplierName.toUpperCase())) {
					flag = true;
					break;
				}
			}
		}
		
		if(flag) 
		{	
			return "nameFailed";
		}
		
		flag=false;
		if(supplierList!=null){
			for (Supplier supplier : supplierList) {
				if (supplier.getGstinNo().toUpperCase().equals(supplierGSTIN.toUpperCase())) {
					flag = true;
					break;
				}
			}
		}
		
		if(flag) 
		{	
			return "gstFailed";
		}
		
		return "Success";
	}
	
	
	@RequestMapping("/checkSupplierNameForUpdate")
	public @ResponseBody String checkSupplierNameForUpdate(Model model,HttpServletRequest request,HttpSession session)
	{
		String supplierName=request.getParameter("name");
		String supplierGSTIN=request.getParameter("gstinNo");
		String supplierId=request.getParameter("supplierId");
		
		List<Supplier> supplierList = supplierService.fetchSupplierForWebAppList();
		
		boolean flag=false;
		if(supplierList!=null){
			for (Supplier supplier : supplierList) {
				if (supplier.getName().toUpperCase().equals(supplierName.toUpperCase()) && supplier.getSupplierId().equals(supplierId)) {
					flag = true;
					break;
				}
			}
		}
		
		if(flag) 
		{	
			return "nameFailed";
		}
		
		flag=false;
		if(supplierList!=null){
			for (Supplier supplier : supplierList) {
				if (supplier.getGstinNo().toUpperCase().equals(supplierGSTIN.toUpperCase()) && supplier.getSupplierId().equals(supplierId)) {
					flag = true;
					break;
				}
			}
		}
		
		if(flag) 
		{	
			return "gstFailed";
		}
		
		return "Success";
	}
	
	
	
	
	@RequestMapping("/saveSupplier")
	public ModelAndView saveSupplier(@ModelAttribute Supplier supplier ,Model model,HttpServletRequest request,HttpSession session) {
		System.out.println("in saveSupplier controller");
		
		String productIdList=request.getParameter("productIdList");
		
		contact.setEmailId(request.getParameter("emailId"));
		contact.setMobileNumber(request.getParameter("mobileNumber"));
		supplier.setContact(contact);
		
		supplier.setSupplierAddedDatetime(new Date());
		supplier.setSupplierUpdatedDatetime(new Date());
		supplierService.saveSupplier(supplier);
		
		supplierService.saveSupplierProductList(productIdList, supplier.getSupplierId());
		
		session.setAttribute("saveMsg", Constants.SAVE_SUCCESS);
		return new ModelAndView("redirect:/fetchSupplierList");
	}
	
	@RequestMapping("/fetchProductListByBrandIdAndCategoryId")
	public @ResponseBody List<Product> fetchProductListByBrandIdAndCategoryId(HttpServletRequest request)
	{
		long categoryId=Long.parseLong(request.getParameter("categoryId"));
		long brandId=Long.parseLong(request.getParameter("brandId"));
		
		List<Product> productList=productService.fetchProductListByBrandIdAndCategoryIdForWebApp(categoryId,brandId);
		List<Product> productList2=null;
		if(productList != null)
		{
			productList2=productService.makeProductImageNull(productList);
		}		
		return productList2;
	}
	
	
	@RequestMapping("/fetchSupplierList")
	public ModelAndView fetchSupplierList(Model model,HttpSession session) {
		System.out.println("in fetchSupplierList controller");
		model.addAttribute("pageName", "Supplier List");
		
		List<FetchSupplierList> fetchSupplierList=supplierService.fetchSupplierForWebApp();
		
		model.addAttribute("fetchSupplierList", fetchSupplierList);
		model.addAttribute("saveMsg", session.getAttribute("saveMsg"));
		session.setAttribute("saveMsg", "");
		return new ModelAndView("ManageSupplier");
	}
	
	@RequestMapping("/fetchSupplierListBySupplierId")
	public ModelAndView fetchSupplierListBySupplierId(Model model,HttpServletRequest request,HttpSession session) {
		System.out.println("in fetchSupplierList controller");
		model.addAttribute("pageName", "Supplier List");
		
		
		
		List<FetchSupplierList> fetchSupplierList=new ArrayList<>();
		supplier=supplierService.fetchSupplier(request.getParameter("supplierId"));
		fetchSupplierList.add(new FetchSupplierList(supplier.getSupplierId(), 1, supplier.getName(), supplier.getContact().getEmailId(), supplier.getContact().getMobileNumber(), supplier.getAddress(), supplier.getGstinNo(),inventoryService.fetchBalanceAmtBySupplierId(supplier.getSupplierId()), supplier.getSupplierAddedDatetime(),supplier.getSupplierUpdatedDatetime()));
		
		model.addAttribute("fetchSupplierList", fetchSupplierList);
		
		return new ModelAndView("ManageSupplier");
	}
	
	@RequestMapping("/fetchSupplierProductList")
	public @ResponseBody List<SupplierProductList> fetchSupplierProductList(Model model,HttpServletRequest request) {
		System.out.println("in fetchSupplierProductList controller");
		
		String supplierId=request.getParameter("supplierId");
		List<SupplierProductList> fetchSupplierProductList=supplierService.fetchSupplierProductListBySupplierIdForWebApp(supplierId);
		if(fetchSupplierProductList==null)
		{
			return null;
		}
		List<SupplierProductList> SupplierProductList=supplierService.makeSupplierProductListNullForWebApp(fetchSupplierProductList);
		
		return SupplierProductList;
	}
	
	//fechSupplier
	@RequestMapping("/fechSupplier")
	public ModelAndView fechSupplier(Model model,HttpServletRequest request,HttpSession session) {
		System.out.println("in fetchSupplierList controller");
		
		  model.addAttribute("pageName", "Update Supplier");
		
		String supplierId=request.getParameter("supplierId");
		Supplier supplier=supplierService.fetchSupplier(supplierId);		
		
		model.addAttribute("supplier", supplier);
		List<SupplierProductList> fetchSupplierProductList=supplierService.fetchSupplierProductListBySupplierIdForWebApp(supplierId);
		List<SupplierProductList> supplierProductList=null;
		String productidlist="";
		model.addAttribute("count","");
		
		if(fetchSupplierProductList!=null)
		{
			supplierProductList=supplierService.makeSupplierProductListNullForWebApp(fetchSupplierProductList);
			productidlist=supplierService.getProductIdList(supplierProductList);
			model.addAttribute("count",supplierProductList.size()+1);
		}
		
		model.addAttribute("supplierProductList", supplierProductList);		
		model.addAttribute("productidlist", productidlist);
		
		
		List<Brand> brandList=brandService.fetchBrandListForWebApp();
		List<Categories> categoryList=categoriesService.fetchCategoriesListForWebApp();
		//List<Product> productList=productService.fetchProductListForWebApp();
		
		model.addAttribute("brandlist", brandList);
		model.addAttribute("categorylist", categoryList);
		//model.addAttribute("productlist", productList);
		
		return new ModelAndView("updateSupplier");
	}
	
	@RequestMapping("/updateSupplier")
	public ModelAndView updateSupplier(@ModelAttribute Supplier supplier ,Model model,HttpServletRequest request,HttpSession session) {
		System.out.println("in updateSupplier controller");
		
		String supplierId=request.getParameter("supplierId");
		Supplier supler=supplierService.fetchSupplier(supplierId);
		String productIdList=request.getParameter("productIdList");
		
		contact.setContactId(supler.getContact().getContactId());
		contact.setEmailId(request.getParameter("emailId"));
		contact.setMobileNumber(request.getParameter("mobileNumber"));
		supplier.setContact(contact);
		
		supplier.setSupplierAddedDatetime(supler.getSupplierAddedDatetime());
		supplier.setSupplierUpdatedDatetime(new Date());
		supplierService.updateSupplier(supplier);
		
		supplierService.updateSupplierProductList(productIdList, supplier.getSupplierId());
		
		session.setAttribute("saveMsg", Constants.UPDATE_SUCCESS);
		return new ModelAndView("redirect:/fetchSupplierList");
	}
	
	@RequestMapping("/sendSMSTOSupplier")
	public @ResponseBody String sendSMSTOSupplier(HttpServletRequest request) {
		
		try {
			String supplierIds=request.getParameter("supplierIds");		
			String smsText=request.getParameter("smsText");	
			supplierService.sendSMSTOSupplier(supplierIds, smsText);
			
			return "Success";
		} catch (Exception e) {
			System.out.println(e.toString());
			return "Failed";
		}
	}
	
	@RequestMapping("/paymentSupplier")
	public  ModelAndView paymentEmployee(Model model,HttpServletRequest request,HttpSession session) {
		
		model.addAttribute("pageName", "Make Supplier Payment");
		
		
		
		String inventoryTransactionId=request.getParameter("inventoryTransactionId");
				
		PaymentDoInfo paymentDoInfo= inventoryService.fetchPaymentStatus(inventoryTransactionId);
		
		model.addAttribute("paymentDoInfo", paymentDoInfo);
		
		return new ModelAndView("makePayment");
	}

	@RequestMapping("/giveSupplierOrderPayment")
	public ModelAndView giveEmployeePayment(HttpServletRequest request,HttpSession session,Model model) {
		
		
		try {			
			//String employeeDetailsId=request.getParameter("employeeDetailsId");
			String inventoryId=request.getParameter("inventoryId");
			double amount=Double.parseDouble(request.getParameter("amount"));
			String bankName=request.getParameter("bankName");
			String cheqNo=request.getParameter("cheqNo");
			String cheqDate=request.getParameter("cheqDate");
			//String comment=request.getParameter("comment");
			
			inventory=inventoryService.fetchInventory(inventoryId);			
			SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
			PaymentDoInfo paymentDoInfo= inventoryService.fetchPaymentStatus(inventoryId);
			
			
			if(cheqDate.equals(""))
			{
				paymentPaySupplier.setChequeDate(null);
				bankName=null;
				cheqNo=null;
				paymentPaySupplier.setPayType(Constants.CASH_PAY_STATUS);
			}
			else
			{
				paymentPaySupplier.setChequeDate(dateFormat.parse(cheqDate));
				paymentPaySupplier.setPayType(Constants.CHEQUE_PAY_STATUS);
			}
			
			paymentPaySupplier.setBankName(bankName);
			paymentPaySupplier.setChequeNumber(cheqNo);
			paymentPaySupplier.setDueAmount(paymentDoInfo.getAmountUnPaid()-amount);
			paymentPaySupplier.setDueDate(new Date());
			paymentPaySupplier.setPaidAmount(amount);
			paymentPaySupplier.setPaidDate(new Date());			
			
			if(paymentPaySupplier.getDueAmount()==0)
			{
				inventory.setPayStatus(true);
			}
			else
			{
				inventory.setPayStatus(false);
			}
			
			paymentPaySupplier.setInventory(inventory);
			inventoryService.givePayment(paymentPaySupplier);			
			
		} catch (Exception e) {
			System.out.println(e.toString());
		}
		
		return new ModelAndView("redirect:/fetchSupplierList");
	}
}
