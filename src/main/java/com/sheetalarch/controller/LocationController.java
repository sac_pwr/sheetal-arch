/**
 * 
 */
package com.sheetalarch.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.sheetalarch.entity.Area;
import com.sheetalarch.entity.Cluster;
import com.sheetalarch.entity.Route;
import com.sheetalarch.service.AreaService;
import com.sheetalarch.service.ClusterService;
import com.sheetalarch.service.RouteService;
import com.sheetalarch.utils.Constants;

/**
 * @author aNKIT
 *
 */
@Controller
public class LocationController {

	@Autowired
	ClusterService clusterService;
	
	@Autowired
	AreaService areaService;
	
	@Autowired
	RouteService routeService;
	
	@RequestMapping("/location")
	public ModelAndView locationPage(Model model)
	{
		model.addAttribute("pageName", "Manage Location");
		return new ModelAndView("location");
	}
	
	@RequestMapping("/fetchAreaByClusterId")
	public @ResponseBody List<Area> fetchAreaByClusterId(HttpServletRequest request)
	{
		List<Area> areaList=areaService.fetchAreaListByClusterId(Long.parseLong(request.getParameter("clusterId")));
		
		return areaList;
	}
	
	@RequestMapping("/fetchRouteByAreaId")
	public @ResponseBody List<Route> fetchRouteByAreaId(HttpServletRequest request)
	{
		List<Route> areaList=routeService.fetchRouteByAreaId(Long.parseLong(request.getParameter("areaId")));
		
		return areaList;
	}
	
	@RequestMapping("/fetchClusterList")
	public @ResponseBody List<Cluster> fetchClusterList(HttpServletRequest request)
	{
		List<Cluster> clusterList=clusterService.fetchClusterList();
		
		return clusterList;
	}
	
	@RequestMapping("/fetchAreaList")
	public @ResponseBody List<Area> fetchAreaList(HttpServletRequest request)
	{
		List<Area> areaList=areaService.fetchAreaList();
		
		return areaList;
	}
	
	@RequestMapping("/fetchRouteList")
	public @ResponseBody List<Route> fetchRouteList(HttpServletRequest request)
	{
		List<Route> routeList=routeService.fetchRouteList();
		
		return routeList;
	}
	
	/// fetch by id
	
	@RequestMapping("/fetchClusterByClusterId")
	public @ResponseBody Cluster fetchClusterByClusterId(HttpServletRequest request)
	{
		Cluster cluster=clusterService.fetchClusterByClusterId(Long.parseLong(request.getParameter("clusterId")));		
		return cluster;
	}
	
	@RequestMapping("/fetchAreaByAreaId")
	public @ResponseBody Area fetchAreaByAreaId(HttpServletRequest request)
	{
		Area area=areaService.fetchAreaByAreaId(Long.parseLong(request.getParameter("areaId")));		
		return area;
	}
	
	@RequestMapping("/fetchRouteByRouteId")
	public @ResponseBody Route fetchRouteByRouteId(HttpServletRequest request)
	{
		Route route=routeService.fetchRouteByRouteId(Long.parseLong(request.getParameter("routeId")));
		
		return route;
	}
	
	///
	
	@RequestMapping("/saveCluster")
	public @ResponseBody String saveCountryForWeb(HttpServletRequest request)  {
		System.out.println("in Save Cluster");		
		Cluster cluster=new Cluster();
		cluster.setClusterId(Long.parseLong(request.getParameter("clusterId")));
		cluster.setName(request.getParameter("name"));
		boolean flag = false;
		List<Cluster> clusterList = clusterService.fetchClusterList();
				
		cluster.setName((Character.toString(cluster.getName().charAt(0)).toUpperCase() + cluster.getName().substring(1)));
		
		if (cluster.getClusterId() == 0) {
			
			if(clusterList!=null){
				for (Cluster clusterFromDb : clusterList) {
					if (clusterFromDb.getName().trim().toUpperCase().equals(cluster.getName().trim().toUpperCase())) {
						flag = true;
						break;
					}
				}
			}
			
			if (!flag) 
			{	
				/*country.setDisable(false);*/
				clusterService.saveCluster(cluster);
				return Constants.SUCCESS_RESPONSE;
			}		
		}
		if (cluster.getClusterId() != 0) {
			
			if(clusterList!=null){
				for (Cluster clusterFromDb : clusterList) {
					if (clusterFromDb.getName().trim().toUpperCase().equals(cluster.getName().trim().toUpperCase()) && clusterFromDb.getClusterId()!=cluster.getClusterId()) {
						flag = true;
						break;
					}
				}
			}
			if (!flag) 
			{
				System.out.println("Moving request for Update");
				return updateClusterForWeb(cluster);
			}
		}
		

		return Constants.ALREADY_EXIST;
	}
	
	@RequestMapping("/updateCluster")
	public @ResponseBody String updateClusterForWeb(@ModelAttribute Cluster cluster) {

		System.out.println("in Update Cluster");

		if (cluster.getClusterId() == 0 || cluster.getName() == null || cluster.getName().equals("")) {
			System.out.println("can not update because 0 city not available");
			return Constants.FAILURE_RESPONSE;
		}

		clusterService.updateCluster(cluster);

		return Constants.UPDATE_SUCCESS_RESPONSE;
	}
	
	@RequestMapping("/saveArea")
	public @ResponseBody String saveAreaForWeb(HttpServletRequest request)  {
		System.out.println("in Save Area");		
		Area area=new Area();
		area.setAreaId(Long.parseLong(request.getParameter("areaId")));
		
		Cluster cluster=new Cluster();
		cluster.setClusterId(Long.parseLong(request.getParameter("clusterId")));
		area.setCluster(cluster);
		
		area.setName(request.getParameter("name"));
		boolean flag = false;
		List<Area> areaList = areaService.fetchAreaList();
				
		area.setName((Character.toString(area.getName().charAt(0)).toUpperCase() + area.getName().substring(1)));
		
		if (area.getAreaId() == 0) {
			
			if(areaList!=null){
				for (Area areaFromDb : areaList) {
					if (areaFromDb.getName().trim().toUpperCase().equals(area.getName().trim().toUpperCase())) {
						flag = true;
						break;
					}
				}
			}
			
			if (!flag) 
			{	
				/*country.setDisable(false);*/
				areaService.saveArea(area);
				return Constants.SUCCESS_RESPONSE;
			}		
		}
		if (area.getAreaId() != 0) {
			
			if(areaList!=null){
				for (Area areaFromDb : areaList) {
					if (areaFromDb.getName().trim().toUpperCase().equals(area.getName().trim().toUpperCase()) && area.getAreaId()!=areaFromDb.getAreaId()) {
						flag = true;
						break;
					}
				}
			}
			if (!flag) 
			{
				System.out.println("Moving request for Update");
				return updateAreaForWeb(area);
			}
		}
		

		return Constants.ALREADY_EXIST;
	}
	
	@RequestMapping("/updateArea")
	public @ResponseBody String updateAreaForWeb(@ModelAttribute Area area) {

		System.out.println("in Update Area");

		if (area.getAreaId() == 0 || area.getName() == null || area.getName().equals("")) {
			System.out.println("can not update because 0 city not available");
			return Constants.FAILURE_RESPONSE;
		}

		areaService.updateArea(area);

		return Constants.UPDATE_SUCCESS_RESPONSE;
	}
	
	@RequestMapping("/saveRoute")
	public @ResponseBody String saveRouteForWeb(HttpServletRequest request)  {
		System.out.println("in Save Route");		
		Route route=new Route();
		route.setRouteId(Long.parseLong(request.getParameter("routeId")));
		
		Area area=new Area();
		area.setAreaId(Long.parseLong(request.getParameter("areaId")));
		route.setArea(area);
		
		route.setRouteNo(request.getParameter("routeNo"));
		boolean flag = false;
		List<Route> routeList = routeService.fetchRouteList();
				
		route.setRouteNo((Character.toString(route.getRouteNo().charAt(0)).toUpperCase() + route.getRouteNo().substring(1)));
		
		if (route.getRouteId() == 0) {
			
			if(routeList!=null){
				for (Route routeFromDb : routeList) {
					if (routeFromDb.getRouteNo().trim().toUpperCase().equals(route.getRouteNo().trim().toUpperCase())) {
						flag = true;
						break;
					}
				}
			}
			
			if (!flag) 
			{	
				/*country.setDisable(false);*/
				routeService.saveRoute(route);
				return Constants.SUCCESS_RESPONSE;
			}		
		}
		if (route.getRouteId() != 0) {
			
			if(routeList!=null){
				for (Route routeFromDb : routeList) {
					if (routeFromDb.getRouteNo().trim().toUpperCase().equals(route.getRouteNo().trim().toUpperCase()) && route.getRouteId()!=routeFromDb.getRouteId()) {
						flag = true;
						break;
					}
				}
			}
			if (!flag) 
			{
				System.out.println("Moving request for Update");
				return updateRouteForWeb(route);
			}
		}
		

		return Constants.ALREADY_EXIST;
	}
	
	@RequestMapping("/updateRoute")
	public @ResponseBody String updateRouteForWeb(@ModelAttribute Route route) {

		System.out.println("in Update Route");

		if (route.getRouteId() == 0 || route.getRouteNo() == null || route.getRouteNo().equals("")) {
			System.out.println("can not update because 0 city not available");
			return Constants.FAILURE_RESPONSE;
		}

		routeService.updateRoute(route);

		return Constants.UPDATE_SUCCESS_RESPONSE;
	}
}
