/**
 * 
 */
package com.sheetalarch.controller;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.sheetalarch.admin.models.EmployeeAreaDetails;
import com.sheetalarch.admin.models.EmployeeHolidayModel;
import com.sheetalarch.admin.models.EmployeeIncentiveModel;
import com.sheetalarch.admin.models.EmployeeLocation;
import com.sheetalarch.admin.models.EmployeePaymentModel;
import com.sheetalarch.admin.models.EmployeeSalaryStatus;
import com.sheetalarch.admin.models.EmployeeViewModel;
import com.sheetalarch.admin.models.PaymentDoInfo;
import com.sheetalarch.admin.models.SMListAssignToASMModel;
import com.sheetalarch.admin.models.SMReportModel;
import com.sheetalarch.admin.models.SMReportOrderDetails;
import com.sheetalarch.entity.Area;
import com.sheetalarch.entity.Cluster;
import com.sheetalarch.entity.Contact;
import com.sheetalarch.entity.Department;
import com.sheetalarch.entity.Employee;
import com.sheetalarch.entity.EmployeeBasicSalaryStatus;
import com.sheetalarch.entity.EmployeeDetails;
import com.sheetalarch.entity.EmployeeHolidays;
import com.sheetalarch.entity.EmployeeIncentives;
import com.sheetalarch.entity.EmployeeRouteList;
import com.sheetalarch.entity.EmployeeSalary;
import com.sheetalarch.entity.Route;
import com.sheetalarch.models.BaseDomain;
import com.sheetalarch.service.ClusterService;
import com.sheetalarch.service.DepartmentService;
import com.sheetalarch.service.EmployeeDetailsService;
import com.sheetalarch.service.EmployeeService;
import com.sheetalarch.utils.Constants;



/**
 * @author aNKIT
 *
 */
@Controller
public class ManageEmployeeController {
	
	@Autowired
	Area area;
	
	@Autowired
	Department department;

	@Autowired
	DepartmentService departmentService;
	
	@Autowired
	EmployeeService employeeService;
	
	@Autowired
	EmployeeDetailsService employeeDetailsService;
	
	@Autowired
	EmployeeDetails employeeDetails;
	
	@Autowired
	Employee employee;
	
	@Autowired
	EmployeeRouteList employeeRouteList;
	
	@Autowired
	Contact contact;
	
	@Autowired
	EmployeeHolidays employeeHolidays;
	
	@Autowired
	EmployeeIncentives employeeIncentives;
	
	@Autowired
	EmployeeSalary employeeSalary;
	
	@Autowired
	ClusterService clusterService;
	
	SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
	
	@RequestMapping("/addEmployee")
	public ModelAndView addEmployee(Model model,HttpSession session) {

		/*EmployeeDetails employeeDetails=(EmployeeDetails)session.getAttribute("employeeDetails");
		  if(employeeDetails==null)
		  {
			  return new ModelAndView("login");
		  }
		*/
		  model.addAttribute("pageName", "Add Employee");
		  
		System.out.println("in addEmployee controller");
		
		List<Department> DepartmentList = departmentService.fetchDepartmentList();
		List<Cluster> clusterList=clusterService.fetchClusterList();
		
		model.addAttribute("departmentList", DepartmentList);
		model.addAttribute("clusterList", clusterList);
		
		return new ModelAndView("addEmployee");
	}
	
	@RequestMapping("/checkUserId")
	public @ResponseBody Boolean checkUserId(Model model,HttpServletRequest request) {

		System.out.println("in checkUserId controller");
		
		return employeeService.checkUserIdForWebApp(request.getParameter("userId"));
	}
	
	@RequestMapping("/checkUserIdForUpdate")
	public @ResponseBody Boolean checkUserIdForUpdate(Model model,HttpServletRequest request) {

		System.out.println("in checkUserId controller");
		
		return employeeService.checkUserIdForUpdateForWebApp(request.getParameter("userId"),request.getParameter("employeeId"));
	}
	
	@RequestMapping("/addClusterInCart")
	public @ResponseBody List<Cluster> addClusterInCart(Model model,HttpServletRequest request,HttpSession session) {

		System.out.println("in addClusterInCart controller");
		
		Set<Long> clusterIds=(Set<Long>)session.getAttribute("clusterIds");
		
		if(clusterIds==null)
		{
			clusterIds=new HashSet<>();
		}
		
		clusterIds.add(Long.parseLong(request.getParameter("clusterId")));
		session.setAttribute("clusterIds", clusterIds);
		
		List<Cluster> clusterList=new ArrayList<>();
		
		for(Long clusterId : clusterIds)
		{
			clusterList.add(clusterService.fetchClusterByClusterId(clusterId));
		}
		
		return clusterList;
	}
	
	@RequestMapping("/removeClusterInCart")
	public @ResponseBody List<Cluster> removeClusterInCart(Model model,HttpServletRequest request,HttpSession session) {
		
		long clusterId=Long.parseLong(request.getParameter("clusterId"));
		Set<Long> clusterIds=(Set<Long>)session.getAttribute("clusterIds");
		Iterator<Long> itr=clusterIds.iterator();
		while(itr.hasNext())
		{
			if(clusterId==itr.next())
			{
				itr.remove();
			}
		}
		session.setAttribute("clusterIds", clusterIds);
		List<Cluster> clusterList=new ArrayList<>();
		
		for(Long clusterId1 : clusterIds)
		{
			clusterList.add(clusterService.fetchClusterByClusterId(clusterId1));
		}
		
		return clusterList;
	}
	
	@RequestMapping("/checkAreaAssignedToASMForSave")
	public  @ResponseBody String checkAreaAssignedToASMForSave(Model model,HttpServletRequest request,HttpSession session) {
		return employeeDetailsService.checkAreaAssignedToASM(Long.parseLong(request.getParameter("areaId")));
	}
	
	@RequestMapping("/checkAreaAssignedToASMForUpdate")
	public  @ResponseBody String checkAreaAssignedToASMForUpdate(Model model,HttpServletRequest request,HttpSession session) {
		return employeeDetailsService.checkAreaAssignedToASMUpdate(Long.parseLong(request.getParameter("areaId")),Long.parseLong(request.getParameter("employeeDetailsId")));
	}
	
	@RequestMapping("/checkRouteAssignedToSMForSave")
	public  @ResponseBody String checkRouteAssignedToSMForSave(Model model,HttpServletRequest request,HttpSession session) {
		return employeeDetailsService.checkRouteAssignedToSM(Long.parseLong(request.getParameter("routeId")));
	}
	
	@RequestMapping("/checkRouteAssignedToSMForUpdate")
	public  @ResponseBody String checkRouteAssignedToSMForUpdate(Model model,HttpServletRequest request,HttpSession session) {
		return employeeDetailsService.checkRouteAssignedToSMUpdate(Long.parseLong(request.getParameter("routeId")),Long.parseLong(request.getParameter("employeeDetailsId")));
	}
	
	@RequestMapping("/saveEmployee")
	public  ModelAndView saveEmployee(Model model,HttpServletRequest request,HttpSession session) {

		System.out.println("in saveEmployee controller");
				
		String userId=request.getParameter("userId");
		String password=request.getParameter("password");
		long departmentId=Long.parseLong(request.getParameter("departmentId"));
		String employeeName=request.getParameter("employeeName");
		String address=request.getParameter("address");
		String emailId=request.getParameter("emailId");
		String mobileNumber=request.getParameter("mobileNumber");
		double basicSalary=Double.parseDouble(request.getParameter("basicSalary"));
		
		department=departmentService.fetchDepartment(departmentId);
		Long cluster[] = null;
		long areaId = 0,routeId = 0;
		if(department.getName().equals(Constants.GATE_KEEPER_DEPT_NAME))
		{
			Set<Long> clusterIds=(Set<Long>)session.getAttribute("clusterIds");
			cluster=clusterIds.toArray(new Long[clusterIds.size()]);
			//cluster=request.getParameterValues("clusterIds");
		}
		else if(department.getName().equals(Constants.AREA_SALESMAN_DEPT_NAME))
		{
			areaId=Long.parseLong(request.getParameter("areaId"));
		}
		else
		{
			routeId=Long.parseLong(request.getParameter("routeId"));
		}
		
		employee.setDepartment(department);
		employee.setPassword(password);
		employee.setUserId(userId);
		employeeService.saveForWebApp(employee);
		
		contact.setEmailId(emailId);
		contact.setMobileNumber(mobileNumber);
		
		employeeDetails.setAddress(address);
		employeeDetails.setBasicSalary(basicSalary);
		employeeDetails.setName(employeeName);
		employeeDetails.setEmployee(employee);
		employeeDetails.setContact(contact);
		employeeDetails.setEmployeeDetailsAddedDatetime(new Date());
		employeeDetails.setEmployeeDetailsUpdatedDatetime(new Date());
		employeeDetails.setStatus(false);
		employeeDetails.setEmployeeDetailsDisableDatetime(new Date());
		employeeDetailsService.saveForWebApp(employeeDetails);
		
		SMListAssignToASMModel sMListAssignToASMModel=employeeDetailsService.saveEmployeeAreasForWebApp(cluster,areaId,routeId,department.getName(),employeeDetails.getEmployeeDetailsId());
		
		employeeDetailsService.updateSM_List_To_ASM_ForWebApp(sMListAssignToASMModel);
		
		EmployeeBasicSalaryStatus employeeBasicSalaryNew=new EmployeeBasicSalaryStatus();
		employeeBasicSalaryNew.setEmployeeDetails(employeeDetails);
		employeeBasicSalaryNew.setBasicSalary(basicSalary);
		employeeBasicSalaryNew.setStartDate(new Date());
		employeeBasicSalaryNew.setEndDate(new Date());
		employeeDetailsService.saveEmployeeOldBasicSalary(employeeBasicSalaryNew);
		session.setAttribute("clusterIds", null);
		return new ModelAndView("redirect:/fetchEmployeeList");
	}
	
	@RequestMapping("/fetchEmployee")
	public  ModelAndView fetchEmployee(Model model,HttpServletRequest request,HttpSession session) {
		
		model.addAttribute("pageName", "Update Employee");	
		
		String employeeDetailsId=request.getParameter("employeeDetailsId");
		
		employeeDetails=employeeDetailsService.fetchEmployeeDetailsForWebApp(Long.parseLong(employeeDetailsId));
		model.addAttribute("employeeDetailsForUpdate", employeeDetails);
		
		List<EmployeeRouteList> employeeRouteList=employeeDetailsService.fetchEmployeeRouteListByEmployeeDetailsId(Long.parseLong(employeeDetailsId));
		model.addAttribute("employeeRouteList", employeeRouteList);
		

		if(employeeDetails.getEmployee().getDepartment().getName().equals(Constants.GATE_KEEPER_DEPT_NAME))
		{
			Set<Long> clusterIds=new HashSet<>();
						
			for(EmployeeRouteList employeeRoute: employeeRouteList)
			{
				clusterIds.add(employeeRoute.getCluster().getClusterId());
			}			
			
			session.setAttribute("clusterIds", clusterIds);
			
			List<Cluster> clusterList=new ArrayList<>();
			
			for(Long clusterId : clusterIds)
			{
				clusterList.add(clusterService.fetchClusterByClusterId(clusterId));
			}			
			model.addAttribute("clusterIdForGK", clusterList);
		}
		
		
		List<Department> DepartmentList = departmentService.fetchDepartmentList();
		List<Cluster> clusterList=clusterService.fetchClusterList();
		
		model.addAttribute("departmentList", DepartmentList);
		model.addAttribute("clusterList", clusterList);
		
		return new ModelAndView("updateEmployee");
	}
	
	@RequestMapping("/updateEmployee")
	public  ModelAndView updateEmployee(Model model,HttpServletRequest request,HttpSession session) {
			
		System.out.println("in saveEmployee controller");
		String employeeDetailsId=request.getParameter("employeeDetailsId");
		employeeDetails=employeeDetailsService.fetchEmployeeDetailsForWebApp(Long.parseLong(employeeDetailsId));
		
		String userId=request.getParameter("userId");
		String password=request.getParameter("password");
		long departmentId=Long.parseLong(request.getParameter("departmentId"));
		String employeeName=request.getParameter("employeeName");
		String address=request.getParameter("address");
		String emailId=request.getParameter("emailId");
		String mobileNumber=request.getParameter("mobileNumber");
		double basicSalary=Double.parseDouble(request.getParameter("basicSalary"));
		double basicSalaryOld=employeeDetails.getBasicSalary();
		
		department=departmentService.fetchDepartment(departmentId);
		Long cluster[] = null;
		long areaId = 0,rootId = 0;
		if(department.getName().equals(Constants.GATE_KEEPER_DEPT_NAME))
		{
			Set<Long> clusterIds=(Set<Long>)session.getAttribute("clusterIds");
			cluster=clusterIds.toArray(new Long[clusterIds.size()]);
			//cluster=request.getParameterValues("clusterIds");
		}
		else if(department.getName().equals(Constants.AREA_SALESMAN_DEPT_NAME))
		{
			areaId=Long.parseLong(request.getParameter("areaId"));
		}
		else
		{
			rootId=Long.parseLong(request.getParameter("routeId"));
		}
		
		employee.setDepartment(department);
		
		employee.setEmployeeId(employeeDetails.getEmployee().getEmployeeId());
		employee.setPassword(password);
		employee.setUserId(userId);
		
		employeeService.updateForWebApp(employee);
		
		contact.setContactId(employeeDetails.getContact().getContactId());
		contact.setEmailId(emailId);
		contact.setMobileNumber(mobileNumber);
		
		employeeDetails.setAddress(address);
		
		employeeDetails.setBasicSalary(basicSalary);
		
		employeeDetails.setName(employeeName);
		employeeDetails.setEmployee(employee);
		employeeDetails.setContact(contact);
		employeeDetails.setEmployeeDetailsAddedDatetime(employeeDetails.getEmployeeDetailsAddedDatetime());
		employeeDetails.setEmployeeDetailsUpdatedDatetime(new Date());
		employeeDetails.setStatus(employeeDetails.isStatus());
		employeeDetailsService.updateForWebApp(employeeDetails);
		
		SMListAssignToASMModel sMListAssignToASMModel=employeeDetailsService.updateEmployeeRoutesForWebApp(cluster,areaId,rootId,department.getName(),employeeDetails.getEmployeeDetailsId());
		
		employeeDetailsService.updateSM_List_To_ASM_ForWebApp(sMListAssignToASMModel);
		
		if(basicSalary!=basicSalaryOld)
		{
			EmployeeBasicSalaryStatus employeeBasicSalary=employeeDetailsService.fetchLastEmployeeOldBasicSalaryByEmployeeOldBasicSalaryId(Long.parseLong(employeeDetailsId));
			employeeBasicSalary.setEndDate(new Date());
			employeeDetailsService.updateEmployeeOldBasicSalary(employeeBasicSalary);
			
			Calendar cal=Calendar.getInstance();
			cal.add(Calendar.DAY_OF_MONTH, 1);
			
			EmployeeBasicSalaryStatus employeeBasicSalaryNew=new EmployeeBasicSalaryStatus();
			employeeBasicSalaryNew.setEmployeeDetails(employeeDetails);
			employeeBasicSalaryNew.setBasicSalary(basicSalary);
			employeeBasicSalaryNew.setStartDate(cal.getTime());
			employeeBasicSalaryNew.setEndDate(cal.getTime());
			employeeDetailsService.saveEmployeeOldBasicSalary(employeeBasicSalaryNew);
		}
		session.setAttribute("clusterIds", null);
		return new ModelAndView("redirect:/fetchEmployeeList");
	}
	
	@RequestMapping("/fetchEmployeeList")
	public  ModelAndView fetchEmployeeList(Model model,HttpServletRequest request,HttpSession session) {
		model.addAttribute("pageName", "HRM");
		/*EmployeeDetails employeeDetails=(EmployeeDetails)session.getAttribute("employeeDetails");
		  if(employeeDetails==null)
		  {
			  return new ModelAndView("login");
		  }*/
		
		List<EmployeeViewModel> employeeViewModelList=employeeDetailsService.fetchEmployeeDetailsForView();
		model.addAttribute("employeeViewModelList", employeeViewModelList);
		return new ModelAndView("ManageEmployee");
	}
	
	@RequestMapping("/fetchEmployeeDetail")
	public @ResponseBody  List<EmployeeSalaryStatus> fetchEmployeeDetail(Model model,HttpServletRequest request) { 
		
		String employeeDetailsId=request.getParameter("employeeDetailsId");
		List<EmployeeSalaryStatus> employeeSalaryStatus=employeeDetailsService.fetchEmployeeSalaryStatusForWebApp(Long.parseLong(employeeDetailsId));
				
		return employeeSalaryStatus;
	}
	
	@RequestMapping("/fetchEmployeeDetailsAjax")
	public @ResponseBody  EmployeeDetails fetchEmployeeDetailsAjax(Model model,HttpServletRequest request) {
		
		String employeeDetailsId=request.getParameter("employeeDetailsId");
		EmployeeDetails employeeDetails=employeeDetailsService.fetchEmployeeDetailsForWebApp(Long.parseLong(employeeDetailsId));
				
		return employeeDetails;
	}
	
	
	@RequestMapping("/fetchEmployeeSalaryByFilter")
	public @ResponseBody  List<EmployeeSalaryStatus> tofilterRangeEmployeeSalaryStatusForWebApp( HttpSession session, HttpServletRequest request, Model model)
	{
		
		String filter=request.getParameter("filter");
		String startDate=request.getParameter("startDate");
		String endDate=request.getParameter("endDate");
		String employeeDetailsId=request.getParameter("employeeDetailsId");
	
		List<EmployeeSalaryStatus> employeeSalaryStatuslist = employeeDetailsService.tofilterRangeEmployeeSalaryStatusForWebApp(startDate, endDate,filter, Long.parseLong(employeeDetailsId));
		 
		 return employeeSalaryStatuslist;
	}
	
	@RequestMapping("/fetchEmployeeHolidayDetail")
	public @ResponseBody  EmployeeHolidayModel fetchEmployeeHolidayDetail(Model model,HttpServletRequest request) {
		
		String employeeDetailsId=request.getParameter("employeeDetailsId");
		EmployeeHolidayModel employeeHolidayModel=employeeDetailsService.fetchEmployeeHolidayModelForWebApp(employeeDetailsId,"CurrentMonth",""	,"");
		
		return employeeHolidayModel;
	}
	
	@RequestMapping("/fetchEmployeeHolidayDetailByFilter")
	public @ResponseBody  EmployeeHolidayModel fetchEmployeeHolidayDetailByFilter(Model model,HttpServletRequest request) {
		
		String employeeDetailsId=request.getParameter("employeeDetailsId");
		String filter=request.getParameter("filter");
		String startDate=request.getParameter("startDate");
		String endDate=request.getParameter("endDate");
		EmployeeHolidayModel employeeHolidayModel=employeeDetailsService.fetchEmployeeHolidayModelForWebApp(employeeDetailsId,filter,startDate	,endDate);
		//model.addAttribute("employeeSalaryStatus", employeeSalaryStatus);
		
		return employeeHolidayModel;
	}
	
	@RequestMapping("/fetchEmployeeIncentiveDetailByFilter")
	public @ResponseBody  EmployeeIncentiveModel fetchEmployeeIncentiveDetailByFilter(Model model,HttpServletRequest request) {
		
		String employeeDetailsId=request.getParameter("employeeDetailsId");
		String filter=request.getParameter("filter");
		String startDate=request.getParameter("startDate");
		String endDate=request.getParameter("endDate");
		List<EmployeeIncentives> employeeIncentiveList=employeeDetailsService.fetchEmployeeIncentiveListByFilter(Long.parseLong(employeeDetailsId), filter, startDate, endDate);
		EmployeeDetails employeeDetails=employeeDetailsService.fetchEmployeeDetailsForWebApp(Long.parseLong(employeeDetailsId));
		
		EmployeeIncentiveModel employeeIncentiveModel=new EmployeeIncentiveModel(employeeDetails, employeeIncentiveList); 
		
		return employeeIncentiveModel;
	}
	
	
	@RequestMapping("/fetchEmployeeIncentivesByEmployeeIncentivesId")
	public @ResponseBody  EmployeeIncentives fetchEmployeeIncentivesByEmployeeIncentivesId(Model model,HttpServletRequest request) {
		
		EmployeeIncentives employeeIncentives = employeeDetailsService.fetchEmployeeIncentivesByEmployeeIncentivesId(Long.parseLong(request.getParameter("employeeIncentivesId")));
		
		return employeeIncentives;
	}
	
	@RequestMapping("/editIncentivesAjax")
	public @ResponseBody  String editIncentivesAjax(Model model,HttpServletRequest request) {
		
		String employeeDetailsId=request.getParameter("employeeDetailsId");
		double incentiveAmount=Double.parseDouble(request.getParameter("incentives"));
		long incentiveId=Long.parseLong(request.getParameter("incentiveId"));

		String reason=request.getParameter("reason");
		
		employeeIncentives.setEmployeeIncentiveId(incentiveId);
		
		employeeDetails.setEmployeeDetailsId(Long.parseLong(employeeDetailsId));
		employeeIncentives.setEmployeeDetails(employeeDetails);
		employeeIncentives.setReason(reason);
		employeeIncentives.setIncentiveAmount(incentiveAmount);
		employeeIncentives.setIncentiveGivenDate(new Date());
		
		String response=employeeDetailsService.updateIncentive(employeeIncentives);
		
		return response;
	}
	
	@RequestMapping("/fetchEmployeeRoutes")
	public @ResponseBody  EmployeeAreaDetails fetchEmployeeAreas(Model model,HttpServletRequest request) {
		
		String employeeDetailsId=request.getParameter("employeeDetailsId");
		
		EmployeeAreaDetails employeeAreaDetails = employeeDetailsService.fetchEmployeeAreaDetails(employeeDetailsId);
		
		
		return employeeAreaDetails;
	}
	
	
	@RequestMapping("/bookHoildays")
	public @ResponseBody  String bookHoildays(Model model,HttpServletRequest request) {
		
		String noOfDays=request.getParameter("noOfDays");
		String oneDate=request.getParameter("oneDate");
		String fromDate=request.getParameter("fromDate");
		String toDate=request.getParameter("toDate");
		String paidStatus=request.getParameter("paidStatus");
		String reason=request.getParameter("reason");		
		String employeeDetailsId=request.getParameter("employeeDetailsId");
		
		
		try {
			if(Integer.parseInt(noOfDays)>1)
			{
				employeeHolidays.setFromDate(dateFormat.parse(fromDate));
				employeeHolidays.setToDate(dateFormat.parse(toDate));
			}
			else 
			{
				employeeHolidays.setFromDate(dateFormat.parse(oneDate));
				employeeHolidays.setToDate(dateFormat.parse(oneDate));
			}
			
			if(paidStatus.equals("paid"))
			{
				employeeHolidays.setPaidHoliday(true);
			}
			else
			{
				employeeHolidays.setPaidHoliday(false);
			}
			employeeHolidays.setGivenHolidayDate(new Date());
			employeeHolidays.setReason(reason);
			employeeDetails.setEmployeeDetailsId(Long.parseLong(employeeDetailsId));		
			employeeHolidays.setEmployeeDetails(employeeDetails);
			employeeDetailsService.bookHolidayForWebApp(employeeHolidays);
		} catch (Exception e) {
			return "Fail";
		}
		
				
		return "Success";
	}
	
	@RequestMapping("/fetchEmployeeHolidaysByEmployeeHolidaysId")
	public @ResponseBody  EmployeeHolidays fetchEmployeeHolidaysByEmployeeHolidaysId(Model model,HttpServletRequest request) {
		
		EmployeeHolidays employeeHolidays = employeeDetailsService.fetchEmployeeHolidayByEmployeeHolidayId(Long.parseLong(request.getParameter("employeeHolidaysId")));
		
		return employeeHolidays;
	}
	
	@RequestMapping("/updateHoildays")
	public @ResponseBody  String updateHoildays(Model model,HttpServletRequest request) {
		
		String noOfDays=request.getParameter("noOfDays");
		String oneDate=request.getParameter("oneDate");
		String fromDate=request.getParameter("fromDate");
		String toDate=request.getParameter("toDate");
		String paidStatus=request.getParameter("paidStatus");
		String reason=request.getParameter("reason");		
		String employeeDetailsId=request.getParameter("employeeDetailsId");
		long employeeHolidaysId=Long.parseLong(request.getParameter("employeeHolidaysId"));
		
		try {
			if(Integer.parseInt(noOfDays)>1)
			{
				employeeHolidays.setFromDate(dateFormat.parse(fromDate));
				employeeHolidays.setToDate(dateFormat.parse(toDate));
			}
			else 
			{
				employeeHolidays.setFromDate(dateFormat.parse(oneDate));
				employeeHolidays.setToDate(dateFormat.parse(oneDate));
			}
			
			if(paidStatus.equals("paid"))
			{
				employeeHolidays.setPaidHoliday(true);
			}
			else
			{
				employeeHolidays.setPaidHoliday(false);
			}
			
			employeeHolidays.setEmployeeHolidaysId(employeeHolidaysId);
			employeeHolidays.setGivenHolidayDate(new Date());
			employeeHolidays.setReason(reason);
			employeeDetails.setEmployeeDetailsId(Long.parseLong(employeeDetailsId));		
			employeeHolidays.setEmployeeDetails(employeeDetails);
			employeeDetailsService.updateEmployeeHoliday(employeeHolidays);
		} catch (Exception e) {
			return "Fail";
		}
		
				
		return "Success";
	}
	
	@RequestMapping("/checkHoliday")
	public @ResponseBody BaseDomain checkHoliday(HttpServletRequest request) {
		
		String employeeDetailsId=request.getParameter("employeeDetailsId");
		String startDate=request.getParameter("startDate");
		String endDate=request.getParameter("endDate");
		BaseDomain baseDomain=new BaseDomain(); 
		String msg = employeeDetailsService.checkHolidayGivenOrNot(startDate, endDate, employeeDetailsId);
		baseDomain.setStatus(msg);
		
		return baseDomain;
	}
	
	@RequestMapping("/checkUpdatingHoliday")
	public @ResponseBody BaseDomain checkUpdatingHoliday(HttpServletRequest request) {
		
		String employeeDetailsId=request.getParameter("employeeDetailsId");
		String startDate=request.getParameter("startDate");
		String endDate=request.getParameter("endDate");
		long employeeHolidayId=Long.parseLong(request.getParameter("employeeHolidayId"));
		BaseDomain baseDomain=new BaseDomain(); 
		String msg = employeeDetailsService.checkUpdatingHolidayGivenOrNot(startDate, endDate, employeeDetailsId, employeeHolidayId);
		baseDomain.setStatus(msg);
		
		return baseDomain;
	}
	
	@RequestMapping("/giveIncentives")
	public @ResponseBody String GiveIncentives(HttpServletRequest request) {
		
		try {
			String employeeDetailsId=request.getParameter("employeeDetailsId");
			double incentives=Double.parseDouble(request.getParameter("incentives"));
			String reason=request.getParameter("reason");
			employeeDetails.setEmployeeDetailsId(Long.parseLong(employeeDetailsId));
			
			employeeIncentives.setEmployeeDetails(employeeDetails);
			employeeIncentives.setIncentiveAmount(incentives);
			employeeIncentives.setReason(reason);
			employeeIncentives.setIncentiveGivenDate(new Date());
			employeeDetailsService.giveIncentives(employeeIncentives);
		} catch (NumberFormatException e) {
			return "Failed";
		}
		
		
		return "Success";
	}
	
	@RequestMapping("/givePayment")
	public @ResponseBody String givePayment(HttpServletRequest request) {
		
		try {
			String employeeDetailsId=request.getParameter("employeeDetailsId");
			double amount=Double.parseDouble(request.getParameter("amount"));
			String bankName=request.getParameter("bankName");
			String cheqNo=request.getParameter("cheqNo");
			String cheqDate=request.getParameter("cheqDate");
			String comment=request.getParameter("comment");
			
			employeeDetails.setEmployeeDetailsId(Long.parseLong(employeeDetailsId));
			if(cheqDate.equals(""))
			{
				employeeSalary.setChequeDate(null);
				bankName=null;
				cheqNo=null;
			}
			else
			{
				employeeSalary.setChequeDate(dateFormat.parse(cheqDate));
			}
			
			employeeSalary.setEmployeeDetails(employeeDetails);
			employeeSalary.setPayingAmount(amount);
			employeeSalary.setBankName(bankName);
			employeeSalary.setChequeNumber(cheqNo);
			
			employeeSalary.setComment(comment);
			employeeSalary.setPayingDate(new Date());			
			employeeDetailsService.givePayment(employeeSalary);
			
		} catch (Exception e) {
			
			System.out.println(e.toString());
			return "Failed";
			
		}
		
		
		return "Success";
	}
	
	
	@RequestMapping("/openPaymentModel")
	public @ResponseBody EmployeePaymentModel openPaymentModel(HttpServletRequest request) {
		
		try {
			String employeeDetailsId=request.getParameter("employeeDetailsId");		
			
			EmployeePaymentModel employeePaymentModel=employeeDetailsService.openPaymentModel(employeeDetailsId);
			
			return employeePaymentModel;
		} catch (Exception e) {
			System.out.println(e.toString());
			return null;
		}
		
		
		
	}
	
	@RequestMapping("/sendSMS")
	public @ResponseBody String sendSMS(HttpServletRequest request) {
		
		try {
			String employeeDetailsIdList=request.getParameter("employeeDetailsId");		
			String smsText=request.getParameter("smsText");	
			employeeDetailsService.sendSMSTOEmployee(employeeDetailsIdList, smsText);
			
			return "Success";
		} catch (Exception e) {
			System.out.println(e.toString());
			return "Failed";
		}
		
		
		
	}
	
	@RequestMapping("/getEmployeeView")
	public  ModelAndView fetchEmployeeDetails(Model model,HttpServletRequest request,HttpSession session) {
		model.addAttribute("pageName", "HRM");
		/*EmployeeDetails employeeDetails=(EmployeeDetails)session.getAttribute("employeeDetails");
		  if(employeeDetails==null)
		  {
			  return new ModelAndView("login");
		  }*/
		
		List<EmployeeViewModel> employeeDetailModelList=employeeDetailsService.fetchEmployeeDetail(request.getParameter("employeeDetailsId"));
		
		
		model.addAttribute("employeeViewModelList", employeeDetailModelList);// sending employeeDetailModelList this list to jsp page
		model.addAttribute("count",employeeDetailModelList.size());//optional, sending the count of list to the jsp page
		return new ModelAndView("ManageEmployee");
	}
	
	@RequestMapping("/viewEmployeeSalary")
	public  ModelAndView viewEmployeeSalary(Model model,HttpServletRequest request,HttpSession session) {
		model.addAttribute("pageName", "Employee Salary Details");
		/*EmployeeDetails employeeDetails=(EmployeeDetails)session.getAttribute("employeeDetails");
		  if(employeeDetails==null)
		  {
			  return new ModelAndView("login");
		  }*/
		
		model.addAttribute("employeeDetailsId", request.getParameter("employeeDetailsId"));
		
		return new ModelAndView("EmployeeSalaryDetails");
	}
	
	//paymentEmployee
	@RequestMapping("/paymentEmployee")
	public  ModelAndView paymentEmployee(Model model,HttpServletRequest request,HttpSession session) {
		model.addAttribute("pageName", "Employee Payment");
		/*EmployeeDetails employeeDetails=(EmployeeDetails)session.getAttribute("employeeDetails");
		  if(employeeDetails==null)
		  {
			  return new ModelAndView("login");
		  }*/
		
		String employeeDetailsId=request.getParameter("employeeDetailsId");
		String filter="ViewAll";
		
		List<EmployeeSalaryStatus> employeeSalaryStatuslist = employeeDetailsService.tofilterRangeEmployeeSalaryStatusForWebApp("", "",filter, Long.parseLong(employeeDetailsId));
		
		PaymentDoInfo paymentDoInfo=new PaymentDoInfo();
		paymentDoInfo.setPkId(employeeSalaryStatuslist.get(0).getEmployeeDetailsPkId());
		paymentDoInfo.setId(employeeSalaryStatuslist.get(0).getEmployeeDetailsId());
		paymentDoInfo.setName(employeeSalaryStatuslist.get(0).getName());
		paymentDoInfo.setType("employee");
		paymentDoInfo.setAmountPaid(employeeSalaryStatuslist.get(0).getAmountPaidCurrentMonth());
		paymentDoInfo.setAmountUnPaid(employeeSalaryStatuslist.get(0).getAmountPendingCurrentMonth());
		paymentDoInfo.setTotalAmount(employeeSalaryStatuslist.get(0).getTotalAmount());
		paymentDoInfo.setUrl("giveEmployeePayment");
		model.addAttribute("paymentDoInfo", paymentDoInfo);
		
		return new ModelAndView("makePayment");
	}
	
	@RequestMapping("/giveEmployeePayment")
	public ModelAndView giveEmployeePayment(HttpServletRequest request,HttpSession session,Model model) {
		
		/*EmployeeDetails employeeDetails=(EmployeeDetails)session.getAttribute("employeeDetails");
		  if(employeeDetails==null)
		  {
			  return new ModelAndView("login");
		  }*/
		
		try {
			
			String employeeDetailsId=request.getParameter("employeeDetailsId");
			double amount=Double.parseDouble(request.getParameter("amount"));
			String bankName=request.getParameter("bankName");
			String cheqNo=request.getParameter("cheqNo");
			String cheqDate=request.getParameter("cheqDate");
			String comment=request.getParameter("comment");
			
			employeeDetails.setEmployeeDetailsId(Long.parseLong(employeeDetailsId));
			if(cheqDate.equals(""))
			{
				employeeSalary.setChequeDate(null);
				bankName=null;
				cheqNo=null;
			}
			else
			{
				employeeSalary.setChequeDate(dateFormat.parse(cheqDate));
			}
			
			employeeSalary.setEmployeeDetails(employeeDetails);
			employeeSalary.setPayingAmount(amount);
			employeeSalary.setBankName(bankName);
			employeeSalary.setChequeNumber(cheqNo);
			
			employeeSalary.setComment(comment);
			employeeSalary.setPayingDate(new Date());			
			employeeDetailsService.givePayment(employeeSalary);
			
		} catch (Exception e) {
			System.out.println(e.toString());
		}
		
		return new ModelAndView("redirect:/fetchEmployeeList");
	}
	
	/*@RequestMapping("/fetchSalesManReport")
	public ModelAndView fetchSalesManReport(Model model,HttpSession session) {
		model.addAttribute("pageName", "Sales Man Report");
		EmployeeDetails employeeDetails=(EmployeeDetails)session.getAttribute("employeeDetails");
		  if(employeeDetails==null)
		  {
			  return new ModelAndView("login");
		  }
		
		SalesManReport salesManReport=employeeDetailsService.fetchSalesManReport();
		model.addAttribute("salesManReport", salesManReport);
		
		return new ModelAndView("SalesManReport");
	}*/
	
	@RequestMapping("/disableEmployee")
	public ModelAndView disableEmployee(Model model,HttpSession session,HttpServletRequest request) {
	
		EmployeeDetails employeeDetails=(EmployeeDetails)session.getAttribute("employeeDetails");
		  if(employeeDetails==null)
		  {
			  return new ModelAndView("login");
		  }
		
		  employeeDetails=employeeDetailsService.fetchEmployeeDetailsForWebApp(Long.parseLong(request.getParameter("employeeDetailsId")));
		  if(employeeDetails.isStatus())
		  {
			  employeeDetails.setStatus(false);
		  }
		  else
		  {
			  employeeDetails.setStatus(true);
		  }
		  employeeDetails.setEmployeeDetailsDisableDatetime(new Date());
		  employeeDetailsService.updateForWebApp(employeeDetails);
		  
		return new ModelAndView("redirect:/fetchEmployeeList");
	}
	
	
	@RequestMapping("/findLocation")
	public ModelAndView findLocation(Model model,HttpSession session,HttpServletRequest request) {
		/*EmployeeDetails employeeDetails=(EmployeeDetails)session.getAttribute("employeeDetails");
		  if(employeeDetails==null)
		  {
			  return new ModelAndView("login");
		  }*/
		
		List<Department> DepartmentList = departmentService.fetchDepartmentList();
		model.addAttribute("departmentList", DepartmentList);
		
		return new ModelAndView("currentLocation");
	}
	
	//fetchEmployeeLatLng
	@RequestMapping("/fetchEmployeeLatLng")
	public @ResponseBody List<EmployeeDetails> fetchEmployeeLatLng(HttpServletRequest request) {
		
		List<EmployeeDetails> employeeDetails=employeeDetailsService.fetchEmployeeDetailsByDepartmentId(Long.parseLong(request.getParameter("departmentId")));
		
		return employeeDetails;
	}
	
	@RequestMapping("/fetchEmployeeDetails")
	public @ResponseBody EmployeeLocation fetchEmployeeDetails(HttpServletRequest request) {
		EmployeeLocation employeeLocation=new EmployeeLocation(); 
		EmployeeDetails employeeDetails=employeeDetailsService.fetchEmployeeDetailsForWebApp(Long.parseLong(request.getParameter("employeeDetailsId")));
		List<Route> areaList=employeeDetailsService.fetchRouteByEmployeeId(employeeDetails.getEmployee().getEmployeeId());
		
		employeeLocation.setEmployeeDetails(employeeDetails);
		employeeLocation.setRouteList(areaList);
		return employeeLocation;
	}
	
	@RequestMapping("/SMReportToday")
	public ModelAndView SMReportToday(Model model,HttpSession session,HttpServletRequest request) {
	
		String type=request.getParameter("type");
		if(type.equals("AreaSalesMan"))
		{
			model.addAttribute("pageName", "AreaSalesMan Report");
		}
		else
		{
			model.addAttribute("pageName", "SalesMan Report");
		}
		
		SMReportModel smReportModel=employeeDetailsService.fetchSMReportModel(dateFormat.format(new Date()), "", "pickdate", type);
		model.addAttribute("smReportModel", smReportModel);
		model.addAttribute("type", type);
		
		return new ModelAndView("salesmanReport");
	}
	
	@RequestMapping("/SMReport")
	public ModelAndView SMReport(Model model,HttpSession session,HttpServletRequest request) {
		
		String type=request.getParameter("type");
		if(type.equals("AreaSalesMan"))
		{
			model.addAttribute("pageName", "AreaSalesMan Report");
		}
		else
		{
			model.addAttribute("pageName", "SalesMan Report");
		}
		
		String startDate=request.getParameter("startDate");
		String endDate=request.getParameter("endDate");
		String range=request.getParameter("range");
		
		SMReportModel smReportModel=employeeDetailsService.fetchSMReportModel(startDate, endDate, range, type);
		model.addAttribute("smReportModel", smReportModel);
		model.addAttribute("type", type);
		
		
		return new ModelAndView("salesmanReport");
	}
	
	@RequestMapping("/SMReportOrderDetailsToday")
	public ModelAndView SMReportOrderDetailsToday(Model model,HttpSession session,HttpServletRequest request) {
		long employeeDetailsId=Long.parseLong(request.getParameter("employeeDetailsId"));
		
		SMReportOrderDetails sMReportOrderDetails=employeeDetailsService.fetchSMReportOrderDetails(dateFormat.format(new Date()), "", "pickdate", employeeDetailsId);
		if(sMReportOrderDetails.getType().equals("AreaSalesMan"))
		{
			model.addAttribute("pageName", "AreaSalesMan Report");
		}
		else
		{
			model.addAttribute("pageName", "SalesMan Report");
		}
		model.addAttribute("sMReportOrderDetails", sMReportOrderDetails);		
		
		return new ModelAndView("salesmanDetails");
	}
	
	@RequestMapping("/SMReportOrderDetails")
	public ModelAndView SMReportOrderDetails(Model model,HttpSession session,HttpServletRequest request) {
	
		String startDate=request.getParameter("startDate");
		String endDate=request.getParameter("endDate");
		String range=request.getParameter("range");
		long employeeDetailsId=Long.parseLong(request.getParameter("employeeDetailsId"));
		
		SMReportOrderDetails sMReportOrderDetails=employeeDetailsService.fetchSMReportOrderDetails(startDate, endDate, range, employeeDetailsId);
		if(sMReportOrderDetails.getType().equals("AreaSalesMan"))
		{
			model.addAttribute("pageName", "AreaSalesMan Report");
		}
		else
		{
			model.addAttribute("pageName", "SalesMan Report");
		}
		model.addAttribute("sMReportOrderDetails", sMReportOrderDetails);		
		
		return new ModelAndView("salesmanDetails");
	}
	
}
