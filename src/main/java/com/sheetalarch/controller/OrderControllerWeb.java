/**
 * 
 */
package com.sheetalarch.controller;

import java.text.DecimalFormat;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.sheetalarch.admin.models.OrderReportModel;
import com.sheetalarch.admin.models.SalesManReportModel;
import com.sheetalarch.entity.Cluster;
import com.sheetalarch.service.ClusterService;
import com.sheetalarch.service.OrderDetailsService;

/**
 * @author aNKIT
 *
 */
@Controller
public class OrderControllerWeb {

	@Autowired
	OrderDetailsService orderDetailsService;
	
	@Autowired
	ClusterService clusterService;
	
	@RequestMapping("/salesReport")
	public ModelAndView salesReport(Model model,HttpServletRequest request,HttpSession session)
	{
		model.addAttribute("pageName", "Sales Report");
		
		String range=request.getParameter("range");
		String startDate=request.getParameter("startDate");
		String endDate=request.getParameter("endDate");		
		List<SalesManReportModel> salesManReportModelList=orderDetailsService.fetchSalesManReportModel(startDate, endDate, range);
		model.addAttribute("salesManReportModelList", salesManReportModelList);
		
		DecimalFormat decimalFormat=new DecimalFormat("#0.00");
		double totalAmount=0;
		long noOfOrders=0;
		if(salesManReportModelList!=null)
		{
			for(SalesManReportModel salesManReportModel:salesManReportModelList)
			{
				totalAmount+=salesManReportModel.getOrderAmount();
				noOfOrders+=1;
			}
		}
		model.addAttribute("noOfOrders", noOfOrders);
		model.addAttribute("totalAmount", Double.parseDouble(decimalFormat.format(totalAmount)));
		return new ModelAndView("SalesReport");
	}
	
	@RequestMapping("/orderReport")
	public ModelAndView orderReport(Model model,HttpServletRequest request,HttpSession session)
	{
		model.addAttribute("pageName", "Order Report");
		String currentPending=request.getParameter("currentPending");	
		model.addAttribute("currentPending", currentPending);
		List<OrderReportModel> orderReportModelList=orderDetailsService.fetchOrderReport(currentPending);
		model.addAttribute("orderReportModelList", orderReportModelList);
		
		DecimalFormat decimalFormat=new DecimalFormat("#0.00");
		double totalAmount=0;
		long noOfOrders=0;
		if(orderReportModelList!=null)
		{
			for(OrderReportModel orderReportModel:orderReportModelList)
			{
				totalAmount+=orderReportModel.getAmount();
				noOfOrders+=1;
			}
		}
		model.addAttribute("noOfOrders", noOfOrders);
		model.addAttribute("totalAmount", Double.parseDouble(decimalFormat.format(totalAmount)));
		
		List<Cluster> clusterList=clusterService.fetchClusterList();
		model.addAttribute("clusterList", clusterList);
		
		return new ModelAndView("orderReport");
	}
	
}
