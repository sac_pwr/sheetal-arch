/**
 * 
 */
package com.sheetalarch.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.ModelAndView;

/**
 * @author aNKIT
 *
 */
@ControllerAdvice
public class ExceptionHandlerController {

	
	@ExceptionHandler(value=Exception.class)
	public ModelAndView exceptionHandler(Model model,Exception e,HttpServletRequest request) {		
	
		model.addAttribute("url", request.getRequestURL());
		model.addAttribute("error", e.toString());
		return new ModelAndView("errorPage"); 
	}
	
}
