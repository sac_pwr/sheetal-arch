/**
 * 
 */
package com.sheetalarch.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.sheetalarch.entity.AdminLogin;
import com.sheetalarch.entity.Employee;
import com.sheetalarch.entity.EmployeeDetails;
import com.sheetalarch.models.BaseDomain;
import com.sheetalarch.service.AdminLoginService;
import com.sheetalarch.utils.Constants;

/**
 * @author aNKIT
 *
 */
@Controller
public class MainController {

	
	@Autowired
	AdminLoginService adminLoginService;
	
	@RequestMapping("/")
	public ModelAndView index(Model model,HttpServletRequest request,HttpSession session)
	{		  
		//return new ModelAndView("NewFile");
						
		
		  AdminLogin adminLogin=(AdminLogin)session.getAttribute("adminLogin");
		  if(adminLogin==null)
		  {
			  return new ModelAndView("login");
		  }
		  else
		  {
			  //model.addAttribute("pageName", "Index");
			  return new ModelAndView("redirect:/fetchBoothList");	
		  }
		  
			
	}
	
	
	
	
	
	@RequestMapping("/loginAdmin")
	public ModelAndView loginEmployee(Model model,HttpServletRequest request,HttpSession session) 
	{
		System.out.println("in loginEmployee controller");
		
		AdminLogin adminLogin=adminLoginService.loginValidate(request.getParameter("userId"), request.getParameter("password"));
		if(adminLogin!=null)
		{
			session.setAttribute("adminLogin", adminLogin);
			return new ModelAndView("redirect:/");
		}
		else
		{
			session.invalidate();
			model.addAttribute("validateMsg", "Login failed");
			return new ModelAndView("login");
		}
		
	}
	
	@RequestMapping("/logoutAdmin")
	public ModelAndView logoutEmployee(HttpServletRequest request,HttpSession session) 
	{
		session.invalidate();
		
		return new ModelAndView("redirect:/");
	}
	
}
