package com.sheetalarch.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.sheetalarch.entity.Department;
import com.sheetalarch.entity.EmployeeDetails;
import com.sheetalarch.service.DepartmentService;
import com.sheetalarch.utils.Constants;



@Controller
public class DepartmentControllerWeb {

	@Autowired
	Department department;

	@Autowired
	DepartmentService departmentService;

	@RequestMapping("/fetchDepartmentList")
	public ModelAndView fetchfDepartmentList(Model model,HttpSession session) {
		System.out.println("in fetchDepartmentList controller");
		model.addAttribute("pageName", "Manage Department");
		EmployeeDetails employeeDetails=(EmployeeDetails)session.getAttribute("employeeDetails");
		  if(employeeDetails==null)
		  {
			  return new ModelAndView("login");
		  }
		
		List<Department> DepartmentList = departmentService.fetchDepartmentList();
		model.addAttribute("departmentList", DepartmentList);

		return new ModelAndView("addDepartment");
	}

	@RequestMapping("/fetchDepartment")
	public @ResponseBody Department fetchDepartment(Model model, HttpServletRequest request) {
		System.out.println("in fetchBrand controller");
		long departmentId = Long.parseLong(request.getParameter("departmentId"));
		department = departmentService.fetchDepartment(departmentId);

		return department;
	}

	// saveCountry
	@RequestMapping("/saveDepartment")
	public ModelAndView saveBrandForWeb(HttpServletRequest request, Model model,HttpSession session) {
		System.out.println("in saveDepartment");

		EmployeeDetails employeeDetails=(EmployeeDetails)session.getAttribute("employeeDetails");
		  if(employeeDetails==null)
		  {
			  return new ModelAndView("login");
		  }
		
		this.department.setDepartmentId(Long.parseLong(request.getParameter("departmentId")));
		this.department.setName(request.getParameter("departmentName"));
		this.department.setShortName(request.getParameter("departmentshortName"));

		
		model.addAttribute("saveMsg", "");

		department.setName((Character.toString(department.getName().charAt(0)).toUpperCase()
				+ department.getName().substring(1)));

		

		boolean flag = false;
		List<Department> DepartmentList = departmentService.fetchDepartmentList();

		if (department.getDepartmentId() == 0) {
			
			if (DepartmentList != null) {
				for (Department DepartmentFromDb : DepartmentList) {
					if (DepartmentFromDb.getName().trim().toUpperCase().equals(department.getName().trim().toUpperCase()) || 
							DepartmentFromDb.getShortName().trim().toUpperCase().equals(department.getShortName().trim().toUpperCase())) {
						flag = true;
						break;
					}
				}
			}
			if (!flag) {
				departmentService.saveDepartment(department);
				model.addAttribute("saveMsg", Constants.SAVE_SUCCESS);
				return new ModelAndView("redirect:/fetchDepartmentList");
			}
		}
		if (department.getDepartmentId() != 0) {
			
			flag = false;
			List<Department> DepartmentList2 = departmentService.fetchDepartmentList();
			if (DepartmentList != null) {
				for (Department DepartmentFromDb : DepartmentList2) {
					if ( (DepartmentFromDb.getName().trim().toUpperCase().equals(department.getName().trim().toUpperCase()) ||
						DepartmentFromDb.getShortName().trim().toUpperCase().equals(department.getShortName().trim().toUpperCase()))
						&& 	DepartmentFromDb.getDepartmentId()!=department.getDepartmentId()) {
						flag = true;
						break;
					}
				}
			}
			
			if (!flag) {
				System.out.println("Moving request for Update");
				model.addAttribute("saveMsg", Constants.UPDATE_SUCCESS);
				return updateDepartmentForWeb(department, model,session);
			}
		}

		model.addAttribute("saveMsg", Constants.ALREADY_EXIST);
		return new ModelAndView("redirect:/fetchDepartmentList");

	}

	@RequestMapping("/updateDepartment")
	public ModelAndView updateDepartmentForWeb(@ModelAttribute Department department, Model model,HttpSession session) {

		System.out.println("in updateDepartment");

		if (department.getDepartmentId() == 0 || department.getName() == null
				|| department.getName().equals("")) {
			System.out.println("con not update because 0 city not available");
			return new ModelAndView("redirect:/fetchDepartmentList");
		}

		this.department = departmentService.fetchDepartment(department.getDepartmentId());
		departmentService.updateDepartment(department);

		return new ModelAndView("redirect:/fetchDepartmentList");

	}
}
