/**
 * 
 */
package com.sheetalarch.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.sheetalarch.admin.models.CustomerRerportModel;
import com.sheetalarch.admin.models.ManageBoothListModel;
import com.sheetalarch.admin.models.SingleCustomerReport;
import com.sheetalarch.entity.Booth;
import com.sheetalarch.entity.Cluster;
import com.sheetalarch.entity.Contact;
import com.sheetalarch.entity.Route;
import com.sheetalarch.service.BoothService;
import com.sheetalarch.service.ClusterService;
import com.sheetalarch.service.RouteService;

/**
 * @author aNKIT
 *
 */
@Controller
public class BoothControllerWeb {

	@Autowired
	BoothService boothService;
	
	@Autowired
	RouteService routeService;
	
	@Autowired
	ClusterService clusterService; 
	
	@RequestMapping("/addBoothPage")
	public ModelAndView addBoothpage(HttpServletRequest request,HttpSession session,Model model)
	{		
		model.addAttribute("pageName", "Add Business");
		List<Route> routeList=routeService.fetchRouteList();
		model.addAttribute("routeList", routeList);
		
		return new ModelAndView("addBusiness");
	}
	
	@RequestMapping("/checkBoothNoAvailabilityForSave")
	public @ResponseBody String checkBoothNoAvailabilityForSave(HttpServletRequest request,HttpSession session,Model model)
	{		
		String boothNo=request.getParameter("boothNo");
		List<Booth> boothList=boothService.fetchBoothList();
		boolean flag=false;
		
		if(boothList!=null){
			for (Booth booth : boothList) {
				if (booth.getBoothNo().trim().toUpperCase().equals(boothNo.trim().toUpperCase())) {
					flag = true;
					break;
				}
			}
		}
		
		if (!flag) 
		{
			return "Success";
		}
		return "Failed";
	}
	
	@RequestMapping("/checkBoothNoAvailabilityForUpdate")
	public @ResponseBody String checkBoothNoAvailabilityForUpdate(HttpServletRequest request,HttpSession session,Model model)
	{		
		String boothNo=request.getParameter("boothNo");
		long boothId=Long.parseLong(request.getParameter("boothId"));
		List<Booth> boothList=boothService.fetchBoothList();
		boolean flag=false;
		
		if(boothList!=null){
			for (Booth booth : boothList) {
				if (booth.getBoothNo().trim().toUpperCase().equals(boothNo.trim().toUpperCase()) && boothId!=booth.getBoothId()) {
					flag = true;
					break;
				}
			}
		}
		
		if (!flag) 
		{
			return "Success";
		}
		return "Failed";
	}
	
	@RequestMapping("/checkGSTInAvailabilityForSave")
	public @ResponseBody String checkGSTInAvailabilityForSave(HttpServletRequest request,HttpSession session,Model model)
	{		
		String gstNo=request.getParameter("gstNo");
		List<Booth> boothList=boothService.fetchBoothList();
		boolean flag=false;
		
		if(boothList!=null){
			for (Booth booth : boothList) {
				if (booth.getGstNo().trim().toUpperCase().equals(gstNo.trim().toUpperCase())) {
					flag = true;
					break;
				}
			}
		}
		
		if (!flag) 
		{
			return "Success";
		}
		return "Failed";
	}
	
	@RequestMapping("/checkGSTInAvailabilityForUpdate")
	public @ResponseBody String checkGSTInAvailabilityForUpdate(HttpServletRequest request,HttpSession session,Model model)
	{		
		String gstNo=request.getParameter("gstNo");
		long boothId=Long.parseLong(request.getParameter("boothId"));
		List<Booth> boothList=boothService.fetchBoothList();
		boolean flag=false;
		
		if(boothList!=null){
			for (Booth booth : boothList) {
				if (booth.getGstNo().trim().toUpperCase().equals(gstNo.trim().toUpperCase()) && boothId!=booth.getBoothId()) {
					flag = true;
					break;
				}
			}
		}
		
		if (!flag) 
		{
			return "Success";
		}
		return "Failed";
	}
	
	//@RequestMapping("/saveBooth")
	@RequestMapping(value = "/saveBooth", method = RequestMethod.POST,consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE)
	public ModelAndView saveBooth(@ModelAttribute Booth booth,HttpServletRequest request,HttpSession session,Model model)
	{		
		Contact contact=new Contact();
		contact.setMobileNumber(request.getParameter("mobileNumber"));
		contact.setEmailId(request.getParameter("emailId"));
		contact.setTelephoneNumber(request.getParameter("telephoneNumber"));
		
		Route route=new Route();
		route.setRouteId(Long.parseLong(request.getParameter("routeId")));
		
		booth.setRoute(route);
		booth.setContact(contact);
		
		booth.setStatus(false);
		boothService.saveBooth(booth);
		
		return new ModelAndView("redirect:/fetchBoothList");
	}
	
	@RequestMapping("/fetchBooth")
	public ModelAndView fetchBooth(HttpServletRequest request,HttpSession session,Model model)
	{		
		model.addAttribute("pageName", "Update Business");
		Booth booth=boothService.fetchBoothByBoothId(Long.parseLong(request.getParameter("boothId")));
		model.addAttribute("booth", booth);
		
		List<Route> routeList=routeService.fetchRouteList();
		model.addAttribute("routeList", routeList);
		
		return new ModelAndView("updateBusiness");
	}
	
	@RequestMapping("/updateBooth")
	public ModelAndView updateBooth(@ModelAttribute Booth booth,HttpServletRequest request,HttpSession session,Model model)
	{		
		Booth boothOld=boothService.fetchBoothByBoothId(booth.getBoothId());
		Contact contact=new Contact();
		contact.setContactId(boothOld.getContact().getContactId());
		contact.setMobileNumber(request.getParameter("mobileNumber"));
		contact.setEmailId(request.getParameter("emailId"));
		contact.setTelephoneNumber(request.getParameter("telephoneNumber"));
		
		Route route=new Route();
		route.setRouteId(Long.parseLong(request.getParameter("routeId")));
		
		booth.setRoute(route);
		booth.setContact(contact);
		
		booth.setStatus(boothOld.isStatus());
		booth.setBoothAddedDatetime(boothOld.getBoothAddedDatetime());
		
		boothService.updateBooth(booth);
		
		return new ModelAndView("redirect:/fetchBoothList");
	}
	
	@RequestMapping("/fetchBoothList")
	public ModelAndView fetchBoothList(HttpServletRequest request,HttpSession session,Model model)
	{		
		model.addAttribute("pageName", "Manage Booth");
		List<ManageBoothListModel> manageBoothListModels=boothService.fetchManageBoothList();
		model.addAttribute("boothList", manageBoothListModels);
		
		return new ModelAndView("ManageBusiness");
	}
	
	@RequestMapping("/disableBooth")
	public ModelAndView disableBooth(Model model,HttpSession session,HttpServletRequest request) {
	
		
		  Booth booth=boothService.fetchBoothByBoothId(Long.parseLong(request.getParameter("boothId")));
		  if(booth.isStatus())
		  {
			  booth.setStatus(false);
		  }
		  else
		  {
			  booth.setStatus(true);
		  }
		  boothService.updateBooth(booth);
		  
		return new ModelAndView("redirect:/fetchBoothList");
	}
	
	@RequestMapping("/sendSMSTOShops")
	public @ResponseBody String sendSMSTOShops(HttpServletRequest request) {
		
		try {
			String shopsIds=request.getParameter("shopsIds");		
			String smsText=request.getParameter("smsText");	
			boothService.sendSMSTOShops(shopsIds, smsText);
			
			return "Success";
		} catch (Exception e) {
			System.out.println(e.toString());
			return "Failed";
		}
	}
	
	@RequestMapping("/customerReportList")
	public ModelAndView customerReport(Model model,HttpSession session,HttpServletRequest request) {
		model.addAttribute("pageName", "Booth Report");
		String startDate=request.getParameter("startDate");
		String endDate=request.getParameter("endDate");
		String range=request.getParameter("range");
		
		List<CustomerRerportModel> customerRerportModelList=boothService.fetchCustomerReport(startDate, endDate, range);
		model.addAttribute("customerRerportModelList", customerRerportModelList);
		
		double totalBoothAmt=0;
		for(CustomerRerportModel customerRerportModel: customerRerportModelList)
		{
			totalBoothAmt+=customerRerportModel.getTotalAmount();
		}
		model.addAttribute("totalBoothAmt", totalBoothAmt);
		
		List<Cluster> clusterList=clusterService.fetchClusterList();
		model.addAttribute("clusterList", clusterList);
		
		return new ModelAndView("customerReport");
	}
	
	@RequestMapping("/customerReportByBoothId")
	public ModelAndView customerReportByBoothId(Model model,HttpSession session,HttpServletRequest request) {
	
		model.addAttribute("pageName", "Customer Report");
		
		long boothId=Long.parseLong(request.getParameter("boothId"));
		String startDate=request.getParameter("startDate");
		String endDate=request.getParameter("endDate");
		String range=request.getParameter("range");
		
		SingleCustomerReport singleCustomerReport=boothService.fetchSingleCustomerReport(startDate, endDate, range, boothId);
		model.addAttribute("singleCustomerOrderReport", singleCustomerReport);
		return new ModelAndView("customerDetails");
	}
	
}
