package com.sheetalarch.controller;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.sheetalarch.admin.models.CategoryAndBrandList;
import com.sheetalarch.admin.models.InventoryProduct;
import com.sheetalarch.admin.models.InventoryReportView;
import com.sheetalarch.entity.Brand;
import com.sheetalarch.entity.Categories;
import com.sheetalarch.entity.EmployeeDetails;
import com.sheetalarch.entity.Inventory;
import com.sheetalarch.entity.InventoryDetails;
import com.sheetalarch.entity.Product;
import com.sheetalarch.entity.Supplier;
import com.sheetalarch.entity.SupplierProductList;
import com.sheetalarch.service.BrandService;
import com.sheetalarch.service.CategoriesService;
import com.sheetalarch.service.InventoryService;
import com.sheetalarch.service.ProductService;
import com.sheetalarch.service.SupplierService;


@Controller
public class ManageInventory {

	@Autowired
	InventoryService inventoryService;
	
	@Autowired
	ProductService productService;
	
	@Autowired
	SupplierService supplierService;
	
	@Autowired
	InventoryDetails inventoryDetails;
	
	@Autowired
	Inventory inventory;
	
	@Autowired
	BrandService brandService; 
	
	@Autowired
	CategoriesService categoriesService;
	
	@Autowired
	Supplier supplier;
	
	SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
	
	@RequestMapping("/fetchProductListForInventory")
	public ModelAndView fetchProductListForInventory(Model model,HttpSession session) {
		
		model.addAttribute("pageName", "Manage inventory");
		
		
		
		List<InventoryProduct> inventoryProductsList=inventoryService.inventoryProductList();
		model.addAttribute("inventoryProductsList", inventoryProductsList);
		
		return new ModelAndView("ManageInventory");
	}
	
	@RequestMapping("/fetchProductListForUnderThresholdValue")
	public ModelAndView fetchProductListForUnderThresholdValue(Model model,HttpSession session) {
		
		model.addAttribute("pageName", "Manage Inventory");
		
		
		
		List<InventoryProduct> inventoryProductsList=inventoryService.inventoryProductList();
		List<InventoryProduct> inventoryProductsListNew=new ArrayList<>();
		long srno=1;		
		for(InventoryProduct inventoryProduct :inventoryProductsList)
		{
			Product product=inventoryProduct.getProduct();
			if(product.getCurrentQuantity()<=product.getThreshold())
			{
				inventoryProduct.setSrno(srno++);
				inventoryProductsListNew.add(inventoryProduct);
			}
		}
		
		model.addAttribute("inventoryProductsList", inventoryProductsListNew);
		
		return new ModelAndView("ManageInventory");
	}
	
	@RequestMapping("/fetchProductListForInventoryAjax")
	public @ResponseBody List<InventoryProduct> fetchProductListForInventoryAjax() {
		
		List<InventoryProduct> inventoryProductsList=inventoryService.inventoryProductList();
		inventoryProductsList=inventoryService.makeInventoryProductViewProductNull(inventoryProductsList);
		
		return inventoryProductsList;
	}
	
	
	@RequestMapping("/fetchSupplierListForAddQuantity")
	public @ResponseBody List<SupplierProductList> fetchSupplierListForAddQuantity(HttpServletRequest request) {
		
		List<SupplierProductList> supplierProductLists = inventoryService.fetchSupplierListByProductId(Long.parseLong(request.getParameter("productId")));
		
		if(supplierProductLists != null)
		{
			supplierProductLists=productService.makeSupplierProductImageNull(supplierProductLists);
		}	
		
		return supplierProductLists;
	}
	
	@RequestMapping("/fetchSupplierById")
	public @ResponseBody double fetchSupplierById(HttpServletRequest request) {
		
		double rate=supplierService.fetchSupplierProductRate(Long.parseLong(request.getParameter("productId")),request.getParameter("supplierId"));
		
		return rate; 
	}
	
	@RequestMapping("/saveInventory")
	public ModelAndView saveInventory(Model model,HttpServletRequest request,HttpSession session) throws ParseException {
		
			System.out.println("inside add inventory");
			
			String productIdList=request.getParameter("productIdList");
			String supplierId=request.getParameter("supplierId");
			String paymentDate=request.getParameter("paymentDate");
			inventory.setInventoryAddedDatetime(new Date());
			
			supplier.setSupplierId(supplierId);		
			inventory.setSupplier(supplier);
			inventory.setInventoryPaymentDatetime(dateFormat.parse(paymentDate));
			inventoryService.addInventory(inventory,productIdList);
		
		
		return new ModelAndView("redirect:/fetchProductListForInventory"); 
	}
	
	@RequestMapping("/saveOneInventory")
	public @ResponseBody String saveOneInventory(Model model,HttpServletRequest request) throws ParseException {
		
		System.out.println("inside add inventory");
		
			String productIdList=request.getParameter("productIdList");
			String supplierId=request.getParameter("supplierId");
			String paymentDate=request.getParameter("paymentDate");
			inventory.setInventoryAddedDatetime(new Date());
			
			supplier.setSupplierId(supplierId);		
			inventory.setSupplier(supplier);
			inventory.setInventoryPaymentDatetime(dateFormat.parse(paymentDate));
			
			inventoryService.addInventory(inventory,productIdList);
		
		
		return "Success"; 
	}
	
	@RequestMapping("/openAddMultipleInventory")
	public ModelAndView openAddMultipleInventory(HttpServletRequest request,Model model,HttpSession session) {
		
		model.addAttribute("pageName", "Add Multiple Inventory");
		
		
		List<Supplier> supplierList=supplierService.fetchSupplierForWebAppList();
		model.addAttribute("supplierList", supplierList);
		
		List<Brand> brandList=brandService.fetchBrandListForWebApp();
		model.addAttribute("brandlist", brandList);
		
		List<Categories> categoriesList=categoriesService.fetchCategoriesListForWebApp();
		model.addAttribute("categorieslist", categoriesList);
		
		
		return new ModelAndView("addMultipleInventory"); 
	}
	
	@RequestMapping("/fetchProductBySupplierId")
	public @ResponseBody List<Product> fetchProductBySupplierId(HttpServletRequest request) {
		
		List<Product> list=productService.fetchProductListBySupplierId(request.getParameter("supplierId"));
		
		return list; 
	}
	
	@RequestMapping("/fetchProductBySupplierIdAndCategoryIdAndBrandId")
	public @ResponseBody List<Product> fetchProductBySupplierIdAndCategoryIdAndBrandId(HttpServletRequest request) {
		
		List<Product> list=productService.fetchProductListByBrandIdAndCategoryIdForWebApp(request.getParameter("supplierId"), Long.parseLong(request.getParameter("categoryId")), Long.parseLong(request.getParameter("brandId")));
		
		return list; 
	}
	
	@RequestMapping("/fetchBrandAndCategoryListWeb")
	public @ResponseBody CategoryAndBrandList fetchBrandAndCategoryList(HttpSession session) {
		
		List<Brand> brandList=brandService.fetchBrandListForWebApp();
		List<Categories> categoriesList=categoriesService.fetchCategoriesListForWebApp();
		List<Supplier> fetchSupplierList=supplierService.fetchSupplierForWebAppList();
		//session.setAttribute("supplierOrderCartMap", null);
		return new CategoryAndBrandList(categoriesList, brandList,fetchSupplierList); 
	}
	
	@RequestMapping("/fetchSupplierBySupplierId")
	public @ResponseBody Supplier fetchSupplierBySupplierId(HttpServletRequest request) {
		
		supplier=supplierService.fetchSupplier(request.getParameter("supplierId"));
		
		return supplier; 
	}
	
	@RequestMapping("/fetchInventoryReportView")
	public ModelAndView fetchInventoryReportView(HttpServletRequest request,Model model,HttpSession session) {
		
		model.addAttribute("pageName", "Inventory Report");
		
		String range=request.getParameter("range");
		String startDate=request.getParameter("startDate");
		String endDate=request.getParameter("endDate");
		String supplierId=request.getParameter("supplierId");
		if(supplierId==null)
		{
			supplierId="";
		}
		
		List<InventoryReportView> inventoryReportViews=inventoryService.fetchInventoryReportView(supplierId, startDate, endDate, range);
		model.addAttribute("inventoryReportViews", inventoryReportViews);
		
		model.addAttribute("supplierId", supplierId);
		
		return new ModelAndView("InventoryReport"); 
	}
	
	@RequestMapping("/fetchTrasactionDetailsByInventoryId")
	public @ResponseBody List<InventoryDetails> fetchTrasactionDetailsByInventoryId(HttpServletRequest request) {
		
		List<InventoryDetails> inventoryDetailList=inventoryService.fetchTrasactionDetailsByInventoryId(request.getParameter("inventoryId"));
		inventoryDetailList=inventoryService.makeProductImageNullOfInventoryDetailsList(inventoryDetailList);
		return inventoryDetailList; 
	}
}
