package com.sheetalarch.utils;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.sheetalarch.entity.Supplier;

@Component
public class SupplierIdGenerator{

	
	@Autowired
	SessionFactory sessionFactory;
	
	public SupplierIdGenerator() {
		// TODO Auto-generated constructor stub
	}
	
	public SupplierIdGenerator(SessionFactory sessionFactory) {
		this.sessionFactory=sessionFactory;
	}
	
	
	public String generateSupplierId() {

        try {
        	
        	String prefix = "SUP";

        	String hql="from Supplier";
        	
        	Query query=sessionFactory.getCurrentSession().createQuery(hql); 
        	List<Supplier> list=(List<Supplier>)query.list();
        	
        	if(list.isEmpty())
        	{
        		String generatedId = prefix + new Long(1000000001).toString();
                System.out.println("generateBusinessNameId : " + generatedId);
                
                return generatedId;
        	}
        	
        	
        	long ids[]=new long[list.size()]; 
        	int i=0;
        	for(Supplier supplier : list)
        	{
        		ids[i]=Long.parseLong(supplier.getSupplierId().substring(3));
        		i++;
        	}
        	
        	long max = ids[0];
            for(int p = 0; p < ids.length; p++)
            {
                if(max < ids[p])
                {
                    max = ids[p];
                }
            }
        	
            long id=max+1;
            String generatedId = prefix + new Long(id).toString();
            System.out.println("generateSupplierId : " + generatedId);
            
            return generatedId;
            
        } catch (Exception e) {
            System.out.println("generateSupplierId Error : "+e.toString());        }

        return null;
	}

}
