package com.sheetalarch.utils;

import java.sql.Blob;
import java.sql.SQLException;

import org.hibernate.Hibernate;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;

public class ImageConvertor {

	@Autowired
	SessionFactory sessionFactory;
	
	public ImageConvertor(SessionFactory sessionFactory) {
	this.sessionFactory=sessionFactory;
	}

	public static String convertBlobToString(Blob decodedStringblob) {

		String encodedString = null;
		try {
			encodedString = java.util.Base64.getEncoder()
					.encodeToString(decodedStringblob.getBytes(1L, (int) decodedStringblob.length()));
		} catch (SQLException e) {
			System.out.println(" convertBlobtoString Error : " + e.toString());
			return null;
		}
		return encodedString;
	}

	public Blob convertStringToBlob(String encodedImageStr) {

		Blob b = Hibernate.getLobCreator(sessionFactory.getCurrentSession())
				.createBlob(Base64.decode(encodedImageStr, Base64.DEFAULT));

		return b;
	}

}
