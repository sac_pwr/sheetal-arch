package com.sheetalarch.utils;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.sheetalarch.entity.ReOrderDetails;



@Component
public class ReturnOrderIdGenerator{

	@Autowired
	SessionFactory sessionFactory;
	
	public ReturnOrderIdGenerator(SessionFactory sessionFactory) {
		this.sessionFactory=sessionFactory;
	}
	
    
    public String generate() {

        try {
        	String prefix = "REORD";

        	//long count=(long) sessionFactory.getCurrentSession().createCriteria("Inventory").setProjection(Projections.rowCount()).uniqueResult();
        	
        	String hql="from ReOrderDetails";
        	
        	Query query=sessionFactory.getCurrentSession().createQuery(hql); 
        	List<ReOrderDetails> list=(List<ReOrderDetails>)query.list();
        	
        	if(list.isEmpty())
        	{
        		String generatedId = prefix + new Long(1000000001).toString();
                System.out.println("generateBusinessNameId : " + generatedId);
                
                return generatedId;
        	}
        	
        	
        	long ids[]=new long[list.size()]; 
        	int i=0;
        	for(ReOrderDetails reOrderDetails : list)
        	{
        		ids[i]=Long.parseLong(reOrderDetails.getReOrderDetailsId().substring(3));
        		i++;
        	}
        	
        	long max = ids[0];
            for(int p = 0; p < ids.length; p++)
            {
                if(max < ids[p])
                {
                    max = ids[p];
                }
            }
        	
            long id=max+1;
                String generatedId = prefix + new Long(id).toString();
                System.out.println("ReOrderDetails Id: " + generatedId);
                return generatedId;
            
        } catch (Exception e) {
            System.out.println("ReOrderDetails   Error : "+e.toString());        }

        return null;
	}

}
