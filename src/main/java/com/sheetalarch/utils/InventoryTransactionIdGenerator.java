package com.sheetalarch.utils;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.sheetalarch.entity.Inventory;



@Component
public class InventoryTransactionIdGenerator{

	
	@Autowired
	SessionFactory sessionFactory;
	
	public InventoryTransactionIdGenerator() {
		// TODO Auto-generated constructor stub
	}
	
	public InventoryTransactionIdGenerator(SessionFactory sessionFactory) {
		this.sessionFactory=sessionFactory;
	}
	
	@Transactional
	public String generateInventoryTransactionId() {
		
    try {
        	String prefix = "TSN";

        	//long count=(long) sessionFactory.getCurrentSession().createCriteria("Inventory").setProjection(Projections.rowCount()).uniqueResult();
        	
        	String hql="from Inventory";
        	
        	Query query=sessionFactory.getCurrentSession().createQuery(hql); 
        	List<Inventory> list=(List<Inventory>)query.list();
        	
        	if(list.isEmpty())
        	{
        		String generatedId = prefix + new Long(1000000001).toString();
                System.out.println("generateBusinessNameId : " + generatedId);
                
                return generatedId;
        	}
        	
        	
        	long ids[]=new long[list.size()]; 
        	int i=0;
        	for(Inventory inventory : list)
        	{
        		ids[i]=Long.parseLong(inventory.getInventoryTransactionId().substring(3));
        		i++;
        	}
        	
        	long max = ids[0];
            for(int p = 0; p < ids.length; p++)
            {
                if(max < ids[p])
                {
                    max = ids[p];
                }
            }
        	
            long id=max+1;
                String generatedId = prefix + new Long(id).toString();
                System.out.println("InventoryTransactionIdGenerator Id: " + generatedId);
                return generatedId;
            
        } catch (Exception e) {
            System.out.println("InventoryTransactionIdGenerator Error : "+e.toString());        }

        return null;
	}

}
