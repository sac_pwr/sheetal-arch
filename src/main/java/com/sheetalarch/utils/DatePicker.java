package com.sheetalarch.utils;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class DatePicker {
	public static String getCurrentMonthLastDate()
	{
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		Calendar c = Calendar.getInstance(); // this takes current date
		c.setTime(new Date());
		c.set(Calendar.DAY_OF_MONTH, c.getActualMaximum(Calendar.DAY_OF_MONTH));
		System.out.println("end date " + dateFormat.format(c.getTime()));
		return dateFormat.format(c.getTime());
	}
	public static String getCurrentMonthStartDate()
	{
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		Calendar c = Calendar.getInstance(); // this takes current date
		c.set(Calendar.DAY_OF_MONTH, 1);
		System.out.println("start date " + dateFormat.format(c.getTime()));
		
		return dateFormat.format(c.getTime());
	}
	public static int daysBetween(Date d1, Date d2)
	   {
	      return (int)( (d2.getTime() - d1.getTime()) / (1000 * 60 * 60 * 24));
	   }
}
