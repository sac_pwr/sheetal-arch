package com.sheetalarch.utils;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.json.simple.JSONObject;



public class Notification {
	
	public static boolean sendNotificationToASMForCancelOrder(String token,String salesPersonName,String boothNo,String ownerName,String orderId,long totalQty) {
		System.out.println("inside sendNotificationToASMForCancelOrder");
		try {
			if (token != null && !token.equals("")) {

				HttpClient client = HttpClientBuilder.create().build();
				HttpPost post = new HttpPost("https://fcm.googleapis.com/fcm/send");
				post.setHeader("Content-type", "application/json");
				post.setHeader("Authorization",
						"key=AAAA3nRsyCU:APA91bHp_ZcatdPKmIDH9hh7TVtH3IqlbeD5FrhaOwRhtS-jbN6Z-9apWyJES3kOYA0emgUHLkoZJiV0221dKL76EyFWq62yG4b-hYsOTYDMhJNbafDqDR_cfOhahBKaNH9lzJYZVrvR");

				JSONObject message = new JSONObject();
				message.put("to", token);
				message.put("priority", "high");

				JSONObject data = new JSONObject();
				data.put("orderDetailsId", orderId);

				message.put("data", data);

				JSONObject notification = new JSONObject();
				notification.put("title", "Sheetal Arch");
				notification.put("click_action", "OPEN_CANCEL_ORDER_DETAILS");
				notification.put("body", "Order "+ orderId+" (total "+totalQty+" quantity) is Cancel by " + salesPersonName + " for booth no : "+ boothNo+"("+ownerName+")");
				     

				message.put("notification", notification);

				post.setEntity(new StringEntity(message.toString(), "UTF-8"));
				HttpResponse response = client.execute(post);
				System.out.println("\n\n ******* Notification Send SuccessFully ********** \n\n Notification response is : "+response+"\n\n Notification Message is : "+message);
			}
		} catch (Exception e) {
			System.out.println("\n\n ******* Notification not Send ********** \n\n Eror is : "+e.toString());
		}

		return false;
	}
	
	public static boolean sendNotificationToDeliveryPersonForDispatchedOrder(String token,String gkName,long totalQty) {
		System.out.println("inside sendNotificationToDeliveryBoyForOrder");
		try {
			if (token != null && !token.equals("")) {

				HttpClient client = HttpClientBuilder.create().build();
				HttpPost post = new HttpPost("https://fcm.googleapis.com/fcm/send");
				post.setHeader("Content-type", "application/json");
				post.setHeader("Authorization",
						"key=AAAA3nRsyCU:APA91bHp_ZcatdPKmIDH9hh7TVtH3IqlbeD5FrhaOwRhtS-jbN6Z-9apWyJES3kOYA0emgUHLkoZJiV0221dKL76EyFWq62yG4b-hYsOTYDMhJNbafDqDR_cfOhahBKaNH9lzJYZVrvR");
				

				JSONObject message = new JSONObject();
				message.put("to", token);
				message.put("priority", "high");

				JSONObject data = new JSONObject();
				data.put("totalQtyId", totalQty);


				JSONObject notification = new JSONObject();
				notification.put("title", "SheetalArch");
				notification.put("click_action", "OPEN_DISPATCHED_ORDER_DETAILS");
				notification.put("body", "Your Total  "+ totalQty+" Quantity is ready for dispatched From " + gkName );

				
				message.put("notification", notification);

				post.setEntity(new StringEntity(message.toString(), "UTF-8"));
				HttpResponse response = client.execute(post);
				System.out.println("\n\n ******* Notification Send DB SuccessFully ********** \n\n Notification response is : "+response+"\n\n Notification Message is : "+message);
			}
		} catch (Exception e) {
			System.out.println("\n\n ******* Notification not DB Send ********** \n\n Eror is : "+e.toString());
		}

		return false;
	}

	public static boolean sendNotificationToASMForDispatchedOrderConfirmation(String token, String employeeName,long totalDispatchedQty,String gateKeeperName) {
		System.out.println("inside sendNotificationToASMForDispatchedOrderConfirmation");
		try {
			if (token != null && !token.equals("")) {

				HttpClient client = HttpClientBuilder.create().build();
				HttpPost post = new HttpPost("https://fcm.googleapis.com/fcm/send");
				post.setHeader("Content-type", "application/json");
				post.setHeader("Authorization",
						"key=AAAA3nRsyCU:APA91bHp_ZcatdPKmIDH9hh7TVtH3IqlbeD5FrhaOwRhtS-jbN6Z-9apWyJES3kOYA0emgUHLkoZJiV0221dKL76EyFWq62yG4b-hYsOTYDMhJNbafDqDR_cfOhahBKaNH9lzJYZVrvR");

				JSONObject message = new JSONObject();
				message.put("to", token);
				message.put("priority", "high");

				JSONObject data = new JSONObject();
				data.put("totalDispatchedQtyId", totalDispatchedQty);

				message.put("data", data);

				JSONObject notification = new JSONObject();
				notification.put("title", "SheetalArch");
				notification.put("click_action", "OPEN_DISPATCHED_ORDER_DETAILS_ASM");
				notification.put("body", employeeName+" has received total "+totalDispatchedQty+" Quantity for delivery from "+gateKeeperName);

				message.put("notification", notification);

				post.setEntity(new StringEntity(message.toString(), "UTF-8"));
				HttpResponse response = client.execute(post);
				System.out.println("\n\n ******* Notification Send  SuccessFully ********** \n\n Notification response is : "+response+"\n\n Notification Message is : "+message);
			}
		} catch (Exception e) {
			System.out.println("\n\n ******* Notification not  Send ********** \n\n Eror is : "+e.toString());
		}

		return false;
	}
}
