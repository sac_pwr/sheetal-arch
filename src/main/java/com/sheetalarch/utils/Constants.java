package com.sheetalarch.utils;

public class Constants {
	public static String SUCCESS_RESPONSE="Success";
	public static String UPDATE_SUCCESS_RESPONSE="UpdateSuccess";
	public static String FAILURE_RESPONSE="Failure";
	
	
	public static String ORDER_RESPONSE="Order Succesfully Done";
	
	public static String GATE_KEEPER_DEPT_NAME = "GateKeeper";
	public static String SALESMAN_DEPT_NAME = "SalesMan";
	public static String AREA_SALESMAN_DEPT_NAME = "AreaSalesMan";
	public static String SUPPLIER_TYPE = "Supplier";
	
	public static String ALREADY_EXIST="Already exist";
	public static String SAVE_SUCCESS="SuccessFully Saved";
	public static String UPDATE_SUCCESS="SuccessFully Updated";
	public static String NOT_FOUND="Not Found";
	public static String NO_CONTENT="Data Not Found";
	public static String LOGIN_FAILED="Invalid UserId or Password";
	public static String NONE="None";
	
	public static String ORDER_STATUS_BOOKED="Booked";
	public static String ORDER_STATUS_PACKED="Packed";
	public static String ORDER_STATUS_ISSUED="Issued";
	public static String ORDER_STATUS_DELIVERED="Delivered";
	public static String ORDER_STATUS_ReOrder="ReOrder";
	public static String ORDER_STATUS_CANCELED="Canceled";
	
	public static String CASH_PAY_STATUS="Cash";
	public static String CHEQUE_PAY_STATUS="Cheque";
	
	public static String FULL_PAY_STATUS="Full Payment";
	public static String PARTIAL_PAY_STATUS="Partial Payment";	
	
	public static String PAY_STATUS_CURRENT_DATE_PAYMENT="Current";
	public static String PAY_STATUS_PENDING_DATE_PAYMENT="Pending";
	public static String PAY_STATUS_PAID_DATE_PAYMENT="Paid";
	
	public static String ISSUE_STATUS_ISSUED="Issued";
	public static String ISSUE_STATUS_REISSUED="ReIssued";
	
	public static String REORDER_STATUS_PENDING="Pending";
	public static String REORDER_STATUS_COMPLETE="Complete";
	
	public static String BOOTH_NO_AVAILABLE = "Booth No. Available";
    public static String BOOTH_NO_DUPLICATION = "Entered Booth No. already exist";
    
    public static String TAX_TYPE_INTRA="Intra";
    public static String TAX_TYPE_INTER="Inter";
    
    public static String TODAY_ORDER="Today";
    public static String FUTURE_ORDER="Future";
	
}
