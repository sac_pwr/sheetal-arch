package com.sheetalarch.utils;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.sheetalarch.entity.OrderDetails;


/**
 * @author aNKIT
 *
 */
public class InvoiceNumberGenerate {

	@Autowired
	SessionFactory sessionFactory;
	
	public InvoiceNumberGenerate(SessionFactory sessionFactory) {
		this.sessionFactory=sessionFactory;
	}
	
    
    public String generateInvoiceNumber() {

        try {
        	String prefix = "INV";

        	//long count=(long) sessionFactory.getCurrentSession().createCriteria("Inventory").setProjection(Projections.rowCount()).uniqueResult();
        	
        	String hql="from OrderDetails where invoiceNumber is not null";
        	
        	Query query=sessionFactory.getCurrentSession().createQuery(hql); 
        	List<OrderDetails> list=(List<OrderDetails>)query.list();
        	
        	if(list.isEmpty())
        	{
        		String generatedId = prefix + new Long(1000000001).toString();
                System.out.println("generateInvoiceNumber : " + generatedId);
                
                return generatedId;
        	}
        	
        	
        	long ids[]=new long[list.size()]; 
        	int i=0;
        	for(OrderDetails orderDetails : list)
        	{
        		if(orderDetails.getInvoiceNumber()!=null)
        		{
	        		ids[i]=Long.parseLong(orderDetails.getInvoiceNumber().substring(3));
	        		i++;
        		}
        	}
        	
        	long max = ids[0];
            for(int p = 0; p < ids.length; p++)
            {
                if(max < ids[p])
                {
                    max = ids[p];
                }
            }
            
            if(ids.length==0)
            {
            	String generatedId = prefix + new Long(1000000001).toString();
                System.out.println("generateInvoiceNumber : " + generatedId);
                
                return generatedId;
            }
        	
            long id=max+1;
            String generatedId = prefix + new Long(id).toString();
            System.out.println("InvoiceNumberGenerate Id: " + generatedId);
            
            return generatedId;
            
        } catch (Exception e) {
            System.out.println("InvoiceNumberGenerate Error : "+e.toString());        }

        return null;
	}

}
