package com.sheetalarch.utils;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.sheetalarch.entity.SupplierOrder;



@Component
public class OrderIdSupplierGenerator{

	@Autowired
	SessionFactory sessionFactory;
	
	public OrderIdSupplierGenerator(SessionFactory sessionFactory) {
		this.sessionFactory=sessionFactory;
	}
	
    
    public String generate() {

        try {
        	String prefix = "ORDS";

        	//long count=(long) sessionFactory.getCurrentSession().createCriteria("Inventory").setProjection(Projections.rowCount()).uniqueResult();
        	
        	String hql="from SupplierOrder";
        	
        	Query query=sessionFactory.getCurrentSession().createQuery(hql); 
        	List<SupplierOrder> list=(List<SupplierOrder>)query.list();
        	
        	if(list.isEmpty())
        	{
        		String generatedId = prefix + new Long(1000000001).toString();
                System.out.println("generateBusinessNameId : " + generatedId);
                
                return generatedId;
        	}
        	
        	
        	long ids[]=new long[list.size()]; 
        	int i=0;
        	for(SupplierOrder supplierOrder : list)
        	{
        		ids[i]=Long.parseLong(supplierOrder.getSupplierOrderId().substring(4));
        		i++;
        	}
        	
        	long max = ids[0];
            for(int p = 0; p < ids.length; p++)
            {
                if(max < ids[p])
                {
                    max = ids[p];
                }
            }
        	
            long id=max+1;
                String generatedId = prefix + new Long(id).toString();
                System.out.println("OrderIdSupplierGenerator Id: " + generatedId);
                return generatedId;
            
        } catch (Exception e) {
            System.out.println("OrderIdSupplierGenerator Error : "+e.toString());        
            }

        return null;
	}

}
