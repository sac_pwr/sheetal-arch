/**
 * 
 */
package com.sheetalarch.admin.models;

import java.util.List;

/**
 * @author aNKIT
 *
 */
public class SMReportOrderDetails {

	private String name;
	private long employeeDetailsId;
	private String type;
	private String areaName;
	private String mobileNumber;
	private String clusterName;
	private String routeName;
	private String ASMName;
	private String totalAmount;
	private List<SMReportOrders> SMReportOrdersList;
	public SMReportOrderDetails(String name, long employeeDetailsId, String type, String areaName, String mobileNumber,
			String clusterName, String routeName, String aSMName, String totalAmount,
			List<SMReportOrders> sMReportOrdersList) {
		super();
		this.name = name;
		this.employeeDetailsId = employeeDetailsId;
		this.type = type;
		this.areaName = areaName;
		this.mobileNumber = mobileNumber;
		this.clusterName = clusterName;
		this.routeName = routeName;
		ASMName = aSMName;
		this.totalAmount = totalAmount;
		SMReportOrdersList = sMReportOrdersList;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public long getEmployeeDetailsId() {
		return employeeDetailsId;
	}
	public void setEmployeeDetailsId(long employeeDetailsId) {
		this.employeeDetailsId = employeeDetailsId;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getAreaName() {
		return areaName;
	}
	public void setAreaName(String areaName) {
		this.areaName = areaName;
	}
	public String getMobileNumber() {
		return mobileNumber;
	}
	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}
	public String getClusterName() {
		return clusterName;
	}
	public void setClusterName(String clusterName) {
		this.clusterName = clusterName;
	}
	public String getRouteName() {
		return routeName;
	}
	public void setRouteName(String routeName) {
		this.routeName = routeName;
	}
	public String getASMName() {
		return ASMName;
	}
	public void setASMName(String aSMName) {
		ASMName = aSMName;
	}
	public String getTotalAmount() {
		return totalAmount;
	}
	public void setTotalAmount(String totalAmount) {
		this.totalAmount = totalAmount;
	}
	public List<SMReportOrders> getSMReportOrdersList() {
		return SMReportOrdersList;
	}
	public void setSMReportOrdersList(List<SMReportOrders> sMReportOrdersList) {
		SMReportOrdersList = sMReportOrdersList;
	}
	@Override
	public String toString() {
		return "SMReportOrderDetails [name=" + name + ", employeeDetailsId=" + employeeDetailsId + ", type=" + type
				+ ", areaName=" + areaName + ", mobileNumber=" + mobileNumber + ", clusterName=" + clusterName
				+ ", routeName=" + routeName + ", ASMName=" + ASMName + ", totalAmount=" + totalAmount
				+ ", SMReportOrdersList=" + SMReportOrdersList + "]";
	}


	
	
	
}
