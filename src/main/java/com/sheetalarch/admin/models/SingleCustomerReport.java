/**
 * 
 */
package com.sheetalarch.admin.models;

import java.util.List;

import com.sheetalarch.entity.Booth;

/**
 * @author aNKIT
 *
 */
public class SingleCustomerReport {

	private Booth booth;
	private double totalAmount;
	private double totalAmountPaid;
	private double totalAmountUnPaid;
	private List<SingleCustomerOrderReport> singleCustomerOrderReportList;
	public SingleCustomerReport(Booth booth, double totalAmount, double totalAmountPaid, double totalAmountUnPaid,
			List<SingleCustomerOrderReport> singleCustomerOrderReportList) {
		super();
		this.booth = booth;
		this.totalAmount = totalAmount;
		this.totalAmountPaid = totalAmountPaid;
		this.totalAmountUnPaid = totalAmountUnPaid;
		this.singleCustomerOrderReportList = singleCustomerOrderReportList;
	}
	public Booth getBooth() {
		return booth;
	}
	public void setBooth(Booth booth) {
		this.booth = booth;
	}
	public double getTotalAmount() {
		return totalAmount;
	}
	public void setTotalAmount(double totalAmount) {
		this.totalAmount = totalAmount;
	}
	public double getTotalAmountPaid() {
		return totalAmountPaid;
	}
	public void setTotalAmountPaid(double totalAmountPaid) {
		this.totalAmountPaid = totalAmountPaid;
	}
	public double getTotalAmountUnPaid() {
		return totalAmountUnPaid;
	}
	public void setTotalAmountUnPaid(double totalAmountUnPaid) {
		this.totalAmountUnPaid = totalAmountUnPaid;
	}
	public List<SingleCustomerOrderReport> getSingleCustomerOrderReportList() {
		return singleCustomerOrderReportList;
	}
	public void setSingleCustomerOrderReportList(List<SingleCustomerOrderReport> singleCustomerOrderReportList) {
		this.singleCustomerOrderReportList = singleCustomerOrderReportList;
	}
	@Override
	public String toString() {
		return "SingleCustomerReport [booth=" + booth + ", totalAmount=" + totalAmount + ", totalAmountPaid="
				+ totalAmountPaid + ", totalAmountUnPaid=" + totalAmountUnPaid + ", singleCustomerOrderReportList="
				+ singleCustomerOrderReportList + "]";
	}
	
	
}
