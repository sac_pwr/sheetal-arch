/**
 * 
 */
package com.sheetalarch.admin.models;

import com.sheetalarch.entity.EmployeeDetails;
import com.sheetalarch.entity.OrderDetails;

/**
 * @author aNKIT
 *
 */
public class OrderReportModel {

	private long srno;
	private String orderId;
	private long boothId;
	private String boothNo;
	private String ownerName;
	private String routeNo;
	private String smName;
	private long employeeDetailsId;
	private double amount;
	private String dateOfOrder;
	private String dateOfDelivery;
	private String orderStatusSM;
	private String orderStatusGK;
	private String orderStatusDB;
	
	
	
	public OrderReportModel(long srno, String orderId, long boothId, String boothNo, String ownerName, String routeNo,
			String smName, long employeeDetailsId, double amount, String dateOfOrder, String dateOfDelivery,
			String orderStatusSM, String orderStatusGK, String orderStatusDB) {
		super();
		this.srno = srno;
		this.orderId = orderId;
		this.boothId = boothId;
		this.boothNo = boothNo;
		this.ownerName = ownerName;
		this.routeNo = routeNo;
		this.smName = smName;
		this.employeeDetailsId = employeeDetailsId;
		this.amount = amount;
		this.dateOfOrder = dateOfOrder;
		this.dateOfDelivery = dateOfDelivery;
		this.orderStatusSM = orderStatusSM;
		this.orderStatusGK = orderStatusGK;
		this.orderStatusDB = orderStatusDB;
	}
	public long getSrno() {
		return srno;
	}
	public void setSrno(long srno) {
		this.srno = srno;
	}
	public String getOrderId() {
		return orderId;
	}
	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}
	public long getBoothId() {
		return boothId;
	}
	public void setBoothId(long boothId) {
		this.boothId = boothId;
	}
	public String getBoothNo() {
		return boothNo;
	}
	public void setBoothNo(String boothNo) {
		this.boothNo = boothNo;
	}
	public String getOwnerName() {
		return ownerName;
	}
	public void setOwnerName(String ownerName) {
		this.ownerName = ownerName;
	}
	public String getRouteNo() {
		return routeNo;
	}
	public void setRouteNo(String routeNo) {
		this.routeNo = routeNo;
	}
	public String getSmName() {
		return smName;
	}
	public void setSmName(String smName) {
		this.smName = smName;
	}
	public long getEmployeeDetailsId() {
		return employeeDetailsId;
	}
	public void setEmployeeDetailsId(long employeeDetailsId) {
		this.employeeDetailsId = employeeDetailsId;
	}
	public double getAmount() {
		return amount;
	}
	public void setAmount(double amount) {
		this.amount = amount;
	}
	public String getDateOfOrder() {
		return dateOfOrder;
	}
	public void setDateOfOrder(String dateOfOrder) {
		this.dateOfOrder = dateOfOrder;
	}
	public String getDateOfDelivery() {
		return dateOfDelivery;
	}
	public void setDateOfDelivery(String dateOfDelivery) {
		this.dateOfDelivery = dateOfDelivery;
	}
	public String getOrderStatusSM() {
		return orderStatusSM;
	}
	public void setOrderStatusSM(String orderStatusSM) {
		this.orderStatusSM = orderStatusSM;
	}
	public String getOrderStatusGK() {
		return orderStatusGK;
	}
	public void setOrderStatusGK(String orderStatusGK) {
		this.orderStatusGK = orderStatusGK;
	}
	public String getOrderStatusDB() {
		return orderStatusDB;
	}
	public void setOrderStatusDB(String orderStatusDB) {
		this.orderStatusDB = orderStatusDB;
	}
	@Override
	public String toString() {
		return "OrderReportModel [srno=" + srno + ", orderId=" + orderId + ", boothId=" + boothId + ", boothNo="
				+ boothNo + ", ownerName=" + ownerName + ", routeNo=" + routeNo + ", smName=" + smName
				+ ", employeeDetailsId=" + employeeDetailsId + ", amount=" + amount + ", dateOfOrder=" + dateOfOrder
				+ ", dateOfDelivery=" + dateOfDelivery + ", orderStatusSM=" + orderStatusSM + ", orderStatusGK="
				+ orderStatusGK + ", orderStatusDB=" + orderStatusDB + "]";
	}
	
	
	
}
