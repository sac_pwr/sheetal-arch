/**
 * 
 */
package com.sheetalarch.admin.models;

/**
 * @author aNKIT
 *
 */
public class SMReportOrders {

	private long srno;
	private String orderId;
	private String boothNo;
	private String ownerName;
	private String orderAmount;
	private String dateOfOrderBooked;
	private String dateOfOrderDelivered;
	public SMReportOrders(long srno, String orderId, String boothNo, String ownerName, String orderAmount,
			String dateOfOrderBooked, String dateOfOrderDelivered) {
		super();
		this.srno = srno;
		this.orderId = orderId;
		this.boothNo = boothNo;
		this.ownerName = ownerName;
		this.orderAmount = orderAmount;
		this.dateOfOrderBooked = dateOfOrderBooked;
		this.dateOfOrderDelivered = dateOfOrderDelivered;
	}
	public long getSrno() {
		return srno;
	}
	public void setSrno(long srno) {
		this.srno = srno;
	}
	public String getOrderId() {
		return orderId;
	}
	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}
	public String getBoothNo() {
		return boothNo;
	}
	public void setBoothNo(String boothNo) {
		this.boothNo = boothNo;
	}
	public String getOwnerName() {
		return ownerName;
	}
	public void setOwnerName(String ownerName) {
		this.ownerName = ownerName;
	}
	public String getOrderAmount() {
		return orderAmount;
	}
	public void setOrderAmount(String orderAmount) {
		this.orderAmount = orderAmount;
	}
	public String getDateOfOrderBooked() {
		return dateOfOrderBooked;
	}
	public void setDateOfOrderBooked(String dateOfOrderBooked) {
		this.dateOfOrderBooked = dateOfOrderBooked;
	}
	public String getDateOfOrderDelivered() {
		return dateOfOrderDelivered;
	}
	public void setDateOfOrderDelivered(String dateOfOrderDelivered) {
		this.dateOfOrderDelivered = dateOfOrderDelivered;
	}
	@Override
	public String toString() {
		return "SMReportOrders [srno=" + srno + ", orderId=" + orderId + ", boothNo=" + boothNo + ", ownerName="
				+ ownerName + ", orderAmount=" + orderAmount + ", dateOfOrderBooked=" + dateOfOrderBooked
				+ ", dateOfOrderDelivered=" + dateOfOrderDelivered + "]";
	}
	
}
