/**
 * 
 */
package com.sheetalarch.admin.models;

import com.sheetalarch.entity.EmployeeDetails;

/**
 * @author aNKIT
 *
 */
public class SMListAssignToASMModel {

	private long areaId;
	private long routeId;
	private EmployeeDetails employeeDetails;
	public SMListAssignToASMModel(long areaId, long routeId, EmployeeDetails employeeDetails) {
		super();
		this.areaId = areaId;
		this.routeId = routeId;
		this.employeeDetails = employeeDetails;
	}
	public long getAreaId() {
		return areaId;
	}
	public void setAreaId(long areaId) {
		this.areaId = areaId;
	}
	public long getRouteId() {
		return routeId;
	}
	public void setRouteId(long routeId) {
		this.routeId = routeId;
	}
	public EmployeeDetails getEmployeeDetails() {
		return employeeDetails;
	}
	public void setEmployeeDetails(EmployeeDetails employeeDetails) {
		this.employeeDetails = employeeDetails;
	}
	@Override
	public String toString() {
		return "SMListAssignToASMModel [areaId=" + areaId + ", routeId=" + routeId + ", employeeDetails="
				+ employeeDetails + "]";
	}
	
	
	
}
