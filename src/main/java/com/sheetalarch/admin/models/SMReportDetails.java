/**
 * 
 */
package com.sheetalarch.admin.models;

/**
 * @author aNKIT
 *
 */
public class SMReportDetails {

	private long srNo;
	private long employeeDetailsId;
	private String salesManName;
	private String routeNo;
	private String areaName;
	private String clusterName;
	private String totalNoOfOrder;
	private String totalAmount;
	public SMReportDetails(long srNo, long employeeDetailsId, String salesManName, String routeNo, String areaName,
			String clusterName, String totalNoOfOrder, String totalAmount) {
		super();
		this.srNo = srNo;
		this.employeeDetailsId = employeeDetailsId;
		this.salesManName = salesManName;
		this.routeNo = routeNo;
		this.areaName = areaName;
		this.clusterName = clusterName;
		this.totalNoOfOrder = totalNoOfOrder;
		this.totalAmount = totalAmount;
	}
	public long getSrNo() {
		return srNo;
	}
	public void setSrNo(long srNo) {
		this.srNo = srNo;
	}
	public long getEmployeeDetailsId() {
		return employeeDetailsId;
	}
	public void setEmployeeDetailsId(long employeeDetailsId) {
		this.employeeDetailsId = employeeDetailsId;
	}
	public String getSalesManName() {
		return salesManName;
	}
	public void setSalesManName(String salesManName) {
		this.salesManName = salesManName;
	}
	public String getRouteNo() {
		return routeNo;
	}
	public void setRouteNo(String routeNo) {
		this.routeNo = routeNo;
	}
	public String getAreaName() {
		return areaName;
	}
	public void setAreaName(String areaName) {
		this.areaName = areaName;
	}
	public String getClusterName() {
		return clusterName;
	}
	public void setClusterName(String clusterName) {
		this.clusterName = clusterName;
	}
	public String getTotalNoOfOrder() {
		return totalNoOfOrder;
	}
	public void setTotalNoOfOrder(String totalNoOfOrder) {
		this.totalNoOfOrder = totalNoOfOrder;
	}
	public String getTotalAmount() {
		return totalAmount;
	}
	public void setTotalAmount(String totalAmount) {
		this.totalAmount = totalAmount;
	}
	@Override
	public String toString() {
		return "SMReportDetails [srNo=" + srNo + ", employeeDetailsId=" + employeeDetailsId + ", salesManName="
				+ salesManName + ", routeNo=" + routeNo + ", areaName=" + areaName + ", clusterName=" + clusterName
				+ ", totalNoOfOrder=" + totalNoOfOrder + ", totalAmount=" + totalAmount + "]";
	}
	
}
