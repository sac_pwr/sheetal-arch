package com.sheetalarch.admin.models;

import java.util.Date;

public class FetchSupplierList {

	private String supplierId;
	private long srNo;
	private String name;
	private String emailId;
	private String mobileNumber;
	private String address;
	private String gstinNo;
	private double balAmt;
	private Date addedDate;
	private Date updatedDate;
	public FetchSupplierList(String supplierId, long srNo, String name, String emailId, String mobileNumber,
			String address, String gstinNo, double balAmt, Date addedDate, Date updatedDate) {
		super();
		this.supplierId = supplierId;
		this.srNo = srNo;
		this.name = name;
		this.emailId = emailId;
		this.mobileNumber = mobileNumber;
		this.address = address;
		this.gstinNo = gstinNo;
		this.balAmt = balAmt;
		this.addedDate = addedDate;
		this.updatedDate = updatedDate;
	}
	public String getSupplierId() {
		return supplierId;
	}
	public void setSupplierId(String supplierId) {
		this.supplierId = supplierId;
	}
	public long getSrNo() {
		return srNo;
	}
	public void setSrNo(long srNo) {
		this.srNo = srNo;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getEmailId() {
		return emailId;
	}
	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}
	public String getMobileNumber() {
		return mobileNumber;
	}
	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getGstinNo() {
		return gstinNo;
	}
	public void setGstinNo(String gstinNo) {
		this.gstinNo = gstinNo;
	}
	public double getBalAmt() {
		return balAmt;
	}
	public void setBalAmt(double balAmt) {
		this.balAmt = balAmt;
	}
	public Date getAddedDate() {
		return addedDate;
	}
	public void setAddedDate(Date addedDate) {
		this.addedDate = addedDate;
	}
	public Date getUpdatedDate() {
		return updatedDate;
	}
	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}
	@Override
	public String toString() {
		return "FetchSupplierList [supplierId=" + supplierId + ", srNo=" + srNo + ", name=" + name + ", emailId="
				+ emailId + ", mobileNumber=" + mobileNumber + ", address=" + address + ", gstinNo=" + gstinNo
				+ ", balAmt=" + balAmt + ", addedDate=" + addedDate + ", updatedDate=" + updatedDate + "]";
	}
	
	
	
	
}
