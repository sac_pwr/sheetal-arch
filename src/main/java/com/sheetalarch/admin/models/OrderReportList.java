package com.sheetalarch.admin.models;

import java.util.Date;

public class OrderReportList {

	private long srno;
	private String orderId;
	private double amountWithoutTax;
	private double amountWithTax;
	private String areaName;
	private long totalQuantity;
	private String shopName;
	private String businessNameId;
	private String employeeName;
	private String employeeDetailsId;
	private long paymentPeriodDays;
	private Date orderDate;
	private Date paymentDate;
	private String orderStatusSM;
	private String orderStatusSMDate;
	private String orderStatusGK;
	private String orderStatusGKDate;
	private String orderStatusDB;
	private String orderStatusDBDate;
	public OrderReportList(long srno, String orderId, double amountWithoutTax, double amountWithTax, String areaName,
			long totalQuantity, String shopName, String businessNameId, String employeeName, String employeeDetailsId,
			long paymentPeriodDays, Date orderDate, Date paymentDate, String orderStatusSM, String orderStatusSMDate,
			String orderStatusGK, String orderStatusGKDate, String orderStatusDB, String orderStatusDBDate) {
		super();
		this.srno = srno;
		this.orderId = orderId;
		this.amountWithoutTax = amountWithoutTax;
		this.amountWithTax = amountWithTax;
		this.areaName = areaName;
		this.totalQuantity = totalQuantity;
		this.shopName = shopName;
		this.businessNameId = businessNameId;
		this.employeeName = employeeName;
		this.employeeDetailsId = employeeDetailsId;
		this.paymentPeriodDays = paymentPeriodDays;
		this.orderDate = orderDate;
		this.paymentDate = paymentDate;
		this.orderStatusSM = orderStatusSM;
		this.orderStatusSMDate = orderStatusSMDate;
		this.orderStatusGK = orderStatusGK;
		this.orderStatusGKDate = orderStatusGKDate;
		this.orderStatusDB = orderStatusDB;
		this.orderStatusDBDate = orderStatusDBDate;
	}
	public long getSrno() {
		return srno;
	}
	public void setSrno(long srno) {
		this.srno = srno;
	}
	public String getOrderId() {
		return orderId;
	}
	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}
	public double getAmountWithoutTax() {
		return amountWithoutTax;
	}
	public void setAmountWithoutTax(double amountWithoutTax) {
		this.amountWithoutTax = amountWithoutTax;
	}
	public double getAmountWithTax() {
		return amountWithTax;
	}
	public void setAmountWithTax(double amountWithTax) {
		this.amountWithTax = amountWithTax;
	}
	public String getAreaName() {
		return areaName;
	}
	public void setAreaName(String areaName) {
		this.areaName = areaName;
	}
	public long getTotalQuantity() {
		return totalQuantity;
	}
	public void setTotalQuantity(long totalQuantity) {
		this.totalQuantity = totalQuantity;
	}
	public String getShopName() {
		return shopName;
	}
	public void setShopName(String shopName) {
		this.shopName = shopName;
	}
	public String getBusinessNameId() {
		return businessNameId;
	}
	public void setBusinessNameId(String businessNameId) {
		this.businessNameId = businessNameId;
	}
	public String getEmployeeName() {
		return employeeName;
	}
	public void setEmployeeName(String employeeName) {
		this.employeeName = employeeName;
	}
	public String getEmployeeDetailsId() {
		return employeeDetailsId;
	}
	public void setEmployeeDetailsId(String employeeDetailsId) {
		this.employeeDetailsId = employeeDetailsId;
	}
	public long getPaymentPeriodDays() {
		return paymentPeriodDays;
	}
	public void setPaymentPeriodDays(long paymentPeriodDays) {
		this.paymentPeriodDays = paymentPeriodDays;
	}
	public Date getOrderDate() {
		return orderDate;
	}
	public void setOrderDate(Date orderDate) {
		this.orderDate = orderDate;
	}
	public Date getPaymentDate() {
		return paymentDate;
	}
	public void setPaymentDate(Date paymentDate) {
		this.paymentDate = paymentDate;
	}
	public String getOrderStatusSM() {
		return orderStatusSM;
	}
	public void setOrderStatusSM(String orderStatusSM) {
		this.orderStatusSM = orderStatusSM;
	}
	public String getOrderStatusSMDate() {
		return orderStatusSMDate;
	}
	public void setOrderStatusSMDate(String orderStatusSMDate) {
		this.orderStatusSMDate = orderStatusSMDate;
	}
	public String getOrderStatusGK() {
		return orderStatusGK;
	}
	public void setOrderStatusGK(String orderStatusGK) {
		this.orderStatusGK = orderStatusGK;
	}
	public String getOrderStatusGKDate() {
		return orderStatusGKDate;
	}
	public void setOrderStatusGKDate(String orderStatusGKDate) {
		this.orderStatusGKDate = orderStatusGKDate;
	}
	public String getOrderStatusDB() {
		return orderStatusDB;
	}
	public void setOrderStatusDB(String orderStatusDB) {
		this.orderStatusDB = orderStatusDB;
	}
	public String getOrderStatusDBDate() {
		return orderStatusDBDate;
	}
	public void setOrderStatusDBDate(String orderStatusDBDate) {
		this.orderStatusDBDate = orderStatusDBDate;
	}
	@Override
	public String toString() {
		return "OrderReportList [srno=" + srno + ", orderId=" + orderId + ", amountWithoutTax=" + amountWithoutTax
				+ ", amountWithTax=" + amountWithTax + ", areaName=" + areaName + ", totalQuantity=" + totalQuantity
				+ ", shopName=" + shopName + ", businessNameId=" + businessNameId + ", employeeName=" + employeeName
				+ ", employeeDetailsId=" + employeeDetailsId + ", paymentPeriodDays=" + paymentPeriodDays
				+ ", orderDate=" + orderDate + ", paymentDate=" + paymentDate + ", orderStatusSM=" + orderStatusSM
				+ ", orderStatusSMDate=" + orderStatusSMDate + ", orderStatusGK=" + orderStatusGK
				+ ", orderStatusGKDate=" + orderStatusGKDate + ", orderStatusDB=" + orderStatusDB
				+ ", orderStatusDBDate=" + orderStatusDBDate + "]";
	}
	
}
