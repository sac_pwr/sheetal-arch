package com.sheetalarch.admin.models;

import java.util.Date;

public class CollectionReportPaymentDetails {

	private long srno;
	private String boothId;	
	private String ownerName;
	private String orderId;
	private String mobileNumber;
	private String routeName;
	private String areaName;
	private String clusterName;
	private double totalAmount;
	private double amountPaid;
	private double balanceAmount;
	private Date nextDueDate;
	private Date paidDate;
	private String payMode;
	private String payStatus;
	public CollectionReportPaymentDetails(long srno, String boothId, String ownerName, String orderId,
			String mobileNumber, String routeName, String areaName, String clusterName, double totalAmount,
			double amountPaid, double balanceAmount, Date nextDueDate, Date paidDate, String payMode,
			String payStatus) {
		super();
		this.srno = srno;
		this.boothId = boothId;
		this.ownerName = ownerName;
		this.orderId = orderId;
		this.mobileNumber = mobileNumber;
		this.routeName = routeName;
		this.areaName = areaName;
		this.clusterName = clusterName;
		this.totalAmount = totalAmount;
		this.amountPaid = amountPaid;
		this.balanceAmount = balanceAmount;
		this.nextDueDate = nextDueDate;
		this.paidDate = paidDate;
		this.payMode = payMode;
		this.payStatus = payStatus;
	}
	public long getSrno() {
		return srno;
	}
	public void setSrno(long srno) {
		this.srno = srno;
	}
	public String getBoothId() {
		return boothId;
	}
	public void setBoothId(String boothId) {
		this.boothId = boothId;
	}
	public String getOwnerName() {
		return ownerName;
	}
	public void setOwnerName(String ownerName) {
		this.ownerName = ownerName;
	}
	public String getOrderId() {
		return orderId;
	}
	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}
	public String getMobileNumber() {
		return mobileNumber;
	}
	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}
	public String getRouteName() {
		return routeName;
	}
	public void setRouteName(String routeName) {
		this.routeName = routeName;
	}
	public String getAreaName() {
		return areaName;
	}
	public void setAreaName(String areaName) {
		this.areaName = areaName;
	}
	public String getClusterName() {
		return clusterName;
	}
	public void setClusterName(String clusterName) {
		this.clusterName = clusterName;
	}
	public double getTotalAmount() {
		return totalAmount;
	}
	public void setTotalAmount(double totalAmount) {
		this.totalAmount = totalAmount;
	}
	public double getAmountPaid() {
		return amountPaid;
	}
	public void setAmountPaid(double amountPaid) {
		this.amountPaid = amountPaid;
	}
	public double getBalanceAmount() {
		return balanceAmount;
	}
	public void setBalanceAmount(double balanceAmount) {
		this.balanceAmount = balanceAmount;
	}
	public Date getNextDueDate() {
		return nextDueDate;
	}
	public void setNextDueDate(Date nextDueDate) {
		this.nextDueDate = nextDueDate;
	}
	public Date getPaidDate() {
		return paidDate;
	}
	public void setPaidDate(Date paidDate) {
		this.paidDate = paidDate;
	}
	public String getPayMode() {
		return payMode;
	}
	public void setPayMode(String payMode) {
		this.payMode = payMode;
	}
	public String getPayStatus() {
		return payStatus;
	}
	public void setPayStatus(String payStatus) {
		this.payStatus = payStatus;
	}
	@Override
	public String toString() {
		return "CollectionReportPaymentDetails [srno=" + srno + ", boothId=" + boothId + ", ownerName=" + ownerName
				+ ", orderId=" + orderId + ", mobileNumber=" + mobileNumber + ", routeName=" + routeName + ", areaName="
				+ areaName + ", clusterName=" + clusterName + ", totalAmount=" + totalAmount + ", amountPaid="
				+ amountPaid + ", balanceAmount=" + balanceAmount + ", nextDueDate=" + nextDueDate + ", paidDate="
				+ paidDate + ", payMode=" + payMode + ", payStatus=" + payStatus + "]";
	}


	
}
