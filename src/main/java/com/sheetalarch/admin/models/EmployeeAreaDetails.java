package com.sheetalarch.admin.models;

import java.util.List;

import com.sheetalarch.entity.EmployeeRouteList;
import com.sheetalarch.entity.Route;

public class EmployeeAreaDetails {

	private String name;
	private String departmentName;
	private String mobileNumber;
	private List<Route> routeList;
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDepartmentName() {
		return departmentName;
	}
	public void setDepartmentName(String departmentName) {
		this.departmentName = departmentName;
	}
	public String getMobileNumber() {
		return mobileNumber;
	}
	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}
	public List<Route> getRouteList() {
		return routeList;
	}
	public void setRouteList(List<Route> routeList) {
		this.routeList = routeList;
	}
	@Override
	public String toString() {
		return "EmployeeAreaDetails [name=" + name + ", departmentName=" + departmentName + ", mobileNumber="
				+ mobileNumber + ", routeList=" + routeList + "]";
	}

	

}
