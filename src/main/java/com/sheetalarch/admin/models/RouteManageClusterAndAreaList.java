/**
 * 
 */
package com.sheetalarch.admin.models;

import java.util.List;

import com.sheetalarch.entity.Area;
import com.sheetalarch.entity.Cluster;

/**
 * @author aNKIT
 *
 */
public class RouteManageClusterAndAreaList {

	private List<Area> areaList;
	private List<Cluster> clusterList;
	public List<Area> getAreaList() {
		return areaList;
	}
	public void setAreaList(List<Area> areaList) {
		this.areaList = areaList;
	}
	public List<Cluster> getClusterList() {
		return clusterList;
	}
	public void setClusterList(List<Cluster> clusterList) {
		this.clusterList = clusterList;
	}
	public RouteManageClusterAndAreaList(List<Area> areaList, List<Cluster> clusterList) {
		super();
		this.areaList = areaList;
		this.clusterList = clusterList;
	}
	@Override
	public String toString() {
		return "RouteManageClusterAndAreaList [areaList=" + areaList + ", clusterList=" + clusterList + "]";
	}
	
	
}
