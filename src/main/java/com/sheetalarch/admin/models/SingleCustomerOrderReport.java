/**
 * 
 */
package com.sheetalarch.admin.models;

/**
 * @author aNKIT
 *
 */
public class SingleCustomerOrderReport {

	private long srno;
	private String orderId;
	private String orderStatus;
	private String orderDate;
	private String deliveryDate;
	private String cancelDate;
	private double orderAmount;
	private double orderAmountPaid;
	private double orderAmountUnPaid;
	private String payMode;
	private String lastPaymentDate;
	public SingleCustomerOrderReport(long srno, String orderId, String orderStatus, String orderDate,
			String deliveryDate, String cancelDate, double orderAmount, double orderAmountPaid,
			double orderAmountUnPaid, String payMode, String lastPaymentDate) {
		super();
		this.srno = srno;
		this.orderId = orderId;
		this.orderStatus = orderStatus;
		this.orderDate = orderDate;
		this.deliveryDate = deliveryDate;
		this.cancelDate = cancelDate;
		this.orderAmount = orderAmount;
		this.orderAmountPaid = orderAmountPaid;
		this.orderAmountUnPaid = orderAmountUnPaid;
		this.payMode = payMode;
		this.lastPaymentDate = lastPaymentDate;
	}
	public long getSrno() {
		return srno;
	}
	public void setSrno(long srno) {
		this.srno = srno;
	}
	public String getOrderId() {
		return orderId;
	}
	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}
	public String getOrderStatus() {
		return orderStatus;
	}
	public void setOrderStatus(String orderStatus) {
		this.orderStatus = orderStatus;
	}
	public String getOrderDate() {
		return orderDate;
	}
	public void setOrderDate(String orderDate) {
		this.orderDate = orderDate;
	}
	public String getDeliveryDate() {
		return deliveryDate;
	}
	public void setDeliveryDate(String deliveryDate) {
		this.deliveryDate = deliveryDate;
	}
	public String getCancelDate() {
		return cancelDate;
	}
	public void setCancelDate(String cancelDate) {
		this.cancelDate = cancelDate;
	}
	public double getOrderAmount() {
		return orderAmount;
	}
	public void setOrderAmount(double orderAmount) {
		this.orderAmount = orderAmount;
	}
	public double getOrderAmountPaid() {
		return orderAmountPaid;
	}
	public void setOrderAmountPaid(double orderAmountPaid) {
		this.orderAmountPaid = orderAmountPaid;
	}
	public double getOrderAmountUnPaid() {
		return orderAmountUnPaid;
	}
	public void setOrderAmountUnPaid(double orderAmountUnPaid) {
		this.orderAmountUnPaid = orderAmountUnPaid;
	}
	public String getPayMode() {
		return payMode;
	}
	public void setPayMode(String payMode) {
		this.payMode = payMode;
	}
	public String getLastPaymentDate() {
		return lastPaymentDate;
	}
	public void setLastPaymentDate(String lastPaymentDate) {
		this.lastPaymentDate = lastPaymentDate;
	}
	@Override
	public String toString() {
		return "SingleCustomerOrderReport [srno=" + srno + ", orderId=" + orderId + ", orderStatus=" + orderStatus
				+ ", orderDate=" + orderDate + ", deliveryDate=" + deliveryDate + ", cancelDate=" + cancelDate
				+ ", orderAmount=" + orderAmount + ", orderAmountPaid=" + orderAmountPaid + ", orderAmountUnPaid="
				+ orderAmountUnPaid + ", payMode=" + payMode + ", lastPaymentDate=" + lastPaymentDate + "]";
	}
	
	
	
	
	
}
