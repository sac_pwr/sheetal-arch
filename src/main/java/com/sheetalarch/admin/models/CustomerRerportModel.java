/**
 * 
 */
package com.sheetalarch.admin.models;

import com.sheetalarch.entity.Booth;

/**
 * @author aNKIT
 *
 */
public class CustomerRerportModel {

	private long srno;
	private Booth booth;
	private long noOfOrders;
	private double totalAmount;
	private double totalAmountPaid;
	private double totalAmountUnPaid;
	public CustomerRerportModel(long srno, Booth booth, long noOfOrders, double totalAmount, double totalAmountPaid,
			double totalAmountUnPaid) {
		super();
		this.srno = srno;
		this.booth = booth;
		this.noOfOrders = noOfOrders;
		this.totalAmount = totalAmount;
		this.totalAmountPaid = totalAmountPaid;
		this.totalAmountUnPaid = totalAmountUnPaid;
	}
	public long getSrno() {
		return srno;
	}
	public void setSrno(long srno) {
		this.srno = srno;
	}
	public Booth getBooth() {
		return booth;
	}
	public void setBooth(Booth booth) {
		this.booth = booth;
	}
	public long getNoOfOrders() {
		return noOfOrders;
	}
	public void setNoOfOrders(long noOfOrders) {
		this.noOfOrders = noOfOrders;
	}
	public double getTotalAmount() {
		return totalAmount;
	}
	public void setTotalAmount(double totalAmount) {
		this.totalAmount = totalAmount;
	}
	public double getTotalAmountPaid() {
		return totalAmountPaid;
	}
	public void setTotalAmountPaid(double totalAmountPaid) {
		this.totalAmountPaid = totalAmountPaid;
	}
	public double getTotalAmountUnPaid() {
		return totalAmountUnPaid;
	}
	public void setTotalAmountUnPaid(double totalAmountUnPaid) {
		this.totalAmountUnPaid = totalAmountUnPaid;
	}
	@Override
	public String toString() {
		return "CustomerRerportModel [srno=" + srno + ", booth=" + booth + ", noOfOrders=" + noOfOrders
				+ ", totalAmount=" + totalAmount + ", totalAmountPaid=" + totalAmountPaid + ", totalAmountUnPaid="
				+ totalAmountUnPaid + "]";
	}
	
	
	
	
}
