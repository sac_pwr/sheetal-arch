/**
 * 
 */
package com.sheetalarch.admin.models;

import java.util.List;

import com.sheetalarch.entity.Cluster;

/**
 * @author aNKIT
 *
 */
public class SMReportModel {

	private double totalAmount;
	private int noOfSMandASM;
	private List<Cluster> clusterList;
	private List<SMReportDetails> smReportDetailsList;
	private String type;
	public SMReportModel(double totalAmount, int noOfSMandASM, List<Cluster> clusterList,
			List<SMReportDetails> smReportDetailsList, String type) {
		super();
		this.totalAmount = totalAmount;
		this.noOfSMandASM = noOfSMandASM;
		this.clusterList = clusterList;
		this.smReportDetailsList = smReportDetailsList;
		this.type = type;
	}
	public double getTotalAmount() {
		return totalAmount;
	}
	public void setTotalAmount(double totalAmount) {
		this.totalAmount = totalAmount;
	}
	public int getNoOfSMandASM() {
		return noOfSMandASM;
	}
	public void setNoOfSMandASM(int noOfSMandASM) {
		this.noOfSMandASM = noOfSMandASM;
	}
	public List<Cluster> getClusterList() {
		return clusterList;
	}
	public void setClusterList(List<Cluster> clusterList) {
		this.clusterList = clusterList;
	}
	public List<SMReportDetails> getSmReportDetailsList() {
		return smReportDetailsList;
	}
	public void setSmReportDetailsList(List<SMReportDetails> smReportDetailsList) {
		this.smReportDetailsList = smReportDetailsList;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	@Override
	public String toString() {
		return "SMReportModel [totalAmount=" + totalAmount + ", noOfSMandASM=" + noOfSMandASM + ", clusterList="
				+ clusterList + ", type=" + type + "]";
	}
	
	
	
}
