/**
 * 
 */
package com.sheetalarch.admin.models;

/**
 * @author aNKIT
 *
 */
public class SalesManReportSub {

	private long srno;
	private long employeeSMId;
	private String employeeSMName;
	private String employeeDetailsSMId;
	private double amountOfBusinessDid;
	private long orderCount;
	public SalesManReportSub(long srno, long employeeSMId, String employeeSMName, String employeeDetailsSMId,
			double amountOfBusinessDid, long orderCount) {
		super();
		this.srno = srno;
		this.employeeSMId = employeeSMId;
		this.employeeSMName = employeeSMName;
		this.employeeDetailsSMId = employeeDetailsSMId;
		this.amountOfBusinessDid = amountOfBusinessDid;
		this.orderCount = orderCount;
	}
	public long getSrno() {
		return srno;
	}
	public void setSrno(long srno) {
		this.srno = srno;
	}
	public long getEmployeeSMId() {
		return employeeSMId;
	}
	public void setEmployeeSMId(long employeeSMId) {
		this.employeeSMId = employeeSMId;
	}
	public String getEmployeeSMName() {
		return employeeSMName;
	}
	public void setEmployeeSMName(String employeeSMName) {
		this.employeeSMName = employeeSMName;
	}
	public String getEmployeeDetailsSMId() {
		return employeeDetailsSMId;
	}
	public void setEmployeeDetailsSMId(String employeeDetailsSMId) {
		this.employeeDetailsSMId = employeeDetailsSMId;
	}
	public double getAmountOfBusinessDid() {
		return amountOfBusinessDid;
	}
	public void setAmountOfBusinessDid(double amountOfBusinessDid) {
		this.amountOfBusinessDid = amountOfBusinessDid;
	}
	public long getOrderCount() {
		return orderCount;
	}
	public void setOrderCount(long orderCount) {
		this.orderCount = orderCount;
	}
	@Override
	public String toString() {
		return "SalesManReportSub [srno=" + srno + ", employeeSMId=" + employeeSMId + ", employeeSMName="
				+ employeeSMName + ", employeeDetailsSMId=" + employeeDetailsSMId + ", amountOfBusinessDid="
				+ amountOfBusinessDid + ", orderCount=" + orderCount + "]";
	}
	
	
	
}
