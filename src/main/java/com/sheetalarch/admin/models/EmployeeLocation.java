/**
 * 
 */
package com.sheetalarch.admin.models;

import java.util.List;

import com.sheetalarch.entity.EmployeeDetails;
import com.sheetalarch.entity.Route;


/**
 * @author aNKIT
 *
 */
public class EmployeeLocation {

	private EmployeeDetails employeeDetails;
	private List<Route> routeList;
	public EmployeeDetails getEmployeeDetails() {
		return employeeDetails;
	}
	public void setEmployeeDetails(EmployeeDetails employeeDetails) {
		this.employeeDetails = employeeDetails;
	}
	public List<Route> getRouteList() {
		return routeList;
	}
	public void setRouteList(List<Route> routeList) {
		this.routeList = routeList;
	}
	@Override
	public String toString() {
		return "EmployeeLocation [employeeDetails=" + employeeDetails + ", routeList=" + routeList + "]";
	}
	
	

	
}
