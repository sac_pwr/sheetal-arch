package com.sheetalarch.admin.models;

public class PaymentDoInfo {

	private long pkId;
	private String id;
	private String name;
	private String inventoryId;
	private double amountPaid;
	private double amountUnPaid;
	private double totalAmount;
	private String type;
	private String url;
	
	
	public PaymentDoInfo() {
		// TODO Auto-generated constructor stub
	}


	public PaymentDoInfo(long pkId, String id, String name, String inventoryId, double amountPaid, double amountUnPaid,
			double totalAmount, String type, String url) {
		super();
		this.pkId = pkId;
		this.id = id;
		this.name = name;
		this.inventoryId = inventoryId;
		this.amountPaid = amountPaid;
		this.amountUnPaid = amountUnPaid;
		this.totalAmount = totalAmount;
		this.type = type;
		this.url = url;
	}


	public long getPkId() {
		return pkId;
	}


	public void setPkId(long pkId) {
		this.pkId = pkId;
	}


	public String getId() {
		return id;
	}


	public void setId(String id) {
		this.id = id;
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getInventoryId() {
		return inventoryId;
	}


	public void setInventoryId(String inventoryId) {
		this.inventoryId = inventoryId;
	}


	public double getAmountPaid() {
		return amountPaid;
	}


	public void setAmountPaid(double amountPaid) {
		this.amountPaid = amountPaid;
	}


	public double getAmountUnPaid() {
		return amountUnPaid;
	}


	public void setAmountUnPaid(double amountUnPaid) {
		this.amountUnPaid = amountUnPaid;
	}


	public double getTotalAmount() {
		return totalAmount;
	}


	public void setTotalAmount(double totalAmount) {
		this.totalAmount = totalAmount;
	}


	public String getType() {
		return type;
	}


	public void setType(String type) {
		this.type = type;
	}


	public String getUrl() {
		return url;
	}


	public void setUrl(String url) {
		this.url = url;
	}


	@Override
	public String toString() {
		return "PaymentDoInfo [pkId=" + pkId + ", id=" + id + ", name=" + name + ", inventoryId=" + inventoryId
				+ ", amountPaid=" + amountPaid + ", amountUnPaid=" + amountUnPaid + ", totalAmount=" + totalAmount
				+ ", type=" + type + ", url=" + url + "]";
	}
	
	
	
	
	
	
	
}
