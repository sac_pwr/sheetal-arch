/**
 * 
 */
package com.sheetalarch.admin.models;

import com.sheetalarch.entity.EmployeeDetails;
import com.sheetalarch.entity.OrderDetails;

/**
 * @author aNKIT
 *
 */
public class SalesManReportModel {

	private long srno;
	private String orderId;
	private long boothId;
	private String boothNo;
	private String ownerName;
	private String routeNo;
	private String smName;
	private long employeeDetailsId;
	private double orderAmount;
	private String dateOfOrder;
	private String dateOfDeliver;
	public SalesManReportModel(long srno, String orderId, long boothId, String boothNo, String ownerName,
			String routeNo, String smName, long employeeDetailsId, double orderAmount, String dateOfOrder,
			String dateOfDeliver) {
		super();
		this.srno = srno;
		this.orderId = orderId;
		this.boothId = boothId;
		this.boothNo = boothNo;
		this.ownerName = ownerName;
		this.routeNo = routeNo;
		this.smName = smName;
		this.employeeDetailsId = employeeDetailsId;
		this.orderAmount = orderAmount;
		this.dateOfOrder = dateOfOrder;
		this.dateOfDeliver = dateOfDeliver;
	}
	public long getSrno() {
		return srno;
	}
	public void setSrno(long srno) {
		this.srno = srno;
	}
	public String getOrderId() {
		return orderId;
	}
	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}
	public long getBoothId() {
		return boothId;
	}
	public void setBoothId(long boothId) {
		this.boothId = boothId;
	}
	public String getBoothNo() {
		return boothNo;
	}
	public void setBoothNo(String boothNo) {
		this.boothNo = boothNo;
	}
	public String getOwnerName() {
		return ownerName;
	}
	public void setOwnerName(String ownerName) {
		this.ownerName = ownerName;
	}
	public String getRouteNo() {
		return routeNo;
	}
	public void setRouteNo(String routeNo) {
		this.routeNo = routeNo;
	}
	public String getSmName() {
		return smName;
	}
	public void setSmName(String smName) {
		this.smName = smName;
	}
	public long getEmployeeDetailsId() {
		return employeeDetailsId;
	}
	public void setEmployeeDetailsId(long employeeDetailsId) {
		this.employeeDetailsId = employeeDetailsId;
	}
	public double getOrderAmount() {
		return orderAmount;
	}
	public void setOrderAmount(double orderAmount) {
		this.orderAmount = orderAmount;
	}
	public String getDateOfOrder() {
		return dateOfOrder;
	}
	public void setDateOfOrder(String dateOfOrder) {
		this.dateOfOrder = dateOfOrder;
	}
	public String getDateOfDeliver() {
		return dateOfDeliver;
	}
	public void setDateOfDeliver(String dateOfDeliver) {
		this.dateOfDeliver = dateOfDeliver;
	}
	@Override
	public String toString() {
		return "SalesManReportModel [srno=" + srno + ", orderId=" + orderId + ", boothId=" + boothId + ", boothNo="
				+ boothNo + ", ownerName=" + ownerName + ", routeNo=" + routeNo + ", smName=" + smName
				+ ", employeeDetailsId=" + employeeDetailsId + ", orderAmount=" + orderAmount + ", dateOfOrder="
				+ dateOfOrder + ", dateOfDeliver=" + dateOfDeliver + "]";
	}
}
