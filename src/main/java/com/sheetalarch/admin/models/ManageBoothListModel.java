/**
 * 
 */
package com.sheetalarch.admin.models;

import com.sheetalarch.entity.Booth;

/**
 * @author aNKIT
 *
 */
public class ManageBoothListModel {

	private Booth booth;
	private String balanceAmount;
	
	
	public ManageBoothListModel(Booth booth, String balanceAmount) {
		super();
		this.booth = booth;
		this.balanceAmount = balanceAmount;
	}
	public Booth getBooth() {
		return booth;
	}
	public void setBooth(Booth booth) {
		this.booth = booth;
	}
	public String getBalanceAmount() {
		return balanceAmount;
	}
	public void setBalanceAmount(String balanceAmount) {
		this.balanceAmount = balanceAmount;
	}
	@Override
	public String toString() {
		return "ManageBoothListModel [booth=" + booth + ", balanceAmount=" + balanceAmount + "]";
	}
	
}
