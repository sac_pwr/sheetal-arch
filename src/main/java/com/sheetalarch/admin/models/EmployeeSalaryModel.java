package com.sheetalarch.admin.models;

import java.util.Date;

public class EmployeeSalaryModel {

	private int srno;
	private double amount;
	private Date date;
	private String modeOfPayment;
	private String comment;
	public EmployeeSalaryModel(int srno, double amount, Date date, String modeOfPayment, String comment) {
		super();
		this.srno = srno;
		this.amount = amount;
		this.date = date;
		this.modeOfPayment = modeOfPayment;
		this.comment = comment;
	}
	public int getSrno() {
		return srno;
	}
	public void setSrno(int srno) {
		this.srno = srno;
	}
	public double getAmount() {
		return amount;
	}
	public void setAmount(double amount) {
		this.amount = amount;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	public String getModeOfPayment() {
		return modeOfPayment;
	}
	public void setModeOfPayment(String modeOfPayment) {
		this.modeOfPayment = modeOfPayment;
	}
	public String getComment() {
		return comment;
	}
	public void setComment(String comment) {
		this.comment = comment;
	}
	@Override
	public String toString() {
		return "EmployeeSalaryModel [srno=" + srno + ", amount=" + amount + ", date=" + date + ", modeOfPayment="
				+ modeOfPayment + ", comment=" + comment + "]";
	}
	
	
	
	
}
