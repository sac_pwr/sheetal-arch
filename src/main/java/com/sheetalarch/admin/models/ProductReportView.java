package com.sheetalarch.admin.models;

import com.sheetalarch.entity.Product;

public class ProductReportView {
	private long srno;
	private Product product;
	private long totalIssuedQuantity;
	private double totalAmountWithTax;
	public ProductReportView(long srno, Product product, long totalIssuedQuantity, double totalAmountWithTax) {
		super();
		this.srno = srno;
		this.product = product;
		this.totalIssuedQuantity = totalIssuedQuantity;
		this.totalAmountWithTax = totalAmountWithTax;
	}
	public long getSrno() {
		return srno;
	}
	public void setSrno(long srno) {
		this.srno = srno;
	}
	public Product getProduct() {
		return product;
	}
	public void setProduct(Product product) {
		this.product = product;
	}
	public long getTotalIssuedQuantity() {
		return totalIssuedQuantity;
	}
	public void setTotalIssuedQuantity(long totalIssuedQuantity) {
		this.totalIssuedQuantity = totalIssuedQuantity;
	}
	public double getTotalAmountWithTax() {
		return totalAmountWithTax;
	}
	public void setTotalAmountWithTax(double totalAmountWithTax) {
		this.totalAmountWithTax = totalAmountWithTax;
	}
	@Override
	public String toString() {
		return "ProductReportView [srno=" + srno + ", product=" + product + ", totalIssuedQuantity="
				+ totalIssuedQuantity + ", totalAmountWithTax=" + totalAmountWithTax + "]";
	}

}
