package com.sheetalarch.rcontroller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.sheetalarch.entity.Product;
import com.sheetalarch.models.BrandAndCategoryRequest;
import com.sheetalarch.models.ProductListResponse;
import com.sheetalarch.service.ProductService;
import com.sheetalarch.utils.Constants;

@RestController
public class ProductController {

	@Autowired
	Product product;

	@Autowired
	ProductService productService;

	@PostMapping("/fetchProductListByBrandIdAndCategoryId")
	public ResponseEntity<ProductListResponse> responseEntity(@RequestBody BrandAndCategoryRequest brandAndCategoryRequest) {

		ProductListResponse productListResponse = new ProductListResponse();
		HttpStatus httpStatus;
		System.out.println("Inside fetchProductListByBrandIdAndCategoryId url");

		try {
			List<Product> productList = productService.fetchProductByBrandAndCategory(brandAndCategoryRequest);
			if (productList == null) {
				productListResponse.setStatus(Constants.FAILURE_RESPONSE);
				productListResponse.setErrorMsg("No Product found");
				httpStatus = HttpStatus.OK;
			} else {

				productList = productService.makeProductImageMNullorderProductDetailsList(productList);

				httpStatus = HttpStatus.OK;
				productListResponse.setStatus(Constants.SUCCESS_RESPONSE);
				productListResponse.setProductsList(productList);
			}
		} catch (Exception e) {
			System.out.println("fetchProductListByBrandIdAndCategoryId :: " + e.toString());
			httpStatus = HttpStatus.FORBIDDEN;
			productListResponse.setStatus(Constants.FAILURE_RESPONSE);
		}

		return new ResponseEntity<ProductListResponse>(productListResponse, httpStatus);
	}

	/*
	 

	/*
	 * // testing & Checking
	 * 
	 * @GetMapping("/fetchSupplierProductListBySupplierId/{supplierId}")
	 * 
	 * public ResponseEntity<ProductListResponse>
	 * fetchSupplierProductListBySupplierIdAndCategoryIdAndBrandIdForApp(@
	 * ModelAttribute("supplierId") String supplierId){
	 * 
	 * ProductListResponse productListResponse=new ProductListResponse();
	 * HttpStatus httpStatus; System.out.println(
	 * "fetchProductBySupplierIdAndCategoryIdAndBrandIdforApp"); try{
	 * List<SupplierProductList>
	 * supplierProductList=supplierService.fetchProductBySupplierId(supplierId);
	 * 
	 * 
	 * if (supplierProductList == null) {
	 * productListResponse.setStatus(Constants.FAILURE_RESPONSE);
	 * 
	 * httpStatus = HttpStatus.NO_CONTENT;
	 * productListResponse.setErrorMsg("No Product is available");
	 * 
	 * }else{ supplierProductList=productService.
	 * makeSupplierProductListImageMNullorderProductDetailsList(
	 * supplierProductList);
	 * 
	 * productListResponse.setStatus(Constants.SUCCESS_RESPONSE); httpStatus =
	 * HttpStatus.OK;
	 * 
	 * productListResponse.setSupplierProductList(supplierProductList); } }
	 * catch (Exception e) { // TODO Auto-generated catch block
	 * e.printStackTrace();
	 * System.out.println("fetchProductBySupplierIdforApp Error:"+
	 * e.toString()); productListResponse.setStatus(Constants.FAILURE_RESPONSE);
	 * httpStatus = HttpStatus.FORBIDDEN;
	 * 
	 * } return new
	 * ResponseEntity<ProductListResponse>(productListResponse,httpStatus); }
	 */

}
