package com.sheetalarch.rcontroller;

import java.util.List;




import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;


import com.sheetalarch.entity.Brand;
import com.sheetalarch.entity.Categories;
import com.sheetalarch.entity.Inventory;
import com.sheetalarch.entity.InventoryDetails;
import com.sheetalarch.entity.Product;
import com.sheetalarch.entity.Supplier;
import com.sheetalarch.entity.SupplierProductList;
import com.sheetalarch.models.BaseDomain;
import com.sheetalarch.models.FetchProductBySupplierResponse;
import com.sheetalarch.models.InventoryProductDetailsReportResponse;
import com.sheetalarch.models.InventoryProductListRequest;
import com.sheetalarch.models.InventoryReportRequest;
import com.sheetalarch.models.InventorySaveRequestModel;
import com.sheetalarch.models.InventotryReportResponse;
import com.sheetalarch.models.NewOrderBoothResponse;
import com.sheetalarch.models.OpenInventoryResponseModel;
import com.sheetalarch.models.ProductAddInventory;
import com.sheetalarch.models.ProductListResponse;
import com.sheetalarch.service.InventoryService;
import com.sheetalarch.service.ProductService;
import com.sheetalarch.service.SupplierService;
import com.sheetalarch.utils.Constants;



@RestController
public class InventoryController {

	@Autowired
	ProductService productService;
	
	@Autowired
	SupplierService supplierService;
	
	@Autowired
	InventoryService inventoryService;
	//  here we are fetching BrandList AND Category list for 1st screen of manage inventory
	@PostMapping("/fetchBrandListAndCategoryListForManageInventory")
	public ResponseEntity<NewOrderBoothResponse> fetchBrandListAndCategoryListForManageInventory(){
		
		NewOrderBoothResponse newOrderBoothResponse=new NewOrderBoothResponse();
		HttpStatus httpStatus;
		System.out.println("fetchBrandListAndCategoryListForManageInventory");
		
		try {
			List<Brand> brandList=productService.fetchBrandList();
			List<Categories> categoriesList=productService.fetchCategories();
			
			newOrderBoothResponse.setStatus(Constants.SUCCESS_RESPONSE);
			newOrderBoothResponse.setBrandList(brandList);
			newOrderBoothResponse.setCategoriesList(categoriesList);
			httpStatus=HttpStatus.OK;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.out.println("fetchBrandListAndCategoryListForManageInventory Error:"+e.toString());
			newOrderBoothResponse.setStatus(Constants.FAILURE_RESPONSE);
			newOrderBoothResponse.setErrorMsg("Something went wrong");
			httpStatus=HttpStatus.FORBIDDEN;
			
		}
		
		
		return new ResponseEntity<NewOrderBoothResponse>(newOrderBoothResponse,httpStatus);
		
		
	}
	
	
	
	

	// fetching SupplierList BrandList And CategoryList while adding inventory
@PostMapping("/addMultipleInventoryForApp")	
public ResponseEntity<OpenInventoryResponseModel> openAddMultipleInventoryforApp(){
	
	OpenInventoryResponseModel openInventoryResponseModel= new OpenInventoryResponseModel();
	HttpStatus httpStatus;
	
	try {
		List<Supplier> supplierList=supplierService.fetchSupplierList();
		openInventoryResponseModel.setSupplier(supplierList);
		
		List<Brand> brandList=productService.fetchBrandList();
		openInventoryResponseModel.setBrand(brandList);
		
		List<Categories> categoriesList=productService.fetchCategories();
		openInventoryResponseModel.setCategory(categoriesList);
		
		openInventoryResponseModel.setStatus(Constants.SUCCESS_RESPONSE);
		httpStatus = HttpStatus.OK;
	} catch (Exception e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
		System.out.println("addMultipleInventoryForApp Error:"+e.toString());
		httpStatus=HttpStatus.FORBIDDEN;
		
	}

	return new ResponseEntity<OpenInventoryResponseModel>(openInventoryResponseModel,httpStatus);
	
}

////fetching ProductList By SupplierId ,CategoryId And BrandId for AddInventory Screen
	@PostMapping("/fetchSupplierProductListBySupplierIdAndCategoryIdAndBrandIdForApp")
	
public ResponseEntity<ProductListResponse> fetchSupplierProductListBySupplierIdAndCategoryIdAndBrandIdForApp(@RequestBody InventoryProductListRequest inventoryProductListRequest){
	
	ProductListResponse productListResponse=new ProductListResponse();
	HttpStatus httpStatus;
	System.out.println("fetchProductBySupplierIdAndCategoryIdAndBrandIdforApp");
	try{
	List<SupplierProductList> supplierProductList=productService.fetchProductListBySupplierIDAndBrandIdAndCategoryIdForApp(inventoryProductListRequest.getSupplierId(), inventoryProductListRequest.getCategoryId(), inventoryProductListRequest.getBrandId());
	

	if (supplierProductList == null) {
		productListResponse.setStatus(Constants.FAILURE_RESPONSE);
		
		httpStatus = HttpStatus.OK;
		productListResponse.setErrorMsg("No Product is available");
		
	}else{
		supplierProductList=productService.makeSupplierProductListImageMNullorderProductDetailsList(supplierProductList);
		
		productListResponse.setStatus(Constants.SUCCESS_RESPONSE);
		httpStatus = HttpStatus.OK;
		
		productListResponse.setSupplierProductList(supplierProductList);
	}
}
	catch (Exception e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
		System.out.println("fetchProductBySupplierIdforApp Error:"+ e.toString());
		productListResponse.setStatus(Constants.FAILURE_RESPONSE);
		httpStatus = HttpStatus.FORBIDDEN;
		
	}
	return new ResponseEntity<ProductListResponse>(productListResponse,httpStatus);
}

@PostMapping("/fetchProductBySupplierIdAndCategoryIdAndBrandIdForApp")

public ResponseEntity<FetchProductBySupplierResponse> fetchProductBySupplierIdAndCategoryIdAndBrandIdforApp(@RequestBody InventoryProductListRequest inventoryProductListRequest){
	
	FetchProductBySupplierResponse fetchProductBySupplierResponse=new FetchProductBySupplierResponse();
	HttpStatus httpStatus;
	System.out.println("fetchProductBySupplierIdAndCategoryIdAndBrandIdforApp");
	try{
	List<ProductAddInventory> productList=inventoryService.fetchProductListByBrandIdAndCategoryIdForApp(inventoryProductListRequest.getSupplierId(), inventoryProductListRequest.getCategoryId(), inventoryProductListRequest.getBrandId());
	

	if (productList == null) {
		fetchProductBySupplierResponse.setStatus(Constants.FAILURE_RESPONSE);
		fetchProductBySupplierResponse.setStatus(Constants.FAILURE_RESPONSE);
		httpStatus = HttpStatus.NO_CONTENT;
		fetchProductBySupplierResponse.setErrorMsg("No Product is available");
		
	}else{
		fetchProductBySupplierResponse.setStatus(Constants.SUCCESS_RESPONSE);
		httpStatus = HttpStatus.OK;
		
		fetchProductBySupplierResponse.setProductAddInventories(productList);
	}
}
	catch (Exception e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
		System.out.println("fetchProductBySupplierIdforApp Error:"+ e.toString());
		fetchProductBySupplierResponse.setStatus(Constants.FAILURE_RESPONSE);
		httpStatus = HttpStatus.FORBIDDEN;
		
	}
	return new ResponseEntity<FetchProductBySupplierResponse>(fetchProductBySupplierResponse,httpStatus);
}

@PostMapping("/saveAddedInventoryForApp")
public ResponseEntity<BaseDomain> saveAddedInventoryForApp (@RequestBody InventorySaveRequestModel inventorySaveRequestModel){
	
	BaseDomain baseDomain=new BaseDomain();
	HttpStatus httpstatus;
	System.out.println("saveAddedInventoryForApp");
	
	try {
		inventoryService.saveInventoryForApp(inventorySaveRequestModel);
		baseDomain.setStatus(Constants.SUCCESS_RESPONSE);
		httpstatus=HttpStatus.OK;
	} catch (Exception e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
		System.out.println("saveAddedInventoryForApp Error:"+e.toString());
		baseDomain.setStatus(Constants.FAILURE_RESPONSE);
		httpstatus=HttpStatus.OK;			
	}
	return new ResponseEntity<BaseDomain>(baseDomain, httpstatus);
	
}



// Report
@PostMapping("/fetchAddInventoryForGateKeeperReportByDateRange")
public ResponseEntity<InventotryReportResponse> fetchAddInventoryForGateKeeperReportByEmpIdAndDateRange(@RequestBody InventoryReportRequest inventoryReportRequest ){
	InventotryReportResponse inventotryReportResonse=new InventotryReportResponse();
	HttpStatus httpStatus;
	System.out.println("fetchAddInventoryForGateKeeperReportByDateRange");
	
	try {
		List<Inventory> inventoryList=inventoryService.fetchAddedInventoryforGateKeeperReportByDateRange(inventoryReportRequest.getFromDate(), inventoryReportRequest.getToDate(), inventoryReportRequest.getRange());
		if(inventoryList==null)
		{
			inventotryReportResonse.setStatus(Constants.FAILURE_RESPONSE);
			httpStatus=HttpStatus.OK;
			inventotryReportResonse.setErrorMsg("No Record found");
		}
		else
		{
		
			inventotryReportResonse.setStatus(Constants.SUCCESS_RESPONSE);
			httpStatus=HttpStatus.OK;
			inventotryReportResonse.setInventory(inventoryList);
		}
	} catch (Exception e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
		System.out.println("fetchAddInventoryForGateKeeperReportByEmpIdAndDateRange Error"+e.toString());
		inventotryReportResonse.setStatus(Constants.FAILURE_RESPONSE);
		httpStatus=HttpStatus.FORBIDDEN;
	}
	
			return new ResponseEntity<InventotryReportResponse>(inventotryReportResonse,httpStatus);

}


@GetMapping("/fetchInventoryProductDetailsForGateKeeperReport/{inventoryTransactionId}")
public ResponseEntity<InventoryProductDetailsReportResponse> fetchInventoryProductDetailsforGateKeeperReport(@ModelAttribute ("inventoryTransactionId") String inventoryTransactionId){
	
	InventoryProductDetailsReportResponse inventoryProductDetailsReportResponse=new InventoryProductDetailsReportResponse();
	HttpStatus httpStatus;
	System.out.println("fetchInventoryProductDetailsforGateKeeperReport");
	
	try {
		Inventory inventory=inventoryService.fetchInventoryByInventoryTransactionIdforGateKeeperReport(inventoryTransactionId);
		if(inventory==null){
			inventoryProductDetailsReportResponse.setStatus(Constants.FAILURE_RESPONSE);
			httpStatus=HttpStatus.NO_CONTENT;
			return new ResponseEntity<InventoryProductDetailsReportResponse>(inventoryProductDetailsReportResponse,httpStatus);
		}
		List<InventoryDetails> inventoryDetailsList=inventoryService.fetchInventoryDetailsByInventoryTransactionIdforGateKeeperReport(inventoryTransactionId);
		if(inventoryDetailsList==null){
			inventoryProductDetailsReportResponse.setStatus(Constants.FAILURE_RESPONSE);
			httpStatus=HttpStatus.NO_CONTENT;
			}else{
				inventoryProductDetailsReportResponse.setStatus(Constants.SUCCESS_RESPONSE);
				inventoryProductDetailsReportResponse.setInventoryDetails(inventoryDetailsList);
				inventoryProductDetailsReportResponse.setInventory(inventory);
				httpStatus=HttpStatus.OK;
			}
	} catch (Exception e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
		System.out.println("fetchInventoryProductDetailsforGateKeeperReport Error:" +e.toString());
		inventoryProductDetailsReportResponse.setStatus(Constants.FAILURE_RESPONSE);
		httpStatus=HttpStatus.FORBIDDEN;
	}
	return new ResponseEntity<InventoryProductDetailsReportResponse>(inventoryProductDetailsReportResponse,httpStatus);
}

}
