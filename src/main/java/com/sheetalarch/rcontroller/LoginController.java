package com.sheetalarch.rcontroller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.sheetalarch.entity.Employee;
import com.sheetalarch.entity.EmployeeDetails;
import com.sheetalarch.models.LoginRequest;
import com.sheetalarch.models.LoginResponse;
import com.sheetalarch.service.EmployeeDetailsService;
import com.sheetalarch.service.EmployeeService;
import com.sheetalarch.utils.Constants;

@RestController
public class LoginController {

	@Autowired
	Employee employee;

	@Autowired
	EmployeeService employeeService;

	@Autowired
	EmployeeDetails employeeDetails;

	@Autowired
	EmployeeDetailsService employeeDetailsService;

	@PostMapping("/authentication")
	public ResponseEntity<LoginResponse> authenticateLogin(@RequestBody LoginRequest loginRequest) {
		System.out.println("Inside Login Response");
		HttpStatus httpStatus;
		LoginResponse loginResponse = new LoginResponse();
		
		try {
			this.employee = employeeService.login(loginRequest.getUserId(), loginRequest.getPassword());

			if (this.employee == null) {
				loginResponse.setStatus(Constants.FAILURE_RESPONSE);
				loginResponse.setErrorMsg(Constants.LOGIN_FAILED);
				httpStatus = HttpStatus.OK;
			} else {
				this.employeeDetails=employeeDetailsService.getEmployeeDetailsByemployeeId(this.employee.getEmployeeId());
				this.employeeDetails.setToken(loginRequest.getToken());
				employeeDetailsService.updateEmployeeDetails(employeeDetails);
				
				httpStatus=HttpStatus.OK;
				this.employee.setPassword("");
				loginResponse.setEmployee(employee);
				loginResponse.setStatus(Constants.SUCCESS_RESPONSE);
				loginResponse.setErrorMsg(null);
			}
		} catch (Exception e) {
			loginResponse.setStatus(Constants.FAILURE_RESPONSE);
			loginResponse.setErrorMsg("No Data found");
			httpStatus=HttpStatus.OK;
			System.out.println("Login Error:: " + e.toString());
		}

		return new ResponseEntity<LoginResponse>(loginResponse,httpStatus);

	}

}
