package com.sheetalarch.rcontroller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RestController;

import com.sheetalarch.entity.Cluster;
import com.sheetalarch.entity.Employee;
import com.sheetalarch.entity.EmployeeDetails;
import com.sheetalarch.models.GKOrderListClusterAndASMListResponse;
import com.sheetalarch.models.SMListResponse;
import com.sheetalarch.service.EmployeeDetailsService;
import com.sheetalarch.utils.Constants;

@RestController
public class EmployeeDetailsController {
	
	@Autowired
	EmployeeDetailsService employeeDetailsService;
	
	
	// here we are fetching AreaSaleman list and cluster list for Gk OrderListscreen
	@GetMapping("/fetchClusterAndASMForOrderList")
	public ResponseEntity<GKOrderListClusterAndASMListResponse> fetchClusterAndASMForOrderList(){
		
		GKOrderListClusterAndASMListResponse gKOrderListClusterAndASMListResponse=new GKOrderListClusterAndASMListResponse();
		HttpStatus httpStatus;
		System.out.println(" Inside fetchClusterAndASMForOrderList");
		
		try {
			List<Cluster> clusterList=employeeDetailsService.fetchClusterList();
			List<EmployeeDetails> asmList=employeeDetailsService.fetchASM();
			gKOrderListClusterAndASMListResponse.setStatus(Constants.SUCCESS_RESPONSE);
			gKOrderListClusterAndASMListResponse.setClusterList(clusterList);
			gKOrderListClusterAndASMListResponse.setAsmList(asmList);
			httpStatus=HttpStatus.OK;
		
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			gKOrderListClusterAndASMListResponse.setStatus(Constants.FAILURE_RESPONSE);
			httpStatus=HttpStatus.FORBIDDEN;
		}
		return new ResponseEntity<GKOrderListClusterAndASMListResponse>(gKOrderListClusterAndASMListResponse,httpStatus);
		
	}

	// here we are fetching Salesman list which comes under ASM
	@GetMapping("/fetchSMListByASMId/{employeeId}")
	public ResponseEntity<SMListResponse> fetchSMListByASMId(@ModelAttribute("employeeId") long employeeId){
		
		SMListResponse sMListResponse=new SMListResponse();
		HttpStatus httpStatus;
		System.out.println("Inside fetchSMListByASMId");
		
		try {
			List<Employee> employeeList=employeeDetailsService.fetchSalesmanByASMId(employeeId);
			if(employeeList==null){
			
				sMListResponse.setStatus(Constants.FAILURE_RESPONSE);
				httpStatus=HttpStatus.NO_CONTENT;
			}else{
				sMListResponse.setStatus(Constants.SUCCESS_RESPONSE);
				sMListResponse.setEmployeeList(employeeList);
				httpStatus=HttpStatus.OK;
				
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.out.println("fetchSMListByASMId Error:"+e.toString());
			sMListResponse.setStatus(Constants.FAILURE_RESPONSE);
			httpStatus=HttpStatus.FORBIDDEN;
			
		}
		return new ResponseEntity<SMListResponse>(sMListResponse,httpStatus);
	}
}
