package com.sheetalarch.rcontroller;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RestController;

import com.sheetalarch.entity.Area;
import com.sheetalarch.entity.Cluster;
import com.sheetalarch.entity.Employee;
import com.sheetalarch.models.ClusterResponse;
import com.sheetalarch.models.GkEmployeeByClusterResponse;
import com.sheetalarch.service.AreaService;
import com.sheetalarch.utils.Constants;

@RestController
public class AreaController {
	
	@Autowired
	SessionFactory sessionFactory;
	
	@Autowired
	Area area;
	
	@Autowired
	AreaService areaService;
	
	
	@GetMapping("/fetchClusterByEmployeeId/{employeeId}")
	public ResponseEntity<ClusterResponse> fetchClusterByEmployeeId(@ModelAttribute("employeeId") long employeeId){
		
		ClusterResponse clusterResponse=new ClusterResponse();
		HttpStatus httpStatus;
		System.out.println("Inside fetchClusterByEmployeeId");
		
		try {
			Cluster cluster=areaService.fetchClusterBySalesmanId(employeeId);
			if(cluster==null){
				clusterResponse.setStatus(Constants.FAILURE_RESPONSE);
				httpStatus=HttpStatus.NO_CONTENT;
			}
			else{
				clusterResponse.setStatus(Constants.SUCCESS_RESPONSE);
				httpStatus=HttpStatus.OK;
				clusterResponse.setCluster(cluster);
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.out.println("fetchClusterByEmployeeId Error:"+e.toString());
			clusterResponse.setStatus(Constants.FAILURE_RESPONSE);
			httpStatus=HttpStatus.FORBIDDEN;
			
		}
		return new ResponseEntity<ClusterResponse>(clusterResponse,httpStatus);
		
	}
	
	@GetMapping("/fetchGkEmployeeByClusterId/{clusterId}")
	public ResponseEntity<GkEmployeeByClusterResponse> fetchGkEmployeeByClusterId(@ModelAttribute("clusterId") long clusterId){
		
		GkEmployeeByClusterResponse gkEmployeeByClusterResponse=new GkEmployeeByClusterResponse();
		HttpStatus httpStatus;
		System.out.println("fetchGkEmployeeByClusterId");
		
		try {
			Employee employee=areaService.fetchGKEmployeeByClusterId(clusterId);
			if(employee==null){
				gkEmployeeByClusterResponse.setStatus(Constants.FAILURE_RESPONSE);
				httpStatus=HttpStatus.NO_CONTENT;
				
			}else{
				gkEmployeeByClusterResponse.setStatus(Constants.SUCCESS_RESPONSE);
				httpStatus=HttpStatus.OK;
				gkEmployeeByClusterResponse.setGkEmployee(employee);
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.out.println(" fetchGkEmployeeByClusterId Error:"+e.toString());
			gkEmployeeByClusterResponse.setStatus(Constants.SUCCESS_RESPONSE);
			httpStatus=HttpStatus.FORBIDDEN;
		}
		return new ResponseEntity<GkEmployeeByClusterResponse>(gkEmployeeByClusterResponse,httpStatus);
	}

}
