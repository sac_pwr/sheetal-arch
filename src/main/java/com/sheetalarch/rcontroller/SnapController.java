package com.sheetalarch.rcontroller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RestController;

import com.sheetalarch.models.FetchSnapShotForSalesman;
import com.sheetalarch.models.GkSnapProductListMainResponse;
import com.sheetalarch.models.GkSnapProductResponse;
import com.sheetalarch.models.SnapSalesmanResponse;
import com.sheetalarch.service.SnapService;
import com.sheetalarch.utils.Constants;

@RestController
public class SnapController {

	@Autowired
	SnapService snapService;
	
	@GetMapping("/fetchSnapShotForSalesman/{employeeId}")
	public ResponseEntity<FetchSnapShotForSalesman> snapShotForSalesman(@ModelAttribute("employeeId") long employeeId){
		
		FetchSnapShotForSalesman fetchSnapShotForSalesman=new FetchSnapShotForSalesman();
		HttpStatus httpStatus;
		
		try{
			SnapSalesmanResponse snapSalesmanResponse=snapService.fetchSnapShotDetailsForSalemanByEmployeeId(employeeId);
			if(snapSalesmanResponse==null){
				fetchSnapShotForSalesman.setStatus(Constants.FAILURE_RESPONSE);
				httpStatus=HttpStatus.OK;
			}else{
				fetchSnapShotForSalesman.setStatus(Constants.SUCCESS_RESPONSE);
				httpStatus=HttpStatus.OK;
				fetchSnapShotForSalesman.setSnapSalesmanResponse(snapSalesmanResponse);
			}
		}catch (Exception e) {
			// TODO: handle exception
			System.out.println("Error"+e.toString());
			httpStatus=HttpStatus.FORBIDDEN;
			fetchSnapShotForSalesman.setStatus(Constants.FAILURE_RESPONSE);
		}
		
		return new ResponseEntity<FetchSnapShotForSalesman>(fetchSnapShotForSalesman,httpStatus);
	}
	
	@GetMapping("/fetchSnapShotForGK/{employeeId}")
	public ResponseEntity<GkSnapProductListMainResponse> fetchSnapShotForGK(@ModelAttribute("employeeId") long employeeId){
		
		GkSnapProductListMainResponse gkSnapProductListMainResponse=new GkSnapProductListMainResponse();
		HttpStatus httpStatus;
		System.out.println("Inside fetchSnapShotForGK");
		try {
			List<GkSnapProductResponse> gkSnapProductResponses=snapService.fetchSnapProductDetailsForGk(employeeId);
			if(gkSnapProductResponses==null){
				gkSnapProductListMainResponse.setStatus(Constants.FAILURE_RESPONSE);
				httpStatus=HttpStatus.OK;
				gkSnapProductListMainResponse.setErrorMsg("No Record Found");
				
			}else{
				gkSnapProductListMainResponse.setStatus(Constants.SUCCESS_RESPONSE);
				httpStatus=HttpStatus.OK;
				gkSnapProductListMainResponse.setGkSnapProductResponses(gkSnapProductResponses);
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.out.println("fetchSnapShotForGK error:"+e.toString());
			gkSnapProductListMainResponse.setStatus(Constants.FAILURE_RESPONSE);
			httpStatus=HttpStatus.FORBIDDEN;
		}
		return new ResponseEntity<GkSnapProductListMainResponse>(gkSnapProductListMainResponse,httpStatus);
	}
}
