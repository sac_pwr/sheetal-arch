package com.sheetalarch.rcontroller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;import org.springframework.jmx.export.annotation.ManagedOperation;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.sheetalarch.entity.Payment;
import com.sheetalarch.models.BaseDomain;
import com.sheetalarch.models.OrderDetailsForPayment;
import com.sheetalarch.models.OrderDetailsListForPayment;
import com.sheetalarch.models.PaymentListResponse;
import com.sheetalarch.models.PaymentNotReceiveRequest;
import com.sheetalarch.models.PaymentTakeRequest;
import com.sheetalarch.models.ReOrderPaymentNotReceiveRequest;
import com.sheetalarch.models.ReOrderPaymentTakeRequest;
import com.sheetalarch.service.PaymentService;
import com.sheetalarch.utils.Constants;

@RestController
public class PaymentController {
	
	@Autowired
	PaymentService paymentService;
	
	@PostMapping("/paymentNotReceivingToday")
	public ResponseEntity<BaseDomain> paymentNotReceiveingToday(@RequestBody PaymentNotReceiveRequest paymentNotReceiveRequest){
		
		BaseDomain baseDomain=new BaseDomain();
		HttpStatus httpStatus;
		System.out.println("paymentNotReceiveingToday");
		
		try {
			paymentService.updatePaymentStatusAndOrderStatus(paymentNotReceiveRequest);
			baseDomain.setStatus(Constants.SUCCESS_RESPONSE);
			httpStatus=HttpStatus.OK;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.out.println("paymentNotReceiveingToday error:"+e.toString());
			baseDomain.setStatus(Constants.FAILURE_RESPONSE);
			httpStatus=HttpStatus.FORBIDDEN;
			
		}
		return new ResponseEntity<BaseDomain>(baseDomain,httpStatus);
	}
	
	@PostMapping("/savePaymentStatusOfPaymentReceivingToday")
	public ResponseEntity<BaseDomain> savePaymentStatusOfPaymentReceivingToday(@RequestBody PaymentTakeRequest paymentTakeRequest){
		 
		BaseDomain  baseDomain=new BaseDomain();
		HttpStatus httpStatus;
		System.out.println("savePaymentStatusOfPaymentReceivingToday");
		
		try {
			paymentService.savePaymentStatus(paymentTakeRequest);
			baseDomain.setStatus(Constants.SUCCESS_RESPONSE);
			httpStatus=HttpStatus.OK;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.out.println("savePaymentStatusOfPaymentReceivingToday Error:"+e.toString());
			baseDomain.setStatus(Constants.FAILURE_RESPONSE);
			httpStatus=HttpStatus.FORBIDDEN;
			
		}
		return new ResponseEntity<BaseDomain>(baseDomain,httpStatus);
	}

	
	@GetMapping("/fetchPaymentListByEmpIdAndPaymentStatus/{employeeId}/{paymentStatus}")
	public ResponseEntity<OrderDetailsListForPayment> fetchPaymentListByEmpIdAndPaymentStatus(@ModelAttribute("employeeId") long employeeId,@ModelAttribute("paymentStatus") String paymentStatus){
	
		OrderDetailsListForPayment orderDetailsListForPayment=new OrderDetailsListForPayment();
		HttpStatus httpStatus;
		System.out.println("Inside fetchPaymentListByEmpIdAndPaymentStatus");
		try {
			List<OrderDetailsForPayment> paymentList=paymentService.fetchOrderListForPayment(employeeId, paymentStatus);
			if(paymentList==null){
				orderDetailsListForPayment.setStatus(Constants.FAILURE_RESPONSE);
				httpStatus=HttpStatus.NO_CONTENT;
			}
			else{
				orderDetailsListForPayment.setStatus(Constants.SUCCESS_RESPONSE);
				httpStatus=HttpStatus.OK;
				orderDetailsListForPayment.setOrderDetailsList(paymentList);
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.out.println(" fetchPaymentListByEmpIdAndPaymentStatus Error:"+e.toString());
			orderDetailsListForPayment.setStatus(Constants.FAILURE_RESPONSE);
			httpStatus=HttpStatus.FORBIDDEN;
		}
		return new ResponseEntity<OrderDetailsListForPayment>(orderDetailsListForPayment,httpStatus);
	}
	
	@PostMapping("/savePaymentStatusOfReOrderPaymentReceivingToday")
	public ResponseEntity<BaseDomain> savePaymentStatusOfReOrderPaymentReceivingToday(@RequestBody ReOrderPaymentTakeRequest reOrderPaymentTakeRequest){
		
		BaseDomain  baseDomain=new BaseDomain();
		HttpStatus httpStatus;
		System.out.println("savePaymentStatusOfReOrderPaymentReceivingToday");
		
		try {
			paymentService.savePaymentStatusOfReOrder(reOrderPaymentTakeRequest);
			baseDomain.setStatus(Constants.SUCCESS_RESPONSE);
			httpStatus=HttpStatus.OK;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.out.println("savePaymentStatusOfPaymentReceivingToday Error:"+e.toString());
			baseDomain.setStatus(Constants.FAILURE_RESPONSE);
			httpStatus=HttpStatus.FORBIDDEN;
			
		}
		return new ResponseEntity<BaseDomain>(baseDomain,httpStatus);
	}

	@PostMapping("/paymentOfReorderNotReceivingToday")
	public ResponseEntity<BaseDomain> paymentOfReorderNotReceivingToday(@RequestBody ReOrderPaymentNotReceiveRequest reOrderPaymentNotReceiveRequest){
		
		BaseDomain baseDomain=new BaseDomain();
		HttpStatus httpStatus;
		System.out.println("paymentOfReorderNotReceivingToday");
		
		try {
			paymentService.updatePaymentNotReceiveStatusOfReOrder(reOrderPaymentNotReceiveRequest);
			baseDomain.setStatus(Constants.SUCCESS_RESPONSE);
			httpStatus=HttpStatus.OK;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.out.println("paymentNotReceiveingToday error:"+e.toString());
			baseDomain.setStatus(Constants.FAILURE_RESPONSE);
			httpStatus=HttpStatus.FORBIDDEN;
			
		}
		return new ResponseEntity<BaseDomain>(baseDomain,httpStatus);
	}
	


}
