package com.sheetalarch.rcontroller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.sheetalarch.entity.Area;
import com.sheetalarch.entity.Booth;
import com.sheetalarch.entity.Brand;
import com.sheetalarch.entity.Categories;
import com.sheetalarch.entity.EmployeeDetails;
import com.sheetalarch.entity.Route;
import com.sheetalarch.models.BaseDomain;
import com.sheetalarch.models.BoothListResponse;
import com.sheetalarch.models.BoothResponse;
import com.sheetalarch.models.BrandCategoryResponse;
import com.sheetalarch.models.CheckBoothResponse;
import com.sheetalarch.models.RouteListResponse;
import com.sheetalarch.service.BoothService;
import com.sheetalarch.service.EmployeeDetailsService;
import com.sheetalarch.service.ProductService;
import com.sheetalarch.utils.Constants;

@RestController
public class BoothController {

	@Autowired
	BoothService boothService;
	
	@Autowired
	ProductService productService;
	
	@Autowired
	EmployeeDetailsService employeeDetailsService;
	
	@Autowired
	Booth booth;
	
	
	
	
	
	

	// Booth No is available by Booth No... to save booth..
	@GetMapping("/checkBoothNo/{boothNo}")
	public ResponseEntity<CheckBoothResponse> checkBoothNo(@ModelAttribute("boothNo") String boothNo) {

		CheckBoothResponse checkBoothResponse = new CheckBoothResponse();
		HttpStatus httpStatus;

		try {
			Booth checkBooth = boothService.fetchBoothByBoothNo(boothNo);
			if (checkBooth == null) {
				checkBoothResponse.setStatus(Constants.SUCCESS_RESPONSE);
				checkBoothResponse.setBoothAvailable(Constants.BOOTH_NO_AVAILABLE);
				httpStatus = HttpStatus.OK;
			} else {
				checkBoothResponse.setStatus(Constants.FAILURE_RESPONSE);
				checkBoothResponse.setBoothAvailable(Constants.BOOTH_NO_DUPLICATION);
				httpStatus = HttpStatus.OK;
			}

		} catch (Exception e) {
			System.out.println("Check Booth No :" + e.toString());
			httpStatus = HttpStatus.FORBIDDEN;
			checkBoothResponse.setErrorMsg("Something Went Wrong");
		}

		return new ResponseEntity<CheckBoothResponse>(checkBoothResponse, httpStatus);

	}
	// here we are fetching RouteList  By employeeId 
	// this is used for saving the Booth
	@GetMapping("/fetchRouteListByEmployeeId/{employeeId}")
	public ResponseEntity<RouteListResponse> fetchRouteListByEmployeeId(@ModelAttribute("employeeId") long employeeId){
		
		RouteListResponse routeListResponse=new RouteListResponse();
		HttpStatus httpStatus;
		System.out.println("fetchRouteListByEmployeeId");
		try {
			EmployeeDetails employeeDetails=employeeDetailsService.getEmployeeDetailsByemployeeId(employeeId);
			List<Route> routeList=new ArrayList<>();
			if(employeeDetails.getEmployee().getDepartment().getName().equals(Constants.AREA_SALESMAN_DEPT_NAME)){
				List<Area> areas=employeeDetailsService.fetchAreaListByEmployeeId(employeeId);
				List<Route> routes=employeeDetailsService.fetchRouteListByAreaId(areas.get(0).getAreaId());
				routeList.addAll(routes);
			}else{
			List<Route> routes=employeeDetailsService.fetchEmployeeRouteListByEmployeeId(employeeId);
			routeList.addAll(routes);
			}
			if(routeList.isEmpty()){
				routeListResponse.setStatus(Constants.FAILURE_RESPONSE);
				httpStatus=HttpStatus.NO_CONTENT;
				
			}else{
				routeListResponse.setStatus(Constants.SUCCESS_RESPONSE);
				httpStatus=HttpStatus.OK;
				routeListResponse.setRouteList(routeList);
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.out.println("fetchRouteListByEmployeeId Error:"+e.toString());
			routeListResponse.setStatus(Constants.FAILURE_RESPONSE);
			httpStatus=HttpStatus.FORBIDDEN;
			routeListResponse.setErrorMsg("Somthing went wrong");
		}
		
		
		return new ResponseEntity<RouteListResponse>(routeListResponse,httpStatus);
		
	}
	
	// here we are saving the new booth 
		@PostMapping("/saveBooth")
		public ResponseEntity<BaseDomain> saveBooth(@RequestBody Booth booth){
			
			System.out.println("Inside Save Booth Entity");
			BaseDomain baseDomain=new BaseDomain();
			HttpStatus httpStatus;
			
			try{
				boothService.saveBooth(booth);
				baseDomain.setStatus(Constants.SUCCESS_RESPONSE);
				httpStatus=HttpStatus.OK;
			}
			catch(Exception e){
				baseDomain.setStatus(Constants.FAILURE_RESPONSE);
				System.out.println("inside save Booth Failure:: "+e.toString());
				httpStatus=HttpStatus.FORBIDDEN;
			}
			
			return new ResponseEntity<BaseDomain>(baseDomain,httpStatus);
			
		}
	
	// here we are fetching the Booth List For Salesman Add new Booth 1st Screen
	@GetMapping("/fetchBoothList/{employeeId}")
	public ResponseEntity<BoothListResponse> fetchBoothList(@ModelAttribute("employeeId") long employeeId){
		
		BoothListResponse boothListResponse=new BoothListResponse();
		HttpStatus httpStatus;
		System.out.println("fetchBoothList");
		
		try {
			List<Booth> boothList=boothService.fetchBoothListByEmployeeId(employeeId);			
			if(boothList==null)
			{
				boothListResponse.setStatus(Constants.FAILURE_RESPONSE);
				boothListResponse.setErrorMsg("No Record Found");
				httpStatus=HttpStatus.NO_CONTENT;
			}
			else
			{
				boothListResponse.setStatus(Constants.SUCCESS_RESPONSE);
				boothListResponse.setBoothList(boothList);
				httpStatus=HttpStatus.OK;
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.out.println("fetchBoothList Error:"+e.toString());
			boothListResponse.setStatus(Constants.FAILURE_RESPONSE);
			httpStatus=HttpStatus.FORBIDDEN;
			
		}
		return new ResponseEntity<BoothListResponse>(boothListResponse,httpStatus);
	
		}
	
	
	@PostMapping("/updateBooth")
	public ResponseEntity<BaseDomain> updateBooth(@RequestBody Booth booth)
	{
		System.out.println("inside updateBooth");
		BaseDomain baseDomain=new BaseDomain();
		HttpStatus httpStatus;		
		
		try {
			Booth booth1=boothService.fetchBoothByBoothNo(booth.getBoothNo());
			booth.setBoothAddedDatetime(booth1.getBoothAddedDatetime());
			boothService.updateBooth(booth);
			baseDomain.setStatus(Constants.SUCCESS_RESPONSE);
			httpStatus=HttpStatus.OK;
			
		} catch (Exception e) {
			baseDomain.setStatus(Constants.FAILURE_RESPONSE);
			httpStatus=HttpStatus.FORBIDDEN;
		}
			return new ResponseEntity<BaseDomain>(baseDomain, httpStatus);

	}
	
	/*// here we are fetching Booth object by boothID and BrandList and CategoryList
	@GetMapping("/fetchBoothBrandAndCategoryByBoothNo/{boothNo}")
	public ResponseEntity<NewOrderBoothResponse> fetchBoothBrandAndCategoryByBoothId(@ModelAttribute("boothNo") String boothNo){
		
		NewOrderBoothResponse newOrderBoothResponse= new NewOrderBoothResponse();
		HttpStatus httpStatus;
		System.out.println("fetchBoothBrandAndCategoryByBoothId");
		
		try {
			Booth booth=boothService.fetchBoothByBoothNo(boothNo);
			if(booth==null)
			{
				newOrderBoothResponse.setStatus(Constants.FAILURE_RESPONSE);
				httpStatus=HttpStatus.NO_CONTENT;
				newOrderBoothResponse.setErrorMsg("No Record Found");
				
			}
			else
			{
			List<Brand> brandList=productService.fetchBrandList();
			List<Categories> categoriesList=productService.fetchCategories();
			
			newOrderBoothResponse.setStatus(Constants.SUCCESS_RESPONSE);
			newOrderBoothResponse.setBooth(booth);
			newOrderBoothResponse.setBrandList(brandList);
			newOrderBoothResponse.setCategoriesList(categoriesList);
			httpStatus=HttpStatus.OK;
			
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.out.println("fetchBoothBrandAndCategoryByBoothId Error:"+e.toString());

			newOrderBoothResponse.setStatus(Constants.FAILURE_RESPONSE);
			httpStatus=HttpStatus.FORBIDDEN;
		}
		return new ResponseEntity<NewOrderBoothResponse>(newOrderBoothResponse,httpStatus);
		
	}*/
	
	
	// fetching BrandList and CategoryList for Taking Order from Booth....
		@GetMapping("/fetchBrandAndCategoryList")
		public ResponseEntity<BrandCategoryResponse> responseEntity() {
			BrandCategoryResponse brandCategoryResponse = new BrandCategoryResponse();
			HttpStatus httpStatus;
			System.out.println("Inside Brand and Category");

			try {
				List<Brand> brand = productService.fetchBrandList();
				List<Categories> categories = productService.fetchCategories();

				if (brand == null && categories == null) {
					brandCategoryResponse.setStatus(Constants.FAILURE_RESPONSE);
					brandCategoryResponse.setErrorMsg("Brand or Category Empty");
					httpStatus = HttpStatus.OK;
				} else {
					brandCategoryResponse.setBrandList(brand);
					brandCategoryResponse.setCategoriesList(categories);
					brandCategoryResponse.setStatus(Constants.SUCCESS_RESPONSE);
					httpStatus = HttpStatus.OK;
				}
			} catch (Exception e) {
				e.printStackTrace();
				brandCategoryResponse.setStatus(Constants.FAILURE_RESPONSE);
				httpStatus = HttpStatus.FORBIDDEN;
				System.out.println("Brand and Category Error::" + e.toString());
			}

			return new ResponseEntity<BrandCategoryResponse>(brandCategoryResponse, httpStatus);
		}
		
		// fetchBoothList by Booth No... to take Orders..
		@GetMapping("/fetchBoothByBoothNo/{boothNo}")
		public ResponseEntity<BoothResponse> boothResponse(@ModelAttribute("boothNo") String boothNo) {
			BoothResponse boothResponse = new BoothResponse();
			HttpStatus httpStatus;
			System.out.println("Inside fetching Booth...");

			try {
				Booth booth = boothService.fetchBoothByBoothNo(boothNo);
				if (booth == null) {
					boothResponse.setStatus(Constants.FAILURE_RESPONSE);
					boothResponse.setErrorMsg("Booth No Found");
					httpStatus = HttpStatus.OK;
				} else {
					boothResponse.setBooth(booth);
					boothResponse.setStatus(Constants.SUCCESS_RESPONSE);
					httpStatus = HttpStatus.OK;
				}

			} catch (Exception e) {
				boothResponse.setStatus(Constants.FAILURE_RESPONSE);
				httpStatus = HttpStatus.OK;
				System.out.println("fetch Booth :::" + e.toString());
			}
			return new ResponseEntity<BoothResponse>(boothResponse, httpStatus);
		}

}
