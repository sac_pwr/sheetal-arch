package com.sheetalarch.rcontroller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.sheetalarch.entity.EmployeeDetails;
import com.sheetalarch.entity.OrderDetails;
import com.sheetalarch.entity.Payment;
import com.sheetalarch.entity.ReOrderDetails;
import com.sheetalarch.entity.ReOrderProductDetails;
import com.sheetalarch.entity.Route;
import com.sheetalarch.models.ASMMemberReportOrderListRequest;
import com.sheetalarch.models.AsmOrderDetailsListReportResponse;
import com.sheetalarch.models.CancelAsmReportMainResponse;
import com.sheetalarch.models.CancelAsmReportResponse;
import com.sheetalarch.models.PaymentListResponse;
import com.sheetalarch.models.ReorderAndReOrderProductListResponse;
import com.sheetalarch.service.ASMReportService;
import com.sheetalarch.service.EmployeeDetailsService;
import com.sheetalarch.utils.Constants;

@RestController
public class ASMReportController {

	@Autowired
	ASMReportService asmReportService;

	@Autowired
	EmployeeDetailsService employeeDetailsService;

	// here we fetch sm list by asmId for cancel report 1st screen
	@GetMapping("/fetchSMListByAsmIdForCancelOrder/{employeeId}")
	public ResponseEntity<CancelAsmReportMainResponse> fetchSMListByAsmIdForCancelOrder(
			@ModelAttribute("employeeId") long employeeId) {

		CancelAsmReportMainResponse cancelAsmReportMainResponse = new CancelAsmReportMainResponse();
		HttpStatus httpStatus;
		System.out.println("Inside fetchSMListByAsmIdForCancelOrder");
		try {

			System.out.println("sjcjd hM");

			List<CancelAsmReportResponse> cancelAsmReportResponses = asmReportService
					.fetchSmAndCancelOrderByAsmId(employeeId);
			if (cancelAsmReportResponses == null) {
				cancelAsmReportMainResponse.setStatus(Constants.FAILURE_RESPONSE);
				httpStatus = HttpStatus.NO_CONTENT;
			} else {
				cancelAsmReportMainResponse.setStatus(Constants.SUCCESS_RESPONSE);
				cancelAsmReportMainResponse.setCancelAsmReportResponses(cancelAsmReportResponses);
				httpStatus = HttpStatus.OK;

			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.out.println("fetchSMListByAsmIdForCancelOrder Error" + e.toString());
			cancelAsmReportMainResponse.setStatus(Constants.FAILURE_RESPONSE);
			httpStatus = HttpStatus.FORBIDDEN;
		}
		return new ResponseEntity<CancelAsmReportMainResponse>(cancelAsmReportMainResponse, httpStatus);
	}

	@GetMapping("/fetchCancelOrderListForAsmCancelReport/{employeeId}")
	public ResponseEntity<AsmOrderDetailsListReportResponse> fetchCancelOrderListForAsmCancelReport(
			@ModelAttribute("employeeId") long employeeId) {

		AsmOrderDetailsListReportResponse asmOrderDetailsListReportResponse = new AsmOrderDetailsListReportResponse();
		HttpStatus httpStatus;
		System.out.println("Inside fetchCancelOrderListForAsmCancelReport");
		try {
			List<OrderDetails> orderDetailsList = asmReportService.todaysCancelOrderListByEmployeeId(employeeId);
			if (orderDetailsList == null) {
				asmOrderDetailsListReportResponse.setStatus(Constants.FAILURE_RESPONSE);
				httpStatus = HttpStatus.NO_CONTENT;
			} else {
				asmOrderDetailsListReportResponse.setStatus(Constants.SUCCESS_RESPONSE);
				httpStatus = HttpStatus.OK;
				EmployeeDetails employeeDetails = employeeDetailsService.getEmployeeDetailsByemployeeId(employeeId);
				List<Route> routesList = employeeDetailsService.fetchEmployeeRouteListByEmployeeId(employeeId);
				asmOrderDetailsListReportResponse.setSalesmanName(employeeDetails.getName());
				asmOrderDetailsListReportResponse.setMobileNo(employeeDetails.getContact().getMobileNumber());
				asmOrderDetailsListReportResponse.setOrderDetailsList(orderDetailsList);
				asmOrderDetailsListReportResponse.setRouteNo(routesList.get(0).getRouteNo());
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.out.println("fetchCancelOrderListForAsmCancelReport Error" + e.toString());
			asmOrderDetailsListReportResponse.setStatus(Constants.FAILURE_RESPONSE);
			httpStatus = HttpStatus.FORBIDDEN;

		}
		return new ResponseEntity<AsmOrderDetailsListReportResponse>(asmOrderDetailsListReportResponse, httpStatus);
	}

	@GetMapping("/fetchSMListByAsmIdForReOrder/{employeeId}")
	public ResponseEntity<CancelAsmReportMainResponse> fetchSMListByAsmIdForReOrder(
			@ModelAttribute("employeeId") long employeeId) {

		CancelAsmReportMainResponse cancelAsmReportMainResponse = new CancelAsmReportMainResponse();
		HttpStatus httpStatus;
		System.out.println("Inside fetchSMListByAsmIdForReOrder");
		try {

			List<CancelAsmReportResponse> cancelAsmReportResponses = asmReportService
					.fetchSmAndReOrderByAsmId(employeeId);
			if (cancelAsmReportResponses == null) {
				cancelAsmReportMainResponse.setStatus(Constants.FAILURE_RESPONSE);
				httpStatus = HttpStatus.NO_CONTENT;
			} else {
				cancelAsmReportMainResponse.setStatus(Constants.SUCCESS_RESPONSE);
				cancelAsmReportMainResponse.setCancelAsmReportResponses(cancelAsmReportResponses);
				httpStatus = HttpStatus.OK;

			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.out.println("fetchSMListByAsmIdForReOrder Error" + e.toString());
			cancelAsmReportMainResponse.setStatus(Constants.FAILURE_RESPONSE);
			httpStatus = HttpStatus.FORBIDDEN;
		}
		return new ResponseEntity<CancelAsmReportMainResponse>(cancelAsmReportMainResponse, httpStatus);
	}

	// here we are fetching the List Of Reorder Details by empId For ASM Reorder
	// Report
	@GetMapping("/fetchReOrderListForAsmReOrderReport/{employeeId}")
	public ResponseEntity<AsmOrderDetailsListReportResponse> fetchReOrderListForAsmReOrderReport(
			@ModelAttribute("employeeId") long employeeId) {

		AsmOrderDetailsListReportResponse asmOrderDetailsListReportResponse = new AsmOrderDetailsListReportResponse();
		HttpStatus httpStatus;
		System.out.println("Inside fetchReOrderListForAsmReOrderReport");
		try {
			List<ReOrderDetails> reOrderDetailsList = asmReportService.todaysReOrderListByEmployeeId(employeeId);
			if (reOrderDetailsList == null) {
				asmOrderDetailsListReportResponse.setStatus(Constants.FAILURE_RESPONSE);
				httpStatus = HttpStatus.NO_CONTENT;
			} else {
				asmOrderDetailsListReportResponse.setStatus(Constants.SUCCESS_RESPONSE);
				httpStatus = HttpStatus.OK;
				EmployeeDetails employeeDetails = employeeDetailsService.getEmployeeDetailsByemployeeId(employeeId);
				List<Route> routesList = employeeDetailsService.fetchEmployeeRouteListByEmployeeId(employeeId);
				asmOrderDetailsListReportResponse.setSalesmanName(employeeDetails.getName());
				asmOrderDetailsListReportResponse.setMobileNo(employeeDetails.getContact().getMobileNumber());
				asmOrderDetailsListReportResponse.setReOrderDetailsList(reOrderDetailsList);
				asmOrderDetailsListReportResponse.setRouteNo(routesList.get(0).getRouteNo());
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.out.println("fetchReOrderListForAsmReOrderReport Error" + e.toString());
			asmOrderDetailsListReportResponse.setStatus(Constants.FAILURE_RESPONSE);
			httpStatus = HttpStatus.FORBIDDEN;

		}
		return new ResponseEntity<AsmOrderDetailsListReportResponse>(asmOrderDetailsListReportResponse, httpStatus);
	}

	@GetMapping("/fetchReorderAndReOrderProductListByReOrderId/{reOrderId}")
	public ResponseEntity<ReorderAndReOrderProductListResponse> fetchReorderAndReOrderProductListByreOrderId(
			@ModelAttribute("reOrderId") String reOrderId) {

		ReorderAndReOrderProductListResponse reorderAndReOrderProductListResponse = new ReorderAndReOrderProductListResponse();
		HttpStatus httpStatus;
		System.out.println(" Inside fetchReorderAndReOrderProductListByreOrderId");

		ReOrderDetails reOrderDetails = asmReportService.fetchReOrderDetailsByReOrderId(reOrderId);
		if (reOrderDetails == null) {
			reorderAndReOrderProductListResponse.setStatus(Constants.FAILURE_RESPONSE);
			httpStatus = HttpStatus.NO_CONTENT;
		} else {
			List<ReOrderProductDetails> reOrderProductDetailsList = asmReportService
					.fetchReOrderProductDetailsByReOrderId(reOrderId);
			reorderAndReOrderProductListResponse.setStatus(Constants.SUCCESS_RESPONSE);
			httpStatus = HttpStatus.OK;
			reorderAndReOrderProductListResponse.setReOrderDetails(reOrderDetails);
			reorderAndReOrderProductListResponse.setReOrderProductDetailsList(reOrderProductDetailsList);

		}
		return new ResponseEntity<ReorderAndReOrderProductListResponse>(reorderAndReOrderProductListResponse,
				httpStatus);
	}

	@PostMapping("/fetchingOrderDetailsListForMemberReportByEmpIdAndDateRange")
	public ResponseEntity<AsmOrderDetailsListReportResponse> fetchingOrderDetailsListForMemberReportByEmpIdAndDateRange(
			@RequestBody ASMMemberReportOrderListRequest asmMemberReportOrderListRequest) {

		AsmOrderDetailsListReportResponse asmOrderDetailsListReportResponse = new AsmOrderDetailsListReportResponse();
		HttpStatus httpStatus;
		System.out.println("Inside fetchingOrderDetailsListForMemberReportByEmpIdAndDateRange");
		try {
			EmployeeDetails employeeDetails = employeeDetailsService
					.getEmployeeDetailsByemployeeId(asmMemberReportOrderListRequest.getEmployeeId());
			List<Route> routeList = employeeDetailsService
					.fetchEmployeeRouteListByEmployeeId(asmMemberReportOrderListRequest.getEmployeeId());

			List<OrderDetails> orderDetailsList = asmReportService
					.fetchOrderDetailsListByEmpIdAndDateRangeforASMMemberReport(
							asmMemberReportOrderListRequest.getEmployeeId(),
							asmMemberReportOrderListRequest.getFromDate(), asmMemberReportOrderListRequest.getToDate(),
							asmMemberReportOrderListRequest.getRange());
			if (orderDetailsList == null) {
				asmOrderDetailsListReportResponse.setStatus(Constants.FAILURE_RESPONSE);
				httpStatus = HttpStatus.OK;
				asmOrderDetailsListReportResponse.setSalesmanName(employeeDetails.getName());
				asmOrderDetailsListReportResponse.setMobileNo(employeeDetails.getContact().getMobileNumber());
				asmOrderDetailsListReportResponse.setRouteNo(routeList.get(0).getRouteNo());
				asmOrderDetailsListReportResponse.setErrorMsg("No Record Found");
			} else {
				asmOrderDetailsListReportResponse.setStatus(Constants.SUCCESS_RESPONSE);
				httpStatus = HttpStatus.OK;
				asmOrderDetailsListReportResponse.setSalesmanName(employeeDetails.getName());
				asmOrderDetailsListReportResponse.setMobileNo(employeeDetails.getContact().getMobileNumber());
				asmOrderDetailsListReportResponse.setRouteNo(routeList.get(0).getRouteNo());
				asmOrderDetailsListReportResponse.setOrderDetailsList(orderDetailsList);
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.out.println("fetchingOrderDetailsListForMemberReportByEmpIdAndDateRange Error:" + e.toString());
			asmOrderDetailsListReportResponse.setStatus(Constants.FAILURE_RESPONSE);
			httpStatus = HttpStatus.FORBIDDEN;
		}
		return new ResponseEntity<AsmOrderDetailsListReportResponse>(asmOrderDetailsListReportResponse, httpStatus);
	}
	
	@PostMapping("/fetchingPaymentListForPaymentReportByEmpIdAndDateRange")
	public ResponseEntity<PaymentListResponse> fetchingPaymentListForPaymentReportByEmpIdAndDateRange(
			@RequestBody ASMMemberReportOrderListRequest asmMemberReportOrderListRequest){
		
		PaymentListResponse paymentListResponse=new PaymentListResponse();
		HttpStatus httpStatus;
		System.out.println("Inside fetchingPaymentListForPaymentReportByEmpIdAndDateRange");
		try {
			List<Payment> paymentsList=asmReportService.fetchPaymentListByEmployeeIdAndDateRange(asmMemberReportOrderListRequest.getEmployeeId(), asmMemberReportOrderListRequest.getFromDate(), asmMemberReportOrderListRequest.getToDate(), asmMemberReportOrderListRequest.getRange());
			if(paymentsList==null){
				paymentListResponse.setStatus(Constants.FAILURE_RESPONSE);
				httpStatus=HttpStatus.NO_CONTENT;
			}
			else{
				paymentListResponse.setStatus(Constants.SUCCESS_RESPONSE);
				httpStatus=HttpStatus.OK;
				paymentListResponse.setPaymentList(paymentsList);
				
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.out.println("fetchingPaymentListForPaymentReportByEmpIdAndDateRange error"+e.toString());
			paymentListResponse.setStatus(Constants.FAILURE_RESPONSE);
			httpStatus=HttpStatus.FORBIDDEN;
		}
		return new ResponseEntity<PaymentListResponse>(paymentListResponse,httpStatus);
	}
}
