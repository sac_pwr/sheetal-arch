package com.sheetalarch.rcontroller;

import java.text.DateFormat;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.sheetalarch.entity.Area;
import com.sheetalarch.entity.Booth;
import com.sheetalarch.entity.Cluster;
import com.sheetalarch.entity.DispatchOrderDetails;
import com.sheetalarch.entity.DispatchOrderProductDetails;
import com.sheetalarch.entity.Employee;
import com.sheetalarch.entity.EmployeeDetails;
import com.sheetalarch.entity.OrderDetails;
import com.sheetalarch.entity.OrderProductDetails;
import com.sheetalarch.entity.ReOrderDetails;
import com.sheetalarch.entity.ReOrderProductDetails;
import com.sheetalarch.entity.Route;
import com.sheetalarch.models.BaseDomain;
import com.sheetalarch.models.BookOrderResponse;
import com.sheetalarch.models.CancelOrderRequest;
import com.sheetalarch.models.ClusterResponse;
import com.sheetalarch.models.DispatchProductListResponse;
import com.sheetalarch.models.DispatchProductNameAndQtyListResponse;
import com.sheetalarch.models.DispatchedOrderAndProductListResponse;
import com.sheetalarch.models.GkDispatchProductReportMainResponse;
import com.sheetalarch.models.GkDispatchProductReportRequest;
import com.sheetalarch.models.GkDispatchProductReportResponse;
import com.sheetalarch.models.GkDispatchedOrderListMainResponse;
import com.sheetalarch.models.GkDispatchedOrderListResponse;
import com.sheetalarch.models.GkDispatchedProductListResponse;
import com.sheetalarch.models.GkProductDetailsReportMainResponse;
import com.sheetalarch.models.GkProductDetailsReportResponse;
import com.sheetalarch.models.OrderDetailsAndOrderProductListResponse;
import com.sheetalarch.models.OrderDetailsListResponse;
import com.sheetalarch.models.OrderIssueRequest;
import com.sheetalarch.models.OrderListGKRequest;
import com.sheetalarch.models.OrderListMainResponse;
import com.sheetalarch.models.OrderListResponse;
import com.sheetalarch.models.OrderPaymentPeriodDaysRequest;
import com.sheetalarch.models.OrderRequest;
import com.sheetalarch.models.ReOrderDeliveryRequest;
import com.sheetalarch.models.ReOrderDetailsAndProductDetailsRespose;
import com.sheetalarch.service.AreaService;
import com.sheetalarch.service.BoothService;
import com.sheetalarch.service.EmployeeDetailsService;
import com.sheetalarch.service.OrderDetailsService;
import com.sheetalarch.utils.Constants;

@RestController
public class OrderDetailsController {

	@Autowired
	OrderDetailsService orderDetailsService;

	@Autowired
	OrderDetails orderDetails;

	@Autowired
	OrderProductDetails orderProductDetails;

	@Autowired
	EmployeeDetailsService employeeDetailsService;

	@Autowired
	BoothService boothService;

	@Autowired
	AreaService areaService;

	
	String range,toDate,fromDate;
	
	@PostMapping("/bookOrder")
	public ResponseEntity<BookOrderResponse> bookOrder(@RequestBody OrderRequest orderRequest) {

		HttpStatus httpStatus;
		BookOrderResponse bookOrderResponse = new BookOrderResponse();

		try {
			String orderDetailsId = orderDetailsService.bookOrder(orderRequest);
			orderDetails = orderDetailsService.fetchOrderDetailsByOrderId(orderDetailsId);

			bookOrderResponse.setBoothNo(orderDetails.getBooth().getBoothNo());
			bookOrderResponse.setOrderId(orderDetailsId);
			bookOrderResponse.setTotalAmount(orderDetails.getTotalAmount());
			bookOrderResponse.setTotalAmountWithTax(orderDetails.getTotalAmountWithTax());
			bookOrderResponse.setStatus(Constants.SUCCESS_RESPONSE);
			httpStatus = HttpStatus.OK;
			return new ResponseEntity<BookOrderResponse>(bookOrderResponse, httpStatus);

		} catch (Exception e) {

			httpStatus = HttpStatus.FORBIDDEN;
			bookOrderResponse.setStatus(Constants.FAILURE_RESPONSE);
			return new ResponseEntity<BookOrderResponse>(bookOrderResponse, httpStatus);
		}
	}

	// here we take the delivery date
	@PostMapping("/orderPaymentPeriodDaysUpdate")
	public ResponseEntity<BaseDomain> orderPaymentPeriodDaysUpdate(
			@RequestBody OrderPaymentPeriodDaysRequest orderPaymentPeriodDaysRequest) {

		HttpStatus httpStatus;
		BaseDomain baseDomain = new BaseDomain();

		try {
			orderDetails = orderDetailsService.fetchOrderDetailsByOrderId(orderPaymentPeriodDaysRequest.getOrderId());
			// DispatchOrderDetails
			// dispatchOrderDetails=orderDetailsService.fetchDispatchOrderDetailsByemployeeIdAndDeliveryDate(orderDetails.getEmployeeASMandSM().getEmployeeId());
			DispatchOrderDetails dispatchOrderDetails = orderDetailsService
					.fetchDispatchOrderDetailsByemployeeIdAndDeliveryDate1(
							orderDetails.getEmployeeASMandSM().getEmployeeId(),
							orderPaymentPeriodDaysRequest.getDeliveryDate());

			String dateStr = orderPaymentPeriodDaysRequest.getDeliveryDate();
			DateFormat formatter = new SimpleDateFormat("yyyy/MM/dd");
			Date deliveryDate = formatter.parse(dateStr);

			orderDetails.setDeliveryDate(deliveryDate);

			// here in below three line we are updating the GateKeeper in
			// OrderDetails
			Cluster cluster = areaService.fetchClusterBySalesmanId(orderDetails.getEmployeeASMandSM().getEmployeeId());
			Employee gKemployee = areaService.fetchGKEmployeeByClusterId(cluster.getClusterId());
			Area salemanArea = areaService.fetchAreaBysalesmanId(orderDetails.getEmployeeASMandSM().getEmployeeId());

			orderDetails.setEmployeeGK(gKemployee);

			orderDetailsService.updateOrderDetailsPaymentDays(orderDetails);
			// dispatchOrderDetails.setEmployeeGk(gKemployee);
			orderDetailsService.updateDispatchProduct(dispatchOrderDetails, orderDetails, gKemployee, salemanArea);

			DispatchOrderDetails dispatchOrderDetails1 = orderDetailsService
					.fetchDispatchOrderDetailsByemployeeIdAndDeliveryDate1(
							orderDetails.getEmployeeASMandSM().getEmployeeId(),
							orderPaymentPeriodDaysRequest.getDeliveryDate());

			List<OrderProductDetails> orderProductDetailsList = orderDetailsService
					.fetchOrderProductDetailsByOrderId(orderPaymentPeriodDaysRequest.getOrderId());
			List<DispatchOrderProductDetails> dispatchOrderProductDetailsList = orderDetailsService
					.fetchDispatchOrderProductListByDispatchId(dispatchOrderDetails1.getDispatchedId());
			orderDetailsService.updateDispatchProductDetails(orderProductDetailsList, dispatchOrderProductDetailsList,
					dispatchOrderDetails1);

			baseDomain.setStatus(Constants.SUCCESS_RESPONSE);
			httpStatus = HttpStatus.OK;

		} catch (Exception e) {
			httpStatus = HttpStatus.FORBIDDEN;
			baseDomain.setStatus(Constants.FAILURE_RESPONSE);

		}
		return new ResponseEntity<BaseDomain>(baseDomain, httpStatus);

	}

	// fetching orderList for future and current day for GK OrderList Screen
	@PostMapping("/fetchOrderListForGK")
	public ResponseEntity<GkDispatchedOrderListMainResponse> fetchOrderListForGK(
			@RequestBody OrderListGKRequest orderListGKRequest) {

		GkDispatchedOrderListMainResponse gkDispatchedOrderListMainResponse = new GkDispatchedOrderListMainResponse();
		HttpStatus httpStatus;
		System.out.println("Inside fetchOrderListForGK");

		try {
			List<GkDispatchedOrderListResponse> gkDispatchedOrderListResponses = orderDetailsService
					.fetchDispatchedOrderDetailsListForGK(orderListGKRequest);

			if (gkDispatchedOrderListResponses == null) {
				gkDispatchedOrderListMainResponse.setStatus(Constants.FAILURE_RESPONSE);
				gkDispatchedOrderListMainResponse.setErrorMsg("Record not found");
				httpStatus = HttpStatus.OK;
			} else {

				gkDispatchedOrderListMainResponse.setStatus(Constants.SUCCESS_RESPONSE);
				gkDispatchedOrderListMainResponse.setGkDispatchedOrderListResponses(gkDispatchedOrderListResponses);
				httpStatus = HttpStatus.OK;
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.err.println("fetchOrderListForGK Error:" + e.toString());
			gkDispatchedOrderListMainResponse.setStatus(Constants.FAILURE_RESPONSE);
			httpStatus = HttpStatus.FORBIDDEN;
		}

		return new ResponseEntity<GkDispatchedOrderListMainResponse>(gkDispatchedOrderListMainResponse, httpStatus);

	}

	@GetMapping("/fetchDispatchProductListAndEmpDetailsByDispatchedIdAndEmpId/{dispatchedId}/{employeeId}")
	public ResponseEntity<GkDispatchedProductListResponse> fetchDispatchProductListAndEmpDetailsByDispatchedIdAndEmpId(
			@ModelAttribute("dispatchedId") long dispatchedId, @ModelAttribute("employeeId") long employeeId) {
		System.out.println("error");
		GkDispatchedProductListResponse gkDispatchedProductListResponse = new GkDispatchedProductListResponse();
		HttpStatus httpStatus;
		System.out.println("Inside fetchDispatchProductListAndEmpDetailsByDispatchedIdAndEmpId");

		try {
			List<DispatchOrderProductDetails> dispatchOrderProductDetailsList = orderDetailsService
					.fetchDispatchOrderProductListByDispatchId(dispatchedId);
			if (dispatchOrderProductDetailsList == null) {
				gkDispatchedProductListResponse.setStatus(Constants.FAILURE_RESPONSE);
				httpStatus = HttpStatus.NO_CONTENT;
			} else {
				List<Route> routesList=new ArrayList<>();
				EmployeeDetails employeeDetails = employeeDetailsService.getEmployeeDetailsByemployeeId(employeeId);
				if(employeeDetails.getEmployee().getDepartment().getName().equals(Constants.AREA_SALESMAN_DEPT_NAME)){
					List<Area> areas=employeeDetailsService.fetchAreaListByEmployeeId(employeeDetails.getEmployee().getEmployeeId());
					List<Route> routes=employeeDetailsService.fetchRouteListByAreaId(areas.get(0).getAreaId());
					routesList.addAll(routes);
				}else{
					List<Route> routes=employeeDetailsService.fetchEmployeeRouteListByEmployeeId(employeeId);
					routesList.addAll(routes);
				}
				
				Cluster cluster = areaService.fetchClusterBySalesmanId(employeeId);
				//List<Route> routesList = employeeDetailsService.fetchEmployeeRouteListByEmployeeId(employeeId);
				DispatchOrderDetails dispatchOrderDetails = orderDetailsService
						.fetchDispatchOrderDetailsByDispatchedId(dispatchedId);
				gkDispatchedProductListResponse.setStatus(Constants.SUCCESS_RESPONSE);
				httpStatus = HttpStatus.OK;
				gkDispatchedProductListResponse.setTotalAmount(dispatchOrderDetails.getTotalAmount());
				gkDispatchedProductListResponse.setTotalQty(dispatchOrderDetails.getTotalQty());
				gkDispatchedProductListResponse.setCluster(cluster.getName());
				gkDispatchedProductListResponse.setEmpName(employeeDetails.getName());
				gkDispatchedProductListResponse.setMobNo(employeeDetails.getContact().getMobileNumber());
				gkDispatchedProductListResponse.setRoutes(routesList);
				gkDispatchedProductListResponse.setDispatchOrderProductDetails(dispatchOrderProductDetailsList);
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.out.println("fetchDispatchProductListAndEmpDetailsByDispatchedIdAndEmpId Error:" + e.toString());
			gkDispatchedProductListResponse.setStatus(Constants.FAILURE_RESPONSE);
			httpStatus = HttpStatus.FORBIDDEN;

		}
		return new ResponseEntity<GkDispatchedProductListResponse>(gkDispatchedProductListResponse, httpStatus);
	}

	/*
	 * // fetching orderList for future and current day for GK OrderList Screen
	 * 
	 * @PostMapping("/fetchOrderListForGK1") public
	 * ResponseEntity<OrderListMainResponse> fetchOrderListForGK1(@RequestBody
	 * OrderListGKRequest orderListGKRequest){
	 * 
	 * OrderListMainResponse orderListMainResponse=new OrderListMainResponse();
	 * HttpStatus httpStatus; System.out.println("Inside fetchOrderListForGK");
	 * 
	 * try {
	 * 
	 * List<OrderListResponse>
	 * orderDetailsListResponse=orderDetailsService.fetchOrderDetailsListForGK1(
	 * orderListGKRequest); if(orderDetailsListResponse==null){
	 * orderListMainResponse.setStatus(Constants.FAILURE_RESPONSE);
	 * orderListMainResponse.setErrorMsg("Record not found");
	 * httpStatus=HttpStatus.OK; }else {
	 * 
	 * orderListMainResponse.setStatus(Constants.SUCCESS_RESPONSE);
	 * orderListMainResponse.setOrderListResponse(orderDetailsListResponse);
	 * httpStatus=HttpStatus.OK; } } catch (Exception e) { // TODO
	 * Auto-generated catch block e.printStackTrace();
	 * System.err.println("fetchOrderListForGK Error:"+e.toString());
	 * orderListMainResponse.setStatus(Constants.FAILURE_RESPONSE);
	 * httpStatus=HttpStatus.FORBIDDEN; }
	 * 
	 * return new
	 * ResponseEntity<OrderListMainResponse>(orderListMainResponse,httpStatus);
	 * }
	 */
	@GetMapping("/fetchOrderDetailsAndOrderProductDetailsListByOrderId/{orderId}")
	public ResponseEntity<OrderDetailsAndOrderProductListResponse> fetchOrderDetailsAndOrderProductDetailsListByOrderId(
			@ModelAttribute("orderId") String orderId) {

		OrderDetailsAndOrderProductListResponse orderDetailsAndOrderProductListResponse = new OrderDetailsAndOrderProductListResponse();

		HttpStatus httpStatus;
		System.out.println("Inside fetchOrderDetailsAndOrderProductDetailsListByOrderId");

		try {
			@SuppressWarnings("unused")
			OrderDetails orderDetails = orderDetailsService.fetchOrderDetailsByOrderId(orderId);
			if (orderDetails == null) {
				orderDetailsAndOrderProductListResponse.setStatus(Constants.FAILURE_RESPONSE);
				orderDetailsAndOrderProductListResponse.setErrorMsg("No Record Found");
				httpStatus = HttpStatus.OK;
			} else {
				List<OrderProductDetails> orderProductDetailsList = orderDetailsService
						.fetchOrderProductDetailsByOrderId(orderId);
				String mobileNo = employeeDetailsService.fetchMobileNoByOrderId(orderDetails);
				orderDetailsAndOrderProductListResponse.setStatus(Constants.SUCCESS_RESPONSE);
				orderDetailsAndOrderProductListResponse.setOrderDetails(orderDetails);
				orderDetailsAndOrderProductListResponse.setMobileNumber(mobileNo);
				orderDetailsAndOrderProductListResponse.setOrderProductDetailsList(orderProductDetailsList);
				httpStatus = HttpStatus.OK;
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			orderDetailsAndOrderProductListResponse.setStatus(Constants.FAILURE_RESPONSE);
			orderDetailsAndOrderProductListResponse.setErrorMsg("Something went wrong");
			httpStatus = HttpStatus.FORBIDDEN;
		}
		return new ResponseEntity<OrderDetailsAndOrderProductListResponse>(orderDetailsAndOrderProductListResponse,
				httpStatus);

	}

	// here we are packed the order from Gk
	@PostMapping("/packedOrderForDelivery")
	public ResponseEntity<BaseDomain> packedOrderForDelivery(@RequestBody OrderIssueRequest orderIssueRequest) {

		BaseDomain baseDomain = new BaseDomain();
		HttpStatus httpStatus;
		System.out.println(" Inside packedOrderForDelivery ");

		try {
			String a = orderDetailsService.packedOrderForDelivery(orderIssueRequest);

			if (a.equals("Success")) {
				baseDomain.setStatus(Constants.SUCCESS_RESPONSE);
				httpStatus = HttpStatus.OK;

			} else {
				baseDomain.setStatus(Constants.FAILURE_RESPONSE);
				httpStatus = HttpStatus.OK;
				baseDomain.setErrorMsg("Delivery Date is not Today");
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.out.println("packedOrderForDelivery Error:" + e.toString());
			baseDomain.setStatus(Constants.FAILURE_RESPONSE);
			httpStatus = HttpStatus.FORBIDDEN;

		}
		return new ResponseEntity<BaseDomain>(baseDomain, httpStatus);
	}

	// PENDING
	// Fetching OrderProductDetails for Dispatch Product Screen
	@GetMapping("/fetchOrderProductDetailsForDeliveryByEmployeeId/{employeeId}")
	public ResponseEntity<DispatchProductListResponse> fetchOrderProductDetailsForDelivery(
			@ModelAttribute("employeeId") long employeeId) {

		DispatchProductListResponse dispatchProductListResponse = new DispatchProductListResponse();
		HttpStatus httpStatus;
		System.out.println("Inside fetchOrderProductDetailsForDeliveryByEmployeeId");

		try {
			List<DispatchProductNameAndQtyListResponse> orderProductDetailsList = orderDetailsService
					.fetchOrderProductDetailsForDeliveryByEmpId(employeeId);

			if (orderProductDetailsList == null) {
				dispatchProductListResponse.setStatus(Constants.FAILURE_RESPONSE);
				dispatchProductListResponse.setErrorMsg("No Record Found");
				httpStatus = HttpStatus.OK;

			} else {
				EmployeeDetails employeeDetails = employeeDetailsService
						.getEmployeeDetailsByemployeeId(orderProductDetailsList.get(0).getEmployeeId());
				dispatchProductListResponse.setStatus(Constants.SUCCESS_RESPONSE);
				dispatchProductListResponse.setDispatchProductNameAndQtyListResponses(orderProductDetailsList);
				dispatchProductListResponse.setGateKeeperName(employeeDetails.getName());
				dispatchProductListResponse.setMobileNo(employeeDetails.getContact().getMobileNumber());
				httpStatus = HttpStatus.OK;

			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.out.println("fetchOrderProductDetailsForDeliveryByEmployeeId error:" + e.toString());
			dispatchProductListResponse.setStatus(Constants.FAILURE_RESPONSE);
			httpStatus = HttpStatus.FORBIDDEN;
		}
		return new ResponseEntity<DispatchProductListResponse>(dispatchProductListResponse, httpStatus);

	}

	// here Salesman confirm the Order for Delivery
	@GetMapping("/OrderConfirmForDelivery/{employeeId}")
	public ResponseEntity<BaseDomain> OrderConfirmForDelivery(@ModelAttribute("employeeId") long employeeId) {

		BaseDomain baseDomain = new BaseDomain();
		HttpStatus httpStatus;
		System.out.println("Inside OrderConfirmForDelivery");

		try {
			orderDetailsService.OrderConfirmForDelivery(employeeId);
			baseDomain.setStatus(Constants.SUCCESS_RESPONSE);
			baseDomain.setErrorMsg("Order is Confirm For Delivery");
			httpStatus = HttpStatus.OK;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.out.println("OrderConfirmForDelivery Error:" + e.toString());
			baseDomain.setStatus(Constants.FAILURE_RESPONSE);
			httpStatus = HttpStatus.FORBIDDEN;

		}
		return new ResponseEntity<BaseDomain>(baseDomain, httpStatus);
	}

	// here we fetching the orderList whose Delivery is pending or Done by their
	// status
	@GetMapping("/remainingOrderListAndDeliveredOrderedListByEmpId/{employeeId}/{status}")
	public ResponseEntity<OrderDetailsListResponse> remainingOrderListAndDeliveredOrderedListByEmpId(
			@ModelAttribute("employeeId") long employeeId, @ModelAttribute("status") String status) {

		OrderDetailsListResponse orderDetailsListResponse = new OrderDetailsListResponse();
		HttpStatus httpStatus;
		System.out.println("Inside remainingOrderListAndDeliveredOrderedListByEmpId");

		try {
			List<OrderDetails> orderDetailsList = orderDetailsService
					.remainingOrderListAndDeliveredOrderedListByEmpId(employeeId, status);
			if (orderDetailsList == null) {
				orderDetailsListResponse.setStatus(Constants.FAILURE_RESPONSE);
				httpStatus = HttpStatus.OK;
				orderDetailsListResponse.setErrorMsg("No Record Found");
			} else {
				orderDetailsListResponse.setStatus(Constants.SUCCESS_RESPONSE);
				httpStatus = HttpStatus.OK;
				orderDetailsListResponse.setOrderDetailsList(orderDetailsList);
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.out.println("remainingOrderListAndDeliveredOrderedListByEmpId Error:" + e.toString());
			orderDetailsListResponse.setStatus(Constants.FAILURE_RESPONSE);
			httpStatus = HttpStatus.FORBIDDEN;
		}
		return new ResponseEntity<OrderDetailsListResponse>(orderDetailsListResponse, httpStatus);
	}

	@PostMapping("/cancelOrder")
	public ResponseEntity<BaseDomain> cancelOrderByOrderId(@RequestBody CancelOrderRequest cancelOrderRequest) {

		BaseDomain baseDomain = new BaseDomain();
		HttpStatus httpStatus;
		System.out.println("Inside Cancel Order");

		try {
			OrderDetails orderDetails=orderDetailsService.fetchOrderDetailsByOrderId(cancelOrderRequest.getOrderId());
			if(orderDetails.getOrderStatus().getStatus().equals(Constants.ORDER_STATUS_DELIVERED)){
				baseDomain.setStatus(Constants.FAILURE_RESPONSE);
				httpStatus = HttpStatus.OK;
				baseDomain.setErrorMsg("Order is already canceled");
			}else{
				orderDetailsService.cancelOrder(cancelOrderRequest);
				baseDomain.setStatus(Constants.SUCCESS_RESPONSE);
				httpStatus = HttpStatus.OK;
			}
			
		} catch (Exception e) {
			// TODO Auto-generated catch block

			System.out.println("Error" + e.toString());
			baseDomain.setStatus(Constants.FAILURE_RESPONSE);
			httpStatus = HttpStatus.FORBIDDEN;
			e.printStackTrace();
		}
		return new ResponseEntity<BaseDomain>(baseDomain, httpStatus);

	}

	/*
	 * // here we fetching the orderList whose Delivery is Done
	 * 
	 * @GetMapping("/deliveredOrderedListByEmployeeId/{employeeId}") public
	 * ResponseEntity<OrderDetailsListResponse>
	 * deliveredOrderedList(@ModelAttribute("employeeId") long employeeId){
	 * 
	 * OrderDetailsListResponse orderDetailsListResponse=new
	 * OrderDetailsListResponse(); HttpStatus httpStatus;
	 * System.out.println("Inside deliveredOrderedListByEmployeeId");
	 * 
	 * try { List<OrderDetails>
	 * orderDetailsList=orderDetailsService.deliveredOrderedList(employeeId);
	 * if(orderDetailsList==null){
	 * orderDetailsListResponse.setStatus(Constants.FAILURE_RESPONSE);
	 * httpStatus=HttpStatus.OK;
	 * orderDetailsListResponse.setErrorMsg("No Record Found"); } else{
	 * orderDetailsListResponse.setStatus(Constants.SUCCESS_RESPONSE);
	 * httpStatus=HttpStatus.OK;
	 * orderDetailsListResponse.setOrderDetailsList(orderDetailsList); } } catch
	 * (Exception e) { // TODO Auto-generated catch block e.printStackTrace();
	 * System.out.println("deliveredOrderedListByEmployeeId Error:"+e.toString()
	 * ); orderDetailsListResponse.setStatus(Constants.FAILURE_RESPONSE);
	 * httpStatus=HttpStatus.FORBIDDEN; } return new
	 * ResponseEntity<OrderDetailsListResponse>(orderDetailsListResponse,
	 * httpStatus); }
	 */
	// here we are fetching OrderList For Reorder
	@GetMapping("/fetchOrderListForReorder/{employeeId}")
	public ResponseEntity<OrderDetailsListResponse> fetchOrderListForReorder(
			@ModelAttribute("employeeId") long employeeId) {

		OrderDetailsListResponse orderDetailsListResponse = new OrderDetailsListResponse();
		HttpStatus httpStatus;
		System.out.println("Inside fetchOrderListForReorder");

		try {
			List<OrderDetails> list = orderDetailsService.fetchOrderListForReorder(employeeId);
			if (list == null) {
				orderDetailsListResponse.setStatus(Constants.FAILURE_RESPONSE);
				httpStatus = HttpStatus.NO_CONTENT;

			} else {
				orderDetailsListResponse.setStatus(Constants.SUCCESS_RESPONSE);
				httpStatus = HttpStatus.OK;
				orderDetailsListResponse.setOrderDetailsList(list);
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.out.println("Inside fetchOrderListForReorder Error:" + e.toString());
			orderDetailsListResponse.setStatus(Constants.FAILURE_RESPONSE);
			httpStatus = HttpStatus.FORBIDDEN;
		}
		return new ResponseEntity<OrderDetailsListResponse>(orderDetailsListResponse, httpStatus);
	}

	// here we fatch boothList OrderDetails OrderProductDetailsList for ReOrder
	// 2ndScreen
	@GetMapping("/fetchBoothAndOrderDetailsAndOrderProductListForReOrder/{boothNo}/{orderId}")
	public ResponseEntity<OrderDetailsAndOrderProductListResponse> fetchBoothAndOrderDetailsAndOrderProductListForReOrder(
			@ModelAttribute("boothNo") String boothNo, @ModelAttribute("orderId") String orderId) {

		OrderDetailsAndOrderProductListResponse orderDetailsAndOrderProductListResponse = new OrderDetailsAndOrderProductListResponse();
		HttpStatus httpStatus;
		System.out.println(" Inside fetchBoothAndOrderDetailsAndOrderProductListForReOrder");

		try {
			Booth booth = boothService.fetchBoothByBoothNo(boothNo);
			if (booth == null) {
				orderDetailsAndOrderProductListResponse.setStatus(Constants.FAILURE_RESPONSE);
				httpStatus = HttpStatus.NO_CONTENT;
			} else {
				OrderDetails orderDetails = orderDetailsService.fetchOrderDetailsByOrderId(orderId);
				if (orderDetails == null) {
					orderDetailsAndOrderProductListResponse.setStatus(Constants.FAILURE_RESPONSE);
					httpStatus = HttpStatus.NO_CONTENT;
				} else {
					
					List<OrderProductDetails> orderProductDetails=orderDetailsService.fetchOrderProductDetailsByOrderId(orderId);
					List<ReOrderProductDetails> reOrderProductDetails=orderDetailsService.fecthReOrderProductDetailsByOrderId(orderId);
					List<OrderProductDetails> orderProductDetailList=new ArrayList<>();
					if(reOrderProductDetails==null)
					{
						orderProductDetailList.addAll(orderProductDetails);
					}
					else{
						orderProductDetailList = orderDetailsService
							.fetchOrderProductDetailsForReOrderByOrderId(orderProductDetails,reOrderProductDetails);
					}
					orderDetailsAndOrderProductListResponse.setStatus(Constants.SUCCESS_RESPONSE);
					httpStatus = HttpStatus.OK;
					orderDetailsAndOrderProductListResponse.setBooth(booth);
					orderDetailsAndOrderProductListResponse.setOrderDetails(orderDetails);
					orderDetailsAndOrderProductListResponse.setOrderProductDetailsList(orderProductDetailList);
				}

			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.out.println("Inside fetchBoothAndOrderDetailsAndOrderProductListForReOrder Error:" + e.toString());
			orderDetailsAndOrderProductListResponse.setStatus(Constants.FAILURE_RESPONSE);
			httpStatus = HttpStatus.FORBIDDEN;
		}

		return new ResponseEntity<OrderDetailsAndOrderProductListResponse>(orderDetailsAndOrderProductListResponse,
				httpStatus);

	}

	@GetMapping("/fetchDispatchProductListForSalesmanByEmpId/{employeeId}")
	public ResponseEntity<DispatchedOrderAndProductListResponse> fetchDispatchProductListForSalesman(
			@ModelAttribute("employeeId") long employeeId) {

		DispatchedOrderAndProductListResponse dispatchedOrderAndProductListResponse = new DispatchedOrderAndProductListResponse();
		HttpStatus httpStatus;
		System.out.println(" Inside fetchDispatchProductListForSalesmanByEmpId");

		Cluster cluster = areaService.fetchClusterBySalesmanId(employeeId);
		Employee employee = areaService.fetchGKEmployeeByClusterId(cluster.getClusterId());
		EmployeeDetails employeeDetails = employeeDetailsService
				.getEmployeeDetailsByemployeeId(employee.getEmployeeId());

		try {
			DispatchOrderDetails dispatchOrderDetails = orderDetailsService
					.fetchDispatchOrderForDeliveryByempId(employeeId);
			if (dispatchOrderDetails == null) {
				dispatchedOrderAndProductListResponse.setStatus(Constants.FAILURE_RESPONSE);
				httpStatus = HttpStatus.OK;
				dispatchedOrderAndProductListResponse.setErrorMsg("No Record Found");
				dispatchedOrderAndProductListResponse.setGateKeeperName(employeeDetails.getName());
				dispatchedOrderAndProductListResponse.setMobileNo(employeeDetails.getContact().getMobileNumber());

			} else {
				List<DispatchOrderProductDetails> dispatchOrderProductDetailsList = orderDetailsService
						.fetchDispatchOrderProductListByDispatchId(dispatchOrderDetails.getDispatchedId());

				dispatchedOrderAndProductListResponse.setStatus(Constants.SUCCESS_RESPONSE);
				httpStatus = HttpStatus.OK;
				dispatchedOrderAndProductListResponse.setGateKeeperName(employeeDetails.getName());
				dispatchedOrderAndProductListResponse.setMobileNo(employeeDetails.getContact().getMobileNumber());
				dispatchedOrderAndProductListResponse.setDispatchOrderDetails(dispatchOrderDetails);
				dispatchedOrderAndProductListResponse
						.setDispatchOrderProductDetailsList(dispatchOrderProductDetailsList);
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.out.println("Inside fetchDispatchProductListForSalesmanByEmpId Error " + e.toString());
			dispatchedOrderAndProductListResponse.setStatus(Constants.FAILURE_RESPONSE);
			httpStatus = HttpStatus.FORBIDDEN;
		}
		return new ResponseEntity<DispatchedOrderAndProductListResponse>(dispatchedOrderAndProductListResponse,
				httpStatus);

	}

	@GetMapping("/confirmDispatchProductListBySalesman/{employeeId}/{dispatchedId}")
	public ResponseEntity<BaseDomain> confirmDispatchProductListBySalesman(
			@ModelAttribute("employeeId") long employeeId, @ModelAttribute("dispatchedId") long dispatchedId) {

		BaseDomain baseDomain = new BaseDomain();
		HttpStatus httpStatus;
		System.out.println("Inside confirmDispatchProductListBySalesman");

		try {
			
			if(dispatchedId==0){
				baseDomain.setStatus(Constants.FAILURE_RESPONSE);
				httpStatus=HttpStatus.OK;
				baseDomain.setErrorMsg("No Record Found");
			}
			else{
			orderDetailsService.confirmOrderForDelivery(employeeId, dispatchedId);
			baseDomain.setStatus(Constants.SUCCESS_RESPONSE);
			httpStatus = HttpStatus.OK;
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.out.println("Inside confirmDispatchProductListBySalesman error:" + e.toString());
			baseDomain.setStatus(Constants.FAILURE_RESPONSE);
			httpStatus = HttpStatus.FORBIDDEN;
		}

		return new ResponseEntity<BaseDomain>(baseDomain, httpStatus);

	}
	/*@PostMapping("/confirmTheDeliveryOfReOrder")
	public ResponseEntity<BaseDomain> confirmTheDeliveryOfReOrder(@RequestBody ReOrderDeliveryRequest reOrderDeliveryRequest ){
		
		BaseDomain baseDomain=new BaseDomain();
		HttpStatus httpStatus;
		System.out.println("Inside confirmTheDeliveryOfReOrder");
		
		try {
			orderDetailsService.deliveryOfReOrderProduct(reOrderDeliveryRequest);
			baseDomain.setStatus(Constants.SUCCESS_RESPONSE);
			httpStatus=HttpStatus.OK;
		} catch (Exception e) {
			System.out.println("confirmTheDeliveryOfReOrder Error:"+e.toString());
			baseDomain.setStatus(Constants.FAILURE_RESPONSE);
			httpStatus=HttpStatus.FORBIDDEN;
		}
		return new ResponseEntity<BaseDomain>(baseDomain,httpStatus);
	}*/
	
	@PostMapping("/fetchDispatchOrderDetailForGkReports")
	public ResponseEntity<GkDispatchProductReportMainResponse> fetchDispatchOrderDetailForGkReports(@RequestBody GkDispatchProductReportRequest gkDispatchProductReportRequest){
		
		GkDispatchProductReportMainResponse gkDispatchProductReportResponse =new GkDispatchProductReportMainResponse();
		HttpStatus httpStatus;
		System.out.println("fetchDispatchOrderDetailForGkReports");
		
		try {
			List<GkDispatchProductReportResponse> gkDispatchProductReportResponses=orderDetailsService.fetchDispatchOrderDetailsListForGkReportByClusterIdAndDateRange(gkDispatchProductReportRequest);
			if(gkDispatchProductReportResponses==null){
				gkDispatchProductReportResponse.setStatus(Constants.FAILURE_RESPONSE);
				httpStatus=HttpStatus.OK;
				gkDispatchProductReportResponse.setErrorMsg("No Record found");
				
			}else{
				range=gkDispatchProductReportRequest.getRange();
				fromDate=gkDispatchProductReportRequest.getFromDate();
				toDate=gkDispatchProductReportRequest.getToDate();
				gkDispatchProductReportResponse.setStatus(Constants.SUCCESS_RESPONSE);
				gkDispatchProductReportResponse.setGkDispatchProductReportResponses(gkDispatchProductReportResponses);
				httpStatus=HttpStatus.OK;
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.out.println("fetchDispatchOrderDetailForGkReports Error:"+e.toString());
			gkDispatchProductReportResponse.setStatus(Constants.FAILURE_RESPONSE);
			httpStatus=HttpStatus.FORBIDDEN;
		}
		return new ResponseEntity<GkDispatchProductReportMainResponse>(gkDispatchProductReportResponse,httpStatus);
	}
	
	@GetMapping("/fetchDispatchProductDetailsForGkReports/{employeeId}")
	public ResponseEntity<GkProductDetailsReportMainResponse> fetchDispatchProductDetailsForGkReports(@ModelAttribute("employeeId") long employeeId){
		
		GkProductDetailsReportMainResponse gkProductDetailsReportMainResponse =new GkProductDetailsReportMainResponse();
		HttpStatus httpStatus;
		System.out.println("Inside fetchDispatchProductDetailsForGkReports");
		
		try {
			List<GkProductDetailsReportResponse> gkProductDetailsReportResponsesList=orderDetailsService.fetchDispatchProductDetailsListForGkReportByDateRange(employeeId, fromDate, toDate, range);
			
			EmployeeDetails employeeDetails=employeeDetailsService.getEmployeeDetailsByemployeeId(employeeId);
			
			List<Route> routes=new ArrayList<>();
			if(employeeDetails.getEmployee().getDepartment().getName().equals(Constants.AREA_SALESMAN_DEPT_NAME)){
				List<Area> areas=employeeDetailsService.fetchAreaListByEmployeeId(employeeId);
				List<Route> routes1=employeeDetailsService.fetchRouteListByAreaId(areas.get(0).getAreaId());
				routes.addAll(routes1);
			}
			else{
			List<Route> routes1=employeeDetailsService.fetchEmployeeRouteListByEmployeeId(employeeId);
			routes.addAll(routes1);
			}
			
			//Cluster cluster=employeeDetailsService.fetchClusterByEmployeeId(employeeId);
			Cluster cluster=areaService.fetchClusterBtRouteId(routes.get(0).getRouteId());
			
			gkProductDetailsReportMainResponse.setStatus(Constants.SUCCESS_RESPONSE);
			httpStatus=HttpStatus.OK;
			gkProductDetailsReportMainResponse.setSalesmanName(employeeDetails.getName());
			gkProductDetailsReportMainResponse.setMobileNo(employeeDetails.getContact().getMobileNumber());
			gkProductDetailsReportMainResponse.setRouteList(routes);
			gkProductDetailsReportMainResponse.setClusterName(cluster.getName());
			gkProductDetailsReportMainResponse.setGkProductDetailsReportResponses(gkProductDetailsReportResponsesList);
		} catch (Exception e) {
			System.out.println(" fetchDispatchProductDetailsForGkReports error:"+e.toString());
			gkProductDetailsReportMainResponse.setStatus(Constants.FAILURE_RESPONSE);
			httpStatus=HttpStatus.FORBIDDEN;
		}
		return new ResponseEntity<GkProductDetailsReportMainResponse>(gkProductDetailsReportMainResponse,httpStatus);
	}
	
	@GetMapping("/fetchClusterListByGkEmployeeId/{employeeId}")
	public ResponseEntity<ClusterResponse> fetchClusterListByGkEmployeeId(@ModelAttribute("employeeId") long employeeId){
		
		ClusterResponse clusterResponse=new ClusterResponse();
		HttpStatus httpStatus;
		System.out.println("fetchClusterListByGkEmployeeId");
		
		try {
			List<Cluster> clusters=employeeDetailsService.fetchClusterListByGkEmployeeId(employeeId);
			if(clusters==null){
				clusterResponse.setStatus(Constants.FAILURE_RESPONSE);
				httpStatus=HttpStatus.NO_CONTENT;
			}
			else{
				clusterResponse.setStatus(Constants.SUCCESS_RESPONSE);
				httpStatus=HttpStatus.OK;
				clusterResponse.setClusterList(clusters);
			}
		} catch (Exception e) {
		System.out.println("fetchClusterListByGkEmployeeId Error:"+e.toString());
		clusterResponse.setStatus(Constants.FAILURE_RESPONSE);
		httpStatus=HttpStatus.FORBIDDEN;
		}
		return new ResponseEntity<ClusterResponse>(clusterResponse,httpStatus);

	}
	
	@GetMapping("/updateDispatchProductReason/{dispatchedId}/{reason}")
	public ResponseEntity<BaseDomain> updateDispatchProductNotReceivingReason(@ModelAttribute("dispatchedId") long dispatchedId,@ModelAttribute("reason") String reason){
		
		BaseDomain baseDomain=new BaseDomain();
		HttpStatus httpStatus;
		System.out.println(" Inside updateDispatchProductNotReceivingReason");
		try {
			if(dispatchedId==0){
				baseDomain.setStatus(Constants.FAILURE_RESPONSE);
				httpStatus=HttpStatus.OK;
				baseDomain.setErrorMsg("No Product for Dispatched");
			}else{
				orderDetailsService.updateDispatchProductNotReceivingReason(dispatchedId, reason);
				baseDomain.setStatus(Constants.SUCCESS_RESPONSE);
				httpStatus=HttpStatus.OK;
			}
			
		} catch (Exception e) {
		System.out.println(" Inside updateDispatchProductNotReceivingReason Error:"+e.toString());
		baseDomain.setStatus(Constants.FAILURE_RESPONSE);
		httpStatus=HttpStatus.FORBIDDEN;
		}
		return new ResponseEntity<BaseDomain>(baseDomain,httpStatus);
	}
	
	/*// here we are fetching reorderProduct And ReOrderProductDetails By ReOrderId
	@GetMapping("/fetchReOrderDetailsAndReOrderProductDetailsListByReOrderId/{reOrderDetailsId}")
	public ResponseEntity<ReOrderDetailsAndProductDetailsRespose> fetchReOrderDetailsAndReOrderProductDetailsListByReOrderId(
			@ModelAttribute("reOrderDetailsId") String reOrderDetailsId) {

		ReOrderDetailsAndProductDetailsRespose reOrderDetailsAndProductDetailsRespose = new ReOrderDetailsAndProductDetailsRespose();

		HttpStatus httpStatus;
		System.out.println("Inside fetchOrderDetailsAndOrderProductDetailsListByOrderId");

		try {
			@SuppressWarnings("unused")
			ReOrderDetails reOrderDetails = orderDetailsService.fetchReOrderDetailsByReOrderId(reOrderDetailsId);
			if (orderDetails == null) {
				reOrderDetailsAndProductDetailsRespose.setStatus(Constants.FAILURE_RESPONSE);
				reOrderDetailsAndProductDetailsRespose.setErrorMsg("No Record Found");
				httpStatus = HttpStatus.OK;
			} else {
				List<ReOrderProductDetails> reOrderProductDetailsList = orderDetailsService
						.fetchReOrderProductDetailsByReOrderId(reOrderDetailsId);
				//String mobileNo = employeeDetailsService.fetchMobileNoByOrderId(orderDetails);
				reOrderDetailsAndProductDetailsRespose.setStatus(Constants.SUCCESS_RESPONSE);
				reOrderDetailsAndProductDetailsRespose.setReOrderDetails(reOrderDetails);
				reOrderDetailsAndProductDetailsRespose.setReOrderProductDetailsList(reOrderProductDetailsList);
				httpStatus = HttpStatus.OK;
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			reOrderDetailsAndProductDetailsRespose.setStatus(Constants.FAILURE_RESPONSE);
			reOrderDetailsAndProductDetailsRespose.setErrorMsg("Something went wrong");
			httpStatus = HttpStatus.FORBIDDEN;
		}
		return new ResponseEntity<ReOrderDetailsAndProductDetailsRespose>(reOrderDetailsAndProductDetailsRespose,
				httpStatus);

	}*/
}