package com.sheetalarch.dao;

import java.util.List;

import org.springframework.web.multipart.MultipartFile;

import com.sheetalarch.admin.models.ProductReportView;
import com.sheetalarch.admin.models.ProductViewList;
import com.sheetalarch.entity.Brand;
import com.sheetalarch.entity.Categories;
import com.sheetalarch.entity.Product;
import com.sheetalarch.entity.SupplierProductList;
import com.sheetalarch.models.BrandAndCategoryRequest;

public interface ProductDAO {

	public List<Brand> fetchBrandList();
	public List<Categories> fetchCategories();
	public List<Product> fetchProductByBrandAndCategory(BrandAndCategoryRequest brandAndCategoryRequest);	
	public List<Product> makeProductImageMNullorderProductDetailsList(List<Product> productList);
	public List<SupplierProductList> fetchProductListBySupplierIDAndBrandIdAndCategoryIdForApp(String supplierId,long categoryId, long brandId);
	public List<SupplierProductList> makeSupplierProductListImageMNullorderProductDetailsList(List<SupplierProductList> supplierProductList);
	public Product fetchProductByProductId(long productId);
	
	// for webapp
	public void saveProductForWebApp(MultipartFile file,Product product);
	public void saveMultipleProductForWebApp(List<Product> productList);
	public List<Product> fetchProductList();
	public List<ProductViewList> fetchProductViewListForWebApp();
	public Product fetchProductForWebApp(long productId);
	public List<Product> fetchProductListForWebApp();
	public void updateProductForWebApp(MultipartFile file,Product product);
	public List<ProductReportView> fetchProductListForReport(String range,String startDate,String endDate,String topProductNo);
	public List<Product> fetchProductListByBrandIdAndCategoryIdForWebApp(long categoryId, long brandId);
	public List<Product> fetchProductListByBrandIdAndCategoryIdForWebApp(String supplierId,long categoryId, long brandId);
	public List<Product> makeProductImageNull(List<Product> productList);
	public List<SupplierProductList> makeSupplierProductImageNull(List<SupplierProductList> supplierProductLists);
	public List<Product> fetchProductListBySupplierId(String supplierId);
	public List<Product> fetchProductByCategoryIdForWebApp(long categoryId);
	public void updateProductForWebApp(Product product);
	
}