package com.sheetalarch.dao;

import java.util.List;

import com.sheetalarch.entity.OrderDetails;
import com.sheetalarch.entity.Payment;
import com.sheetalarch.entity.ReOrderDetails;
import com.sheetalarch.entity.ReOrderProductDetails;
import com.sheetalarch.models.ASMMemberReportOrderListRequest;
import com.sheetalarch.models.CancelAsmReportResponse;

public interface ASMReportDAO {
	
	public List<CancelAsmReportResponse> fetchSmAndCancelOrderByAsmId(long employeeId);
	public List<OrderDetails> todaysCancelOrderListByEmployeeId(long employeeId);
	
	public List<ReOrderDetails> todaysReOrderListByEmployeeId(long employeeId);
	public List<CancelAsmReportResponse> fetchSmAndReOrderByAsmId(long employeeId);
	
	public ReOrderDetails fetchReOrderDetailsByReOrderId(String reOrderId);
	public List<ReOrderProductDetails> fetchReOrderProductDetailsByReOrderId(String reOrderId);
	
	public List<ReOrderProductDetails> makeReOrderProductDetailsListImageMNull(
			List<ReOrderProductDetails> reOrderProductDetails);
	public List<OrderDetails> fetchOrderDetailsListByEmpIdAndDateRangeforASMMemberReport(long employeeId,
			String fromDate, String toDate, String range);
	
	public List<Payment> fetchPaymentListByEmployeeIdAndDateRange(long employeeId,
			String fromDate, String toDate, String range);
	
	

}
