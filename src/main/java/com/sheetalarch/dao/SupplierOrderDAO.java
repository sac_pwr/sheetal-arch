package com.sheetalarch.dao;

import java.util.List;

import com.sheetalarch.entity.SupplierOrder;
import com.sheetalarch.entity.SupplierOrderDetails;


public interface SupplierOrderDAO {

	public void saveSupplierOrder(String productWithSupplier);
	public SupplierOrder fetchSupplierOrder(String supplierOrderId);
	public List<SupplierOrder> fetchSupplierOrderList();
	public List<SupplierOrderDetails> fetchSupplierOrderDetailsList(String supplierOrderId);
	public void editSupplierOrder(String productWithSupplier, SupplierOrder supplierOrder);
	public List<SupplierOrder> fetchSupplierOrders24hour(String range,String startDate,String endDate);
	public List<SupplierOrderDetails> fetchSupplierOrderDetailsListBySupplierOrderId(String supplierOrderId);
}
