/**
 * 
 */
package com.sheetalarch.dao;

import java.util.List;

import com.sheetalarch.entity.Cluster;
import com.sheetalarch.entity.Employee;

/**
 * @author aNKIT
 *
 */
public interface ClusterDAO {
	public Cluster fetchClusterBySalesmanId(long employeeId);
	public Employee fetchGKEmployeeByClusterId(long clusterId);
	public List<Cluster> fetchClusterList();
	public Cluster fetchClusterByClusterId(long clusterId);
	public void saveCluster(Cluster cluster);
	public void updateCluster(Cluster cluster);
}
