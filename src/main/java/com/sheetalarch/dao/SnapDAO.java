package com.sheetalarch.dao;

import java.util.List;

import com.sheetalarch.entity.DispatchOrderProductDetails;
import com.sheetalarch.models.GkSnapProductResponse;
import com.sheetalarch.models.SnapSalesmanResponse;

public interface SnapDAO {

	public SnapSalesmanResponse fetchSnapShotDetailsForSalemanByEmployeeId(long employeeId);
	public long noOfOrderTaken(long employeeId);
	public long noOfOrderDelivered(long employeeId);
	public long noOfOrderCanceled(long employeeId);
	public long noOfReOrder(long employeeId);
	public double totalCollection(long employeeId);
	public double totalCollectionOfReOrder(long employeeId);
	public long quantityReceived(long employeeId);
	public long quantityDelivered(long employeeId);
	public long reOrderQuantityDelivered(long employeeId) ;
	
	public List<DispatchOrderProductDetails> fetchDispatchedOrderProductDetailsByGkId(long employeeId);
	public List<GkSnapProductResponse> fetchSnapProductDetailsForGk(long employeeId);
	

}
