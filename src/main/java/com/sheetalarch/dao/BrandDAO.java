package com.sheetalarch.dao;

import java.util.List;

import com.sheetalarch.entity.Brand;

public interface BrandDAO {

	public void saveBrandForWebApp(Brand brand);
	public void updateBrandForWebApp(Brand brand);
	public Brand fetchBrandForWebApp(long brandId);
	public List<Brand> fetchBrandListForWebApp();	
}
