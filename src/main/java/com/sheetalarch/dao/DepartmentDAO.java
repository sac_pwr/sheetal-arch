/**
 * 
 */
package com.sheetalarch.dao;

import java.util.List;

import com.sheetalarch.entity.Department;

/**
 * @author aNKIT
 *
 */
public interface DepartmentDAO {

	
	//webApp
	public String saveDepartment(Department department);
	public String updateDepartment(Department department);
	public List<Department> fetchDepartmentList();
	public Department fetchDepartment(long departmentId);
}
