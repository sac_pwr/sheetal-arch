package com.sheetalarch.dao;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import com.sheetalarch.entity.Route;

public interface RouteDAO {


	
	
	//webApp
	public List<Route> fetchRouteByAreaId(long areaId);
	public List<Route> fetchRouteList();
	public void saveRoute(Route route);
	public void updateRoute(Route route);
	@Transactional
	public Route fetchRouteByRouteId(long routeId);
	
}
