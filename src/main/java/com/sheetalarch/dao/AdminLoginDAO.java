/**
 * 
 */
package com.sheetalarch.dao;

import com.sheetalarch.entity.AdminLogin;

/**
 * @author aNKIT
 *
 */
public interface AdminLoginDAO {

	public AdminLogin loginValidate(String userName,String password);
}
