package com.sheetalarch.dao;

import java.util.List;

import com.sheetalarch.entity.OrderDetails;

public interface ReOrderDAO {
	
	public List<OrderDetails> fetchOrderDetailsForReOrder(long employeeId);
	

}
