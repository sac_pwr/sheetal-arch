package com.sheetalarch.dao;

import java.util.List;

import com.sheetalarch.entity.Categories;

public interface CategoriesDAO {

	public void save(Categories categories);

	public void Update(Categories categories);

	public void delete(long id);

	public List<Categories> getCategoriesList();

	public List<Categories> getCategoriesListAdmin();

	public boolean getCategoryByNameBoolean(String name);

	public Categories getById(long id);
	
	//webapp

		public void saveCategoriesForWebApp(Categories categories);
		public void updateCategoriesForWebApp(Categories categories);
		public Categories fetchCategoriesForWebApp(long categoriesId);
		public List<Categories> fetchCategoriesListForWebApp();

}
