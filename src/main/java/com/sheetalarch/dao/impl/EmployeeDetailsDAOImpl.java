package com.sheetalarch.dao.impl;

import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.sheetalarch.admin.models.EmployeeAreaDetails;
import com.sheetalarch.admin.models.EmployeeHolidayList;
import com.sheetalarch.admin.models.EmployeeHolidayModel;
import com.sheetalarch.admin.models.EmployeePaymentModel;
import com.sheetalarch.admin.models.EmployeeSalaryModel;
import com.sheetalarch.admin.models.EmployeeSalaryStatus;
import com.sheetalarch.admin.models.EmployeeViewModel;
import com.sheetalarch.admin.models.SMListAssignToASMModel;
import com.sheetalarch.admin.models.SMReportDetails;
import com.sheetalarch.admin.models.SMReportModel;
import com.sheetalarch.admin.models.SMReportOrderDetails;
import com.sheetalarch.admin.models.SMReportOrders;
import com.sheetalarch.dao.EmployeeDetailsDAO;
import com.sheetalarch.entity.Area;
import com.sheetalarch.entity.Cluster;
import com.sheetalarch.entity.Employee;
import com.sheetalarch.entity.EmployeeASMAndSM;
import com.sheetalarch.entity.EmployeeBasicSalaryStatus;
import com.sheetalarch.entity.EmployeeDetails;
import com.sheetalarch.entity.EmployeeHolidays;
import com.sheetalarch.entity.EmployeeIncentives;
import com.sheetalarch.entity.EmployeeRouteList;
import com.sheetalarch.entity.EmployeeSalary;
import com.sheetalarch.entity.OrderDetails;
import com.sheetalarch.entity.Route;
import com.sheetalarch.utils.Constants;
import com.sheetalarch.utils.DatePicker;
import com.sheetalarch.utils.EmployeeDetailsIdGenerator;
import com.sheetalarch.utils.SendSMS;

@Repository("employeeDetailsDAO")
@Component
public class EmployeeDetailsDAOImpl implements EmployeeDetailsDAO {

	@Autowired
	SessionFactory sessionFactory;
	
	SimpleDateFormat simpleDateFormat=new SimpleDateFormat("yyyy/MM/dd");

	public EmployeeDetailsDAOImpl(SessionFactory sessionFactory) {
		// TODO Auto-generated constructor stub
		this.sessionFactory = sessionFactory;
	}


	@Transactional
	public EmployeeDetails getEmployeeDetailsByemployeeId(long employeeId) {

		String hql = "from EmployeeDetails where employee.employeeId=" + employeeId;
		Query query = sessionFactory.getCurrentSession().createQuery(hql);

		@SuppressWarnings("unchecked")
		List<EmployeeDetails> employeeDetailsList = (List<EmployeeDetails>) query.list();
		System.out.println("hevsh");
		if (employeeDetailsList.isEmpty()) {
			return null;
		}
		return employeeDetailsList.get(0);
	}

	@Transactional
	public EmployeeDetails getGateKeeperEmployeeDetailsByrouteId(long routeId) {

		String hql = "from EmployeeRouteList where route.routeId=" + routeId
				+ " and employeeDetails.employee.department.name='" + Constants.GATE_KEEPER_DEPT_NAME + "'";
		Query query = sessionFactory.getCurrentSession().createQuery(hql);
		@SuppressWarnings("unchecked")
		List<EmployeeRouteList> employeeAreaList = (List<EmployeeRouteList>) query.list();
		if (employeeAreaList.isEmpty()) {
			return null;
		}
		return employeeAreaList.get(0).getEmployeeDetails();
	}

	@Transactional
	public void updateEmployeeDetails(EmployeeDetails employeeDetails) {
		// TODO Auto-generated method stub
		ContactDAOImpl contactDAOImpl = new ContactDAOImpl(sessionFactory);
		contactDAOImpl.update(employeeDetails.getContact());

		sessionFactory.getCurrentSession().update(employeeDetails);

	}

	@Transactional
	public List<Route> fetchEmployeeRouteListByEmployeeId(long employeeId) {
		// TODO Auto-generated method stub

		String hql = "from EmployeeRouteList where employeeDetails.employee.employeeId=" + employeeId;
		Query query = sessionFactory.getCurrentSession().createQuery(hql);
		List<EmployeeRouteList> employeeRouteList = (List<EmployeeRouteList>) query.list();

		List<Route> routeList = new ArrayList<>();
		for (EmployeeRouteList employeeRouteList1 : employeeRouteList) {
			routeList.add(employeeRouteList1.getRoute());
		}
		if (routeList.isEmpty()) {
			return null;

		}

		return routeList;
	}

	@Transactional
	public List<Area> fetchAreaListByEmployeeId(long employeeId){
		
		String hql = "from EmployeeRouteList where employeeDetails.employee.employeeId=" + employeeId;
		Query query = sessionFactory.getCurrentSession().createQuery(hql);
		List<EmployeeRouteList> employeeRouteList = (List<EmployeeRouteList>) query.list();

		List<Area> areasList=new ArrayList<>();
		
		for (EmployeeRouteList employeeRouteList1 : employeeRouteList) {
			areasList.add(employeeRouteList1.getArea());
		}
		if (areasList.isEmpty()) {
			return null;

		}

		return areasList;
		

	}
	@Transactional
	public List<Cluster> fetchClusterList() {
		// TODO Auto-generated method stub

		Query query = sessionFactory.getCurrentSession().createQuery("from Cluster ");
		List<Cluster> clusterList = (List<Cluster>) query.list();
		if (clusterList.isEmpty()) {
			return null;
		}
		return clusterList;
	}

	@Override
	public List<EmployeeDetails> fetchASM() {
		String hql = "from EmployeeDetails where employee.department.name='" + Constants.AREA_SALESMAN_DEPT_NAME + "'";
		Query query = sessionFactory.getCurrentSession().createQuery(hql);
		List<EmployeeDetails> employeeDetailsList = (List<EmployeeDetails>) query.list();
		if (employeeDetailsList.isEmpty()) {
			return null;
		}
		return employeeDetailsList;
	}

	@Transactional
	public List<Employee> fetchSalesmanByASMId(long employeeId) {
		// TODO Auto-generated method stub

		String hql = "from EmployeeASMAndSM where employeeDetailsASMId.employee.employeeId=" + employeeId;
		Query query = sessionFactory.getCurrentSession().createQuery(hql);

		List<EmployeeASMAndSM> employeeASMAndSMList = (List<EmployeeASMAndSM>) query.list();

		List<Employee> employeeList = new ArrayList<>();
		for (EmployeeASMAndSM employeeASMAndSM : employeeASMAndSMList) {
			employeeList.add(employeeASMAndSM.getEmployeeDetailsSMId().getEmployee());
		}
		if (employeeList.isEmpty()) {
			return null;
		}
		return employeeList;
	}

	// here we are fetching list of employeeName by orderDetails
	@Transactional
	public String fetchEmployeeNameByOrderDetails(OrderDetails orderDetails) {

		String hql = "From EmployeeDetails where employee.employeeId="
				+ orderDetails.getEmployeeASMandSM().getEmployeeId();
		Query query = sessionFactory.getCurrentSession().createQuery(hql);
		List<EmployeeDetails> employeeDetailsList = (List<EmployeeDetails>) query.list();

		if (employeeDetailsList.isEmpty()) {
			return null;

		} else {
			return employeeDetailsList.get(0).getName();
		}

	}

	@Transactional
	public String fetchMobileNoByOrderId(OrderDetails orderDetails) {

		String hql = "From EmployeeDetails where employee.employeeId="
				+ orderDetails.getEmployeeASMandSM().getEmployeeId();
		Query query = sessionFactory.getCurrentSession().createQuery(hql);
		List<EmployeeDetails> employeeDetailsList = (List<EmployeeDetails>) query.list();

		if (employeeDetailsList.isEmpty()) {
			return null;

		} else {
			return employeeDetailsList.get(0).getContact().getMobileNumber();
		}

	}

	@Transactional
	public Cluster fetchClusterByEmployeeId(long employeeId) {
		// TODO Auto-generated method stub

		String hql = "From EmployeeRouteList where employeeDetails.employee.employeeId=" + employeeId;
		Query query = sessionFactory.getCurrentSession().createQuery(hql);
		List<EmployeeRouteList> employeeRouteLists = (List<EmployeeRouteList>) query.list();

		List<Cluster> clusters = new ArrayList<>();
		for (EmployeeRouteList list : employeeRouteLists) {
			clusters.add(list.getCluster());
		}
		if (clusters.isEmpty()) {
			return null;
		}
		return clusters.get(0);
	}

	@Transactional
	public List<Cluster> fetchClusterListByGkEmployeeId(long employeeId) {
		// TODO Auto-generated method stub
		String hql = "From EmployeeRouteList where employeeDetails.employee.employeeId=" + employeeId;
		Query query = sessionFactory.getCurrentSession().createQuery(hql);
		List<EmployeeRouteList> employeeRouteLists = (List<EmployeeRouteList>) query.list();

		List<Cluster> clusters = new ArrayList<>();
		for (EmployeeRouteList list : employeeRouteLists) {
			clusters.add(list.getCluster());
		}
		if (clusters.isEmpty()) {
			return null;
		}
		return clusters;

	}
	
	//webApp start
	@Transactional
	public EmployeeDetails fetchEmployeeDetailsForWebApp(long employeeDetailsId) {
		String hql = "from EmployeeDetails where employeeDetailsId='" + employeeDetailsId+"'";
		Query query = sessionFactory.getCurrentSession().createQuery(hql);
		@SuppressWarnings("unchecked")
		List<EmployeeDetails> employeeDetailsList = (List<EmployeeDetails>) query.list();
		if (employeeDetailsList.isEmpty()) {
			return null;
		}
		return employeeDetailsList.get(0);
	}
	/* (non-Javadoc)
	 * @see com.sheetalarch.dao.EmployeeDetailsDAO#saveForWebApp(com.sheetalarch.entity.EmployeeDetails)
	 * Nov 29, 201710:54:54 AM
	 */
	@Transactional
	public void saveForWebApp(EmployeeDetails employeeDetails) {
		// TODO Auto-generated method stub
		ContactDAOImpl contactDAOImpl=new ContactDAOImpl(sessionFactory);
		contactDAOImpl.save(employeeDetails.getContact());
		
		EmployeeDetailsIdGenerator employeeDetailsIdGenerator=new EmployeeDetailsIdGenerator(sessionFactory);
		employeeDetails.setEmployeeDetailsGenId(employeeDetailsIdGenerator.generateEmployeeDetailsId(employeeDetails.getEmployee().getDepartment()));
		sessionFactory.getCurrentSession().save(employeeDetails);
	}

	/* (non-Javadoc)
	 * @see com.sheetalarch.dao.EmployeeDetailsDAO#updateForWebApp(com.sheetalarch.entity.EmployeeDetails)
	 * Nov 29, 201710:54:54 AM
	 */
	@Transactional
	public void updateForWebApp(EmployeeDetails employeeDetails) {
		// TODO Auto-generated method stub
		ContactDAOImpl contactDAOImpl=new ContactDAOImpl(sessionFactory);
		contactDAOImpl.update(employeeDetails.getContact());
		
		EmployeeDetailsIdGenerator employeeDetailsIdGenerator=new EmployeeDetailsIdGenerator(sessionFactory);
		employeeDetails.setEmployeeDetailsGenId(employeeDetailsIdGenerator.generateEmployeeDetailsId(employeeDetails.getEmployee().getDepartment()));
		employeeDetails=(EmployeeDetails)sessionFactory.getCurrentSession().merge(employeeDetails);
		sessionFactory.getCurrentSession().update(employeeDetails);
	}

	@Transactional
	public List<EmployeeRouteList> fetchEmployeeRouteListByEmployeeDetailsId(long employeeDetailsId) {

		String hql="from EmployeeRouteList where employeeDetails.employeeDetailsId='"+employeeDetailsId+"'";
		Query query=sessionFactory.getCurrentSession().createQuery(hql);
		List<EmployeeRouteList> list=(List<EmployeeRouteList>)query.list();
		
		if(list.isEmpty())
		{
			return null;
		}
		
		return list;
	}
	
	/* (non-Javadoc)
	 * @see com.sheetalarch.dao.EmployeeDetailsDAO#saveEmployeeOldBasicSalary(com.sheetalarch.entity.EmployeeBasicSalaryStatus)
	 * Nov 29, 201710:54:54 AM
	 */
	@Transactional
	public void saveEmployeeOldBasicSalary(EmployeeBasicSalaryStatus employeeOldBasicSalary)
	{
		sessionFactory.getCurrentSession().save(employeeOldBasicSalary);
	}
	
	

	/* (non-Javadoc)
	 * @see com.sheetalarch.dao.EmployeeDetailsDAO#updateEmployeeOldBasicSalary(com.sheetalarch.entity.EmployeeBasicSalaryStatus)
	 * Nov 29, 201710:54:54 AM
	 */
	@Transactional
	public void updateEmployeeOldBasicSalary(EmployeeBasicSalaryStatus employeeOldBasicSalary)
	{
		sessionFactory.getCurrentSession().update(employeeOldBasicSalary);
	}

	@Transactional
	public SMListAssignToASMModel saveEmployeeAreasForWebApp(Long cluster[],long areaId,long routeId,String deptType, long employeeDetailsId) {

		
		if(deptType.equals(Constants.GATE_KEEPER_DEPT_NAME))
		{
			for(int i=0; i<cluster.length; i++)
			{
				EmployeeRouteList employeeRouteList=new EmployeeRouteList();
				Cluster cluster2=new Cluster();
				cluster2.setClusterId(cluster[i]);
				employeeRouteList.setCluster(cluster2);
				EmployeeDetails employeeDetails=fetchEmployeeDetailsForWebApp(employeeDetailsId);
				employeeRouteList.setEmployeeDetails(employeeDetails);					
				sessionFactory.getCurrentSession().save(employeeRouteList);
			}
			
			return null;
		}
		else if(deptType.equals(Constants.AREA_SALESMAN_DEPT_NAME))
		{
			EmployeeRouteList employeeRouteList=new EmployeeRouteList();
			Area area=new Area();
			area.setAreaId(areaId);
			employeeRouteList.setArea(area);
			EmployeeDetails employeeDetails=fetchEmployeeDetailsForWebApp(employeeDetailsId);
			employeeRouteList.setEmployeeDetails(employeeDetails);					
			sessionFactory.getCurrentSession().save(employeeRouteList);
						
			return new SMListAssignToASMModel(areaId, 0, employeeDetails);
		}
		else
		{
			EmployeeRouteList employeeRouteList=new EmployeeRouteList();
			Route route=new Route();
			route.setRouteId(routeId);
			employeeRouteList.setRoute(route);
			EmployeeDetails employeeDetails=fetchEmployeeDetailsForWebApp(employeeDetailsId);
			employeeRouteList.setEmployeeDetails(employeeDetails);					
			sessionFactory.getCurrentSession().save(employeeRouteList);
			
			return new SMListAssignToASMModel(0, routeId, employeeDetails);
		}
		
	}
	
	@Transactional
	public List<EmployeeDetails> fetchEmployeeDetailsListByAreaId(long areaId)
	{
		String hql="from EmployeeRouteList where route.area.areaId="+areaId;
		Query query=sessionFactory.getCurrentSession().createQuery(hql);
		List<EmployeeRouteList> employeeRouteLists=(List<EmployeeRouteList>)query.list();
		if(employeeRouteLists.isEmpty())
		{
			return null;
		}
		
		List<EmployeeDetails> employeeDetailsList=new ArrayList<>();
		for(EmployeeRouteList employeeRouteList: employeeRouteLists)
		{
			boolean addBool=false;
			for(EmployeeDetails employeeDetails : employeeDetailsList)
			{
				if(employeeDetails.getEmployeeDetailsId()==employeeRouteList.getEmployeeDetails().getEmployeeDetailsId())
				{
					addBool=true;
				}
			}
			
			if(addBool==false)
			{
				employeeDetailsList.add(employeeRouteList.getEmployeeDetails());
			}
		}
		return employeeDetailsList;
	}
	
	@Transactional
	public EmployeeDetails deleteASMFromEmployeeASMAndSMListByAreaId(long areaId)
	{
		Query query1=sessionFactory.getCurrentSession().createQuery("from EmployeeRouteList where area.areaId="+areaId);
		List<EmployeeRouteList> employeeRouteList=(List<EmployeeRouteList>)query1.list();
		if(!employeeRouteList.isEmpty())
		{
			Query query=sessionFactory.getCurrentSession().createQuery("delete from EmployeeASMAndSM where employeeDetailsASMId.employeeDetailsId="+employeeRouteList.get(0).getEmployeeDetails().getEmployeeDetailsId());  
			query.executeUpdate();
			return employeeRouteList.get(0).getEmployeeDetails();
		}
		return null;
	}
	
	@Transactional
	public SMListAssignToASMModel updateEmployeeRoutesForWebApp(Long cluster[],long areaId,long routeId,String deptType, long employeeDetailsId) {

		String hql="from EmployeeRouteList where employeeDetails.employeeDetailsId='"+employeeDetailsId+"'";
		Query query=sessionFactory.getCurrentSession().createQuery(hql);	
		
		List<EmployeeRouteList> list=query.list();
		Iterator<EmployeeRouteList> itr=list.iterator();
		while(itr.hasNext())
		{
			sessionFactory.getCurrentSession().delete(itr.next());
		}		
		
		if(deptType.equals(Constants.GATE_KEEPER_DEPT_NAME))
		{
			for(int i=0; i<cluster.length; i++)
			{
				EmployeeRouteList employeeRouteList=new EmployeeRouteList();
				Cluster cluster2=new Cluster();
				cluster2.setClusterId(cluster[i]);
				employeeRouteList.setCluster(cluster2);
				EmployeeDetails employeeDetails=fetchEmployeeDetailsForWebApp(employeeDetailsId);
				employeeRouteList.setEmployeeDetails(employeeDetails);					
				sessionFactory.getCurrentSession().save(employeeRouteList);
			}
			return null;
		}
		else if(deptType.equals(Constants.AREA_SALESMAN_DEPT_NAME))
		{
			EmployeeRouteList employeeRouteList=new EmployeeRouteList();
			Area area=new Area();
			area.setAreaId(areaId);
			employeeRouteList.setArea(area);
			EmployeeDetails employeeDetails=fetchEmployeeDetailsForWebApp(employeeDetailsId);
			employeeRouteList.setEmployeeDetails(employeeDetails);				
			sessionFactory.getCurrentSession().save(employeeRouteList);
			
			return new SMListAssignToASMModel(areaId, 0, employeeDetails);
		}
		else
		{
			EmployeeRouteList employeeRouteList=new EmployeeRouteList();
			Route route=new Route();
			route.setRouteId(routeId);
			employeeRouteList.setRoute(route);
			EmployeeDetails employeeDetails=fetchEmployeeDetailsForWebApp(employeeDetailsId);
			employeeRouteList.setEmployeeDetails(employeeDetails);				
			sessionFactory.getCurrentSession().save(employeeRouteList);
						
			return new SMListAssignToASMModel(0, routeId, employeeDetails);
		}
	}
	
	@Transactional
	public void updateSM_List_To_ASM_ForWebApp(SMListAssignToASMModel sMListAssignToASMModel) {

		if(sMListAssignToASMModel==null)
		{
			return;
		}
		
		String deptType=sMListAssignToASMModel.getEmployeeDetails().getEmployee().getDepartment().getName();
		long areaId=sMListAssignToASMModel.getAreaId();
		long routeId=sMListAssignToASMModel.getRouteId();
		
		
		if(deptType.equals(Constants.AREA_SALESMAN_DEPT_NAME))
		{
			EmployeeDetails employeeDetails2=deleteASMFromEmployeeASMAndSMListByAreaId(areaId);
			if(employeeDetails2!=null)
			{
				List<EmployeeDetails> employeeDetailsList=fetchEmployeeDetailsListByAreaId(areaId);
				if(employeeDetailsList!=null)
				{
					for(EmployeeDetails employeeDetailsSM : employeeDetailsList)
					{
						EmployeeASMAndSM employeeASMAndSM=new EmployeeASMAndSM();
						employeeASMAndSM.setEmployeeDetailsASMId(employeeDetails2);
						employeeASMAndSM.setEmployeeDetailsSMId(employeeDetailsSM);
						sessionFactory.getCurrentSession().save(employeeASMAndSM);
					}
				}
			}
		}
		else
		{
			RouteDAOImpl routeDAOImpl=new RouteDAOImpl(sessionFactory);
			Route route=routeDAOImpl.fetchRouteByRouteId(routeId);
			
			EmployeeDetails employeeDetails2=deleteASMFromEmployeeASMAndSMListByAreaId(route.getArea().getAreaId());
			if(employeeDetails2!=null)
			{
				List<EmployeeDetails> employeeDetailsList=fetchEmployeeDetailsListByAreaId(route.getArea().getAreaId());
				if(employeeDetailsList!=null)
				{
					for(EmployeeDetails employeeDetailsSM : employeeDetailsList)
					{
						EmployeeASMAndSM employeeASMAndSM=new EmployeeASMAndSM();
						employeeASMAndSM.setEmployeeDetailsASMId(employeeDetails2);
						employeeASMAndSM.setEmployeeDetailsSMId(employeeDetailsSM);
						sessionFactory.getCurrentSession().save(employeeASMAndSM);
					}
				}
			}
		}
	}

	@Transactional
	public EmployeeBasicSalaryStatus fetchLastEmployeeOldBasicSalaryByEmployeeOldBasicSalaryId(long employeeDetailsId)
	{
		String hql="from EmployeeBasicSalaryStatus where employeeDetails.employeeDetailsId='"+employeeDetailsId+"' order by employeeBasicSalaryStatusId desc";
		Query query=sessionFactory.getCurrentSession().createQuery(hql);
		List<EmployeeBasicSalaryStatus> employeeOldBasicSalaryList=(List<EmployeeBasicSalaryStatus>)query.list();
		if(employeeOldBasicSalaryList.isEmpty())
		{
			return null;
		}
		return employeeOldBasicSalaryList.get(0);
	}
	
	
	@Transactional
	public List<EmployeeViewModel> fetchEmployeeDetailsForView() {
		// TODO Auto-generated method stub
		String hql = "from EmployeeDetails";
		Query query = sessionFactory.getCurrentSession().createQuery(hql);
		@SuppressWarnings("unchecked")
		List<EmployeeDetails> employeeList = (List<EmployeeDetails>) query.list();

		if (employeeList.isEmpty()) {
			return null;
		}
		
		List<EmployeeViewModel> list=new ArrayList<>();
		Iterator<EmployeeDetails> itr=employeeList.iterator();
		int srno=0;
		while(itr.hasNext())
		{
			EmployeeDetails employeeDetails=itr.next();
			double paidCurrentMonthSalary=getPaidSalary(DatePicker.getCurrentMonthStartDate(), DatePicker.getCurrentMonthLastDate(), "CurrentMonth", employeeDetails);
			long noOfHolidays=getNoOfHolidays(DatePicker.getCurrentMonthStartDate(), DatePicker.getCurrentMonthLastDate(), "CurrentMonth", employeeDetails);
			double incentives=getIncentivesForWebApp(DatePicker.getCurrentMonthStartDate(), DatePicker.getCurrentMonthLastDate(), "CurrentMonth", employeeDetails );
			double deduction=getDeductionAmount(DatePicker.getCurrentMonthStartDate(), DatePicker.getCurrentMonthLastDate(), "CurrentMonth", employeeDetails );
			double totalAmount=findTotalSalaryTwoDates(DatePicker.getCurrentMonthStartDate(), DatePicker.getCurrentMonthLastDate(), "CurrentMonth", employeeDetails)+incentives-deduction;
			double unpaidCurrentMonthSalary=totalAmount-paidCurrentMonthSalary;
			srno++;
			list.add(new EmployeeViewModel(
					srno,
					employeeDetails.getEmployeeDetailsId(),
					employeeDetails.getEmployeeDetailsGenId(), 
					employeeDetails.getName(), 
					employeeDetails.getContact().getMobileNumber(), 
					employeeDetails.getContact().getEmailId(), 
					totalAmount,
					paidCurrentMonthSalary, 
					unpaidCurrentMonthSalary, 
					incentives,
					(int)noOfHolidays, 
					employeeDetails.getEmployee().getDepartment().getName()
					,employeeDetails.isStatus()));
			
		}
		
		return list;
	}
	
	private double getPaidSalary(String startDate, String endDate,
			String range, EmployeeDetails employeeDetails) {
		try {
			double paidCurrentMonthSalary=0;
			long employeeDetailsId=employeeDetails.getEmployeeDetailsId();
			
			SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
			String hql = "";
			Query query = null;
			Calendar cal = Calendar.getInstance();  //Get current date/month i.e 27 Feb, 2012
			Calendar regDate = Calendar.getInstance();
			regDate.setTime(employeeDetails.getEmployeeDetailsAddedDatetime());
			if (range.equals("Last6Months")) {
				cal.add(Calendar.MONTH, -6);
				if(regDate.compareTo(cal)<0)
				{
					hql = "from EmployeeSalary where date(payingDate) >= '"+dateFormat.format(cal.getTime())+"' and employeeDetails.employeeDetailsId='"+employeeDetailsId+"'";
				}
				else
				{
					hql = "from EmployeeSalary where date(payingDate) >= '"+dateFormat.format(regDate.getTime())+"' and employeeDetails.employeeDetailsId='"+employeeDetailsId+"'";
				}
				query = sessionFactory.getCurrentSession().createQuery(hql);

			}
			else if (range.equals("Last1Year")) {
				cal.add(Calendar.MONTH, -12);
				if(regDate.compareTo(cal)<0)
				{
					hql = "from EmployeeSalary where date(payingDate) >= '"+dateFormat.format(cal.getTime())+"' and employeeDetails.employeeDetailsId='"+employeeDetailsId+"'";
				}
				else
				{
					hql = "from EmployeeSalary where date(payingDate) >= '"+dateFormat.format(regDate.getTime())+"' and employeeDetails.employeeDetailsId='"+employeeDetailsId+"'";
				}
				query = sessionFactory.getCurrentSession().createQuery(hql);

			}
			
			else if (range.equals("ViewAll")) {

				hql = "from EmployeeSalary where employeeDetails.employeeDetailsId='"+employeeDetailsId+"'";

				query = sessionFactory.getCurrentSession().createQuery(hql);

			}
			else if (range.equals("CurrentMonth")) {
				cal.setTime(dateFormat.parse(DatePicker.getCurrentMonthStartDate()));
				if(regDate.compareTo(cal)<0)
				{
					hql="from EmployeeSalary where date(payingDate) >= '"+dateFormat.format(cal.getTime())+"' and date(payingDate) <='"+DatePicker.getCurrentMonthLastDate()+"' and employeeDetails.employeeDetailsId='"+employeeDetailsId+"'";
				}
				else
				{
					hql="from EmployeeSalary where date(payingDate) >= '"+dateFormat.format(regDate.getTime())+"' and date(payingDate) <='"+DatePicker.getCurrentMonthLastDate()+"' and employeeDetails.employeeDetailsId='"+employeeDetailsId+"'";
				}
				query = sessionFactory.getCurrentSession().createQuery(hql);

			}
			else if (range.equals("Range")) {
				cal.setTime(dateFormat.parse(startDate));
				if(regDate.compareTo(cal)<0)
				{
					hql="from EmployeeSalary where date(payingDate) >= '"+startDate+"' and date(payingDate) <='"+endDate+"' and employeeDetails.employeeDetailsId='"+employeeDetailsId+"'";
				}
				else
				{
					hql="from EmployeeSalary where date(payingDate) >= '"+dateFormat.format(regDate.getTime())+"' and date(payingDate) <='"+endDate+"' and employeeDetails.employeeDetailsId='"+employeeDetailsId+"'";
				}
				query = sessionFactory.getCurrentSession().createQuery(hql);

			}
			
			List<EmployeeSalary> list=(List<EmployeeSalary>)query.list();
			if(list.isEmpty())
			{
				return 0;
			}
			Iterator<EmployeeSalary> itr=list.iterator();
			while(itr.hasNext())
			{
				EmployeeSalary employeeSalary=itr.next();
				paidCurrentMonthSalary=paidCurrentMonthSalary+employeeSalary.getPayingAmount();
			}
			return paidCurrentMonthSalary;
		} catch (HibernateException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return 0;
	}
	
	private long getNoOfHolidays(String startDate, String endDate,
			String range, EmployeeDetails employeeDetails) {
		try {
			long noOfHolidays=0;
			long employeeDetailsId=employeeDetails.getEmployeeDetailsId();
			SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
			String hql = "";
			Query query = null;
			Calendar cal = Calendar.getInstance();  //Get current date/month i.e 27 Feb, 2012
			Calendar regDate = Calendar.getInstance();
			regDate.setTime(employeeDetails.getEmployeeDetailsAddedDatetime());
			
			if (range.equals("Last6Months")) {
				cal.add(Calendar.MONTH, -6);
				
				if(regDate.compareTo(cal)<0)
				{
					hql = "from EmployeeHolidays where date(givenHolidayDate) >= '"+dateFormat.format(cal.getTime())+"' and employeeDetails.employeeDetailsId='"+employeeDetailsId+"'";
				}
				else
				{
					hql = "from EmployeeHolidays where date(givenHolidayDate) >= '"+dateFormat.format(regDate.getTime())+"' and employeeDetails.employeeDetailsId='"+employeeDetailsId+"'";
				}

				query = sessionFactory.getCurrentSession().createQuery(hql);

			}
			else if (range.equals("Last1Year")) {
				cal.add(Calendar.MONTH, -12);
				if(regDate.compareTo(cal)<0)
				{
					hql = "from EmployeeHolidays where date(givenHolidayDate) >= '"+dateFormat.format(cal.getTime())+"' and employeeDetails.employeeDetailsId='"+employeeDetailsId+"'";
				}
				else
				{
					hql = "from EmployeeHolidays where date(givenHolidayDate) >= '"+dateFormat.format(regDate.getTime())+"' and employeeDetails.employeeDetailsId='"+employeeDetailsId+"'";
				}
				query = sessionFactory.getCurrentSession().createQuery(hql);

			}
			
			else if (range.equals("ViewAll")) {

				hql = "from EmployeeHolidays where employeeDetails.employeeDetailsId='"+employeeDetailsId+"'";

				query = sessionFactory.getCurrentSession().createQuery(hql);

			}
			else if (range.equals("CurrentMonth")) {
				cal.setTime(dateFormat.parse(DatePicker.getCurrentMonthStartDate()));

				if(regDate.compareTo(cal)<0)
				{
					hql="from EmployeeHolidays where date(givenHolidayDate) >= '"+dateFormat.format(cal.getTime())+"' and date(givenHolidayDate) <='"+DatePicker.getCurrentMonthLastDate()+"' and employeeDetails.employeeDetailsId='"+employeeDetailsId+"'";
				}
				else
				{
					hql="from EmployeeHolidays where date(givenHolidayDate) >= '"+dateFormat.format(regDate.getTime())+"' and date(givenHolidayDate) <='"+DatePicker.getCurrentMonthLastDate()+"' and employeeDetails.employeeDetailsId='"+employeeDetailsId+"'";
				}
				query = sessionFactory.getCurrentSession().createQuery(hql);

			}
			else if (range.equals("Range")) {
				cal.setTime(dateFormat.parse(startDate));
				if(regDate.compareTo(cal)<0)
				{
					hql="from EmployeeHolidays where date(givenHolidayDate) >= '"+startDate+"' and date(givenHolidayDate) <='"+endDate+"' and employeeDetails.employeeDetailsId='"+employeeDetailsId+"'";
				}
				else
				{
					hql="from EmployeeHolidays where date(givenHolidayDate) >= '"+dateFormat.format(regDate.getTime())+"' and date(givenHolidayDate) <='"+endDate+"' and employeeDetails.employeeDetailsId='"+employeeDetailsId+"'";
				}
				query = sessionFactory.getCurrentSession().createQuery(hql);

			}
			
			
			List<EmployeeHolidays> list=(List<EmployeeHolidays>)query.list();
			if(list.isEmpty())
			{
				return 0;
			}
			Iterator<EmployeeHolidays> itr=list.iterator();
			while(itr.hasNext())
			{
				EmployeeHolidays employeeHolidays=itr.next();
				Date from=employeeHolidays.getFromDate();			
				Date to;

				Calendar cEnd = Calendar.getInstance();
				Calendar cStart = Calendar.getInstance();
				long diff;
				if(employeeHolidays.getToDate()!=null)
				{
					to=employeeHolidays.getToDate();
					cStart.setTime(from);
					 cEnd.setTime(to);
					 diff = cEnd.getTime().getTime() - cStart.getTime().getTime();
					noOfHolidays=noOfHolidays+TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS)+1;
				}
				else
				{				
					noOfHolidays=noOfHolidays+1;
				}    
			}
			return noOfHolidays;
		} catch (HibernateException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return 0;
	}
	
	@Transactional
	public double getIncentivesForWebApp(String startDate, String endDate,
			String range, EmployeeDetails employeeDetails){
		
		try {
			long employeeDetailsId=employeeDetails.getEmployeeDetailsId();
			
			SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
			String hql = "";
			Query query = null;
			Calendar cal = Calendar.getInstance();  //Get current date/month i.e 27 Feb, 2012
			Calendar regDate = Calendar.getInstance();
			regDate.setTime(employeeDetails.getEmployeeDetailsAddedDatetime());
			if (range.equals("Last6Months")) {
				cal.add(Calendar.MONTH, -6);
				if(regDate.compareTo(cal)<0)
				 {	
					hql = "from EmployeeIncentives where date(incentiveGivenDate) >= '"+dateFormat.format(cal.getTime())+"' and employeeDetails.employeeDetailsId='"+employeeDetailsId+"'";
				 }
				else
				{
					hql = "from EmployeeIncentives where date(incentiveGivenDate) >= '"+dateFormat.format(regDate.getTime())+"' and employeeDetails.employeeDetailsId='"+employeeDetailsId+"'";
				}
				query = sessionFactory.getCurrentSession().createQuery(hql);
			}
			else if (range.equals("Last1Year")) {
				cal.add(Calendar.MONTH, -12);
				if(regDate.compareTo(cal)<0)
				 {	
					hql = "from EmployeeIncentives where date(incentiveGivenDate) >= '"+dateFormat.format(cal.getTime())+"' and employeeDetails.employeeDetailsId='"+employeeDetailsId+"'";
				 }
				else
				{
					hql = "from EmployeeIncentives where date(incentiveGivenDate) >= '"+dateFormat.format(regDate.getTime())+"' and employeeDetails.employeeDetailsId='"+employeeDetailsId+"'";
				}
				query = sessionFactory.getCurrentSession().createQuery(hql);
			}		
			else if (range.equals("ViewAll")) {
				hql = "from EmployeeIncentives where employeeDetails.employeeDetailsId='"+employeeDetailsId+"'";
				query = sessionFactory.getCurrentSession().createQuery(hql);
			}
			else if (range.equals("CurrentMonth")) {
				cal.setTime(dateFormat.parse(DatePicker.getCurrentMonthStartDate()));
				if(regDate.compareTo(cal)<0)
				 {
					hql="from EmployeeIncentives where date(incentiveGivenDate) >= '"+DatePicker.getCurrentMonthStartDate()+"' and date(incentiveGivenDate) <='"+DatePicker.getCurrentMonthLastDate()+"' and employeeDetails.employeeDetailsId='"+employeeDetailsId+"'";
				 }
				else
				{
					hql="from EmployeeIncentives where date(incentiveGivenDate) >= '"+dateFormat.format(regDate.getTime())+"' and date(incentiveGivenDate) <='"+DatePicker.getCurrentMonthLastDate()+"' and employeeDetails.employeeDetailsId='"+employeeDetailsId+"'";
				}
				query = sessionFactory.getCurrentSession().createQuery(hql);
			}
			else if (range.equals("Range")) {
				cal.setTime(dateFormat.parse(startDate));
				if(regDate.compareTo(cal)<0)
				 {
					hql="from EmployeeIncentives where date(incentiveGivenDate) >= '"+startDate+"' and date(incentiveGivenDate) <='"+endDate+"' and employeeDetails.employeeDetailsId='"+employeeDetailsId+"'";
				 }
				else
				{
					hql="from EmployeeIncentives where date(incentiveGivenDate) >= '"+dateFormat.format(regDate.getTime())+"' and date(incentiveGivenDate) <='"+endDate+"' and employeeDetails.employeeDetailsId='"+employeeDetailsId+"'";
				}
				query = sessionFactory.getCurrentSession().createQuery(hql);
			}
			
			List<EmployeeIncentives> list=(List<EmployeeIncentives>)query.list();
			if(list.isEmpty())
			{
				return 0;
			}
			Iterator<EmployeeIncentives> itr=list.iterator();
			double incentives=0;
			while(itr.hasNext())
			{
				EmployeeIncentives employeeIncentives=itr.next();
				incentives=incentives+employeeIncentives.getIncentiveAmount();
			}
			return incentives;
		} catch (HibernateException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return 0;
	}
	
	public double  getDeductionAmount(String startDate, String endDate,
			String range, EmployeeDetails employeeDetails)
	{
		try {
			double deduction=0;
			Calendar cal = Calendar.getInstance();
			Calendar regDate = Calendar.getInstance();
			regDate.setTime(employeeDetails.getEmployeeDetailsAddedDatetime());
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

			if (range.equals("Last6Months")) {
				cal.add(Calendar.MONTH, -6);
				if(regDate.compareTo(cal)<0)
				 {
					deduction=getDeductionCriteriaAmount(sdf.format(cal.getTime()), sdf.format(new Date()), range, employeeDetails);
				 }
				else
				{
					deduction=getDeductionCriteriaAmount(sdf.format(regDate.getTime()), sdf.format(new Date()), range, employeeDetails);
				}
			}
			else if (range.equals("Last1Year")) {
				cal.add(Calendar.MONTH, -12);
				if(regDate.compareTo(cal)<0)
				 {
				deduction=getDeductionCriteriaAmount(sdf.format(cal.getTime()), sdf.format(new Date()), range, employeeDetails);
				 }
				else
				{
					deduction=getDeductionCriteriaAmount(sdf.format(regDate.getTime()), sdf.format(new Date()), range, employeeDetails);
				}
			}			
			else if (range.equals("ViewAll")) {
				deduction=getDeductionCriteriaAmount(sdf.format(employeeDetails.getEmployeeDetailsAddedDatetime()), sdf.format(new Date()), range, employeeDetails);
			}
			else if (range.equals("CurrentMonth")) {
				cal.setTime(dateFormat.parse(DatePicker.getCurrentMonthStartDate()));
				if(regDate.compareTo(cal)<0)
				 {
				deduction=getDeductionCriteriaAmount(DatePicker.getCurrentMonthStartDate(), DatePicker.getCurrentMonthLastDate(), range, employeeDetails);
				 }
				else
				{
					deduction=getDeductionCriteriaAmount(sdf.format(regDate.getTime()), DatePicker.getCurrentMonthLastDate(), range, employeeDetails);
				}
			}
			else if (range.equals("Range")) {
				cal.setTime(dateFormat.parse(startDate));
				if(regDate.compareTo(cal)<0)
				 {
				deduction=getDeductionCriteriaAmount(startDate, endDate, range, employeeDetails);
				 }
				else
				{
					deduction=getDeductionCriteriaAmount(sdf.format(regDate.getTime()), endDate, range, employeeDetails);
				}
			}
			
			return deduction;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			System.out.println(e.toString());
		}
		return 0;		
	}
	
	public double getDeductionCriteriaAmount(String startDate, String endDate,
			String range, EmployeeDetails employeeDetails)
	{
		
		try {
			//double basicSalary=employeeDetails.getBasicSalary();
			
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			Date start = sdf.parse(startDate);
			Date end = sdf.parse(endDate);

			Calendar cStart = Calendar.getInstance(); cStart.setTime(start);
			Calendar cEnd = Calendar.getInstance(); cEnd.setTime(end);
			Calendar last=Calendar.getInstance();
			long diff,days,maxDay;
			double perday,deductions=0;
					while (cStart.before(cEnd) || cStart.equals(cEnd)) 
					{
							 List<String> holidayDateList=new ArrayList<>();
							 
							 last=cStart.getInstance();
							 last.set(Calendar.DAY_OF_MONTH, cStart.getActualMaximum(Calendar.DAY_OF_MONTH));
							 maxDay=cStart.getActualMaximum(Calendar.DAY_OF_MONTH);
							 
							 if(last.compareTo(cEnd)<0)
							 {	 
								 holidayDateList=getNoOfPaidHolidays(sdf.format(cStart.getTime()), sdf.format(last.getTime()), range, employeeDetails);
								 cStart.set(Calendar.DAY_OF_MONTH, cStart.getActualMaximum(Calendar.DAY_OF_MONTH));
							 }
							 else
							 {
								 holidayDateList=getNoOfPaidHolidays(sdf.format(cStart.getTime()), sdf.format(cEnd.getTime()), range, employeeDetails);
								 cStart.setTime(cEnd.getTime());
							 }							 
							 
							EmployeeBasicSalaryStatus employeeBasicSalary=fetchLastEmployeeOldBasicSalaryByEmployeeOldBasicSalaryId(employeeDetails.getEmployeeDetailsId());
							employeeBasicSalary.setEndDate(new Date());
							updateEmployeeOldBasicSalary(employeeBasicSalary);
							 
							if(holidayDateList!=null)
							{
							  for (int i=0; i<holidayDateList.size(); i++) 
							  {
								//String hql="from EmployeeBasicSalaryStatus where date(startDate) <= '"+holidayDateList.get(i)+"' and date(endDate) >= '"+holidayDateList.get(i)+"' and employeeDetails.employeeDetailsId='"+employeeDetails.getEmployeeDetailsId()+"' order by employeeBasicSalaryStatusId desc";
																
								double basicSalary=fetchEmployeeBasicSalaryStatusByDate(holidayDateList.get(i),employeeDetails.getEmployeeDetailsId());
								
								perday=basicSalary/30;
								deductions=deductions+perday;
							 }
							} 
							 
							// perday=basicSalary/30;
							 //deductions=deductions+(perday*days);
						
					    //add one day to date
					    cStart.add(Calendar.DAY_OF_MONTH, 1);
					   // System.out.println(cStart.getTime() +"-"+ cStart.get(Calendar.MONTH));
					    //do something...
					
					}
					
					return deductions;
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			System.out.println(e.toString());
		}
		return 0;
		
		
	}
	
	@Transactional
	public double fetchEmployeeBasicSalaryStatusByDate(String date,long employeeDetailsId)
	{
		String hql="from EmployeeBasicSalaryStatus where date(startDate) <= '"+date+"' and date(endDate) >= '"+date+"' and employeeDetails.employeeDetailsId='"+employeeDetailsId+"' order by employeeBasicSalaryStatusId desc";
		
		Query query=sessionFactory.getCurrentSession().createQuery(hql);
		
		List<EmployeeBasicSalaryStatus> employeeOldBasicSalaryList=(List<EmployeeBasicSalaryStatus>)query.list();
		
		if(employeeOldBasicSalaryList.isEmpty())
		{
			return fetchEmployeeDetailsForWebApp(employeeDetailsId).getBasicSalary();
		}
		
		return employeeOldBasicSalaryList.get(0).getBasicSalary();
	}
	
	private List<String> getNoOfPaidHolidays(String startDate, String endDate,
			String range, EmployeeDetails employeeDetails) {
		try {
			long noOfHolidays=0;
			long employeeDetailsId=employeeDetails.getEmployeeDetailsId();
			SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
			String hql = "";
			Query query = null; 
			Calendar cal = Calendar.getInstance();  //Get current date/month i.e 27 Feb, 2012
			Calendar regDate = Calendar.getInstance();
			regDate.setTime(employeeDetails.getEmployeeDetailsAddedDatetime());
			if (range.equals("Last6Months")) {
				cal.add(Calendar.MONTH, -6);
				if(regDate.compareTo(cal)<0)
				{
					hql = "from EmployeeHolidays where date(givenHolidayDate) >= '"+dateFormat.format(cal.getTime())+"' and paidHoliday=false and employeeDetails.employeeDetailsId='"+employeeDetailsId+"' order by employeeHolidaysId desc";
				}
				else
				{
					hql = "from EmployeeHolidays where date(givenHolidayDate) >= '"+dateFormat.format(regDate.getTime())+"' and paidHoliday=false and employeeDetails.employeeDetailsId='"+employeeDetailsId+"' order by employeeHolidaysId desc";
				}
				query = sessionFactory.getCurrentSession().createQuery(hql);

			}
			else if (range.equals("Last1Year")) {
				cal.add(Calendar.MONTH, -12);
				if(regDate.compareTo(cal)<0)
				{
					hql = "from EmployeeHolidays where date(givenHolidayDate) >= '"+dateFormat.format(cal.getTime())+"' and paidHoliday=false and employeeDetails.employeeDetailsId='"+employeeDetailsId+"' order by employeeHolidaysId desc";
				}
				else
				{
					hql = "from EmployeeHolidays where date(givenHolidayDate) >= '"+dateFormat.format(regDate.getTime())+"' and paidHoliday=false and employeeDetails.employeeDetailsId='"+employeeDetailsId+"' order by employeeHolidaysId desc";
				}
				query = sessionFactory.getCurrentSession().createQuery(hql);

			}
			
			else if (range.equals("ViewAll")) {

				hql = "from EmployeeHolidays where  paidHoliday=false and employeeDetails.employeeDetailsId='"+employeeDetailsId+"' order by employeeHolidaysId desc";

				query = sessionFactory.getCurrentSession().createQuery(hql);

			}
			else if (range.equals("CurrentMonth")) {
				cal.setTime(dateFormat.parse(DatePicker.getCurrentMonthStartDate()));
				if(regDate.compareTo(cal)<0)
				{
					hql="from EmployeeHolidays where date(givenHolidayDate) >= '"+DatePicker.getCurrentMonthStartDate()+"' and paidHoliday=false and date(givenHolidayDate) <='"+DatePicker.getCurrentMonthLastDate()+"' and employeeDetails.employeeDetailsId='"+employeeDetailsId+"' order by employeeHolidaysId desc";
				}
				else
				{
					hql="from EmployeeHolidays where date(givenHolidayDate) >= '"+dateFormat.format(regDate.getTime())+"' and paidHoliday=false and date(givenHolidayDate) <='"+DatePicker.getCurrentMonthLastDate()+"' and employeeDetails.employeeDetailsId='"+employeeDetailsId+"' order by employeeHolidaysId desc";
				}
				query = sessionFactory.getCurrentSession().createQuery(hql);

			}
			else if (range.equals("Range")) {
				cal.setTime(dateFormat.parse(startDate));
				if(regDate.compareTo(cal)<0)
				{
					hql="from EmployeeHolidays where date(givenHolidayDate) >= '"+startDate+"' and date(givenHolidayDate) <='"+endDate+"' and paidHoliday=false and employeeDetails.employeeDetailsId='"+employeeDetailsId+"' order by employeeHolidaysId desc";
				}
				else
				{
					hql="from EmployeeHolidays where date(givenHolidayDate) >= '"+dateFormat.format(regDate.getTime())+"' and date(givenHolidayDate) <='"+endDate+"' and paidHoliday=false and employeeDetails.employeeDetailsId='"+employeeDetailsId+"' order by employeeHolidaysId desc";
				}
				query = sessionFactory.getCurrentSession().createQuery(hql);

			}
			
			
			List<EmployeeHolidays> list=(List<EmployeeHolidays>)query.list();
			if(list.isEmpty())
			{
				return null;
			}
			
			List<String> holidayDateList=new ArrayList<>();
			
			Iterator<EmployeeHolidays> itr=list.iterator();
			while(itr.hasNext())
			{
				EmployeeHolidays employeeHolidays=itr.next();
				Date from=employeeHolidays.getFromDate();
				Date to;
				Calendar cEnd = Calendar.getInstance();
				Calendar cStart = Calendar.getInstance();
				long diff;
				if(employeeHolidays.getToDate()!=null)
				{
					to=employeeHolidays.getToDate();
					
					cStart.setTime(from);
					cEnd.setTime(to);
					/*diff = cEnd.getTime().getTime() - cStart.getTime().getTime();
					noOfHolidays=noOfHolidays+TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS)+1;*/
					
					while (cStart.before(cEnd) || cStart.equals(cEnd)) {
						holidayDateList.add(new SimpleDateFormat("yyyy-MM-dd").format(cStart.getTime()));
						cStart.add(Calendar.DAY_OF_MONTH, 1);
					}
					
				}
				else
				{				
					//noOfHolidays=noOfHolidays+1;
					holidayDateList.add(new SimpleDateFormat("yyyy-MM-dd").format(from));
				}    
			}
			return holidayDateList;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return null;
	}
	
	public double findTotalSalaryTwoDates(String startDate, String endDate,
			String range, EmployeeDetails employeeDetails) 
	{
		try {
			double salary=0;
			Calendar cal = Calendar.getInstance();
			Calendar regDate = Calendar.getInstance();
			regDate.setTime(employeeDetails.getEmployeeDetailsAddedDatetime());
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			
			if (range.equals("Last6Months")) {
				
				cal.add(Calendar.MONTH, -6);
				if(regDate.compareTo(cal)<0)
				{
					salary=getTotalSalary(sdf.format(cal.getTime()), sdf.format(new Date()), range, employeeDetails);
				}
				else
				{
					salary=getTotalSalary(sdf.format(regDate.getTime()), sdf.format(new Date()), range, employeeDetails);
				}
			}
			else if (range.equals("Last1Year")) {
				cal.add(Calendar.MONTH, -12);
				if(regDate.compareTo(cal)<0)
				{
					salary=getTotalSalary(sdf.format(cal.getTime()), sdf.format(new Date()), range, employeeDetails);
				}
				else
				{
					salary=getTotalSalary(sdf.format(regDate.getTime()), sdf.format(new Date()), range, employeeDetails);
				}
			}			
			else if (range.equals("ViewAll")) {
				salary=getTotalSalary(sdf.format(employeeDetails.getEmployeeDetailsAddedDatetime()), sdf.format(new Date()), range, employeeDetails);
			}
			else if (range.equals("CurrentMonth")) {
				//salary=getTotalSalary(DatePicker.getCurrentMonthStartDate(), DatePicker.getCurrentMonthLastDate(), range, employeeDetails);
				cal.setTime(sdf.parse(DatePicker.getCurrentMonthStartDate()));
				if(regDate.compareTo(cal)<0)
				{
					salary=getTotalSalary(sdf.format(cal.getTime()), sdf.format(new Date()), range, employeeDetails);
				}
				else
				{
					salary=getTotalSalary(sdf.format(regDate.getTime()), sdf.format(new Date()), range, employeeDetails);
				}
			}
			else if (range.equals("Range")) {				
				//salary=getTotalSalary(startDate, endDate, range, employeeDetails);
				cal.setTime(sdf.parse(startDate));
				if(regDate.compareTo(cal)<0)
				{
					salary=getTotalSalary(sdf.format(cal.getTime()), sdf.format(new Date()), range, employeeDetails);
				}
				else
				{
					salary=getTotalSalary(sdf.format(regDate.getTime()), sdf.format(new Date()), range, employeeDetails);
				}
				
			}
			
			return salary;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			System.out.println(e.toString());
		}
		return 0;		
	}
	
	public double getTotalSalary(String startDate, String endDate,
			String range, EmployeeDetails employeeDetails)
	{
		
		try {
			double basicSalary=0;
			Calendar disableDate = Calendar.getInstance(); disableDate.setTime(employeeDetails.getEmployeeDetailsDisableDatetime());
			
			
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			Date start = sdf.parse(startDate);
			Date end = sdf.parse(endDate);

			Calendar cStart = Calendar.getInstance(); cStart.setTime(start);
			Calendar cEnd = Calendar.getInstance(); cEnd.setTime(end);
			Calendar last=Calendar.getInstance();
			long diff,days,maxDay;
			double perday,salary=0;
					while (cStart.before(cEnd) || cStart.equals(cEnd)) 
					{
							last.setTime(cStart.getTime());
							 last.set(Calendar.DAY_OF_MONTH, last.getActualMaximum(Calendar.DAY_OF_MONTH));
							// maxDay=cStart.getActualMaximum(Calendar.DAY_OF_MONTH);
							 if(last.compareTo(cEnd)<0)
							 {	
								// diff = last.getTime().getTime()-cStart.getTime().getTime();
								 
								 Calendar tempDate=Calendar.getInstance();
								 tempDate.setTime(cStart.getTime());
								 
								 while (tempDate.before(last) || tempDate.equals(last))  
								 {
									if(employeeDetails.isStatus())
									{
										if(tempDate.before(disableDate)  || tempDate.equals(disableDate))
										{
											String date=new SimpleDateFormat("yyyy-MM-dd").format(tempDate.getTime());
										
											basicSalary=fetchEmployeeBasicSalaryStatusByDate(date,employeeDetails.getEmployeeDetailsId());
											perday=basicSalary/30;
											salary=salary+perday;
										}
									}
									else
									{
										String date=new SimpleDateFormat("yyyy-MM-dd").format(tempDate.getTime());
										
										basicSalary=fetchEmployeeBasicSalaryStatusByDate(date,employeeDetails.getEmployeeDetailsId());
										perday=basicSalary/30;
										salary=salary+perday;
									}
									tempDate.add(Calendar.DAY_OF_MONTH, 1);
								}
								 
								 cStart.set(Calendar.DAY_OF_MONTH, cStart.getActualMaximum(Calendar.DAY_OF_MONTH));
							 }
							 else
							 {
								// diff = cEnd.getTime().getTime()-cStart.getTime().getTime();
								 
								 Calendar tempDate=Calendar.getInstance();
								 tempDate.setTime(cStart.getTime());
								 
								 while (tempDate.before(cEnd) || tempDate.equals(cEnd))  
								 {
									if(employeeDetails.isStatus())
									{
										if( tempDate.before(disableDate)  || tempDate.equals(disableDate) )
										{
											String date=new SimpleDateFormat("yyyy-MM-dd").format(tempDate.getTime());
											
											basicSalary=fetchEmployeeBasicSalaryStatusByDate(date,employeeDetails.getEmployeeDetailsId());
											
											perday=basicSalary/30;
											salary=salary+perday;
										}
									}
									else
									{
										String date=new SimpleDateFormat("yyyy-MM-dd").format(tempDate.getTime());
										
										basicSalary=fetchEmployeeBasicSalaryStatusByDate(date,employeeDetails.getEmployeeDetailsId());
										
										perday=basicSalary/30;
										salary=salary+perday;
									}
									tempDate.add(Calendar.DAY_OF_MONTH, 1);
								}
								 
								 cStart.setTime(cEnd.getTime());
							 }
							 //days=TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS)+1;
							 
							 /*//perday=basicSalary/maxDay;
							 perday=basicSalary/30;
							 salary=salary+(perday*days);*/
							 
						
						
						
					    //add one day to date
					    cStart.add(Calendar.DAY_OF_MONTH, 1);
					   // System.out.println(cStart.getTime() +"-"+ cStart.get(Calendar.MONTH));
					    //do something...
					
				}
					
					return salary;
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			System.out.println(e.toString());
		}
		return 0;
	}
	
	@Transactional
	public List<EmployeeSalaryStatus> fetchEmployeeSalaryStatusForWebApp(long employeeDetailsId) {

		String hql = "from EmployeeDetails where employeeDetailsId='"+employeeDetailsId+"'";
		Query query = sessionFactory.getCurrentSession().createQuery(hql);
		
		@SuppressWarnings("unchecked")
		List<EmployeeDetails> employeeList = (List<EmployeeDetails>) query.list();

		if (employeeList.isEmpty()) {
			return null;
		}
		
		List<EmployeeSalaryStatus> list=new ArrayList<>();
		Iterator<EmployeeDetails> itr=employeeList.iterator();
		int srno=0;
		while(itr.hasNext()){
			EmployeeDetails employeeDetails=itr.next();
			String areaList=getLocationList(employeeDetails.getEmployeeDetailsId());
			long noOfHolidays=getNoOfHolidays(DatePicker.getCurrentMonthStartDate(), DatePicker.getCurrentMonthLastDate(), "CurrentMonth", employeeDetails);
			double basicMothlySalary=employeeDetails.getBasicSalary();
			double deduction=getDeductionAmount(DatePicker.getCurrentMonthStartDate(), DatePicker.getCurrentMonthLastDate(), "CurrentMonth", employeeDetails );
			double incentives=getIncentivesForWebApp(DatePicker.getCurrentMonthStartDate(), DatePicker.getCurrentMonthLastDate(), "CurrentMonth", employeeDetails );			
			double paidCurrentMonthSalary=getPaidSalary(DatePicker.getCurrentMonthStartDate(), DatePicker.getCurrentMonthLastDate(), "CurrentMonth", employeeDetails);
			double totalAmount=findTotalSalaryTwoDates(DatePicker.getCurrentMonthStartDate(), DatePicker.getCurrentMonthLastDate(), "CurrentMonth", employeeDetails)+incentives-deduction;
			double unpaidCurrentMonthSalary=totalAmount-paidCurrentMonthSalary;
			
			List<EmployeeSalaryModel> employeeSalaryList=getEmployeeSalaryList(employeeDetails);
			list.add(new EmployeeSalaryStatus(
					(long)srno,
					employeeDetails.getEmployeeDetailsId(),
					employeeDetails.getEmployeeDetailsGenId(),
					employeeDetails.getName(), 
					employeeDetails.getEmployee().getDepartment().getName(), 
					employeeDetails.getContact().getMobileNumber(), 
					employeeDetails.getAddress(), 
					areaList, 
					(int)noOfHolidays, 
					findTotalSalaryTwoDates(DatePicker.getCurrentMonthStartDate(), DatePicker.getCurrentMonthLastDate(), "CurrentMonth", employeeDetails), 
					paidCurrentMonthSalary, 
					unpaidCurrentMonthSalary, 
					basicMothlySalary,
					incentives,
					deduction,
					totalAmount,
					employeeSalaryList));
		}
		
		
		return list;
	}
	
	private String getLocationList(long employeeDetailsId) {
		
		String hql = "from EmployeeRouteList where employeeDetails.employeeDetailsId='"+employeeDetailsId+"'";
		Query query = sessionFactory.getCurrentSession().createQuery(hql);
		@SuppressWarnings("unchecked")
		List<EmployeeRouteList> employeeRouteList = (List<EmployeeRouteList>) query.list();

		if (employeeRouteList.isEmpty()) {
			return null;
		}
		String routeList="";
		EmployeeDetails employeeDetails=fetchEmployeeDetailsForWebApp(employeeDetailsId);
		
		if(employeeDetails.getEmployee().getDepartment().getName().equals(Constants.GATE_KEEPER_DEPT_NAME))
		{
			for(EmployeeRouteList employeeRoute: employeeRouteList)
			{
				routeList+=employeeRoute.getCluster().getName();
			}
		}
		else if(employeeDetails.getEmployee().getDepartment().getName().equals(Constants.AREA_SALESMAN_DEPT_NAME))
		{
			for(EmployeeRouteList employeeRoute: employeeRouteList)
			{
				routeList+=employeeRoute.getArea().getName();
			}
		}
		else if(employeeDetails.getEmployee().getDepartment().getName().equals(Constants.SALESMAN_DEPT_NAME))
		{
			for(EmployeeRouteList employeeRoute: employeeRouteList)
			{
				routeList+=employeeRoute.getRoute().getRouteNo();
			}
		}
		
		return routeList;
	}
	
	private List<EmployeeSalaryModel> getEmployeeSalaryList(EmployeeDetails employeeDetails) {
		// TODO Auto-generated method stub
		long employeeDetailsId=employeeDetails.getEmployeeDetailsId();
		Calendar cal = Calendar.getInstance();
		Calendar regDate = Calendar.getInstance();
		regDate.setTime(employeeDetails.getEmployeeDetailsAddedDatetime());
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		String hql;
		if(regDate.compareTo(cal)<0)
		{
			hql="from EmployeeSalary where date(payingDate) >= '"+DatePicker.getCurrentMonthStartDate()+"' and date(payingDate) <='"+DatePicker.getCurrentMonthLastDate()+"' and employeeDetails.employeeDetailsId='"+employeeDetailsId+"'";
		}
		else
		{
			hql="from EmployeeSalary where date(payingDate) >= '"+sdf.format(regDate)+"' and date(payingDate) <='"+DatePicker.getCurrentMonthLastDate()+"' and employeeDetails.employeeDetailsId='"+employeeDetailsId+"'";
		}
		Query query=sessionFactory.getCurrentSession().createQuery(hql);
		List<EmployeeSalary> employeeSalaryList=(List<EmployeeSalary>)query.list();
		if(employeeSalaryList.isEmpty())
		{
			return null;
		}
		List<EmployeeSalaryModel> employeeSalaryModelList=new ArrayList<>();
		Iterator<EmployeeSalary> itr=employeeSalaryList.iterator();
		int srno=0;
		while(itr.hasNext())
		{
			srno++;
			EmployeeSalary employeeSalary=itr.next();
			employeeSalaryModelList.add(new EmployeeSalaryModel(srno, 
																employeeSalary.getPayingAmount(),
																/*employeeSalary.getIncentive(),*/
																employeeSalary.getPayingDate(), 
																findModeOfPayment(employeeSalary), 
																employeeSalary.getComment()));
		}
		
		return employeeSalaryModelList;
	}
	
	private String findModeOfPayment(EmployeeSalary employeeSalary) {
		// TODO Auto-generated method stub
		
		if(employeeSalary.getChequeNumber()==null || employeeSalary.getChequeNumber().equals(""))
		{
			return "Cash";
		}
		
		return "Cheque";
	}
	
	@Transactional
	public List<EmployeeSalaryStatus> tofilterRangeEmployeeSalaryStatusForWebApp(String startDate, String endDate,
			String range, long employeeDetailsId)
	{
		
		String hql = "from EmployeeDetails where employeeDetailsId='"+employeeDetailsId+"'";
		Query query = sessionFactory.getCurrentSession().createQuery(hql);
		
		@SuppressWarnings("unchecked")
		List<EmployeeDetails> employeeList = (List<EmployeeDetails>) query.list();

		if (employeeList.isEmpty()) {
			return null;
		}
		
		List<EmployeeSalaryStatus> list=new ArrayList<>();
		Iterator<EmployeeDetails> itr=employeeList.iterator();
		long srno=0;
		while(itr.hasNext()){
			EmployeeDetails employeeDetails=itr.next();
			String areaList=getLocationList(employeeDetails.getEmployeeDetailsId());
			long noOfHolidays=getNoOfHolidays(startDate, endDate, range, employeeDetails);
			double basicMothlySalary=employeeDetails.getBasicSalary();
			double deduction=getDeductionAmount(startDate, endDate, range, employeeDetails);
			double incentives=getIncentivesForWebApp(startDate, endDate, range, employeeDetails);
			double paidCurrentMonthSalary=getPaidSalary(startDate, endDate, range, employeeDetails);
			double totalAmount=findTotalSalaryTwoDates(startDate, endDate, range, employeeDetails)+incentives-deduction;//paidCurrentMonthSalary-deduction;
			double unpaidCurrentMonthSalary=totalAmount-paidCurrentMonthSalary;
			List<EmployeeSalaryModel> employeeSalaryList=tofilterRangeEmployeeSalaryModelStatusForWebApp(startDate, endDate, range, employeeDetails);
			list.add(new EmployeeSalaryStatus(
					(long)srno,
					employeeDetails.getEmployeeDetailsId(),
					employeeDetails.getEmployeeDetailsGenId(),
					employeeDetails.getName(), 
					employeeDetails.getEmployee().getDepartment().getName(), 
					employeeDetails.getContact().getMobileNumber(), 
					employeeDetails.getAddress(), 
					areaList, 
					(int)noOfHolidays, 
					findTotalSalaryTwoDates(DatePicker.getCurrentMonthStartDate(), DatePicker.getCurrentMonthLastDate(), "CurrentMonth", employeeDetails), 
					paidCurrentMonthSalary, 
					unpaidCurrentMonthSalary, 
					basicMothlySalary,
					incentives,
					deduction,
					totalAmount,
					employeeSalaryList));
		}
		
		
		return list;
	}
	
	@Transactional
	public List<EmployeeSalaryModel> tofilterRangeEmployeeSalaryModelStatusForWebApp(String startDate, String endDate,
			String range, EmployeeDetails employeeDetails) {

		try {
			long employeeDetailsId=employeeDetails.getEmployeeDetailsId();
			
			SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
			String hql = "";
			Query query = null;
			Calendar cal = Calendar.getInstance();  //Get current date/month i.e 27 Feb, 2012
			Calendar regDate = Calendar.getInstance();
			regDate.setTime(employeeDetails.getEmployeeDetailsAddedDatetime());
			
			if (range.equals("Last6Months")) {
				cal.add(Calendar.MONTH, -6);
				if(regDate.compareTo(cal)<0)
				{
					hql = "from EmployeeSalary where date(payingDate) >= '"+dateFormat.format(cal.getTime())+"' and employeeDetails.employeeDetailsId='"+employeeDetailsId+"'";
				}
				else
				{
					hql = "from EmployeeSalary where date(payingDate) >= '"+dateFormat.format(regDate.getTime())+"' and employeeDetails.employeeDetailsId='"+employeeDetailsId+"'";
				}
				query = sessionFactory.getCurrentSession().createQuery(hql);

			}
			else if (range.equals("Last1Year")) {
				cal.add(Calendar.MONTH, -12);
				if(regDate.compareTo(cal)<0)
				{
					hql = "from EmployeeSalary where date(payingDate) >= '"+dateFormat.format(cal.getTime())+"' and employeeDetails.employeeDetailsId='"+employeeDetailsId+"'";
				}
				else
				{
					hql = "from EmployeeSalary where date(payingDate) >= '"+dateFormat.format(regDate.getTime())+"' and employeeDetails.employeeDetailsId='"+employeeDetailsId+"'";
				}
				query = sessionFactory.getCurrentSession().createQuery(hql);

			}
			
			else if (range.equals("ViewAll")) {

				hql = "from EmployeeSalary where employeeDetails.employeeDetailsId='"+employeeDetailsId+"'";

				query = sessionFactory.getCurrentSession().createQuery(hql);

			}
			else if (range.equals("CurrentMonth")) {
				cal.setTime(dateFormat.parse(DatePicker.getCurrentMonthStartDate()));
				if(regDate.compareTo(cal)<0)
				{
					hql="from EmployeeSalary where date(payingDate) >= '"+DatePicker.getCurrentMonthStartDate()+"' and date(payingDate) <='"+DatePicker.getCurrentMonthLastDate()+"' and employeeDetails.employeeDetailsId='"+employeeDetailsId+"'";
				}
				else
				{
					hql="from EmployeeSalary where date(payingDate) >= '"+dateFormat.format(regDate.getTime())+"' and date(payingDate) <='"+DatePicker.getCurrentMonthLastDate()+"' and employeeDetails.employeeDetailsId='"+employeeDetailsId+"'";
				}
				query = sessionFactory.getCurrentSession().createQuery(hql);

			}
			else if (range.equals("Range")) {
				cal.setTime(dateFormat.parse(startDate));
				if(regDate.compareTo(cal)<0)
				{
					hql="from EmployeeSalary where date(payingDate) >= '"+startDate+"' and date(payingDate) <='"+endDate+"' and employeeDetails.employeeDetailsId='"+employeeDetailsId+"'";
				}
				else
				{
					hql="from EmployeeSalary where date(payingDate) >= '"+dateFormat.format(regDate.getTime())+"' and date(payingDate) <='"+endDate+"' and employeeDetails.employeeDetailsId='"+employeeDetailsId+"'";
				}
				query = sessionFactory.getCurrentSession().createQuery(hql);

			}
			
			List<EmployeeSalary> employeeSalaryList=(List<EmployeeSalary>)query.list();
			if(employeeSalaryList.isEmpty())
			{
				return null;
			}
			List<EmployeeSalaryModel> employeeSalaryModelList=new ArrayList<>();
			Iterator<EmployeeSalary> itr=employeeSalaryList.iterator();
			int srno=0;
			while(itr.hasNext())
			{
				srno++;
				EmployeeSalary employeeSalary=itr.next();
				employeeSalaryModelList.add(new EmployeeSalaryModel(srno, 
																	employeeSalary.getPayingAmount(),
																	/*employeeSalary.getIncentive(),*/
																	employeeSalary.getPayingDate(), 
																	findModeOfPayment(employeeSalary), 
																	employeeSalary.getComment()));
			}
			
			return employeeSalaryModelList;
		} catch (HibernateException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	return null;
	}
	@Transactional
	public EmployeeHolidayModel fetchEmployeeHolidayModelForWebApp(String employeeDetailsId,String filter,String startDate,String endDate) {
		//TODO Auto-generated method stub
		
		String hql = "from EmployeeDetails where employeeDetailsId='"+employeeDetailsId+"'";

		Query query = sessionFactory.getCurrentSession().createQuery(hql);
		
		List<EmployeeDetails> employeeHolidayslist=(List<EmployeeDetails>)query.list();
		
		if(employeeHolidayslist.isEmpty())
		{
			return null;
		}
		
		
		EmployeeDetails employeeDetails=employeeHolidayslist.get(0);
			
			long employeeDetailId=employeeDetails.getEmployeeDetailsId();
			String name=employeeDetails.getName();
			String departmentname=employeeDetails.getEmployee().getDepartment().getName();
			String mobileNumber=employeeDetails.getContact().getMobileNumber();
			long noOfHolidays=getNoOfHolidays(startDate, endDate, filter, employeeDetails);
			List<EmployeeHolidayList> employeeHolidayList=tofilterRangeEmployeeHolidayListModelStatusForWebApp(startDate, endDate, filter, employeeDetails);;
	
		return new EmployeeHolidayModel(employeeDetailId,employeeDetails.getEmployeeDetailsGenId(), name, departmentname, mobileNumber,employeeDetails.isStatus(), noOfHolidays, employeeHolidayList);
	}
	
	@Transactional
	public List<EmployeeHolidayList> tofilterRangeEmployeeHolidayListModelStatusForWebApp(String startDate, String endDate,
			String range, EmployeeDetails employeeDetails) {
		try 
		{
			long employeeDetailsId=employeeDetails.getEmployeeDetailsId();
	
			SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
			String hql = "";
			Query query = null;
			Calendar cal = Calendar.getInstance();  //Get current date/month i.e 27 Feb, 2012
			Calendar regDate = Calendar.getInstance();
			regDate.setTime(employeeDetails.getEmployeeDetailsAddedDatetime());
			if (range.equals("Last6Months")) {
				cal.add(Calendar.MONTH, -6);
				if(regDate.compareTo(cal)<0)
				{
					hql = "from EmployeeHolidays where date(givenHolidayDate) >= '"+dateFormat.format(cal.getTime())+"' and employeeDetails.employeeDetailsId='"+employeeDetailsId+"'";
				}
				else
				{
					hql = "from EmployeeHolidays where date(givenHolidayDate) >= '"+dateFormat.format(regDate.getTime())+"'  and employeeDetails.employeeDetailsId='"+employeeDetailsId+"'";
				}
				query = sessionFactory.getCurrentSession().createQuery(hql);
	
			}
			else if (range.equals("Last1Year")) {
				cal.add(Calendar.MONTH, -12);
				if(regDate.compareTo(cal)<0)
				{
					hql = "from EmployeeHolidays where date(givenHolidayDate) >= '"+dateFormat.format(cal.getTime())+"'  and employeeDetails.employeeDetailsId='"+employeeDetailsId+"'";
				}
				else
				{
					hql = "from EmployeeHolidays where date(givenHolidayDate) >= '"+dateFormat.format(regDate.getTime())+"'  and employeeDetails.employeeDetailsId='"+employeeDetailsId+"'";
				}
				query = sessionFactory.getCurrentSession().createQuery(hql);
	
			}
			
			else if (range.equals("ViewAll")) {
	
				hql = "from EmployeeHolidays where employeeDetails.employeeDetailsId='"+employeeDetailsId+"'";
	
				query = sessionFactory.getCurrentSession().createQuery(hql);
	
			}
			else if (range.equals("CurrentMonth")) {
				cal.setTime(dateFormat.parse(DatePicker.getCurrentMonthStartDate()));
				if(regDate.compareTo(cal)<0)
				{
					hql="from EmployeeHolidays where date(givenHolidayDate) >= '"+DatePicker.getCurrentMonthStartDate()+"'  and date(givenHolidayDate) <='"+DatePicker.getCurrentMonthLastDate()+"' and employeeDetails.employeeDetailsId='"+employeeDetailsId+"'";
				}
				else
				{
					hql="from EmployeeHolidays where date(givenHolidayDate) >= '"+dateFormat.format(regDate.getTime())+"'  and date(givenHolidayDate) <='"+DatePicker.getCurrentMonthLastDate()+"' and employeeDetails.employeeDetailsId='"+employeeDetailsId+"'";
				}
				query = sessionFactory.getCurrentSession().createQuery(hql);
	
			}
			else if (range.equals("Range")) {
				cal.setTime(dateFormat.parse(startDate));
				if(regDate.compareTo(cal)<0)
				{
					hql="from EmployeeHolidays where date(givenHolidayDate) >= '"+startDate+"' and date(givenHolidayDate) <='"+endDate+"'  and employeeDetails.employeeDetailsId='"+employeeDetailsId+"'";
				}
				else
				{
					hql="from EmployeeHolidays where date(givenHolidayDate) >= '"+dateFormat.format(regDate.getTime())+"'  and date(givenHolidayDate) <='"+endDate+"' and employeeDetails.employeeDetailsId='"+employeeDetailsId+"'";
				}
				query = sessionFactory.getCurrentSession().createQuery(hql);
	
			}
			
			List<EmployeeHolidays> employeeHolidayList=(List<EmployeeHolidays>)query.list();
			if(employeeHolidayList.isEmpty())
			{
				return null;
			}
			List<EmployeeHolidayList> employeeHolidayListList=new ArrayList<>();
			Iterator<EmployeeHolidays> itr=employeeHolidayList.iterator();
			long srno=0;
			while(itr.hasNext())
			{
				srno++;
				EmployeeHolidays employeeHolidays=itr.next();
				Date from=employeeHolidays.getFromDate();
				Date to;
				long diff;
				long noOfHolidays=0;
				Calendar cEnd = Calendar.getInstance();
				Calendar cStart = Calendar.getInstance();
				if(employeeHolidays.getToDate()!=null)
				{
					to=employeeHolidays.getToDate();
					
					cStart.setTime(from);
					 cEnd.setTime(to);
					 diff = cEnd.getTime().getTime() - cStart.getTime().getTime();
					noOfHolidays=noOfHolidays+TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS)+1;
				}
				else
				{				
					noOfHolidays=noOfHolidays+1;
				}    
				double amountDeduct=getAmountDeductOnHoliday(employeeHolidays.getFromDate(),employeeHolidays.getToDate(),employeeDetailsId);
				String typeOfLeave="";
				if(employeeHolidays.isPaidHoliday())
				{
					typeOfLeave="Paid";
					amountDeduct=0;
				}
				else
				{
					typeOfLeave="UnPaid";					
				}
				
				employeeHolidayListList.add(new EmployeeHolidayList(srno, 
																	employeeHolidays.getEmployeeHolidaysId(),
																	noOfHolidays, 
																	typeOfLeave, 
																	amountDeduct, 
																	employeeHolidays.getGivenHolidayDate(),
																	employeeHolidays.getFromDate(),
																	employeeHolidays.getToDate(),
																	employeeHolidays.getReason()));
			}
			
			return employeeHolidayListList;
} catch (HibernateException e) {
	// TODO Auto-generated catch block
	e.printStackTrace();
} catch (ParseException e) {
	// TODO Auto-generated catch block
	e.printStackTrace();
}
		
		return null;
	
	}
	
	private double getAmountDeductOnHoliday(Date fromDate, Date toDate,long employeeDtailsId) {

		
		long diff;
		long noOfHolidays=0;
		double amountDeducted=0;
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		Calendar cEnd = Calendar.getInstance();
		Calendar cStart = Calendar.getInstance();
		List<String> holidayDateList=new ArrayList<>();
		
		if(toDate!=null)
		{			
			 cStart.setTime(fromDate);
			 cEnd.setTime(toDate);
			diff = cEnd.getTime().getTime() - cStart.getTime().getTime();
			//noOfHolidays=noOfHolidays+TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS)+1;
			//amountDeducted=getAmountDeductByDates(sdf.format(fromDate), sdf.format(toDate),basicSalary);
			
			while (cStart.before(cEnd) || cStart.equals(cEnd)) {
				holidayDateList.add(new SimpleDateFormat("yyyy-MM-dd").format(cStart.getTime()));
				cStart.add(Calendar.DAY_OF_MONTH, 1);
			}
		}
		else
		{		
			cStart.setTime(fromDate);
			//noOfHolidays=noOfHolidays+1;
			//amountDeducted=getAmountDeductByDates(sdf.format(fromDate), sdf.format(fromDate),basicSalary);
			
				holidayDateList.add(new SimpleDateFormat("yyyy-MM-dd").format(cStart.getTime()));
		}    
		 
		double deductions=0;
		 for (int i=0; i<holidayDateList.size(); i++) 
		 {
			
			double basicSalary=fetchEmployeeBasicSalaryStatusByDate(holidayDateList.get(i),employeeDtailsId);
			
			double perday=basicSalary/30;
			deductions=deductions+perday;
		}
		
		return deductions;
	}
	
	@Transactional
	public List<EmployeeIncentives> fetchEmployeeIncentiveListByFilter(long employeeDetailsId,String filter,String startDate,String endDate)
	{
		try{
		
			EmployeeDetails employeeDetails=fetchEmployeeDetailsForWebApp(employeeDetailsId);
		
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		String hql = "";
		Query query = null;
		Calendar cal = Calendar.getInstance();  //Get current date/month i.e 27 Feb, 2012
		Calendar regDate = Calendar.getInstance();
		regDate.setTime(employeeDetails.getEmployeeDetailsAddedDatetime());
		if (filter.equals("Last6Months")) {
			cal.add(Calendar.MONTH, -6);
			if(regDate.compareTo(cal)<0)
			{
				hql = "from EmployeeIncentives where date(incentiveGivenDate) >= '"+dateFormat.format(cal.getTime())+"' and employeeDetails.employeeDetailsId='"+employeeDetailsId+"'";
			}
			else
			{
				hql = "from EmployeeIncentives where date(incentiveGivenDate) >= '"+dateFormat.format(regDate.getTime())+"'  and employeeDetails.employeeDetailsId='"+employeeDetailsId+"'";
			}
			query = sessionFactory.getCurrentSession().createQuery(hql);

		}
		else if (filter.equals("Last1Year")) {
			cal.add(Calendar.MONTH, -12);
			if(regDate.compareTo(cal)<0)
			{
				hql = "from EmployeeIncentives where date(incentiveGivenDate) >= '"+dateFormat.format(cal.getTime())+"'  and employeeDetails.employeeDetailsId='"+employeeDetailsId+"'";
			}
			else
			{
				hql = "from EmployeeIncentives where date(incentiveGivenDate) >= '"+dateFormat.format(regDate.getTime())+"'  and employeeDetails.employeeDetailsId='"+employeeDetailsId+"'";
			}
			query = sessionFactory.getCurrentSession().createQuery(hql);

		}
		
		else if (filter.equals("ViewAll")) {

			hql = "from EmployeeIncentives where employeeDetails.employeeDetailsId='"+employeeDetailsId+"'";

			query = sessionFactory.getCurrentSession().createQuery(hql);

		}
		else if (filter.equals("CurrentMonth")) {
			cal.setTime(dateFormat.parse(DatePicker.getCurrentMonthStartDate()));
			if(regDate.compareTo(cal)<0)
			{
				hql="from EmployeeIncentives where date(incentiveGivenDate) >= '"+DatePicker.getCurrentMonthStartDate()+"'  and date(incentiveGivenDate) <='"+DatePicker.getCurrentMonthLastDate()+"' and employeeDetails.employeeDetailsId='"+employeeDetailsId+"'";
			}
			else
			{
				hql="from EmployeeIncentives where date(incentiveGivenDate) >= '"+dateFormat.format(regDate.getTime())+"'  and date(incentiveGivenDate) <='"+DatePicker.getCurrentMonthLastDate()+"' and employeeDetails.employeeDetailsId='"+employeeDetailsId+"'";
			}
			query = sessionFactory.getCurrentSession().createQuery(hql);

		}
		else if (filter.equals("Range")) {
			cal.setTime(dateFormat.parse(startDate));
			if(regDate.compareTo(cal)<0)
			{
				hql="from EmployeeIncentives where date(incentiveGivenDate) >= '"+startDate+"' and date(incentiveGivenDate) <='"+endDate+"'  and employeeDetails.employeeDetailsId='"+employeeDetailsId+"'";
			}
			else
			{
				hql="from EmployeeIncentives where date(incentiveGivenDate) >= '"+dateFormat.format(regDate.getTime())+"'  and date(incentiveGivenDate) <='"+endDate+"' and employeeDetails.employeeDetailsId='"+employeeDetailsId+"'";
			}
			query = sessionFactory.getCurrentSession().createQuery(hql);

		}
		
		List<EmployeeIncentives> employeeIncentiveList=(List<EmployeeIncentives>)query.list();
 		return employeeIncentiveList;
		
		}catch(Exception e){
			return null;
		}
	}
	@Transactional
	public EmployeeIncentives fetchEmployeeIncentivesByEmployeeIncentivesId(long employeeIncentivesId)
	{
		
		try {
			
			return (EmployeeIncentives)sessionFactory.getCurrentSession().get(EmployeeIncentives.class,employeeIncentivesId);
		} catch (Exception e) {
			
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
		
	}
	@Transactional
	public String updateIncentive(EmployeeIncentives employeeIncentives)
	{
		
		try {
			sessionFactory.getCurrentSession().update(employeeIncentives);
			return "Success";
		} catch (Exception e) {
			
			// TODO Auto-generated catch block
			e.printStackTrace();
			return "Failed";
		}
		
	}
	
	@Transactional
	public EmployeeAreaDetails fetchEmployeeAreaDetails(String employeeDetailsId) {

		String hql="from EmployeeDetails where employeeDetailsId='"+employeeDetailsId+"'";
		Query query=sessionFactory.getCurrentSession().createQuery(hql);
		List<EmployeeDetails> list=(List<EmployeeDetails>)query.list();
		
		EmployeeAreaDetails employeeAreaDetails=new EmployeeAreaDetails();
		employeeAreaDetails.setName(list.get(0).getName());
		employeeAreaDetails.setDepartmentName(list.get(0).getEmployee().getDepartment().getName());
		employeeAreaDetails.setMobileNumber(list.get(0).getContact().getMobileNumber());
		
			employeeAreaDetails.setRouteList(fetchRouteByEmployeeId(list.get(0).getEmployee().getEmployeeId()));		
		
		return employeeAreaDetails;
	}
	
	@Transactional
	public void bookHolidayForWebApp(EmployeeHolidays employeeHolidays) {
		// TODO Auto-generated method stub
		sessionFactory.getCurrentSession().save(employeeHolidays);
	}
	
	@Transactional
	public EmployeeHolidays fetchEmployeeHolidayByEmployeeHolidayId(long employeeHolidayId)
	{
		
		try {
			
			return (EmployeeHolidays)sessionFactory.getCurrentSession().get(EmployeeHolidays.class,employeeHolidayId);
		} catch (Exception e) {
			
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
		
	}
	
	@Transactional
	public String updateEmployeeHoliday(EmployeeHolidays employeeHolidays)
	{
		
		try {
			sessionFactory.getCurrentSession().update(employeeHolidays);
			return "Success";
		} catch (Exception e) {
			
			// TODO Auto-generated catch block
			e.printStackTrace();
			return "Failed";
		}
		
	}
	
	@Transactional
	public String checkHolidayGivenOrNot(String startDate, String endDate,String employeeDetailsId) {
		String msg="";
		String hql="from EmployeeHolidays where employeeDetails.employeeDetailsId='"+employeeDetailsId+"'";// where (fromDate<='"+startDate+"' and toDate>='"+startDate+"') and (fromDate<='"+endDate+"' and toDate>='"+endDate+"')";
		Query query=sessionFactory.getCurrentSession().createQuery(hql);
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		
		List<EmployeeHolidays> list=(List<EmployeeHolidays>)query.list();
		Iterator<EmployeeHolidays> itr=list.iterator();
		
		
		while(itr.hasNext())
		{
			EmployeeHolidays employeeHolidays=itr.next();
			
			Calendar cStart = Calendar.getInstance();
			Calendar cEnd = Calendar.getInstance();
			Calendar start = Calendar.getInstance();
			Calendar end = Calendar.getInstance();
			
			try {
				 start.setTime(dateFormat.parse(startDate));
				 end.setTime(dateFormat.parse(endDate));
			} catch (Exception e) {}
			
			
			if(employeeHolidays.getFromDate()!=null && employeeHolidays.getToDate()!=null)
			{
				
				cStart.setTime(employeeHolidays.getFromDate());
				cEnd.setTime(employeeHolidays.getToDate());
				
			    while (cStart.before(cEnd) || cStart.equals(cEnd)) 
				{
			    	//System.out.println("cStart : "+cStart.getTime() +" - cEnd"+cEnd.getTime());
			    	
			    	while (start.before(end) || start.equals(end)) 
					{ 
			    		 //System.out.println("start : "+start.getTime() +" - end"+end.getTime());
				    	if(cStart.compareTo(start)==0)
						 {
				    		if(employeeHolidays.getFromDate().compareTo(employeeHolidays.getToDate()) == 0)
				    		{
				    			msg="Holiday already taken in "+dateFormat.format(employeeHolidays.getFromDate());
				    		}
				    		else
				    		{
							 msg="Holiday already taken from "+dateFormat.format(employeeHolidays.getFromDate())+" to "+dateFormat.format(employeeHolidays.getToDate());
				    		}
							 return msg;
							 //break;
						 }	
				    	start.add(Calendar.DAY_OF_MONTH, 1);
					}
			    	
			    	try {
						start.setTime(dateFormat.parse(startDate));
					} catch (Exception e) {}
			    	
					cStart.add(Calendar.DAY_OF_MONTH, 1);
				}
			}		
			
		}
		return msg;
	}
	@Transactional
	public String checkUpdatingHolidayGivenOrNot(String startDate, String endDate,String employeeDetailsId,long employeeHolidayId) {
		String msg="";
		String hql="from EmployeeHolidays where employeeDetails.employeeDetailsId='"+employeeDetailsId+"'";// where (fromDate<='"+startDate+"' and toDate>='"+startDate+"') and (fromDate<='"+endDate+"' and toDate>='"+endDate+"')";
		Query query=sessionFactory.getCurrentSession().createQuery(hql);
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		
		List<EmployeeHolidays> list=(List<EmployeeHolidays>)query.list();
		Iterator<EmployeeHolidays> itr=list.iterator();
		while(itr.hasNext())
		{
			EmployeeHolidays employeeHolidays=itr.next();
			if(employeeHolidays.getEmployeeHolidaysId()==employeeHolidayId)
			{
				itr.remove();
			}
		}
		
		Iterator<EmployeeHolidays> itr2=list.iterator();
		while(itr2.hasNext())
		{
			EmployeeHolidays employeeHolidays=itr2.next();
			
			Calendar cStart = Calendar.getInstance();
			Calendar cEnd = Calendar.getInstance();
			Calendar start = Calendar.getInstance();
			Calendar end = Calendar.getInstance();
			
			try {
				 start.setTime(dateFormat.parse(startDate));
				 end.setTime(dateFormat.parse(endDate));
			} catch (Exception e) {}
			
			
			if(employeeHolidays.getFromDate()!=null && employeeHolidays.getToDate()!=null)
			{
				
				cStart.setTime(employeeHolidays.getFromDate());
				cEnd.setTime(employeeHolidays.getToDate());
				
			    while (cStart.before(cEnd) || cStart.equals(cEnd)) 
				{
			    	//System.out.println("cStart : "+cStart.getTime() +" - cEnd"+cEnd.getTime());
			    	
			    	while (start.before(end) || start.equals(end)) 
					{ 
			    		 //System.out.println("start : "+start.getTime() +" - end"+end.getTime());
				    	if(cStart.compareTo(start)==0)
						 {
				    		if(employeeHolidays.getFromDate().compareTo(employeeHolidays.getToDate()) == 0)
				    		{
				    			msg="Holiday already taken in "+dateFormat.format(employeeHolidays.getFromDate());
				    		}
				    		else
				    		{
				    			msg="Holiday already taken from "+dateFormat.format(employeeHolidays.getFromDate())+" to "+dateFormat.format(employeeHolidays.getToDate());
				    		}
							 return msg;
							 //break;
						 }	
				    	start.add(Calendar.DAY_OF_MONTH, 1);
					}
			    	
			    	try {
						start.setTime(dateFormat.parse(startDate));
					} catch (Exception e) {}
			    	
					cStart.add(Calendar.DAY_OF_MONTH, 1);
				}
			}		
			
		}
		return msg;
	}
	@Transactional
	public void giveIncentives(EmployeeIncentives employeeIncentives) {
		// TODO Auto-generated method stub
		sessionFactory.getCurrentSession().save(employeeIncentives);
	}
	@Transactional
	public void givePayment(EmployeeSalary employeeSalary)
	{
		sessionFactory.getCurrentSession().save(employeeSalary);
	}
	@Transactional
	public EmployeePaymentModel openPaymentModel(String employeeDetailsId){
		String  hql="from EmployeeDetails where employeeDetailsId='"+employeeDetailsId+"'";
		Query query=sessionFactory.getCurrentSession().createQuery(hql);
		
		
		List<EmployeeDetails> list=(List<EmployeeDetails>)query.list();
		if(list.isEmpty())
		{
			return null;
		}
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		EmployeePaymentModel employeePaymentModel=null;
		Iterator<EmployeeDetails> itr=list.iterator();
		while(itr.hasNext())
		{
			EmployeeDetails employeeDetails=itr.next();
			
			double deduction=getDeductionAmount(dateFormat.format(employeeDetails.getEmployeeDetailsAddedDatetime()), dateFormat.format(new Date()), "CurrentMonth", employeeDetails );
			double incentives=getIncentivesForWebApp(dateFormat.format(employeeDetails.getEmployeeDetailsAddedDatetime()), dateFormat.format(new Date()), "CurrentMonth", employeeDetails );			
			double paidCurrentMonthSalary=getPaidSalary(dateFormat.format(employeeDetails.getEmployeeDetailsAddedDatetime()), dateFormat.format(new Date()), "CurrentMonth", employeeDetails );
			double totalAmount=findTotalSalaryTwoDates(dateFormat.format(employeeDetails.getEmployeeDetailsAddedDatetime()), dateFormat.format(new Date()), "CurrentMonth", employeeDetails )+incentives-deduction;
			double unpaidCurrentMonthSalary=totalAmount-paidCurrentMonthSalary;
			
			employeePaymentModel=new EmployeePaymentModel(employeeDetails.getName(),
					totalAmount, 
					paidCurrentMonthSalary, 
					unpaidCurrentMonthSalary);
			
		}
			return employeePaymentModel;	
	}
	@Transactional
	public String sendSMSTOEmployee(String employeeDetailsIdList,String smsText) {

		try {
			String[] employeeDetailsIdList2 = employeeDetailsIdList.split(",");
			
			for(int i=0; i<employeeDetailsIdList2.length; i++)
			{
				EmployeeDetails employeeDetails=fetchEmployeeDetailsForWebApp(Long.parseLong(employeeDetailsIdList2[i]));
				SendSMS.sendSMS(Long.parseLong(employeeDetails.getContact().getMobileNumber()), smsText);
				System.out.println("SMS send to : "+employeeDetailsIdList2[i]);
			}
			
			return "Success";
		} catch (Exception e) {
			System.out.println("sms sending failed "+e.toString());
			return "Failed";
			
		}
		
	}
	
	@Transactional
	public List<EmployeeViewModel> fetchEmployeeDetail(String employeeDetailId) {
	
	
		String hql="from EmployeeDetails where employeeDetailsId='"+employeeDetailId+"'";
		Query query=sessionFactory.getCurrentSession().createQuery(hql);
		
		List<EmployeeDetails> employeeList = (List<EmployeeDetails>) query.list();
		
		if (employeeList.isEmpty()) {
			return null;
		}
		
		List<EmployeeViewModel> employeeViewModelList=new ArrayList<>();
		
		EmployeeDetails employeeDetails=employeeList.get(0);
		
		double paidCurrentMonthSalary=getPaidSalary(DatePicker.getCurrentMonthStartDate(), DatePicker.getCurrentMonthLastDate(), "CurrentMonth", employeeDetails);
		//double unpaidCurrentMonthSalary=findTotalSalaryTwoDates(DatePicker.getCurrentMonthStartDate(), DatePicker.getCurrentMonthLastDate(), "CurrentMonth", employeeDetails)-paidCurrentMonthSalary;
		long noOfHolidays=getNoOfHolidays(DatePicker.getCurrentMonthStartDate(), DatePicker.getCurrentMonthLastDate(), "CurrentMonth", employeeDetails);
		double incentives=getIncentivesForWebApp(DatePicker.getCurrentMonthStartDate(), DatePicker.getCurrentMonthLastDate(), "CurrentMonth", employeeDetails );
		double deduction=getDeductionAmount(DatePicker.getCurrentMonthStartDate(), DatePicker.getCurrentMonthLastDate(), "CurrentMonth", employeeDetails );
		double totalAmount=findTotalSalaryTwoDates(DatePicker.getCurrentMonthStartDate(), DatePicker.getCurrentMonthLastDate(), "CurrentMonth", employeeDetails)+incentives-deduction;
		double unpaidCurrentMonthSalary=totalAmount-paidCurrentMonthSalary;
		
		int srno=1;
		employeeViewModelList.add(new EmployeeViewModel(srno, 
				employeeDetails.getEmployeeDetailsId(),
				employeeDetails.getEmployeeDetailsGenId(), 
				employeeDetails.getName(),
				employeeDetails.getContact().getMobileNumber(), 
				employeeDetails.getContact().getEmailId(), 
				totalAmount,
				paidCurrentMonthSalary, 
				unpaidCurrentMonthSalary, 
				incentives,
				(int)noOfHolidays, 
				employeeDetails.getEmployee().getDepartment().getName()
				,employeeDetails.isStatus()));
		
		return employeeViewModelList;
	
	}
	
	public List<EmployeeDetails> fetchEmployeeDetailsByDepartmentId(long deparmentId)
	{
		String hql="from EmployeeDetails where employee.department.departmentId="+deparmentId;
		Query query=sessionFactory.getCurrentSession().createQuery(hql);
		List<EmployeeDetails> employeeDetails=(List<EmployeeDetails>)query.list();
		if(employeeDetails.isEmpty())
		{
			return null;
		}
		return employeeDetails;
	}
	@Transactional
    public List<Route> fetchRouteByEmployeeId(long employeeId) {

        String hql="";
        Query query;
        hql="from EmployeeDetails where employee.employeeId="+employeeId;
        query=sessionFactory.getCurrentSession().createQuery(hql);
        List<EmployeeDetails> list=(List<EmployeeDetails>)query.list();
        EmployeeDetails employeeDetails=list.get(0);
            
        List<EmployeeRouteList> employeeRouteList= fetchEmployeeRouteListByEmployeeDetailsId(employeeDetails.getEmployeeDetailsId());
        List<Route> routeList=new ArrayList<>();
        
        if(employeeDetails.getEmployee().getDepartment().getName().equals(Constants.GATE_KEEPER_DEPT_NAME))
        {
        	for(EmployeeRouteList  employeeRoute: employeeRouteList)
        	{
        		routeList.addAll(fetchRouteListByClusterId(employeeRoute.getCluster().getClusterId()));
        	}
        	
        	return routeList;
        }
        else  if(employeeDetails.getEmployee().getDepartment().getName().equals(Constants.AREA_SALESMAN_DEPT_NAME))
        {
        	for(EmployeeRouteList  employeeRoute: employeeRouteList)
        	{
        		routeList.addAll(fetchRouteListByAreaId(employeeRoute.getArea().getAreaId()));
        	}
        	
        	return routeList;
        }
        else  
        {
        	
        	routeList.add(employeeRouteList.get(0).getRoute());
        	return routeList;
        }
        
        
    }
	
	@Transactional
	public List<Route> fetchRouteListByAreaId(long areaId)
	{
		String hql="from Route where area.areaId="+areaId;
		Query query=sessionFactory.getCurrentSession().createQuery(hql);
		List<Route> routeList=(List<Route>)query.list();
		if(routeList.isEmpty())
		{
			return null;
		}
		return routeList;
	}
	
	@Transactional
	public List<Route> fetchRouteListByClusterId(long clusterId)
	{
		String hql="from Route where area.cluster.clusterId="+clusterId;
		Query query=sessionFactory.getCurrentSession().createQuery(hql);
		List<Route> routeList=(List<Route>)query.list();
		if(routeList.isEmpty())
		{
			return null;
		}
		return routeList;
	}
	
	@Transactional
	public String checkAreaAssignedToASM(long areaId)
	{		
		Query query1=sessionFactory.getCurrentSession().createQuery("from EmployeeRouteList where area.areaId="+areaId);
		List<EmployeeRouteList> employeeRouteList=(List<EmployeeRouteList>)query1.list();
		if(employeeRouteList.isEmpty())
		{
			return "Success";
		}				
		return "Failed";
	}
	
	@Transactional
	public String checkAreaAssignedToASMUpdate(long areaId,long employeeDetailsId)
	{		
		Query query1=sessionFactory.getCurrentSession().createQuery("from EmployeeRouteList where area.areaId="+areaId+" and employeeDetails.employeeDetailsId<>"+employeeDetailsId);
		List<EmployeeRouteList> employeeRouteList=(List<EmployeeRouteList>)query1.list();
		if(employeeRouteList.isEmpty())
		{
			return "Success";
		}				
		return "Failed";
	}
	
	@Transactional
	public String checkRouteAssignedToSM(long routeId)
	{		
		Query query1=sessionFactory.getCurrentSession().createQuery("from EmployeeRouteList where route.routeId="+routeId);
		List<EmployeeRouteList> employeeRouteList=(List<EmployeeRouteList>)query1.list();
		if(employeeRouteList.isEmpty())
		{
			return "Success";
		}				
		return "Failed";
	}
	
	@Transactional
	public String checkRouteAssignedToSMUpdate(long areaId,long employeeDetailsId)
	{		
		Query query1=sessionFactory.getCurrentSession().createQuery("from EmployeeRouteList where route.routeId="+areaId+" and employeeDetails.employeeDetailsId<>"+employeeDetailsId);
		List<EmployeeRouteList> employeeRouteList=(List<EmployeeRouteList>)query1.list();
		if(employeeRouteList.isEmpty())
		{
			return "Success";
		}				
		return "Failed";
	}
	
	@Transactional
	public SMReportModel fetchSMReportModel(String startDate,String endDate,String range,String type) 
	{		
		double totalAmount=0;
		int noOfSMandASM=0;
		
		ClusterDAOImpl clusterDAOImpl=new ClusterDAOImpl(sessionFactory);
		List<Cluster> clusterList=clusterDAOImpl.fetchClusterList();
		DecimalFormat decimalFormat=new DecimalFormat("#0.00");
		SimpleDateFormat dateFormat=new SimpleDateFormat("yyyy-MM-dd");
		
		String hql="from EmployeeDetails where employee.department.name in ('"+type+"')";
		Query query=sessionFactory.getCurrentSession().createQuery(hql);
		List<EmployeeDetails> employeeDetailsList=(List<EmployeeDetails>)query.list();
		if(employeeDetailsList.isEmpty())
		{
			return new SMReportModel(0, 0, clusterList, null, type);
		}
		
		List<SMReportDetails> sMReportDetailsList=new ArrayList<>();
		OrderDetailsDAOImpl orderDetailsDAOImpl=new OrderDetailsDAOImpl(sessionFactory);
		long srNo=1;
		
		
		for(EmployeeDetails employeeDetails : employeeDetailsList)
		{	
			List<OrderDetails> orderDetailsList=orderDetailsDAOImpl.fetchOrderDetailsForSMReport(startDate, endDate, range, employeeDetails,type);
			
			List<EmployeeRouteList> employeeRouteList=fetchEmployeeRouteListByEmployeeDetailsId(employeeDetails.getEmployeeDetailsId());
		    long totalNoOfOrder=0;
		    double totalOrdersAmount=0;
			if(orderDetailsList!=null)
			{
				totalNoOfOrder=orderDetailsList.size();
				for(OrderDetails orderDetails: orderDetailsList)
				{
					totalOrdersAmount+=orderDetails.getTotalAmountWithTax();
					totalAmount+=orderDetails.getTotalAmountWithTax();
				}
			}
			
			if(employeeDetails.getEmployee().getDepartment().getName().equals(type))
			{				
				sMReportDetailsList.add(new SMReportDetails(srNo,employeeDetails.getEmployeeDetailsId(), employeeDetails.getName(), "", "", employeeRouteList.get(0).getArea().getCluster().getName(), String.valueOf(totalNoOfOrder), String.valueOf(totalOrdersAmount)));
				srNo++;
			}
			else
			{
				sMReportDetailsList.add(new SMReportDetails(srNo,employeeDetails.getEmployeeDetailsId(), employeeDetails.getName(), employeeRouteList.get(0).getRoute().getRouteNo(), employeeRouteList.get(0).getRoute().getArea().getName(), employeeRouteList.get(0).getRoute().getArea().getCluster().getName(), String.valueOf(totalNoOfOrder), String.valueOf(totalOrdersAmount)));
				srNo++;
			}
		}
		noOfSMandASM=employeeDetailsList.size();
		totalAmount=Double.parseDouble(decimalFormat.format(totalAmount));
		
		return new SMReportModel(totalAmount, noOfSMandASM, clusterList, sMReportDetailsList, type);		
	}
	
	@Transactional
	public SMReportOrderDetails fetchSMReportOrderDetails(String startDate,String endDate,String range,long employeeDetailsId){
		
		EmployeeDetails employeeDetails=fetchEmployeeDetailsForWebApp(employeeDetailsId);
		OrderDetailsDAOImpl orderDetailsDAOImpl=new OrderDetailsDAOImpl(sessionFactory);
		SimpleDateFormat dateFormat=new SimpleDateFormat("yyyy-MM-dd");
		DecimalFormat decimalFormat=new DecimalFormat("#0.00");
		List<SMReportOrders> smReportOrdersList=new ArrayList<>();
		double totalAmount=0;
		String aSMName="",clusterName="", routeName="",areaName="";
		
		if(employeeDetails.getEmployee().getDepartment().getName().equals(Constants.AREA_SALESMAN_DEPT_NAME))
		{
			List<OrderDetails> orderDetailsList=orderDetailsDAOImpl.fetchOrderDetailsForSMReport(startDate, endDate, range, employeeDetails,employeeDetails.getEmployee().getDepartment().getName());
			List<EmployeeRouteList> employeeRouteList=fetchEmployeeRouteListByEmployeeDetailsId(employeeDetails.getEmployeeDetailsId());
						
			clusterName=employeeRouteList.get(0).getArea().getCluster().getName(); 
			routeName="";
			areaName=employeeRouteList.get(0).getArea().getName();
			List<Route> routeList=fetchRouteListByAreaId(employeeRouteList.get(0).getArea().getAreaId());
			if(routeList!=null)
			{
				for(Route route: routeList)
				{
					routeName=routeName+route.getRouteNo()+",";
				}
			}
			routeName=routeName.substring(0, routeName.length()-2);
			
 			long srno=1;
			if(orderDetailsList!=null)
			{
				for(OrderDetails orderDetails: orderDetailsList)
				{
					smReportOrdersList.add(new SMReportOrders(srno, orderDetails.getOrderId(), orderDetails.getBooth().getBoothNo(), orderDetails.getBooth().getOwnerName(), decimalFormat.format(orderDetails.getTotalAmountWithTax()), dateFormat.format(orderDetails.getOrderDetailsAddedDatetime()), dateFormat.format(orderDetails.getDeliveryDate())));
					totalAmount+=orderDetails.getTotalAmountWithTax();
					srno++;
				}
			}
		}
		else 
		{
			List<OrderDetails> orderDetailsList=orderDetailsDAOImpl.fetchOrderDetailsForSMReport(startDate, endDate, range, employeeDetails,employeeDetails.getEmployee().getDepartment().getName());
			List<EmployeeRouteList> employeeRouteList=fetchEmployeeRouteListByEmployeeDetailsId(employeeDetails.getEmployeeDetailsId());
			EmployeeDetails employeeDetailsASM=fetchASMDetailsBySMEmployeeDetailsId(employeeDetailsId);
			if(employeeDetailsASM==null)
			{
				aSMName="Not Assigned";
			}
			else
			{
				aSMName=employeeDetailsASM.getName();
			}
			
			clusterName=employeeRouteList.get(0).getRoute().getArea().getCluster().getName(); 
			routeName=employeeRouteList.get(0).getRoute().getRouteNo();
			areaName=employeeRouteList.get(0).getRoute().getArea().getName(); 
			
			long srno=1;
			if(orderDetailsList!=null)
			{
				for(OrderDetails orderDetails: orderDetailsList)
				{
					smReportOrdersList.add(new SMReportOrders(srno, orderDetails.getOrderId(), orderDetails.getBooth().getBoothNo(), orderDetails.getBooth().getOwnerName(), decimalFormat.format(orderDetails.getTotalAmountWithTax()), dateFormat.format(orderDetails.getOrderDetailsAddedDatetime()), dateFormat.format(orderDetails.getDeliveryDate())));
					totalAmount+=orderDetails.getTotalAmountWithTax();
					srno++;
				}     
			}
		}
		
		totalAmount=Double.parseDouble(decimalFormat.format(totalAmount));
		return new SMReportOrderDetails(employeeDetails.getName(), employeeDetails.getEmployeeDetailsId(),employeeDetails.getEmployee().getDepartment().getName(),areaName, employeeDetails.getContact().getMobileNumber(), clusterName, routeName, aSMName, decimalFormat.format(totalAmount), smReportOrdersList);
	}
	
	public EmployeeDetails fetchASMDetailsBySMEmployeeDetailsId(long employeeDetailsId)
	{
		EmployeeDetails employeeDetails=fetchEmployeeDetailsForWebApp(employeeDetailsId);
		List<EmployeeRouteList> employeeRouteList=fetchEmployeeRouteListByEmployeeDetailsId(employeeDetails.getEmployeeDetailsId());
	
		String hql="from EmployeeRouteList where area.areaId="+employeeRouteList.get(0).getRoute().getArea().getAreaId();
		Query query=sessionFactory.getCurrentSession().createQuery(hql);
		List<EmployeeRouteList> employeeRouteList2=(List<EmployeeRouteList>)query.list();
		if(employeeRouteList2.isEmpty())
		{
			return null;
		}
		EmployeeDetails employeeDetails2=employeeRouteList2.get(0).getEmployeeDetails();
		return employeeDetails2;
	}
	
	// here we are fetching Asm by sm id
		@Transactional
		public List<Employee> fetchAsmBySMId(long employeeId){
			String hql = "from EmployeeASMAndSM where employeeDetailsSMId.employee.employeeId=" + employeeId +" and employeeDetailsSMId.status=false";
			Query query = sessionFactory.getCurrentSession().createQuery(hql);

			List<EmployeeASMAndSM> employeeASMAndSMList = (List<EmployeeASMAndSM>) query.list();

			List<Employee> employeeList = new ArrayList<>();
			for (EmployeeASMAndSM employeeASMAndSM : employeeASMAndSMList) {
				employeeList.add(employeeASMAndSM.getEmployeeDetailsASMId().getEmployee());
			}
			if (employeeList.isEmpty()) {
				return null;
			}
			return employeeList;
		}
		// here we are fetching Employee By EmployeeId
		@Transactional
		public Employee fetchEmployeeByEmpId(long employeeId){
			String hql="from Employee where employeeId="+employeeId;
			Query query=sessionFactory.getCurrentSession().createQuery(hql);
			List<Employee> employeeList=(List<Employee>)query.list();
			if(employeeList.isEmpty()){
				return  null;
			}
			return employeeList.get(0);
			
		}
	
}
