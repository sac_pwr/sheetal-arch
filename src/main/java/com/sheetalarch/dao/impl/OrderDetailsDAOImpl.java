package com.sheetalarch.dao.impl;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.sheetalarch.admin.models.OrderReportModel;
import com.sheetalarch.admin.models.SalesManReportModel;
import com.sheetalarch.dao.OrderDetailsDAO;
import com.sheetalarch.entity.Area;
import com.sheetalarch.entity.Booth;
import com.sheetalarch.entity.DispatchOrderDetails;
import com.sheetalarch.entity.DispatchOrderProductDetails;
import com.sheetalarch.entity.Employee;
import com.sheetalarch.entity.EmployeeDetails;
import com.sheetalarch.entity.OrderDetails;
import com.sheetalarch.entity.OrderProductDetails;
import com.sheetalarch.entity.OrderStatus;
import com.sheetalarch.entity.OrderUsedBrand;
import com.sheetalarch.entity.OrderUsedCategories;
import com.sheetalarch.entity.OrderUsedProduct;
import com.sheetalarch.entity.Product;
import com.sheetalarch.entity.ReOrderDetails;
import com.sheetalarch.entity.ReOrderProductDetails;
import com.sheetalarch.entity.Route;
import com.sheetalarch.models.CancelOrderRequest;
import com.sheetalarch.models.DispatchProductNameAndQtyListResponse;
import com.sheetalarch.models.GkDispatchProductReportRequest;
import com.sheetalarch.models.GkDispatchProductReportResponse;
import com.sheetalarch.models.GkDispatchedOrderListResponse;
import com.sheetalarch.models.GkProductDetailsReportResponse;
import com.sheetalarch.models.OrderIssueRequest;
import com.sheetalarch.models.OrderListGKRequest;
import com.sheetalarch.models.OrderListResponse;
import com.sheetalarch.models.OrderRequest;
import com.sheetalarch.models.ReOrderDeliveryRequest;
import com.sheetalarch.utils.Constants;
import com.sheetalarch.utils.InvoiceNumberGenerate;
import com.sheetalarch.utils.Notification;
import com.sheetalarch.utils.OrderIdGenerator;
import com.sheetalarch.utils.ReOrderIdGenerator;
import com.sheetalarch.utils.SendSMS;

@Repository("orderDetailsDAO")
@Component
public class OrderDetailsDAOImpl implements OrderDetailsDAO {

	@Autowired
	OrderDetails orderDetails;

	@Autowired
	OrderStatus orderStatus;
	
	@Autowired
	DispatchOrderDetails dispatchOrderDetails;

	@Autowired
	SessionFactory sessionFactory;

	@Autowired
	Booth booth;

	public OrderDetailsDAOImpl(SessionFactory sessionFactory) {
		this.sessionFactory=sessionFactory;
	}

	@Transactional
	public OrderStatus fetchOrderStatus(String orderStatusName) {
		String hql = "from OrderStatus where status LIKE '" + orderStatusName + "'";
		Query query = sessionFactory.getCurrentSession().createQuery(hql);

		List<OrderStatus> list = (List<OrderStatus>) query.list();
		if (list.isEmpty()) {
			return null;
		}
		return list.get(0);
	}

	@Transactional
	public OrderDetails fetchOrderDetailsByOrderId(String orderDetailsId) {

		String hql = "from OrderDetails where orderId='" + orderDetailsId + "'";
		Query query = sessionFactory.getCurrentSession().createQuery(hql);

		List<OrderDetails> list = (List<OrderDetails>) query.list();

		if (list.isEmpty()) {
			return null;
		}

		return list.get(0);
	}

	@Transactional
	public ReOrderDetails fetchReOrderDetailsByReOrderId(String reOrderDetailsId) {

		String hql = "from ReOrderDetails where reOrderDetailsId='" + reOrderDetailsId + "'";
		Query query = sessionFactory.getCurrentSession().createQuery(hql);

		List<ReOrderDetails> list = (List<ReOrderDetails>) query.list();

		if (list.isEmpty()) {
			return null;
		}

		return list.get(0);
	}
	
	@Transactional
	public List<ReOrderProductDetails> fetchReOrderProductDetailsByReOrderId(String reOrderDetailsId){
		
		String hql="from ReOrderProductDetails where reOrderDetails.reOrderDetailsId='" + reOrderDetailsId + "'";
		Query query=sessionFactory.getCurrentSession().createQuery(hql);
		List<ReOrderProductDetails> list=(List<ReOrderProductDetails>)query.list();
		if(list.isEmpty()){
			return null;
		}
		return list;
	}
	@Transactional
	public String bookOrder(OrderRequest orderRequest) {

		OrderDetails orderDetails = orderRequest.getOrderDetails();

		OrderIdGenerator orderIdGenerator = new OrderIdGenerator(sessionFactory);
		orderDetails.setOrderId(orderIdGenerator.generate());

		orderDetails.setOrderDetailsAddedDatetime(new Date());
		orderDetails.setPaymentPeriodDays(0);

		OrderStatus orderStatus = fetchOrderStatus(Constants.ORDER_STATUS_BOOKED);
		// orderDetails.getOrderStatus().setStatus(Constants.ORDER_STATUS_BOOKED);

		orderDetails.setOrderStatus(orderStatus);
		// orderDetails.getOrderStatus().setStatus(orderStatus);
		orderDetails.setPayStatus(false);
		// orderDetails.setEmployeeGK(employeeGK);
		
		

		sessionFactory.getCurrentSession().save(orderDetails);

		for (OrderProductDetails orderProductDetails : orderRequest.getOrderProductDetailList()) {
			
			OrderProductDetails orderProductDetails2=new OrderProductDetails();
			orderProductDetails2.setOrderDetails(orderDetails);
			
			//fetching product by productId
			ProductDAOImpl productDAOImpl=new ProductDAOImpl(sessionFactory);
			Product product=productDAOImpl.fetchProductByProductId(orderProductDetails.getProduct().getProductId());
			
			// updating orderUsedProdct entity
			OrderUsedBrand orderUsedBrand=new OrderUsedBrand();
			orderUsedBrand.setName(product.getBrand().getName());
			sessionFactory.getCurrentSession().save(orderUsedBrand);
			
			// updating orderCategory entity
			OrderUsedCategories orderUsedCategories=new OrderUsedCategories();
			orderUsedCategories.setCategoryName(product.getCategories().getCategoryName());
			orderUsedCategories.setCgst(product.getCategories().getCgst());
			orderUsedCategories.setHsnCode(product.getCategories().getHsnCode());
			orderUsedCategories.setIgst(product.getCategories().getIgst());
			orderUsedCategories.setSgst(product.getCategories().getSgst());
			sessionFactory.getCurrentSession().save(orderUsedCategories);
			
			// updating OrderUsedProduct
			OrderUsedProduct orderUsedProduct=new OrderUsedProduct();
			orderUsedProduct.setBrand(orderUsedBrand);
			orderUsedProduct.setCategories(orderUsedCategories);
			orderUsedProduct.setProduct(product);
			orderUsedProduct.setProductCode(product.getProductCode());
			orderUsedProduct.setCurrentQuantity(product.getCurrentQuantity());
			orderUsedProduct.setProductImage(product.getProductImage());
			orderUsedProduct.setProductName(product.getProductName());
			orderUsedProduct.setRate(product.getRate());
			orderUsedProduct.setInterRate(product.getInterRate());
			orderUsedProduct.setIntraRate(product.getIntraRate());
			sessionFactory.getCurrentSession().save(orderUsedProduct);
			
			orderProductDetails.setProduct(orderUsedProduct);
			
			
			sessionFactory.getCurrentSession().save(orderProductDetails);
		}

		
/*		Booth booth = orderDetails.getBooth();

		// System.out.println("Testing");
		EmployeeDetailsDAOImpl employeeDetailsDAOImpl = new EmployeeDetailsDAOImpl(sessionFactory);

		EmployeeDetails employeeDetails = employeeDetailsDAOImpl
				.getEmployeeDetailsByemployeeId(orderDetails.getEmployeeASMandSM().getEmployeeId());

		String salesPersonId = employeeDetails.getEmployeeDetailsGenId();
		employeeDetails = employeeDetailsDAOImpl.getGateKeeperEmployeeDetailsByrouteId(booth.getRoute().getRouteId());

		try {
			Notification.sendNotificationToGateKeeperForOrder(employeeDetails.getToken(), salesPersonId,
					booth.getShopName(), orderDetails.getOrderId());
		} catch (Exception e) {
			System.err.println("sendNotificationToGateKeeperForOrder error : " + e.toString());
		}
*/
		return orderDetails.getOrderId();
	}

	@Transactional
	public void updateOrderDetailsPaymentDays(OrderDetails orderDetails) {

		sessionFactory.getCurrentSession().update(orderDetails);

		// updating dispatched orderDetails

	}

	@Transactional
	public void updateDispatchProduct(DispatchOrderDetails dispatchOrderDetailsList, OrderDetails orderDetails,
			Employee gKemployee, Area salemanArea) {

		/*
		 * AreaDAOImpl areaDAOImpl=new AreaDAOImpl(sessionFactory); Cluster
		 * cluster=areaDAOImpl.fetchClusterBySalesmanId(dispatchOrderDetailsList
		 * .getEmployeeASMAndSM().getEmployeeId()); Employee
		 * employee=areaDAOImpl.fetchGKEmployeeByClusterId(cluster.getClusterId(
		 * ));
		 */

		OrderStatus orderStatus = fetchOrderStatus(Constants.ORDER_STATUS_BOOKED);
		Date date = new Date();
		// DispatchOrderDetails
		// dispatchOrderDetailsList=fetchDispatchOrderDetailsByemployeeIdAndDeliveryDate(orderDetails.getEmployeeASMandSM().getEmployeeId());
		if (dispatchOrderDetailsList == null) {

			dispatchOrderDetails.setSalemanArea(salemanArea);
			dispatchOrderDetails.setEmployeeGk(gKemployee);
			dispatchOrderDetails.setOrderStatus(orderStatus);
			dispatchOrderDetails.setEmployeeASMAndSM(orderDetails.getEmployeeASMandSM());
			dispatchOrderDetails.setTotalQty(orderDetails.getTotalQuantity());
			dispatchOrderDetails.setTotalAmount(orderDetails.getTotalAmountWithTax());
			dispatchOrderDetails.setDeliveryDate(orderDetails.getDeliveryDate());
			dispatchOrderDetails.setDispatchedAddedDateTime(new Date());
			sessionFactory.getCurrentSession().save(dispatchOrderDetails);

		} else if (orderDetails.getEmployeeASMandSM().getEmployeeId() == dispatchOrderDetailsList.getEmployeeASMAndSM()
				.getEmployeeId()
				&& (orderDetails.getDeliveryDate()).equals(dispatchOrderDetailsList.getDeliveryDate())) {

			double a = dispatchOrderDetailsList.getTotalAmount();
			double b = orderDetails.getTotalAmountWithTax();
			dispatchOrderDetails.setSalemanArea(salemanArea);
			dispatchOrderDetails.setEmployeeGk(gKemployee);

			dispatchOrderDetails.setDispatchedId(dispatchOrderDetailsList.getDispatchedId());
			dispatchOrderDetails
					.setTotalAmount(dispatchOrderDetailsList.getTotalAmount() + orderDetails.getTotalAmountWithTax());
			dispatchOrderDetails.setTotalQty(dispatchOrderDetailsList.getTotalQty() + orderDetails.getTotalQuantity());
			dispatchOrderDetails.setOrderStatus(orderStatus);
			dispatchOrderDetails.setEmployeeASMAndSM(orderDetails.getEmployeeASMandSM());
			dispatchOrderDetails.setDeliveryDate(orderDetails.getDeliveryDate());
			dispatchOrderDetails.setDispatchedAddedDateTime(new Date());
			sessionFactory.getCurrentSession().update(dispatchOrderDetails);

		} else {

			dispatchOrderDetails.setSalemanArea(salemanArea);
			dispatchOrderDetails.setEmployeeGk(gKemployee);
			dispatchOrderDetails.setOrderStatus(orderStatus);
			dispatchOrderDetails.setEmployeeASMAndSM(orderDetails.getEmployeeASMandSM());
			dispatchOrderDetails.setDeliveryDate(orderDetails.getDeliveryDate());
			dispatchOrderDetails.setDispatchedAddedDateTime(new Date());
			dispatchOrderDetails.setTotalAmount(orderDetails.getTotalAmountWithTax());
			dispatchOrderDetails.setTotalQty(orderDetails.getTotalQuantity());
			sessionFactory.getCurrentSession().save(dispatchOrderDetails);
		}

	}

	// here we updating dispatchProduct details
	@Transactional
	public void updateDispatchProductDetails(List<OrderProductDetails> orderProductDetailsList,
			List<DispatchOrderProductDetails> dispatchOrderProductDetailsList,
			DispatchOrderDetails dispatchOrderDetails) {

		if (dispatchOrderProductDetailsList == null) {

			for (OrderProductDetails orderProductDetails : orderProductDetailsList) {
				DispatchOrderProductDetails dispatchOrderProductDetails2 = new DispatchOrderProductDetails();
				dispatchOrderProductDetails2.setProduct(orderProductDetails.getProduct());
				dispatchOrderProductDetails2.setTotalQty(orderProductDetails.getPurchaseQuantity());
				dispatchOrderProductDetails2.setTotalAmt(orderProductDetails.getPurchaseAmount());
				dispatchOrderProductDetails2.setDispatchOrderDetails(dispatchOrderDetails);
				sessionFactory.getCurrentSession().save(dispatchOrderProductDetails2);

			}
		} else {

			List<Long> productKeys = new ArrayList<>();

			for (OrderProductDetails orderProductDetails : orderProductDetailsList) {
				// DispatchOrderProductDetails dispatchOrderProductDetails2=new
				// DispatchOrderProductDetails();
				for (DispatchOrderProductDetails dispatchOrderProductDetails : dispatchOrderProductDetailsList) {
					DispatchOrderProductDetails dispatchOrderProductDetails2 = new DispatchOrderProductDetails();
					if (dispatchOrderProductDetails.getProduct().getProductId() == orderProductDetails.getProduct()
							.getProductId()) {
						dispatchOrderProductDetails2
								.setDispatchedProductId(dispatchOrderProductDetails.getDispatchedProductId());
						dispatchOrderProductDetails2.setTotalQty(
								dispatchOrderProductDetails.getTotalQty() + orderProductDetails.getPurchaseQuantity());
						dispatchOrderProductDetails2.setTotalAmt(
								dispatchOrderProductDetails.getTotalAmt() + orderProductDetails.getPurchaseAmount());
						dispatchOrderProductDetails2.setDispatchOrderDetails(dispatchOrderDetails);
						dispatchOrderProductDetails2.setProduct(dispatchOrderProductDetails.getProduct());
						productKeys.add(orderProductDetails.getProduct().getProductId());
						sessionFactory.getCurrentSession().update(dispatchOrderProductDetails2);
						break;
					}
				}

				// sessionFactory.getCurrentSession().update(dispatchOrderProductDetails2);
			}

			for (OrderProductDetails orderProductDetails : orderProductDetailsList) {
				// DispatchOrderProductDetails dispatchOrderProductDetails2=new
				// DispatchOrderProductDetails();
				for (DispatchOrderProductDetails dispatchOrderProductDetails : dispatchOrderProductDetailsList) {
					DispatchOrderProductDetails dispatchOrderProductDetails2 = new DispatchOrderProductDetails();
					if (dispatchOrderProductDetails.getProduct().getProductId() != orderProductDetails.getProduct()
							.getProductId() && !productKeys.contains(orderProductDetails.getProduct().getProductId())) {
						dispatchOrderProductDetails2.setProduct(orderProductDetails.getProduct());
						dispatchOrderProductDetails2.setTotalAmt(orderProductDetails.getPurchaseAmount());
						dispatchOrderProductDetails2.setTotalQty(orderProductDetails.getPurchaseQuantity());
						dispatchOrderProductDetails2.setDispatchOrderDetails(dispatchOrderDetails);
						sessionFactory.getCurrentSession().save(dispatchOrderProductDetails2);
						break;
					}
				}

				// sessionFactory.getCurrentSession().save(dispatchOrderProductDetails2);
			}
		}
	}

	@Transactional
	public List<DispatchOrderProductDetails> fetchDispatchOrderProductListByDispatchId(long dispatchedId) {

		String hql = "from DispatchOrderProductDetails where dispatchOrderDetails.dispatchedId=" + dispatchedId;
		Query query = sessionFactory.getCurrentSession().createQuery(hql);

		List<DispatchOrderProductDetails> dispatchOrderProductDetailsList = (List<DispatchOrderProductDetails>) query
				.list();
		if (dispatchOrderProductDetailsList.isEmpty()) {
			return null;
		} else {
			return dispatchOrderProductDetailsList;
		}
	}

	
	// here we are fetching OrderProductDetails By orderID
	@Transactional
	public List<OrderProductDetails> fetchOrderProductDetailsByOrderId(String orderId) {
		// TODO Auto-generated method stub

		String hql = "from OrderProductDetails where orderDetails.orderId='" + orderId + "'";
		Query query = sessionFactory.getCurrentSession().createQuery(hql);
		
		List<OrderProductDetails> orderProductDetailsList = (List<OrderProductDetails>) query.list();
		if (orderProductDetailsList.isEmpty()) {
			return null;
		}
		@SuppressWarnings("unused")
		List<OrderProductDetails> orderProductDetailsList1 = makeOrderProductDetailsListImageMNull(
				orderProductDetailsList);

		return orderProductDetailsList1;
	}
	
	@Transactional
	public List<ReOrderProductDetails> fecthReOrderProductDetailsByOrderId(String orderId){
		
		String hql = "from ReOrderProductDetails  where reOrderDetails.orderDetails.orderId='" + orderId + "'";
		Query query = sessionFactory.getCurrentSession().createQuery(hql);
		
		List<ReOrderProductDetails> reOrderProductDetailsList=(List<ReOrderProductDetails>)query.list();
		if (reOrderProductDetailsList.isEmpty()) {
			return null;
		}
		@SuppressWarnings("unused")
		List<ReOrderProductDetails> reOrderProductDetails =makeReOrderProductDetailsListImageMNull(reOrderProductDetailsList);

		return reOrderProductDetails;
	}
		
	
	@Transactional
	public List<OrderProductDetails> fetchOrderProductDetailsForReOrderByOrderId(List<OrderProductDetails> orderProductDetails,List<ReOrderProductDetails> reOrderProductDetails){
		
		List<OrderProductDetails> orderProductDetailsFinal=new ArrayList<>();
		
		
		for(OrderProductDetails orderProductDetails2: orderProductDetails)
		{
			for(ReOrderProductDetails  reOrderProductDetails2:reOrderProductDetails)
			{
				if(orderProductDetails2.getProduct().getProductId()==reOrderProductDetails2.getProduct().getProductId())
				{
					orderProductDetails2.setPurchaseAmount(orderProductDetails2.getPurchaseAmount()-reOrderProductDetails2.getReOrderPurchaseAmt());
					orderProductDetails2.setPurchaseQuantity(orderProductDetails2.getPurchaseQuantity()-reOrderProductDetails2.getReOrderQty());					
				}
			}
			orderProductDetailsFinal.add(orderProductDetails2);
		}
		
				
				 
		return orderProductDetailsFinal;
		
	}

	// here we make product image null
	@Transactional
	public List<OrderProductDetails> makeOrderProductDetailsListImageMNull(
			List<OrderProductDetails> orderProductDetails) {
		List<OrderProductDetails> orderProductDetails2 = new ArrayList<>();
		for (OrderProductDetails orderProductDetails1 : orderProductDetails) {
			orderProductDetails1.getProduct().setProductImage(null);
			orderProductDetails2.add(orderProductDetails1);
		}
		return orderProductDetails2;
	}
	@Transactional
	public List<ReOrderProductDetails> makeReOrderProductDetailsListImageMNull(
			List<ReOrderProductDetails> reOrderProductDetails){
		
		List<ReOrderProductDetails> reOrderProductDetails2 = new ArrayList<>();
		for (ReOrderProductDetails reOrderProductDetails1 : reOrderProductDetails) {
			reOrderProductDetails1.getProduct().setProductImage(null);
			reOrderProductDetails2.add(reOrderProductDetails1);
		}
		return reOrderProductDetails2;
	
	}
		
		

	@Transactional
	public DispatchOrderDetails fetchDispatchOrderDetailsByDispatchedId(long dispatchedId) {

		String hql = "from DispatchOrderDetails where dispatchedId=" + dispatchedId;
		Query query = sessionFactory.getCurrentSession().createQuery(hql);
		List<DispatchOrderDetails> dispatchOrderDetailsList = (List<DispatchOrderDetails>) query.list();
		if (dispatchOrderDetailsList.isEmpty()) {
			return null;
		} else {
			return dispatchOrderDetailsList.get(0);
		}

	}

	@Transactional
	public List<OrderDetails> fetchOrderDetailsBySalesmanId(long employeeId) {
		String hql = "from OrderDetails where employeeASMandSM.employeeId=" + employeeId + " And orderStatus.status= '"
				+ Constants.ORDER_STATUS_BOOKED + "' and date(deliveryDate)=date(CURRENT_DATE())";
		Query query = sessionFactory.getCurrentSession().createQuery(hql);
		List<OrderDetails> list = (List<OrderDetails>) query.list();
		if (list.isEmpty()) {
			return null;
		} else {
			return list;
		}
	}

	@Transactional
	public List<OrderDetails> fetchPackedOrderDetailsBySalesmanId(long employeeId) {
		String hql = "from OrderDetails where employeeASMandSM.employeeId=" + employeeId + " And orderStatus.status= '"
				+ Constants.ORDER_STATUS_PACKED + "' and date(deliveryDate)=date(CURRENT_DATE())";
		Query query = sessionFactory.getCurrentSession().createQuery(hql);
		List<OrderDetails> list = (List<OrderDetails>) query.list();
		if (list.isEmpty()) {
			return null;
		} else {
			return list;
		}
	}

	// here we are confirm the order from Gk side send the notification to the
	// DB person that is nothing but the salesman
	@Transactional
	public String packedOrderForDelivery(OrderIssueRequest orderIssueRequest) {

		try {

			// orderDetails=fetchOrderDetailsByOrderId(orderIssueRequest.getOrderDetailsId());
			DispatchOrderDetails dispatchOrderDetails = fetchDispatchOrderDetailsByDispatchedId(
					orderIssueRequest.getDispatchedId());
			
			SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
			Calendar cal = Calendar.getInstance();
			cal.add(Calendar.DAY_OF_MONTH, +0); // here we get Current date
			
			/*String a=dateFormat.format(cal.getTime());
			System.out.println("date:'"+a+"'");
			String input = dateFormat.format(dispatchOrderDetails.getDeliveryDate());
			String output = input.substring(0, 10);  // Output : 2012/01/20
			if(!dispatchOrderDetails.getDeliveryDate().equals(a))
			{
				return "Failed";
			}*/
			if(dateFormat.format(dispatchOrderDetails.getDeliveryDate()).equals(dateFormat.format(cal.getTime()))){
			// here we updating the status of order
			orderStatus = fetchOrderStatus(Constants.ORDER_STATUS_PACKED);
			dispatchOrderDetails.setOrderStatus(orderStatus);
			// orderDetails.setOrderStatus(orderStatus);
			List<OrderDetails> orderDetailsList = fetchOrderDetailsBySalesmanId(
					dispatchOrderDetails.getEmployeeASMAndSM().getEmployeeId());
			/*if(orderDetailsList==null){
				return "Failed";
			}*/
			for (OrderDetails orderDetails : orderDetailsList) {
				//if(orderDetails.getDeliveryDate().equals("date(CURRENT_DATE())")){
					
				orderDetails.setOrderStatus(orderStatus);
				orderDetails.setPackedDate(new Date());
				// here we are generate the invoice no.
				InvoiceNumberGenerate invoiceNumberGenerate = new InvoiceNumberGenerate(sessionFactory);
				orderDetails.setInvoiceNumber(invoiceNumberGenerate.generateInvoiceNumber());
				sessionFactory.getCurrentSession().update(orderDetails);

				List<OrderProductDetails> orderProductDetailsList = fetchOrderProductDetailsByOrderId(
						orderDetails.getOrderId());

				for (OrderProductDetails orderProductDetails : orderProductDetailsList) {

					orderProductDetails.getProduct()
							.setCurrentQuantity(orderProductDetails.getProduct().getCurrentQuantity()
									- orderProductDetails.getPurchaseQuantity());
					sessionFactory.getCurrentSession().update(orderProductDetails);

				}
			}
			//}
			}else{
				return "Failed";
			}

			// here we are sending the notification Sm amd asm for packed order
						Employee employeeDB = new Employee();
						employeeDB.setEmployeeId(dispatchOrderDetails.getEmployeeASMAndSM().getEmployeeId());

						Employee employeeGk = new Employee();
						employeeGk.setEmployeeId(orderIssueRequest.getEmployeeIdGk());

						EmployeeDetailsDAOImpl employeeDetailsDAOImpl = new EmployeeDetailsDAOImpl(sessionFactory);
						EmployeeDetails employeeDetailsDB = employeeDetailsDAOImpl
								.getEmployeeDetailsByemployeeId(employeeDB.getEmployeeId());
						EmployeeDetails employeeDetailsGK = employeeDetailsDAOImpl
								.getEmployeeDetailsByemployeeId(employeeGk.getEmployeeId());
				
					 Notification.sendNotificationToDeliveryPersonForDispatchedOrder(employeeDetailsDB.getToken(),
																					 employeeDetailsGK.getName(),
																					 dispatchOrderDetails.getTotalQty());
						return "Success";
		} catch (HibernateException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.out.println("Error:" + e.toString());
			return "Failed";
		}

	}

	@Transactional
	public void confirmOrderForDelivery(long employeeId, long dispatchedId) {

		DispatchOrderDetails dispatchOrderDetails = fetchDispatchOrderDetailsByDispatchedId(dispatchedId);
		// here we updating the status of order
		orderStatus = fetchOrderStatus(Constants.ORDER_STATUS_ISSUED);
		dispatchOrderDetails.setOrderStatus(orderStatus);
		// orderDetails.setOrderStatus(orderStatus);
		List<OrderDetails> orderDetailsList = fetchPackedOrderDetailsBySalesmanId(
				dispatchOrderDetails.getEmployeeASMAndSM().getEmployeeId());
		for (OrderDetails orderDetails : orderDetailsList) {
			orderDetails.setOrderStatus(orderStatus);
			orderDetails.setIssueDate(new Date());
		}
		// sending notification to ASM from SM for order confirmation
				EmployeeDetailsDAOImpl employeeDetailsDAOImpl=new EmployeeDetailsDAOImpl(sessionFactory);
				Employee employee=employeeDetailsDAOImpl.fetchEmployeeByEmpId(employeeId);
				if(employee.getDepartment().getName().equals(Constants.AREA_SALESMAN_DEPT_NAME)){
					
				}else{
				List<Employee> employeeASM=employeeDetailsDAOImpl.fetchAsmBySMId(employeeId);
				
				EmployeeDetails employeeDetailsASM=employeeDetailsDAOImpl.getEmployeeDetailsByemployeeId(employeeASM.get(0).getEmployeeId());
				EmployeeDetails employeeDetailsSM=employeeDetailsDAOImpl.getEmployeeDetailsByemployeeId(employeeId);
				EmployeeDetails employeeDetailsGk=employeeDetailsDAOImpl.getEmployeeDetailsByemployeeId(dispatchOrderDetails.getEmployeeGk().getEmployeeId());
				
				Notification.sendNotificationToASMForDispatchedOrderConfirmation(employeeDetailsASM.getToken(), employeeDetailsSM.getName(), dispatchOrderDetails.getTotalQty(),employeeDetailsGk.getName());
				}
		

	}

	/*
	 * @Transactional public String packedOrderToDeliverBoy(OrderIssueRequest
	 * orderIssueRequest) {
	 * 
	 * try {
	 * 
	 * orderDetails=fetchDispatchedOrderDetailsListForGK(orderListGKRequest)
	 * 
	 * // here we updating the status of order orderStatus =
	 * fetchOrderStatus(Constants.ORDER_STATUS_PACKED);
	 * //orderDetails.getOrderStatus().setStatus(Constants.ORDER_STATUS_PACKED);
	 * orderDetails.setOrderStatus(orderStatus); orderDetails.setPackedDate(new
	 * Date());
	 * 
	 * // here we are generate the invoice no. InvoiceNumberGenerate
	 * invoiceNumberGenerate=new InvoiceNumberGenerate(sessionFactory);
	 * orderDetails.setInvoiceNumber(invoiceNumberGenerate.generateInvoiceNumber
	 * ()); sessionFactory.getCurrentSession().update(orderDetails);
	 * 
	 * 
	 * // here we are update Order ProductDetais And Product current Quantity
	 * List<OrderProductDetails>
	 * orderProductDetailsList=fetchOrderProductDetailsByOrderId(
	 * orderIssueRequest.getOrderDetailsId());
	 * 
	 * for(OrderProductDetails orderProductDetails:orderProductDetailsList){
	 * 
	 * orderProductDetails.getProduct().setCurrentQuantity(orderProductDetails.
	 * getProduct().getCurrentQuantity()-orderProductDetails.getPurchaseQuantity
	 * ()); sessionFactory.getCurrentSession().update(orderProductDetails);
	 * 
	 * 
	 * }
	 * 
	 * Employee employeeDB=new Employee();
	 * employeeDB.setEmployeeId(orderDetails.getEmployeeASMandSM().getEmployeeId
	 * ());
	 * 
	 * Employee employeeGk=new Employee();
	 * employeeGk.setEmployeeId(orderIssueRequest.getEmployeeIdGk());
	 * 
	 * EmployeeDetailsDAOImpl employeeDetailsDAOImpl=new
	 * EmployeeDetailsDAOImpl(sessionFactory); EmployeeDetails
	 * employeeDetailsDB=employeeDetailsDAOImpl.getEmployeeDetailsByemployeeId(
	 * employeeDB.getEmployeeId()); EmployeeDetails
	 * employeeDetailsGK=employeeDetailsDAOImpl.getEmployeeDetailsByemployeeId(
	 * employeeGk.getEmployeeId());
	 * Notification.sendNotificationToDeliveryBoyForOrder(employeeDetailsDB.
	 * getToken(),employeeDetailsGK.getEmployeeDetailsId() ,
	 * orderDetails.getBooth().getShopName(), orderDetails.getOrderId()); return
	 * "Success"; } catch (HibernateException e) { // TODO Auto-generated catch
	 * block e.printStackTrace(); System.out.println("Error:"+e.toString());
	 * return "Failed"; }
	 * 
	 * }
	 */

	// here we are fetching future and current OrderDetailsList for Gk OrderList
	// Screen
	@Transactional
	public List<GkDispatchedOrderListResponse> fetchDispatchedOrderDetailsListForGK(
			OrderListGKRequest orderListGKRequest) {

		String hql = "";
		Query query = null;
		GkDispatchedOrderListResponse gkDispatchedOrderListResponse = new GkDispatchedOrderListResponse();

		EmployeeDetailsDAOImpl employeeDetailsDAOImpl = new EmployeeDetailsDAOImpl(sessionFactory);
		/*List<Route> routeList = employeeDetailsDAOImpl
				.fetchEmployeeRouteListByEmployeeId(orderListGKRequest.getEmployeeId());

		List<Long> routeIdList = new ArrayList<>();
		for (Route routeList1 : routeList) {
			routeIdList.add(routeList1.getRouteId());
		}*/
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.DAY_OF_MONTH, +1); // here we get the next date from
											// today date

		// here we are using 3 If Condition
		// first if Condition for Future And Todays Order
		// 2nd if Condition whether the Asm employeeId is given or not
		// 3rd if Condition for ClusterId is given or not

		// first if condition
		if (orderListGKRequest.getOrderScheduling().equals(Constants.TODAY_ORDER)) {

			// 2nd if condition
			if (orderListGKRequest.getAsmEmployeeId() != 0) {
				// here we are fecthing the empDetailsId List from
				// employeeDetails
				List<Employee> employee = employeeDetailsDAOImpl
						.fetchSalesmanByASMId(orderListGKRequest.getAsmEmployeeId());
				List<Long> employeeIdList = new ArrayList<>();
				for (Employee employee2 : employee) {
					employeeIdList.add(employee2.getEmployeeId());
				}
				employeeIdList.add(orderListGKRequest.getAsmEmployeeId());

				// 3rd if condition
				
				if (orderListGKRequest.getClusterId() != 0) {
					hql = "from DispatchOrderDetails where date(deliveryDate)<='" + dateFormat.format(cal.getTime())
							+ "' and date(deliveryDate)>= date(CURRENT_DATE()) And employeeGk.employeeId=" + orderListGKRequest.getEmployeeId()
							+ " And employeeASMAndSM.employeeId in(:ids1) And salemanArea.cluster.clusterId="
							+ orderListGKRequest.getClusterId() + " And orderStatus.status='"
							+ Constants.ORDER_STATUS_BOOKED + "'";
					query = sessionFactory.getCurrentSession().createQuery(hql);
					query.setParameterList("ids1", employeeIdList);

				} else {
					hql = "from DispatchOrderDetails where date(deliveryDate)<='" + dateFormat.format(cal.getTime())
							+ "' and date(deliveryDate)>= date(CURRENT_DATE()) And employeeGk.employeeId=" + orderListGKRequest.getEmployeeId()
							+ " And employeeASMAndSM.employeeId in(:ids1)And orderStatus.status='"
							+ Constants.ORDER_STATUS_BOOKED + "'";
					query = sessionFactory.getCurrentSession().createQuery(hql);
					// query.setParameterList("ids", routeIdList);
					query.setParameterList("ids1", employeeIdList);
				}

			} else {

				if (orderListGKRequest.getClusterId() != 0) {
					hql = "from DispatchOrderDetails where date(deliveryDate)<='" + dateFormat.format(cal.getTime())
							+ "'  and date(deliveryDate)>= date(CURRENT_DATE())  And employeeGk.employeeId=" + orderListGKRequest.getEmployeeId()
							+ " And salemanArea.cluster.clusterId=" + orderListGKRequest.getClusterId()
							+ " And orderStatus.status='" + Constants.ORDER_STATUS_BOOKED + "'";
					query = sessionFactory.getCurrentSession().createQuery(hql);

				} else {
					hql = "from DispatchOrderDetails where date(deliveryDate)<='" + dateFormat.format(cal.getTime())
							+ "'  and date(deliveryDate)>= date(CURRENT_DATE())  And employeeGk.employeeId=" + orderListGKRequest.getEmployeeId()
							+ " And orderStatus.status='" + Constants.ORDER_STATUS_BOOKED + "'";
					query = sessionFactory.getCurrentSession().createQuery(hql);

				}
			}

		}

		else if (orderListGKRequest.getOrderScheduling().equals(Constants.FUTURE_ORDER)) {

			if (orderListGKRequest.getAsmEmployeeId() != 0) {

				List<Employee> employee = employeeDetailsDAOImpl
						.fetchSalesmanByASMId(orderListGKRequest.getAsmEmployeeId());
				List<Long> employeeIdList = new ArrayList<>();
				for (Employee employee2 : employee) {
					employeeIdList.add(employee2.getEmployeeId());
				}
				employeeIdList.add(orderListGKRequest.getAsmEmployeeId());

				if (orderListGKRequest.getClusterId() != 0) {
					hql = "from DispatchOrderDetails where date(deliveryDate)>'" + dateFormat.format(cal.getTime())
							+ "' And employeeGk.employeeId=" + orderListGKRequest.getEmployeeId()
							+ " And employeeASMAndSM.employeeId in(:ids1) And salemanArea.cluster.clusterId="
							+ orderListGKRequest.getClusterId() + " And orderStatus.status='"
							+ Constants.ORDER_STATUS_BOOKED + "'";
					query = sessionFactory.getCurrentSession().createQuery(hql);
					// query.setParameterList("ids", routeIdList);
					query.setParameterList("ids1", employeeIdList);

				} else {
					hql = "from DispatchOrderDetails where date(deliveryDate)>'" + dateFormat.format(cal.getTime())
							+ "' And employeeGk.employeeId=" + orderListGKRequest.getEmployeeId()
							+ " And employeeASMAndSM.employeeId in(:ids1)And orderStatus.status='"
							+ Constants.ORDER_STATUS_BOOKED + "'";
					query = sessionFactory.getCurrentSession().createQuery(hql);
					// query.setParameterList("ids", routeIdList);
					query.setParameterList("ids1", employeeIdList);
				}

			} else if (orderListGKRequest.getClusterId() != 0) {
				hql = "from DispatchOrderDetails where date(deliveryDate)>'" + dateFormat.format(cal.getTime())
						+ "' And employeeGk.employeeId=" + orderListGKRequest.getEmployeeId()
						+ " And salemanArea.cluster.clusterId=" + orderListGKRequest.getClusterId()
						+ " And orderStatus.status='" + Constants.ORDER_STATUS_BOOKED + "'";
				query = sessionFactory.getCurrentSession().createQuery(hql);
				// query.setParameterList("ids", routeIdList);
			} else {
				hql = "from DispatchOrderDetails where date(deliveryDate)>'" + dateFormat.format(cal.getTime())
						+ "' And employeeGk.employeeId=" + orderListGKRequest.getEmployeeId()
						+ " And orderStatus.status='" + Constants.ORDER_STATUS_BOOKED + "'";
				query = sessionFactory.getCurrentSession().createQuery(hql);
				// query.setParameterList("ids", routeIdList);

			}

		}

		List<DispatchOrderDetails> dispatchOrderDetailsList = (List<DispatchOrderDetails>) query.list();

		List<GkDispatchedOrderListResponse> gkDispatchedOrderListResponse1 = new ArrayList<>();
		for (DispatchOrderDetails dispatchOrderDetails : dispatchOrderDetailsList) {
			GkDispatchedOrderListResponse listResponse = new GkDispatchedOrderListResponse();
			EmployeeDetails employeeDetails = employeeDetailsDAOImpl
					.getEmployeeDetailsByemployeeId(dispatchOrderDetails.getEmployeeASMAndSM().getEmployeeId());
			
			List<Route> routeList=new ArrayList<>();
			if(employeeDetails.getEmployee().getDepartment().getName().equals(Constants.AREA_SALESMAN_DEPT_NAME)){
				List<Area> areas=employeeDetailsDAOImpl.fetchAreaListByEmployeeId(employeeDetails.getEmployee().getEmployeeId());
				List<Route> routes=employeeDetailsDAOImpl.fetchRouteListByAreaId(areas.get(0).getAreaId());
				routeList.addAll(routes);
			}else{
			List<Route> routes = employeeDetailsDAOImpl
					.fetchEmployeeRouteListByEmployeeId(dispatchOrderDetails.getEmployeeASMAndSM().getEmployeeId());
			routeList.addAll(routes);
			}
			listResponse.setEmployeeName(employeeDetails.getName());
			listResponse.setRoutes(routeList);
			listResponse.setDispatchOrderDetails(dispatchOrderDetails);

			gkDispatchedOrderListResponse1.add(listResponse);
		}

		if (gkDispatchedOrderListResponse1.isEmpty()) {
			return null;
		}
		return gkDispatchedOrderListResponse1;
	}

	@Transactional
	public List<OrderListResponse> fetchOrderDetailsListForGK1(OrderListGKRequest orderListGKRequest) {
		// TODO Auto-generated method stub
		String hql = "";
		Query query = null;

		OrderListResponse orderListResponse = new OrderListResponse();

		EmployeeDetailsDAOImpl employeeDetailsDAOImpl = new EmployeeDetailsDAOImpl(sessionFactory);
		List<Route> routeList = employeeDetailsDAOImpl
				.fetchEmployeeRouteListByEmployeeId(orderListGKRequest.getEmployeeId());

		List<Long> routeIdList = new ArrayList<>();
		for (Route routeList1 : routeList) {
			routeIdList.add(routeList1.getRouteId());
		}
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.DAY_OF_MONTH, +1); // here we get the next date from
											// today date

		// here we are using 3 If Condition
		// first if Condition for Future And Todays Order
		// 2nd if Condition whether the Asm employeeId is given or not
		// 3rd if Condition for ClusterId is given or not

		// first if condition
		if (orderListGKRequest.getOrderScheduling().equals(Constants.TODAY_ORDER)) {

			// 2nd if condition
			if (orderListGKRequest.getAsmEmployeeId() != 0) {
				// here we are fecthing the empDetailsId List from
				// employeeDetails
				List<Employee> employee = employeeDetailsDAOImpl
						.fetchSalesmanByASMId(orderListGKRequest.getAsmEmployeeId());
				List<Long> employeeIdList = new ArrayList<>();
				for (Employee employee2 : employee) {
					employeeIdList.add(employee2.getEmployeeId());
				}
				employeeIdList.add(orderListGKRequest.getAsmEmployeeId());

				// 3rd if condition
				if (orderListGKRequest.getClusterId() != 0) {
					hql = "from OrderDetails where date(deliveryDate)<='" + dateFormat.format(cal.getTime())
							+ "' And booth.route.routeId in(:ids) And employeeASMandSM.employeeId in(:ids1) And booth.route.area.cluster.clusterId="
							+ orderListGKRequest.getClusterId() + " And orderStatus.status='"
							+ Constants.ORDER_STATUS_BOOKED + "'";
					query = sessionFactory.getCurrentSession().createQuery(hql);
					query.setParameterList("ids", routeIdList);
					query.setParameterList("ids1", employeeIdList);

				} else {
					hql = "from OrderDetails where date(deliveryDate)<='" + dateFormat.format(cal.getTime())
							+ "' And booth.route.routeId in(:ids) And employeeASMandSM.employeeId in(:ids1) And orderStatus.status='"
							+ Constants.ORDER_STATUS_BOOKED + "'";
					query = sessionFactory.getCurrentSession().createQuery(hql);
					query.setParameterList("ids", routeIdList);
					query.setParameterList("ids1", employeeIdList);
				}

			} else {

				if (orderListGKRequest.getClusterId() != 0) {
					hql = "from OrderDetails where date(deliveryDate)<='" + dateFormat.format(cal.getTime())
							+ "' And booth.route.routeId in(:ids) And booth.route.area.cluster.clusterId="
							+ orderListGKRequest.getClusterId() + " And orderStatus.status='"
							+ Constants.ORDER_STATUS_BOOKED + "'";
					query = sessionFactory.getCurrentSession().createQuery(hql);
					query.setParameterList("ids", routeIdList);
				} else {
					hql = "from OrderDetails where date(deliveryDate)<='" + dateFormat.format(cal.getTime())
							+ "' And booth.route.routeId in(:ids) And orderStatus.status='"
							+ Constants.ORDER_STATUS_BOOKED + "'";
					query = sessionFactory.getCurrentSession().createQuery(hql);
					query.setParameterList("ids", routeIdList);
				}
			}

		}

		else if (orderListGKRequest.getOrderScheduling().equals(Constants.FUTURE_ORDER)) {

			if (orderListGKRequest.getAsmEmployeeId() != 0) {

				List<Employee> employee = employeeDetailsDAOImpl
						.fetchSalesmanByASMId(orderListGKRequest.getAsmEmployeeId());
				List<Long> employeeIdList = new ArrayList<>();
				for (Employee employee2 : employee) {
					employeeIdList.add(employee2.getEmployeeId());
				}
				employeeIdList.add(orderListGKRequest.getAsmEmployeeId());

				if (orderListGKRequest.getClusterId() != 0) {
					hql = "from OrderDetails where date(deliveryDate)>'" + dateFormat.format(cal.getTime())
							+ "' And booth.route.routeId in(:ids) And employeeASMandSM.employeeId in(:ids1) And booth.route.area.cluster.clusterId="
							+ orderListGKRequest.getClusterId() + " And orderStatus.status='"
							+ Constants.ORDER_STATUS_BOOKED + "'";
					query = sessionFactory.getCurrentSession().createQuery(hql);
					query.setParameterList("ids", routeIdList);
					query.setParameterList("ids1", employeeIdList);

				} else {
					hql = "from OrderDetails where date(deliveryDate)>'" + dateFormat.format(cal.getTime())
							+ "' And booth.route.routeId in(:ids) And employeeASMandSM.employeeId in(:ids1) And orderStatus.status='"
							+ Constants.ORDER_STATUS_BOOKED + "'";
					query = sessionFactory.getCurrentSession().createQuery(hql);
					query.setParameterList("ids", routeIdList);
					query.setParameterList("ids1", employeeIdList);
				}

			} else if (orderListGKRequest.getClusterId() != 0) {
				hql = "from OrderDetails where date(deliveryDate)>'" + dateFormat.format(cal.getTime())
						+ "' And booth.route.routeId in(:ids) And booth.route.area.cluster.clusterId="
						+ orderListGKRequest.getClusterId() + " And orderStatus.status='"
						+ Constants.ORDER_STATUS_BOOKED + "'";
				query = sessionFactory.getCurrentSession().createQuery(hql);
				query.setParameterList("ids", routeIdList);
			} else {
				hql = "from OrderDetails where date(deliveryDate)>'" + dateFormat.format(cal.getTime())
						+ "' And booth.route.routeId in(:ids) And orderStatus.status='" + Constants.ORDER_STATUS_BOOKED
						+ "'";
				query = sessionFactory.getCurrentSession().createQuery(hql);
				query.setParameterList("ids", routeIdList);

			}

		}
		@SuppressWarnings("unchecked")
		List<OrderDetails> orderDetailsList = (List<OrderDetails>) query.list();

		List<OrderListResponse> orderListResponses = new ArrayList<>();
		for (OrderDetails orderDetails2 : orderDetailsList) {
			OrderListResponse listResponse = new OrderListResponse();
			String empName = employeeDetailsDAOImpl.fetchEmployeeNameByOrderDetails(orderDetails2);
			listResponse.setEmployeeName(empName);
			listResponse.setOrderDetailsList(orderDetails2);

			orderListResponses.add(listResponse);
		}

		if (orderListResponses.isEmpty()) {
			return null;
		}
		return orderListResponses;
	}

	/*
	 * @Transactional public Employee
	 * fetchDeliveryPersonEmployeeIdFromOrderDetails(long employeeId) { // TODO
	 * Auto-generated method stub String
	 * hql="from OrderDetails where employeeASMandSM.employeeId="+employeeId;
	 * Query query=sessionFactory.getCurrentSession().createQuery(hql);
	 * List<OrderDetails> orderDetailsList=(List<OrderDetails>)query.list();
	 * 
	 * Employee employee=orderDetailsList.get(0).getEmployeeASMandSM();
	 * List<Employee> employeeList=new ArrayList<>(); for(OrderDetails
	 * orderDetails:orderDetailsList){
	 * employeeList.add(orderDetails.getEmployeeASMandSM()); }
	 * if(employeeList.isEmpty()){ return null; }
	 * 
	 * return employeeList; }
	 */

	// here we are fetching Dispatched ProductList
	@Transactional
	public List<DispatchProductNameAndQtyListResponse> fetchOrderProductDetailsForDeliveryByEmpId(long employeeId) {
		// TODO Auto-generated method stub

		List<DispatchProductNameAndQtyListResponse> dispatchProductNameAndQtyListResponseList = new ArrayList<>();
		String hql = "from OrderProductDetails where orderDetails.employeeASMandSM.employeeId=" + employeeId
				+ " And orderDetails.orderStatus.status='" + Constants.ORDER_STATUS_PACKED + "'";
		Query query = sessionFactory.getCurrentSession().createQuery(hql);

		List<OrderProductDetails> orderProductDetailsList = (List<OrderProductDetails>) query.list();

		List<OrderProductDetails> orderProductDetails2 = makeOrderProductDetailsListImageMNull(orderProductDetailsList);

		List<Long> qtyList = new ArrayList<>();
		List<Long> productIdList = new ArrayList<>();
		List<String> productNameList = new ArrayList<>();

		for (int i = 0; i < orderProductDetails2.size(); i++) {
			long a = 0;
			long b = 0;
			String c = null;
			int j = 0;
			for (j = 0; j < orderProductDetails2.size(); j++) {
				if (orderProductDetails2.get(i).getProduct().getProductId() == orderProductDetails2.get(j).getProduct()
						.getProductId()) {
					a = a + orderProductDetails2.get(j).getPurchaseQuantity();
					b = orderProductDetails2.get(j).getProduct().getProductId();
					c = orderProductDetails2.get(j).getProduct().getProductName();

				}
			}

			productIdList.add(orderProductDetails2.get(j).getProduct().getProductId());
			productNameList.add(orderProductDetails2.get(j).getProduct().getProductName());

			qtyList.add(a);

		}

		for (int k = 0; k < productIdList.size(); k++) {

			DispatchProductNameAndQtyListResponse dispatchProductNameAndQtyListResponse = new DispatchProductNameAndQtyListResponse();
			dispatchProductNameAndQtyListResponse.setProductId(productIdList.get(k));
			dispatchProductNameAndQtyListResponse.setProductName(productNameList.get(k));
			dispatchProductNameAndQtyListResponse.setQty(qtyList.get(k));
			dispatchProductNameAndQtyListResponse
					.setEmployeeId(orderProductDetails2.get(0).getOrderDetails().getEmployeeGK().getEmployeeId());
			dispatchProductNameAndQtyListResponseList.add(dispatchProductNameAndQtyListResponse);
		}

		if (dispatchProductNameAndQtyListResponseList.isEmpty()) {
			return null;
		}

		return dispatchProductNameAndQtyListResponseList;
	}

	@Transactional
	public void OrderConfirmForDelivery(long employeeId) {
		// TODO Auto-generated method stub
		String hql = "From OrderDetails where employeeASMandSM.employeeId=" + employeeId + " And orderStatus.status='"
				+ Constants.ORDER_STATUS_PACKED + "'";
		Query query = sessionFactory.getCurrentSession().createQuery(hql);
		List<OrderDetails> orderDetailsList = (List<OrderDetails>) query.list();

		for (OrderDetails orderDetails : orderDetailsList) {
			OrderStatus orderStatus = fetchOrderStatus(Constants.ORDER_STATUS_ISSUED);
			orderDetails.setOrderStatus(orderStatus);
			orderDetails.setIssueDate(new Date());
			sessionFactory.getCurrentSession().update(orderDetails);
		}

	}

	// here we fetched the Order whose delivery is pending and whose delivery is
	// done
	@Transactional
	public List<OrderDetails> remainingOrderListAndDeliveredOrderedListByEmpId(long employeeId, String status) {

		String hql = "";
		Query query = null;
		if (status.equalsIgnoreCase("RemainingOrder")) {
			hql = "From OrderDetails where employeeASMandSM.employeeId=" + employeeId + " And orderStatus.status='"
					+ Constants.ORDER_STATUS_ISSUED + "' and date(deliveryDate)=date(CURRENT_DATE())";
		} else if (status.equalsIgnoreCase("CompletedOrder")) {
			hql = "From OrderDetails where employeeASMandSM.employeeId=" + employeeId + " And orderStatus.status='"
					+ Constants.ORDER_STATUS_DELIVERED + "' and date(deliveryDate)=date(CURRENT_DATE())";
		}

		query = sessionFactory.getCurrentSession().createQuery(hql);
		List<OrderDetails> orderDetailsList = (List<OrderDetails>) query.list();
		if (orderDetailsList.isEmpty()) {
			return null;
		}
		return orderDetailsList;
	}

	// here we fetched the Order whose status is packed
	@Transactional
	public List<OrderDetails> deliveredOrderedList(long employeeId) {

		String hql = "From OrderDetails where employeeASMandSM.employeeId=" + employeeId + " And orderStatus.status='"
				+ Constants.ORDER_STATUS_PACKED + "'";
		Query query = sessionFactory.getCurrentSession().createQuery(hql);
		List<OrderDetails> orderDetailsList = (List<OrderDetails>) query.list();
		if (orderDetailsList.isEmpty()) {
			return null;
		}
		return orderDetailsList;
	}

	@Transactional
	public void cancelOrder(CancelOrderRequest cancelOrderRequest) {

		String hql = "from OrderDetails where orderId='" + cancelOrderRequest.getOrderId()
				+ "' And employeeASMandSM.employeeId=" + cancelOrderRequest.getEmployeeId()
				+ " And orderStatus.status='" + Constants.ORDER_STATUS_ISSUED + "'";
		Query query = sessionFactory.getCurrentSession().createQuery(hql);

		@SuppressWarnings("unchecked")
		List<OrderDetails> orderDetailsList = (List<OrderDetails>) query.list();

		orderStatus = fetchOrderStatus(Constants.ORDER_STATUS_CANCELED);
		orderDetailsList.get(0).setOrderStatus(orderStatus);
		orderDetailsList.get(0).setCancelDate(new Date());
		orderDetailsList.get(0).setCancelReason(cancelOrderRequest.getCancelReason());
		orderDetailsList.get(0).setReOrderQty(orderDetailsList.get(0).getTotalQuantity());
		orderDetailsList.get(0).setReOrderAmt(orderDetailsList.get(0).getTotalAmount());
		orderDetailsList.get(0).setReOrderAmtWithTax(orderDetailsList.get(0).getTotalAmountWithTax());
		orderDetailsList.get(0).setReOrderStatus(Constants.REORDER_STATUS_PENDING);
		
		// Notification sending for cancel order in case of salesman
		
				EmployeeDetailsDAOImpl employeeDetailsDAOImpl=new EmployeeDetailsDAOImpl(sessionFactory);
				Employee employee=employeeDetailsDAOImpl.fetchEmployeeByEmpId(orderDetailsList.get(0).getEmployeeASMandSM().getEmployeeId());
				if(employee.getDepartment().getName().equals(Constants.AREA_SALESMAN_DEPT_NAME)){
					
				}else{
				List<Employee> employeeASM=employeeDetailsDAOImpl.fetchAsmBySMId(employee.getEmployeeId());
				
				EmployeeDetails employeeDetailsASM=employeeDetailsDAOImpl.getEmployeeDetailsByemployeeId(employeeASM.get(0).getEmployeeId());
				EmployeeDetails employeeDetailsSM=employeeDetailsDAOImpl.getEmployeeDetailsByemployeeId(employee.getEmployeeId());
				//EmployeeDetails employeeDetailsGk=employeeDetailsDAOImpl.getEmployeeDetailsByemployeeId(dispatchOrderDetails.getEmployeeGk().getEmployeeId());
				Notification.sendNotificationToASMForCancelOrder(employeeDetailsASM.getToken(), employeeDetailsSM.getName(), orderDetailsList.get(0).getBooth().getBoothNo(), orderDetailsList.get(0).getBooth().getOwnerName(), orderDetailsList.get(0).getOrderId(), orderDetailsList.get(0).getTotalQuantity());
				}
	}

	@Transactional
	public List<OrderDetails> fetchOrderListForReorder(long employeeId) {
		// TODO Auto-generated method stub
		String hql = "";
		Query query;

		hql = "from OrderDetails where employeeASMandSM.employeeId=" + employeeId + " And reOrderStatus='"
				+ Constants.REORDER_STATUS_PENDING + "' And date(cancelDate)=date(CURRENT_DATE())";
		query = sessionFactory.getCurrentSession().createQuery(hql);
		List<OrderDetails> orderDetailsList = (List<OrderDetails>) query.list();

		if (orderDetailsList.isEmpty()) {
			return null;
		}

		return orderDetailsList;
	}

	// here we are fetching DispatchOrderDetails By OrderId
	@Transactional
	public DispatchOrderDetails fetchDispatchOrderDetailsByemployeeId(long employeeId) {
		String hql = "from DispatchOrderDetails where employeeASMAndSM.employeeId=" + employeeId; // +"
																									// and
																									// date(dispatchedAddedDateTime)=date(CURRENT_DATE())";

		Query query = sessionFactory.getCurrentSession().createQuery(hql);
		List<DispatchOrderDetails> dispatchOrderDetails = (List<DispatchOrderDetails>) query.list();
		if (dispatchOrderDetails.isEmpty()) {
			return null;

		} else {
			return dispatchOrderDetails.get(0);
		}

	}

	@Transactional
	public DispatchOrderDetails fetchDispatchOrderDetailsByemployeeIdAndDeliveryDate1(long employeeId,
			String deliveryDate) {
		String hql = "from DispatchOrderDetails where employeeASMAndSM.employeeId=" + employeeId
				+ " and date(deliveryDate)='" + deliveryDate + "'";

		Query query = sessionFactory.getCurrentSession().createQuery(hql);
		List<DispatchOrderDetails> dispatchOrderDetails = (List<DispatchOrderDetails>) query.list();
		if (dispatchOrderDetails.isEmpty()) {
			return null;

		} else {
			return dispatchOrderDetails.get(0);
		}

	}

	@Transactional
	public DispatchOrderDetails fetchDispatchOrderForDeliveryByempId(long employeeId) {
		// TODO Auto-generated method stub
		String hql = "from DispatchOrderDetails where employeeASMAndSM.employeeId=" + employeeId
				+ " and orderStatus.status='" + Constants.ORDER_STATUS_PACKED + "'";

		Query query = sessionFactory.getCurrentSession().createQuery(hql);
		List<DispatchOrderDetails> dispatchOrderDetails = (List<DispatchOrderDetails>) query.list();
		if (dispatchOrderDetails.isEmpty()) {
			return null;

		} else {
			return dispatchOrderDetails.get(0);
		}

	}

	// here we are generating ReOrder ID and updating Reorder table
 	@Transactional
	public void deliveryOfReOrderProduct(ReOrderDeliveryRequest reOrderDeliveryRequest) {
		// TODO Auto-generated method stub
		
		ReOrderDetails reOrderDetails=new ReOrderDetails();
		
		// in below two lines we are generating the reorderId
		ReOrderIdGenerator reOrderIdGenerator=new ReOrderIdGenerator(sessionFactory);
		reOrderDetails.setReOrderDetailsId(reOrderIdGenerator.generate());
		
		// in below lines we are going to set the values Of Reorder Entity
		reOrderDetails.setOrderDetails(reOrderDeliveryRequest.getOrderDetails());
		reOrderDetails.setReOrderAddedDatetime(new Date());
		reOrderDetails.setReOrderToBooth(reOrderDeliveryRequest.getBooth());
		reOrderDetails.setReOrderAmt(reOrderDeliveryRequest.getOrderDetails().getTotalAmount());
		reOrderDetails.setReOrderAmtWithTax(reOrderDeliveryRequest.getOrderDetails().getTotalAmountWithTax());
		reOrderDetails.setReOrderQty(reOrderDeliveryRequest.getOrderDetails().getTotalQuantity());
		
		sessionFactory.getCurrentSession().save(reOrderDetails);  // here we are saving the reOrderDetails
		for(OrderProductDetails orderProductDetails:reOrderDeliveryRequest.getOrderProductDetailsList()){
			
			ReOrderProductDetails reOrderProductDetails=new ReOrderProductDetails();
			
			// in below lines we are going to set the values Of ReorderProductDetails Entity
			reOrderProductDetails.setProduct(orderProductDetails.getProduct());
			reOrderProductDetails.setReOrderPurchaseAmt(orderProductDetails.getPurchaseAmount());
			reOrderProductDetails.setReOrderQty(orderProductDetails.getPurchaseQuantity());
			reOrderProductDetails.setReOrderDetails(reOrderDetails);
			reOrderProductDetails.setSellingRate(orderProductDetails.getSellingRate());
			
			sessionFactory.getCurrentSession().save(reOrderProductDetails);// here we are saving the ReOrderProduct details
		}
		
	}

	@Transactional
	public List<DispatchOrderDetails> fetchDispatchOrderDetailsForGkReportByClusterIdAndDateRange(
			GkDispatchProductReportRequest gkDispatchProductReportRequest) {
		String hql="";
		Query query;
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		Calendar cal = Calendar.getInstance(); 
		if(gkDispatchProductReportRequest.getClusterId()==0){
			if(gkDispatchProductReportRequest.getRange().equals("range")){
				hql="from DispatchOrderDetails where employeeGk.employeeId="+gkDispatchProductReportRequest.getGkEmployeeId()+" and orderStatus.status='"+Constants.ORDER_STATUS_ISSUED+"'and (date(deliveryDate)>='"+gkDispatchProductReportRequest.getFromDate()+"' And date(deliveryDate)<='"+gkDispatchProductReportRequest.getToDate()+"')"; 
				}else if(gkDispatchProductReportRequest.getRange().equals("last7days")){
					cal.add(Calendar.DAY_OF_MONTH, -7);
					hql="from DispatchOrderDetails where  employeeGk.employeeId="+gkDispatchProductReportRequest.getGkEmployeeId()+" and orderStatus.status='"+Constants.ORDER_STATUS_ISSUED+"' and date(deliveryDate)>='"+dateFormat.format(cal.getTime())+"'";
				}else if(gkDispatchProductReportRequest.getRange().equals("last1month")){
					cal.add(Calendar.MONTH, -1);
					hql="from DispatchOrderDetails where  employeeGk.employeeId="+gkDispatchProductReportRequest.getGkEmployeeId()+" and orderStatus.status='"+Constants.ORDER_STATUS_ISSUED+"' and date(deliveryDate)>='"+dateFormat.format(cal.getTime())+"'";
			
				}else if(gkDispatchProductReportRequest.getRange().equals("last3months")){
					cal.add(Calendar.MONTH, -3);
			
					hql="from DispatchOrderDetails where   employeeGk.employeeId="+gkDispatchProductReportRequest.getGkEmployeeId()+" and orderStatus.status='"+Constants.ORDER_STATUS_ISSUED+"' and date(deliveryDate)>='"+dateFormat.format(cal.getTime())+"'";
				}else if(gkDispatchProductReportRequest.getRange().equals("pickDate")){
					hql="from DispatchOrderDetails where  employeeGk.employeeId="+gkDispatchProductReportRequest.getGkEmployeeId()+"  and orderStatus.status='"+Constants.ORDER_STATUS_ISSUED+"' and  date(deliveryDate)='"+gkDispatchProductReportRequest.getFromDate() +"'";
				}
				else if(gkDispatchProductReportRequest.getRange().equals("currentDate"))
	            {
	                hql="from DispatchOrderDetails where  employeeGk.employeeId="+gkDispatchProductReportRequest.getGkEmployeeId()+"  and orderStatus.status='"+Constants.ORDER_STATUS_ISSUED+"' and  date(deliveryDate)=date(CURRENT_DATE())";
	            }
				else if(gkDispatchProductReportRequest.getRange().equals("viewAll"))
				{
					hql="from DispatchOrderDetails where  employeeGk.employeeId="+gkDispatchProductReportRequest.getGkEmployeeId()+" and orderStatus.status='"+Constants.ORDER_STATUS_ISSUED+"'";
				}

				
			
		}else{

			if(gkDispatchProductReportRequest.getRange().equals("range")){
				hql="from DispatchOrderDetails where employeeGk.employeeId="+gkDispatchProductReportRequest.getGkEmployeeId()+"  and orderStatus.status='"+Constants.ORDER_STATUS_ISSUED+"' And salemanArea.cluster.clusterId="+gkDispatchProductReportRequest.getClusterId()+" And (date(deliveryDate)>='"+gkDispatchProductReportRequest.getFromDate()+"' And date(deliveryDate)<='"+gkDispatchProductReportRequest.getToDate()+"'"; 
				}else if(gkDispatchProductReportRequest.getRange().equals("last7days")){
					cal.add(Calendar.DAY_OF_MONTH, -7);
					hql="from DispatchOrderDetails where  employeeGk.employeeId="+gkDispatchProductReportRequest.getGkEmployeeId()+"  and orderStatus.status='"+Constants.ORDER_STATUS_ISSUED+"' And salemanArea.cluster.clusterId="+gkDispatchProductReportRequest.getClusterId()+" And date(deliveryDate)>='"+dateFormat.format(cal.getTime())+"'";
				}else if(gkDispatchProductReportRequest.getRange().equals("last1month")){
					cal.add(Calendar.MONTH, -1);
					hql="from DispatchOrderDetails where  employeeGk.employeeId="+gkDispatchProductReportRequest.getGkEmployeeId()+"  and orderStatus.status='"+Constants.ORDER_STATUS_ISSUED+"'  And salemanArea.cluster.clusterId="+gkDispatchProductReportRequest.getClusterId()+" And  date(deliveryDate)>='"+dateFormat.format(cal.getTime())+"'";
			
				}else if(gkDispatchProductReportRequest.getRange().equals("last3months")){
					cal.add(Calendar.MONTH, -3);
			
					hql="from DispatchOrderDetails where   employeeGk.employeeId="+gkDispatchProductReportRequest.getGkEmployeeId()+"  and orderStatus.status='"+Constants.ORDER_STATUS_ISSUED+"'And salemanArea.cluster.clusterId="+gkDispatchProductReportRequest.getClusterId()+" And date(deliveryDate)>='"+dateFormat.format(cal.getTime())+"'";
				}else if(gkDispatchProductReportRequest.getRange().equals("pickDate")){
					hql="from DispatchOrderDetails where  employeeGk.employeeId="+gkDispatchProductReportRequest.getGkEmployeeId()+"  and orderStatus.status='"+Constants.ORDER_STATUS_ISSUED+"' And salemanArea.cluster.clusterId="+gkDispatchProductReportRequest.getClusterId()+" And date(deliveryDate)='"+gkDispatchProductReportRequest.getFromDate() +"'";
				}
				else if(gkDispatchProductReportRequest.getRange().equals("currentDate"))
	            {
	                hql="from DispatchOrderDetails where  employeeGk.employeeId="+gkDispatchProductReportRequest.getGkEmployeeId()+"  and orderStatus.status='"+Constants.ORDER_STATUS_ISSUED+"' And salemanArea.cluster.clusterId="+gkDispatchProductReportRequest.getClusterId()+" And  date(deliveryDate)=date(CURRENT_DATE())";
	            }
				else if(gkDispatchProductReportRequest.getRange().equals("viewAll"))
				{
					hql="from DispatchOrderDetails where  employeeGk.employeeId="+gkDispatchProductReportRequest.getGkEmployeeId()+" and orderStatus.status='"+Constants.ORDER_STATUS_ISSUED+"' And salemanArea.cluster.clusterId="+gkDispatchProductReportRequest.getClusterId();
				}

		}
		query=sessionFactory.getCurrentSession().createQuery(hql);
		List<DispatchOrderDetails> dispatchOrderDetailsList=(List<DispatchOrderDetails>)query.list();
		if(dispatchOrderDetailsList.isEmpty()){
			return null;
		}
		return dispatchOrderDetailsList;
	}

	@Transactional
	public List<GkDispatchProductReportResponse> fetchDispatchOrderDetailsListForGkReportByClusterIdAndDateRange(
			GkDispatchProductReportRequest gkDispatchProductReportRequest) {
		// TODO Auto-generated method stub
		List<DispatchOrderDetails> dispatchOrderDetailsList=fetchDispatchOrderDetailsForGkReportByClusterIdAndDateRange(gkDispatchProductReportRequest);
		EmployeeDetailsDAOImpl employeeDetailsDAOImpl = new EmployeeDetailsDAOImpl(sessionFactory);
		
		if(dispatchOrderDetailsList==null){
			return null;
		}
		
		Map<Long, GkDispatchProductReportResponse> map =new HashMap<>();
		for(DispatchOrderDetails dispatchOrderDetails:dispatchOrderDetailsList){
			boolean exist=false;
			for(Map.Entry<Long, GkDispatchProductReportResponse> entry : map.entrySet())
			{
				if(dispatchOrderDetails.getEmployeeASMAndSM().getEmployeeId()==entry.getKey()){
					exist=true;
				}
			}
			GkDispatchProductReportResponse gkDispatchProductReportResponse=new GkDispatchProductReportResponse();
			long totalQuantity=dispatchOrderDetails.getTotalQty();
			double totalAmt=dispatchOrderDetails.getTotalAmount();
			if(exist==false){		
				EmployeeDetails details=employeeDetailsDAOImpl.getEmployeeDetailsByemployeeId(dispatchOrderDetails.getEmployeeASMAndSM().getEmployeeId());
				List<Route> routeList=new ArrayList<>();
				if(details.getEmployee().getDepartment().getName().equals(Constants.AREA_SALESMAN_DEPT_NAME)){
					List<Area> areas=employeeDetailsDAOImpl.fetchAreaListByEmployeeId(details.getEmployee().getEmployeeId());
					List<Route> routeList1=employeeDetailsDAOImpl.fetchRouteListByAreaId(areas.get(0).getAreaId());
					routeList.addAll(routeList1);
				}else{
				List<Route> routeList1=employeeDetailsDAOImpl.fetchEmployeeRouteListByEmployeeId(dispatchOrderDetails.getEmployeeASMAndSM().getEmployeeId());
				routeList.addAll(routeList1);
				}
				gkDispatchProductReportResponse.setSalesmanName(details.getName());
				gkDispatchProductReportResponse.setSalesmanEmployeeId(dispatchOrderDetails.getEmployeeASMAndSM().getEmployeeId());
				gkDispatchProductReportResponse.setRouteList(routeList);
				gkDispatchProductReportResponse.setTotalQty(totalQuantity);
				gkDispatchProductReportResponse.setTotalAmt(totalAmt);
				map.put(dispatchOrderDetails.getEmployeeASMAndSM().getEmployeeId(), gkDispatchProductReportResponse);
			}else{
				gkDispatchProductReportResponse=map.get(dispatchOrderDetails.getEmployeeASMAndSM().getEmployeeId());
				EmployeeDetails details=employeeDetailsDAOImpl.getEmployeeDetailsByemployeeId(dispatchOrderDetails.getEmployeeASMAndSM().getEmployeeId());
				List<Route> routes=employeeDetailsDAOImpl.fetchEmployeeRouteListByEmployeeId(dispatchOrderDetails.getEmployeeASMAndSM().getEmployeeId());
				
				gkDispatchProductReportResponse.setSalesmanName(details.getName());
				gkDispatchProductReportResponse.setSalesmanEmployeeId(dispatchOrderDetails.getEmployeeASMAndSM().getEmployeeId());
				gkDispatchProductReportResponse.setRouteList(routes);
				gkDispatchProductReportResponse.setTotalQty(totalQuantity+gkDispatchProductReportResponse.getTotalQty());
				gkDispatchProductReportResponse.setTotalAmt(totalAmt+gkDispatchProductReportResponse.getTotalAmt());
				map.put(dispatchOrderDetails.getEmployeeASMAndSM().getEmployeeId(), gkDispatchProductReportResponse);
				
			}

		}
		List<GkDispatchProductReportResponse> list=new ArrayList<>();
		
		for(Map.Entry<Long, GkDispatchProductReportResponse> map2:map.entrySet()){
			list.add(map2.getValue());
		}
		
		return list;
	}

	@Transactional
	public List<DispatchOrderProductDetails> fetchDispatchOrderProductDetailsForGKReportByDateRange(long employeeId,
			String fromDate, String toDate, String range) {
		// TODO Auto-generated method stub
		
		String hql="";
		Query query;
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		Calendar cal = Calendar.getInstance();
		
		if(range.equals("range")){
			hql="from DispatchOrderProductDetails where dispatchOrderDetails.employeeASMAndSM.employeeId="+employeeId+" and (date(dispatchOrderDetails.deliveryDate)>='"+fromDate+"' And date(deliveryDate)<='"+toDate+"'"; 
			}else if(range.equals("last7days")){
				cal.add(Calendar.DAY_OF_MONTH, -7);
				hql="from DispatchOrderProductDetails where  dispatchOrderDetails.employeeASMAndSM.employeeId="+employeeId+" and date(dispatchOrderDetails.deliveryDate)>='"+dateFormat.format(cal.getTime())+"'";
			}else if(range.equals("last1month")){
				cal.add(Calendar.MONTH, -1);
				hql="from DispatchOrderProductDetails where  dispatchOrderDetails.employeeASMAndSM.employeeId="+employeeId+" and date(dispatchOrderDetails.deliveryDate)>='"+dateFormat.format(cal.getTime())+"'";
		
			}else if(range.equals("last3months")){
				cal.add(Calendar.MONTH, -3);
		
				hql="from DispatchOrderProductDetails where   dispatchOrderDetails.employeeASMAndSM.employeeId="+employeeId+" and date(dispatchOrderDetails.deliveryDate)>='"+dateFormat.format(cal.getTime())+"'";
			}else if(range.equals("pickDate")){
				hql="from DispatchOrderProductDetails where  dispatchOrderDetails.employeeASMAndSM.employeeId="+employeeId+" and  date(dispatchOrderDetails.deliveryDate)='"+fromDate +"'";
			}
			else if(range.equals("currentDate"))
            {
                hql="from DispatchOrderProductDetails where  dispatchOrderDetails.employeeASMAndSM.employeeId="+employeeId+" and  date(dispatchOrderDetails.deliveryDate)=date(CURRENT_DATE())";
            }
			else if(range.equals("viewAll"))
			{
				hql="from DispatchOrderProductDetails where  dispatchOrderDetails.employeeASMAndSM.employeeId="+employeeId;
			}
		
		query=sessionFactory.getCurrentSession().createQuery(hql);
		List<DispatchOrderProductDetails> dispatchOrderProductDetailsList=(List<DispatchOrderProductDetails>)query.list();
		if(dispatchOrderProductDetailsList.isEmpty()){
			return null;
		}
		
		return dispatchOrderProductDetailsList;
	}

	@Transactional
	public List<GkProductDetailsReportResponse> fetchDispatchProductDetailsListForGkReportByDateRange(long employeeId,
			String fromDate, String toDate, String range) {
		List<DispatchOrderProductDetails> dispatchOrderProductDetailsList=fetchDispatchOrderProductDetailsForGKReportByDateRange(employeeId, fromDate, toDate, range);
		
		Map<Long, GkProductDetailsReportResponse> map =new HashMap<>();
		for(DispatchOrderProductDetails dispatchOrderProductDetails:dispatchOrderProductDetailsList){
			boolean exist=false;
			for(Map.Entry<Long, GkProductDetailsReportResponse> entry : map.entrySet())
			{
				if(dispatchOrderProductDetails.getProduct().getProductId()==entry.getKey()){
					exist=true;
				}
			}
			

			GkProductDetailsReportResponse gkProductDetailsReportResponse=new GkProductDetailsReportResponse();
			long totalQuantity=dispatchOrderProductDetails.getTotalQty();
			double totalAmt=dispatchOrderProductDetails.getTotalAmt();
			if(exist==false){		
				gkProductDetailsReportResponse.setProductName(dispatchOrderProductDetails.getProduct().getProductName());
				gkProductDetailsReportResponse.setAmount(dispatchOrderProductDetails.getTotalAmt());
				gkProductDetailsReportResponse.setQty(dispatchOrderProductDetails.getTotalQty());
				map.put(dispatchOrderProductDetails.getProduct().getProductId(), gkProductDetailsReportResponse);
			}else{
				gkProductDetailsReportResponse.setProductName(dispatchOrderProductDetails.getProduct().getProductName());
				gkProductDetailsReportResponse.setAmount(totalAmt+dispatchOrderProductDetails.getTotalAmt());
				gkProductDetailsReportResponse.setQty(totalQuantity+dispatchOrderProductDetails.getTotalQty());
				map.put(dispatchOrderProductDetails.getProduct().getProductId(), gkProductDetailsReportResponse);
				
			}

		}
		List<GkProductDetailsReportResponse> list=new ArrayList<>();
		
		for(Map.Entry<Long, GkProductDetailsReportResponse> map2:map.entrySet()){
			list.add(map2.getValue());
		}
		
		return list;
		
	}

	@Transactional
	public void updateDispatchProductNotReceivingReason(long dispatchedId, String reason) {
		
		String hql="from DispatchOrderDetails where dispatchedId="+dispatchedId;
		Query query=sessionFactory.getCurrentSession().createQuery(hql);
		List<DispatchOrderDetails> dispatchOrderDetails=(List<DispatchOrderDetails>)query.list();
		dispatchOrderDetails.get(0).setSalesManMsg(reason);

		//Notification
		EmployeeDetailsDAOImpl employeeDetailsDAOImpl = new EmployeeDetailsDAOImpl(sessionFactory);
		EmployeeDetails employeeDetails = employeeDetailsDAOImpl
				.getEmployeeDetailsByemployeeId(dispatchOrderDetails.get(0).getEmployeeASMAndSM().getEmployeeId());


		
	}	

	/* (non-Javadoc)
	 * @see com.sheetalarch.dao.OrderDetailsDAO#fetchOrderDetailsListForWebApp(long)
	 * Dec 5, 20175:08:25 PM
	 */
	@Transactional
	public List<OrderDetails> fetchOrderDetailsListForWebApp(long boothId) {
		// TODO Auto-generated method stub
		
		String hql="from OrderDetails where booth.boothId="+boothId+" and orderStatus.status ='"+Constants.ORDER_STATUS_DELIVERED+"'"; 
		
		Query query=sessionFactory.getCurrentSession().createQuery(hql);
		List<OrderDetails> orderDetailsList=(List<OrderDetails>)query.list();
		if(orderDetailsList.isEmpty())
		{
			return null;
		}
		return orderDetailsList;
	}
	
	/***
	 * here show record of order details by filter and boothId 
	 */
	
	@Transactional
	public List<OrderDetails> fetchOrderDetailsListByBoothIdAndFilterForWebApp(String startDate,String endDate,String range,long boothId) {
		// TODO Auto-generated method stub
		
		String hql="";
		Query query;
		Calendar cal = Calendar.getInstance();
		SimpleDateFormat dateFormat=new SimpleDateFormat("yyyy-MM-dd");
		
		if (range.equals("range")) {
			hql="from OrderDetails where booth.boothId="+boothId+" and (date(orderDetailsAddedDatetime)>='"+startDate+"' and date(orderDetailsAddedDatetime)<='"+endDate+"')";
		}
		else if (range.equals("yesterday")) {
			cal.add(Calendar.DAY_OF_MONTH, -1);
			hql="from OrderDetails where booth.boothId="+boothId+" and (date(orderDetailsAddedDatetime)='"+dateFormat.format(cal.getTime())+"')";
		}
		else if (range.equals("last7days")) {
			cal.add(Calendar.DAY_OF_MONTH, -7);
			hql="from OrderDetails where booth.boothId="+boothId+" and (date(orderDetailsAddedDatetime)>='"+dateFormat.format(cal.getTime())+"')";
		}
		else if (range.equals("last1month")) {
			cal.add(Calendar.MONTH, -1);
			hql="from OrderDetails where booth.boothId="+boothId+" and (date(orderDetailsAddedDatetime)>='"+dateFormat.format(cal.getTime())+"')";
		}
		else if (range.equals("last3months")) {
			cal.add(Calendar.MONTH, -3);
			hql="from OrderDetails where booth.boothId="+boothId+" and (date(orderDetailsAddedDatetime)>='"+dateFormat.format(cal.getTime())+"')";
		}
		else if (range.equals("today")) {
			hql="from OrderDetails where booth.boothId="+boothId+" and (date(orderDetailsAddedDatetime)>='"+dateFormat.format(cal.getTime())+"')";
		}
		else if (range.equals("pickdate")) {
			hql="from OrderDetails where booth.boothId="+boothId+" and (date(orderDetailsAddedDatetime)='"+startDate+"')";
		}
		else if (range.equals("viewAll")) {
			hql="from OrderDetails where booth.boothId="+boothId+"";
		}
		
		
		query=sessionFactory.getCurrentSession().createQuery(hql);
		List<OrderDetails> orderDetailsList=(List<OrderDetails>)query.list();
		if(orderDetailsList.isEmpty())
		{
			return null;
		}
		return orderDetailsList;
	}
	
	@Transactional
	public List<ReOrderDetails> fetchReOrderDetailsListByBoothIdAndFilterForWebApp(String startDate,String endDate,String range,long boothId) {
		// TODO Auto-generated method stub
		
		String hql="";
		Query query;
		Calendar cal = Calendar.getInstance();
		SimpleDateFormat dateFormat=new SimpleDateFormat("yyyy-MM-dd");
		
		if (range.equals("range")) {
			hql="from ReOrderDetails where reOrderToBooth.boothId="+boothId+" and (date(reOrderAddedDatetime)>='"+startDate+"' and date(reOrderAddedDatetime)<='"+endDate+"')";
		}
		else if (range.equals("yesterday")) {
			cal.add(Calendar.DAY_OF_MONTH, -1);
			hql="from ReOrderDetails where reOrderToBooth.boothId="+boothId+" and (date(reOrderAddedDatetime)='"+dateFormat.format(cal.getTime())+"')";
		}
		else if (range.equals("last7days")) {
			cal.add(Calendar.DAY_OF_MONTH, -7);
			hql="from ReOrderDetails where reOrderToBooth.boothId="+boothId+" and (date(reOrderAddedDatetime)>='"+dateFormat.format(cal.getTime())+"')";
		}
		else if (range.equals("last1month")) {
			cal.add(Calendar.MONTH, -1);
			hql="from ReOrderDetails where reOrderToBooth.boothId="+boothId+" and (date(reOrderAddedDatetime)>='"+dateFormat.format(cal.getTime())+"')";
		}
		else if (range.equals("last3months")) {
			cal.add(Calendar.MONTH, -3);
			hql="from ReOrderDetails where reOrderToBooth.boothId="+boothId+" and (date(reOrderAddedDatetime)>='"+dateFormat.format(cal.getTime())+"')";
		}
		else if (range.equals("today")) {
			hql="from ReOrderDetails where reOrderToBooth.boothId="+boothId+" and (date(reOrderAddedDatetime)>='"+dateFormat.format(cal.getTime())+"')";
		}
		else if (range.equals("pickdate")) {
			hql="from ReOrderDetails where reOrderToBooth.boothId="+boothId+" and (date(reOrderAddedDatetime)='"+startDate+"')";
		}
		else if (range.equals("viewAll")) {
			hql="from ReOrderDetails where reOrderToBooth.boothId="+boothId+"";
		}
		
		
		query=sessionFactory.getCurrentSession().createQuery(hql);
		List<ReOrderDetails> reOrderDetailsList=(List<ReOrderDetails>)query.list();
		if(reOrderDetailsList.isEmpty())
		{
			return null;
		}
		return reOrderDetailsList;
	}

	/* (non-Javadoc)
	 * @see com.sheetalarch.dao.OrderDetailsDAO#fetchReOrderDetailsListForWebApp(long)
	 * Dec 5, 20175:08:25 PM
	 */
	@Transactional
	public List<ReOrderDetails> fetchReOrderDetailsListForWebApp(long boothId) {
		// TODO Auto-generated method stub
		
		String hql="from ReOrderDetails where reOrderToBooth.boothId="+boothId; 
		
		Query query=sessionFactory.getCurrentSession().createQuery(hql);
		List<ReOrderDetails> reOrderDetailsList=(List<ReOrderDetails>)query.list();
		if(reOrderDetailsList.isEmpty())
		{
			return null;
		}
		return reOrderDetailsList;
	}
	
	@Transactional
	
	public ReOrderDetails fetchReOrderDetailsByOrderId(String reOrderDetailsId) {

		String hql="from ReOrderDetails where reOrderDetailsId='" + reOrderDetailsId + "'";
		Query query = sessionFactory.getCurrentSession().createQuery(hql);

		List<ReOrderDetails> list = (List<ReOrderDetails>) query.list();
		
		if (list.isEmpty()) {
			return null;
		}

		return list.get(0);
	}

	
	@Transactional
	public String sendSMSTOShopsUsingOrderId(String orderIds,String smsText) {

		try {
			String[] shopsId2 = orderIds.split(",");
			
			for(int i=0; i<shopsId2.length; i++)
			{
				String id[]=shopsId2[i].split("REORD");
				if(id.length>1)
				{
					ReOrderDetails reOrderDetails=fetchReOrderDetailsByOrderId(shopsId2[i]);
					SendSMS.sendSMS(Long.parseLong(reOrderDetails.getReOrderToBooth().getContact().getMobileNumber()), smsText);
				}
				else
				{
					orderDetails=fetchOrderDetailsByOrderId(shopsId2[i]);
					SendSMS.sendSMS(Long.parseLong(orderDetails.getBooth().getContact().getMobileNumber()), smsText);
				}
				System.out.println("SMS send to : "+shopsId2[i]);
			}
			
			return "Success";
		} catch (Exception e) {
			System.out.println("sms sending failed "+e.toString());
			return "Failed";
			
		}
		
	}
	
	@Transactional
	public List<OrderDetails> fetchOrderDetailsForSMReport(String startDate, String endDate,String range, EmployeeDetails employeeDetails,String type) 
	{
		try {
			String hql="";
			Query query;
			Calendar cal = Calendar.getInstance();
			SimpleDateFormat dateFormat=new SimpleDateFormat("yyyy-MM-dd");
			
			if (range.equals("range")) {
				hql="from OrderDetails where employeeASMandSM.employeeId="+employeeDetails.getEmployee().getEmployeeId()+"  and employeeASMandSM.department.name='"+type+"' and (date(orderDetailsAddedDatetime)>='"+startDate+"' and date(orderDetailsAddedDatetime)<='"+endDate+"')  and orderStatus.status in ('"+Constants.ORDER_STATUS_DELIVERED+"','"+Constants.ORDER_STATUS_CANCELED+"') and reOrderQty=0";
			}
			else if (range.equals("yeaterday")) {
				cal.add(Calendar.DAY_OF_MONTH, -1);
				hql="from OrderDetails where employeeASMandSM.employeeId="+employeeDetails.getEmployee().getEmployeeId()+"   and employeeASMandSM.department.name='"+type+"' and (date(orderDetailsAddedDatetime)='"+dateFormat.format(cal.getTime())+"')  and orderStatus.status in ('"+Constants.ORDER_STATUS_DELIVERED+"','"+Constants.ORDER_STATUS_CANCELED+"') and reOrderQty=0";
			}
			else if (range.equals("last7days")) {
				cal.add(Calendar.DAY_OF_MONTH, -7);
				hql="from OrderDetails where employeeASMandSM.employeeId="+employeeDetails.getEmployee().getEmployeeId()+"   and employeeASMandSM.department.name='"+type+"' and (date(orderDetailsAddedDatetime)>='"+dateFormat.format(cal.getTime())+"')  and orderStatus.status in ('"+Constants.ORDER_STATUS_DELIVERED+"','"+Constants.ORDER_STATUS_CANCELED+"') and reOrderQty=0";
			}
			else if (range.equals("last1month")) {
				cal.add(Calendar.MONTH, -1);
				hql="from OrderDetails where employeeASMandSM.employeeId="+employeeDetails.getEmployee().getEmployeeId()+"   and employeeASMandSM.department.name='"+type+"' and (date(orderDetailsAddedDatetime)>='"+dateFormat.format(cal.getTime())+"')  and orderStatus.status in ('"+Constants.ORDER_STATUS_DELIVERED+"','"+Constants.ORDER_STATUS_CANCELED+"') and reOrderQty=0";
			}
			else if (range.equals("last3months")) {
				cal.add(Calendar.MONTH, -3);
				hql="from OrderDetails where employeeASMandSM.employeeId="+employeeDetails.getEmployee().getEmployeeId()+"   and employeeASMandSM.department.name='"+type+"' and (date(orderDetailsAddedDatetime)>='"+dateFormat.format(cal.getTime())+"')  and orderStatus.status in ('"+Constants.ORDER_STATUS_DELIVERED+"','"+Constants.ORDER_STATUS_CANCELED+"') and reOrderQty=0";
			}
			else if (range.equals("pickdate")) {
				hql="from OrderDetails where employeeASMandSM.employeeId="+employeeDetails.getEmployee().getEmployeeId()+"   and employeeASMandSM.department.name='"+type+"' and (date(orderDetailsAddedDatetime)='"+startDate+"')  and orderStatus.status in ('"+Constants.ORDER_STATUS_DELIVERED+"','"+Constants.ORDER_STATUS_CANCELED+"') and reOrderQty=0";
			}
			else if (range.equals("viewAll")) {
				hql="from OrderDetails where employeeASMandSM.employeeId="+employeeDetails.getEmployee().getEmployeeId()+"    and employeeASMandSM.department.name='"+type+"' and orderStatus.status in ('"+Constants.ORDER_STATUS_DELIVERED+"','"+Constants.ORDER_STATUS_CANCELED+"') and reOrderQty=0";
			}
			
			query=sessionFactory.getCurrentSession().createQuery(hql);
			List<OrderDetails> orderDetailsList=(List<OrderDetails>)query.list();
			
			if(orderDetailsList.isEmpty())
			{
				return null;			
			}	
			
			return orderDetailsList;
		} catch (HibernateException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			
			return null;
		}
	}
	
	@Transactional
    public List<OrderProductDetails> fetchOrderProductDetailByOrderId(String orderId) {
        // TODO Auto-generated method stub
        
        Query query=sessionFactory.getCurrentSession().createQuery("from OrderProductDetails where orderDetails.orderId='"+orderId+"'");
        List<OrderProductDetails> list=(List<OrderProductDetails>)query.list();
        if(list.isEmpty()){
            return null;
        }
        
        return list;
    
    }
	
	@Transactional
	public List<SalesManReportModel> fetchSalesManReportModel(String startDate, String endDate,String range)
	{
		String hql="",hqlReOrder="";
		Query query;
		Calendar cal = Calendar.getInstance();
		SimpleDateFormat dateFormat=new SimpleDateFormat("yyyy-MM-dd");
		List<SalesManReportModel> salesManReportModelList=new ArrayList<>();
		
		if (range.equals("range")) {
			hql="from OrderDetails where (date(deliveryDate)>='"+startDate+"' and date(deliveryDate)<='"+endDate+"')  and orderStatus.status in ('"+Constants.ORDER_STATUS_DELIVERED+"')";
			hqlReOrder="from ReOrderDetails where (date(reOrderAddedDatetime)>='"+startDate+"' and date(reOrderAddedDatetime)<='"+endDate+"') and orderDetails.reOrderStatus='"+Constants.REORDER_STATUS_COMPLETE+"'";
		}
		else if (range.equals("yesterday")) {
			cal.add(Calendar.DAY_OF_MONTH, -1);
			hql="from OrderDetails where (date(deliveryDate)='"+dateFormat.format(cal.getTime())+"')  and orderStatus.status in ('"+Constants.ORDER_STATUS_DELIVERED+"')";
			hqlReOrder="from ReOrderDetails where (date(reOrderAddedDatetime)='"+dateFormat.format(cal.getTime())+"') and orderDetails.reOrderStatus='"+Constants.REORDER_STATUS_COMPLETE+"'";
		}
		else if (range.equals("last7days")) {
			cal.add(Calendar.DAY_OF_MONTH, -7);
			hql="from OrderDetails where (date(deliveryDate)>='"+dateFormat.format(cal.getTime())+"')  and orderStatus.status in ('"+Constants.ORDER_STATUS_DELIVERED+"')";
			hqlReOrder="from ReOrderDetails where (date(reOrderAddedDatetime)>='"+dateFormat.format(cal.getTime())+"') and orderDetails.reOrderStatus='"+Constants.REORDER_STATUS_COMPLETE+"'";
		}
		else if (range.equals("last1month")) {
			cal.add(Calendar.MONTH, -1);
			hql="from OrderDetails where (date(deliveryDate)>='"+dateFormat.format(cal.getTime())+"')  and orderStatus.status in ('"+Constants.ORDER_STATUS_DELIVERED+"')";
			hqlReOrder="from ReOrderDetails where (date(reOrderAddedDatetime)>='"+dateFormat.format(cal.getTime())+"') and orderDetails.reOrderStatus='"+Constants.REORDER_STATUS_COMPLETE+"'";
		}
		else if (range.equals("last3months")) {
			cal.add(Calendar.MONTH, -3);
			hql="from OrderDetails where (date(deliveryDate)>='"+dateFormat.format(cal.getTime())+"')  and orderStatus.status in ('"+Constants.ORDER_STATUS_DELIVERED+"')";
			hqlReOrder="from ReOrderDetails where (date(reOrderAddedDatetime)>='"+dateFormat.format(cal.getTime())+"') and orderDetails.reOrderStatus='"+Constants.REORDER_STATUS_COMPLETE+"'";
		}
		else if (range.equals("today")) {
			hql="from OrderDetails where (date(deliveryDate)>='"+dateFormat.format(cal.getTime())+"')  and orderStatus.status in ('"+Constants.ORDER_STATUS_DELIVERED+"')";
			hqlReOrder="from ReOrderDetails where (date(reOrderAddedDatetime)>='"+dateFormat.format(cal.getTime())+"') and orderDetails.reOrderStatus='"+Constants.REORDER_STATUS_COMPLETE+"'";
		}
		else if (range.equals("pickdate")) {
			hql="from OrderDetails where (date(deliveryDate)='"+startDate+"')  and orderStatus.status in ('"+Constants.ORDER_STATUS_DELIVERED+"')";
			hqlReOrder="from ReOrderDetails where (date(reOrderAddedDatetime)='"+startDate+"') and orderDetails.reOrderStatus='"+Constants.REORDER_STATUS_COMPLETE+"'";
		}
		else if (range.equals("viewAll")) {
			hql="from OrderDetails  where orderStatus.status in ('"+Constants.ORDER_STATUS_DELIVERED+"')";
			hqlReOrder="from ReOrderDetails where orderDetails.reOrderStatus='"+Constants.REORDER_STATUS_COMPLETE+"'";
		}
		query=sessionFactory.getCurrentSession().createQuery(hql);
		List<OrderDetails> orderDetailsList=(List<OrderDetails>)query.list();
		
		query=sessionFactory.getCurrentSession().createQuery(hqlReOrder);
		List<ReOrderDetails> reOrderDetailsList=(List<ReOrderDetails>)query.list();
		
		if(orderDetailsList.isEmpty() && reOrderDetailsList.isEmpty())
		{
			return null;			
		}	
		
		long srno=1;
		DecimalFormat decimalFormat=new DecimalFormat("#0.00");
		EmployeeDetailsDAOImpl employeeDetailsDAOImpl=new EmployeeDetailsDAOImpl(sessionFactory);
		for(OrderDetails orderDetails: orderDetailsList)
		{
			EmployeeDetails employeeDetails=employeeDetailsDAOImpl.getEmployeeDetailsByemployeeId(orderDetails.getEmployeeASMandSM().getEmployeeId());
			salesManReportModelList.add(new SalesManReportModel(
					srno, 
					orderDetails.getOrderId(), 
					orderDetails.getBooth().getBoothId(), 
					orderDetails.getBooth().getBoothNo(), 
					orderDetails.getBooth().getOwnerName(), 
					orderDetails.getBooth().getRoute().getRouteNo(), 
					employeeDetails.getName(), 
					employeeDetails.getEmployeeDetailsId(), 
					Double.parseDouble(decimalFormat.format(orderDetails.getTotalAmountWithTax())), 
					dateFormat.format(orderDetails.getOrderDetailsAddedDatetime()),
					dateFormat.format(orderDetails.getDeliveryDate())));
			srno++;
		}
		for(ReOrderDetails reOrderDetails: reOrderDetailsList)
		{
			EmployeeDetails employeeDetails=employeeDetailsDAOImpl.getEmployeeDetailsByemployeeId(reOrderDetails.getOrderDetails().getEmployeeASMandSM().getEmployeeId());
			salesManReportModelList.add(new SalesManReportModel(
					srno, 
					reOrderDetails.getReOrderDetailsId(), 
					reOrderDetails.getReOrderToBooth().getBoothId(), 
					reOrderDetails.getReOrderToBooth().getBoothNo(), 
					reOrderDetails.getReOrderToBooth().getOwnerName(), 
					reOrderDetails.getReOrderToBooth().getRoute().getRouteNo(), 
					employeeDetails.getName(), 
					employeeDetails.getEmployeeDetailsId(), 
					Double.parseDouble(decimalFormat.format(reOrderDetails.getReOrderAmtWithTax())), 
					dateFormat.format(reOrderDetails.getReOrderAddedDatetime()),
					dateFormat.format(reOrderDetails.getReOrderAddedDatetime())));
			srno++;
		}
		return salesManReportModelList;
	}
	
	@Transactional
	public List<OrderReportModel> fetchOrderReport(String currrentPending)
	{
		List<OrderReportModel> reportModelList=new ArrayList<>();
		String hql="",hqlReOrder="";
		Query query;
		SimpleDateFormat dateFormat=new SimpleDateFormat("yyyy-MM-dd");
		
		if (currrentPending.equals("current")) {
			hql="from OrderDetails where date(orderDetailsAddedDatetime)=date(CURRENT_DATE())  and orderStatus.status not in ('"+Constants.ORDER_STATUS_DELIVERED+"','"+Constants.ORDER_STATUS_CANCELED+"')";
			hqlReOrder="from ReOrderDetails where (date(reOrderAddedDatetime)=date(CURRENT_DATE())) and orderDetails.reOrderStatus='"+Constants.REORDER_STATUS_PENDING+"'";
		}
		else if (currrentPending.equals("pending")) {
			hql="from OrderDetails where date(orderDetailsAddedDatetime)<date(CURRENT_DATE())  and orderStatus.status not in ('"+Constants.ORDER_STATUS_DELIVERED+"','"+Constants.ORDER_STATUS_CANCELED+"')";
			hqlReOrder="from ReOrderDetails where (date(reOrderAddedDatetime)<date(CURRENT_DATE())) and orderDetails.reOrderStatus='"+Constants.REORDER_STATUS_PENDING+"'";
		}
		else
		{
			hql="from OrderDetails where date(orderDetailsAddedDatetime)>date(CURRENT_DATE())  and orderStatus.status not in ('"+Constants.ORDER_STATUS_DELIVERED+"','"+Constants.ORDER_STATUS_CANCELED+"')";
			hqlReOrder="from ReOrderDetails where (date(reOrderAddedDatetime)>date(CURRENT_DATE())) and orderDetails.reOrderStatus='"+Constants.REORDER_STATUS_PENDING+"'";
		}
		query=sessionFactory.getCurrentSession().createQuery(hql);
		List<OrderDetails> orderDetailsList=(List<OrderDetails>)query.list();
		
		query=sessionFactory.getCurrentSession().createQuery(hqlReOrder);
		List<ReOrderDetails> reOrderDetailsList=(List<ReOrderDetails>)query.list();
		
		if(orderDetailsList.isEmpty() && reOrderDetailsList.isEmpty())
		{
			return null;			
		}		
		EmployeeDetailsDAOImpl employeeDetailsDAOImpl=new EmployeeDetailsDAOImpl(sessionFactory);
		DecimalFormat decimalFormat=new DecimalFormat("#0.00");
		long srno=1;
		for(OrderDetails orderDetails: orderDetailsList)
		{
			srno++;
			String orderStatusSM=""; 
			String orderStatusGK=""; 
			String orderStatusDB="";
			
			EmployeeDetails employeeDetails=employeeDetailsDAOImpl.getEmployeeDetailsByemployeeId(orderDetails.getEmployeeASMandSM().getEmployeeId());
			
			if(orderDetails.getOrderStatus().getStatus().equals(Constants.ORDER_STATUS_BOOKED))
			{
				orderStatusSM="Completed"; 
				orderStatusGK="Pending"; 
				orderStatusDB="Pending";
			}
			else if(orderDetails.getOrderStatus().getStatus().equals(Constants.ORDER_STATUS_PACKED))
			{
				orderStatusSM="Completed"; 
				orderStatusGK="Completed"; 
				orderStatusDB="Pending";
			}
			else if(orderDetails.getOrderStatus().getStatus().equals(Constants.ORDER_STATUS_ISSUED))
			{
				orderStatusSM="Completed"; 
				orderStatusGK="Completed"; 
				orderStatusDB="Pending";
			}
			
			reportModelList.add(new OrderReportModel(srno, 
					orderDetails.getOrderId(), 
					orderDetails.getBooth().getBoothId(), 
					orderDetails.getBooth().getBoothNo(), 
					orderDetails.getBooth().getOwnerName(), 
					orderDetails.getBooth().getRoute().getRouteNo(), 
					employeeDetails.getName(), 
					employeeDetails.getEmployeeDetailsId(), 
					Double.parseDouble(decimalFormat.format(orderDetails.getTotalAmountWithTax())), 
					dateFormat.format(orderDetails.getOrderDetailsAddedDatetime()), 
					dateFormat.format(orderDetails.getDeliveryDate()), 
					orderStatusSM, 
					orderStatusGK, 
					orderStatusDB));
		}
		
		for(ReOrderDetails reOrderDetails: reOrderDetailsList)
		{
			srno++;
			String orderStatusSM=""; 
			String orderStatusGK=""; 
			String orderStatusDB="";
			
			EmployeeDetails employeeDetails=employeeDetailsDAOImpl.getEmployeeDetailsByemployeeId(orderDetails.getEmployeeASMandSM().getEmployeeId());
			
			orderStatusSM="Completed"; 
			orderStatusGK="Completed"; 
			orderStatusDB="Pending";
			
			reportModelList.add(new OrderReportModel(srno, 
					reOrderDetails.getReOrderDetailsId(), 
					reOrderDetails.getReOrderToBooth().getBoothId(), 
					reOrderDetails.getReOrderToBooth().getBoothNo(), 
					reOrderDetails.getReOrderToBooth().getOwnerName(), 
					reOrderDetails.getReOrderToBooth().getRoute().getRouteNo(), 
					employeeDetails.getName(), 
					employeeDetails.getEmployeeDetailsId(), 
					Double.parseDouble(decimalFormat.format(reOrderDetails.getReOrderAmtWithTax())), 
					dateFormat.format(reOrderDetails.getReOrderAddedDatetime()), 
					dateFormat.format(reOrderDetails.getReOrderAddedDatetime()), 
					orderStatusSM, 
					orderStatusGK, 
					orderStatusDB));
		}
		
		return reportModelList;
	}
	@Transactional
	public List<OrderUsedProduct> fetchOrderUsedProductByProductId(long productId)
	{
		String hql="from OrderUsedProduct where product.productId="+productId;
		Query query=sessionFactory.getCurrentSession().createQuery(hql);
		List<OrderUsedProduct> orderUsedProductList=(List<OrderUsedProduct>)query.list();
		if(orderUsedProductList.isEmpty())
		{
			return null;
		}
		return orderUsedProductList;
	}
	@Transactional
	public String updateOrderUsedProductCurrentQuantity()
	{	
		ProductDAOImpl productDAOImpl=new ProductDAOImpl(sessionFactory);
		List<Product> productList=productDAOImpl.fetchProductListForWebApp();
		if(productList!=null)
		{
			for(Product product:productList)
			{
				List<OrderUsedProduct> orderUsedProductList=fetchOrderUsedProductByProductId(product.getProductId());
				if(orderUsedProductList!=null)
				{
					for(OrderUsedProduct orderUsedProduct : orderUsedProductList)
					{
						orderUsedProduct.setCurrentQuantity(product.getCurrentQuantity());
						sessionFactory.getCurrentSession().update(orderUsedProduct);
					}
				}
			}
		}
		
		return "Success";
	}
}
