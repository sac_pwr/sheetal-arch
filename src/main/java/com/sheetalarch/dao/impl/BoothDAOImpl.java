package com.sheetalarch.dao.impl;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.sheetalarch.admin.models.CustomerRerportModel;
import com.sheetalarch.admin.models.ManageBoothListModel;
import com.sheetalarch.admin.models.SingleCustomerOrderReport;
import com.sheetalarch.admin.models.SingleCustomerReport;
import com.sheetalarch.dao.BoothDAO;
import com.sheetalarch.entity.Area;
import com.sheetalarch.entity.Booth;
import com.sheetalarch.entity.EmployeeDetails;
import com.sheetalarch.entity.OrderDetails;
import com.sheetalarch.entity.Payment;
import com.sheetalarch.entity.ReOrderDetails;
import com.sheetalarch.entity.Route;
import com.sheetalarch.utils.Constants;
import com.sheetalarch.utils.SendSMS;

@Repository("boothDAO")

@Component
public class BoothDAOImpl implements BoothDAO{

	@Autowired
	SessionFactory sessionFactory;
	
	public BoothDAOImpl(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
		// TODO Auto-generated constructor stub
	}

	@Transactional
	public void saveBooth(Booth booth) {
		
		ContactDAOImpl contactDAOImpl=new ContactDAOImpl(sessionFactory);
		contactDAOImpl.save(booth.getContact());
		
		/*RouteDAOImpl routeDAOImpl=new RouteDAOImpl(sessionFactory);
		routeDAOImpl.saveRoute(booth.getRoute().getRouteId());*/
		booth.setBoothAddedDatetime(new Date());
		booth.setTaxType(Constants.TAX_TYPE_INTRA);
		sessionFactory.getCurrentSession().save(booth);
		
	}

	@Transactional
	public List<Booth> fetchBoothList() {
		// TODO Auto-generated method stub
		
		String hql="from Booth";
		Query query=sessionFactory.getCurrentSession().createQuery(hql);
		List<Booth> boothList=(List<Booth>)query.list();
		
		if(boothList.isEmpty())
		{
			return  null;
		}
		
		
		return boothList;
	}

	@Transactional
	public Booth fetchBoothByBoothId(long boothId) {
		// TODO Auto-generated method stub
		String hql="from Booth where boothId="+boothId;
		Query query=sessionFactory.getCurrentSession().createQuery(hql);
		List<Booth> boothList=(List<Booth>)query.list();
		
		if(boothList.isEmpty())
		{
			return  null;
		}
		return boothList.get(0);
	}
	
	@Transactional
	public Booth fetchBoothByBoothNo(String boothNo) {
		// TODO Auto-generated method stub
		String hql="from Booth where boothNo='"+boothNo+"'";
		Query query=sessionFactory.getCurrentSession().createQuery(hql);
		List<Booth> boothList=(List<Booth>)query.list();
		
		if(boothList.isEmpty())
		{
			return  null;
		}
		
		
		return boothList.get(0);
	}

	@Transactional
	public void updateBooth(Booth booth) {
		// TODO Auto-generated method stub
		
		ContactDAOImpl contactDAOImpl=new ContactDAOImpl(sessionFactory);
		contactDAOImpl.update(booth.getContact());
				
		sessionFactory.getCurrentSession().update(booth);
	}
	
	@Transactional
	public List<Booth> fetchBoothListByEmployeeId(long employeeId) {
		// TODO Auto-generated method stub
		String hql="";
		Query query=null;
		EmployeeDetailsDAOImpl employeeDetailsDAOImpl = new EmployeeDetailsDAOImpl(sessionFactory);
		EmployeeDetails employeeDetails=employeeDetailsDAOImpl.getEmployeeDetailsByemployeeId(employeeId);
		
		List<Route> routesList=new ArrayList<>();
		if(employeeDetails.getEmployee().getDepartment().getName().equals(Constants.AREA_SALESMAN_DEPT_NAME)){
			List<Area> areas=employeeDetailsDAOImpl.fetchAreaListByEmployeeId(employeeDetails.getEmployee().getEmployeeId());
			List<Route> routes=employeeDetailsDAOImpl.fetchRouteListByAreaId(areas.get(0).getAreaId());
			routesList.addAll(routes);
		}else{
		List<Route> routes=employeeDetailsDAOImpl.fetchEmployeeRouteListByEmployeeId(employeeId);
		routesList.addAll(routes);
		}
		List<Long> routeId=new ArrayList<>();
		for(Route route:routesList){
			routeId.add(route.getRouteId());
		}
		
		 hql="from Booth where route.routeId in (:ids) ";
		query=sessionFactory.getCurrentSession().createQuery(hql);
		query.setParameterList("ids", routeId);
		
		List<Booth> boothsList=(List<Booth>)query.list();
		
		if(boothsList.isEmpty()){
			return null;
		}
		return boothsList;
	}
	
	@Transactional
	public List<ManageBoothListModel> fetchManageBoothList()
	{
		List<ManageBoothListModel> manageBoothListModels=new ArrayList<>();
		List<Booth> boothList=fetchBoothList();
		
		if(boothList!=null)
		{
			for(Booth booth: boothList)
			{
				PaymentDAOImpl paymentDAOImpl=new PaymentDAOImpl(sessionFactory);
				
				DecimalFormat decimalFormat=new DecimalFormat("#0.00");
				manageBoothListModels.add(new ManageBoothListModel(booth, decimalFormat.format(paymentDAOImpl.balanceAmountOfBooth(booth.getBoothId()))));
			}
		}
		
		return manageBoothListModels;
	}
	
	@Transactional
	public String sendSMSTOShops(String shopsId,String smsText) {

		try {
			String[] shopsId2 = shopsId.split(",");
			
			for(int i=0; i<shopsId2.length; i++)
			{
				Booth booth=fetchBoothByBoothId(Long.parseLong(shopsId2[i]));
				SendSMS.sendSMS(Long.parseLong(booth.getContact().getMobileNumber()), smsText);
				System.out.println("SMS send to : "+shopsId2[i]);
			}
			
			return "Success";
		} catch (Exception e) {
			System.out.println("sms sending failed "+e.toString());
			return "Failed";
			
		}
		
	}
	/****
	 * customer report 
	 */
	@Transactional
	public List<CustomerRerportModel> fetchCustomerReport(String startDate,String endDate,String range)
	{
		List<CustomerRerportModel> customerRerportModelList=new ArrayList<>();
		List<Booth> boothList=fetchBoothList();
		DecimalFormat decimalFormat=new DecimalFormat("#0.00");
		PaymentDAOImpl paymentDAOImpl=new PaymentDAOImpl(sessionFactory);
		if(boothList!=null)
		{
			long srno=1;
			for(Booth booth: boothList)
			{
				OrderDetailsDAOImpl orderDetailsDAOImpl=new OrderDetailsDAOImpl(sessionFactory);
				List<OrderDetails> orderDetailsList=orderDetailsDAOImpl.fetchOrderDetailsListByBoothIdAndFilterForWebApp(startDate, endDate, range, booth.getBoothId());
				List<ReOrderDetails> reOrderDetailsList=orderDetailsDAOImpl.fetchReOrderDetailsListByBoothIdAndFilterForWebApp(startDate, endDate, range, booth.getBoothId());
				
				long noOfOrders=0;
				double totalAmount=0;
				double totalAmountPaid=0;
				double totalAmountUnPaid=0;
				
				if(orderDetailsList!=null)
				{
					for(OrderDetails orderDetails: orderDetailsList)
					{
						if(!orderDetails.getOrderStatus().getStatus().equals(Constants.ORDER_STATUS_CANCELED))
						{
							List<Payment> paymentList=paymentDAOImpl.fetchPaymentListByOrderDetailsId(orderDetails.getOrderId());
						
							if(paymentList==null)
							{
								totalAmountUnPaid+=orderDetails.getTotalAmountWithTax();
							}
							else
							{
								totalAmountUnPaid+=paymentList.get(0).getDueAmount();
							}
							
							totalAmount+=orderDetails.getTotalAmountWithTax();
						}
					}
					noOfOrders+=orderDetailsList.size();
				}
				
				if(reOrderDetailsList!=null)
				{
					for(ReOrderDetails reOrderDetails: reOrderDetailsList)
					{
						
						List<Payment> paymentList=paymentDAOImpl.fetchPaymentListByReOrderDetailsId(reOrderDetails.getReOrderDetailsId());
						if(paymentList==null)
						{
							totalAmountUnPaid+=reOrderDetails.getReOrderAmtWithTax();
						}
						else
						{
							totalAmountUnPaid+=paymentList.get(0).getDueAmount();
						}
						totalAmount+=reOrderDetails.getReOrderAmtWithTax();
					}
					noOfOrders+=reOrderDetailsList.size();
				}
				totalAmountPaid=totalAmount-totalAmountUnPaid;
				customerRerportModelList.add(new CustomerRerportModel(srno,booth, noOfOrders, Double.parseDouble(decimalFormat.format(totalAmount)), Double.parseDouble(decimalFormat.format(totalAmountPaid)), Double.parseDouble(decimalFormat.format(totalAmountUnPaid))));
				srno++;
			}
		}
		return customerRerportModelList;
	}
	
	/**
	 * 
	 * @param boothId
	 * @return
	 * Dec 12, 20172:45:46 PM
	 * 
	 * single customer report and show order details
	 * 
	 */
	@Transactional
	public SingleCustomerReport fetchSingleCustomerReport(String startDate,String endDate,String range,long boothId)
	{
		Booth booth=fetchBoothByBoothId(boothId);
		List<SingleCustomerOrderReport> singleCustomerOrderReportList=new ArrayList<>(); 
		OrderDetailsDAOImpl orderDetailsDAOImpl=new OrderDetailsDAOImpl(sessionFactory);
		List<OrderDetails> orderDetailsList=orderDetailsDAOImpl.fetchOrderDetailsListByBoothIdAndFilterForWebApp(startDate, endDate, range, booth.getBoothId());
		List<ReOrderDetails> reOrderDetailsList=orderDetailsDAOImpl.fetchReOrderDetailsListByBoothIdAndFilterForWebApp(startDate, endDate, range, booth.getBoothId());
		DecimalFormat decimalFormat=new DecimalFormat("#0.00");
		PaymentDAOImpl paymentDAOImpl=new PaymentDAOImpl(sessionFactory);
		SimpleDateFormat dateFormat=new SimpleDateFormat("yyyy-MM-dd");
		
		
		long srno=1;
		double mainTotalAmount=0, mainTotalAmountPaid=0, mainTotalAmountUnPaid=0;
		
		if(orderDetailsList!=null)
		{
			for(OrderDetails orderDetails: orderDetailsList)
			{				
				double totalAmount=0;
				double totalAmountPaid=0;
				double totalAmountUnPaid=0;
				String lastPaymentDate="--";
				String payMode="--";
				
				if(orderDetails.getOrderStatus().getStatus().equals(Constants.ORDER_STATUS_CANCELED))
				{
					singleCustomerOrderReportList.add(new SingleCustomerOrderReport(srno, 
							orderDetails.getOrderId(),
							orderDetails.getOrderStatus().getStatus(),
							dateFormat.format(orderDetails.getOrderDetailsAddedDatetime()), 
							"--", 
							dateFormat.format(orderDetails.getCancelDate()),
							totalAmount, 
							totalAmountPaid, 
							totalAmountUnPaid, 
							payMode, 
							lastPaymentDate));
					
					continue;
				}
				
				totalAmount=orderDetails.getTotalAmountWithTax();
				mainTotalAmount+=totalAmount;
				List<Payment> paymentList=paymentDAOImpl.fetchPaymentListByOrderDetailsId(orderDetails.getOrderId());
				if(paymentList==null)
				{
					totalAmountUnPaid=orderDetails.getTotalAmountWithTax();
					payMode="UnPaid";
				}
				else
				{
					totalAmountUnPaid=paymentList.get(0).getDueAmount();
					lastPaymentDate=dateFormat.format(paymentList.get(0).getPaidDate());
					if(totalAmountUnPaid==0)
					{
						payMode="Paid";
					}
					else
					{
						payMode="Partial Paid";
					}
				}
				
				totalAmountPaid=totalAmount-totalAmountUnPaid;
				mainTotalAmountPaid+=totalAmountPaid;
				mainTotalAmountUnPaid+=totalAmountUnPaid;
				singleCustomerOrderReportList.add(new SingleCustomerOrderReport(srno, 
																				orderDetails.getOrderId(),
																				orderDetails.getOrderStatus().getStatus(),
																				dateFormat.format(orderDetails.getOrderDetailsAddedDatetime()), 
																				dateFormat.format(orderDetails.getDeliveryDate()), 
																				"--",
																				totalAmount, 
																				totalAmountPaid, 
																				totalAmountUnPaid, 
																				payMode, 
																				lastPaymentDate));
				srno++;
			}			
			
		}
		
		if(reOrderDetailsList!=null)
		{
			for(ReOrderDetails reOrderDetails: reOrderDetailsList)
			{
				double totalAmount=0;
				double totalAmountPaid=0;
				double totalAmountUnPaid=0;
				String lastPaymentDate="--";
				String payMode="";
				
				totalAmount=reOrderDetails.getReOrderAmtWithTax();
				mainTotalAmount+=totalAmount;
				List<Payment> paymentList=paymentDAOImpl.fetchPaymentListByReOrderDetailsId(reOrderDetails.getReOrderDetailsId());
				if(paymentList==null)
				{
					totalAmountUnPaid=reOrderDetails.getReOrderAmtWithTax();
					payMode="UnPaid";
				}
				else
				{
					totalAmountUnPaid=paymentList.get(0).getDueAmount();
					lastPaymentDate=dateFormat.format(paymentList.get(0).getPaidDate());
					if(totalAmountUnPaid==0)
					{
						payMode="Paid";
					}
					else
					{
						payMode="Partial Paid";
					}
				}
				
				totalAmountPaid=totalAmount-totalAmountUnPaid;
				mainTotalAmountPaid+=totalAmountPaid;
				mainTotalAmountUnPaid+=totalAmountUnPaid;
				singleCustomerOrderReportList.add(new SingleCustomerOrderReport(srno, 
																				reOrderDetails.getReOrderDetailsId(),
																				"Delivered",
																				dateFormat.format(reOrderDetails.getReOrderAddedDatetime()), 
																				dateFormat.format(reOrderDetails.getReOrderAddedDatetime()), 
																				"--",
																				totalAmount, 
																				totalAmountPaid, 
																				totalAmountUnPaid, 
																				payMode, 
																				lastPaymentDate));
				srno++;
			}
		}
		
		return new SingleCustomerReport(booth, mainTotalAmount, mainTotalAmountPaid, mainTotalAmountUnPaid, singleCustomerOrderReportList);
	}
	
}