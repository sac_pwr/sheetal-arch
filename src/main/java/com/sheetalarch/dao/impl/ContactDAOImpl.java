package com.sheetalarch.dao.impl;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.sheetalarch.dao.ContactDAO;
import com.sheetalarch.entity.Contact;



@Repository("contactDAO")
@Component
public class ContactDAOImpl implements ContactDAO {

	@Autowired
	SessionFactory sessionFactory;
	
	public ContactDAOImpl(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
	
	public ContactDAOImpl() { }

	@Transactional
	public void save(Contact contact) {
		sessionFactory.getCurrentSession().save(contact);
	}

	@Transactional
	public void update(Contact contact) {
		sessionFactory.getCurrentSession().update(contact);
	}
	
	@Transactional
	public void delete(long id) {
		Contact ContactToDelete = new Contact();
		ContactToDelete.setContactId(id);
		sessionFactory.getCurrentSession().delete(ContactToDelete);
	}

}
