package com.sheetalarch.dao.impl;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.sheetalarch.dao.ASMReportDAO;
import com.sheetalarch.entity.Employee;
import com.sheetalarch.entity.EmployeeDetails;
import com.sheetalarch.entity.OrderDetails;
import com.sheetalarch.entity.OrderProductDetails;
import com.sheetalarch.entity.Payment;
import com.sheetalarch.entity.ReOrderDetails;
import com.sheetalarch.entity.ReOrderProductDetails;
import com.sheetalarch.entity.Route;
import com.sheetalarch.models.ASMMemberReportOrderListRequest;
import com.sheetalarch.models.CancelAsmReportResponse;
import com.sheetalarch.utils.Constants;

@Component
@Repository("cancelReportDAO")
public class ASMReportDAOImpl implements ASMReportDAO {

	@Autowired
	SessionFactory sessionFactory;

	@Transactional
	public List<CancelAsmReportResponse> fetchSmAndCancelOrderByAsmId(long employeeId) {

		EmployeeDetailsDAOImpl employeeDetailsDAOImpl = new EmployeeDetailsDAOImpl(sessionFactory);
		List<Employee> employeeList = employeeDetailsDAOImpl.fetchSalesmanByASMId(employeeId);

		List<CancelAsmReportResponse> cancelAsmReportResponsesList = new ArrayList<>();
		for (Employee employee : employeeList) {
			CancelAsmReportResponse cancelAsmReportResponse = new CancelAsmReportResponse();
			EmployeeDetails employeeDetails = employeeDetailsDAOImpl
					.getEmployeeDetailsByemployeeId(employee.getEmployeeId());
			List<Route> routes = employeeDetailsDAOImpl.fetchEmployeeRouteListByEmployeeId(employee.getEmployeeId());
			List<OrderDetails> cancelOrderList = todaysCancelOrderListByEmployeeId(employee.getEmployeeId());

			cancelAsmReportResponse.setEmployeeId(employee.getEmployeeId());
			cancelAsmReportResponse.setSalesmanName(employeeDetails.getName());
			cancelAsmReportResponse.setMobileNo(employeeDetails.getContact().getMobileNumber());
			cancelAsmReportResponse.setRouteNo(routes.get(0).getRouteNo());
			if (cancelOrderList == null) {
				cancelAsmReportResponse.setNoOfCancelOrder(0);
			} else {
				cancelAsmReportResponse.setNoOfCancelOrder(cancelOrderList.size());
			}

			cancelAsmReportResponsesList.add(cancelAsmReportResponse);
		}
		if (cancelAsmReportResponsesList.isEmpty()) {
			return null;
		}
		return cancelAsmReportResponsesList;
	}

	@Transactional
	public List<OrderDetails> todaysCancelOrderListByEmployeeId(long employeeId) {
		// TODO Auto-generated method stub

		String hql = "from OrderDetails where employeeASMandSM.employeeId=" + employeeId + " And orderStatus.status='"
				+ Constants.ORDER_STATUS_CANCELED + "'And date(cancelDate)=date(CURRENT_DATE())";
		// + "'"+a+"'";
		Query query = sessionFactory.getCurrentSession().createQuery(hql);
		List<OrderDetails> list = (List<OrderDetails>) query.list();
		if (list.isEmpty()) {
			return null;
		}
		return list;
	}

	@Transactional
	public List<ReOrderDetails> todaysReOrderListByEmployeeId(long employeeId) {

		String hql = "from ReOrderDetails where orderDetails.employeeASMandSM.employeeId=" + employeeId
				+ " and orderDetails.reOrderStatus='" + Constants.REORDER_STATUS_COMPLETE
				+ "'And date(orderDetails.deliveryDate)=date(CURRENT_DATE())";
		Query query = sessionFactory.getCurrentSession().createQuery(hql);
		List<ReOrderDetails> reOrderDetailsList = (List<ReOrderDetails>) query.list();
		if (reOrderDetailsList.isEmpty()) {
			return null;
		}
		return reOrderDetailsList;
	}

	@Transactional
	public List<CancelAsmReportResponse> fetchSmAndReOrderByAsmId(long employeeId) {

		EmployeeDetailsDAOImpl employeeDetailsDAOImpl = new EmployeeDetailsDAOImpl(sessionFactory);
		List<Employee> employeeList = employeeDetailsDAOImpl.fetchSalesmanByASMId(employeeId);

		List<CancelAsmReportResponse> cancelAsmReportResponsesList = new ArrayList<>();
		for (Employee employee : employeeList) {
			CancelAsmReportResponse cancelAsmReportResponse = new CancelAsmReportResponse();
			EmployeeDetails employeeDetails = employeeDetailsDAOImpl
					.getEmployeeDetailsByemployeeId(employee.getEmployeeId());
			List<Route> routes = employeeDetailsDAOImpl.fetchEmployeeRouteListByEmployeeId(employee.getEmployeeId());
			List<ReOrderDetails> reOrderDetailsList = todaysReOrderListByEmployeeId(employee.getEmployeeId());

			cancelAsmReportResponse.setEmployeeId(employee.getEmployeeId());
			cancelAsmReportResponse.setSalesmanName(employeeDetails.getName());
			cancelAsmReportResponse.setMobileNo(employeeDetails.getContact().getMobileNumber());
			cancelAsmReportResponse.setRouteNo(routes.get(0).getRouteNo());
			if (reOrderDetailsList == null) {
				cancelAsmReportResponse.setNoOfReOrder(0);
			} else {
				cancelAsmReportResponse.setNoOfReOrder(reOrderDetailsList.size());
			}
			cancelAsmReportResponsesList.add(cancelAsmReportResponse);
		}
		if (cancelAsmReportResponsesList.isEmpty()) {
			return null;
		}
		return cancelAsmReportResponsesList;
	}

	@Transactional
	public ReOrderDetails fetchReOrderDetailsByReOrderId(String reOrderId) {
		// TODO Auto-generated method stub
		String hql = "from ReOrderDetails where reOrderDetailsId='" + reOrderId + "'";
		Query query = sessionFactory.getCurrentSession().createQuery(hql);
		List<ReOrderDetails> reOrderDetails = (List<ReOrderDetails>) query.list();
		if (reOrderDetails.isEmpty()) {
			return null;
		}
		return reOrderDetails.get(0);
	}

	@Transactional
	public List<ReOrderProductDetails> fetchReOrderProductDetailsByReOrderId(String reOrderId) {
		// TODO Auto-generated method stub
		String hql = "from ReOrderProductDetails where reOrderDetails.reOrderDetailsId='" + reOrderId + "'";
		Query query = sessionFactory.getCurrentSession().createQuery(hql);
		List<ReOrderProductDetails> reOrderProductDetails = (List<ReOrderProductDetails>) query.list();
		reOrderProductDetails = makeReOrderProductDetailsListImageMNull(reOrderProductDetails);
		if (reOrderProductDetails.isEmpty()) {
			return null;
		}

		return reOrderProductDetails;
	}

	// here we make product image null
	@Transactional
	public List<ReOrderProductDetails> makeReOrderProductDetailsListImageMNull(
			List<ReOrderProductDetails> reOrderProductDetails) {
		List<ReOrderProductDetails> reOrderProductDetails2 = new ArrayList<>();
		for (ReOrderProductDetails reOrderProductDetails1 : reOrderProductDetails) {
			reOrderProductDetails1.getProduct().setProductImage(null);
			reOrderProductDetails2.add(reOrderProductDetails1);
		}
		return reOrderProductDetails2;
	}

	@Transactional
	public List<OrderDetails> fetchOrderDetailsListByEmpIdAndDateRangeforASMMemberReport(long employeeId,
			String fromDate, String toDate, String range) {
		// TODO Auto-generated method stub
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		String hql = "";
		Query query;
		Calendar cal = Calendar.getInstance();
		hql = "from OrderDetails where employeeASMandSM.employeeId=" + employeeId
				+ " And date(issueDate)=date(CURRENT_DATE())";

		/*
		 * if(range.equals("range")) {
		 * hql="from OrderDetails where employeeASMandSM.employeeId="+
		 * employeeId +
		 * " And (date(issueDate)>='"+fromDate+"' And date(issueDate)<='"+toDate
		 * +"')"; } else if(range.equals("last7days")){
		 * cal.add(Calendar.DAY_OF_MONTH, -7);
		 * hql="from OrderDetails where employeeASMandSM.employeeId="+
		 * employeeId +
		 * " And date(issueDate)>='"+dateFormat.format(cal.getTime())+"'"; }else
		 * if(range.equals("last1month")){ cal.add(Calendar.MONTH, -1);
		 * hql="from OrderDetails where employeeASMandSM.employeeId="+
		 * employeeId +
		 * " And date(issueDate)>='"+dateFormat.format(cal.getTime())+"'"; }else
		 * if(range.equals("last3months")){ cal.add(Calendar.MONTH, -3);
		 * hql="from OrderDetails where employeeASMandSM.employeeId="+
		 * employeeId +
		 * " And date(issueDate)>='"+dateFormat.format(cal.getTime())+"'";
		 * 
		 * }else if(range.equals("pickDate")){
		 * hql="from OrderDetails where employeeASMandSM.employeeId="+
		 * employeeId + " And date(issueDate)='"+fromDate +"'"; } else
		 * if(range.equals("viewAll")) {
		 * hql="from OrderDetails where employeeASMandSM.employeeId="+
		 * employeeId + " And orderDetails.orderStatus.status in ('"+Constants.
		 * ORDER_STATUS_DELIVERED+"','"+Constants.ORDER_STATUS_DELIVERED_PENDING
		 * +"')"; } else if(range.equals("currentDate")) {
		 * hql="from OrderDetails where employeeASMandSM.employeeId="+
		 * employeeId + " And date(issueDate)=date(CURRENT_DATE())"; }
		 */
		query = sessionFactory.getCurrentSession().createQuery(hql);
		List<OrderDetails> orderDetailsList = (List<OrderDetails>) query.list();
		if (orderDetailsList.isEmpty()) {
			return null;
		}
		return orderDetailsList;
	}

	@Transactional
	public List<Payment> fetchPaymentListByEmployeeIdAndDateRange(long employeeId, String fromDate, String toDate,
			String range) {
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		String hql = "", hql2 = "";
		Query query, query2;
		Calendar cal = Calendar.getInstance();

		if (range.equals("range")) {
			hql = "from Payment where  (date(paidDate)>='" + fromDate + "' And date(paidDate)<='" + toDate
					+ "')   And (orderDetails.employeeASMandSM.employeeId=" + employeeId + ")  order by paidDate desc";
			hql2 = "from Payment where  (date(paidDate)>='" + fromDate + "' And date(paidDate)<='" + toDate
					+ "')  And (reOrderDetails.orderDetails.employeeASMandSM.employeeId=" + employeeId
					+ ")  order by paidDate desc";
		} else if (range.equals("last7days")) {
			cal.add(Calendar.DAY_OF_MONTH, -7);
			hql = "from Payment where  date(paidDate)>='" + dateFormat.format(cal.getTime())
					+ "'  And (orderDetails.employeeASMandSM.employeeId=" + employeeId + ")  order by paidDate desc";
			hql2 = "from Payment where  date(paidDate)>='" + dateFormat.format(cal.getTime())
					+ "'  And (reOrderDetails.orderDetails.employeeASMandSM.employeeId=" + employeeId
					+ ")  order by paidDate desc";

		} else if (range.equals("last1month")) {
			cal.add(Calendar.MONTH, -1);
			hql = "from Payment where  date(paidDate)>='" + dateFormat.format(cal.getTime())
					+ "'  And (orderDetails.employeeASMandSM.employeeId=" + employeeId + ")  order by paidDate desc";
			hql2 = "from Payment where  date(paidDate)>='" + dateFormat.format(cal.getTime())
					+ "'  And (reOrderDetails.orderDetails.employeeASMandSM.employeeId=" + employeeId
					+ ")  order by paidDate desc";
		} else if (range.equals("last3months")) {
			cal.add(Calendar.MONTH, -3);
			hql = "from Payment where  date(paidDate)>='" + dateFormat.format(cal.getTime())
					+ "'  And (orderDetails.employeeASMandSM.employeeId=" + employeeId + ")  order by paidDate desc";
			hql2 = "from Payment where  date(paidDate)>='" + dateFormat.format(cal.getTime())
					+ "'  And (reOrderDetails.orderDetails.employeeASMandSM.employeeId=" + employeeId
					+ ")  order by paidDate desc";

		} else if (range.equals("pickDate")) {
			hql = "from Payment where  date(paidDate)='" + fromDate
					+ "'  And (orderDetails.employeeASMandSM.employeeId=" + employeeId + ")  order by paidDate desc";
			hql2 = "from Payment where  date(paidDate)='" + fromDate
					+ "' And (reOrderDetails.orderDetails.employeeASMandSM.employeeId=" + employeeId
					+ ")  order by paidDate desc";
		}
		/*
		 * else if(range.equals("viewAll")) {
		 * hql="from OrderDetails where employeeASMandSM.employeeId="+
		 * employeeId + " And orderDetails.orderStatus.status in ('"+Constants.
		 * ORDER_STATUS_DELIVERED+"','"+Constants.ORDER_STATUS_DELIVERED_PENDING
		 * +"')"; }
		 */
		else if (range.equals("currentDate")) {
			hql = "from Payment where date(paidDate)=date(CURRENT_DATE())  And (orderDetails.employeeASMandSM.employeeId="
					+ employeeId + ")  order by paidDate desc";
			hql2 = "from Payment where date(paidDate)=date(CURRENT_DATE())  And (reOrderDetails.orderDetails.employeeASMandSM.employeeId="
					+ employeeId + ")  order by paidDate desc";
		}

		query = sessionFactory.getCurrentSession().createQuery(hql);
		List<Payment> paymentsList = (List<Payment>) query.list();

		query2 = sessionFactory.getCurrentSession().createQuery(hql2);
		List<Payment> paymentsList2 = (List<Payment>) query2.list();

		paymentsList.addAll(paymentsList2);

		if (paymentsList.isEmpty()) {
			return null;
		}

		return paymentsList;
	}
}
