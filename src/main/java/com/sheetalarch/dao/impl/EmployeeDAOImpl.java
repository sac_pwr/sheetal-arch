package com.sheetalarch.dao.impl;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.sheetalarch.dao.EmployeeDAO;
import com.sheetalarch.entity.Employee;
import com.sheetalarch.entity.EmployeeDetails;

@Repository("employeeDAO")

@Component
public class EmployeeDAOImpl implements EmployeeDAO {

	@Autowired
	SessionFactory sessionFactory;
	
	@Autowired
	Employee employee;
	
	@Autowired
	EmployeeDetails employeeDetails;
	
	
	@Transactional
	public Employee login(String userId, String password) {
		// TODO Auto-generated method stub
		
		try{
			String hql="from Employee where userId='"+userId+"' and password='"+password+"'";
			Query query=sessionFactory.getCurrentSession().createQuery(hql);
			
			@SuppressWarnings("unchecked")
			List<Employee> employeeList=(List<Employee>) query.list();
			if(employeeList.isEmpty()){
				return null;
			}
			return employeeList.get(0);
		}catch (Exception e) {
			System.out.println("Inside login error:: "+e.toString());
			return null;
		}

	}
	@Transactional
	public boolean checkUserIdForWebApp(String userId) {

		String hql="from Employee where userId='"+userId+"'";
		Query query=sessionFactory.getCurrentSession().createQuery(hql);
		List<Employee> list=(List<Employee>)query.list();
		
		if(list.isEmpty())
		{
			return true;
		}
		
		return false;
	}
	
	
	@Transactional
	public boolean checkUserIdForUpdateForWebApp(String userId,String employeeId) {

		String hql="from Employee where userId='"+userId+"' and employeeId!="+employeeId;
		Query query=sessionFactory.getCurrentSession().createQuery(hql);
		List<Employee> list=(List<Employee>)query.list();
		
		if(list.isEmpty())
		{
			return true;
		}
		
		return false;
	}
	
	@Transactional
	public void saveForWebApp(Employee employee) {
		// TODO Auto-generated method stub
		sessionFactory.getCurrentSession().save(employee);
	}

	@Transactional
	public void updateForWebApp(Employee employee) {
		// TODO Auto-generated method stub
		sessionFactory.getCurrentSession().update(employee);
	}
}
