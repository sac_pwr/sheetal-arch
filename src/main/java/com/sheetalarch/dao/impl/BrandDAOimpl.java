package com.sheetalarch.dao.impl;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.sheetalarch.dao.BrandDAO;
import com.sheetalarch.entity.Brand;


@Repository("brandDAO")
@Component
public class BrandDAOimpl implements BrandDAO {

	@Autowired
	SessionFactory sessionFactory;
	
	@Transactional
	public void saveBrandForWebApp(Brand brand) {
		// TODO Auto-generated method stub
		sessionFactory.getCurrentSession().save(brand);
	}

	@Transactional
	public void updateBrandForWebApp(Brand brand) {
		// TODO Auto-generated method stub
		sessionFactory.getCurrentSession().update(brand);
	}

	@Transactional
	public Brand fetchBrandForWebApp(long brandId) {
		String hql="from Brand where brandId="+brandId;
		Query query=sessionFactory.getCurrentSession().createQuery(hql);
		List<Brand> list=(List<Brand>)query.list();
		if(list.isEmpty())
		{
			return null;
		}		
		return list.get(0);
	}

	@Transactional
	public List<Brand> fetchBrandListForWebApp() {
		String hql="from Brand";
		Query query=sessionFactory.getCurrentSession().createQuery(hql);
		List<Brand> list=(List<Brand>)query.list();
		if(list.isEmpty())
		{
			return null;
		}		
		return list;
	}

}
