package com.sheetalarch.dao.impl;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.sheetalarch.dao.RouteDAO;
import com.sheetalarch.entity.Cluster;
import com.sheetalarch.entity.Route;

@Repository("routeDAO")
@Component
public class RouteDAOImpl implements RouteDAO {

	@Autowired
	SessionFactory sessionFactory;
	
	public RouteDAOImpl(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}



	@Transactional
	public List<Route> fetchRouteByAreaId(long areaId)
	{
		String hql="from Route where area.areaId="+areaId;
		Query query=sessionFactory.getCurrentSession().createQuery(hql);
		List<Route> routeList=(List<Route>)query.list();
		if(routeList.isEmpty())
		{
			return null;
		}
		
		return routeList;
	}



	/* (non-Javadoc)
	 * @see com.sheetalarch.dao.RouteDAO#fetchRouteList()
	 * Nov 30, 20171:14:05 PM
	 */
	@Override
	public List<Route> fetchRouteList() {

		String hql="from Route";
		
		Query query=sessionFactory.getCurrentSession().createQuery(hql);
		List<Route> routeList=(List<Route>)query.list();
			
		if(routeList.isEmpty())
		{
			return null;
		}
		
		return routeList;
	}



	/* (non-Javadoc)
	 * @see com.sheetalarch.dao.RouteDAO#saveRoute(com.sheetalarch.entity.Route)
	 * Nov 30, 20171:14:05 PM
	 */
	@Override
	public void saveRoute(Route route) {
		// TODO Auto-generated method stub
		sessionFactory.getCurrentSession().save(route);
	}



	/* (non-Javadoc)
	 * @see com.sheetalarch.dao.RouteDAO#updateRoute(com.sheetalarch.entity.Route)
	 * Nov 30, 20171:14:05 PM
	 */
	@Override
	public void updateRoute(Route route) {
		// TODO Auto-generated method stub
		sessionFactory.getCurrentSession().update(route);
	}
	@Transactional
	public Route fetchRouteByRouteId(long routeId) {
		// TODO Auto-generated method stub
		String hql="from Route where routeId="+routeId;
		
		Query query=sessionFactory.getCurrentSession().createQuery(hql);
		List<Route> routeList=(List<Route>)query.list();
			
		if(routeList.isEmpty())
		{
			return null;
		}
		
		return routeList.get(0);
	}
}
