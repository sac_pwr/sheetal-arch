package com.sheetalarch.dao.impl;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.sheetalarch.dao.CategoriesDAO;
import com.sheetalarch.entity.Categories;
import com.sheetalarch.utils.Constants;


@Repository("categoriesDAO")

public class CategoriesDAOImpl implements CategoriesDAO {
	@Autowired
	SessionFactory sessionFactory;

	public CategoriesDAOImpl(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	public CategoriesDAOImpl() {
	}

	@Transactional
	public void save(Categories categories) {
		sessionFactory.getCurrentSession().save(categories);
	}

	@Transactional
	public void Update(Categories categories) {
		sessionFactory.getCurrentSession().update(categories);
	}

	@Transactional
	public List<Categories> getCategoriesList() {
		String hql = "from Categories";
		Query query = sessionFactory.getCurrentSession().createQuery(hql);
		System.out.println("Query : " + query);
		@SuppressWarnings("unchecked")
		List<Categories> categories = (List<Categories>) query.list();
		if (categories.isEmpty()) {
			return null;
		}
		return categories;
	}

	// Admin Part

	@Transactional
	public List<Categories> getCategoriesListAdmin() {
		String hql = "from Categories";
		Query query = sessionFactory.getCurrentSession().createQuery(hql);
		@SuppressWarnings("unchecked")
		List<Categories> categoriesList = (List<Categories>) query.list();

		if (categoriesList != null && !categoriesList.isEmpty()) {
			return categoriesList;
		}
		return categoriesList;
	}

	@Transactional
	public boolean getCategoryByNameBoolean(String name) {
		String hql = "from Categories where categoryName = '" + name + "' ";
		Query query = sessionFactory.getCurrentSession().createQuery(hql);
		@SuppressWarnings("unchecked")
		List<Categories> list = (List<Categories>) query.list();
		if (list != null && !list.isEmpty()) {
			return true;
		}
		return false;
	}

	@Transactional
	public Categories getById(long id) {
		String hql = "from Categories where categoryId=" + id;
		Query query = sessionFactory.getCurrentSession().createQuery(hql);
		@SuppressWarnings("unchecked")
		List<Categories> categoriesList = (List<Categories>) query.list();
		if (categoriesList != null && !categoriesList.isEmpty()) {
			return categoriesList.get(0);
		}

		return categoriesList.get(0);
	}

	@Transactional
	public void delete(long id) {
		Categories CategoryToDelete = new Categories();
		CategoryToDelete.setCategoryId(id);
		sessionFactory.getCurrentSession().delete(CategoryToDelete);
	}

	@Override
	public void saveCategoriesForWebApp(Categories categories) {
		// TODO Auto-generated method stub
		sessionFactory.getCurrentSession().save(categories);
	}

	@Override
	public void updateCategoriesForWebApp(Categories categories) {
		// TODO Auto-generated method stub
		sessionFactory.getCurrentSession().update(categories);
	}

	@Override
	public Categories fetchCategoriesForWebApp(long categoriesId) {
		String hql = "from Categories where categoryId=" + categoriesId;
		Query query = sessionFactory.getCurrentSession().createQuery(hql);
		@SuppressWarnings("unchecked")
		List<Categories> categoriesList = (List<Categories>) query.list();
		if (categoriesList.isEmpty()) {
			return null;
		}

		return categoriesList.get(0);
	}

	@Override
	public List<Categories> fetchCategoriesListForWebApp() {
		String hql = "from Categories";
		Query query = sessionFactory.getCurrentSession().createQuery(hql);
		@SuppressWarnings("unchecked")
		List<Categories> categoriesList = (List<Categories>) query.list();

		if (categoriesList.isEmpty()) {
			return null;
		}
		return categoriesList;
	}

}
