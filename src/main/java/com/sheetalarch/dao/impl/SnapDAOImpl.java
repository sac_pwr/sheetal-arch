package com.sheetalarch.dao.impl;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.sheetalarch.dao.SnapDAO;
import com.sheetalarch.entity.DispatchOrderDetails;
import com.sheetalarch.entity.DispatchOrderProductDetails;
import com.sheetalarch.entity.OrderDetails;
import com.sheetalarch.entity.Payment;
import com.sheetalarch.entity.ReOrderDetails;
import com.sheetalarch.models.GkSnapProductResponse;
import com.sheetalarch.models.SnapSalesmanResponse;
import com.sheetalarch.utils.Constants;

@Repository("snapDAO")
@Component
public class SnapDAOImpl implements SnapDAO {

	@Autowired
	SessionFactory sessionFactory;

	@Transactional
	public SnapSalesmanResponse fetchSnapShotDetailsForSalemanByEmployeeId(long employeeId) {

		SnapSalesmanResponse salesmanResponse = new SnapSalesmanResponse();
		long orderTaken = noOfOrderTaken(employeeId);
		long orderDelivered = noOfOrderDelivered(employeeId);
		long orderCanceled = noOfOrderCanceled(employeeId);
		long noOfReOrder=noOfReOrder(employeeId);
		double totalCollection = totalCollection(employeeId);
		double totalCollectionOfReOrder=totalCollectionOfReOrder(employeeId);
		
		long quantityReceived = quantityReceived(employeeId);
		long quantityDelivered=quantityDelivered(employeeId);
		long reOrderQquantityDelivered=reOrderQuantityDelivered(employeeId);
		long remainngQuantity=quantityReceived-quantityDelivered-reOrderQquantityDelivered;
		
		salesmanResponse.setTotalNoOfOrder(orderTaken);
		salesmanResponse.setTotalNoOfOrderDelivered(orderDelivered);
		salesmanResponse.setTotalCancelOrder(orderCanceled);
		salesmanResponse.setTotalNoOfReOrder(noOfReOrder);
		salesmanResponse.setTotalCollectionAmount(totalCollection+totalCollectionOfReOrder);
		salesmanResponse.setTotalNoOfQtyReceivedFromGk(quantityReceived);
		salesmanResponse.setTotalNoOfQtyDelivered(quantityDelivered+reOrderQquantityDelivered);
		salesmanResponse.setRemainingQty(remainngQuantity);

		return salesmanResponse;
	}

	@Transactional
	public long noOfOrderTaken(long employeeId) {

		String hql = "from OrderDetails where employeeASMandSM.employeeId=" + employeeId
				+ " And date(orderDetailsAddedDatetime)=date(CURRENT_DATE())";

		Query query = sessionFactory.getCurrentSession().createQuery(hql);

		@SuppressWarnings("unchecked")
		List<OrderDetails> orderDetailsListForOrderTaken = (List<OrderDetails>) query.list();
		if (orderDetailsListForOrderTaken.isEmpty()) {
			return 0;
		}
		return orderDetailsListForOrderTaken.size();
	}

	@Transactional
	public long noOfOrderDelivered(long employeeId) {

		String hql = "from OrderDetails where employeeASMandSM.employeeId=" + employeeId
				+ " And date(deliveryDate)=date(CURRENT_DATE()) And orderStatus.status='"
				+ Constants.ORDER_STATUS_DELIVERED + "'";

		Query query = sessionFactory.getCurrentSession().createQuery(hql);

		@SuppressWarnings("unchecked")
		List<OrderDetails> orderDetailsListForOrderDelivery = (List<OrderDetails>) query.list();
		if (orderDetailsListForOrderDelivery.isEmpty()) {
			return 0;
		}
		return orderDetailsListForOrderDelivery.size();
	}

	@Transactional
	public long noOfOrderCanceled(long employeeId) {

		String hql = "from OrderDetails where employeeASMandSM.employeeId=" + employeeId
				+ " And date(cancelDate)=date(CURRENT_DATE()) And orderStatus.status='"
				+ Constants.ORDER_STATUS_CANCELED + "'";
		Query query = sessionFactory.getCurrentSession().createQuery(hql);

		@SuppressWarnings("unchecked")
		List<OrderDetails> orderDetailsListForOrderCancel = (List<OrderDetails>) query.list();
		if (orderDetailsListForOrderCancel.isEmpty()) {
			return 0;
		}
		return orderDetailsListForOrderCancel.size();
	}

	@Transactional
	public long noOfReOrder(long employeeId){
		String hql="from ReOrderDetails where orderDetails.employeeASMandSM.employeeId=" + employeeId+" and date(reOrderAddedDatetime)=date(CURRENT_DATE())";
		Query query=sessionFactory.getCurrentSession().createQuery(hql);
		List<ReOrderDetails> list=(List<ReOrderDetails>)query.list();
		if(list.isEmpty()){
			return 0;
		}
		return list.size();
	}
	@Transactional
	public double totalCollection(long employeeId) {

		String hql = "from Payment where orderDetails.employeeASMandSM.employeeId=" + employeeId
				+ " And date(paidDate)=date(CURRENT_DATE())";
		Query query = sessionFactory.getCurrentSession().createQuery(hql);

		@SuppressWarnings("unchecked")
		List<Payment> paymentsList = (List<Payment>) query.list();
		double amount = 0;
		if (paymentsList.isEmpty()) {
			return 0;
		}
		for (Payment payment : paymentsList) {
			amount += payment.getPaidAmount();
		}
		return amount;
	}

	@Transactional
	public double totalCollectionOfReOrder(long employeeId){
		String hql="from Payment where reOrderDetails.orderDetails.employeeASMandSM.employeeId=" + employeeId + " And date(paidDate)=date(CURRENT_DATE())";
		Query query=sessionFactory.getCurrentSession().createQuery(hql);
		List<Payment> paymentsList=(List<Payment>)query.list();
		double amount = 0;
		if (paymentsList.isEmpty()) {
			return 0;
		}
		for (Payment payment : paymentsList) {
			amount += payment.getPaidAmount();
		}
		return amount;
	}
	
	@Transactional
	public long quantityReceived(long employeeId) {

		String hql = "from DispatchOrderDetails where employeeASMAndSM.employeeId=" + employeeId
				+ " And date(deliveryDate)=date(CURRENT_DATE()) And orderStatus.status='"+Constants.ORDER_STATUS_ISSUED+"'";
		Query query = sessionFactory.getCurrentSession().createQuery(hql);

		@SuppressWarnings("unchecked")
		List<DispatchOrderDetails> dispatchOrderDetailsList = (List<DispatchOrderDetails>) query.list();
		if (dispatchOrderDetailsList.isEmpty()) {
			return 0;
		}

		return dispatchOrderDetailsList.get(0).getTotalQty();
	}

	@Transactional
	public long quantityDelivered(long employeeId) {

		String hql = "from OrderDetails where employeeASMandSM.employeeId=" + employeeId
				+ " And date(deliveryDate)=date(CURRENT_DATE()) And orderStatus.status='"+Constants.ORDER_STATUS_DELIVERED+"'";
		Query query=sessionFactory.getCurrentSession().createQuery(hql);
		
		@SuppressWarnings("unchecked")
		List<OrderDetails> orderDetailsList=(List<OrderDetails>)query.list();
		long quantityDelivered=0;
		if(orderDetailsList.isEmpty()){
			return 0;
		}
		for(OrderDetails details:orderDetailsList){
			quantityDelivered+=details.getTotalQuantity();
		}
		return quantityDelivered;
	}
	
	@Transactional
	public long reOrderQuantityDelivered(long employeeId) {

		String hql = "from ReOrderDetails where orderDetails.employeeASMandSM.employeeId=" + employeeId
				+ " And date(orderDetails.deliveryDate)=date(CURRENT_DATE())";
		Query query=sessionFactory.getCurrentSession().createQuery(hql);
		
		@SuppressWarnings("unchecked")
		List<ReOrderDetails> reOrderDetailsList=(List<ReOrderDetails>)query.list();
		long reOrderQuantityDelivered=0;
		if(reOrderDetailsList.isEmpty()){
			return 0;
		}
		for(ReOrderDetails details:reOrderDetailsList){
			reOrderQuantityDelivered+=details.getReOrderQty();
		}
		return reOrderQuantityDelivered;
	}

	@Transactional
	public List<DispatchOrderProductDetails> fetchDispatchedOrderProductDetailsByGkId(long employeeId) {
		
		String hql="";
		Query query;
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.DAY_OF_MONTH, +1); // here we get the next date from
											// today date
		
		hql="from DispatchOrderProductDetails where dispatchOrderDetails.employeeGk.employeeId="+employeeId+" and dispatchOrderDetails.orderStatus.status='"+Constants.ORDER_STATUS_BOOKED+"' and date(dispatchOrderDetails.deliveryDate)=date(CURRENT_DATE()) or date(dispatchOrderDetails.deliveryDate)='"+dateFormat.format(cal.getTime())+"'";
		query=sessionFactory.getCurrentSession().createQuery(hql);
		List<DispatchOrderProductDetails> dispatchOrderProductDetailsList=(List<DispatchOrderProductDetails>)query.list();
		if(dispatchOrderProductDetailsList.isEmpty()){
			return null;
		}
		return dispatchOrderProductDetailsList;
	}

	@Transactional
	public List<GkSnapProductResponse> fetchSnapProductDetailsForGk(long employeeId) {
		
		List<DispatchOrderProductDetails> dispatchOrderProductDetailsList=fetchDispatchedOrderProductDetailsByGkId(employeeId);
		if(dispatchOrderProductDetailsList==null){
			return null;
		}
		long requiredQty;
		Map<Long, GkSnapProductResponse> map=new HashMap<>();
		for(DispatchOrderProductDetails dispatchOrderProductDetails: dispatchOrderProductDetailsList){
			boolean exist=false;
			for(Map.Entry<Long, GkSnapProductResponse> entry : map.entrySet())
			{
				if(dispatchOrderProductDetails.getProduct().getProductId()==entry.getKey()){
					exist=true;
				}
			}
			GkSnapProductResponse gkSnapProductResponse=new GkSnapProductResponse();
			long totalQuantity=dispatchOrderProductDetails.getTotalQty();
				if(exist==false){					
					gkSnapProductResponse.setProductName(dispatchOrderProductDetails.getProduct().getProductName());
					gkSnapProductResponse.setAvailableQty(dispatchOrderProductDetails.getProduct().getCurrentQuantity());
					gkSnapProductResponse.setTotalOrderedQty(totalQuantity);
					requiredQty=gkSnapProductResponse.getTotalOrderedQty()-gkSnapProductResponse.getAvailableQty();
					if(requiredQty<0){
						gkSnapProductResponse.setRequiredQty(0);	
					}else{
					gkSnapProductResponse.setRequiredQty(requiredQty);
					}
					//gkSnapProductResponse.setRequiredQty(totalQuantity-dispatchOrderProductDetails.getProduct().getCurrentQuantity());
					map.put(dispatchOrderProductDetails.getProduct().getProductId(), gkSnapProductResponse);
				}else{
					gkSnapProductResponse=map.get(dispatchOrderProductDetails.getProduct().getProductId());
					
					gkSnapProductResponse.setProductName(dispatchOrderProductDetails.getProduct().getProductName());
					gkSnapProductResponse.setAvailableQty(dispatchOrderProductDetails.getProduct().getCurrentQuantity());
					gkSnapProductResponse.setTotalOrderedQty(totalQuantity+gkSnapProductResponse.getTotalOrderedQty());
					
					requiredQty=gkSnapProductResponse.getTotalOrderedQty()-gkSnapProductResponse.getAvailableQty();
					if(requiredQty<0){
						gkSnapProductResponse.setRequiredQty(0);	
					}else{
					gkSnapProductResponse.setRequiredQty(requiredQty);
					}
					//gkSnapProductResponse.setRequiredQty(totalQuantity-dispatchOrderProductDetails.getProduct().getCurrentQuantity());
					map.put(dispatchOrderProductDetails.getProduct().getProductId(), gkSnapProductResponse);
					
				}

		}
		List<GkSnapProductResponse> list=new ArrayList<>();
		for(Map.Entry<Long, GkSnapProductResponse> map2:map.entrySet()){
			list.add(map2.getValue());
		}
		return list;
	}

}
