package com.sheetalarch.dao.impl;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.sheetalarch.admin.models.CollectionReportMain;
import com.sheetalarch.admin.models.CollectionReportPaymentDetails;
import com.sheetalarch.dao.PaymentDAO;
import com.sheetalarch.entity.DeliveredProduct;
import com.sheetalarch.entity.OrderDetails;
import com.sheetalarch.entity.OrderProductDetails;
import com.sheetalarch.entity.OrderStatus;
import com.sheetalarch.entity.Payment;
import com.sheetalarch.entity.ReOrderDetails;
import com.sheetalarch.entity.ReOrderProductDetails;
import com.sheetalarch.models.OrderDetailsForPayment;
import com.sheetalarch.models.PaymentNotReceiveRequest;
import com.sheetalarch.models.PaymentTakeRequest;
import com.sheetalarch.models.ReOrderPaymentNotReceiveRequest;
import com.sheetalarch.models.ReOrderPaymentTakeRequest;
import com.sheetalarch.utils.Constants;
import com.sheetalarch.utils.ImageConvertor;
import com.sheetalarch.utils.ReOrderIdGenerator;

@Repository("paymentDAO")
@Component
public class PaymentDAOImpl implements PaymentDAO {

	@Autowired
	SessionFactory sessionFactory;
	@Autowired
	Payment payment;

	@Autowired
	OrderStatus orderStatus;

	@Autowired
	DeliveredProduct deliveredProduct;
	
	/**
	 * 
	 * Dec 5, 20176:03:55 PM
	 */
	public PaymentDAOImpl(SessionFactory sessionFactory) {
		// TODO Auto-generated constructor stub
		this.sessionFactory=sessionFactory;
	}

	@Transactional
	public void updatePaymentStatusAndOrderStatus(PaymentNotReceiveRequest paymentNotReceiveRequest) {
		// TODO Auto-generated method stub

		OrderDetailsDAOImpl orderDetailsDAOImpl = new OrderDetailsDAOImpl(sessionFactory);
		OrderDetails orderDetails = orderDetailsDAOImpl
				.fetchOrderDetailsByOrderId(paymentNotReceiveRequest.getOrderId());

		orderStatus = orderDetailsDAOImpl.fetchOrderStatus(Constants.ORDER_STATUS_DELIVERED);
		orderDetails.setOrderStatus(orderStatus);
		// orderDetails.setDeliveryDate(new Date());

		payment.setOrderDetails(orderDetails);
		orderDetails.setPayStatus(false);

		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy/MM/dd");
		// Date date=new Date();
		try {
			Date date = simpleDateFormat.parse(paymentNotReceiveRequest.getNextDueDate());
			payment.setDueDate(date);
			orderDetails.setOrderDetailsPaymentTakeDatetime(date);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		payment.setDueAmount(orderDetails.getTotalAmountWithTax());

		payment.setTotalAmount(orderDetails.getTotalAmountWithTax());
		payment.setPaidAmount(0);
		payment.setReOrderDetails(null);

		sessionFactory.getCurrentSession().save(payment);
	}

	@Transactional
	public void updatePaymentNotReceiveStatusOfReOrder(
			ReOrderPaymentNotReceiveRequest reOrderPaymentNotReceiveRequest) {

		ReOrderDetails reOrderDetails = new ReOrderDetails();

		// in below two lines we are generating the reorderId
		ReOrderIdGenerator reOrderIdGenerator = new ReOrderIdGenerator(sessionFactory);
		reOrderDetails.setReOrderDetailsId(reOrderIdGenerator.generate());

		// in below lines we are going to set the values Of Reorder Entity
		reOrderDetails.setOrderDetails(reOrderPaymentNotReceiveRequest.getOrderDetails());
		reOrderDetails.setReOrderAddedDatetime(new Date());
		reOrderDetails.setReOrderToBooth(reOrderPaymentNotReceiveRequest.getBooth());
		reOrderDetails.setReOrderAmt(reOrderPaymentNotReceiveRequest.getOrderDetails().getReOrderAmt());
		reOrderDetails.setReOrderAmtWithTax(reOrderPaymentNotReceiveRequest.getOrderDetails().getReOrderAmtWithTax());
		reOrderDetails.setReOrderQty(reOrderPaymentNotReceiveRequest.getOrderDetails().getReOrderQty());

		sessionFactory.getCurrentSession().save(reOrderDetails); // here we are
																	// saving
																	// the
																	// reOrderDetails

		OrderDetailsDAOImpl orderDetailsDAOImpl = new OrderDetailsDAOImpl(sessionFactory);
		OrderDetails orderDetails = orderDetailsDAOImpl
				.fetchOrderDetailsByOrderId(reOrderPaymentNotReceiveRequest.getOrderDetails().getOrderId());

		orderDetails.setReOrderAmt(orderDetails.getReOrderAmt() - reOrderDetails.getReOrderAmt());
		orderDetails.setReOrderAmtWithTax(orderDetails.getReOrderAmtWithTax() - reOrderDetails.getReOrderAmtWithTax());
		orderDetails.setReOrderQty(orderDetails.getReOrderQty() - reOrderDetails.getReOrderQty());
		if (orderDetails.getReOrderQty() == 0) {
			orderDetails.setReOrderStatus(Constants.REORDER_STATUS_COMPLETE);
		}

		for (OrderProductDetails orderProductDetails : reOrderPaymentNotReceiveRequest.getOrderProductDetails()) {

			ReOrderProductDetails reOrderProductDetails = new ReOrderProductDetails();

			// in below lines we are going to set the values Of
			// ReorderProductDetails Entity
			reOrderProductDetails.setProduct(orderProductDetails.getProduct());
			reOrderProductDetails.setReOrderPurchaseAmt(orderProductDetails.getPurchaseAmount());
			reOrderProductDetails.setReOrderQty(orderProductDetails.getPurchaseQuantity());
			reOrderProductDetails.setReOrderDetails(reOrderDetails);
			reOrderProductDetails.setSellingRate(orderProductDetails.getSellingRate());

			sessionFactory.getCurrentSession().save(reOrderProductDetails);// here
																			// we
																			// are
																			// saving
																			// the
																			// ReOrderProduct
																			// details
		}

		payment.setReOrderDetails(reOrderDetails);
		payment.setOrderDetails(null);
		reOrderDetails.setPayStatus(false);

		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy/MM/dd");
		// Date date=new Date();
		try {
			Date date = simpleDateFormat.parse(reOrderPaymentNotReceiveRequest.getNextDueDate());
			payment.setDueDate(date);
			reOrderDetails.setReOrderDetailsPaymentTakeDatetime(date);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		/*
		 * OrderDetailsDAOImpl orderDetailsDAOImpl = new
		 * OrderDetailsDAOImpl(sessionFactory); OrderDetails orderDetails =
		 * orderDetailsDAOImpl
		 * .fetchOrderDetailsByOrderId(reOrderPaymentNotReceiveRequest.
		 * getOrderDetails().getOrderId());
		 * orderDetails.setReOrderQty(orderDetails.getReOrderQty() -
		 * reOrderDetails.getReOrderQty()); if (orderDetails.getReOrderQty() ==
		 * 0) {
		 * orderDetails.setReOrderStatus(Constants.REORDER_STATUS_COMPLETE); }
		 */
		payment.setDueAmount(reOrderDetails.getReOrderAmtWithTax());

		payment.setTotalAmount(reOrderDetails.getReOrderAmtWithTax());
		payment.setPaidAmount(0);

		sessionFactory.getCurrentSession().save(payment);

	}

	@Transactional
	public void savePaymentStatus(PaymentTakeRequest paymentTakeRequest) {

		if (paymentTakeRequest.getInstantPayment().equals("Yes")) {
			// here we find the orderDetails By orderId
			OrderDetailsDAOImpl orderDetailsDAOImpl = new OrderDetailsDAOImpl(sessionFactory);
			OrderDetails orderDetails = orderDetailsDAOImpl.fetchOrderDetailsByOrderId(paymentTakeRequest.getOrderId());

			// here we update the order status in OrderDetails as Delivered
			orderStatus = orderDetailsDAOImpl.fetchOrderStatus(Constants.ORDER_STATUS_DELIVERED);
			orderDetails.setOrderStatus(orderStatus);

			// in below four lines we are save the signature in delivered
			// product entity
			ImageConvertor imageConvertor = new ImageConvertor(sessionFactory);
			deliveredProduct
					.setOrderReceiverSignature(imageConvertor.convertStringToBlob(paymentTakeRequest.getSignature()));
			deliveredProduct.setOrderDetails(orderDetails);
			sessionFactory.getCurrentSession().save(deliveredProduct);

			// here we save the payment
			payment.setOrderDetails(orderDetails);
			payment.setTotalAmount(orderDetails.getTotalAmountWithTax());

			/*
			 * List<Payment>
			 * paymentList=fetchPaymentListByOrderDetailsId(orderDetails.
			 * getOrderId()); if(paymentList==null) {
			 * payment.setPaidAmount(paymentTakeRequest.getPaidAmount());
			 * payment.setDueAmount(orderDetails.getTotalAmountWithTax()-
			 * paymentTakeRequest.getPaidAmount()); } else { double
			 * paidAmount=paymentTakeRequest.getPaidAmount()+paymentList.get(0).
			 * getPaidAmount(); payment.setPaidAmount(paidAmount);
			 * payment.setDueAmount(orderDetails.getTotalAmountWithTax()-
			 * paidAmount); }
			 */

			Calendar cal = Calendar.getInstance();

			if (paymentTakeRequest.getCashCheckStatus().equals(Constants.CASH_PAY_STATUS)) {
				payment.setBankName(null);
				payment.setChequeDate(null);
				payment.setChequeNumber(null);
				payment.setPayType(Constants.CASH_PAY_STATUS);
			} else {
				payment.setBankName(paymentTakeRequest.getBankName());
				SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy/MM/dd");
				// Date date=new Date();
				try {
					Date date = simpleDateFormat.parse(paymentTakeRequest.getCheckDate());
					payment.setChequeDate(date);
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				payment.setChequeNumber(paymentTakeRequest.getCheckNo());
				payment.setPayType(Constants.CHEQUE_PAY_STATUS);
			}

			if (paymentTakeRequest.getFullPartialPayment().equals(Constants.FULL_PAY_STATUS)) {
				payment.setDueDate(new Date());
				orderDetails.setPayStatus(true);
				payment.setPaidAmount(paymentTakeRequest.getPaidAmount());
				orderDetails.setOrderDetailsPaymentTakeDatetime(new Date());
				payment.setDueAmount(0);
			} else {
				SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy/MM/dd");
				// Date date=new Date();
				try {
					Date date = simpleDateFormat.parse(paymentTakeRequest.getDueDate());
					payment.setDueDate(date);
					orderDetails.setOrderDetailsPaymentTakeDatetime(date);
				} catch (ParseException e) {
					e.printStackTrace();
				}

				orderDetails.setPayStatus(false);
				payment.setPaidAmount(paymentTakeRequest.getPaidAmount());
				payment.setDueAmount(orderDetails.getTotalAmountWithTax() - paymentTakeRequest.getPaidAmount());

			}
			payment.setReOrderDetails(null);
			payment.setPaidDate(new Date());
			sessionFactory.getCurrentSession().save(payment);

			// SACHIN SHARMA
		} else if (paymentTakeRequest.getInstantPayment().equals("No")) {

			OrderDetailsDAOImpl orderDetailsDAOImpl = new OrderDetailsDAOImpl(sessionFactory);
			OrderDetails orderDetails = orderDetailsDAOImpl.fetchOrderDetailsByOrderId(paymentTakeRequest.getOrderId());

			payment.setOrderDetails(orderDetails);

			String hql = "from Payment where orderDetails.orderId='" + orderDetails.getOrderId() + "'";
			Query query = sessionFactory.getCurrentSession().createQuery(hql);
			List<Payment> paymentList = (List<Payment>) query.list();

			// in below four lines we are save the signature in deliveredProduct
			// entity
			ImageConvertor imageConvertor = new ImageConvertor(sessionFactory);
			deliveredProduct
					.setOrderReceiverSignature(imageConvertor.convertStringToBlob(paymentTakeRequest.getSignature()));
			deliveredProduct.setOrderDetails(orderDetails);
			sessionFactory.getCurrentSession().save(deliveredProduct);

			if (paymentTakeRequest.getCashCheckStatus().equals(Constants.CASH_PAY_STATUS)) {
				payment.setBankName(null);
				payment.setChequeDate(null);
				payment.setChequeNumber(null);
				payment.setPayType(Constants.CASH_PAY_STATUS);
			} else {
				payment.setBankName(paymentTakeRequest.getBankName());
				SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy/MM/dd");
				try {
					Date date = simpleDateFormat.parse(paymentTakeRequest.getCheckDate());
					payment.setChequeDate(date);
				} catch (ParseException e) {
					e.printStackTrace();
				}
				payment.setChequeNumber(paymentTakeRequest.getCheckNo());
				payment.setPayType(Constants.CHEQUE_PAY_STATUS);
			}

			if (paymentTakeRequest.getFullPartialPayment().equals(Constants.FULL_PAY_STATUS)) {
				payment.setDueDate(new Date());
				orderDetails.setPayStatus(true);
				payment.setPaidAmount(paymentTakeRequest.getPaidAmount());
				payment.setDueAmount(0);
			} else {
				SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy/MM/dd");
				// Date date=new Date();
				try {
					Date date = simpleDateFormat.parse(paymentTakeRequest.getDueDate());
					payment.setDueDate(date);
					orderDetails.setOrderDetailsPaymentTakeDatetime(date);
					orderDetails.setOrderDetailsPaymentTakeDatetime(date);
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				orderDetails.setPayStatus(false);
				payment.setPaidAmount(paymentTakeRequest.getPaidAmount());
				payment.setDueAmount(
						paymentList.get(paymentList.size() - 1).getDueAmount() - paymentTakeRequest.getPaidAmount());

			}
			payment.setTotalAmount(orderDetails.getTotalAmountWithTax());

			payment.setReOrderDetails(null);
			payment.setPaidDate(new Date());
			sessionFactory.getCurrentSession().save(payment);

		}

	}

	@Transactional
	public void savePaymentStatusOfReOrder(ReOrderPaymentTakeRequest reOrderPaymentTakeRequest) {

		if (reOrderPaymentTakeRequest.getInstantPayment().equals("Yes")) {
			ReOrderDetails reOrderDetails = new ReOrderDetails();

			// in below two lines we are generating the reorderId
			ReOrderIdGenerator reOrderIdGenerator = new ReOrderIdGenerator(sessionFactory);
			reOrderDetails.setReOrderDetailsId(reOrderIdGenerator.generate());

			// in below lines we are going to set the values Of Reorder Entity
			reOrderDetails.setOrderDetails(reOrderPaymentTakeRequest.getOrderDetails());
			reOrderDetails.setReOrderAddedDatetime(new Date());
			reOrderDetails.setReOrderToBooth(reOrderPaymentTakeRequest.getBooth());
			reOrderDetails.setReOrderAmt(reOrderPaymentTakeRequest.getOrderDetails().getReOrderAmt());
			reOrderDetails.setReOrderAmtWithTax(reOrderPaymentTakeRequest.getOrderDetails().getReOrderAmtWithTax());
			reOrderDetails.setReOrderQty(reOrderPaymentTakeRequest.getOrderDetails().getReOrderQty());

			sessionFactory.getCurrentSession().save(reOrderDetails); // here we
																		// are
																		// saving
																		// the
																		// reOrderDetails

			OrderDetailsDAOImpl orderDetailsDAOImpl = new OrderDetailsDAOImpl(sessionFactory);
			OrderDetails orderDetails = orderDetailsDAOImpl
					.fetchOrderDetailsByOrderId(reOrderPaymentTakeRequest.getOrderDetails().getOrderId());

			orderDetails.setReOrderAmt(orderDetails.getReOrderAmt() - reOrderDetails.getReOrderAmt());
			orderDetails
					.setReOrderAmtWithTax(orderDetails.getReOrderAmtWithTax() - reOrderDetails.getReOrderAmtWithTax());
			orderDetails.setReOrderQty(orderDetails.getReOrderQty() - reOrderDetails.getReOrderQty());
			if (orderDetails.getReOrderQty() == 0) {
				orderDetails.setReOrderStatus(Constants.REORDER_STATUS_COMPLETE);
			}

			for (OrderProductDetails orderProductDetails : reOrderPaymentTakeRequest.getOrderProductDetails()) {

				ReOrderProductDetails reOrderProductDetails = new ReOrderProductDetails();

				// in below lines we are going to set the values Of
				// ReorderProductDetails Entity
				reOrderProductDetails.setProduct(orderProductDetails.getProduct());
				reOrderProductDetails.setReOrderPurchaseAmt(orderProductDetails.getPurchaseAmount());
				reOrderProductDetails.setReOrderQty(orderProductDetails.getPurchaseQuantity());
				reOrderProductDetails.setReOrderDetails(reOrderDetails);
				reOrderProductDetails.setSellingRate(orderProductDetails.getSellingRate());

				sessionFactory.getCurrentSession().save(reOrderProductDetails);// here
																				// we
																				// are
																				// saving
																				// the
																				// ReOrderProduct
																				// details
			}

			

			// in below four lines we are save the signature in delivered
			// product entity
			ImageConvertor imageConvertor = new ImageConvertor(sessionFactory);
			deliveredProduct.setOrderReceiverSignature(
					imageConvertor.convertStringToBlob(reOrderPaymentTakeRequest.getSignature()));
			deliveredProduct.setReOrderDetails(reOrderDetails);
			sessionFactory.getCurrentSession().save(deliveredProduct);

			// here we save the payment
			payment.setReOrderDetails(reOrderDetails);
			payment.setTotalAmount(reOrderDetails.getReOrderAmtWithTax());

		

			Calendar cal = Calendar.getInstance();

			if (reOrderPaymentTakeRequest.getCashCheckStatus().equals(Constants.CASH_PAY_STATUS)) {
				payment.setBankName(null);
				payment.setChequeDate(null);
				payment.setChequeNumber(null);
				payment.setPayType(Constants.CASH_PAY_STATUS);
			} else {
				payment.setBankName(reOrderPaymentTakeRequest.getBankName());
				SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy/MM/dd");
				// Date date=new Date();
				try {
					Date date = simpleDateFormat.parse(reOrderPaymentTakeRequest.getCheckDate());
					payment.setChequeDate(date);
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				payment.setChequeNumber(reOrderPaymentTakeRequest.getCheckNo());
				payment.setPayType(Constants.CHEQUE_PAY_STATUS);
			}

			if (reOrderPaymentTakeRequest.getFullPartialPayment().equals(Constants.FULL_PAY_STATUS)) {
				payment.setDueDate(new Date());
				reOrderDetails.setPayStatus(true);
				payment.setPaidAmount(reOrderPaymentTakeRequest.getPaidAmount());
				reOrderDetails.setReOrderDetailsPaymentTakeDatetime(new Date());
				payment.setDueAmount(0);
			} else {
				SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy/MM/dd");
				// Date date=new Date();
				try {
					Date date = simpleDateFormat.parse(reOrderPaymentTakeRequest.getDueDate());
					payment.setDueDate(date);
					reOrderDetails.setReOrderDetailsPaymentTakeDatetime(date);

				} catch (ParseException e) {
					e.printStackTrace();
				}

				reOrderDetails.setPayStatus(false);
				payment.setPaidAmount(reOrderPaymentTakeRequest.getPaidAmount());
				payment.setDueAmount(reOrderDetails.getReOrderAmtWithTax() - reOrderPaymentTakeRequest.getPaidAmount());

			}

			payment.setPaidDate(new Date());
			payment.setOrderDetails(null);
			sessionFactory.getCurrentSession().save(payment);

			// SACHIN SHARMA
		} else if (reOrderPaymentTakeRequest.getInstantPayment().equals("No")) {

			OrderDetailsDAOImpl orderDetailsDAOImpl = new OrderDetailsDAOImpl(sessionFactory);
			ReOrderDetails reOrderDetails = orderDetailsDAOImpl
					.fetchReOrderDetailsByReOrderId(reOrderPaymentTakeRequest.getReOrderId());

			payment.setReOrderDetails(reOrderDetails);

			String hql = "from Payment where reOrderDetails.reOrderDetailsId='" + reOrderDetails.getReOrderDetailsId()
					+ "'";
			Query query = sessionFactory.getCurrentSession().createQuery(hql);
			List<Payment> paymentList = (List<Payment>) query.list();

			// in below four lines we are save the signature in deliveredProduct
			// entity
			ImageConvertor imageConvertor = new ImageConvertor(sessionFactory);
			deliveredProduct.setOrderReceiverSignature(
					imageConvertor.convertStringToBlob(reOrderPaymentTakeRequest.getSignature()));
			deliveredProduct.setReOrderDetails(reOrderDetails);
			sessionFactory.getCurrentSession().save(deliveredProduct);

			if (reOrderPaymentTakeRequest.getCashCheckStatus().equals(Constants.CASH_PAY_STATUS)) {
				payment.setBankName(null);
				payment.setChequeDate(null);
				payment.setChequeNumber(null);
				payment.setPayType(Constants.CASH_PAY_STATUS);
			} else {
				payment.setBankName(reOrderPaymentTakeRequest.getBankName());
				SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy/MM/dd");
				try {
					Date date = simpleDateFormat.parse(reOrderPaymentTakeRequest.getCheckDate());
					payment.setChequeDate(date);
				} catch (ParseException e) {
					e.printStackTrace();
				}
				payment.setChequeNumber(reOrderPaymentTakeRequest.getCheckNo());
				payment.setPayType(Constants.CHEQUE_PAY_STATUS);
			}

			if (reOrderPaymentTakeRequest.getFullPartialPayment().equals(Constants.FULL_PAY_STATUS)) {
				payment.setDueDate(new Date());
				reOrderDetails.setPayStatus(true);
				payment.setPaidAmount(reOrderPaymentTakeRequest.getPaidAmount());
				payment.setDueAmount(0);
			} else {
				SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy/MM/dd");
				// Date date=new Date();
				try {
					Date date = simpleDateFormat.parse(reOrderPaymentTakeRequest.getDueDate());
					payment.setDueDate(date);
					reOrderDetails.setReOrderDetailsPaymentTakeDatetime(date);
					// reOrderDetails.setOrderDetailsPaymentTakeDatetime(date);
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				reOrderDetails.setPayStatus(false);
				payment.setPaidAmount(reOrderPaymentTakeRequest.getPaidAmount());
				payment.setDueAmount(paymentList.get(paymentList.size() - 1).getDueAmount()
						- reOrderPaymentTakeRequest.getPaidAmount());

			}

			payment.setTotalAmount(reOrderDetails.getReOrderAmtWithTax());
			payment.setPaidDate(new Date());
			sessionFactory.getCurrentSession().save(payment);

		}

	}

	@Transactional
	public List<Payment> fetchOrderDetaisForPaymentByEmployeeId(long employeeId, String paymentStatus) {
		// TODO Auto-generated method stub
		String hql = "";
		Query query;
		Date dateWithoutTime = new Date();

		Calendar cal = Calendar.getInstance();
		cal.set(Calendar.HOUR_OF_DAY, 0);
		cal.set(Calendar.MINUTE, 0);
		cal.set(Calendar.SECOND, 0);
		cal.set(Calendar.MILLISECOND, 0);
		dateWithoutTime = cal.getTime();
		System.out.println("date'" + dateWithoutTime + "'");

		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
		String strDate = simpleDateFormat.format(dateWithoutTime);

		if (paymentStatus.equals(Constants.PAY_STATUS_CURRENT_DATE_PAYMENT)) {

			hql = "from Payment where orderDetails.employeeASMandSM.employeeId=" + employeeId + " And date(dueDate)='"
					+ strDate + "' And orderDetails.orderStatus.status='" + Constants.ORDER_STATUS_DELIVERED
					+ "' And orderDetails.payStatus= false ";
		} else {
			hql = "from Payment where orderDetails.employeeASMandSM.employeeId=" + employeeId + " And date(dueDate)<'"
					+ strDate + "' And orderDetails.orderStatus.status='" + Constants.ORDER_STATUS_DELIVERED
					+ "' And orderDetails.payStatus= false";
		}

		query = sessionFactory.getCurrentSession().createQuery(hql);

		List<Payment> paymentList = (List<Payment>) query.list();

		if (paymentList.isEmpty()) {
			return null;
		}

		return paymentList;
	}

	/*
	 * @Transactional public Payment fetchPaymentByOrderId(String orderId){
	 * 
	 * String hql="from Payment where orderDetails.orderId='"+orderId+"'"; Query
	 * query=sessionFactory.getCurrentSession().createQuery(hql); List<Payment>
	 * 
	 * }
	 */

	@Transactional
	public List<OrderDetails> fetchOrderDetailsListForPayment(long employeeId, String paymentStatus) {

		String hql;
		if (paymentStatus.equals(Constants.PAY_STATUS_CURRENT_DATE_PAYMENT)) {
			hql = "from OrderDetails where payStatus = false and date(orderDetailsPaymentTakeDatetime) = date(CURRENT_DATE()) and employeeASMandSM.employeeId="
					+ employeeId + " and orderStatus.status ='" + Constants.ORDER_STATUS_DELIVERED + "'";
		} else {
			hql = "from OrderDetails where payStatus = false and date(orderDetailsPaymentTakeDatetime) < date(CURRENT_DATE()) and employeeASMandSM.employeeId="
					+ employeeId + " and orderStatus.status ='" + Constants.ORDER_STATUS_DELIVERED + "'";
		}

		Query query = sessionFactory.getCurrentSession().createQuery(hql);

		List<OrderDetails> orderDetailsList = (List<OrderDetails>) query.list();

		return orderDetailsList;

	}

	@Transactional
	public List<ReOrderDetails> fetchReOrderDetailsListForPayment(long employeeId, String paymentStatus) {

		String hql = "";
		if (paymentStatus.equals(Constants.PAY_STATUS_CURRENT_DATE_PAYMENT)) {
			hql = "from ReOrderDetails where payStatus = false and date(reOrderDetailsPaymentTakeDatetime) = date(CURRENT_DATE()) and orderDetails.employeeASMandSM.employeeId="
					+ employeeId + " and orderDetails.orderStatus.status ='" + Constants.ORDER_STATUS_CANCELED + "'";
		} else {
			hql = "from ReOrderDetails where payStatus = false and date(reOrderDetailsPaymentTakeDatetime) < date(CURRENT_DATE()) and orderDetails.employeeASMandSM.employeeId="
					+ employeeId + " and orderDetails.orderStatus.status ='" + Constants.ORDER_STATUS_CANCELED + "'";
		}

		Query query = sessionFactory.getCurrentSession().createQuery(hql);
		List<ReOrderDetails> reOrderDetails = (List<ReOrderDetails>) query.list();
		return reOrderDetails;
	}

	
	// here we are fetching OrderDetails for payment list 1st screen
	@Transactional
	public List<OrderDetailsForPayment> fetchOrderListForPayment(long employeeId, String paymentStatus) {

		List<OrderDetailsForPayment> orderDetailsForPaymentsList = new ArrayList<>();
		List<OrderDetails> orderDetailsList = fetchOrderDetailsListForPayment(employeeId, paymentStatus);

		List<ReOrderDetails> reOrderDetails = fetchReOrderDetailsListForPayment(employeeId, paymentStatus);

		String hql;

		if (reOrderDetails.isEmpty() && orderDetailsList.isEmpty()) {
			return null;
		} else if (orderDetailsList.isEmpty() && !reOrderDetails.isEmpty()) {
			for (ReOrderDetails reOrderDetails2 : reOrderDetails) {
				List<Payment> paymentsList1 = fetchPaymentListByReOrderDetailsId(reOrderDetails2.getReOrderDetailsId());
				double amountDue = 0;
				if (paymentsList1 == null) {
					amountDue = reOrderDetails2.getReOrderAmtWithTax();

				} else {
					amountDue = paymentsList1.get(0).getDueAmount();
				}
				orderDetailsForPaymentsList
						.add(new OrderDetailsForPayment(reOrderDetails2.getReOrderToBooth().getBoothNo(),
								reOrderDetails2.getReOrderToBooth().getOwnerName(), amountDue,
								reOrderDetails2.getReOrderDetailsPaymentTakeDatetime(), null,
								reOrderDetails2.getReOrderDetailsId(), "ReOrder"));
			}

		}

		else if (!orderDetailsList.isEmpty() && reOrderDetails.isEmpty()) {
			for (OrderDetails orderDetails : orderDetailsList) {
				List<Payment> paymentList = fetchPaymentListByOrderDetailsId(orderDetails.getOrderId());
				double amountDue = 0;

				if (paymentList == null) {
					amountDue = orderDetails.getTotalAmountWithTax();
				} else {
					amountDue = paymentList.get(0).getDueAmount();
				}

				orderDetailsForPaymentsList.add(new OrderDetailsForPayment(orderDetails.getBooth().getBoothNo(),
						orderDetails.getBooth().getOwnerName(), amountDue,
						orderDetails.getOrderDetailsPaymentTakeDatetime(), orderDetails.getOrderId(), null, "Fresh"));
			}
		} else {

			for (ReOrderDetails reOrderDetails2 : reOrderDetails) {
				List<Payment> paymentsList1 = fetchPaymentListByReOrderDetailsId(reOrderDetails2.getReOrderDetailsId());
				double amountDue = 0;
				if (paymentsList1 == null) {
					amountDue = reOrderDetails2.getReOrderAmtWithTax();

				} else {
					amountDue = paymentsList1.get(0).getDueAmount();
				}
				orderDetailsForPaymentsList
						.add(new OrderDetailsForPayment(reOrderDetails2.getReOrderToBooth().getBoothNo(),
								reOrderDetails2.getReOrderToBooth().getOwnerName(), amountDue,
								reOrderDetails2.getReOrderDetailsPaymentTakeDatetime(), null,
								reOrderDetails2.getReOrderDetailsId(), "ReOrder"));
			}

			for (OrderDetails orderDetails : orderDetailsList) {
				List<Payment> paymentList = fetchPaymentListByOrderDetailsId(orderDetails.getOrderId());
				double amountDue = 0;

				if (paymentList == null) {
					amountDue = orderDetails.getTotalAmountWithTax();
				} else {
					amountDue = paymentList.get(0).getDueAmount();
				}

				
				orderDetailsForPaymentsList.add(new OrderDetailsForPayment(orderDetails.getBooth().getBoothNo(),
						orderDetails.getBooth().getOwnerName(), amountDue,
						orderDetails.getOrderDetailsPaymentTakeDatetime(), orderDetails.getOrderId(), null, "Fresh"));
			}

		}

		return orderDetailsForPaymentsList;
	}

	@Transactional
	public List<Payment> fetchPaymentListByOrderDetailsId(String orderDetailsId) {

		Query query = sessionFactory.getCurrentSession()
				.createQuery("from Payment where orderDetails.orderId='" + orderDetailsId + "' order by paidDate desc");
		List<Payment> paymentList = (List<Payment>) query.list();
		if (paymentList.isEmpty()) {
			return null;
		}

		return paymentList;
	}

	@Transactional
	public List<Payment> fetchPaymentListByReOrderDetailsId(String reOrderDetailsId) {

		Query query = sessionFactory.getCurrentSession().createQuery(
				"from Payment where reOrderDetails.reOrderDetailsId='" + reOrderDetailsId + "' order by paidDate desc");
		List<Payment> paymentList = (List<Payment>) query.list();
		if (paymentList.isEmpty()) {
			return null;
		}

		return paymentList;
	}
	
	

	/* (non-Javadoc)
	 * @see com.sheetalarch.dao.PaymentDAO#balanceAmountOfBooth(long)
	 * Dec 5, 20175:04:18 PM
	 */
	@Override
	public double balanceAmountOfBooth(long boothId) {
		// TODO Auto-generated method stub
		
		
		OrderDetailsDAOImpl orderDetailsDAOImpl=new OrderDetailsDAOImpl(sessionFactory);
		List<OrderDetails> orderDetailsList=orderDetailsDAOImpl.fetchOrderDetailsListForWebApp(boothId);
		List<ReOrderDetails> reOrderDetailsList=orderDetailsDAOImpl.fetchReOrderDetailsListForWebApp(boothId);
		
		String hql="";
		Query query;
		double balanceAmount=0;
		
		if(orderDetailsList!=null)
		{
			for(OrderDetails orderDetails: orderDetailsList)
			{
				List<Payment> paymentList=fetchPaymentListByOrderDetailsId(orderDetails.getOrderId());
				if(paymentList.isEmpty())
				{
					balanceAmount+=orderDetails.getTotalAmountWithTax();
				}
				else
				{
					balanceAmount+=paymentList.get(0).getDueAmount();
				}
			}
		}
		
		if(reOrderDetailsList!=null)
		{
			for(ReOrderDetails reOrderDetails: reOrderDetailsList)
			{
				List<Payment> paymentList=fetchPaymentListByReOrderDetailsId(reOrderDetails.getReOrderDetailsId());
				if(paymentList.isEmpty())
				{
					balanceAmount+=reOrderDetails.getReOrderAmtWithTax();
				}
				else
				{
					balanceAmount+=paymentList.get(0).getDueAmount();
				}
			}
		}
		
		
		return balanceAmount;
	}
	
	@Transactional
	public CollectionReportMain getCollectionReportDetails(String startDate, String endDate,String range)
	{
		String hql = "",reOrderHql="";
		Query query,reOrderQuery;
		Calendar cal = Calendar.getInstance();
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		List<CollectionReportPaymentDetails> collectionReportPaymentDetailList=new ArrayList<>();
		double amountToBePaid=0;
		double pendingAmount=0;
		double fullAmountCollected=0;
		double partialAmountCollected=0;
		
		if (range.equals("Last6Months")) {
			cal.add(Calendar.MONTH, -6);
			hql="from OrderDetails where date(orderDetailsPaymentTakeDatetime)>='"+dateFormat.format(cal.getTime())+"' and orderStatus.status in ('"+Constants.ORDER_STATUS_DELIVERED+"')";
			reOrderHql="from ReOrderDetails where date(reOrderDetailsPaymentTakeDatetime)>='"+dateFormat.format(cal.getTime())+"'";
		}
		else if (range.equals("Last7days")) {
			cal.add(Calendar.DAY_OF_MONTH, -7);
			hql="from OrderDetails where date(orderDetailsPaymentTakeDatetime)>='"+dateFormat.format(cal.getTime())+"' and orderStatus.status in ('"+Constants.ORDER_STATUS_DELIVERED+"')";
			reOrderHql="from ReOrderDetails where date(reOrderDetailsPaymentTakeDatetime)>='"+dateFormat.format(cal.getTime())+"'";
		}
		else if (range.equals("Last1Year")) {
			cal.add(Calendar.MONTH, -12);
			hql="from OrderDetails where date(orderDetailsPaymentTakeDatetime)>='"+dateFormat.format(cal.getTime())+"' and orderStatus.status in ('"+Constants.ORDER_STATUS_DELIVERED+"')";			
			reOrderHql="from ReOrderDetails where date(reOrderDetailsPaymentTakeDatetime)>='"+dateFormat.format(cal.getTime())+"'";
		}
		else if (range.equals("ViewAll")) {
			hql="from OrderDetails where orderStatus.status in ('"+Constants.ORDER_STATUS_DELIVERED+"')";	
			reOrderHql="from ReOrderDetails";
		}
		else if (range.equals("CurrentMonth")) {
			hql="from OrderDetails where MONTH(orderDetailsPaymentTakeDatetime)=MONTH(CURRENT_DATE) and orderStatus.status in ('"+Constants.ORDER_STATUS_DELIVERED+"')";
			reOrderHql="from ReOrderDetails where MONTH(reOrderDetailsPaymentTakeDatetime)>=MONTH(CURRENT_DATE)";
		}
		else if (range.equals("Range")) {
			hql="from OrderDetails where date(orderDetailsPaymentTakeDatetime)>='"+startDate+"' and date(orderDetailsPaymentTakeDatetime)<='"+endDate+"' and orderStatus.status in ('"+Constants.ORDER_STATUS_DELIVERED+"')";			
			reOrderHql="from ReOrderDetails where date(reOrderDetailsPaymentTakeDatetime)>='"+startDate+"' and date(reOrderDetailsPaymentTakeDatetime)<='"+endDate+"'";
		}
		else if (range.equals("CurrentDate")) {
			hql="from OrderDetails where date(orderDetailsPaymentTakeDatetime)=date(CURRENT_DATE) and orderStatus.status in ('"+Constants.ORDER_STATUS_DELIVERED+"')";
			reOrderHql="from ReOrderDetails where date(reOrderDetailsPaymentTakeDatetime)>=date(CURRENT_DATE)";
		}
		
		query=sessionFactory.getCurrentSession().createQuery(hql);
		List<OrderDetails> orderDetailsList=(List<OrderDetails>)query.list();
		
		reOrderQuery=sessionFactory.getCurrentSession().createQuery(reOrderHql);
		List<ReOrderDetails> reOrderDetailsList=(List<ReOrderDetails>)reOrderQuery.list();
		
		if(orderDetailsList.isEmpty() && reOrderDetailsList.isEmpty())
		{
			return null;
		}
		for(OrderDetails orderDetails:orderDetailsList)
		{
			long srno=1;
			
			List<Payment> paymentList=fetchPaymentListByOrderDetailsId(orderDetails.getOrderId());
				
				if(paymentList==null)
				{
					collectionReportPaymentDetailList.add(new CollectionReportPaymentDetails(srno, 
																							orderDetails.getBooth().getBoothNo(), 
																							orderDetails.getBooth().getOwnerName(), 
																							orderDetails.getOrderId(), 
																							orderDetails.getBooth().getContact().getMobileNumber(), 
																							orderDetails.getBooth().getRoute().getRouteNo(), 
																							orderDetails.getBooth().getRoute().getArea().getName(), 
																							orderDetails.getBooth().getRoute().getArea().getCluster().getName(), 
																							orderDetails.getTotalAmountWithTax(),
																							0, 
																							orderDetails.getTotalAmountWithTax(), 
																							orderDetails.getOrderDetailsPaymentTakeDatetime(), 
																							null, 
																							"--",
																							"UnPaid")); 
					pendingAmount+=orderDetails.getTotalAmountWithTax();
					
				}
				else
				{
				  for(Payment payment:paymentList)
				  {
					double amountDue=payment.getDueAmount();
					double amountPaid=orderDetails.getTotalAmountWithTax()-amountDue;
					String payMode="";
					
					if(payment.getChequeDate()==null)
					{
						payMode="Cash";
					}
					else
					{
						payMode=payment.getChequeNumber();
					}
					
					if(amountDue==0)
					{
						collectionReportPaymentDetailList.add(new CollectionReportPaymentDetails(srno, 
																							orderDetails.getBooth().getBoothNo(), 
																							orderDetails.getBooth().getOwnerName(), 
																							orderDetails.getOrderId(), 
																							orderDetails.getBooth().getContact().getMobileNumber(), 
																							orderDetails.getBooth().getRoute().getRouteNo(), 
																							orderDetails.getBooth().getRoute().getArea().getName(), 
																							orderDetails.getBooth().getRoute().getArea().getCluster().getName(), 
																							orderDetails.getTotalAmountWithTax(),
																							amountDue, 
																							amountPaid,
																							null,
																							payment.getPaidDate(),
																							payMode,
																							"Full")); 
						fullAmountCollected+=amountPaid;
					}
					else
					{
						collectionReportPaymentDetailList.add(new CollectionReportPaymentDetails(srno, 
																							orderDetails.getBooth().getBoothNo(), 
																							orderDetails.getBooth().getOwnerName(), 
																							orderDetails.getOrderId(), 
																							orderDetails.getBooth().getContact().getMobileNumber(), 
																							orderDetails.getBooth().getRoute().getRouteNo(), 
																							orderDetails.getBooth().getRoute().getArea().getName(), 
																							orderDetails.getBooth().getRoute().getArea().getCluster().getName(), 
																							orderDetails.getTotalAmountWithTax(),
																							amountDue, 
																							amountPaid,
																							payment.getDueDate(),
																							payment.getPaidDate(),
																							payMode,
																							"Partial"));
						partialAmountCollected=+amountDue;
						pendingAmount+=amountDue;
					}
				}
			  }
				amountToBePaid+=orderDetails.getTotalAmountWithTax();
				srno++;
			}
		
		
		for(ReOrderDetails reOrderDetails : reOrderDetailsList)
		{
			long srno=1;
			
			List<Payment> paymentList=fetchPaymentListByReOrderDetailsId(reOrderDetails.getReOrderDetailsId());
				
				if(paymentList==null)
				{
					collectionReportPaymentDetailList.add(new CollectionReportPaymentDetails(srno, 
																							reOrderDetails.getReOrderToBooth().getBoothNo(), 
																							reOrderDetails.getReOrderToBooth().getOwnerName(), 
																							reOrderDetails.getReOrderDetailsId(), 
																							reOrderDetails.getReOrderToBooth().getContact().getMobileNumber(), 
																							reOrderDetails.getReOrderToBooth().getRoute().getRouteNo(), 
																							reOrderDetails.getReOrderToBooth().getRoute().getArea().getName(), 
																							reOrderDetails.getReOrderToBooth().getRoute().getArea().getCluster().getName(), 
																							reOrderDetails.getReOrderAmtWithTax(),
																							0, 
																							reOrderDetails.getReOrderAmtWithTax(), 
																							reOrderDetails.getReOrderDetailsPaymentTakeDatetime(), 
																							null, 
																							"--",
																							"UnPaid")); 
					pendingAmount+=reOrderDetails.getReOrderAmtWithTax();
					
				}
				else
				{
				  for(Payment payment:paymentList)
				  {
					double amountDue=payment.getDueAmount();
					double amountPaid=reOrderDetails.getReOrderAmtWithTax()-amountDue;
					String payMode="";
					
					if(payment.getChequeDate()==null)
					{
						payMode="Cash";
					}
					else
					{
						payMode=payment.getChequeNumber();
					}
					
					if(amountDue==0)
					{
						collectionReportPaymentDetailList.add(new CollectionReportPaymentDetails(srno, 
																							reOrderDetails.getReOrderToBooth().getBoothNo(), 
																							reOrderDetails.getReOrderToBooth().getOwnerName(), 
																							reOrderDetails.getReOrderDetailsId(), 
																							reOrderDetails.getReOrderToBooth().getContact().getMobileNumber(), 
																							reOrderDetails.getReOrderToBooth().getRoute().getRouteNo(), 
																							reOrderDetails.getReOrderToBooth().getRoute().getArea().getName(), 
																							reOrderDetails.getReOrderToBooth().getRoute().getArea().getCluster().getName(), 
																							reOrderDetails.getReOrderAmtWithTax(),
																							amountDue, 
																							amountPaid,
																							null,
																							payment.getPaidDate(),
																							payMode,
																							"Full")); 
						fullAmountCollected+=amountPaid;
					}
					else
					{
						collectionReportPaymentDetailList.add(new CollectionReportPaymentDetails(srno, 
																							reOrderDetails.getReOrderToBooth().getBoothNo(), 
																							reOrderDetails.getReOrderToBooth().getOwnerName(), 
																							reOrderDetails.getReOrderDetailsId(), 
																							reOrderDetails.getReOrderToBooth().getContact().getMobileNumber(), 
																							reOrderDetails.getReOrderToBooth().getRoute().getRouteNo(), 
																							reOrderDetails.getReOrderToBooth().getRoute().getArea().getName(), 
																							reOrderDetails.getReOrderToBooth().getRoute().getArea().getCluster().getName(), 
																							reOrderDetails.getReOrderAmtWithTax(),
																							amountDue, 
																							amountPaid,
																							payment.getDueDate(),
																							payment.getPaidDate(),
																							payMode,
																							"Partial"));
						partialAmountCollected=+amountDue;
						pendingAmount+=amountDue;
					}
				}
			  }
				amountToBePaid+=reOrderDetails.getReOrderAmtWithTax();
				srno++;
			}
		
		return new CollectionReportMain(amountToBePaid, pendingAmount, fullAmountCollected, partialAmountCollected, collectionReportPaymentDetailList);
	}
}
