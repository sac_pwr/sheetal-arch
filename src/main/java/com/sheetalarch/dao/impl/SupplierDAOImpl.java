package com.sheetalarch.dao.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.sheetalarch.admin.models.FetchSupplierList;
import com.sheetalarch.dao.SupplierDAO;
import com.sheetalarch.entity.Product;
import com.sheetalarch.entity.Supplier;
import com.sheetalarch.entity.SupplierProductList;
import com.sheetalarch.utils.SendSMS;
import com.sheetalarch.utils.SupplierIdGenerator;


@Repository("SupplierDAO")
@Component
public class SupplierDAOImpl implements SupplierDAO {

	@Autowired
	SessionFactory sessionFactory;
	
	@Autowired
	Supplier supplier;

	@Autowired
	SupplierProductList supplierProductList;
	
	@Autowired
	Product product;
	
	public SupplierDAOImpl(SessionFactory sessionFactory) {
		// TODO Auto-generated constructor stub
		this.sessionFactory = sessionFactory;

	}

	@Override
	public List<Supplier> fetchSupplierList() {
		// TODO Auto-generated method stub
		String hql="";
		Query query;
		hql="from Supplier";
		query=sessionFactory.getCurrentSession().createQuery(hql);
		List<Supplier> suppliersList=(List<Supplier>)query.list();
		if(suppliersList.isEmpty()){
			return null;
		}else
		{
			return suppliersList;
		}
		}
	
	@Transactional
	public List<SupplierProductList> fetchProductBySupplierId(String supplierId){
		
		

		String hql="from SupplierProductList where supplier.supplierId='"+supplierId+"'";
		   
		Query query=sessionFactory.getCurrentSession().createQuery(hql);
		
		System.out.println("testingdjhcbhvjxx");
		List<SupplierProductList> list= (List<SupplierProductList>)query.list();
		
		if(list.isEmpty()){
			return null;
		}
		return list;
	}
	
	@Transactional
	public List<Supplier> fetchSupplierForWebAppList() {

		Query query=sessionFactory.getCurrentSession().createQuery("from Supplier");	
		List<Supplier> list=(List<Supplier>)query.list();
		if(list.isEmpty())
		{
			return null;
		}
		return list;
	}

	@Transactional
	public Supplier fetchSupplier(String supplierId) {
		Query query=sessionFactory.getCurrentSession().createQuery("from Supplier where supplierId='"+supplierId+"'");	
		List<Supplier> list=(List<Supplier>)query.list();
		if(list.isEmpty())
		{
			return null;
		}
		return list.get(0);
	}

	@Transactional
	public Supplier saveSupplier(Supplier supplier) {

		sessionFactory.getCurrentSession().save(supplier.getContact());
		
		SupplierIdGenerator supplierIdGenerator=new SupplierIdGenerator(sessionFactory); 
		supplier.setSupplierId(supplierIdGenerator.generateSupplierId());
		sessionFactory.getCurrentSession().save(supplier);
		
		return null;
	}

	@Transactional
	public Supplier updateSupplier(Supplier supplier) {
		sessionFactory.getCurrentSession().update(supplier.getContact());
		sessionFactory.getCurrentSession().update(supplier);
		return null;
	}

	@Transactional
	public Supplier saveSupplierProductList(String productIdList,String supplierId) {
		// TODO Auto-generated method stub
		String[] prdutIdAndRateList = productIdList.split(",");
		
		for(int i=0; i<prdutIdAndRateList.length; i++)
		{
			SupplierProductList supplierProductList=new SupplierProductList();
					
			String[] prdutIdAndRate=prdutIdAndRateList[i].split("-");
			product.setProductId(Long.parseLong(prdutIdAndRate[0]));
			supplierProductList.setProduct(product);
			supplierProductList.setSupplierRate(Double.parseDouble(prdutIdAndRate[1]));
			
			supplier.setSupplierId(supplierId);
			supplierProductList.setSupplier(supplier);
			
			sessionFactory.getCurrentSession().save(supplierProductList);
		}
		
		return null;
	}
	
	@Transactional
	public Supplier updateSupplierProductList(String productIdList,String supplierId) {
		
		String hql="from SupplierProductList where supplier.supplierId='"+supplierId+"'";
		Query query=sessionFactory.getCurrentSession().createQuery(hql);	
		
		List<SupplierProductList> list=query.list();
		Iterator<SupplierProductList> itr=list.iterator();
		while(itr.hasNext())
		{
			sessionFactory.getCurrentSession().delete(itr.next());
		}		
		// TODO Auto-generated method stub
		String[] prdutIdAndRateList = productIdList.split(",");
		
		for(int i=0; i<prdutIdAndRateList.length; i++)
		{
			SupplierProductList supplierProductList=new SupplierProductList();
					
			String[] prdutIdAndRate=prdutIdAndRateList[i].split("-");
			product.setProductId(Long.parseLong(prdutIdAndRate[0]));
			supplierProductList.setProduct(product);
			supplierProductList.setSupplierRate(Double.parseDouble(prdutIdAndRate[1]));
			
			supplier.setSupplierId(supplierId);
			supplierProductList.setSupplier(supplier);
			
			sessionFactory.getCurrentSession().save(supplierProductList);
		}
		
		return null;
	}

	@Transactional
	public List<FetchSupplierList> fetchSupplierForWebApp() {
		Query query=sessionFactory.getCurrentSession().createQuery("from Supplier");	
		List<Supplier> list=(List<Supplier>)query.list();
		if(list.isEmpty())
		{
			return null;
		}
		
		InventoryDAOImpl inventoryDAOImpl=new InventoryDAOImpl(sessionFactory);
		Iterator<Supplier> itr=list.iterator();
		List<FetchSupplierList> fetchSupplierList=new ArrayList<FetchSupplierList>();
		long srNo=0;
		while(itr.hasNext())
		{
			this.supplier=itr.next();
			srNo++;
			fetchSupplierList.add(new FetchSupplierList(this.supplier.getSupplierId(), 
														srNo, 
														this.supplier.getName(), 
														this.supplier.getContact().getEmailId(), 
														this.supplier.getContact().getMobileNumber(), 
														this.supplier.getAddress(), 
														this.supplier.getGstinNo(),
														inventoryDAOImpl.fetchBalanceAmtBySupplierId(this.supplier.getSupplierId()),
														this.supplier.getSupplierAddedDatetime(),
														this.supplier.getSupplierUpdatedDatetime()));
			
		}
		
		
		return fetchSupplierList;
	}

	@Transactional
	public List<SupplierProductList> fetchSupplierProductListBySupplierIdForWebApp(String supplierId) {
		Query query=sessionFactory.getCurrentSession().createQuery("from SupplierProductList where supplier.supplierId='"+supplierId+"'");	
		List<SupplierProductList> list=(List<SupplierProductList>)query.list();
		if(list.isEmpty())
		{
			return null;
		}
		return list;
	}

	@Transactional
	public List<SupplierProductList> makeSupplierProductListNullForWebApp(
			List<SupplierProductList> supplierProductList) {
		
			List<SupplierProductList> sPList=new ArrayList<>();
			
			Iterator<SupplierProductList> itr=supplierProductList.iterator();
			while(itr.hasNext())
			{
				SupplierProductList supplierPrdctList=itr.next();
				supplierPrdctList.getProduct().setProductImage(null);
				sPList.add(supplierPrdctList);
			}			

		return sPList;
	}

	@Transactional
	public String getProductIdList(List<SupplierProductList> supplierProductList) {
		List<SupplierProductList> sPList=new ArrayList<>();
		
		Iterator<SupplierProductList> itr=supplierProductList.iterator();
		String productIds="";
		while(itr.hasNext())
		{
			SupplierProductList supplierPrdctList=itr.next();
			productIds=productIds+supplierPrdctList.getProduct().getProductId()+"-"+supplierPrdctList.getSupplierRate()+",";
			sPList.add(supplierPrdctList);
		}			

		productIds=productIds.substring(0, productIds.length() - 1);
	return productIds;
	}

	@Transactional
	public double fetchSupplierProductRate(long productId,String supplierId) {
		
		String hql="from SupplierProductList where product.productId="+productId+" and supplier.supplierId='"+supplierId+"'";
		Query query=sessionFactory.getCurrentSession().createQuery(hql);	
		List<SupplierProductList> list=(List<SupplierProductList>)query.list();
		if(list.isEmpty())
		{
			return 0;
		}
		
		
		return list.get(0).getSupplierRate();
	}
	
	@Transactional
	public SupplierProductList fetchSupplierByProductIdAndSupplierId(long productId,String supplierId) {
		
		String hql="from SupplierProductList where product.productId="+productId+" and supplier.supplierId='"+supplierId+"'";
		Query query=sessionFactory.getCurrentSession().createQuery(hql);	
		List<SupplierProductList> list=(List<SupplierProductList>)query.list();
		if(list.isEmpty())
		{
			return null;
		}
		
		
		return list.get(0);
	}
	
	
	@Transactional
	public String sendSMSTOSupplier(String supplierIds,String smsText) {

		try {
			String[] supplierIds2 = supplierIds.split(",");
			
			for(int i=0; i<supplierIds2.length; i++)
			{
				supplier=fetchSupplier(supplierIds2[i]);
				SendSMS.sendSMS(Long.parseLong(supplier.getContact().getMobileNumber()), smsText);
				System.out.println("SMS send to : "+supplierIds2[i]);
			}
			
			return "Success";
		} catch (Exception e) {
			System.out.println("sms sending failed "+e.toString());
			return "Failed";
			
		}
		
	}

}
