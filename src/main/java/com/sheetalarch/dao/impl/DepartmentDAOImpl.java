/**
 * 
 */
package com.sheetalarch.dao.impl;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.sheetalarch.dao.DepartmentDAO;
import com.sheetalarch.entity.Department;
import com.sheetalarch.utils.Constants;

/**
 * @author aNKIT
 *
 */
@Component
public class DepartmentDAOImpl implements DepartmentDAO{

	
	@Autowired
	SessionFactory sessionFactory;
	
	/* (non-Javadoc)
	 * @see com.sheetalarch.dao.DepartmentDAO#saveDepartment(com.sheetalarch.entity.Department)
	 * Nov 28, 20171:43:30 PM
	 */
	@Transactional
	public String saveDepartment(Department department) {
		// TODO Auto-generated method stub
		
		sessionFactory.getCurrentSession().save(department);
		
		return Constants.SUCCESS_RESPONSE;
	}

	/* (non-Javadoc)
	 * @see com.sheetalarch.dao.DepartmentDAO#updateDepartment(com.sheetalarch.entity.Department)
	 * Nov 28, 20171:43:30 PM
	 */
	@Transactional
	public String updateDepartment(Department department) {
		// TODO Auto-generated method stub
		
		sessionFactory.getCurrentSession().update(department);
		
		return Constants.SUCCESS_RESPONSE;
	}

	/* (non-Javadoc)
	 * @see com.sheetalarch.dao.DepartmentDAO#fetchDepartmentList()
	 * Nov 28, 20171:43:30 PM
	 */
	@Transactional
	public List<Department> fetchDepartmentList() {
		// TODO Auto-generated method stub
		
		String hql="from Department";
		Query query=sessionFactory.getCurrentSession().createQuery(hql);
		List<Department> departmentList=(List<Department>)query.list();
		if(departmentList.isEmpty())
		{
			return null;
		}
		return departmentList;
	}

	/* (non-Javadoc)
	 * @see com.sheetalarch.dao.DepartmentDAO#fetchDepartment(com.sheetalarch.entity.Department)
	 * Nov 28, 20172:41:17 PM
	 */
	@Transactional
	public Department fetchDepartment(long departmentId) {
		// TODO Auto-generated method stub
		return (Department)sessionFactory.getCurrentSession().get(Department.class, departmentId);
	}

	
	
}
