package com.sheetalarch.dao.impl;


import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.sheetalarch.admin.models.InventoryProduct;
import com.sheetalarch.admin.models.InventoryReportView;
import com.sheetalarch.admin.models.PaymentDoInfo;
import com.sheetalarch.dao.InventoryDAO;
import com.sheetalarch.entity.Inventory;
import com.sheetalarch.entity.InventoryDetails;
import com.sheetalarch.entity.PaymentPaySupplier;
import com.sheetalarch.entity.Product;
import com.sheetalarch.entity.SupplierProductList;
import com.sheetalarch.models.InventorySaveRequestModel;
import com.sheetalarch.models.ProductAddInventory;
import com.sheetalarch.utils.InventoryTransactionIdGenerator;



@Repository("inventoryDAO")
@Component
public class InventoryDAOImpl implements InventoryDAO {

	@Autowired
	SessionFactory sessionFactory;
	
	
	SimpleDateFormat simpleDateFormat=new SimpleDateFormat("yyyy/MM/dd");
	
	@Transactional
	public List<ProductAddInventory> fetchProductBySupplierId(String supplierId){
		
		List<ProductAddInventory> productAddInventories=new ArrayList<ProductAddInventory>();
		
		
		String hql="from SupplierProductList where supplier.supplierId='"+supplierId+"'";
		Query query=sessionFactory.getCurrentSession().createQuery(hql);
		
		@SuppressWarnings("unchecked")
		List<SupplierProductList> list= (List<SupplierProductList>) query.list();
		
		if(list.isEmpty()){
			return null;
		}
		
		Iterator<SupplierProductList> iterator=list.iterator();
		while(iterator.hasNext()){
		SupplierProductList supplierProductList=iterator.next();
		ProductAddInventory addInventory=new ProductAddInventory();
		addInventory.setCurrentQuantity(supplierProductList.getProduct().getCurrentQuantity());
		addInventory.setProductBrandId(supplierProductList.getProduct().getBrand().getBrandId());
		addInventory.setProductCategoryId(supplierProductList.getProduct().getCategories().getCategoryId());
		addInventory.setProductCode(supplierProductList.getProduct().getProductCode());
		addInventory.setProductId(supplierProductList.getProduct().getProductId());
		addInventory.setProductName(supplierProductList.getProduct().getProductName());
		addInventory.setRate(supplierProductList.getProduct().getRate());
		addInventory.setProductThreshold(supplierProductList.getProduct().getThreshold());
		addInventory.setSupplierRate(supplierProductList.getSupplierRate());
		addInventory.setCategories(supplierProductList.getProduct().getCategories());
		productAddInventories.add(addInventory);
		}
		return productAddInventories;
	}
	
	@Transactional
	public List<ProductAddInventory> fetchProductListByBrandIdAndCategoryIdForApp(String supplierId,long categoryId, long brandId) {
						
		
		List<ProductAddInventory> productAddInventoryList=fetchProductBySupplierId(supplierId);
			
			if(productAddInventoryList.isEmpty())
			{
				return null;				
			}			
			
			if(categoryId==0 && brandId==0){
				return productAddInventoryList;
			}
			List<ProductAddInventory> productAddInventoryList2=new ArrayList<>();

			if(categoryId==0){

				for(ProductAddInventory productAddInventory : productAddInventoryList)
				{
					if(productAddInventory.getProductBrandId()==brandId)
					{
						productAddInventoryList2.add(productAddInventory);
					}
				}
	        
	        }
	        else if(brandId==0){
	        	for(ProductAddInventory productAddInventory : productAddInventoryList)
				{
					if(productAddInventory.getProductCategoryId()==categoryId)
					{
						productAddInventoryList2.add(productAddInventory);
					}
				}    
	        }		
	        else
	        {
	        	for(ProductAddInventory productAddInventory : productAddInventoryList)
				{
					if(productAddInventory.getProductBrandId()==brandId && productAddInventory.getProductCategoryId()==categoryId)
					{
						productAddInventoryList2.add(productAddInventory);
					}
				}  
	        }
			
		return productAddInventoryList2;
	}
	
	
	public InventoryDAOImpl(	SessionFactory sessionFactory) {
		// TODO Auto-generated constructor stub
		this.sessionFactory=sessionFactory;
	}
	
	public InventoryDAOImpl() {
		// TODO Auto-generated constructor stub
	}
	
	@Transactional
	public List<InventoryProduct> inventoryProductList() {

		Query query=sessionFactory.getCurrentSession().createQuery("from Product");
		
		List<InventoryProduct> inventoryProductsList=new ArrayList<>();
		
		List<Product> productList=(List<Product>)query.list();
		if(productList.isEmpty())
		{
			return null;
		}
		long srno=1;
		for(Product product : productList)
		{
			double igst=product.getCategories().getIgst();
			double sgst=product.getCategories().getSgst();;
			double cgst=product.getCategories().getCgst();;
			double rate=product.getRate();
			double rateWithTax= ((rate*igst)/100)/*+
								((rate*cgst)/100)+
								((rate*sgst)/100)*/+rate;
			double curQty=product.getCurrentQuantity();
			double taxableAmount=curQty*rate;
			double tax=	((taxableAmount*igst)/100)/*+
						((taxableAmount*cgst)/100)+
						((taxableAmount*sgst)/100)*/;
			double taxAmount=curQty*rateWithTax;
			inventoryProductsList.add(new InventoryProduct(srno,product, rateWithTax, taxableAmount, tax,taxAmount));
			srno++;
		}
		
		return inventoryProductsList;
	}

	
	@Transactional
	public List<SupplierProductList> fetchSupplierListByProductId(long productId) {

		String hql="from SupplierProductList where product.productId="+productId;
		Query query=sessionFactory.getCurrentSession().createQuery(hql);
		List<SupplierProductList> supplierProductLists=(List<SupplierProductList>)query.list();
		if(supplierProductLists.isEmpty())
		{
			return null;
		}
		
		return supplierProductLists;
	}
	
	@Transactional
	public void addInventory(Inventory inventory,String productIdList)
	{
		//supplierIdForOrder,productIdForOrder, orderQuantity,supplierMobileNumber
		SupplierDAOImpl supplierDAOimpl=new SupplierDAOImpl(sessionFactory);
		ProductDAOImpl productDAOImpl=new ProductDAOImpl(sessionFactory);
		
		InventoryTransactionIdGenerator transactionIdGenerator=new InventoryTransactionIdGenerator(sessionFactory);
		inventory.setInventoryTransactionId(transactionIdGenerator.generateInventoryTransactionId());
		double totalAmount=0;
		double totalAmountWithTax=0;
	    long totalQuantity=0;
		
		String[] prdutIdAndRateList = productIdList.split(",");
		
		for(int i=0; i<prdutIdAndRateList.length; i++)
		{
			String[] prdutIdAndRate=prdutIdAndRateList[i].split("-");
			
			SupplierProductList supplierProductList=supplierDAOimpl.fetchSupplierByProductIdAndSupplierId(Long.parseLong(prdutIdAndRate[0]), inventory.getSupplier().getSupplierId());
			totalAmount=totalAmount+(Long.parseLong(prdutIdAndRate[1])*supplierProductList.getSupplierRate());
			
			Product product=new Product();
			product=productDAOImpl.fetchProductForWebApp(Long.parseLong(prdutIdAndRate[0]));
			float igst=product.getCategories().getIgst();
			float cgst=product.getCategories().getCgst();
			float sgst=product.getCategories().getSgst();
			totalAmountWithTax=totalAmount+
								((igst*totalAmount)/100)/*+
								((cgst*totalAmount)/100)+
								((sgst*totalAmount)/100)*/;
			totalQuantity+=Long.parseLong(prdutIdAndRate[1]);
		}

		inventory.setTotalQuantity(totalQuantity);
		inventory.setTotalAmount(totalAmount);
		inventory.setTotalAmountTax(totalAmountWithTax);
		inventory.setPayStatus(false);
		sessionFactory.getCurrentSession().save(inventory);
		
		prdutIdAndRateList = productIdList.split(",");
		
		for(int i=0; i<prdutIdAndRateList.length; i++)
		{
			InventoryDetails inventoryDetails=new InventoryDetails();
			
			inventoryDetails.setInventory(inventory);
			
			String[] prdutIdAndRate=prdutIdAndRateList[i].split("-");
			Product product=new Product();
			product=productDAOImpl.fetchProductForWebApp(Long.parseLong(prdutIdAndRate[0]));
			//product.setProductId(Long.parseLong(prdutIdAndRate[0]));
			inventoryDetails.setProduct(product);
			
			inventoryDetails.setQuantity(Long.parseLong(prdutIdAndRate[1]));
			
			SupplierProductList supplierProductList=supplierDAOimpl.fetchSupplierByProductIdAndSupplierId(Long.parseLong(prdutIdAndRate[0]), inventory.getSupplier().getSupplierId());
			inventoryDetails.setAmount(supplierProductList.getSupplierRate()*Long.parseLong(prdutIdAndRate[1]));
			
			inventoryDetails.setRate((float)supplierProductList.getSupplierRate());			
			
			
			product.setCurrentQuantity(product.getCurrentQuantity()+Long.parseLong(prdutIdAndRate[1]));
			
			sessionFactory.getCurrentSession().update(product);
			sessionFactory.getCurrentSession().save(inventoryDetails);
		}

		
	}

	@Transactional
	public Inventory fetchInventory(String inventoryId) {		
		return (Inventory)sessionFactory.getCurrentSession().get(Inventory.class, inventoryId);
	}
	
	@Transactional
	public PaymentDoInfo fetchPaymentStatus(String inventoryTransactionId)
	{		
		Inventory inventory=fetchInventory(inventoryTransactionId);
		
		String id=inventory.getSupplier().getSupplierId();
		String name=inventory.getSupplier().getName();
		String inventoryId=inventory.getInventoryTransactionId();
		double amountPaid=0.0d;
		double amountUnPaid=inventory.getTotalAmountTax();
		double totalAmount=inventory.getTotalAmount();
		String type="supplier";
		String url="giveSupplierOrderPayment";
		
		if(inventory.isPayStatus()==false)
		{
			Query query=sessionFactory.getCurrentSession().createQuery("from PaymentPaySupplier where inventory.inventoryTransactionId='"+inventoryTransactionId+"' order by paidDate desc");
			List<PaymentPaySupplier> paymentPaySupplierList=(List<PaymentPaySupplier>)query.list();
			
			if(paymentPaySupplierList.isEmpty()==false)
			{
				amountUnPaid=paymentPaySupplierList.get(0).getDueAmount();
				amountPaid=totalAmount-amountUnPaid;
			}
		}

		return new PaymentDoInfo(0,id, name, inventoryId, amountPaid, amountUnPaid, totalAmount, type, url);
	}
	
	@Transactional
	public void givePayment(PaymentPaySupplier paymentPaySupplier)
	{
		sessionFactory.getCurrentSession().update(paymentPaySupplier.getInventory());
		sessionFactory.getCurrentSession().save(paymentPaySupplier);
	}

	@Transactional
	public List<InventoryReportView> fetchInventoryReportView(String supplierId,String startDate,String endDate, String range)
	{
		List<InventoryReportView> inventoryReportViews=new ArrayList<>();
		
		String hql="";
		Query query;
		Calendar cal=Calendar.getInstance();
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		/**
		 * here only show inventory details which payment unpaid till current date
		 * 
		 * */
		if(supplierId.equals(""))
		{
			if(range.equals("range")){
				hql="from Inventory where (date(inventoryAddedDatetime) >= '"+startDate+"' and date(inventoryAddedDatetime) <= '"+endDate+"') and payStatus=false order by inventoryAddedDatetime desc";
			}
			else if(range.equals("yesterday")){
				cal.add(Calendar.DAY_OF_MONTH, -1);
				hql="from Inventory where (date(inventoryAddedDatetime) >= '"+dateFormat.format(cal.getTime())+"') and payStatus=false order by inventoryAddedDatetime desc";
			}
			else if(range.equals("last7days")){
				cal.add(Calendar.DAY_OF_MONTH, -7);
				hql="from Inventory where (date(inventoryAddedDatetime) >= '"+dateFormat.format(cal.getTime())+"') and payStatus=false order by inventoryAddedDatetime desc";
			}
			else if(range.equals("last1month")){
				cal.add(Calendar.MONTH, -1);
				hql="from Inventory where (date(inventoryAddedDatetime) >= '"+dateFormat.format(cal.getTime())+"') and payStatus=false order by inventoryAddedDatetime desc";
			}
			else if(range.equals("last3months")){
				cal.add(Calendar.MONTH, -3);
				hql="from Inventory where (date(inventoryAddedDatetime) >= '"+dateFormat.format(cal.getTime())+"' ) and payStatus=false order by inventoryAddedDatetime desc";
			}
			else if(range.equals("today")){
				hql="from Inventory where (date(inventoryAddedDatetime) = '"+dateFormat.format(cal.getTime())+"' ) and payStatus=false order by inventoryAddedDatetime desc";
			}
			else if(range.equals("last1year")){
				cal.add(Calendar.MONTH, -12);
				hql="from Inventory where (date(inventoryAddedDatetime) >= '"+dateFormat.format(cal.getTime())+"') and payStatus=false order by inventoryAddedDatetime desc";
			}
			else if(range.equals("viewAll")){
				hql="from Inventory where payStatus=false order by inventoryAddedDatetime desc";
			}
			else if(range.equals("pickdate")){
				hql="from Inventory where (date(inventoryAddedDatetime) = '"+startDate+"') and payStatus=false order by inventoryAddedDatetime desc";
			}
		}
		else
		{
			if(range.equals("range")){
				hql="from Inventory where (date(inventoryAddedDatetime) >= '"+startDate+"' and date(inventoryAddedDatetime) <= '"+endDate+"') and payStatus=false and supplier.supplierId='"+supplierId+"' order by inventoryAddedDatetime desc";
			}
			else if(range.equals("yesterday")){
				cal.add(Calendar.DAY_OF_MONTH, -1);
				hql="from Inventory where (date(inventoryAddedDatetime) >= '"+dateFormat.format(cal.getTime())+"') and payStatus=false and supplier.supplierId='"+supplierId+"' order by inventoryAddedDatetime desc";
			}
			else if(range.equals("last7days")){
				cal.add(Calendar.DAY_OF_MONTH, -7);
				hql="from Inventory where (date(inventoryAddedDatetime) >= '"+dateFormat.format(cal.getTime())+"') and payStatus=false and supplier.supplierId='"+supplierId+"' order by inventoryAddedDatetime desc";
			}
			else if(range.equals("last1month")){
				cal.add(Calendar.MONTH, -1);
				hql="from Inventory where (date(inventoryAddedDatetime) >= '"+dateFormat.format(cal.getTime())+"') and payStatus=false and supplier.supplierId='"+supplierId+"' order by inventoryAddedDatetime desc";
			}
			else if(range.equals("last3months")){
				cal.add(Calendar.MONTH, -3);
				hql="from Inventory where (date(inventoryAddedDatetime) >= '"+dateFormat.format(cal.getTime())+"') and payStatus=false and supplier.supplierId='"+supplierId+"' order by inventoryAddedDatetime desc";
			}
			else if(range.equals("last1year")){
				cal.add(Calendar.MONTH, -12);
				hql="from Inventory where (date(inventoryAddedDatetime) >= '"+dateFormat.format(cal.getTime())+"') and payStatus=false and supplier.supplierId='"+supplierId+"' order by inventoryAddedDatetime desc";
			}
			else if(range.equals("viewAll")){
				hql="from Inventory where payStatus=false and supplier.supplierId='"+supplierId+"' order by inventoryAddedDatetime desc";
			}
			else if(range.equals("pickdate")){
				hql="from Inventory where (date(inventoryAddedDatetime) = '"+startDate+"') and payStatus=false and supplier.supplierId='"+supplierId+"' order by inventoryAddedDatetime desc";
			}
			else if(range.equals("today")){
				hql="from Inventory where (date(inventoryAddedDatetime) = '"+dateFormat.format(cal.getTime())+"' ) and payStatus=false and supplier.supplierId='"+supplierId+"' order by inventoryAddedDatetime desc";
			}
		}		
		
		query=sessionFactory.getCurrentSession().createQuery(hql);
		List<Inventory> inventoryList=(List<Inventory>)query.list();
		long srno=1;
		for(Inventory inventory:inventoryList)
		{
			double amountPaid=0;
			double amountUnPaid=0;
			hql="from PaymentPaySupplier where inventory.inventoryTransactionId='"+inventory.getInventoryTransactionId()+"' order by paidDate desc";
			query=sessionFactory.getCurrentSession().createQuery(hql);
			List<PaymentPaySupplier> paymentPaySupplierList=(List<PaymentPaySupplier>)query.list();
			
			if(paymentPaySupplierList.isEmpty())
			{
				amountUnPaid=inventory.getTotalAmountTax();
			}
			else
			{
				amountUnPaid=paymentPaySupplierList.get(0).getDueAmount();
			}
			amountPaid=inventory.getTotalAmountTax()-amountUnPaid;
			
			String payStatus="";
			
			if(amountPaid>=inventory.getTotalAmountTax())
			{
				payStatus="Paid";
			}
			else if(amountUnPaid<inventory.getTotalAmountTax() && amountUnPaid>0)
			{
				payStatus="Partially Paid";
			}
			else
			{
				payStatus="UnPaid";
			}
			
			inventoryReportViews.add(new InventoryReportView(
					srno, 
					inventory.getInventoryTransactionId(), 
					inventory.getSupplier(), 
					inventory.getTotalQuantity(), 
					inventory.getTotalAmount(), 
					inventory.getTotalAmountTax(), 
					amountPaid, 
					amountUnPaid, 
					inventory.getInventoryPaymentDatetime(), 
					payStatus
					));
		}
		
		return inventoryReportViews;
	}
	
	@Transactional
	public List<InventoryDetails> fetchTrasactionDetailsByInventoryId(String inventoryTransactionId)
	{
		String hql="from InventoryDetails where inventory.inventoryTransactionId='"+inventoryTransactionId+"'";
		Query query=sessionFactory.getCurrentSession().createQuery(hql);
		List<InventoryDetails> inventoryDetailList=(List<InventoryDetails>)query.list();
		
		return inventoryDetailList;
	}
	
	public List<InventoryProduct> makeInventoryProductViewProductNull(List<InventoryProduct> inventoryProductsList){
		
		List<InventoryProduct> inventoryProducts=new ArrayList<>();
		
		for(InventoryProduct inventoryProduct : inventoryProductsList)
		{
			inventoryProduct.getProduct().setProductImage(null);
			inventoryProducts.add(inventoryProduct);			
		}
		
		return inventoryProducts;
	}
	@Transactional
	public void saveInventoryForApp(InventorySaveRequestModel inventorySaveRequestModel) {
		// TODO Auto-generated method stub
		
		try {
			Inventory inventory=inventorySaveRequestModel.getInventory();		
			inventory.setInventoryAddedDatetime(new Date());	
			inventory.setInventoryPaymentDatetime(simpleDateFormat.parse(inventorySaveRequestModel.getPaymentDate()));
			InventoryTransactionIdGenerator inventoryTransactionIdGenerator=new InventoryTransactionIdGenerator(sessionFactory);
			inventory.setInventoryTransactionId(inventoryTransactionIdGenerator.generateInventoryTransactionId());
			
			sessionFactory.getCurrentSession().save(inventory);  // here we are saveing the inventory object
			
			// here we are saveing the Listof InventoryDetails 
			List<InventoryDetails> finalListToSave=inventorySaveRequestModel.getInventoryDetails();
			
			ProductDAOImpl productDAOImpl=new ProductDAOImpl(sessionFactory);
			
			for(InventoryDetails inventoryDetails:finalListToSave)
			{
				Product product=new Product();
				product=productDAOImpl.fetchProductForWebApp(inventoryDetails.getProduct().getProductId());
				inventoryDetails.setProduct(product);
				
				product.setCurrentQuantity(product.getCurrentQuantity()+inventoryDetails.getQuantity());
				
				sessionFactory.getCurrentSession().update(product);
				
				inventoryDetails.setInventory(inventory);
				sessionFactory.getCurrentSession().save(inventoryDetails);
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	
	}
	
	@Transactional
	public List<Inventory> fetchAddedInventoryforGateKeeperReportByDateRange(String fromDate,
			String toDate, String range) {
		
		String hql="";
		Query query;
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		Calendar cal = Calendar.getInstance(); 
	
		if(range.equals("range")){
			hql="from Inventory where date(inventoryAddedDatetime)>='"+fromDate+"' And date(inventoryAddedDatetime)<='"+toDate+"'"; 
			}else if(range.equals("last7days")){
				cal.add(Calendar.DAY_OF_MONTH, -7);
				hql="from Inventory where date(inventoryAddedDatetime)>='"+dateFormat.format(cal.getTime())+"'";
			}else if(range.equals("last1month")){
				cal.add(Calendar.MONTH, -1);
				hql="from Inventory where  date(inventoryAddedDatetime)>='"+dateFormat.format(cal.getTime())+"'";
		
			}else if(range.equals("last3months")){
				cal.add(Calendar.MONTH, -3);
		
				hql="from Inventory where  date(inventoryAddedDatetime)>='"+dateFormat.format(cal.getTime())+"'";
			}else if(range.equals("pickDate")){
				hql="from Inventory where  date(inventoryAddedDatetime)='"+fromDate +"'";
			}
			else if(range.equals("currentDate"))
            {
                hql="from Inventory where  date(inventoryAddedDatetime)=date(CURRENT_DATE())";
            }
			else if(range.equals("viewAll"))
			{
				hql="from Inventory ";
			}

			query=sessionFactory.getCurrentSession().createQuery(hql);
			List<Inventory> inventoryAddedList=(List<Inventory>)query.list();
			
			if(inventoryAddedList.isEmpty()){
				return null;
			}
			return inventoryAddedList;					
	}
	@Transactional
	public Inventory fetchInventoryByInventoryTransactionIdforGateKeeperReport(String inventoryTransactionId) {
		// TODO Auto-generated method stub
		
		String hql="";
		Query query;
		

		
		hql="from Inventory where inventoryTransactionId='"+inventoryTransactionId +"'";
		query=sessionFactory.getCurrentSession().createQuery(hql);
		List<Inventory> inventoryList=(List<Inventory>)query.list();
		
		if(inventoryList.isEmpty()){
			return null;
		}
		return inventoryList.get(0);
	}
	@Transactional
	public List<InventoryDetails> fetchInventoryDetailsByInventoryTransactionIdforGateKeeperReport(
			String inventoryTransactionId) {
		// TODO Auto-generated method stub
		String hql="";
		Query query;
		
		hql="from InventoryDetails where inventory.inventoryTransactionId='"+inventoryTransactionId +"'";
		query=sessionFactory.getCurrentSession().createQuery(hql);
		List<InventoryDetails> inventoryDetailsList=(List<InventoryDetails>)query.list();
		if(inventoryDetailsList.isEmpty()){
			return null;
		}
		
		
		return inventoryDetailsList;
	}
	
	public List<InventoryDetails> makeProductImageNullOfInventoryDetailsList(List<InventoryDetails> inventoryDetailsList)
	{
		List<InventoryDetails> inventoryDetailsList2=new ArrayList<>();
		
		for(InventoryDetails inventoryDetails: inventoryDetailsList)
		{
			inventoryDetails.getProduct().setProductImage(null);
			inventoryDetailsList2.add(inventoryDetails);
		}
		return inventoryDetailsList2;
	}
	
	@Transactional
	public double fetchTotalValueOfCurrrentInventory()
	{
		ProductDAOImpl productDAOImpl=new ProductDAOImpl(sessionFactory);
		List<Product> productsList=productDAOImpl.fetchProductListForWebApp();
		
		double totalValueOfCurrrentInventory=0;
		for(Product product: productsList)
		{
			totalValueOfCurrrentInventory+=product.getCurrentQuantity()+product.getRate();
		}
		
		return totalValueOfCurrrentInventory;
	}
	
	@Transactional
	public long fetchProductUnderThresholdCount(){
		
		ProductDAOImpl productDAOImpl=new ProductDAOImpl(sessionFactory);
		List<Product> productsList=productDAOImpl.fetchProductListForWebApp();
		
		long productUnderThresholdCount=0;
		for(Product product: productsList)
		{
			if(product.getCurrentQuantity()<=product.getThreshold())
			{
				productUnderThresholdCount++;
			}
		}
		
		return productUnderThresholdCount;
	}
	
	@Transactional
	public double fetchBalanceAmtBySupplierId(String supplierId)
	{
		double balAmount=0;
		String hql;
		Query query;
		
		hql="from Inventory where supplier.supplierId='"+supplierId+"' and payStatus=false";
		query=sessionFactory.getCurrentSession().createQuery(hql);
		List<Inventory> inventoryList=(List<Inventory>)query.list();
		if(inventoryList.isEmpty())
		{
			return 0;
		}
		
		for(Inventory inventory : inventoryList)
		{
			hql="from PaymentPaySupplier where inventory.inventoryTransactionId='"+inventory.getInventoryTransactionId()+"' order by paidDate desc";
			query=sessionFactory.getCurrentSession().createQuery(hql);
			List<PaymentPaySupplier> paymentPaySupplierList=(List<PaymentPaySupplier>)query.list();
			
			if(paymentPaySupplierList.isEmpty())
			{
				balAmount+=inventory.getTotalAmountTax();
			}
			else
			{
				balAmount+=paymentPaySupplierList.get(0).getDueAmount();
			}
		}
		
		return balAmount;
	}
}
