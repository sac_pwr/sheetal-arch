package com.sheetalarch.dao.impl;


import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.sheetalarch.admin.models.SupplierOrderProduct;
import com.sheetalarch.dao.SupplierOrderDAO;
import com.sheetalarch.entity.Product;
import com.sheetalarch.entity.SupplierOrder;
import com.sheetalarch.entity.SupplierOrderDetails;
import com.sheetalarch.entity.SupplierProductList;
import com.sheetalarch.utils.OrderIdSupplierGenerator;
import com.sheetalarch.utils.SendSMS;


@Repository("supplierOrderDAO")
@Component
public class SupplierOrderDAOImpl implements SupplierOrderDAO {
	@Autowired
	SessionFactory sessionFactory;

	@Autowired
	SupplierOrder supplierOrder;
	
	
	public SupplierOrderDAOImpl(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	public SupplierOrderDAOImpl() {
	}
	
	@Transactional
	public void saveSupplierOrder(String productWithSupplier) {
		SupplierDAOImpl supplierDAOimpl=new SupplierDAOImpl(sessionFactory);
		ProductDAOImpl productDAOImpl=new ProductDAOImpl(sessionFactory);
				
		double totalAmount=0;
		double totalAmountWithTax=0;
		long totalQuantity=0;
		String[] inventoryRecordList = productWithSupplier.split(",");
		
		for(int i=0; i<inventoryRecordList.length; i++)
		{
			String[] inventoryRecord=inventoryRecordList[i].split("-");
			
			SupplierProductList supplierProductList=supplierDAOimpl.fetchSupplierByProductIdAndSupplierId(Long.parseLong(inventoryRecord[1]), inventoryRecord[0]);
			totalAmount=totalAmount+(Long.parseLong(inventoryRecord[2])*supplierProductList.getSupplierRate());
			
			totalQuantity+=Long.parseLong(inventoryRecord[2]);
			
			Product product=new Product();
			product=productDAOImpl.fetchProductForWebApp(Long.parseLong(inventoryRecord[1]));
			float igst=product.getCategories().getIgst();
			float cgst=product.getCategories().getCgst();
			float sgst=product.getCategories().getSgst();
			double tA=Long.parseLong(inventoryRecord[2])*supplierProductList.getSupplierRate();
			totalAmountWithTax+=tA+
								((igst*tA)/100)/*+
								((cgst*tA)/100)+
								((sgst*tA)/100)*/;
		}

		supplierOrder.setTotalAmount(totalAmount);
		supplierOrder.setTotalAmountWithTax(totalAmountWithTax);
		supplierOrder.setTotalQuantity(totalQuantity);
		supplierOrder.setSupplierOrderDatetime(new Date());
		
		OrderIdSupplierGenerator orderIdSupplierGenerator=new OrderIdSupplierGenerator(sessionFactory); 
		supplierOrder.setSupplierOrderId(orderIdSupplierGenerator.generate());
		sessionFactory.getCurrentSession().save(supplierOrder);				
		
		inventoryRecordList = productWithSupplier.split(",");
		
		Map<String,List<SupplierOrderProduct>> mapSupplierOrderProduct=new HashMap<>();
		
		for(int i=0; i<inventoryRecordList.length; i++)
		{
			SupplierOrderDetails supplierOrderDetails=new SupplierOrderDetails();
			
			supplierOrderDetails.setSupplierOrder(supplierOrder);
			
			String[] inventoryRecord=inventoryRecordList[i].split("-");
			Product product=new Product();
			product=productDAOImpl.fetchProductForWebApp(Long.parseLong(inventoryRecord[1]));
			//product.setProductId(Long.parseLong(prdutIdAndRate[0]));
			supplierOrderDetails.setProduct(product);
			
			supplierOrderDetails.setQuantity(Long.parseLong(inventoryRecord[2]));
			
			SupplierProductList supplierProductList=supplierDAOimpl.fetchSupplierByProductIdAndSupplierId(Long.parseLong(inventoryRecord[1]), inventoryRecord[0]);
			totalAmount=supplierProductList.getSupplierRate()*Long.parseLong(inventoryRecord[2]);
			supplierOrderDetails.setTotalAmount(totalAmount);
			
			totalAmountWithTax=0;
			float igst=product.getCategories().getIgst();
			float cgst=product.getCategories().getCgst();
			float sgst=product.getCategories().getSgst();
			totalAmountWithTax=totalAmount+
								((igst*totalAmount)/100)/*+
								((cgst*totalAmount)/100)+
								((sgst*totalAmount)/100)*/;
			supplierOrderDetails.setTotalAmountWithTax(totalAmountWithTax);
			
			supplierOrderDetails.setSupplierRate(supplierProductList.getSupplierRate());	
			supplierOrderDetails.setSupplier(supplierDAOimpl.fetchSupplier(inventoryRecord[0]));
			
			sessionFactory.getCurrentSession().save(supplierOrderDetails);
			
			List<SupplierOrderProduct> supplierOrderProductList=mapSupplierOrderProduct.get(supplierOrderDetails.getSupplier().getSupplierId());
			if(supplierOrderProductList==null)
			{
				supplierOrderProductList=new ArrayList<>();
				supplierOrderProductList.add(new SupplierOrderProduct(product.getProductName(), supplierOrderDetails.getQuantity()));
				mapSupplierOrderProduct.put(supplierOrderDetails.getSupplier().getSupplierId(), supplierOrderProductList);
			}
			else
			{
				for(Map.Entry<String,List<SupplierOrderProduct>> entry : mapSupplierOrderProduct.entrySet())
				{
					
					if(entry.getKey()==supplierOrderDetails.getSupplier().getSupplierId())
					{
						List<SupplierOrderProduct> supplierOrderProductList2=entry.getValue();
						supplierOrderProductList2.add(new SupplierOrderProduct(product.getProductName(), supplierOrderDetails.getQuantity()));
						mapSupplierOrderProduct.put(supplierOrderDetails.getSupplier().getSupplierId(), supplierOrderProductList2);
					}
				}
			}
		}	
		
		System.out.println(" Supplier SMS going start .....");
		
		for(Map.Entry<String,List<SupplierOrderProduct>> entry : mapSupplierOrderProduct.entrySet())
		{
			long mobileno=Long.parseLong(supplierDAOimpl.fetchSupplier(entry.getKey()).getContact().getMobileNumber());
			String smsText="Order Book From BlueSquare : \r\n";
			
			for(SupplierOrderProduct supplierOrderProduct : entry.getValue())
			{
				smsText+=supplierOrderProduct.getProductName()+" - "+supplierOrderProduct.getQuatity()+" qty\r\n";
			}
			
			SendSMS.sendSMS(mobileno, smsText);
		}
		
		
		
	}
	
	@Transactional
	public void editSupplierOrder(String productWithSupplier, SupplierOrder supplierOrder) {
		
		SupplierDAOImpl supplierDAOimpl=new SupplierDAOImpl(sessionFactory);
		ProductDAOImpl productDAOImpl=new ProductDAOImpl(sessionFactory);
				
		double totalAmount=0;
		double totalAmountWithTax=0;
		long totalQuantity=0;
		
		String[] inventoryRecordList = productWithSupplier.split(",");
		
		
		
		for(int i=0; i<inventoryRecordList.length; i++)
		{
			String[] inventoryRecord=inventoryRecordList[i].split("-");
			
			SupplierProductList supplierProductList=supplierDAOimpl.fetchSupplierByProductIdAndSupplierId(Long.parseLong(inventoryRecord[1]), inventoryRecord[0]);
			totalAmount=totalAmount+(Long.parseLong(inventoryRecord[2])*supplierProductList.getSupplierRate());

			totalQuantity+=Long.parseLong(inventoryRecord[2]);
			
			
			Product product=new Product();
			product=productDAOImpl.fetchProductForWebApp(Long.parseLong(inventoryRecord[1]));
			float igst=product.getCategories().getIgst();
			float cgst=product.getCategories().getCgst();
			float sgst=product.getCategories().getSgst();
			double tA=Long.parseLong(inventoryRecord[2])*supplierProductList.getSupplierRate();
			totalAmountWithTax+=tA+
								((igst*tA)/100)/*+
								((cgst*tA)/100)+
								((sgst*tA)/100)*/;
		}

		supplierOrder.setTotalAmount(totalAmount);
		supplierOrder.setTotalAmountWithTax(totalAmountWithTax);
		supplierOrder.setTotalQuantity(totalQuantity);
		supplierOrder.setSupplierOrderUpdateDatetime(new Date());
		sessionFactory.getCurrentSession().update(supplierOrder);				
		
		Query query=sessionFactory.getCurrentSession().createQuery("from SupplierOrderDetails where supplierOrder.supplierOrderId='"+supplierOrder.getSupplierOrderId()+"'");
		List<SupplierOrderDetails> supplierOrderDetailList=(List<SupplierOrderDetails>)query.list();
		for(SupplierOrderDetails supplierOrderDetail : supplierOrderDetailList)
		{
			sessionFactory.getCurrentSession().delete(supplierOrderDetail);	
		}
		
		inventoryRecordList = productWithSupplier.split(",");
		
		Map<String,List<SupplierOrderProduct>> mapSupplierOrderProduct=new HashMap<>();
		
		for(int i=0; i<inventoryRecordList.length; i++)
		{
			SupplierOrderDetails supplierOrderDetails=new SupplierOrderDetails();
			
			supplierOrderDetails.setSupplierOrder(supplierOrder);
			
			String[] inventoryRecord=inventoryRecordList[i].split("-");
			Product product=new Product();
			product=productDAOImpl.fetchProductForWebApp(Long.parseLong(inventoryRecord[1]));
			//product.setProductId(Long.parseLong(prdutIdAndRate[0]));
			supplierOrderDetails.setProduct(product);
			
			supplierOrderDetails.setQuantity(Long.parseLong(inventoryRecord[2]));
			
			SupplierProductList supplierProductList=supplierDAOimpl.fetchSupplierByProductIdAndSupplierId(Long.parseLong(inventoryRecord[1]), inventoryRecord[0]);
			totalAmount=supplierProductList.getSupplierRate()*Long.parseLong(inventoryRecord[2]);
			supplierOrderDetails.setTotalAmount(totalAmount);
			
			totalAmountWithTax=0;
			float igst=product.getCategories().getIgst();
			float cgst=product.getCategories().getCgst();
			float sgst=product.getCategories().getSgst();
			totalAmountWithTax=totalAmount+
								((igst*totalAmount)/100)/*+
								((cgst*totalAmount)/100)+
								((sgst*totalAmount)/100)*/;
			supplierOrderDetails.setTotalAmountWithTax(totalAmountWithTax);
			
			supplierOrderDetails.setSupplierRate(supplierProductList.getSupplierRate());	
			supplierOrderDetails.setSupplier(supplierDAOimpl.fetchSupplier(inventoryRecord[0]));
			
			sessionFactory.getCurrentSession().save(supplierOrderDetails);
			List<SupplierOrderProduct> supplierOrderProductList=mapSupplierOrderProduct.get(supplierOrderDetails.getSupplier().getSupplierId());
			if(supplierOrderProductList==null)
			{
				supplierOrderProductList=new ArrayList<>();
				supplierOrderProductList.add(new SupplierOrderProduct(product.getProductName(), supplierOrderDetails.getQuantity()));
				mapSupplierOrderProduct.put(supplierOrderDetails.getSupplier().getSupplierId(), supplierOrderProductList);
			}
			else
			{
				for(Map.Entry<String,List<SupplierOrderProduct>> entry : mapSupplierOrderProduct.entrySet())
				{
					
					if(entry.getKey()==supplierOrderDetails.getSupplier().getSupplierId())
					{
						List<SupplierOrderProduct> supplierOrderProductList2=entry.getValue();
						supplierOrderProductList2.add(new SupplierOrderProduct(product.getProductName(), supplierOrderDetails.getQuantity()));
						mapSupplierOrderProduct.put(supplierOrderDetails.getSupplier().getSupplierId(), supplierOrderProductList2);
					}
				}
			}
		}	
		
		System.out.println(" Supplier SMS going start .....");
		
		for(Map.Entry<String,List<SupplierOrderProduct>> entry : mapSupplierOrderProduct.entrySet())
		{
			long mobileno=Long.parseLong(supplierDAOimpl.fetchSupplier(entry.getKey()).getContact().getMobileNumber());
			String smsText="Last Order Book Change From BlueSquare : \r\n";
			
			for(SupplierOrderProduct supplierOrderProduct : entry.getValue())
			{
				smsText+=supplierOrderProduct.getProductName()+" - "+supplierOrderProduct.getQuatity()+" qty\r\n";
			}
			
			SendSMS.sendSMS(mobileno, smsText);
		}
	}
	
	@Transactional
	public SupplierOrder fetchSupplierOrder(String supplierOrderId)
	{		
		return (SupplierOrder)sessionFactory.getCurrentSession().get(SupplierOrder.class, supplierOrderId);
	}
	
	@Transactional	
	public List<SupplierOrder> fetchSupplierOrderList()
	{
		String hql="from SupplierOrder";
		Query query= sessionFactory.getCurrentSession().createQuery(hql);
		List<SupplierOrder> supplierOrderList=(List<SupplierOrder>)query.list();
		if(supplierOrderList.isEmpty())
		{
			return null;
		}
		return supplierOrderList;
	}
	
	@Transactional	
	public List<SupplierOrderDetails> fetchSupplierOrderDetailsList(String supplierOrderId)
	{
		String hql="from SupplierOrderDetails where supplierOrder.supplierOrderId='"+supplierOrderId+"'";
		Query query= sessionFactory.getCurrentSession().createQuery(hql);
		List<SupplierOrderDetails> supplierOrderDetailsList=(List<SupplierOrderDetails>)query.list();
		if(supplierOrderDetailsList.isEmpty())
		{
			return null;
		}
		return supplierOrderDetailsList;
	}

	
	@Transactional
	public List<SupplierOrder> fetchSupplierOrders24hour(String range,String startDate,String endDate){
		
		try {
			Calendar cal = Calendar.getInstance();
			Calendar cal2 = Calendar.getInstance();
			String hql;
			Query query = null;
			SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
			//HH:mm:ss.SSSSSS
			SimpleDateFormat dateFormat2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSSSSS");
		/*	String orderDetailsId,
		 	OrderDetailsDAOImpl orderDetailsDAOImpl=new OrderDetailsDAOImpl(sessionFactory);
			orderDetails=orderDetailsDAOImpl.fetchOrderDetailsByOrderId(orderDetailsId);
			Calendar orderDate=Calendar.getInstance();
			orderDate.setTime(orderDetails.getOrderDetailsAddedDatetime());*/
			
			if (range.equals("last7days")) {
				cal.add(Calendar.DAY_OF_MONTH, -7);
				
				hql="from SupplierOrder where date(supplierOrderDatetime) >= date(:date1day) order by supplierOrderDatetime desc";
				query=sessionFactory.getCurrentSession().createQuery(hql);
				query.setCalendarDate("date1day", cal);

			}
			else if (range.equals("last1month")) {
				cal.add(Calendar.MONTH, -1);

				hql="from SupplierOrder where date(supplierOrderDatetime) >= date(:date1day) order by supplierOrderDatetime desc";
				query=sessionFactory.getCurrentSession().createQuery(hql);
				query.setCalendarDate("date1day", cal);

			}		
			else if (range.equals("all")) {

				hql="from SupplierOrder";
				query=sessionFactory.getCurrentSession().createQuery(hql);

			}
			else if (range.equals("CurrentDay")) {
				
				cal.add(Calendar.HOUR_OF_DAY, -24);			
				hql="from SupplierOrder where supplierOrderDatetime >= '"+dateFormat2.format(cal.getTime())+"' order by supplierOrderDatetime desc";
				query=sessionFactory.getCurrentSession().createQuery(hql);
				//query.setCalendarDate("date1day", cal);
			}
			else if (range.equals("range")) {
				cal.setTime(dateFormat.parse(startDate));
				cal2.setTime(dateFormat.parse(endDate));
			
				hql="from SupplierOrder where date(supplierOrderDatetime) >= date(:startDate) and date(supplierOrderDatetime) <= date(:endDate) order by supplierOrderDatetime desc";
				query=sessionFactory.getCurrentSession().createQuery(hql);
				query.setCalendarDate("startDate", cal);
				query.setCalendarDate("endDate", cal);
				
				query = sessionFactory.getCurrentSession().createQuery(hql);

			}
			List<SupplierOrder> list=(List<SupplierOrder>)query.list();
			if(list.isEmpty())
			{
				return null;
			}
					
			return list;
		} catch (Exception e) {
			System.out.println("fetchSupplierOrders24hour Error : "+e.toString());

			return null;
		}
	}
	
	
	@Transactional 
	public List<SupplierOrderDetails> fetchSupplierOrderDetailsListBySupplierOrderId(String supplierOrderId)
	{
		String hql2="from SupplierOrderDetails where supplierOrder.supplierOrderId='"+supplierOrderId+"'";
		Query query2= sessionFactory.getCurrentSession().createQuery(hql2);
		List<SupplierOrderDetails> supplierOrderDetailsList=(List<SupplierOrderDetails>)query2.list();
		if(supplierOrderDetailsList.isEmpty())
		{
			return null;			
		}
		
		return supplierOrderDetailsList;
	}
	
}
