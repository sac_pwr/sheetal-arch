/**
 * 
 */
package com.sheetalarch.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.sheetalarch.dao.ClusterDAO;
import com.sheetalarch.entity.Area;
import com.sheetalarch.entity.Cluster;
import com.sheetalarch.entity.Employee;
import com.sheetalarch.entity.EmployeeRouteList;
import com.sheetalarch.entity.Route;
import com.sheetalarch.utils.Constants;

/**
 * @author aNKIT
 *
 */
@Component
public class ClusterDAOImpl implements ClusterDAO{

	@Autowired
	SessionFactory sessionFactory;
	
	/**
	 * 
	 * Dec 9, 20171:02:31 PM
	 */
	public ClusterDAOImpl(SessionFactory sessionFactory) {
		// TODO Auto-generated constructor stub
		this.sessionFactory=sessionFactory;
	}
	
	@Transactional
	public Cluster fetchClusterBySalesmanId(long employeeId) {
		// TODO Auto-generated method stub
		
		String hql="from EmployeeRouteList where employeeDetails.employee.employeeId="+employeeId;
		Query query=sessionFactory.getCurrentSession().createQuery(hql);
		List<EmployeeRouteList> employeeRouteLists=(List<EmployeeRouteList>)query.list();
		
		List<Cluster> clusters=new ArrayList<>();
		for(EmployeeRouteList employeeRouteList:employeeRouteLists)
		{
			clusters.add(employeeRouteList.getRoute().getArea().getCluster());
		}
		if(clusters.isEmpty()){
			
		}
		return clusters.get(0);
	}

	@Transactional
	public Employee fetchGKEmployeeByClusterId(long clusterId) {
		// TODO Auto-generated method stub
		String hql="from EmployeeRouteList where route.area.cluster.clusterId="+ clusterId +" And employeeDetails.employee.department.name='"+ Constants.GATE_KEEPER_DEPT_NAME+"'";
		Query query=sessionFactory.getCurrentSession().createQuery(hql);
		List<EmployeeRouteList> employeeRouteLists=(List<EmployeeRouteList>)query.list();
		
		List<Employee> employees=new ArrayList<>();
		for(EmployeeRouteList employeeRouteList:employeeRouteLists){
			employees.add(employeeRouteList.getEmployeeDetails().getEmployee());
		}
		if(employees.isEmpty()){
			return null;
		}
		return employees.get(0);
	}

	/* (non-Javadoc)
	 * @see com.sheetalarch.dao.ClusterDAO#fetchClusterList()
	 * Nov 28, 20173:16:41 PM
	 */
	@Transactional
	public List<Cluster> fetchClusterList() {

		String hql="from Cluster";
		
		Query query=sessionFactory.getCurrentSession().createQuery(hql);
		List<Cluster> clusterList=(List<Cluster>)query.list();
			
		if(clusterList.isEmpty())
		{
			return null;
		}
		
		return clusterList;
	}
	
	@Transactional
	public void saveCluster(Cluster cluster) {
		sessionFactory.getCurrentSession().save(cluster);
	}
	
	@Transactional
	public void updateCluster(Cluster cluster) {
		sessionFactory.getCurrentSession().update(cluster);
	}

	/* (non-Javadoc)
	 * @see com.sheetalarch.dao.ClusterDAO#fetchClusterByClusterId(long)
	 * Nov 30, 20177:26:08 PM
	 */
	@Transactional
	public Cluster fetchClusterByClusterId(long clusterId) {
		// TODO Auto-generated method stub
		String hql="from Cluster where clusterId="+clusterId;
		
		Query query=sessionFactory.getCurrentSession().createQuery(hql);
		List<Cluster> clusterList=(List<Cluster>)query.list();
			
		if(clusterList.isEmpty())
		{
			return null;
		}
		
		return clusterList.get(0);
	}
	
	
	
	

}
