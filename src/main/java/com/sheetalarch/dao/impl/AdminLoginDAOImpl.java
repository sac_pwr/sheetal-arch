/**
 * 
 */
package com.sheetalarch.dao.impl;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.sheetalarch.dao.AdminLoginDAO;
import com.sheetalarch.entity.AdminLogin;

/**
 * @author aNKIT
 *
 */
@Repository("adminLoginDAO")
@Component
public class AdminLoginDAOImpl implements AdminLoginDAO {

	@Autowired
	SessionFactory sessionFactory;
	
	public AdminLoginDAOImpl(SessionFactory sessionFactory) {
		// TODO Auto-generated constructor stub
		this.sessionFactory=sessionFactory;
	}
	
	/* (non-Javadoc)
	 * @see com.sheetalarch.dao.AdminLoginDAO#loginValidate(java.lang.String, java.lang.String)
	 * Dec 14, 201712:58:02 PM
	 * 
	 * use for validate adming login
	 */
	@Transactional
	public AdminLogin loginValidate(String userName, String password) {
		// TODO Auto-generated method stub
		String hql = "from AdminLogin where userId= :user and password=:pass";
		Query query = sessionFactory.getCurrentSession().createQuery(hql);
		query.setParameter("user", userName);
		query.setParameter("pass", password);
		
		@SuppressWarnings("unchecked")
		List<AdminLogin> list = (List<AdminLogin>) query.list();

		if (list.isEmpty()) {
			return null;
		}
		return list.get(0);
	}

}
