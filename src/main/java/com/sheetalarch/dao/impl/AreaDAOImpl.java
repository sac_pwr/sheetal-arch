package com.sheetalarch.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.sheetalarch.dao.AreaDAO;
import com.sheetalarch.entity.Area;
import com.sheetalarch.entity.Cluster;
import com.sheetalarch.entity.Employee;
import com.sheetalarch.entity.EmployeeRouteList;
import com.sheetalarch.entity.Route;
import com.sheetalarch.utils.Constants;



@Repository("areaDAO")

@Component
public class AreaDAOImpl implements AreaDAO {
	
	@Autowired
	SessionFactory sessionFactory;
	
	public AreaDAOImpl(SessionFactory sessionFactory) {
		// TODO Auto-generated constructor stub
		this.sessionFactory=sessionFactory;
	}

	@Transactional
	public void save(Area area) {
		sessionFactory.getCurrentSession().save(area);
	}

	@Transactional
	public void update(Area area) {
		sessionFactory.getCurrentSession().update(area);
	}

	@Transactional
	public Cluster fetchClusterBySalesmanId(long employeeId) {
		// TODO Auto-generated method stub
		
		String hql="from EmployeeRouteList where employeeDetails.employee.employeeId="+employeeId;
		Query query=sessionFactory.getCurrentSession().createQuery(hql);
		List<EmployeeRouteList> employeeRouteLists=(List<EmployeeRouteList>)query.list();
		
		EmployeeDetailsDAOImpl employeeDetailsDAOImpl = new EmployeeDetailsDAOImpl(sessionFactory);
		
		List<Cluster> clusters=new ArrayList<>();
		for(EmployeeRouteList employeeRouteList:employeeRouteLists)
		{
			if(employeeRouteList.getEmployeeDetails().getEmployee().getDepartment().getName().equals(Constants.AREA_SALESMAN_DEPT_NAME)){
				List<Area> areas=employeeDetailsDAOImpl.fetchAreaListByEmployeeId(employeeId);
				List<Route> routes=employeeDetailsDAOImpl.fetchRouteListByAreaId(areas.get(0).getAreaId());
				clusters.add(areas.get(0).getCluster());
			}else{
				List<Route> routes=employeeDetailsDAOImpl.fetchEmployeeRouteListByEmployeeId(employeeId);
			clusters.add(routes.get(0).getArea().getCluster());
			}
		}
		if(clusters.isEmpty()){
			return null;
		}
		return clusters.get(0);
	}

	@Transactional
	public Employee fetchGKEmployeeByClusterId(long clusterId) {
		// TODO Auto-generated method stub
		String hql="from EmployeeRouteList where cluster.clusterId="+ clusterId +" And employeeDetails.employee.department.name='"+ Constants.GATE_KEEPER_DEPT_NAME+"'";
		Query query=sessionFactory.getCurrentSession().createQuery(hql);
		List<EmployeeRouteList> employeeRouteLists=(List<EmployeeRouteList>)query.list();
		
		List<Employee> employees=new ArrayList<>();
		for(EmployeeRouteList employeeRouteList:employeeRouteLists){
			employees.add(employeeRouteList.getEmployeeDetails().getEmployee());
		}
		if(employees.isEmpty()){
			return null;
		}
		return employees.get(0);
	}

	@Transactional
	public Area fetchAreaBysalesmanId(long employeeId) {
		// TODO Auto-generated method stub
		
		String hql="from EmployeeRouteList where employeeDetails.employee.employeeId="+employeeId;
		Query query=sessionFactory.getCurrentSession().createQuery(hql);
		List<EmployeeRouteList> employeeRouteLists=(List<EmployeeRouteList>)query.list();
		
		EmployeeDetailsDAOImpl employeeDetailsDAOImpl = new EmployeeDetailsDAOImpl(sessionFactory);
		List<Area> areas=new ArrayList<>();
		for(EmployeeRouteList employeeRouteList:employeeRouteLists){
			//areas.add(employeeRouteList/*.getRoute()*/.getArea());
			if(employeeRouteList.getEmployeeDetails().getEmployee().getDepartment().getName().equals(Constants.AREA_SALESMAN_DEPT_NAME)){
				List<Area> areas1=employeeDetailsDAOImpl.fetchAreaListByEmployeeId(employeeId);
				areas.addAll(areas1);
				//List<Route> routes=employeeDetailsDAOImpl.fetchRouteListByAreaId(areas.get(0).getAreaId());
				//clusters.add(areas.get(0).getCluster());
			}else{
				List<Route> routes=employeeDetailsDAOImpl.fetchEmployeeRouteListByEmployeeId(employeeId);
				areas.add(routes.get(0).getArea());
				/*clusters.add(routes.get(0).getArea().getCluster());*/
			}
			
		}
		if(areas.isEmpty()){
			return null;
		}
		else{
			return areas.get(0);
		}
	}

	@Transactional
	public Area fetchAreaByRouteId(long routeId) {
		// TODO Auto-generated method stub
		String hql="from Route where routeId="+routeId;
				Query query=sessionFactory.getCurrentSession().createQuery(hql);
				List<Route> routes=(List<Route>)query.list();
				if(routes.isEmpty()){
					return null;
				}
				return routes.get(0).getArea();
	}

	@Transactional
	public Cluster fetchClusterBtRouteId(long routeId) {
		String hql="from Route where routeId="+routeId;
		Query query=sessionFactory.getCurrentSession().createQuery(hql);
		List<Route> routes=(List<Route>)query.list();
		if(routes.isEmpty()){
			return null;
		}
		return routes.get(0).getArea().getCluster();
}


	//webApp
	@Transactional
	public List<Area> fetchAreaListByClusterId(long clusterId)
	{
		String hql="from Area where cluster.clusterId="+clusterId;
		Query query=sessionFactory.getCurrentSession().createQuery(hql);
		List<Area> areaList=(List<Area>)query.list();
		if(areaList.isEmpty())
		{
			return null;
		}
		return areaList;
	}

	/* (non-Javadoc)
	 * @see com.sheetalarch.dao.AreaDAO#fetchAreaList()
	 * Nov 30, 20171:13:04 PM
	 */
	@Override
	public List<Area> fetchAreaList() {

		String hql="from Area";
		
		Query query=sessionFactory.getCurrentSession().createQuery(hql);
		List<Area> areaList=(List<Area>)query.list();
			
		if(areaList.isEmpty())
		{
			return null;
		}
		
		return areaList;
	}

	/* (non-Javadoc)
	 * @see com.sheetalarch.dao.AreaDAO#saveArea(com.sheetalarch.entity.Area)
	 * Nov 30, 20171:13:04 PM
	 */
	@Override
	public void saveArea(Area area) {
		// TODO Auto-generated method stub
		sessionFactory.getCurrentSession().save(area);
	}

	/* (non-Javadoc)
	 * @see com.sheetalarch.dao.AreaDAO#updateArea(com.sheetalarch.entity.Area)
	 * Nov 30, 20171:13:04 PM
	 */
	@Override
	public void updateArea(Area area) {
		// TODO Auto-generated method stub
		sessionFactory.getCurrentSession().update(area);
	}
	
	
	@Transactional
	public Area fetchAreaByAreaId(long areaId) {
		// TODO Auto-generated method stub
		String hql="from Area where areaId="+areaId;
		
		Query query=sessionFactory.getCurrentSession().createQuery(hql);
		List<Area> areaList=(List<Area>)query.list();
			
		if(areaList.isEmpty())
		{
			return null;
		}
		
		return areaList.get(0);
	}
}
