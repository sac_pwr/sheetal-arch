package com.sheetalarch.dao.impl;

import java.sql.Blob;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;

import org.hibernate.Hibernate;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import com.sheetalarch.admin.models.ProductReportView;
import com.sheetalarch.admin.models.ProductViewList;
import com.sheetalarch.dao.ProductDAO;
import com.sheetalarch.entity.Brand;
import com.sheetalarch.entity.Categories;
import com.sheetalarch.entity.OrderDetails;
import com.sheetalarch.entity.OrderProductDetails;
import com.sheetalarch.entity.Product;
import com.sheetalarch.entity.SupplierProductList;
import com.sheetalarch.models.BrandAndCategoryRequest;
import com.sheetalarch.utils.Constants;

@Repository("productDAO")

@Component
public class ProductDAOImpl implements ProductDAO {
	

	@Autowired
	Brand brand;
	
	@Autowired
	SessionFactory sessionFactory;
	

	public ProductDAOImpl(SessionFactory sessionFactory) {
		// TODO Auto-generated constructor stub
		this.sessionFactory = sessionFactory;
	
	}
	
	@Transactional
	public List<Brand> fetchBrandList() {
		// TODO Auto-generated method stub
		String hql="";
		Query query;
		hql="from Brand";
		query=sessionFactory.getCurrentSession().createQuery(hql);
		List<Brand> brandList=(List<Brand>)query.list();
		
		if(brandList.isEmpty()){
			return null;
		}
		
		return brandList;
	}
	@Override
	public List<Categories> fetchCategories() {
		// TODO Auto-generated method stub
		
		String hql="";
		Query query;
		
		hql="from Categories";
		query=sessionFactory.getCurrentSession().createQuery(hql);
		List<Categories> categoriesList=(List<Categories>)query.list();
		if(categoriesList.isEmpty())
		{
			return null;
		}
		else
		{
			return categoriesList;
		}
		

	}
@Transactional
	public List<Product> fetchProductByBrandAndCategory(BrandAndCategoryRequest brandAndCategoryRequest) {
		// TODO Auto-generated method stub

		String hql="";
		Query query;
		
		
		if(brandAndCategoryRequest.categoryId==0 && brandAndCategoryRequest.getBrandId()==0){
			hql="from Product";
		}
		else if(brandAndCategoryRequest.categoryId==0){
		 hql="from Product where brand.brandId="+brandAndCategoryRequest.getBrandId();		
		}
		else if(brandAndCategoryRequest.getBrandId()==0){
			hql="from Product where categories.categoryId="+brandAndCategoryRequest.getCategoryId();		
		}
		else{
			hql="from Product where brand.brandId="+brandAndCategoryRequest.getBrandId()+" and categories.categoryId="+brandAndCategoryRequest.getCategoryId();
			}
		
		 query=sessionFactory.getCurrentSession().createQuery(hql);
		@SuppressWarnings("unchecked")
		List<Product> productList=(List<Product>) query.list();
		
		if(productList.isEmpty()){
			return null;
		}
		else
			return productList;
	}

@Transactional
public List<Product> makeProductImageMNullorderProductDetailsList(List<Product> productList)
{
	List<Product> productsList2=new ArrayList<>();
	
	for(Product productList1 : productList)
	{
		productList1.setProductImage(null);
		
		productsList2.add(productList1);
	}
	
	return productsList2;
}



// here we are fetching ProductList SupplierID And BrandId And CategoryId ForApp
// some Error is going on resolving tomorrow
@Transactional
public List<SupplierProductList> fetchProductListBySupplierIDAndBrandIdAndCategoryIdForApp(String supplierId,long categoryId, long brandId) {
					
	
/*	SupplierDAOImpl supplierDAOImpl=new SupplierDAOImpl(sessionFactory);
	List<SupplierProductList> supplierProductList=supplierDAOImpl.fetchProductBySupplierId(supplierId);*/
	
	String hql="from SupplierProductList where supplier.supplierId='"+supplierId+"'";
	   
	Query query=sessionFactory.getCurrentSession().createQuery(hql);
	
	
	List<SupplierProductList> supplierProductList= (List<SupplierProductList>)query.list();
	
	
		
		if(supplierProductList.isEmpty())
		{
			return null;				
		}			
		
		if(categoryId==0 && brandId==0){
			return supplierProductList;
		}
		List<SupplierProductList> supplierProductList2=new ArrayList<>();

		if(categoryId==0){

			for(SupplierProductList supplierProductListAddInventory : supplierProductList)
			{
				if(supplierProductListAddInventory.getProduct().getBrand().getBrandId()==brandId)
				{
					supplierProductList2.add(supplierProductListAddInventory);
				}
			}
        
        }
        else if(brandId==0){
        	for(SupplierProductList supplierProductListAddInventory : supplierProductList)
			{
				if(supplierProductListAddInventory.getProduct().getCategories().getCategoryId()==categoryId)
				{
					supplierProductList2.add(supplierProductListAddInventory);
				}
			}    
        }		
        else
        {
        	for(SupplierProductList supplierProductListAddInventory : supplierProductList)
			{
				if(supplierProductListAddInventory.getProduct().getBrand().getBrandId()==brandId && supplierProductListAddInventory.getProduct().getCategories().getCategoryId()==categoryId)
				{
					supplierProductList2.add(supplierProductListAddInventory);
				}
			}  
        }
		
	return supplierProductList2;
	

	}


@Transactional
public List<SupplierProductList> makeSupplierProductListImageMNullorderProductDetailsList(List<SupplierProductList> supplierProductList){
	
	List<SupplierProductList> supplierProductList2=new ArrayList<>();
	
	for(SupplierProductList supplierProductList1 : supplierProductList)
	{
		supplierProductList1.getProduct().setProductImage(null);
		
		supplierProductList2.add(supplierProductList1);
	}
	
	return supplierProductList2;
}

@Transactional
public Product fetchProductByProductId(long productId) {
	// TODO Auto-generated method stub
	Query query=sessionFactory.getCurrentSession().createQuery("From Product where productId="+productId);
	List<Product> productList=(List<Product>)query.list();
	
	if(productList.isEmpty())
	{
		return null;	
	}
	return productList.get(0);
}


// below two method are for web App

@Transactional
public void saveProductForWebApp(MultipartFile file, Product product) {
	try {
		Blob blob = Hibernate.getLobCreator(sessionFactory.getCurrentSession()).createBlob(file.getInputStream(),
				file.getSize());
		product.setProductImage(blob);
		sessionFactory.getCurrentSession().save(product);
	} catch (Exception e) {
		e.printStackTrace();
		System.out.println("Save Product for WebApp Error :" + e.toString());
	}
}

@Transactional
public void saveMultipleProductForWebApp(List<Product> productList) {
	try {
		if(productList!=null)
		{
			for(Product product:productList)
			{
				sessionFactory.getCurrentSession().save(product);
			}
		}
		
	} catch (Exception e) {
		e.printStackTrace();
		System.out.println("Save Multiple Product for WebApp Error :" + e.toString());
	}
}

@Transactional
public List<Product> fetchProductList() {

	String hql = "from Product";
	Query query = sessionFactory.getCurrentSession().createQuery(hql);

	@SuppressWarnings("unchecked")
	List<Product> productsList = (List<Product>) query.list();
	if (productsList.isEmpty()) {
		return null;
	}
	return productsList;
}
@Transactional
public List<ProductViewList> fetchProductViewListForWebApp() {

	String hql = "from Product";
	Query query = sessionFactory.getCurrentSession().createQuery(hql);
	System.out.println("Query : " + query);
	@SuppressWarnings("unchecked")
	List<Product> productList = (List<Product>) query.list();
	if (productList.isEmpty()) {
		return null;
	}	
	
	List<ProductViewList> productViewList=new ArrayList<ProductViewList>();
	Iterator<Product> itr=productList.iterator();
	int srno=1;
	while(itr.hasNext())
	{
		Product product=itr.next();
		double taxableTotal=product.getCurrentQuantity()*product.getRate();
		double cgstamount=(taxableTotal*product.getCategories().getCgst())/100;
		double igstamount=(taxableTotal*product.getCategories().getIgst())/100;
		double sgstamount=(taxableTotal*product.getCategories().getSgst())/100;
		double total=taxableTotal+igstamount;
		double rateWithTax=product.getRate() + ((product.getRate()*product.getCategories().getIgst())/100);
		productViewList.add(new ProductViewList(srno, 
				product.getProductId(), 
				product.getProductCode(),
				product.getProductName(), 
				product.getCategories().getCategoryName(), 
				product.getBrand().getName(), 
				product.getCategories().getHsnCode(), 
				product.getThreshold(), 
				product.getRate(), 
				rateWithTax,
				product.getCurrentQuantity(), 
				taxableTotal, 
				product.getCategories().getCgst(), 
				cgstamount, 
				product.getCategories().getIgst(),
				igstamount, 
				product.getCategories().getSgst(), 
				sgstamount, 
				total, 
				product.getProductAddedDatetime(), 
				product.getProductQuantityUpdatedDatetime()));
		srno++;
	}
	
	return productViewList;
}

@Transactional
public Product fetchProductForWebApp(long productId) {
	String hql = "from Product where productId=" + productId;
	Query query = sessionFactory.getCurrentSession().createQuery(hql);
	System.out.println("Query : " + query);
	@SuppressWarnings("unchecked")
	List<Product> productList = (List<Product>) query.list();
	if (productList.isEmpty()) {
		return null;
	}
	return productList.get(0);
}

@Transactional
public List<Product> fetchProductListForWebApp() {
	String hql = "from Product";
	Query query = sessionFactory.getCurrentSession().createQuery(hql);
	System.out.println("Query : " + query);
	@SuppressWarnings("unchecked")
	List<Product> productList = (List<Product>) query.list();
	if (productList.isEmpty()) {
		return null;
	}
	return productList;
}

@Transactional
public void updateProductForWebApp(MultipartFile file,Product product) {
	// TODO Auto-generated method stub
	
	try {
		if(!file.isEmpty())
		{
			Blob blob = Hibernate.getLobCreator(sessionFactory.getCurrentSession()).createBlob(file.getInputStream(),
					file.getSize());
			product.setProductImage(blob);
		}
		
		sessionFactory.getCurrentSession().update(product);
	} catch (Exception e) {
		System.out.println("updateProductForWebApp Error : "+e.toString());
	}
}

@Transactional
public List<ProductReportView> fetchProductListForReport(String range,String startDate,String endDate,String topProductNo)
{
	
	List<ProductReportView> productReportViewList=new ArrayList<>();
	String hql="";
	Query query;
	SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
	List<Product> productList=fetchProductListForWebApp();
	long srno=1;
	Calendar cal = Calendar.getInstance();		
	DecimalFormat decimalFormat=new DecimalFormat("#0.00");
	
	if (range.equals("yesterday")) {
		cal.add(Calendar.DAY_OF_MONTH, -1);
		hql="from OrderDetails where date(issueDate) = '"+dateFormat.format(cal.getTime())+"' and orderStatus.status in ('"+Constants.ORDER_STATUS_ISSUED+"','"+Constants.ORDER_STATUS_DELIVERED+"','"+Constants.ORDER_STATUS_CANCELED+"')";
	}
	else if (range.equals("today")) {
		hql="from OrderDetails where date(issueDate) = '"+dateFormat.format(cal.getTime())+"' and orderStatus.status in ('"+Constants.ORDER_STATUS_ISSUED+"','"+Constants.ORDER_STATUS_DELIVERED+"','"+Constants.ORDER_STATUS_CANCELED+"')";
	}
	else if (range.equals("last1year")) {
		cal.add(Calendar.MONTH, -12);
		hql="from OrderDetails where date(issueDate) >= '"+dateFormat.format(cal.getTime())+"' and orderStatus.status in ('"+Constants.ORDER_STATUS_ISSUED+"','"+Constants.ORDER_STATUS_DELIVERED+"','"+Constants.ORDER_STATUS_CANCELED+"')";
	}	
	else if (range.equals("last1month")) {
		cal.add(Calendar.MONTH, -1);
		hql="from OrderDetails where date(issueDate) >= '"+dateFormat.format(cal.getTime())+"' and orderStatus.status in ('"+Constants.ORDER_STATUS_ISSUED+"','"+Constants.ORDER_STATUS_DELIVERED+"','"+Constants.ORDER_STATUS_CANCELED+"')";
	}	
	else if (range.equals("last3months")) {
		cal.add(Calendar.MONTH, -3);
		hql="from OrderDetails where date(issueDate) >= '"+dateFormat.format(cal.getTime())+"' and orderStatus.status in ('"+Constants.ORDER_STATUS_ISSUED+"','"+Constants.ORDER_STATUS_DELIVERED+"','"+Constants.ORDER_STATUS_CANCELED+"')";
	}	
	else if (range.equals("viewAll")) {
		hql="from OrderDetails where orderStatus.status in ('"+Constants.ORDER_STATUS_ISSUED+"','"+Constants.ORDER_STATUS_DELIVERED+"','"+Constants.ORDER_STATUS_CANCELED+"')";
	}
	else if (range.equals("last7days")) {
		cal.add(Calendar.DAY_OF_MONTH, -7);
		hql="from OrderDetails where date(issueDate) >= '"+dateFormat.format(cal.getTime())+"' and orderStatus.status in ('"+Constants.ORDER_STATUS_ISSUED+"','"+Constants.ORDER_STATUS_DELIVERED+"','"+Constants.ORDER_STATUS_CANCELED+"')";
	}
	else if (range.equals("range")) {
		hql="from OrderDetails where date(issueDate) >= '"+startDate+"' and orderStatus.status in ('"+Constants.ORDER_STATUS_ISSUED+"','"+Constants.ORDER_STATUS_DELIVERED+"','"+Constants.ORDER_STATUS_CANCELED+"')";
	}
	else if (range.equals("pickdate")) {
		hql="from OrderDetails where (date(issueDate) = '"+startDate+"') and orderStatus.status in ('"+Constants.ORDER_STATUS_ISSUED+"','"+Constants.ORDER_STATUS_DELIVERED+"','"+Constants.ORDER_STATUS_CANCELED+"')";
	}
	else if (range.equals("TopProducts")) {
		cal.add(Calendar.DAY_OF_MONTH, -1);
		hql="from OrderDetails where orderStatus.status in ('"+Constants.ORDER_STATUS_ISSUED+"','"+Constants.ORDER_STATUS_DELIVERED+"','"+Constants.ORDER_STATUS_CANCELED+"')";
	}
	query=sessionFactory.getCurrentSession().createQuery(hql);
	List<OrderDetails> list=(List<OrderDetails>) query.list();
	
	OrderDetailsDAOImpl orderDetailsDAOImpl=new OrderDetailsDAOImpl(sessionFactory);
	if(productList!=null)
	{
		for(Product product : productList)
		{
			if(product.getProductName().equals("Puranpoli"))
			{
				System.out.println("dsd");
			}
			long quantity=0;
			double amountWithTax=0;
			
			for(OrderDetails orderDetails:list)
			{
				List<OrderProductDetails> orderProductDetailsList=orderDetailsDAOImpl.fetchOrderProductDetailByOrderId(orderDetails.getOrderId());
				for(OrderProductDetails orderProductDetails:orderProductDetailsList)
				{
					if(product.getProductId()==orderProductDetails.getProduct().getProductId())
					{
						quantity+=orderProductDetails.getPurchaseQuantity();
						amountWithTax+=orderProductDetails.getPurchaseAmount();
					}
				}
			}
			
			productReportViewList.add(new ProductReportView(srno, product,quantity, Double.parseDouble(decimalFormat.format(amountWithTax))));
			srno++;
		}
	}
	
	List<ProductReportView> productReportViewList2=new ArrayList<>();
	
	if (range.equals("TopProducts")) {
		Collections.sort(productReportViewList,new SortByQuantity());
		if(productReportViewList.size()>=Long.parseLong(topProductNo))
		{
			long i=1;
			for(ProductReportView productReportView: productReportViewList)
			{
				
				productReportView.setSrno(i);
				productReportViewList2.add(productReportView);
				if(i==Long.parseLong(topProductNo))
				{
					break;
				}
				i++;
			}
			return productReportViewList2;
		}
		else
		{
			return productReportViewList;
		}
	}
	return productReportViewList;
	
}

public class SortByQuantity implements Comparator<ProductReportView> 
{
    public int compare(ProductReportView p1, ProductReportView p2) 
    {
        if (p1.getTotalIssuedQuantity() < p2.getTotalIssuedQuantity()) return -1;
        if (p1.getTotalIssuedQuantity() > p2.getTotalIssuedQuantity()) return 1;
        return 0;
    }    
}

@Transactional
public List<Product> fetchProductListByBrandIdAndCategoryIdForWebApp(long categoryId, long brandId) {
		String hql="";//="from Product where brand.brandId="+brandId+ " and categories.categoryId="+categoryId;
		
		if(categoryId==0 && brandId==0){
			hql="from Product";
		}
		else if(categoryId==0){
			hql="from Product where brand.brandId="+brandId;
		}
		else if(brandId==0)
		{
			hql="from Product where categories.categoryId="+categoryId;
		}
		else
		{
			hql="from Product where brand.brandId="+brandId+ " and categories.categoryId="+categoryId;
		}
		Query query=sessionFactory.getCurrentSession().createQuery(hql);
		List<Product> list=(List<Product>)query.list();
		
		if(list.isEmpty())
		{
			return null;				
		}
		
	return list;
}

@Transactional
public List<Product> fetchProductListByBrandIdAndCategoryIdForWebApp(String supplierId,long categoryId, long brandId) {
					
		List<Product> productsList=fetchProductListBySupplierId(supplierId);
		if(productsList.isEmpty())
		{
			return null;				
		}
		List<Product> productsList2=new ArrayList<>();
		
		
		if(categoryId==0 && brandId==0){
			return productsList;
		}
		
		if(categoryId==0){

			for(Product product : productsList)
			{
				if(product.getBrand().getBrandId()==brandId)
				{
					productsList2.add(product);
				}
			}
        
        }
        else if(brandId==0){
        	for(Product product : productsList)
			{
				if(product.getCategories().getCategoryId()==categoryId)
				{
					productsList2.add(product);
				}
			}    
        }		
        else
        {
        	for(Product product : productsList)
			{
				if(product.getCategories().getCategoryId()==categoryId && product.getBrand().getBrandId()==brandId)
				{
					productsList2.add(product);
				}
			}  
        }
		
	return productsList2;
}

@Transactional
public List<Product> makeProductImageNull(List<Product> productList) {
	// TODO Auto-generated method stub
	
	List<Product> productList2=new ArrayList<>();
	for(Product product : productList)
	{
		product.setProductImage(null);
		productList2.add(product);
	}
		
	return productList2;
}
@Transactional
public List<SupplierProductList> makeSupplierProductImageNull(List<SupplierProductList> supplierProductLists) {
	// TODO Auto-generated method stub
	
	List<SupplierProductList> supplierProductLists2=new ArrayList<>();
	for(SupplierProductList supplierProductList : supplierProductLists)
	{
		supplierProductList.getProduct().setProductImage(null);
		supplierProductLists2.add(supplierProductList);
	}
		
	return supplierProductLists2;
}

@Transactional
public List<Product> fetchProductListBySupplierId(String supplierId) {

	String hql="from SupplierProductList where supplier.supplierId='"+supplierId+"'";
	Query query=sessionFactory.getCurrentSession().createQuery(hql);
	List<SupplierProductList> list=(List<SupplierProductList>)query.list();
	
	if(list.isEmpty())
	{
		return null;
	}
	
	List<Product> productList=new ArrayList<>();
	
	for(SupplierProductList supplierProductList : list )
	{
		productList.add(supplierProductList.getProduct());
	}
	
	productList=makeProductImageNull(productList);
	
return productList;
}
@Transactional
public List<Product> fetchProductByCategoryIdForWebApp(long categoryId) {
	String hql = "from Product where categories.categoryId=" + categoryId;
	Query query = sessionFactory.getCurrentSession().createQuery(hql);
	System.out.println("Query : " + query);
	@SuppressWarnings("unchecked")
	List<Product> productList = (List<Product>) query.list();
	if (productList.isEmpty()) {
		return null;
	}
	return productList;
}
@Transactional
public void updateProductForWebApp(Product product) {
	// TODO Auto-generated method stub
	
	try {
		sessionFactory.getCurrentSession().update(product);
	} catch (Exception e) {
		System.out.println("updateProductForWebApp Error : "+e.toString());
	}
}
}
