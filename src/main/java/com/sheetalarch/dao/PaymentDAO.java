package com.sheetalarch.dao;

import java.util.List;

import com.sheetalarch.admin.models.CollectionReportMain;
import com.sheetalarch.entity.OrderDetails;
import com.sheetalarch.entity.Payment;
import com.sheetalarch.entity.ReOrderDetails;
import com.sheetalarch.models.OrderDetailsForPayment;
import com.sheetalarch.models.PaymentNotReceiveRequest;
import com.sheetalarch.models.PaymentTakeRequest;
import com.sheetalarch.models.ReOrderPaymentNotReceiveRequest;
import com.sheetalarch.models.ReOrderPaymentTakeRequest;

public interface PaymentDAO {

	public void updatePaymentStatusAndOrderStatus(PaymentNotReceiveRequest paymentNotReceiveRequest);
	public void updatePaymentNotReceiveStatusOfReOrder(ReOrderPaymentNotReceiveRequest reOrderPaymentNotReceiveRequest);
	public void savePaymentStatus(PaymentTakeRequest paymentTakeRequest);
	
	public void savePaymentStatusOfReOrder(ReOrderPaymentTakeRequest reOrderPaymentTakeRequest);
	
	//public List<Payment> fetchOrderDetaisForPaymentByEmployeeId(long employeeId,String paymentStatus);
	
	//public Payment fetchPaymentByOrderId(String orderId);


	public List<OrderDetailsForPayment> fetchOrderListForPayment(long employeeId,String paymentStatus);
	public List<OrderDetails> fetchOrderDetailsListForPayment(long employeeId,String paymentStatus);
	public List<ReOrderDetails> fetchReOrderDetailsListForPayment(long employeeId, String paymentStatus);
	public List<Payment> fetchPaymentListByOrderDetailsId(String orderDetailsId);
	
	//webApp
	public double balanceAmountOfBooth(long boothId);
	public CollectionReportMain getCollectionReportDetails(String startDate, String endDate,String range);
}
