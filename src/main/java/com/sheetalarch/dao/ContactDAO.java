package com.sheetalarch.dao;

import com.sheetalarch.entity.Contact;

public interface ContactDAO {
	
	public void save(Contact contact);

	public void update(Contact contact);

	public void delete(long id);

}
