package com.sheetalarch.dao;

import java.util.List;

import com.sheetalarch.entity.Area;
import com.sheetalarch.entity.Cluster;
import com.sheetalarch.entity.Employee;
import com.sheetalarch.entity.Route;



public interface AreaDAO {
	public void save(Area area);

	public void update(Area area);	
	
	//public List<Route> fetchRouteListByEmployeeId(long employeeId);
	public Cluster fetchClusterBySalesmanId(long employeeId);
	public Employee fetchGKEmployeeByClusterId(long clusterId);
	
	public Area fetchAreaBysalesmanId(long employeeId);
	public Area fetchAreaByRouteId(long routeId);
	public Cluster fetchClusterBtRouteId(long routeId);
	
	
	
	
	//webApp
	public List<Area> fetchAreaListByClusterId(long clusterId);
	public List<Area> fetchAreaList();
	public void saveArea(Area area);
	public void updateArea(Area area);
	public Area fetchAreaByAreaId(long areaId);
}
