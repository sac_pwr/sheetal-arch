package com.sheetalarch.dao;

import java.util.List;

import com.sheetalarch.entity.Employee;
import com.sheetalarch.entity.EmployeeDetails;

public interface EmployeeDAO {

	public Employee login(String userId, String password);

	//webApp
	public boolean checkUserIdForWebApp(String userId);
	public boolean checkUserIdForUpdateForWebApp(String userId,String employeeId);
	public void saveForWebApp(Employee employee);
	public void updateForWebApp(Employee employee);
}
