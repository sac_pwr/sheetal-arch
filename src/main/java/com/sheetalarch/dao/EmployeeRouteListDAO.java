/**
 * 
 */
package com.sheetalarch.dao;

import java.util.List;

import com.sheetalarch.entity.EmployeeRouteList;


/**
 * @author aNKIT
 *
 */
public interface EmployeeRouteListDAO {

	public void saveEmployeeAreasForWebApp(String areaIdLists, String employeeDetailsId);
	
	public void updateEmployeeAreasForWebApp(String areaIdLists, String employeeDetailsId);	
	
	
	public List<EmployeeRouteList> fetchEmployeeAreaListByEmployeeDetailsId(String employeeDetailsId);
	
	public String getAreaIdList(List<EmployeeRouteList> employeeAreaList);
	
}
