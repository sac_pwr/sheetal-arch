package com.sheetalarch.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.springframework.stereotype.Component;

@Entity
@Table(name = "employee_rout_list")
@Component
public class EmployeeRouteList {

	@Id
	@Column(name = "employee_route_list_id")
	@GeneratedValue(strategy =GenerationType.AUTO)
	private long employeeRouteListId;
	
	@ManyToOne
    @JoinColumn(name = "cluster_Id")
	private Cluster cluster;
	
	@ManyToOne
    @JoinColumn(name = "area_Id")
	private Area area;
	
	@ManyToOne
    @JoinColumn(name = "route_Id")
	private Route route;
	
	@ManyToOne
    @JoinColumn(name = "employee_details_id")
	private EmployeeDetails employeeDetails;

	public long getEmployeeRouteListId() {
		return employeeRouteListId;
	}

	public void setEmployeeRouteListId(long employeeRouteListId) {
		this.employeeRouteListId = employeeRouteListId;
	}

	public Cluster getCluster() {
		return cluster;
	}

	public void setCluster(Cluster cluster) {
		this.cluster = cluster;
	}

	public Area getArea() {
		return area;
	}

	public void setArea(Area area) {
		this.area = area;
	}

	public Route getRoute() {
		return route;
	}

	public void setRoute(Route route) {
		this.route = route;
	}

	public EmployeeDetails getEmployeeDetails() {
		return employeeDetails;
	}

	public void setEmployeeDetails(EmployeeDetails employeeDetails) {
		this.employeeDetails = employeeDetails;
	}

	@Override
	public String toString() {
		return "EmployeeRouteList [employeeRouteListId=" + employeeRouteListId + ", cluster=" + cluster + ", area="
				+ area + ", route=" + route + ", employeeDetails=" + employeeDetails + "]";
	}

	

	
}
