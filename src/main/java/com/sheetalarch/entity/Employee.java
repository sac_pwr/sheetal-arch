package com.sheetalarch.entity;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.springframework.stereotype.Component;


@Entity
@Table(name="employee")
@Component


public class Employee {
	
	
	@Id
	@Column(name = "employee_id")
	@GeneratedValue(strategy =GenerationType.AUTO)
	private long employeeId;
		
	@Column(name = "user_id")
	private String userId;
	
	@Column(name = "password")
	private String password;
	
	@ManyToOne
    @JoinColumn(name = "department_id")
	private Department department;

	public long getEmployeeId() {
		return employeeId;
	}

	public void setEmployeeId(long employeeId) {
		this.employeeId = employeeId;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}


	public Department getDepartment() {
		return department;
	}

	public void setDepartment(Department department) {
		this.department = department;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	@Override
	public String toString() {
		return "Employee [employeeId=" + employeeId + ", department=" + department + ", userId=" + userId
				+ ", password=" + password + "]";
	}

	
	
	
	
	
	
}
