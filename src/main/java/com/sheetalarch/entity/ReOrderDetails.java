package com.sheetalarch.entity;

import java.io.IOException;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.sheetalarch.entity.OrderDetails.NumericBooleanDeserializer;
import com.sheetalarch.entity.OrderDetails.NumericBooleanSerializer;

@Entity
@Table(name="reOrderDetails")
@Component
public class ReOrderDetails {

	
	@Id
	@Column(name = "reOrder_details_id")
	private String reOrderDetailsId;
	
	@ManyToOne
	@JoinColumn(name="order_details_id")
	private OrderDetails orderDetails;
	
	@Column(name="reorder_Qty")
	private long reOrderQty;
	
	@Column(name="reorder_amt",precision=15, scale=2)
	private double reOrderAmt;
	
	@Column(name="reorder_amt_with_tax",precision=15, scale=2)
	private double reOrderAmtWithTax;
	
	@ManyToOne
	@JoinColumn(name="reorder_booth_id")
	private Booth reOrderToBooth;
	
	@Column(name="reorder_added_datetime")
	@DateTimeFormat(pattern="yyyy-MM-dd")
	private Date reOrderAddedDatetime;
	
	@Column(name="re_order_details_payment_take_datetime")
	@DateTimeFormat(pattern="yyyy-MM-dd")
	private Date reOrderDetailsPaymentTakeDatetime;
	
	@JsonProperty
	@JsonSerialize(using = NumericBooleanSerializer.class)
	@JsonDeserialize(using = NumericBooleanDeserializer.class)
	@Column(name="paid_status")
	private boolean payStatus;


	
	public String getReOrderDetailsId() {
		return reOrderDetailsId;
	}

	public void setReOrderDetailsId(String reOrderDetailsId) {
		this.reOrderDetailsId = reOrderDetailsId;
	}

	public OrderDetails getOrderDetails() {
		return orderDetails;
	}

	public void setOrderDetails(OrderDetails orderDetails) {
		this.orderDetails = orderDetails;
	}

	public long getReOrderQty() {
		return reOrderQty;
	}

	public void setReOrderQty(long reOrderQty) {
		this.reOrderQty = reOrderQty;
	}

	public double getReOrderAmt() {
		return reOrderAmt;
	}

	public void setReOrderAmt(double reOrderAmt) {
		this.reOrderAmt = reOrderAmt;
	}

	public double getReOrderAmtWithTax() {
		return reOrderAmtWithTax;
	}

	public void setReOrderAmtWithTax(double reOrderAmtWithTax) {
		this.reOrderAmtWithTax = reOrderAmtWithTax;
	}

	public Booth getReOrderToBooth() {
		return reOrderToBooth;
	}

	public void setReOrderToBooth(Booth reOrderToBooth) {
		this.reOrderToBooth = reOrderToBooth;
	}

	public Date getReOrderAddedDatetime() {
		return reOrderAddedDatetime;
	}

	public void setReOrderAddedDatetime(Date reOrderAddedDatetime) {
		this.reOrderAddedDatetime = reOrderAddedDatetime;
	}

	public Date getReOrderDetailsPaymentTakeDatetime() {
		return reOrderDetailsPaymentTakeDatetime;
	}

	public void setReOrderDetailsPaymentTakeDatetime(Date reOrderDetailsPaymentTakeDatetime) {
		this.reOrderDetailsPaymentTakeDatetime = reOrderDetailsPaymentTakeDatetime;
	}

	public boolean isPayStatus() {
		return payStatus;
	}

	public void setPayStatus(boolean payStatus) {
		this.payStatus = payStatus;
	}
	

	@Override
	public String toString() {
		return "ReOrderDetails [reOrderDetailsId=" + reOrderDetailsId + ", orderDetails=" + orderDetails
				+ ", reOrderQty=" + reOrderQty + ", reOrderAmt=" + reOrderAmt + ", reOrderAmtWithTax="
				+ reOrderAmtWithTax + ", reOrderToBooth=" + reOrderToBooth + ", reOrderAddedDatetime="
				+ reOrderAddedDatetime + ", reOrderDetailsPaymentTakeDatetime=" + reOrderDetailsPaymentTakeDatetime
				+ ", payStatus=" + payStatus + "]";
	}


	public static class NumericBooleanSerializer extends JsonSerializer<Boolean> {

		@Override
		public void serialize(Boolean bool, JsonGenerator generator, SerializerProvider provider)
				throws IOException, JsonProcessingException {
			generator.writeString(bool ? "1" : "0");
		}  
	}

	public static class NumericBooleanDeserializer extends JsonDeserializer<Boolean> {

		@Override
		public Boolean deserialize(JsonParser parser, DeserializationContext context)
				throws IOException, JsonProcessingException {
			return !"0".equals(parser.getText());
		}
	}
	
	
}
