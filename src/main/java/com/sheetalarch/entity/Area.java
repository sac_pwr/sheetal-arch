package com.sheetalarch.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.springframework.stereotype.Component;


@Entity
@Table(name = "area")
@Component
public class Area {

	@Id
	@Column(name = "area_id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long areaId;
	
	@Column(name = "name")
	private String name;

	@Column(name = "pincode")
	private long pincode;
	
	@ManyToOne
    @JoinColumn(name = "cluster_id")
	private Cluster cluster;

	public long getAreaId() {
		return areaId;
	}

	public void setAreaId(long areaId) {
		this.areaId = areaId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public long getPincode() {
		return pincode;
	}

	public void setPincode(long pincode) {
		this.pincode = pincode;
	}

	public Cluster getCluster() {
		return cluster;
	}

	public void setCluster(Cluster cluster) {
		this.cluster = cluster;
	}

	@Override
	public String toString() {
		return "Area [areaId=" + areaId + ", name=" + name + ", pincode=" + pincode + ", cluster=" + cluster + "]";
	}

	
	

}
