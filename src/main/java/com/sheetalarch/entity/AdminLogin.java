/**
 * 
 */
package com.sheetalarch.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.springframework.stereotype.Component;

/**
 * @author aNKIT
 *
 */
@Entity
@Table(name="admin_login")
@Component
public class AdminLogin {

	@Id
	@Column(name = "admin_login_id")
	@GeneratedValue(strategy =GenerationType.AUTO)
	private long adminLoginId;
	
	@Column(name = "name")
	private String name;
	
	@Column(name = "user_id")
	private String userId;
	
	@Column(name = "password")
	private String password;

	public long getAdminLoginId() {
		return adminLoginId;
	}

	public void setAdminLoginId(long adminLoginId) {
		this.adminLoginId = adminLoginId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	@Override
	public String toString() {
		return "AdminLogin [adminLoginId=" + adminLoginId + ", name=" + name + ", userId=" + userId + ", password="
				+ password + "]";
	}

	
	
	
}
