package com.sheetalarch.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.springframework.stereotype.Component;

@Entity
@Table(name = "dispatchOrderProductDetails")
@Component
public class DispatchOrderProductDetails {
	
	@Id
	@Column(name = "dispatched_product_id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long dispatchedProductId;
	
	@Column(name="total_qty")
	private long totalQty;
	
	@Column(name="total_amt", precision = 19, scale = 2, columnDefinition="DECIMAL(19,2)")
	private double totalAmt;
	
	@ManyToOne
	@JoinColumn(name="product_id")
	private OrderUsedProduct product;
	
	@ManyToOne
	@JoinColumn(name="dispatch_order_details_id")
	private DispatchOrderDetails dispatchOrderDetails;

	public long getDispatchedProductId() {
		return dispatchedProductId;
	}

	public void setDispatchedProductId(long dispatchedProductId) {
		this.dispatchedProductId = dispatchedProductId;
	}

	public long getTotalQty() {
		return totalQty;
	}

	public void setTotalQty(long totalQty) {
		this.totalQty = totalQty;
	}

	public double getTotalAmt() {
		return totalAmt;
	}

	public void setTotalAmt(double totalAmt) {
		this.totalAmt = totalAmt;
	}

	public OrderUsedProduct getProduct() {
		return product;
	}

	public void setProduct(OrderUsedProduct product) {
		this.product = product;
	}

	public DispatchOrderDetails getDispatchOrderDetails() {
		return dispatchOrderDetails;
	}

	public void setDispatchOrderDetails(DispatchOrderDetails dispatchOrderDetails) {
		this.dispatchOrderDetails = dispatchOrderDetails;
	}

	@Override
	public String toString() {
		return "DispatchOrderProductDetails [dispatchedProductId=" + dispatchedProductId + ", totalQty=" + totalQty
				+ ", totalAmt=" + totalAmt + ", product=" + product + ", dispatchOrderDetails=" + dispatchOrderDetails
				+ "]";
	}

	
}
