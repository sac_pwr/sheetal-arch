package com.sheetalarch.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.springframework.stereotype.Component;

@Entity
@Table(name = "cluster")
@Component
public class Cluster {

	@Id
	@Column(name="cluster_id")
	@GeneratedValue(strategy=GenerationType.AUTO)
	private long clusterId;
	
	@Column(name="name")
	private String name;

	public long getClusterId() {
		return clusterId;
	}

	public void setClusterId(long clusterId) {
		this.clusterId = clusterId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return "Cluster [clusterId=" + clusterId + ", name=" + name + "]";
	}
	


	
}
