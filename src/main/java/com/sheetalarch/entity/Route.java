package com.sheetalarch.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.springframework.stereotype.Component;

@Entity
@Table(name = "route")
@Component
public class Route {
	
	
	@Id
	@Column(name = "route_id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long routeId;
	
	@Column(name = "route_no")
	private String routeNo;
	
	@ManyToOne
	@JoinColumn(name="area_id")
	private Area area;

	
	public long getRouteId() {
		return routeId;
	}


	public void setRouteId(long routeId) {
		this.routeId = routeId;
	}


	public String getRouteNo() {
		return routeNo;
	}


	public void setRouteNo(String routeNo) {
		this.routeNo = routeNo;
	}


	public Area getArea() {
		return area;
	}


	public void setArea(Area area) {
		this.area = area;
	}


	@Override
	public String toString() {
		return "Route [routeId=" + routeId + ", routeNo=" + routeNo + ", area=" + area + "]";
	}

	

}
