package com.sheetalarch.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.springframework.stereotype.Component;


@Entity
@Table(name = "dispatchOrderDetails")
@Component
public class DispatchOrderDetails {

	@Id
	@Column(name = "dispatched_id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long dispatchedId;
	
	@ManyToOne
	@JoinColumn(name="order_status")
	private OrderStatus orderStatus;
	
	@Column(name="total_qty")
	private long totalQty;
	
	@Column(name="total_amt")
	private double totalAmount;
	
	@Column(name="dispatched_Added_Date_Time")
	private Date dispatchedAddedDateTime;
	
	@Column(name="delivery_Date")
	private Date deliveryDate;
	
	@ManyToOne
	@JoinColumn(name="area_id")
	private Area salemanArea;
	
	@ManyToOne
	@JoinColumn(name="employeeId")
	private Employee employeeASMAndSM;
	
	@ManyToOne
	@JoinColumn(name="employee_Gk_Id")
	private Employee employeeGk;
	
	@Column(name="salesman_msg")
	private String salesManMsg;

	public long getDispatchedId() {
		return dispatchedId;
	}

	public void setDispatchedId(long dispatchedId) {
		this.dispatchedId = dispatchedId;
	}

	public OrderStatus getOrderStatus() {
		return orderStatus;
	}

	public void setOrderStatus(OrderStatus orderStatus) {
		this.orderStatus = orderStatus;
	}

	public long getTotalQty() {
		return totalQty;
	}

	public void setTotalQty(long totalQty) {
		this.totalQty = totalQty;
	}

	public double getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(double totalAmount) {
		this.totalAmount = totalAmount;
	}

	public Date getDispatchedAddedDateTime() {
		return dispatchedAddedDateTime;
	}

	public void setDispatchedAddedDateTime(Date dispatchedAddedDateTime) {
		this.dispatchedAddedDateTime = dispatchedAddedDateTime;
	}

	public Date getDeliveryDate() {
		return deliveryDate;
	}

	public void setDeliveryDate(Date deliveryDate) {
		this.deliveryDate = deliveryDate;
	}

	public Area getSalemanArea() {
		return salemanArea;
	}

	public void setSalemanArea(Area salemanArea) {
		this.salemanArea = salemanArea;
	}

	public Employee getEmployeeASMAndSM() {
		return employeeASMAndSM;
	}

	public void setEmployeeASMAndSM(Employee employeeASMAndSM) {
		this.employeeASMAndSM = employeeASMAndSM;
	}

	public Employee getEmployeeGk() {
		return employeeGk;
	}

	public void setEmployeeGk(Employee employeeGk) {
		this.employeeGk = employeeGk;
	}

	public String getSalesManMsg() {
		return salesManMsg;
	}

	public void setSalesManMsg(String salesManMsg) {
		this.salesManMsg = salesManMsg;
	}

	@Override
	public String toString() {
		return "DispatchOrderDetails [dispatchedId=" + dispatchedId + ", orderStatus=" + orderStatus + ", totalQty="
				+ totalQty + ", totalAmount=" + totalAmount + ", dispatchedAddedDateTime=" + dispatchedAddedDateTime
				+ ", deliveryDate=" + deliveryDate + ", salemanArea=" + salemanArea + ", employeeASMAndSM="
				+ employeeASMAndSM + ", employeeGk=" + employeeGk + ", salesManMsg=" + salesManMsg + "]";
	}

	
	
}
