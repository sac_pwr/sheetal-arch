package com.sheetalarch.entity;

import java.io.IOException;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@Entity
@Table(name = "order_details")
@Component

public class OrderDetails {

	@Id
	@Column(name = "order_id")
	private String orderId;

	@Column(name = "total_amount", precision = 15, scale = 2)
	private double totalAmount;

	@Column(name = "total_amount_with_tax", precision = 15, scale = 2)
	private double totalAmountWithTax;

	@Column(name = "total_quantity")
	private long totalQuantity;

	@ManyToOne
	@JoinColumn(name = "employee_id_asm_sm")
	private Employee employeeASMandSM;

	@ManyToOne
	@JoinColumn(name = "employee_id_gk")
	private Employee employeeGK;

	@ManyToOne
	@JoinColumn(name = "booth_id")
	private Booth booth;

	@Column(name = "payment_period_days")
	private long paymentPeriodDays;

	@ManyToOne
	@JoinColumn(name = "order_status_id")
	private OrderStatus orderStatus;

	@JsonProperty
	@JsonSerialize(using = NumericBooleanSerializer.class)
	@JsonDeserialize(using = NumericBooleanDeserializer.class)
	@Column(name = "paid_status")
	private boolean payStatus;

	@Column(name = "order_details_payment_take_datetime")
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date orderDetailsPaymentTakeDatetime;

	@Column(name = "order_details_added_datetime")
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date orderDetailsAddedDatetime;

	@Column(name = "cancel_date")
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date cancelDate;

	@Column(name = "issue_date")
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date issueDate;

	@Column(name = "packed_date")
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date packedDate;

	@Column(name = "delivery_date")
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date deliveryDate;

	@Column(name = "confirm_date")
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date confirmDate;

	@Column(name = "cancel_reason")
	private String cancelReason;

	@Column(name = "invoice_number", unique = true)
	private String invoiceNumber;

	@Column(name = "reorder_qty")
	private long reOrderQty;

	@Column(name = "reorder_amt")
	private double reOrderAmt;

	@Column(name = "reorder_amt_with_tax")
	private double reOrderAmtWithTax;

	@Column(name = "reorder_status")
	private String reOrderStatus;

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public double getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(double totalAmount) {
		this.totalAmount = totalAmount;
	}

	public double getTotalAmountWithTax() {
		return totalAmountWithTax;
	}

	public void setTotalAmountWithTax(double totalAmountWithTax) {
		this.totalAmountWithTax = totalAmountWithTax;
	}

	public long getTotalQuantity() {
		return totalQuantity;
	}

	public void setTotalQuantity(long totalQuantity) {
		this.totalQuantity = totalQuantity;
	}

	public Employee getEmployeeASMandSM() {
		return employeeASMandSM;
	}

	public void setEmployeeASMandSM(Employee employeeASMandSM) {
		this.employeeASMandSM = employeeASMandSM;
	}

	public Employee getEmployeeGK() {
		return employeeGK;
	}

	public void setEmployeeGK(Employee employeeGK) {
		this.employeeGK = employeeGK;
	}

	public Booth getBooth() {
		return booth;
	}

	public void setBooth(Booth booth) {
		this.booth = booth;
	}

	public long getPaymentPeriodDays() {
		return paymentPeriodDays;
	}

	public void setPaymentPeriodDays(long paymentPeriodDays) {
		this.paymentPeriodDays = paymentPeriodDays;
	}

	public OrderStatus getOrderStatus() {
		return orderStatus;
	}

	public void setOrderStatus(OrderStatus orderStatus) {
		this.orderStatus = orderStatus;
	}

	public boolean isPayStatus() {
		return payStatus;
	}

	public void setPayStatus(boolean payStatus) {
		this.payStatus = payStatus;
	}

	public Date getOrderDetailsPaymentTakeDatetime() {
		return orderDetailsPaymentTakeDatetime;
	}

	public void setOrderDetailsPaymentTakeDatetime(Date orderDetailsPaymentTakeDatetime) {
		this.orderDetailsPaymentTakeDatetime = orderDetailsPaymentTakeDatetime;
	}

	public Date getOrderDetailsAddedDatetime() {
		return orderDetailsAddedDatetime;
	}

	public void setOrderDetailsAddedDatetime(Date orderDetailsAddedDatetime) {
		this.orderDetailsAddedDatetime = orderDetailsAddedDatetime;
	}

	public Date getCancelDate() {
		return cancelDate;
	}

	public void setCancelDate(Date cancelDate) {
		this.cancelDate = cancelDate;
	}

	public Date getIssueDate() {
		return issueDate;
	}

	public void setIssueDate(Date issueDate) {
		this.issueDate = issueDate;
	}

	public Date getPackedDate() {
		return packedDate;
	}

	public void setPackedDate(Date packedDate) {
		this.packedDate = packedDate;
	}

	public Date getDeliveryDate() {
		return deliveryDate;
	}

	public void setDeliveryDate(Date deliveryDate) {
		this.deliveryDate = deliveryDate;
	}

	public Date getConfirmDate() {
		return confirmDate;
	}

	public void setConfirmDate(Date confirmDate) {
		this.confirmDate = confirmDate;
	}

	public String getCancelReason() {
		return cancelReason;
	}

	public void setCancelReason(String cancelReason) {
		this.cancelReason = cancelReason;
	}

	public String getInvoiceNumber() {
		return invoiceNumber;
	}

	public void setInvoiceNumber(String invoiceNumber) {
		this.invoiceNumber = invoiceNumber;
	}

	public long getReOrderQty() {
		return reOrderQty;
	}

	public void setReOrderQty(long reOrderQty) {
		this.reOrderQty = reOrderQty;
	}

	public double getReOrderAmt() {
		return reOrderAmt;
	}

	public void setReOrderAmt(double reOrderAmt) {
		this.reOrderAmt = reOrderAmt;
	}

	public double getReOrderAmtWithTax() {
		return reOrderAmtWithTax;
	}

	public void setReOrderAmtWithTax(double reOrderAmtWithTax) {
		this.reOrderAmtWithTax = reOrderAmtWithTax;
	}

	public String getReOrderStatus() {
		return reOrderStatus;
	}

	public void setReOrderStatus(String reOrderStatus) {
		this.reOrderStatus = reOrderStatus;
	}

	@Override
	public String toString() {
		return "OrderDetails [orderId=" + orderId + ", totalAmount=" + totalAmount + ", totalAmountWithTax="
				+ totalAmountWithTax + ", totalQuantity=" + totalQuantity + ", employeeASMandSM=" + employeeASMandSM
				+ ", employeeGK=" + employeeGK + ", booth=" + booth + ", paymentPeriodDays=" + paymentPeriodDays
				+ ", orderStatus=" + orderStatus + ", payStatus=" + payStatus + ", orderDetailsPaymentTakeDatetime="
				+ orderDetailsPaymentTakeDatetime + ", orderDetailsAddedDatetime=" + orderDetailsAddedDatetime
				+ ", cancelDate=" + cancelDate + ", issueDate=" + issueDate + ", packedDate=" + packedDate
				+ ", deliveryDate=" + deliveryDate + ", confirmDate=" + confirmDate + ", cancelReason=" + cancelReason
				+ ", invoiceNumber=" + invoiceNumber + ", reOrderQty=" + reOrderQty + ", reOrderAmt=" + reOrderAmt
				+ ", reOrderAmtWithTax=" + reOrderAmtWithTax + ", reOrderStatus=" + reOrderStatus + "]";
	}

	public static class NumericBooleanSerializer extends JsonSerializer<Boolean> {

		@Override
		public void serialize(Boolean bool, JsonGenerator generator, SerializerProvider provider)
				throws IOException, JsonProcessingException {
			generator.writeString(bool ? "1" : "0");
		}
	}

	public static class NumericBooleanDeserializer extends JsonDeserializer<Boolean> {

		@Override
		public Boolean deserialize(JsonParser parser, DeserializationContext context)
				throws IOException, JsonProcessingException {
			return !"0".equals(parser.getText());
		}
	}
}
