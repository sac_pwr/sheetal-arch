package com.sheetalarch.entity;

import java.io.IOException;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@Entity
@Table(name = "booth")
@Component
public class Booth {
	
	@Id
	@Column(name = "booth_id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long boothId;
	
	@Column(name="booth_no",unique=true)
	private String boothNo;
	
	@Column(name = "shop_name")
	private String shopName;
	
	@Column(name="owner_name")
	private String ownerName;
	
	@Column(name="address")
	private String address;
	
	@Column(name = "tax_type")
	private String taxType;
	
	@ManyToOne
	@JoinColumn(name="contact_id")
	private Contact contact;
	
	@ManyToOne
	@JoinColumn(name="route_id")
	private Route route;
	
	@Column(name="gst_no")
	private String gstNo;
	
	@Column(name="booth_added_datetime")
	@DateTimeFormat(pattern="yyyy-MM-dd")
	private Date boothAddedDatetime;
	
	@JsonProperty
	@JsonSerialize(using = NumericBooleanSerializer.class)
	@JsonDeserialize(using = NumericBooleanDeserializer.class)
	@Column(name="status")
	private boolean status;

	

	
	@Override
	public String toString() {
		return "Booth [boothId=" + boothId + ", boothNo=" + boothNo + ", shopName=" + shopName + ", ownerName="
				+ ownerName + ", address=" + address + ", taxType=" + taxType + ", contact=" + contact + ", route="
				+ route + ", gstNo=" + gstNo + ", boothAddedDatetime=" + boothAddedDatetime + ", status=" + status
				+ "]";
	}

	public long getBoothId() {
		return boothId;
	}

	public void setBoothId(long boothId) {
		this.boothId = boothId;
	}

	public String getBoothNo() {
		return boothNo;
	}

	public void setBoothNo(String boothNo) {
		this.boothNo = boothNo;
	}

	public String getShopName() {
		return shopName;
	}

	public void setShopName(String shopName) {
		this.shopName = shopName;
	}

	public String getOwnerName() {
		return ownerName;
	}

	public void setOwnerName(String ownerName) {
		this.ownerName = ownerName;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getTaxType() {
		return taxType;
	}

	public void setTaxType(String taxType) {
		this.taxType = taxType;
	}

	public Contact getContact() {
		return contact;
	}

	public void setContact(Contact contact) {
		this.contact = contact;
	}

	public Route getRoute() {
		return route;
	}

	public void setRoute(Route route) {
		this.route = route;
	}

	public String getGstNo() {
		return gstNo;
	}

	public void setGstNo(String gstNo) {
		this.gstNo = gstNo;
	}

	public Date getBoothAddedDatetime() {
		return boothAddedDatetime;
	}

	public void setBoothAddedDatetime(Date boothAddedDatetime) {
		this.boothAddedDatetime = boothAddedDatetime;
	}

	public boolean isStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}

	public static class NumericBooleanSerializer extends JsonSerializer<Boolean> {

		@Override
		public void serialize(Boolean bool, JsonGenerator generator, SerializerProvider provider)
				throws IOException, JsonProcessingException {
			generator.writeString(bool ? "1" : "0");
		}
	}

	public static class NumericBooleanDeserializer extends JsonDeserializer<Boolean> {

		@Override
		public Boolean deserialize(JsonParser parser, DeserializationContext context)
				throws IOException, JsonProcessingException {
			return !"0".equals(parser.getText());
		}
	}
	
}
