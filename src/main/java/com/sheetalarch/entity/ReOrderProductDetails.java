package com.sheetalarch.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.springframework.stereotype.Component;

@Entity
@Table(name="reOrderProductDetails")
@Component
public class ReOrderProductDetails {

	@Id
	@Column(name="reorder_product_details_id")
	@GeneratedValue(strategy=GenerationType.AUTO)
	private long reOrderProductDetailsId;
	
	@ManyToOne
	@JoinColumn(name="product_id")
	private OrderUsedProduct product;
	
	@ManyToOne
	@JoinColumn(name="reorder_details_id")
	private ReOrderDetails reOrderDetails;
	
	@Column(name="reorder_qty")
	private long reOrderQty;
	
	@Column(name = "selling_rate",precision=15, scale=2)
	private double sellingRate;
	
	@Column(name="reorder_Purchase_amt",precision=15, scale=2)
	private double reOrderPurchaseAmt;

	public long getReOrderProductDetailsId() {
		return reOrderProductDetailsId;
	}

	public void setReOrderProductDetailsId(long reOrderProductDetailsId) {
		this.reOrderProductDetailsId = reOrderProductDetailsId;
	}

	public OrderUsedProduct getProduct() {
		return product;
	}

	public void setProduct(OrderUsedProduct product) {
		this.product = product;
	}

	public ReOrderDetails getReOrderDetails() {
		return reOrderDetails;
	}

	public void setReOrderDetails(ReOrderDetails reOrderDetails) {
		this.reOrderDetails = reOrderDetails;
	}

	public long getReOrderQty() {
		return reOrderQty;
	}

	public void setReOrderQty(long reOrderQty) {
		this.reOrderQty = reOrderQty;
	}

	public double getSellingRate() {
		return sellingRate;
	}

	public void setSellingRate(double sellingRate) {
		this.sellingRate = sellingRate;
	}

	public double getReOrderPurchaseAmt() {
		return reOrderPurchaseAmt;
	}

	public void setReOrderPurchaseAmt(double reOrderPurchaseAmt) {
		this.reOrderPurchaseAmt = reOrderPurchaseAmt;
	}

	@Override
	public String toString() {
		return "ReOrderProductDetails [reOrderProductDetailsId=" + reOrderProductDetailsId + ", product=" + product
				+ ", reOrderDetails=" + reOrderDetails + ", reOrderQty=" + reOrderQty + ", sellingRate=" + sellingRate
				+ ", reOrderPurchaseAmt=" + reOrderPurchaseAmt + "]";
	}

	
}
