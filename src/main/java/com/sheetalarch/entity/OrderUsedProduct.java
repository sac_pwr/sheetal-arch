package com.sheetalarch.entity;	
	import java.sql.Blob;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.springframework.stereotype.Component;

	@Entity
	@Table(name = "order_used_product")
	@Component

	public class OrderUsedProduct {

		@Id
		@Column(name = "product_id")
		@GeneratedValue(strategy = GenerationType.AUTO)
		private long productId;
		
		
		@ManyToOne
	    @JoinColumn(name = "product_entity_id")
	    private Product product;
		
		@Column(name="intra_rate")
		private double intraRate;
		
		@Column(name="inter_rate")
		private double interRate;
		
		@Column(name = "product_name")
		private String productName;

		@Column(name = "product_code")
		private String productCode;

		@ManyToOne
		@JoinColumn(name = "category_id")
		private OrderUsedCategories categories;

		@ManyToOne
		@JoinColumn(name = "brand_id")
		private OrderUsedBrand brand;

		@Column(name = "rate", precision = 19, scale = 2, columnDefinition = "DECIMAL(19,2)")
		private float rate;

		@Column(name = "product_image")
		private Blob productImage;

		@Column(name = "threshold")
		private long threshold;

		@Column(name = "current_quantity")
		private long currentQuantity;

		
	
		public OrderUsedProduct() {
			// TODO Auto-generated constructor stub
		}



		public OrderUsedProduct(long productId, Product product, double intraRate, double interRate, String productName,
				String productCode, OrderUsedCategories categories, OrderUsedBrand brand, float rate, Blob productImage,
				long threshold, long currentQuantity) {
			super();
			this.productId = productId;
			this.product = product;
			this.intraRate = intraRate;
			this.interRate = interRate;
			this.productName = productName;
			this.productCode = productCode;
			this.categories = categories;
			this.brand = brand;
			this.rate = rate;
			this.productImage = productImage;
			this.threshold = threshold;
			this.currentQuantity = currentQuantity;
		}



		public long getProductId() {
			return productId;
		}



		public void setProductId(long productId) {
			this.productId = productId;
		}



		public Product getProduct() {
			return product;
		}



		public void setProduct(Product product) {
			this.product = product;
		}



		public double getIntraRate() {
			return intraRate;
		}



		public void setIntraRate(double intraRate) {
			this.intraRate = intraRate;
		}



		public double getInterRate() {
			return interRate;
		}



		public void setInterRate(double interRate) {
			this.interRate = interRate;
		}



		public String getProductName() {
			return productName;
		}



		public void setProductName(String productName) {
			this.productName = productName;
		}



		public String getProductCode() {
			return productCode;
		}



		public void setProductCode(String productCode) {
			this.productCode = productCode;
		}



		public OrderUsedCategories getCategories() {
			return categories;
		}



		public void setCategories(OrderUsedCategories categories) {
			this.categories = categories;
		}



		public OrderUsedBrand getBrand() {
			return brand;
		}



		public void setBrand(OrderUsedBrand brand) {
			this.brand = brand;
		}



		public float getRate() {
			return rate;
		}



		public void setRate(float rate) {
			this.rate = rate;
		}



		public Blob getProductImage() {
			return productImage;
		}



		public void setProductImage(Blob productImage) {
			this.productImage = productImage;
		}



		public long getThreshold() {
			return threshold;
		}



		public void setThreshold(long threshold) {
			this.threshold = threshold;
		}



		public long getCurrentQuantity() {
			return currentQuantity;
		}



		public void setCurrentQuantity(long currentQuantity) {
			this.currentQuantity = currentQuantity;
		}



		@Override
		public String toString() {
			return "OrderUsedProduct [productId=" + productId + ", product=" + product + ", intraRate=" + intraRate
					+ ", interRate=" + interRate + ", productName=" + productName + ", productCode=" + productCode
					+ ", categories=" + categories + ", brand=" + brand + ", rate=" + rate + ", productImage="
					+ productImage + ", threshold=" + threshold + ", currentQuantity=" + currentQuantity + "]";
		}



}
