package com.sheetalarch.entity;


import java.sql.Blob;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Component;

@Entity
@Table(name = "product")
@Component

public class Product {

	@Id
	@Column(name = "product_id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long productId;

	@Column(name="product_name")
	private String productName;
	
	@Column(name="product_code")
	private String productCode;
	
	@ManyToOne
	@JoinColumn(name="category_id")
	private Categories categories;	
	
	@ManyToOne
	@JoinColumn(name="brand_id")
	private Brand brand;
	
	@Column(name="rate",precision=15, scale=2)
	private float rate;
	
	@Column(name = "product_image")
	private Blob productImage;
	
	@Column(name="product_description")
	private String productDescription;
	
	@Column(name="threshold")
	private long threshold;
	
	@Column(name="current_quantity")
	private long currentQuantity;
	
	@Column(name="intra_rate")
	private double intraRate;
	
	@Column(name="inter_rate")
	private double interRate;
	
	@Column(name="product_added_datetime")
	@DateTimeFormat(pattern="yyyy-MM-dd")
	private Date productAddedDatetime;
	
	@Column(name="product_unit")
    private String productUnit;
		
	@Column(name="product_quantity_updated_datetime")
	@DateTimeFormat(pattern="yyyy-MM-dd")
	private Date productQuantityUpdatedDatetime;

	public long getProductId() {
		return productId;
	}

	public void setProductId(long productId) {
		this.productId = productId;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public String getProductCode() {
		return productCode;
	}

	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}

	public Categories getCategories() {
		return categories;
	}

	public void setCategories(Categories categories) {
		this.categories = categories;
	}

	public Brand getBrand() {
		return brand;
	}

	public void setBrand(Brand brand) {
		this.brand = brand;
	}

	public float getRate() {
		return rate;
	}

	public void setRate(float rate) {
		this.rate = rate;
	}

	public Blob getProductImage() {
		return productImage;
	}

	public void setProductImage(Blob productImage) {
		this.productImage = productImage;
	}

	public String getProductDescription() {
		return productDescription;
	}

	public void setProductDescription(String productDescription) {
		this.productDescription = productDescription;
	}

	public long getThreshold() {
		return threshold;
	}

	public void setThreshold(long threshold) {
		this.threshold = threshold;
	}

	public long getCurrentQuantity() {
		return currentQuantity;
	}

	public void setCurrentQuantity(long currentQuantity) {
		this.currentQuantity = currentQuantity;
	}

	public double getIntraRate() {
		return intraRate;
	}

	public void setIntraRate(double intraRate) {
		this.intraRate = intraRate;
	}

	public double getInterRate() {
		return interRate;
	}

	public void setInterRate(double interRate) {
		this.interRate = interRate;
	}

	public Date getProductAddedDatetime() {
		return productAddedDatetime;
	}

	public void setProductAddedDatetime(Date productAddedDatetime) {
		this.productAddedDatetime = productAddedDatetime;
	}

	public String getProductUnit() {
		return productUnit;
	}

	public void setProductUnit(String productUnit) {
		this.productUnit = productUnit;
	}

	public Date getProductQuantityUpdatedDatetime() {
		return productQuantityUpdatedDatetime;
	}

	public void setProductQuantityUpdatedDatetime(Date productQuantityUpdatedDatetime) {
		this.productQuantityUpdatedDatetime = productQuantityUpdatedDatetime;
	}

	@Override
	public String toString() {
		return "Product [productId=" + productId + ", productName=" + productName + ", productCode=" + productCode
				+ ", categories=" + categories + ", brand=" + brand + ", rate=" + rate + ", productImage="
				+ productImage + ", productDescription=" + productDescription + ", threshold=" + threshold
				+ ", currentQuantity=" + currentQuantity + ", intraRate=" + intraRate + ", interRate=" + interRate
				+ ", productAddedDatetime=" + productAddedDatetime + ", productUnit=" + productUnit
				+ ", productQuantityUpdatedDatetime=" + productQuantityUpdatedDatetime + "]";
	}

	

}