package com.sheetalarch.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.springframework.stereotype.Component;

@Entity
@Table (name="employeeASMAndSM")
@Component
public class EmployeeASMAndSM {

	@Id
	@Column(name="id")
	@GeneratedValue(strategy=GenerationType.AUTO)
	private long employeeASMAndSMId;
	
	@ManyToOne
	@JoinColumn(name="employeeDetail_id_asm")
	private EmployeeDetails employeeDetailsASMId;
	
	@ManyToOne
	@JoinColumn(name="employeeDetail_id_sm")
	private EmployeeDetails employeeDetailsSMId;

	public long getEmployeeASMAndSMId() {
		return employeeASMAndSMId;
	}

	public void setEmployeeASMAndSMId(long employeeASMAndSMId) {
		this.employeeASMAndSMId = employeeASMAndSMId;
	}

	public EmployeeDetails getEmployeeDetailsASMId() {
		return employeeDetailsASMId;
	}

	public void setEmployeeDetailsASMId(EmployeeDetails employeeDetailsASMId) {
		this.employeeDetailsASMId = employeeDetailsASMId;
	}

	public EmployeeDetails getEmployeeDetailsSMId() {
		return employeeDetailsSMId;
	}

	public void setEmployeeDetailsSMId(EmployeeDetails employeeDetailsSMId) {
		this.employeeDetailsSMId = employeeDetailsSMId;
	}

	@Override
	public String toString() {
		return "EmployeeASMAndSM [employeeASMAndSMId=" + employeeASMAndSMId + ", employeeDetailsASMId="
				+ employeeDetailsASMId + ", employeeDetailsSMId=" + employeeDetailsSMId + "]";
	}
	
	
	
}
